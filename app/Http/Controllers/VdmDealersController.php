<?php
namespace App\Http\Controllers;

use App, Config, Request, AuditDealer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;

class VdmDealersController extends Controller {
	

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		Log::info('username:' . $username . '  :: fcode' . $fcode);
		$redisDealerCacheID = 'S_Dealers_' . $fcode;
	
		$dealerlist = $redis->smembers ( $redisDealerCacheID);
		
		$userGroups = null;
		$userGroupsArr = null;
		$dealerWeb=null;
		foreach ( $dealerlist as $key => $value ) {
			
			
			$userGroups = $redis->smembers ( $value);
			
			$userGroups = implode ( '<br/>', $userGroups );
			$detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $value);
			$detail=json_decode($detailJson,true);
			$userGroupsArr = array_add ( $userGroupsArr, $value, $detail['mobileNo'] );
			$dealerWeb = array_add ( $dealerWeb, $value, $detail['website'] );
		}
		
		return view ( 'vdm.dealers.index' )->with ( 'fcode', $fcode )->with ( 'userGroupsArr', $userGroupsArr )->with ( 'dealerlist', $dealerlist )->with ( 'dealerWeb', $dealerWeb );
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	 
	 
	 
	public function create() {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		
	
		Log::info('---------------create:--------------');
		
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		
		$dealer = 'S_dealer_' . $fcode;
		// $vehicleGroups=array("No groups found");
		$vehicleGroups = null;
		$user=null;
		
		$user1= new VdmDealersController;
		$user=$user1->checkuser();
		$id='';
        $availableDeatils=null;
        if(Session::get('cur1') == 'prePaidAdmin'){
        $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);  
        $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
        $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
        $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
        $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
        if($availableBasicLicence==null){
            $availableBasicLicence=0;
        }
        if($availableAdvanceLicence==null){
            $availableAdvanceLicence=0;
        }
        if($availablePremiumLicence==null){
            $availablePremiumLicence=0;
        }
        if($availablePremPlusLicence==null){
            $availablePremPlusLicence=0;
        }
        $availableDeatils='* AVAILABLE DEATAILS  Basic : '.$availableBasicLicence.' Advance : '.$availableAdvanceLicence.' Premium : '.$availablePremiumLicence.' Premium Plus : '.$availablePremPlusLicence;
        }
        
		return view ( 'vdm.dealers.create' )->with ( 'vehicleGroups', $vehicleGroups )->with('dealerName1', $id)->with('user',$user)
		->with('smsP',VdmFranchiseController::smsP())->with('availableDeatils',$availableDeatils);
	}
	
	public function createNew($id)
    {
        if (! Auth::check()) {
            return redirect('login');
        }
        
        $username = Auth::user()->username;
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        
        $dealer = 'S_dealer_' . $fcode;
        $vehicleGroups = null;
        $user=null;
        $availableDeatils=null;
        if(Session::get('cur1') == 'prePaidAdmin'){
        $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);  
        $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
        $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
        $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
        $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
        if($availableBasicLicence==null){
            $availableBasicLicence=0;
        }
        if($availableAdvanceLicence==null){
            $availableAdvanceLicence=0;
        }
        if($availablePremiumLicence==null){
            $availablePremiumLicence=0;
        }
        if($availablePremPlusLicence==null){
            $availablePremPlusLicence=0;
        }
        $availableDeatils='* AVAILABLE DEATAILS  Basic : '.$availableBasicLicence.' Advance : '.$availableAdvanceLicence.' Premium : '.$availablePremiumLicence.' Premium Plus : '.$availablePremPlusLicence;
        }
        $user1= new VdmDealersController;
        $user=$user1->checkuser();
        return view('vdm.dealers.create')->with('dealerName1', $id)->with('vehicleGroups', $vehicleGroups)->with('user', $user)
        ->with('smsP', VdmFranchiseController::smsP())->with('availableDeatils',$availableDeatils);
    }
	
	public function checkuser()
	{
		if (! Auth::check ()) {
            return Redirect::to ( 'login' );
      }
      $username = Auth::user ()->username;
      
       $uri = Route::current()->getName();
      Log::info('URL  '. $uri);
      if(strpos($username,'admin')!==false || $uri=='vdmVehicles.edit') {
             //do nothing
			log::info( '---------- inside if filter----------' . $username) ;
		return 'admin';
      }
	 
      else {
        
		  $redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		log::info( '-----------------inside else filter-------------' . $username .$fcode) ;
		$val1= $redis->sismember ( 'S_Dealers_' . $fcode, $username );
		$val = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		  if($val1==1 || isset($val)) {
			Log::info('---------------is dealer:--------------');
			return 'dealer';
			
		}
		else{
			return null; //TODO should be replaced with aunthorized page - error
		}
	  }
	}
	/**
	 * Store a newly created resource in storage.
	 * TODO validations should be improved to prevent any attacks
	 * 
	 * @return Response
	 */
	
	
	/**
	 * Display the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function show($id) {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		Log::info('---------------inside show:--------------'.$id);
		$redis = Redis::connection ();
		$s3 = App::make('aws')->get('s3');
		
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $id);
			$detail=json_decode($detailJson,true);
		
		$userId=$id;
        $numofBasic=0;$numofAdvance=0;$numofPremium=0;$numofPremiumPlus=0;
		$avlofBasic=0;$avlofAdvance=0;$avlofPremium=0;$avlofPremiumPlus=0;
		if(Session::get('cur1') == 'prePaidAdmin'){
			$numofBasic=isset($detail['numofBasic'])?$detail['numofBasic']:'0';
			$numofAdvance=isset($detail['numofAdvance'])?$detail['numofAdvance']:'0';
			$numofPremium=isset($detail['numofPremium'])?$detail['numofPremium']:'0';
			$numofPremiumPlus=isset($detail['numofPremiumPlus'])?$detail['numofPremiumPlus']:'0';
			$avlofBasic=isset($detail['avlofBasic'])?$detail['avlofBasic']:'0';
			$avlofAdvance=isset($detail['avlofAdvance'])?$detail['avlofAdvance']:'0';
			$avlofPremium=isset($detail['avlofPremium'])?$detail['avlofPremium']:'0';
			$avlofPremiumPlus=isset($detail['avlofPremiumPlus'])?$detail['avlofPremiumPlus']:'0';
            
            if($numofBasic==null){
                $numofBasic=0;
		    }
		    if($numofAdvance==null){
			    $numofAdvance=0;
		    }
		    if($numofPremium==null){
			    $numofPremium=0;
		     }
		    if($numofPremiumPlus==null){
			    $numofPremiumPlus=0;
		    }
		    if($avlofBasic==null){
			  $avlofBasic=0;
		    }
		    if($avlofAdvance==null){
			   $avlofAdvance=0;
		    }
		    if($avlofPremium==null){
			   $avlofPremium=0;
		     }
		    if($avlofPremiumPlus==null){
			   $avlofPremiumPlus=0;
		     }
		}
		Log::info(' image Name----> '.$userId);
		$mobileNo = $detail['mobileNo'];
		$email =$detail['email'];
		if(isset($detail['website'])==1)
			$website=$detail['website'];
		else
			$website='';
		if(isset($detail['website1'])==1)
			$website1=$detail['website1'];
		else
			$website1='';
		if(isset($detail['smsSender'])==1)
			$smsSender=$detail['smsSender'];
		else
			$smsSender='';
		if(isset($detail['smsProvider'])==1)
			$smsProvider=$detail['smsProvider'];
		else
			$smsProvider='nill';
		if(isset($detail['providerUserName'])==1)
			$providerUserName=$detail['providerUserName'];
		else
			$providerUserName='';
		if(isset($detail['providerPassword'])==1)
			$providerPassword=$detail['providerPassword'];
		else
			$providerPassword='';		
		$vehicleGroups = $redis->smembers ( $userId );
		
		$path = $_SERVER['REQUEST_URI'];
		$pathValue =  explode("/", $path);
		$imgLoc = '/'.$pathValue[1].'/public/uploads/';

		$imageName = $this->utilMethod($website);

		$imgSmall=Config::get('constant.endpoint').'/'.Config::get('constant.bucket').'/picture/'.$imageName.'.small.png';
		$imgMob=Config::get('constant.endpoint').'/'.Config::get('constant.bucket').'/picture/'.$imageName.'.png';
		$imgLogo = Config::get('constant.endpoint').'/'.Config::get('constant.bucket').'/picture/'.$userId.'.png';
		//Log::info(' image Name----> '.$userId);
		/*$imgSmall = $imgLoc.$imageName.'.small.png';
		$imgMob = $imgLoc.$imageName.'.png';
		$imgLogo = $imgLoc.$userId.'.png';
		//Log::info(' image as '.$imgSmall);
		if($imageName!=''){
      		$objects = $s3->getListObjectsIterator(array(
            				'Bucket' => Config::get('constant.bucket'),
            				'Prefix' => 'picture/'.$imageName
      					));
      		foreach ($objects as $object) {
      			if (preg_match("/".$imageName.".small.(jpg|png|gif|jpeg|JPG|PNG|GIF|JPEG)/", $object['Key'])) {
          			$imgSmall=Config::get('constant.endpoint').'/'.Config::get('constant.bucket').'/'.$object['Key'];
     			}
     			if (preg_match("/".$imageName.".(jpg|png|gif|jpeg|JPG|PNG|GIF|JPEG)/", $object['Key'])){
          			$imgMob=Config::get('constant.endpoint').'/'.Config::get('constant.bucket').'/'.$object['Key'];
          			
     			}
     			if (preg_match("/".$userId.".(jpg|png|gif|jpeg|JPG|PNG|GIF|JPEG)/", $object['Key'])){
     				$imgLogo=Config::get('constant.endpoint').'/'.Config::get('constant.bucket').'/'.$object['Key'];
     			}
     			log::info('does not match '.$object['Key']);
     			
     		}
      	}*/
		
		return view ( 'vdm.dealers.show', array (
				'userId' => $userId 
		) )->with('mobileNo',$mobileNo)->with('email',$email)->with('website',$website)->with('website1',$website1)->with('smsSender',$smsSender)->with('smsProvider',$smsProvider)->with('providerUserName',$providerUserName)->with('providerPassword',$providerPassword)->with('imgSmall', $imgSmall)->with('imgMob',$imgMob)->with('imgLogo',$imgLogo)->with('numofBasic',$numofBasic)->with('numofAdvance',$numofAdvance)->with('numofPremium',$numofPremium)->with('avlofBasic',$avlofBasic)->with('avlofAdvance',$avlofAdvance)->with('avlofPremium',$avlofPremium)->with('avlofPremiumPlus',$avlofPremiumPlus)->with('numofPremiumPlus',$numofPremiumPlus);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	 
	 
	 public function editDealer($id)
	 {
		 log::info( '------@@@@-----------inside edit dealer------@@@@@-------'.$id) ;
		
		 
		 
		 
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$userId = Session::get('user');
		
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		 log::info( '---------- user----------' . $username) ;
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		
		$detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, Session::get('user'));
			$detail=json_decode($detailJson,true);
         
        $numofBasic=null;$numofAdvance=null;$numofPremium=null;$numofPremiumPlus=null;
		$avlofBasic=null;$avlofAdvance=null;$avlofPremium=null;$avlofPremiumPlus=null;
		if(Session::get('cur1') == 'prePaidAdmin'){
			$numofBasic=isset($detail['numofBasic'])?$detail['numofBasic']:'0';
			$numofAdvance=isset($detail['numofAdvance'])?$detail['numofAdvance']:'0';
			$numofPremium=isset($detail['numofPremium'])?$detail['numofPremium']:'0';
            $numofPremiumPlus=isset($detail['numofPremiumPlus'])?$detail['numofPremiumPlus']:'0';
			$avlofBasic=isset($detail['avlofBasic'])?$detail['avlofBasic']:'0';
			$avlofAdvance=isset($detail['avlofAdvance'])?$detail['avlofAdvance']:'0';
			$avlofPremium=isset($detail['avlofPremium'])?$detail['avlofPremium']:'0';
            $avlofPremiumPlus=isset($detail['avlofPremiumPlus'])?$detail['avlofPremiumPlus']:'0';
		}
         if($numofBasic==null){
			$numofBasic=0;
		}
		if($numofAdvance==null){
			$numofAdvance=0;
		}
		if($numofPremium==null){
			$numofPremium=0;
		}
		if($numofPremiumPlus==null){
			$numofPremiumPlus=0;
		}
		if($avlofBasic==null){
			$avlofBasic=0;
		}
		if($avlofAdvance==null){
			$avlofAdvance=0;
		}
		if($avlofPremium==null){
			$avlofPremium=0;
		}
		if($avlofPremiumPlus==null){
			$avlofPremiumPlus=0;
		}
		
		$dealerid=Session::get('user');
		$mobileNo = $detail['mobileNo'];
		$email =$detail['email'];
		//$zoho = $detail['zoho'];
		if(isset($detail['website'])==1)
			$website=$detail['website'];
		else
			$website='';
		if(isset($detail['website1'])==1)
			$website1=$detail['website1'];
		else
			$website1='';
		if(isset($detail['smsSender'])==1)
			$smsSender=$detail['smsSender'];
		else
			$smsSender='';
		//ram-zoho
		if(isset($detail['zoho'])==1)
		 	$zoho=$detail['zoho'];
		 else
		 	$zoho='';
		 if(isset($detail['mapKey'])==1)
		 	$mapKey=$detail['mapKey'];
		 else
		 	$mapKey='';
		if(isset($detail['mapKey1'])==1)
		 	$mapKey1=$detail['mapKey1'];
		 else
		 	$mapKey1='';
		 if(isset($detail['addressKey'])==1)
		 	$addressKey=$detail['addressKey'];
		 else
		 	$addressKey='';
		 if(isset($detail['notificationKey'])==1)
		 	$notificationKey=$detail['notificationKey'];
		 else
		 	$notificationKey=$redis->hget('H_Google_Android_ServerKey',$fcode.':'.$dealerid);
		if(isset($detail['smsProvider'])==1)
			$smsProvider=$detail['smsProvider'];
		else
			$smsProvider='nill';
		if(isset($detail['providerUserName'])==1)
			$providerUserName=$detail['providerUserName'];
		else
			$providerUserName='';
		if(isset($detail['providerPassword'])==1)
			$providerPassword=$detail['providerPassword'];
		else
			$providerPassword='';	
		if(isset($detail['gpsvtsApp'])==1)
			$gpsvtsAppKey=$detail['gpsvtsApp'];
		else
			$gpsvtsAppKey='null';
		if(isset($detail['subDomain'])==1)
			$subDomain=$detail['subDomain'];
		else
			$subDomain='';
         
         $show='yes';
		 
		$fransRef=$redis->hget('H_Franchise',$fcode);
			$franRefDatajson=json_decode($fransRef,true);
			$adminwebsiteArry=array($franRefDatajson['website'],$franRefDatajson['website2'],$franRefDatajson['website3'],$franRefDatajson['website4']);
			$adminwebsiteArry1=array('www.'.$franRefDatajson['website'],'www.'.$franRefDatajson['website2'],'www.'.$franRefDatajson['website3'],'www.s'.$franRefDatajson['website4']);
			if(in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower('www'.($website)), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry1))) {
				$uploads='no';
			}	else	{
				$uploads='yes';
			}
		return view ( 'vdm.dealers.edit', array (
				'dealerid' => $dealerid 
		) )->with ( 'mobileNo', $mobileNo )-> 
		with('email',$email)-> with('zoho',$zoho)->with('gpsvtsAppKey',$gpsvtsAppKey)->with('mapKey',$mapKey)->with('mapKey1',$mapKey1)->with('addressKey',$addressKey)->with('notificationKey',$notificationKey)->with('website',$website)->with('website1',$website1)->with('smsSender',$smsSender)->with('smsProvider',$smsProvider)->with('providerUserName',$providerUserName)->with('providerPassword',$providerPassword)->with('smsP',VdmFranchiseController::smsP())->with('numofBasic',$numofBasic)->with('numofAdvance',$numofAdvance)->with('numofPremium',$numofPremium)->with('avlofBasic',$avlofBasic)->with('avlofAdvance',$avlofAdvance)->with('avlofPremium',$avlofPremium)->with('show',$show)->with('avlofPremiumPlus',$avlofPremiumPlus)->with('numofPremiumPlus',$numofPremiumPlus)->with('subDomain',$subDomain)->with('uploads',$uploads);
		 
	 }
	 
	 
	 
	 //arun edit
	 
	public function edit($id) {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$userId = $id;
		Log::info(' user id  '.$userId);
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		
		$detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $id);
			$detail=json_decode($detailJson,true);
		
		$dealerid=$id;
        $numofBasic=null;$numofAdvance=null;$numofPremium=null;$numofPremiumPlus=null;
		$avlofBasic=null;$avlofAdvance=null;$avlofPremium=null;$avlofPremiumPlus=null;
		if(Session::get('cur1') == 'prePaidAdmin'){
			$numofBasic=isset($detail['numofBasic'])?$detail['numofBasic']:'0';
			$numofAdvance=isset($detail['numofAdvance'])?$detail['numofAdvance']:'0';
			$numofPremium=isset($detail['numofPremium'])?$detail['numofPremium']:'0';
            $numofPremiumPlus=isset($detail['numofPremiumPlus'])?$detail['numofPremiumPlus']:'0';
			$avlofBasic=isset($detail['avlofBasic'])?$detail['avlofBasic']:'0';
			$avlofAdvance=isset($detail['avlofAdvance'])?$detail['avlofAdvance']:'0';
			$avlofPremium=isset($detail['avlofPremium'])?$detail['avlofPremium']:'0';
            $avlofPremiumPlus=isset($detail['avlofPremiumPlus'])?$detail['avlofPremiumPlus']:'0';
		}
        if($numofBasic==null){
			$numofBasic=0;
		}
		if($numofAdvance==null){
			$numofAdvance=0;
		}
		if($numofPremium==null){
			$numofPremium=0;
		}
		if($numofPremiumPlus==null){
			$numofPremiumPlus=0;
		}
		if($avlofBasic==null){
			$avlofBasic=0;
		}
		if($avlofAdvance==null){
			$avlofAdvance=0;
		}
		if($avlofPremium==null){
			$avlofPremium=0;
		}
		if($avlofPremiumPlus==null){
			$avlofPremiumPlus=0;
		}
        
		$mobileNo = $detail['mobileNo'];
		$email =$detail['email'];
		//$zoho = $detail['zoho'];	
		if(isset($detail['website'])==1)
			$website=$detail['website'];
		else
			$website='';
		if(isset($detail['website1'])==1)
			$website1=$detail['website1'];
		else
			$website1='';
		if(isset($detail['smsSender'])==1)
			$smsSender=$detail['smsSender'];
		else
			$smsSender='';
		//ram-zoho
		if(isset($detail['zoho'])==1)
		 	$zoho=$detail['zoho'];
		 else
		 	$zoho='';
		if(isset($detail['mapKey'])==1)
		 	$mapKey=$detail['mapKey'];
		 else
		 	$mapKey='';
		if(isset($detail['mapKey1'])==1)
		 	$mapKey1=$detail['mapKey1'];
		 else
		 	$mapKey1='';
		 if(isset($detail['addressKey'])==1)
		 	$addressKey=$detail['addressKey'];
		 else
		 	$addressKey='';
		 if(isset($detail['notificationKey'])==1)
		 	$notificationKey=$detail['notificationKey'];
		 else
		 	$notificationKey=$redis->hget ('H_Google_Android_ServerKey',$fcode.':'.$dealerid);
		if(isset($detail['smsProvider'])==1)
			$smsProvider=$detail['smsProvider'];
		else
			$smsProvider='nill';
		if(isset($detail['providerUserName'])==1)
			$providerUserName=$detail['providerUserName'];
		else
			$providerUserName='';
		if(isset($detail['providerPassword'])==1)
			$providerPassword=$detail['providerPassword'];
		else
			$providerPassword='';
		if(isset($detail['gpsvtsApp'])==1)
			$gpsvtsAppKey=$detail['gpsvtsApp'];
		else
			$gpsvtsAppKey='null';
		if(isset($detail['subDomain'])==1)
			$subDomain=$detail['subDomain'];
		else
			$subDomain='';
        $show='no';
        $fransRef=$redis->hget('H_Franchise',$fcode);
		$franRefDatajson=json_decode($fransRef,true);
		$adminwebsiteArry=array($franRefDatajson['website'],$franRefDatajson['website2'],$franRefDatajson['website3'],$franRefDatajson['website4']);
		$adminwebsiteArry1=array('www.'.$franRefDatajson['website'],'www.'.$franRefDatajson['website2'],'www.'.$franRefDatajson['website3'],'www.s'.$franRefDatajson['website4']);
		if(in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower('www'.($website)), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry1))) {
			$uploads='no';
		}
		else{
			$uploads='yes';
		}
		return view ( 'vdm.dealers.edit', array (
				'dealerid' => $dealerid 
		) )->with ( 'mobileNo', $mobileNo )->
		with('email',$email)->with('zoho',$zoho)->with('gpsvtsAppKey',$gpsvtsAppKey)->with('mapKey',$mapKey)->with('addressKey',$addressKey)->with('notificationKey',$notificationKey)->  with('website',$website)->with('smsSender',$smsSender)->with('smsProvider',$smsProvider)->with('providerUserName',$providerUserName)->with('providerPassword',$providerPassword)->with('smsP',VdmFranchiseController::smsP())->with('numofBasic',$numofBasic)->with('numofAdvance',$numofAdvance)->with('numofPremium',$numofPremium)->with('avlofBasic',$avlofBasic)->with('avlofAdvance',$avlofAdvance)->with('avlofPremium',$avlofPremium)->with('show',$show)->with('avlofPremiumPlus',$avlofPremiumPlus)->with('numofPremiumPlus',$numofPremiumPlus)->with('subDomain',$subDomain)->with('uploads',$uploads)->with('website1',$website1)->with('mapKey1',$mapKey1);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function update($id) {
	
		$dealerid = $id;
		Log::info(Input::get ( 'providerPassword' ).' update id  '.$dealerid);
		
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$s3 = App::make('aws')->get('s3');
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		
		$rules = array (
				'mobileNo' => 'required',
				'email' => 'required',
				//'zoho'  => 'required',
		);
		$validator = Validator::make ( Input::all(), $rules );
		if ($validator->fails ()) {
			Log::error('VDM User Controller update validation failed');
			Session::flash ( 'message', 'Update failed. Please check logs for more details' . '!' );
			return Redirect::to ( 'vdmDealers/update' )->withErrors ( $validator );
		} else {
			// store
			$path = $_SERVER['REQUEST_URI'];
			$pathValue =  explode("/", $path);
			
			$oldDealerList  = $redis->hget( 'H_DealerDetails_'.$fcode, $dealerid );
			$detail=json_decode($oldDealerList,true);

			$numofBasic=0;
			$numofAdvance=0;
			$numofPremium=0;
            $numofPremiumPlus=0;
			$avlofBasic=0;
			$avlofAdvance=0;
			$avlofPremium=0;
            $avlofPremiumPlus=0;
            $addofBasic=0;
            $addofAdvance=0;
            $addofPremium=0;
            $addofPremiumPlus=0;
			$mobileNo = Input::get ( 'mobileNo' );
			$email = Input::get ( 'email' );
			$zoho  = Input::get ( 'zoho');
			$mapKey=Input::get('mapKey');
			$mapKey1=Input::get('mapKey1');
			$addressKey=Input::get('addressKey');
			$notificationKey=Input::get('notificationKey');
			$gpsvtsAppKey=Input::get('gpsvtsAppKey');
			$website=Input::get ( 'website' );
			$website1=Input::get ( 'website1' );
			$smsSender=Input::get ( 'smsSender' );
			$smsProvider=Input::get ( 'smsProvider' );
			$providerUserName=Input::get ( 'providerUserName' );
			$providerPassword=Input::get ( 'providerPassword' );
			$subDomain=Input::get ( 'subDomain' );

			$show=Input::get ('show');
			if($website == $website1){
				return Redirect::back()->withErrors ( 'Website names should not be same.' );
			}
						
			$fransRef=$redis->hget('H_Franchise',$fcode);
			$franRefDatajson=json_decode($fransRef,true);
			$adminwebsiteArry=array($franRefDatajson['website'],$franRefDatajson['website2'],$franRefDatajson['website3'],$franRefDatajson['website4']);
			$adminwebsiteArry1=array('www.'.$franRefDatajson['website'],'www.'.$franRefDatajson['website2'],'www.'.$franRefDatajson['website3'],'www.s'.$franRefDatajson['website4']);
			if(in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower('www'.($website)), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry1))) {
				$uploads='no';
			}
			else{
				$uploads='yes';
			}
			log::info('show---------------'.$show);
			//prepaid franchise
			if(Session::get('cur1') == 'prePaidAdmin'){
				$numofBasic1=isset($detail['numofBasic'])?$detail['numofBasic']:'0';
				$numofAdvance1=isset($detail['numofAdvance'])?$detail['numofAdvance']:'0';
				$numofPremium1=isset($detail['numofPremium'])?$detail['numofPremium']:'0';
                $numofPremiumPlus1=isset($detail['numofPremiumPlus'])?$detail['numofPremiumPlus']:'0';
				$avlofBasic1=isset($detail['avlofBasic'])?$detail['avlofBasic']:'0';
				$avlofAdvance1=isset($detail['avlofAdvance'])?$detail['avlofAdvance']:'0';
				$avlofPremium1=isset($detail['avlofPremium'])?$detail['avlofPremium']:'0';
                $avlofPremiumPlus1=isset($detail['avlofPremiumPlus'])?$detail['avlofPremiumPlus']:'0';
				//input from franchise
				$addofBasic=Input::get ( 'addofBasic' );
				$addofAdvance=Input::get ( 'addofAdvance' );
				$addofPremium=Input::get ( 'addofPremium' );
                $addofPremiumPlus=Input::get ( 'addofPremiumPlus' );
                
				$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
				$franchiseDetails=json_decode($franDetails_json,true);
				$availableBasicLicence		=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
				$availableAdvanceLicence	=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
				$availablePremiumLicence	=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
                $availablePremPlusLicence	=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
                
                if($availableBasicLicence<$addofBasic || $availableAdvanceLicence<$addofAdvance || $availablePremiumLicence<$addofPremium || $availablePremPlusLicence<$addofPremiumPlus){
                    return Redirect::back()->withErrors ( 'Number of entered licences is exceeded the available licences.' );
                }
                
				//count add
				$numofBasic=$numofBasic1+$addofBasic;
				$numofAdvance=$numofAdvance1+$addofAdvance;
				$numofPremium=$numofPremium1+$addofPremium;
                $numofPremiumPlus=$numofPremiumPlus1+$addofPremiumPlus;
				$avlofBasic=$avlofBasic1+$addofBasic;
				$avlofAdvance=$avlofAdvance1+$addofAdvance;
				$avlofPremium=$avlofPremium1+$addofPremium;
                $avlofPremiumPlus=$avlofPremiumPlus1+$addofPremiumPlus;
                $LicenceissuedDate=date('d-m-Y');
                if($numofBasic==null){
                    $numofBasic=0;
                }
                if($numofAdvance==null){
                    $numofAdvance=0;
                }
                if($numofPremium==null){
                    $numofPremium=0;
                }
                if($numofPremiumPlus==null){
                    $numofPremiumPlus=0;
                }
                if($avlofBasic==null){
                    $avlofBasic=0;
                }
                if($avlofAdvance==null){
                    $avlofAdvance=0;
                }
                if($avlofPremium==null){
                    $avlofPremium=0;
                }
                if($avlofPremiumPlus==null){
                    $avlofPremiumPlus=0;
                }
				$detail=array(
					'email'	=> $email,
					'mobileNo' => $mobileNo,
					'zoho'   => $zoho,
					'mapKey' => $mapKey,
					'mapKey1' => $mapKey1,
					'addressKey' => $addressKey,
					'notificationKey' => $notificationKey,
					'gpsvtsApp' => $gpsvtsAppKey,
					'website' => $website,
					'website1' => $website1,
					'subDomain'=>$subDomain,
					'smsSender'=>$smsSender,
					'smsProvider'=>$smsProvider,
					'providerUserName'=>$providerUserName,
					'providerPassword'=>$providerPassword,
					'numofBasic'	=>$numofBasic,
					'numofAdvance'	=>$numofAdvance,
					'numofPremium'	=>$numofPremium,
                    'numofPremiumPlus'	=>$numofPremiumPlus,
					'avlofBasic'	=>$avlofBasic,
					'avlofAdvance'	=>$avlofAdvance,
					'avlofPremium'	=>$avlofPremium,
                    'avlofPremiumPlus'	=>$avlofPremiumPlus,
                    'LicenceissuedDate' =>$LicenceissuedDate,
				);
				$franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']-$addofBasic;
				$franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']-$addofAdvance;
				$franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']-$addofPremium;
                if(isset($franchiseDetails['availablePremPlusLicence'])==1){
                     $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']-$addofPremiumPlus;
                }
                
				$detailsJson = json_encode ( $franchiseDetails );
          		$redis->hmset ( 'H_Franchise', $fcode,$detailsJson);
			}else{
				$detail=array(
					'email'	=> $email,
					'mobileNo' => $mobileNo,
					'zoho'   => $zoho,
					'mapKey' => $mapKey,
					'mapKey1' => $mapKey1,
					'addressKey' => $addressKey,
					'notificationKey' => $notificationKey,
					'gpsvtsApp' => $gpsvtsAppKey,
					'website' => $website,
					'website1' => $website1,
					'subDomain'=>$subDomain,
					'smsSender'=>$smsSender,
					'smsProvider'=>$smsProvider,
					'providerUserName'=>$providerUserName,
					'providerPassword'=>$providerPassword,
				);

			}
			
			
			
			Log::info('---------------Dealers 11:--------------');
			$detailJson=json_encode($detail);
			$redis->hset ( 'H_DealerDetails_' . $fcode, $dealerid, $detailJson );
            
            log::info('before entry in AuditDealer ');
            try{
			$mysqlDetails =array();
            $status =array();
            $status=array(
              'fcode' => $fcode,
              'dealerId' => $dealerid,
              'userName'=>$username,
              'status' => Config::get('constant.updated'),
              'addBasicLicence'=> $addofBasic,
			  'addAdvanceLicence'=>$addofAdvance,
              'addPremiumLicence'=>$addofPremium,
              'addPrePlusLicence'=>$addofPremiumPlus,
              'userIpAddress'=>Session::get('userIP'),
            );
            $mysqlDetails   = array_merge($status,$detail);
            //AuditDealer::create($mysqlDetails);
            $modelname = new AuditDealer();   
		    $table = $modelname->getTable();
			$db=$fcode;
		    AuditTables::ChangeDB($db);
			$tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
			if(count($tableExist)>0){
				AuditDealer::create($mysqlDetails);
			}
			else{
				AuditTables::CreateAuditDealer();
				AuditDealer::create($mysqlDetails);
			}
			AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
			}
		   catch (Exception $e) {
		          AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				  log::info('Error inside AuditDealer Update'.$e->getMessage());
				  log::info($mysqlDetails);
				}
            log::info('entry added in AuditDealer');
              



			$redis->hmset ( 'H_UserId_Cust_Map', $dealerid . ':fcode', $fcode, $dealerid . ':mobileNo', $mobileNo,$dealerid.':email',$email, $dealerid.':zoho',$zoho, $dealerid.':mapKey',$mapKey, $dealerid.':addressKey',$addressKey, $dealerid.':notificationKey',$notificationKey );
			if(strpos($gpsvtsAppKey, 'com.') !== false)  {
            $redis->hset ( 'H_Google_Android_ServerKey',$fcode.':'.$dealerid, $notificationKey );
            }
            else {
            $redis->hdel ( 'H_Google_Android_ServerKey',$fcode.':'.$dealerid);
            }
			$oldDealerList = json_decode($oldDealerList, true);

			$imageName = $this->utilMethod($website);

			$mailId = array();
        	$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
        	$franchiseDetails=json_decode($franDetails_json,true);
        	if(isset($franchiseDetails['email2'])==1){
                $mailId[]               = $franchiseDetails['email2'];
        		log::info(array_values($mailId));
        	}

        	$oldList = array();
        	$newList = array();

        	if($oldDealerList['mobileNo'] != $mobileNo){
        		$oldList = array_add($oldList, 'Mobile No ', $oldDealerList['mobileNo']);
        		$newList = array_add($newList, 'Mobile No ', $mobileNo);        		
        	}
    
           if($oldDealerList['zoho'] != $zoho){
        		$oldList = array_add($oldList, 'zoho ', $oldDealerList['zoho']);
        		$newList = array_add($newList, 'zoho ', $zoho);        		
        	}
			
           if($oldDealerList['mapKey'] != $mapKey){
        		$oldList = array_add($oldList, 'Map Key ', $oldDealerList['mapKey']);
        		$newList = array_add($newList, 'Map Key ', $mapKey);        		
        	}
			if((isset($oldDealerList['mapKey1'])?$oldDealerList['mapKey1']:'') != $mapKey1){
        		$oldList = array_add($oldList, 'Map Key1 ', $oldDealerList['mapKey1']);
        		$newList = array_add($newList, 'Map Key1 ', $mapKey1);        		
        	}
           if($oldDealerList['addressKey'] != $addressKey){
        		$oldList = array_add($oldList, 'Address Key ', $oldDealerList['addressKey']);
        		$newList = array_add($newList, 'Address Key ', $addressKey);        		
        	}
        	if($oldDealerList['notificationKey'] != $notificationKey){
        		$oldList = array_add($oldList, 'Notification Key ', $oldDealerList['notificationKey']);
        		$newList = array_add($newList, 'Notification Key ', $notificationKey);        		
        	}
			$gpsvtskey=isset($oldDealerList['gpsvtsAppKey'])?$oldDealerList['gpsvtsAppKey']:'null';
 	        if($gpsvtskey != $gpsvtsAppKey){
        		$oldList = array_add($oldList, 'App Key ', $gpsvtskey);
        		$newList = array_add($newList, 'App Key ', $gpsvtsAppKey);        		
        	}
			
        	if($oldDealerList['email'] != $email){
        		$oldList = array_add($oldList, 'Email ', $oldDealerList['email']);
        		$newList = array_add($newList, 'Email ', $email);        			
        	}

        	if($oldDealerList['website'] != $website){
        		$oldList = array_add($oldList, 'website ', $oldDealerList['website']);
        		$newList = array_add($newList, 'website ', $website);        			
        	}
			if((isset($oldDealerList['website1'])?$oldDealerList['website1']:'') != $website1){
        		$oldList = array_add($oldList, 'website1 ', $oldDealerList['website1']);
        		$newList = array_add($newList, 'website1 ', $website1);        			
        	}
        	if($oldDealerList['smsSender'] != $smsSender){
        		$oldList = array_add($oldList, 'Sms Sender ', $oldDealerList['smsSender']);
        		$newList = array_add($newList, 'Sms Sender ', $smsSender);        			
        	}
        	if($oldDealerList['smsProvider'] != $smsProvider){
        		$oldList = array_add($oldList, 'Sms Provider ', $oldDealerList['smsProvider']);
        		$newList = array_add($newList, 'Sms Provider ', $smsProvider);        			
        	}

        	if($oldDealerList['providerUserName'] != $providerUserName){
        		$oldList = array_add($oldList, 'Provider UserName ', $oldDealerList['providerUserName']);
        		$newList = array_add($newList, 'Provider UserName ', $providerUserName);        			
        	}
        	if($oldDealerList['providerPassword'] != $providerPassword){
        		$oldList = array_add($oldList, 'Provider Password ', $oldDealerList['providerPassword']);
        		$newList = array_add($newList, 'Provider Password ', $providerPassword);        			
        	}
			$oldSubDomain=isset($oldDealerList['subDomain'])?$oldDealerList['subDomain']:'';
			if($oldSubDomain != $subDomain){
        		$oldList = array_add($oldList, 'Sub Domain', $oldSubDomain);
        		$newList = array_add($newList, 'Sub Domain', $subDomain);        			
        	}

        	if((count($oldList) >0) && (count($newList) >0))
	        	if(sizeof($mailId) > 0)
	        		try{
	        			log::info(' inside the try function ');
	        			$caption = "Dealer Id";
			        	Mail::queue('emails.user', array('username'=>$fcode, 'groupName'=>$dealerid, 'oldVehi'=>$oldList, 'newVehi'=> $newList, 'cap'=>$caption), function($message) use ($mailId, $dealerid)
			        	{
			                //Log::info("Inside email :" . Session::get ( 'email' ));
			        		$message->to($mailId)->subject('Dealer Name Updated -' . $dealerid);
			        	});
			        } catch(\Swift_TransportException $e){
				        log::info($e->getMessage());
				    }

			
			$upload_folder = '/var/www/'.$pathValue[1].'/public/temp/'; ///var/www/gitsrc/vamos/public/assets/imgs/
			if($uploads=='yes') {
			if (Input::hasFile('logo_smallEdit'))
			{

				$logoSmall=  Input::file('logo_smallEdit');
			    // $link=  Input::file('logo_mob');
			    // $link=  Input::file('logo_desktop');
			    list($width, $height, $type, $attr) = getimagesize($logoSmall);
			    if($height==52 && $width==52 && $type == 3){
			    	
			    	$file_name_small = $imageName. '.'.'small'.'.' . $logoSmall->getClientOriginalExtension();
			    	 if (!file_exists($upload_folder)) {
		 	            $result = File::makeDirectory($upload_folder, 0777,true);
	                }
			   		$logoSmall->move($upload_folder, $file_name_small);
			   		  $s3->putObject(array(
            			 'Bucket' => Config::get('constant.bucket'), 
            			 'Key' => "picture/".$file_name_small, 
            			 'SourceFile' => $upload_folder.$file_name_small,
            			 'ACL' => 'public-read' 
                     ));
                    File::deleteDirectory($upload_folder);
			    } else {
			    	return Redirect::to ( 'vdmDealers/'.$dealerid.'/edit' )->withErrors ( 'Image Size 52*52 or Image format png is missing.' );
			    }
			}  
			if (Input::hasFile('logo_mobEdit'))
			{
				$logoMob=  Input::file('logo_mobEdit');
				// $link=  Input::file('logo_mob');
			    // $link=  Input::file('logo_desktop');
			    list($width, $height, $type, $attr) = getimagesize($logoMob);
			    if($height==144 && $width==272 && $type == 3){
			    	$file_name_Mob = $imageName.'.' . $logoMob->getClientOriginalExtension();
                    if (!file_exists($upload_folder)) {
		 	            $result = File::makeDirectory($upload_folder, 0777,true);
	                }
			   		$logoMob->move($upload_folder, $file_name_Mob);
                    $s3->putObject(array(
            			'Bucket' => Config::get('constant.bucket'), 
            			'Key' => "picture/".$file_name_Mob, 
            			'SourceFile' => $upload_folder.$file_name_Mob,
            			'ACL' => 'public-read' 
        			));
                    File::deleteDirectory($upload_folder);
			    } else {
			    	return Redirect::to ( 'vdmDealers/'.$dealerid.'/edit' )->withErrors ( 'Image Size 272*144 or Image format png is missing.' );
			    }
			}
			if (Input::hasFile('logo_deskEdit'))
			{
				$logoDesk =  Input::file('logo_deskEdit');
				// $link=  Input::file('logo_mob');
			    // $link=  Input::file('logo_desktop');
			    list($width, $height, $type, $attr) = getimagesize($logoDesk);
			    if($height==144 && $width==144 && $type == 3){
			    	if (!file_exists($upload_folder)) {
		 	            $result = File::makeDirectory($upload_folder, 0777,true);
	                }
			    	$file_name_Desk = $dealerid.'.' . $logoDesk->getClientOriginalExtension();
			   		$logoDesk->move($upload_folder, $file_name_Desk);
                    $s3->putObject(array(
            			'Bucket' => Config::get('constant.bucket'), 
            			'Key' => "picture/".$file_name_Desk, 
            			'SourceFile' => $upload_folder.$file_name_Desk,
            			'ACL' => 'public-read' 
        			));
                    File::deleteDirectory($file_name_Desk);
			    } else {
			    	return Redirect::to ( 'vdmDealers/'.$dealerid.'/edit' )->withErrors ( 'Image Size 144*144 or Image format png is missing.' );
			    }
			}
			}

			// redirect
			Session::flash ( 'message', 'Successfully updated ' . $dealerid . '!' );
			/*if(Session::get('cur')=='dealer')
			{
				Session::flash ( 'message', 'Updates Successfully !' );
			return view ( 'vdm.dealers.edit', array (
				'dealerid' => $dealerid 
		) )->with ( 'mobileNo', $mobileNo )->
		with('email',$email)->with('zoho',$zoho)->with('gpsvtsAppKey',$gpsvtsAppKey)->with('mapKey',$mapKey)->with('addressKey',$addressKey)->with('notificationKey',$notificationKey)->with('website',$website)->with('smsSender',$smsSender)->with('smsProvider',$smsProvider)->with('providerUserName',$providerUserName)->with('providerPassword',$providerPassword)->with('smsP',VdmFranchiseController::smsP())->with('numofBasic',$numofBasic)->with('numofAdvance',$numofAdvance)->with('numofPremium',$numofPremium)->with('avlofBasic',$avlofBasic)->with('avlofAdvance',$avlofAdvance)->with('avlofPremium',$avlofPremium)->with('show',$show)->with('avlofPremiumPlus',$avlofPremiumPlus)->with('numofPremiumPlus',$numofPremiumPlus)->with('subDomain',$subDomain)->with('uploads',$uploads);
			}*/
			return Redirect::to ( 'vdmDealersScan/Search'.$dealerid );
		}
	}



	public function utilMethod($imageName){
		if(substr($imageName,0,3) == 'www')
	       	$imageName = substr($imageName, 4);

		return $imageName;
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	 public function store() {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$s3 = App::make('aws')->get('s3');
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		
			Log::info('---------------Dealers:--------------');
		$rules = array (
				'dealerId' => 'required|alpha_dash',
				'email' => 'required|email',
				'mobileNo' => 'required',  
				'zoho'   => 'required'
				);
                
                
		$validator = Validator::make ( Input::all (), $rules );
       
		if ($validator->fails ()) {
			return Redirect::to ( 'vdmDealers/create' )->withErrors ( $validator );
		}else {
		      $dealerId = Input::get ( 'dealerId' );
              $val = $redis->hget ( 'H_UserId_Cust_Map', $dealerId . ':fcode' );
              $val1= $redis->sismember ( 'S_Dealers_' . $fcode, $dealerId );
		}
		if (strpos($dealerId, 'admin') !== false || strpos($dealerId, 'ADMIN') !== false || strpos($dealerId, 'Admin') !== false) 
		{
			return Redirect::to ( 'vdmDealers/create' )->withErrors ( 'Name with admin not acceptable' );
		}
		if($val1==1 || isset($val)) {
			Log::info('---------------already prsent:--------------');
			Session::flash ( 'message', $dealerId . ' already exist. Please use different id ' . '!' );
			return Redirect::to ( 'vdmDealers/create' );
		}
		else {
			// store
			
			$path = $_SERVER['REQUEST_URI'];
			$pathValue =  explode("/", $path);

			$dealerId = Input::get ( 'dealerId' );
			$email = Input::get ( 'email' );
			$mobileNo = Input::get ( 'mobileNo' ); 
            $website=Input::get ( 'website' );
			$website1=Input::get ( 'website1' );
			$subDomain=Input::get ( 'subDomain' );
            $smsSender=Input::get ( 'smsSender' );
            $smsProvider=Input::get ( 'smsProvider' );
            $providerUserName=Input::get ( 'providerUserName' );
            $providerPassword=Input::get ( 'providerPassword' );
            $zoho=Input::get ( 'zoho' ); //log::info($zoho);
			$mapKey=Input::get ( 'mapKey' );
			$mapKey1=Input::get ( 'mapKey1' );
            $addressKey=Input::get ( 'addressKey' );
            $notificationKey=Input::get ('notificationKey');
			$gpsvtsAppKey=Input::get ( 'gpsvtsAppKey' );
			
			if($website == $website1){
				return Redirect::back()->withErrors ( 'Website names should not be same.' );
			}
			
			$fransRef=$redis->hget('H_Franchise',$fcode);
			$franRefDatajson=json_decode($fransRef,true);
			$adminwebsiteArry=array($franRefDatajson['website'],$franRefDatajson['website2'],$franRefDatajson['website3'],$franRefDatajson['website4']);
			$adminwebsiteArry1=array('www.'.$franRefDatajson['website'],'www.'.$franRefDatajson['website2'],'www.'.$franRefDatajson['website3'],'www.s'.$franRefDatajson['website4']);
			if(in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower('www'.($website)), array_map("strtolower", $adminwebsiteArry)) || in_array(strtolower($website), array_map("strtolower", $adminwebsiteArry1))) {
				$uploads='no';
			}	else	{
				$uploads='yes';
			}
			
            // /var/www/gitsrc/vamos/public/assets/imgs/ for production path
            $upload_folder = '/var/www/'.$pathValue[1].'/public/temp/';

            $imageName = $this->utilMethod($website);

            $totalReports = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
              if($totalReports != null)
              {
              foreach ($totalReports as $key => $value) {
                 $redis-> sadd('S_Users_Reports_Dealer_'.$dealerId.'_'.$fcode, $value);
              }
            }
		if($uploads=='yes') {
            if (Input::hasFile('logo_small'))
			{
				$logoSmall=  Input::file('logo_small');
			    // $link=  Input::file('logo_mob');
			    // $link=  Input::file('logo_desktop');
			    list($width, $height, $type, $attr) = getimagesize($logoSmall);
			    if($height==52 && $width==52 && $type == 3){
			    	$file_name_small = $imageName. '.'.'small'.'.' . $logoSmall->getClientOriginalExtension();
			    	if (!file_exists($upload_folder)) {
		 	            $result = File::makeDirectory($upload_folder, 0777,true);
	                }
			   		$logoSmall->move($upload_folder, $file_name_small);
                    $s3->putObject(array(
            			 'Bucket' => Config::get('constant.bucket'), 
            			 'Key' => "picture/".$file_name_small, 
            			 'SourceFile' => $upload_folder.$file_name_small,
            			 'ACL' => 'public-read' 
                     ));
                    File::deleteDirectory($upload_folder);
			    } else {
			    	return Redirect::to ( 'vdmDealers/create' )->withErrors ( 'Image Size 52*52 or Image format png is missing.' );
			    }
			}  
			if (Input::hasFile('logo_mob'))
			{
				$logoMob=  Input::file('logo_mob');
			    // $link=  Input::file('logo_mob');
			    // $link=  Input::file('logo_desktop');
			    list($width, $height, $type, $attr) = getimagesize($logoMob);
			    if($height==144 && $width==272 && $type == 3){
			    	$file_name_Mob = $imageName.'.' . $logoMob->getClientOriginalExtension();
			    	if (!file_exists($upload_folder)) {
		 	            $result = File::makeDirectory($upload_folder, 0777,true);
	                }
			   		$logoMob->move($upload_folder, $file_name_Mob);
			   		$s3->putObject(array(
            			'Bucket' => Config::get('constant.bucket'), 
            			'Key' => "picture/".$file_name_Mob, 
            			'SourceFile' => $upload_folder.$file_name_Mob,
            			'ACL' => 'public-read' 
        			));
                    File::deleteDirectory($upload_folder);
			    } else {
			    	return Redirect::to ( 'vdmDealers/create' )->withErrors ( 'Image Size 272*144 or Image format png is missing.' );
			    }
			}
			if (Input::hasFile('logo_desk'))
			{
				$logoDesk =  Input::file('logo_desk');
			    // $link=  Input::file('logo_mob');
			    // $link=  Input::file('logo_desktop');
			    list($width, $height, $type, $attr) = getimagesize($logoDesk);
			    if($height==144 && $width==144 && $type == 3){
			    	$file_name_Desk = $dealerId.'.' . $logoDesk->getClientOriginalExtension();
			    	if (!file_exists($upload_folder)) {
		 	            $result = File::makeDirectory($upload_folder, 0777,true);
	                }
			   		$logoDesk->move($upload_folder, $file_name_Desk);
			   		$s3->putObject(array(
            			'Bucket' => Config::get('constant.bucket'), 
            			'Key' => "picture/".$file_name_Desk, 
            			'SourceFile' => $upload_folder.$file_name_Desk,
            			'ACL' => 'public-read' 
        			));
                    File::deleteDirectory($file_name_Desk);
			    } else {
			    	return Redirect::to ( 'vdmDealers/create' )->withErrors ( 'Image Size 144*144 or Image format png is missing.' );
			    }
			}
		}
            if(Session::get('cur1') =='prePaidAdmin'){
                $LicenceissuedDate=date('d-m-Y');
				$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
				$franchiseDetails=json_decode($franDetails_json,true);
				$availableBasicLicence		=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
				$availableAdvanceLicence	=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
				$availablePremiumLicence	=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
                $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
                
				$numofbasic = Input::get ( 'numofbasic' ); 
				$numofadvance = Input::get ( 'numofadvance' ); 
				$numofpremium = Input::get ( 'numofpremium' ); 
                $numofPremiumPlus=Input::get ( 'numofpremiumplus' );
                
                if($numofbasic==null){
					$numofbasic=0;
				}
				if($numofadvance==null){
					$numofadvance=0;
				}
				if($numofpremium==null){
					$numofpremium=0;
				}
				if($numofPremiumPlus==null){
					$numofPremiumPlus=0;
				}
                
				if($availableBasicLicence<$numofbasic || $availableAdvanceLicence<$numofadvance || $availablePremiumLicence<$numofpremium || $availablePremPlusLicence<$numofPremiumPlus){
					return Redirect::to ( 'vdmDealers/create' )->withErrors ( 'Number of entered licences is exceeded the available licences.' );
				}

				$detail=array(
					'email'	=> $email,
					'mobileNo' => $mobileNo,
					'website'=>$website,
					'website1'=>$website1,
					'subDomain'=>$subDomain,
					'smsSender'=>$smsSender,
					'smsProvider'=>$smsProvider,
					'providerUserName'=>$providerUserName,
					'providerPassword'=>$providerPassword,
					'numofBasic'	=>$numofbasic,
					'numofAdvance'	=>$numofadvance,
					'numofPremium'	=>$numofpremium,
                    'numofPremiumPlus'=>$numofPremiumPlus,
					'avlofBasic'	=>$numofbasic,
					'avlofAdvance'	=>$numofadvance,
					'avlofPremium'	=>$numofpremium,
                    'avlofPremiumPlus'=>$numofPremiumPlus,
					'zoho'=>$zoho,
					'mapKey'=>$mapKey,
					'mapKey1'=>$mapKey1,
					'addressKey'=>$addressKey,
					'notificationKey'=>$notificationKey,
					'gpsvtsApp'=>$gpsvtsAppKey,
                    'LicenceissuedDate'=>$LicenceissuedDate,
				);
				$franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']-$numofbasic;
				$franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']-$numofadvance;
				$franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']-$numofpremium;
                if(isset($franchiseDetails['availablePremPlusLicence'])==1){
                     $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']-$numofPremiumPlus;
                }
                
                
				$detailsJson = json_encode ( $franchiseDetails );
          		$redis->hmset ( 'H_Franchise', $fcode,$detailsJson);
			}else{
                // ram
			     $detail=array(
                     'email'	=> $email,
                     'mobileNo' => $mobileNo,
                     'website'=>$website,
					 'website1'=>$website1,
					 'subDomain'=>$subDomain,
                     'smsSender'=>$smsSender,
                     'smsProvider'=>$smsProvider,
                     'providerUserName'=>$providerUserName,
                     'providerPassword'=>$providerPassword,
                     'zoho'=>$zoho,
			         'mapKey'=>$mapKey,
					 'mapKey1'=>$mapKey1,
			         'addressKey'=>$addressKey,
			         'notificationKey'=>$notificationKey,
			         'gpsvtsApp'=>$gpsvtsAppKey,
			     );
                
            }
			$detailJson=json_encode($detail);
			
			$redis->sadd ( 'S_Dealers_' .$fcode, $dealerId );
			$redis->hset ( 'H_DealerDetails_' . $fcode, $dealerId, $detailJson );

            log::info('before entry in AuditDealer ');
            try{
            $mysqlDetails =array();
            $status =array();
            $status=array(
              'fcode' => $fcode,
              'dealerId' => $dealerId,
              'userName'=>$username,
              'status' => Config::get('constant.created'),
              'userIpAddress'=>Session::get('userIP'),
            );
            $mysqlDetails   = array_merge($status,$detail);
            $modelname = new AuditDealer();   
		    $table = $modelname->getTable();
            $db=$fcode;
		    AuditTables::ChangeDB($db);
			$tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
			if(count($tableExist)>0){
				AuditDealer::create($mysqlDetails);
			}
			else{
				AuditTables::CreateAuditDealer();
				AuditDealer::create($mysqlDetails);
			}
			AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
			}
			catch (Exception $e) {
				  AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				  log::info('Error inside Audit_Dealer Create'.$e->getMessage());
				  log::info($mysqlDetails);
				}
            log::info('entry added in AuditDealer');

			if(strpos($gpsvtsAppKey, 'com.') !== false)  {
            $redis->hset ( 'H_Google_Android_ServerKey',$fcode.':'.$dealerId, $notificationKey );
            }
            else {
            $redis->hdel ( 'H_Google_Android_ServerKey',$fcode.':'.$dealerId);
            }
			$password=Input::get ( 'password' );
			if($password==null)
			{
				$password='awesome';
			}
			$redis->hmset ( 'H_UserId_Cust_Map', $dealerId . ':fcode', $fcode, $dealerId . ':mobileNo', $mobileNo,$dealerId.':email',$email, $dealerId.':password', $password );
			$user = new User;
			$user->name = $dealerId;
			$user->username=$dealerId;
			$user->email=$email;
			$user->mobileNo=$mobileNo;
			$user->password=Hash::make($password);
			$user->save();
			
			Session::flash ( 'message', 'Successfully created ' . $dealerId . '!' );
			return Redirect::to ( 'vdmDealersScan/Search'.$dealerId );
		}
	}
	public function destroy($id) {
        if (! Auth::check ()) {
            return Redirect::to ( 'login' );
        }
        $username = Auth::user ()->username;
        $userId = $id;
        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        $delDetail=$redis->hget ( 'H_DealerDetails_' . $fcode, $userId);
        $redis->srem ( 'S_Dealers_' . $fcode, $userId );
        $redis->hdel ( 'H_DealerDetails_' . $fcode, $userId);

        log::info('before entry in AuditDealer ');
           
          try{
          $details = json_decode($delDetail, TRUE);
          $mysqlDetails =array();
            $status =array();
            $status=array(
              'fcode' => $fcode,
              'dealerId' => $userId,
              'userName'=>$username,
              'status' => Config::get('constant.deleted'),
              'userIpAddress'=>Session::get('userIP'),
            );
            $mysqlDetails   = array_merge($status,$details);
            $modelname = new AuditDealer();   
		    $table = $modelname->getTable();
            $db=$fcode;
		    AuditTables::ChangeDB($db);
			$tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
			if(count($tableExist)>0){
				AuditDealer::create($mysqlDetails);
			}
			else{
				AuditTables::CreateAuditDealer();
				AuditDealer::create($mysqlDetails);
			}
			AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
			}
		   catch (Exception $e) {
		          AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				  log::info('Error inside AuditDealer Delete'.$e->getMessage());
				  log::info($mysqlDetails);
				}
            log::info('entry added in AuditDealer');



        $email=$redis->hget('H_UserId_Cust_Map',$userId.':email');
        $mobileNo=$redis->hget('H_UserId_Cust_Map',$userId.':mobileNo');
        $redis->hdel ( 'H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo,$userId.':email',$email,$userId.':password' );
        
        $vehicles=$redis->smembers('S_Vehicles_Dealer_'.$userId.'_'.$fcode);
        foreach ($vehicles as $key => $value) {
            Log::info(" vehicle " .$value);
            $refData=$redis->hget('H_RefData_'.$fcode,$value); 
            $refDataJson1=json_decode($refData, true);
            $refDataArr =   array (
            'deviceId' => isset($refDataJson1['deviceId'])?$refDataJson1['deviceId']:'',
            'shortName' => isset($refDataJson1['shortName'])?$refDataJson1['shortName']:'nill',
            'deviceModel' => isset($refDataJson1['deviceModel'])?$refDataJson1['deviceModel']:'GT06N',
            'regNo' => isset($refDataJson1['regNo'])?$refDataJson1['regNo']:'XXXXX',
            'vehicleMake' => isset($refDataJson1['vehicleMake'])?$refDataJson1['vehicleMake']:' ',
            'vehicleType' =>  isset($refDataJson1['vehicleType'])?$refDataJson1['vehicleType']:'Bus',
            'oprName' => isset($refDataJson1['oprName'])?$refDataJson1['oprName']:'airtel',
            'mobileNo' =>isset($refDataJson1['mobileNo'])?$refDataJson1['mobileNo']:'0123456789',
            'vehicleExpiry' =>isset($refDataJson1['vehicleExpiry'])?$refDataJson1['vehicleExpiry']:'null',
            'onboardDate' =>'',
            'overSpeedLimit' => isset($refDataJson1['overSpeedLimit'])?$refDataJson1['overSpeedLimit']:'60',
            'odoDistance' => isset($refDataJson1['odoDistance'])?$refDataJson1['odoDistance']:'0',
            'driverName' => isset($refDataJson1['driverName'])?$refDataJson1['driverName']:'XXX',
            'gpsSimNo' => isset($refDataJson1['gpsSimNo'])?$refDataJson1['gpsSimNo']:'0123456789',
            'email' => isset($refDataJson1['email'])?$refDataJson1['email']:'',
            'orgId' =>'DEFAULT',
            'sendGeoFenceSMS' => isset($refDataJson1['sendGeoFenceSMS'])?$refDataJson1['sendGeoFenceSMS']:'no',
            'morningTripStartTime' => isset($refDataJson1['morningTripStartTime'])?$refDataJson1['morningTripStartTime']:'',
            'eveningTripStartTime' => isset($refDataJson1['eveningTripStartTime'])?$refDataJson1['eveningTripStartTime']:'',
            'parkingAlert' => isset($refDataJson1['parkingAlert'])?$refDataJson1['parkingAlert']:'no',
            'altShortName'=>isset($refDataJson1['altShortName'])?$refDataJson1['altShortName']:'nill',
            'paymentType'=>isset($refDataJson1['paymentType'])?$refDataJson1['paymentType']:' ',
            'expiredPeriod'=>'',
            'fuel'=>isset($refDataJson1['fuel'])?$refDataJson1['fuel']:'no',
            'fuelType'=>isset($refDataJson1['fuelType'])?$refDataJson1['fuelType']:' ',
            'isRfid'=>isset($refDataJson1['isRfid'])?$refDataJson1['isRfid']:'no',
            'rfidType'=>isset($refDataJson1['rfidType'])?$refDataJson1['rfidType']:'no',
            'OWN'=>'OWN',
            'Licence'=>isset($refDataJson1['Licence'])?$refDataJson1['Licence']:'',
            'Payment_Mode'=>isset($refDataJson1['Payment_Mode'])?$refDataJson1['Payment_Mode']:'',
            'descriptionStatus'=>isset($refDataJson1['descriptionStatus'])?$refDataJson1['descriptionStatus']:'',
            'mintemp'=>isset($refDataJson1['mintemp'])?$refDataJson1['mintemp']:'',
            'maxtemp'=>isset($refDataJson1['maxtemp'])?$refDataJson1['maxtemp']:'',
            'safetyParking'=>isset($refDataJson1['safetyParking'])?$refDataJson1['safetyParking']:'no',
            'licenceissuedDate'=>'',
            'VolIg'=>isset($refDataJson1['VolIg'])?$refDataJson1['VolIg']:'',
            'batteryvolt'=>isset($refDataJson1['batteryvolt'])?$refDataJson1['batteryvolt']:'',
            'ac'=>isset($refDataJson1['ac'])?$refDataJson1['ac']:'',
            'acVolt'=>isset($refDataJson1['acVolt'])?$refDataJson1['acVolt']:'',
            'assetTracker'=>isset($refDataJson1['assetTracker'])?$refDataJson1['assetTracker']:'no',
			 'communicatingPortNo'=>isset($refDataJson1['communicatingPortNo'])?$refDataJson1['communicatingPortNo']:'',
			 'vehicleMode'=>isset($refDataJson1['vehicleMode'])?$refDataJson1['vehicleMode']:'',
            );
            $redis->del('L_Suggest_'.$refDataJson1['shortName'].'_'.$refDataJson1['orgId'].'_'.$fcode);
            $redis->hdel('H_Stopseq_'.$refDataJson1['orgId'].'_'.$fcode, $refDataJson1['shortName'].':'.'morning');
            $redis->hdel('H_Stopseq_'.$refDataJson1['orgId'].'_'.$fcode, $refDataJson1['shortName'].':'.'evening');
            $refDataJson = json_encode ( $refDataArr );
            $redis->hset('H_RefData_' . $fcode, $value, $refDataJson);

            $redis->del('S_'.$value.'_'.$fcode);
            $redis->srem('S_Vehicles_Dealer_'.$userId.'_'.$fcode,$value);
            $redis->sadd('S_Vehicles_Admin_'.$fcode,$value);
            $prod='12.9995857,80.2027206,0,N,1480153001488,0.0,N,N,ON,0,S,N';
            $redis->hset('H_ProData_' . $fcode, $value, $prod);
            $value11=strtoupper($value);
            $devId=strtoupper($refDataJson1['deviceId']);
            $shortName1=strtoupper($refDataJson1['shortName']);
            $shortName=str_replace(' ', '', $shortName1);
            $orgId1=strtoupper($refDataJson1['orgId']);
            $searchKey=$value11.':'.$devId.':'.$shortName.':'.$orgId1.':'.$refDataJson1['gpsSimNo'];
            $redis->hset('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode,$searchKey, $value);
            
            //$redis->sadd ( 'S_Vehicles_' . $fcode, $vehicle );
        }

        $groups=$redis->smembers('S_Groups_Dealer_'.$userId.'_'.$fcode);
        foreach ($groups as $key => $value1) {
            Log::info(" group " .$value1);
            $redis->del($value1);
            $redis->del('S_'.$value1);
            $redis->srem('S_Groups_Dealer_'.$userId.'_'.$fcode,$value1);
            //$redis->sadd('S_Groups_Admin_'.$fcode,$value1);
        }

        $orgDel=$redis->smembers('S_Organisations_Dealer_'.$userId.'_'.$fcode);
        foreach ($orgDel as $key => $value2) {
            Log::info(" org " .$value2);
            $redis->hdel('H_Organisations_'.$fcode,$value2);
            $redis->srem('S_Organisations_Dealer_'.$userId.'_'.$fcode,$value2);
            //$redis->sadd('S_Organisations_Admin'.$fcode,$value2);
            $redis->del('H_Site_'.$value2.'_'.$fcode);
            $redis->del('S_'.$value2);
            $redis->hdel('H_Scheduler_'.$fcode, 'getReadyForMorningSchoolTrips_'.$value2);
            $redis->hdel('H_Scheduler_'.$fcode, 'getReadyForAfternoonSchoolTrips_'.$value2);
            $redis->hdel('H_Scheduler_'.$fcode, 'getReadyForEveningSchoolTrips_'.$value2);
            $redis->del('H_Poi_'.$value2.'_'.$fcode);
            $redis->del('S_Poi_'.$value2.'_'.$fcode);
            $redis->del('S_RouteList_'. $value2.'_'.$fcode);
            
        }

        $users=$redis->smembers('S_Users_Dealer_'.$userId.'_'.$fcode);
        foreach ($users as $key => $value3) {
            Log::info(" user " .$value3);
            $redis->del($value3);
            $redis->srem('S_Users_Dealer_'.$userId.'_'.$fcode,$value3);
            $redis->hdel ( 'H_UserSmsInfo_' . $fcode,$value3);
            $redis->srem('S_Users_Virtual_' . $fcode, $value3);
            $redis->srem ( 'S_Users_AssetTracker_' . $fcode, $value3);
            $redis->del('S_Orgs_' .$value3 . '_' . $fcode);
            $redis->hdel('H_Notification_Map_User', $value3);
            //$redis->sadd('S_Users_Admin_'.$fcode,$value3);
            $redis->hdel ( 'H_UserId_Cust_Map', $value3 . ':fcode', $fcode, $value3 . ':mobileNo', $mobileNo,$value3.':email',$email,$value3.':password' );
            DB::table('users')->where('username', $value3)->delete();
        }
         //'H_VehicleName_Mobile_Dealer_'.$userId.'_Org_'.$fcode
        $redis->del('S_Pre_Onboard_Dealer_'.$userId.'_'.$fcode); 
        $redis->del('H_Pre_Onboard_Dealer_'.$userId.'_'.$fcode); 
        $redis->del('H_VehicleName_Mobile_Dealer_'.$userId.'_Org_'.$fcode);
        $redis->hdel ( 'H_Google_Android_ServerKey',$fcode.':'.$userId );
        Log::info(" about to delete dealer" .$userId);
        
        DB::table('users')->where('username', $userId)->delete();
        
        
        Session::put('email',$email);
        Log::info("Email Id :" . Session::get ( 'email' ));
    /*  
        Mail::queue('emails.welcome', array('fname'=>$fcode,'userId'=>$userId), function($message)
        {
            Log::info("Inside email :" . Session::get ( 'email' ));
        
            $message->to(Session::pull ( 'email' ))->subject('User Id deleted');
        });
*/
		Session::flash ( 'message', 'Successfully deleted ' . $userId . '!' );
		return Redirect::to ( 'vdmDealers' );
	}


	public function dealerCheck(){

		if(!Auth::check()){
			return Redirect::to('login');
		}
		$username =	Auth::user()->username;
		$redis = Redis::connection();
		$newGroupId = Input::get ( 'id');
		$fcode = $redis->hget('H_UserId_Cust_Map',$username.':fcode');
		$dealerName = $redis->sismember('S_Dealers_'.$fcode, $newGroupId);
		$dealerVal = $redis->hget ( 'H_UserId_Cust_Map', $newGroupId . ':fcode' );

		

		if (strpos($newGroupId, 'admin') !== false || strpos($newGroupId, 'ADMIN') !== false || strpos($newGroupId, 'Admin') !== false) 
		{
			return  'Name with admin not acceptable';
		}
		if($dealerName == 1 || isset($dealerVal)){
			return 'Dealer Name already written !';
		}
	}
 public function sendExcel()
    {
        log::info('inside excel');
        $username = Auth::user()->username;
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $devicesList=$redis->smembers('S_Device_' . $fcode);
	
        $file = Excel::create('Dealers List', function ($excel) {
        
            $excel->sheet('Short Name', function ($sheet) {
                $redis = Redis::connection();
                $username = Auth::user()->username;
                log::info('inside the vehiclescan sendExcel---');
                $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
                $redisDealerCacheID = 'S_Dealers_' . $fcode;
                $dealerlist = $redis->smembers($redisDealerCacheID); 
                $userGroups = null;
                $userGroupsArr = null;
                $dealerWeb=null;
		$mailid=null;
                $mobilekey=null;
                $mapkey=null;
                $addressKey=null;
                $notikey=null;
             foreach ($dealerlist as $key => $value) {
            $userGroups = $redis->smembers($value);
            
            $userGroups = implode('<br/>', $userGroups);
            $detailJson=$redis->hget('H_DealerDetails_' . $fcode, $value);
            $detail=json_decode($detailJson, true);
            $userGroupsArr = array_add($userGroupsArr, $value, $detail['mobileNo']);
            $dealerWeb = array_add($dealerWeb, $value, $detail['website']);
            $mailid = array_add($mailid, $value, $detail['email']);
            $gpsvts=isset($detail['gpsvtsApp'])?$detail['gpsvtsApp']:'';
            $mobilekey = array_add($mobilekey, $value, $gpsvts);
            $mapkey = array_add($mapkey, $value, $detail['mapKey']);
            $addressKey = array_add($addressKey, $value, $detail['addressKey']);
            $notikey = array_add($notikey, $value, $detail['notificationKey']);
            }
        
                          
                    $sheet->setWidth([
                                     'A'     =>  5,
                                     'B'    =>   20,
                                     'C'    =>   25,
                                     'D'    =>   40,
				                     'E'    =>   40,
                                     'F'    =>   50,
                                     'G'    =>   50,
                                     'H'    =>   50,
                                     'I'    =>   50
                                     
                             ]);
                $sheet->loadView('emails.dealerDetails',['userGroupsArr'=>$userGroupsArr,'dealerlist'=>$dealerlist,'dealerWeb'=>$dealerWeb,'mailid'=>$mailid,'mobilekey'=>$mobilekey,'mapkey'=>$mapkey,'addressKey'=>$addressKey,'notikey'=>$notikey]);
         
            });
        })->download('xls');
          //    $emailFcode=$redis->hget('H_Franchise', $fcode);
          //    $emailFile=json_decode($emailFcode, true);
          //    $email1=$emailFile['email2'];
          //    $email2=$emailFile['email1'];
          //    $data[]='get all de';
          //    log::info('--------------email outsite------------------>');
          //    $mymail=Mail::send('emails.dealersL', $data, function ($message) use ($file, $email1,$owntype) {
          //      $message->to($email1);
                //$message->to('senthamizhselvi.vamosys@gmail.com');
         //       $message->subject('Dealers List - '.$owntype);
          //      $message->attach($file->store("xls", false, true)['full']);
           //     log::info('-----------email send------------------>');
           //   });
         //   Session::flash('message', 'Dealers List Sent to Your Mail-Id Successfully');
        return Redirect::back();
    }
    
    
     public function licenceAlloc(){
    	if(!Auth::check()){
			return Redirect::to('login');
		}
		$username =	Auth::user()->username;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map',$username.':fcode');
		$dealersList1 = $redis->smembers('S_Dealers_'. $fcode);
        $dealersList=array('' => 'Select' );
		foreach ($dealersList1 as $key => $dealerId) {
			$dealersList=array_add($dealersList,$dealerId,$dealerId);
		}

		if(Session::get('cur')=='admin'){
          $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
          $franchiseDetails=json_decode($franDetails_json,true);
          $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
          $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
          $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0'; 
          $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0'; 
      }

		return view('vdm.dealers.dealLicenceAlc')->with('dealersList',$dealersList)->with('availableBasicLicence',$availableBasicLicence)->with('availableAdvanceLicence',$availableAdvanceLicence)->with('availablePremiumLicence',$availablePremiumLicence)->with('availablePremPlusLicence',$availablePremPlusLicence);

    }

    public function licenceUpdate(){
    	if(!Auth::check()){
			return Redirect::to('login');
		}
		log::info('Indide_Dealer_Licence');
		$username =	Auth::user()->username;
		$redis = Redis::connection();
		$fcode = $redis->hget('H_UserId_Cust_Map',$username.':fcode');
		$rules = array (
        'dealersList' => 'required',
        );

    	$validator = Validator::make ( Input::all (), $rules );
    	if ($validator->fails ()) {
       		 return Redirect::back()->withErrors ( $validator );
   		}else{
   			 $addofBasic = Input::get ( 'addofBasic' );
        	 $addofAdvance = Input::get ( 'addofAdvance' );
        	 $addofPremium = Input::get ( 'addofPremium' );
             $addofPremiumPlus = Input::get ( 'addofPremiumPlus' );
        	 $dealerId = Input::get ( 'dealersList' );
        	 log::info('DealerId '.$dealerId);
        	 if($addofPremium == null && $addofAdvance ==null && $addofBasic==null && $addofPremiumPlus==null){
        	 	return Redirect::back()->withErrors ( 'Please enter the licence Quantity !' );
        	 }else{
        	 	//Dealer Licence Increase
        	 	if($addofBasic==null){
        	 		$addofBasic=0;
        	 	}
        	 	if($addofAdvance==null){
        	 		$addofAdvance=0;
        	 	}
        	 	if($addofPremium==null){
        	 		$addofPremium=0;
        	 	}
                if($addofPremiumPlus==null){
        	 		$addofPremiumPlus=0;
        	 	}
          		$dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode,$dealerId );
          		$dealersDetails=json_decode($dealersDetails_json,true);
          		log::info($dealersDetails);
              	$dealersDetails['avlofBasic']= $dealersDetails['avlofBasic']+$addofBasic;
              	$dealersDetails['avlofAdvance']=$dealersDetails['avlofAdvance']+$addofAdvance;
              	$dealersDetails['avlofPremium']=$dealersDetails['avlofPremium']+$addofPremium;
                if(isset($dealersDetails['avlofPremiumPlus'])==1){
                    $dealersDetails['avlofPremiumPlus']=$dealersDetails['avlofPremiumPlus']+$addofPremiumPlus;
                }
                
              	//number of Licence
              	$dealersDetails['numofBasic']= $dealersDetails['numofBasic']+$addofBasic;
              	$dealersDetails['numofAdvance']=$dealersDetails['numofAdvance']+$addofAdvance;
              	$dealersDetails['numofPremium']=$dealersDetails['numofPremium']+$addofPremium;
                if(isset($dealersDetails['numofPremiumPlus'])==1){
                    $dealersDetails['numofPremiumPlus']=$dealersDetails['numofPremiumPlus']+$addofPremiumPlus;
                }
          		$detailsJson = json_encode ( $dealersDetails );
          		$redis->hmset ( 'H_DealerDetails_'.$fcode,$dealerId,$detailsJson);
          		//Franchise Licence Reduce
          		$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
      			$franchiseDetails=json_decode($franDetails_json,true);
       			$franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']-$addofBasic;  
       			$franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']-$addofAdvance;
       			$franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']-$addofPremium;
                if(isset($franchiseDetails['availablePremPlusLicence'])==1){
                    $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']-$addofPremiumPlus;
                }
       			$detailsJson = json_encode ( $franchiseDetails );
       			$redis->hmset ( 'H_Franchise', $fcode,$detailsJson);
                
                $detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $dealerId);
				$detail=json_decode($detailJson,true);
                $detailofMysql=array(
					'email'	=> isset($detail['email'])?$detail['email']:'-',
					'mobileNo' => isset($detail['mobileNo'])?$detail['mobileNo']:'-',
					'website'=>isset($detail['website'])?$detail['website']:'-',
					'website1'=>isset($detail['website1'])?$detail['website1']:'-',
					'smsSender'=>isset($detail['smsSender'])?$detail['smsSender']:'-',
					'smsProvider'=>isset($detail['smsProvider'])?$detail['smsProvider']:'-',
					'providerUserName'=>isset($detail['providerUserName'])?$detail['providerUserName']:'-',
					'providerPassword'=>isset($detail['providerPassword'])?$detail['providerPassword']:'-',
					'numofBasic'	=>isset($detail['numofBasic'])?$detail['numofBasic']:'-',
					'numofAdvance'	=>isset($detail['numofAdvance'])?$detail['numofAdvance']:'-',
					'numofPremium'	=>isset($detail['numofPremium'])?$detail['numofPremium']:'-',
                    'numofPremiumPlus'=>isset($detail['numofPremiumPlus'])?$detail['numofPremiumPlus']:'-',
					'avlofBasic'	=>isset($detail['avlofBasic'])?$detail['avlofBasic']:'-',
					'avlofAdvance'	=>isset($detail['avlofAdvance'])?$detail['avlofAdvance']:'-',
					'avlofPremium'	=>isset($detail['avlofPremium'])?$detail['avlofPremium']:'-',
                    'avlofPremiumPlus'=>isset($detail['avlofPremiumPlus'])?$detail['avlofPremiumPlus']:'-',
					'zoho'=>isset($detail['zoho'])?$detail['zoho']:'-',
					'mapKey'=>isset($detail['mapKey'])?$detail['mapKey']:'-',
					'mapKey1'=>isset($detail['mapKey1'])?$detail['mapKey1']:'-',
					'addressKey'=>isset($detail['addressKey'])?$detail['addressKey']:'-',
					'notificationKey'=>isset($detail['notificationKey'])?$detail['notificationKey']:'-',
					'gpsvtsApp'=>isset($detail['gpsvtsApp'])?$detail['gpsvtsApp']:'-',
                    'LicenceissuedDate'=>isset($detail['LicenceissuedDate'])?$detail['LicenceissuedDate']:'-',
				  );
                log::info('before entry in AuditDealer ');
                try{
                $mysqlDetails =array();
                $status =array();
                $status=array(
                    'fcode' => $fcode,
                    'dealerId' => $dealerId,
                    'userName'=>$username,
                    'status' => Config::get('constant.licenceALC'),
                    'addBasicLicence'=> $addofBasic,
			        'addAdvanceLicence'=>$addofAdvance,
				    'addPremiumLicence'=>$addofPremium,
                    'addPrePlusLicence'=>$addofPremiumPlus,
                    'userIpAddress'=>Session::get('userIP'),
                );
                $mysqlDetails   = array_merge($status,$detailofMysql);
                $modelname = new AuditDealer();   
		        $table = $modelname->getTable();
                $db=$fcode;
		        AuditTables::ChangeDB($db);
			    $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
			    if(count($tableExist)>0){
				    AuditDealer::create($mysqlDetails);
			    }else{
				    AuditTables::CreateAuditDealer();
				    AuditDealer::create($mysqlDetails);
			    }
			    AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
			    }
		       catch (Exception $e) {
		          AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				  log::info('Error inside AuditDealer licence Allocation'.$e->getMessage());
				  log::info($mysqlDetails);
				}
                log::info('entry added in AuditDealer');
                 
                 
       			$error=' Successfully Allocated Licence for '.$dealerId. '!';
                return Redirect::back()->withErrors ( $error );
        	 }
    	}

    }
  public function disable($dealerId) {
    if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }
    $username =	Auth::user()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $dealerId . ':fcode' );
    $lock= $redis->hget ( 'H_UserId_Cust_Map', $dealerId . ':lock');
    if($lock=='Disabled'){
    	Session::flash ( 'message',$dealerId .'Already Disabled !' );
    }else{
        $redis->hmset ( 'H_UserId_Cust_Map', $dealerId . ':lock','Disabled');
        $detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $dealerId);
		$detail=json_decode($detailJson,true);
         if(Session::get('cur1') == 'prePaidAdmin'){
            $detailofMysql=array(
					'email'	=> isset($detail['email'])?$detail['email']:'-',
					'mobileNo' => isset($detail['mobileNo'])?$detail['mobileNo']:'-',
					'website'=>isset($detail['website'])?$detail['website']:'-',
					'website1'=>isset($detail['website1'])?$detail['website1']:'-',
					'smsSender'=>isset($detail['smsSender'])?$detail['smsSender']:'-',
					'smsProvider'=>isset($detail['smsProvider'])?$detail['smsProvider']:'-',
					'providerUserName'=>isset($detail['providerUserName'])?$detail['providerUserName']:'-',
					'providerPassword'=>isset($detail['providerPassword'])?$detail['providerPassword']:'-',
					'numofBasic'	=>isset($detail['numofBasic'])?$detail['numofBasic']:'-',
					'numofAdvance'	=>isset($detail['numofAdvance'])?$detail['numofAdvance']:'-',
					'numofPremium'	=>isset($detail['numofPremium'])?$detail['numofPremium']:'-',
                    'numofPremiumPlus'=>isset($detail['numofPremiumPlus'])?$detail['numofPremiumPlus']:'-',
					'avlofBasic'	=>isset($detail['avlofBasic'])?$detail['avlofBasic']:'-',
					'avlofAdvance'	=>isset($detail['avlofAdvance'])?$detail['avlofAdvance']:'-',
					'avlofPremium'	=>isset($detail['avlofPremium'])?$detail['avlofPremium']:'-',
                    'avlofPremiumPlus'=>isset($detail['avlofPremiumPlus'])?$detail['avlofPremiumPlus']:'-',
					'zoho'=>isset($detail['zoho'])?$detail['zoho']:'-',
					'mapKey'=>isset($detail['mapKey'])?$detail['mapKey']:'-',
					'mapKey1'=>isset($detail['mapKey1'])?$detail['mapKey1']:'-',
					'addressKey'=>isset($detail['addressKey'])?$detail['addressKey']:'-',
					'notificationKey'=>isset($detail['notificationKey'])?$detail['notificationKey']:'-',
					'gpsvtsApp'=>isset($detail['gpsvtsApp'])?$detail['gpsvtsApp']:'-',
                    'LicenceissuedDate'=>isset($detail['LicenceissuedDate'])?$detail['LicenceissuedDate']:'-',
				);
			}else{
			     $detailofMysql=array(
                     'email'	=> isset($detail['email'])?$detail['email']:'-',
                     'mobileNo' =>isset($detail['mobileNo'])?$detail['mobileNo']:'-',
                     'website'=>isset($detail['website'])?$detail['website']:'-',
					 'website1'=>isset($detail['website1'])?$detail['website1']:'-',
                     'smsSender'=>isset($detail['smsSender'])?$detail['smsSender']:'-',
                     'smsProvider'=>isset($detail['smsProvider'])?$detail['smsProvider']:'-',
                     'providerUserName'=>isset($detail['providerUserName'])?$detail['providerUserName']:'-',
                     'providerPassword'=>isset($detail['providerPassword'])?$detail['providerPassword']:'-',
                     'zoho'=>isset($detail['zoho'])?$detail['zoho']:'-',
			         'mapKey'=>isset($detail['mapKey'])?$detail['mapKey']:'-',
					 'mapKey1'=>isset($detail['mapKey1'])?$detail['mapKey1']:'-',
			         'addressKey'=>isset($detail['addressKey'])?$detail['addressKey']:'-',
			         'notificationKey'=>isset($detail['notificationKey'])?$detail['notificationKey']:'-',
			         'gpsvtsApp'=>isset($detail['gpsvtsApp'])?$detail['gpsvtsApp']:'-',
			     );   
            }
             
            log::info('before entry in AuditDealer ');
            try{
            $mysqlDetails =array();
            $status =array();
            $status=array(
              'fcode' => $fcode,
              'dealerId' => $dealerId,
              'userName'=>$username,
              'status' => Config::get('constant.disabled'),
              'userIpAddress'=>Session::get('userIP'),
            );
            $mysqlDetails   = array_merge($status,$detailofMysql);
            $modelname = new AuditDealer();   
		    $table = $modelname->getTable();
            $db=$fcode;
		    AuditTables::ChangeDB($db);
			$tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
			if(count($tableExist)>0){
				AuditDealer::create($mysqlDetails);
			}
			else{
				AuditTables::CreateAuditDealer();
				AuditDealer::create($mysqlDetails);
			}
			AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
			}
		   catch (Exception $e) {
		          AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				  log::info('Error inside AuditDealer for Disable'.$e->getMessage());
				  log::info($mysqlDetails);
				}
            log::info('entry added in AuditDealer');
    	Session::flash ( 'message',$dealerId .' Successfully Disabled !' );
    }
   	
   	
   	return Redirect::to ( 'vdmDealersScan/Search'.$dealerId );

  }
   public function enable($dealerId) {
    if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }
    $username =	Auth::user()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $dealerId . ':fcode' );
    $lock= $redis->hget ( 'H_UserId_Cust_Map', $dealerId . ':lock');
    if($lock=='Disabled'){
    	$redis->hdel ( 'H_UserId_Cust_Map', $dealerId . ':lock');

    	$detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $dealerId);
		$detail=json_decode($detailJson,true);
         if(Session::get('cur1') == 'prePaidAdmin'){
            $detailofMysql=array(
					'email'	=> isset($detail['email'])?$detail['email']:'-',
					'mobileNo' => isset($detail['mobileNo'])?$detail['mobileNo']:'-',
					'website'=>isset($detail['website'])?$detail['website']:'-',
					'website1'=>isset($detail['website1'])?$detail['website1']:'-',
					'smsSender'=>isset($detail['smsSender'])?$detail['smsSender']:'-',
					'smsProvider'=>isset($detail['smsProvider'])?$detail['smsProvider']:'-',
					'providerUserName'=>isset($detail['providerUserName'])?$detail['providerUserName']:'-',
					'providerPassword'=>isset($detail['providerPassword'])?$detail['providerPassword']:'-',
					'numofBasic'	=>isset($detail['numofBasic'])?$detail['numofBasic']:'-',
					'numofAdvance'	=>isset($detail['numofAdvance'])?$detail['numofAdvance']:'-',
					'numofPremium'	=>isset($detail['numofPremium'])?$detail['numofPremium']:'-',
                    'numofPremiumPlus'=>isset($detail['numofPremiumPlus'])?$detail['numofPremiumPlus']:'-',
					'avlofBasic'	=>isset($detail['avlofBasic'])?$detail['avlofBasic']:'-',
					'avlofAdvance'	=>isset($detail['avlofAdvance'])?$detail['avlofAdvance']:'-',
					'avlofPremium'	=>isset($detail['avlofPremium'])?$detail['avlofPremium']:'-',
                    'avlofPremiumPlus'=>isset($detail['avlofPremiumPlus'])?$detail['avlofPremiumPlus']:'-',
					'zoho'=>isset($detail['zoho'])?$detail['zoho']:'-',
					'mapKey'=>isset($detail['mapKey'])?$detail['mapKey']:'-',
					'mapKey1'=>isset($detail['mapKey1'])?$detail['mapKey1']:'-',
					'addressKey'=>isset($detail['addressKey'])?$detail['addressKey']:'-',
					'notificationKey'=>isset($detail['notificationKey'])?$detail['notificationKey']:'-',
					'gpsvtsApp'=>isset($detail['gpsvtsApp'])?$detail['gpsvtsApp']:'-',
                    'LicenceissuedDate'=>isset($detail['LicenceissuedDate'])?$detail['LicenceissuedDate']:'-',
				);
			}else{
			     $detailofMysql=array(
                     'email'	=> isset($detail['email'])?$detail['email']:'-',
                     'mobileNo' =>isset($detail['mobileNo'])?$detail['mobileNo']:'-',
                     'website'=>isset($detail['website'])?$detail['website']:'-',
					 'website1'=>isset($detail['website1'])?$detail['website1']:'-',
                     'smsSender'=>isset($detail['smsSender'])?$detail['smsSender']:'-',
                     'smsProvider'=>isset($detail['smsProvider'])?$detail['smsProvider']:'-',
                     'providerUserName'=>isset($detail['providerUserName'])?$detail['providerUserName']:'-',
                     'providerPassword'=>isset($detail['providerPassword'])?$detail['providerPassword']:'-',
                     'zoho'=>isset($detail['zoho'])?$detail['zoho']:'-',
			         'mapKey'=>isset($detail['mapKey'])?$detail['mapKey']:'-',
					 'mapKey1'=>isset($detail['mapKey1'])?$detail['mapKey1']:'-',
			         'addressKey'=>isset($detail['addressKey'])?$detail['addressKey']:'-',
			         'notificationKey'=>isset($detail['notificationKey'])?$detail['notificationKey']:'-',
			         'gpsvtsApp'=>isset($detail['gpsvtsApp'])?$detail['gpsvtsApp']:'-',
			     );   
            }
             
            log::info('before entry in AuditDealer ');
            try{
            $mysqlDetails =array();
            $status =array();
            $status=array(
              'fcode' => $fcode,
              'dealerId' => $dealerId,
              'userName'=>$username,
              'status' => Config::get('constant.enabled'),
              'userIpAddress'=>Session::get('userIP'),
            );
            $mysqlDetails   = array_merge($status,$detailofMysql);
            $modelname = new AuditDealer();   
		    $table = $modelname->getTable();
            $db=$fcode;
		    AuditTables::ChangeDB($db);
			$tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
			if(count($tableExist)>0){
				AuditDealer::create($mysqlDetails);
			}
			else{
				AuditTables::CreateAuditDealer();
				AuditDealer::create($mysqlDetails);
			}
			AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
			}
		   catch (Exception $e) {
		          AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				  log::info('Error inside AuditDealer Enable'.$e->getMessage());
				  log::info($mysqlDetails);
				}
            log::info('entry added in AuditDealer');
            Session::flash ( 'message',$dealerId .' Successfully Enabled !' );
        
	}else{
		Session::flash ( 'message',$dealerId .'Already Enabled !' );
	}
    return Redirect::to ( 'vdmDealersScan/Search'.$dealerId );
  }
    
    
}
