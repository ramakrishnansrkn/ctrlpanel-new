<?php
namespace App\Http\Controllers;

use Request;
use App, Config, DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;


class AuditTables extends Controller {

public static function CreateAuditFrans()
	{
		Schema::create('Audit_Frans', function($table)
		{
			$table->increments('id'); //Autoincremented primary key
			$table->string('username');
			$table->string('status');
            $table->string('userIpAddress')->nullable();
			$table->string('fname')->nullable();
			$table->text('description')->nullable();
			$table->text('fcode')->nullable();
			$table->string('landline')->nullable();
			$table->string('mobileNo1')->nullable();
			$table->string('mobileNo2')->nullable();
			$table->string('prepaid')->nullable();
			$table->string('email1')->nullable();
			$table->string('email2')->nullable();
			$table->string('userId')->nullable();
			$table->text('fullAddress')->nullable();
			$table->text('otherDetails')->nullable();
			$table->integer('numberofLicence')->nullable();
			$table->integer('availableLincence')->nullable();
			$table->integer('addLicence')->nullable();
            $table->integer('numberofBasicLicence')->nullable();
			$table->integer('numberofAdvanceLicence')->nullable();
			$table->integer('numberofPremiumLicence')->nullable();
            $table->integer('numberofPremPlusLicence')->nullable();
			$table->integer('availableBasicLicence')->nullable();
			$table->integer('availableAdvanceLicence')->nullable();
			$table->integer('availablePremiumLicence')->nullable();
            $table->integer('availablePremPlusLicence')->nullable();
			$table->integer('addBasicLicence')->nullable();
			$table->integer('addAdvanceLicence')->nullable();
            $table->integer('addPremiumLicence')->nullable();
            $table->integer('addPrePlusLicence')->nullable();
			$table->string('website')->nullable();
			$table->string('trackPage')->nullable();
			$table->string('smsSender')->nullable();
			$table->string('smsProvider')->nullable();
			$table->string('providerUserName')->nullable();
			$table->string('providerPassword')->nullable();
			$table->string('timeZone')->nullable();
			$table->string('apiKey')->nullable();
			$table->string('mapKey')->nullable();
			$table->string('addressKey')->nullable();
			$table->string('notificationKey')->nullable();
			$table->string('gpsvtsApp')->nullable();
			$table->integer('backUpDays')->nullable();
			$table->string('dbType')->nullable();
			$table->string('zoho')->nullable();
			$table->string('auth')->nullable();
            $table->string('Details')->nullable();
            $table->text('addedReports')->nullable();
			$table->text('removedReports')->nullable();
			$table->text('reports')->nullable();
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->nullable();
			//$table->timestamps();

		});
	}

	public static function CreateAuditVehicle()
	{
		Schema::create('Audit_Vehicle', function($table)
		{
			   $table->increments('id'); //Autoincremented primary key
			   $table->string('fcode');   
               $table->string('vehicleId'); 
               $table->string('userName');  
               $table->string('status');
               $table->string('userIpAddress')->nullable();
			   $table->string('deviceId');          
               $table->string('deviceModel'); 
               $table->string('shortName')->nullable();   
               $table->string('regNo')->nullable();  
               $table->string('orgId')->nullable();   
               $table->string('vehicleType')->nullable();   
               $table->string('oprName')->nullable();   
               $table->string('mobileNo')->nullable();   
               $table->string('odoDistance')->nullable();   
               $table->string('gpsSimNo')->nullable();  
               $table->string('paymentType')->nullable();   
               $table->string('OWN')->nullable();   
               $table->string('expiredPeriod')->nullable();          
               $table->string('overSpeedLimit')->nullable();             
               $table->string('driverName')->nullable();  
               $table->string('driverMobile')->nullable(); 
               $table->string('email')->nullable();   
               $table->string('altShortName')->nullable();   
               $table->string('sendGeoFenceSMS')->nullable();   
               $table->string('morningTripStartTime')->nullable();    
               $table->string('eveningTripStartTime')->nullable();   
               $table->string('parkingAlert')->nullable();    
               $table->string('vehicleMake')->nullable();   
               $table->string('Licence')->nullable();   
               $table->string('Payment_Mode')->nullable();   
               $table->string('descriptionStatus')->nullable();  
               $table->string('vehicleExpiry')->nullable();   
               $table->string('onboardDate')->nullable();    
               $table->Integer('tankSize')->nullable();   
               $table->string('licenceissuedDate')->nullable();   
               $table->string('communicatingPortNo')->nullable();
               $table->string('oldVehicleId')->nullable();
               $table->string('oldDeviceId')->nullable();   
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			//$table->timestamps();

		});
	}
	public static function CreateAuditDealer()
	{
		Schema::create('Audit_Dealer', function($table)
		{
			   $table->increments('id'); //Autoincremented primary key
			   $table->string('fcode');   
               $table->string('dealerId'); 
               $table->string('userName');  
               $table->string('status');
               $table->string('userIpAddress')->nullable();
			   $table->string('email')->nullable();
			   $table->string('mobileNo')->nullable();  
               $table->string('zoho')->nullable(); 
               $table->string('mapKey')->nullable();   
               $table->string('addressKey')->nullable();  
               $table->string('notificationKey')->nullable();   
               $table->string('gpsvtsApp')->nullable();   
               $table->string('website')->nullable();   
               $table->string('smsSender')->nullable();   
               $table->string('smsProvider')->nullable();   
               $table->string('providerUserName')->nullable(); 
               $table->string('providerPassword')->nullable(); 
               $table->string('numofBasic')->nullable(); 
               $table->string('numofAdvance')->nullable();    
               $table->string('numofPremium')->nullable(); 
               $table->string('numofPremiumPlus')->nullable(); 
               $table->string('avlofBasic')->nullable(); 
               $table->string('avlofAdvance')->nullable(); 
               $table->string('avlofPremium')->nullable(); 
               $table->string('avlofPremiumPlus')->nullable();
               $table->integer('addBasicLicence')->nullable();
			   $table->integer('addAdvanceLicence')->nullable();
               $table->integer('addPremiumLicence')->nullable();
               $table->integer('addPrePlusLicence')->nullable();
               $table->string('LicenceissuedDate')->nullable();  
               $table->string('Details')->nullable();
               $table->text('addedReports')->nullable();
			   $table->text('removedReports')->nullable();
			   $table->text('reports')->nullable();
			   $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
              
			
			//$table->timestamps();

		});
	}
	public static function CreateAuditUser()
	{
		Schema::create('Audit_User', function($table)
		{
			   $table->increments('id'); //Autoincremented primary key
			   $table->string('fcode');   
               $table->string('userId'); 
               $table->string('userName');  
               $table->string('status');
               $table->string('userIpAddress')->nullable();
			   $table->string('email')->nullable();
			   $table->string('mobileNo')->nullable();  
               $table->string('password')->nullable();
               $table->string('cc_email')->nullable();
               $table->string('zoho')->nullable();
               $table->text('userType')->nullable();
               $table->string('companyName')->nullable();
               $table->string('addedGroups')->nullable();
               $table->string('removedGroups')->nullable();
               $table->string('groups')->nullable();
               $table->string('vehicles')->nullable();
			   $table->text('addedReports')->nullable();
			   $table->text('removedReports')->nullable();
			   $table->text('reports')->nullable();
			   $table->text('Notification')->nullable();
			   $table->text('removedNotification')->nullable();
			   $table->text('addedNotification')->nullable();
			   $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			
			//$table->timestamps();

		});
	}
	public static function CreateAuditGroup()
	{
		Schema::create('Audit_Group', function($table)
		{
			   $table->increments('id'); //Autoincremented primary key
			   $table->string('fcode');   
               $table->string('groupName'); 
               $table->string('userName');  
               $table->string('status');
               $table->string('userIpAddress')->nullable();
               $table->text('addedVehicles');
               $table->text('removedVehicles');
               $table->text('vehicleID');
               $table->text('vehicleName');
			   $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			  
			
			//$table->timestamps();

		});
	}

	public static function CreateAuditOrg()
	{
		Schema::create('Audit_Org', function($table)
		{
			    $table->increments('id'); //Autoincremented primary key
			    $table->string('fcode');   
                $table->string('organizationName'); 
                $table->string('userName');  
                $table->string('status');
                $table->string('userIpAddress')->nullable();
                $table->text('description')->nullable();
				$table->string('email')->nullable();
				$table->text('address')->nullable();
				$table->string('mobile')->nullable();
				$table->string('startTime')->nullable();
				$table->string('endTime')->nullable();
				$table->string('atc')->nullable();
				$table->string('etc')->nullable();
				$table->string('mtc')->nullable();
				$table->string('parkingAlert')->nullable();
				$table->string('idleAlert')->nullable();
				$table->string('parkDuration')->nullable();
				$table->string('idleDuration')->nullable();
				$table->string('overspeedalert')->nullable();
				$table->string('sendGeoFenceSMS')->nullable();
				$table->string('radius')->nullable();
				$table->string('smsSender')->nullable();
				$table->string('sosAlert')->nullable();
				$table->string('live')->nullable();
				$table->string('smsProvider')->nullable();
				$table->string('providerUserName')->nullable();
				$table->string('providerPassword')->nullable();
				$table->string('geofense')->nullable();
				$table->string('safemove')->nullable();
				$table->string('deleteHistoryEod')->nullable();
				$table->string('harshBreak')->nullable();
				$table->string('dailySummary')->nullable();
				$table->string('dailyDieselSummary')->nullable();
				$table->string('fuelLevelBelow')->nullable();
				$table->string('fuelLevelBelowValue')->nullable();
				$table->string('noDataDuration')->nullable();
				$table->string('SchoolGeoFence')->nullable();
				$table->string('morningEntryStartTime')->nullable();
				$table->string('morningEntryEndTime')->nullable();
				$table->string('eveningEntryStartTime')->nullable();
				$table->string('eveningEntryEndTime')->nullable();
				$table->string('morningExitStartTime')->nullable();
				$table->string('morningExitEndTime')->nullable();
				$table->string('eveningExitStartTime')->nullable();
				$table->string('eveningExitEndTime')->nullable();
				$table->string('isRfid')->nullable();
				$table->string('PickupStartTime')->nullable();
				$table->string('PickupEndTime')->nullable();
				$table->string('DropStartTime')->nullable();
				$table->string('DropEndTime')->nullable();
				$table->string('escalationsms')->nullable();
				$table->string('emailAlerts')->nullable();
				$table->string('smsAlerts')->nullable();
			    $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			  
			
			//$table->timestamps();

		});
	}

	public static function CreateCustomizeLandingPage()
	{
		Schema::create('Customize_Landing_Page', function($table)
		{
			   $table->increments('id'); //Autoincremented primary key
			   $table->string('fcode'); 
               $table->string('userName');  
               $table->string('userIpAddress')->nullable();
               $table->string('status')->nullable();
               $table->string('oldLandingPage')->nullable();
               $table->string('newLandingPage')->nullable();
			   $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			  
		});
	}
	public static function CreateLoginCustomisation()
	{
		Schema::create('Audit_LoginCustomisation', function($table)
		{
			   $table->increments('id'); //Autoincremented primary key
			   $table->string('username');
			   $table->string('fcode'); 
               $table->string('status')->nullable();
               $table->string('userIpAddress')->nullable();
               $table->string('Domain')->nullable();
               $table->string('Template')->nullable();
               $table->string('bcolor')->nullable();
               $table->string('bcolor1')->nullable();
               $table->string('fcolor')->nullable();
               $table->string('logo')->nullable();
               $table->string('background')->nullable();
			   $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			  
		});
	}


	public static function CreatelanguageSuggession()
	{
		Schema::create('LangSuggession', function($table)
		{
			   $table->increments('id'); //Autoincremented primary key
			   $table->string('userName')->nullable();
			   $table->string('fcode')->nullable();
               $table->text('wordsList')->nullable();
               $table->integer('flag')->nullable();
			   $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			  
		});
	}

	// public static function ChangeDB($db){
	// 		\Config::set('database.connections.mysql.database', $db);
	// 		DB::purge('mysql');
	// 	}
	public static function ChangeDB($db){
			$redis = Redis::connection();
			//$servername='104.248.153.17';
			$servername = $redis->get('Vamosys_DB');
			if($db!=Config::get('constant.VAMOSdb')){
			$servername  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $db);
			}
			\Config::set('database.connections.mysql.host', $servername);
			\Config::set('database.connections.mysql.database', $db);
			DB::purge('mysql');
     }


}

