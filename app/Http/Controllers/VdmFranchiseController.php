<?php
namespace App\Http\Controllers;

use Request;
use App;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

class VdmFranchiseController extends Controller {

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
        public function index() {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $username = Auth::user ()->username;

                log::info( 'User name  :' . $username);

                $redis = Redis::connection ();

                $fcodeArray = $redis->smembers('S_Franchises');

                $disableArr     = $redis->smembers('S_Franchises_Disable');
                $disable=null;
                foreach ($disableArr as $key => $value) {
                        $disable=array_add($disable,$value,$value);
                }

                $fnameArray=null;
                $prepaidArray=null;
                $franArray = array();
                foreach ( $fcodeArray as $key => $value ) {
                        $details = $redis->hget('H_Franchise',$value);
                        $details_json=json_decode($details,true);
                        $fnameArray=array_add($fnameArray, $details_json['fname'] , $value);
                        $isPrePaid1=ucfirst(isset( $details_json['prepaid'])? $details_json['prepaid']:'No');
                        if($isPrePaid1=="Yes")  {
                                $isPrePaid="Prepaid";
                        }
                        else {
                                $isPrePaid="Normal";
                        }
                        $enableExpiryexist=$redis->sismember('S_EnableVehicleExpiry',$value);
                        if($enableExpiryexist!=null)    {
                                $enableExpiry='yes';
                        }       else    {
                                $enableExpiry='no';
                        }
                        $franArray[$value] = ['fname' =>  $details_json['fname'], 'fcode' => $value, 'prepaid' => $isPrePaid,'enableExpiry'=>$enableExpiry];

                }
                ksort($franArray);
                //$count = count($redis->keys ( 'NoData:*' ));
                $apnKey = $redis->exists('Update:Apn');
        $timezoneKey = $redis->exists('Update:Timezone');
                return view ( 'vdm.franchise.index', array (
                                'fcodeArray' => $fcodeArray
                ) )->with('disable',$disable)->with('franArray', $franArray)->with('apnKey',$apnKey)->with('timezoneKey',$timezoneKey);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return Response
         */
        public function create() {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $redis = Redis::connection ();
                $smsP=VdmFranchiseController::smsP();
                $dbIp=VdmFranchiseController::dbIp();
                //$count = count($redis->keys ( 'NoData:*' ));
                $apnKey = $redis->exists('Update:Apn');
        $timezoneKey = $redis->exists('Update:Timezone');
                log::info( 'VdmFranchiseController  :' . count($dbIp));
                return view ( 'vdm.franchise.create' )->with('smsP',$smsP)->with('dbIp',$dbIp)->with('timeZoneC',VdmFranchiseController::timeZoneC())->with('backType',VdmFranchiseController::backTypeC())->with('apnKey',$apnKey)->with('timezoneKey',$timezoneKey);
        }

 public function licenceType(){
     if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcodeArray = $redis->smembers('S_Franchises');
    foreach ($fcodeArray as $key => $fcode) {
      $details = $redis->hget('H_Franchise',$fcode);
        $details_json=json_decode($details,true);
        $isPrePaid1=ucfirst(isset( $details_json['prepaid'])? $details_json['prepaid']:'No');
        if($isPrePaid1=="Yes")  {
          $vehicleList = $redis->smembers('S_Vehicles_'.$fcode);
          foreach ($vehicleList as $key => $vehicleId) {
             $vehicleRefData1 = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
             log::info('before refData');
             $refDataJson1=json_decode($vehicleRefData1,true);
             log::info($refDataJson1);
             $LicenceId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$vehicleId);
             $type=substr($LicenceId,0,2);
             if($type=='BC'){
               $Licence='Basic';
               log::info('Licence Id '.$LicenceId.' ------>'.$vehicleId.'---> Basic');
             }else if($type=='AV'){
               $Licence='Advance';
               log::info('Licence Id '.$LicenceId.' ------>'.$vehicleId.'---> Advance');
             }else if($type=='PM'){
               $Licence='Premium';
               log::info('Licence Id '.$LicenceId.' ------>'.$vehicleId.'---> Premium');
             }else if($type=='PL'){
               $Licence='PremiumPlus';
               log::info('Licence Id '.$LicenceId.' ------>'.$vehicleId.'---> PremiumPlus');
             }else{
               $Licence='';
               log::info('Licence Id '.$LicenceId.' ------>'.$vehicleId.'--->LicenceID NOT FOUND');
             }
             $refData =   array (
            'deviceId' => isset($refDataJson1['deviceId'])?$refDataJson1['deviceId']:'',
            'shortName' => isset($refDataJson1['shortName'])?$refDataJson1['shortName']:'nill',
            'deviceModel' => isset($refDataJson1['deviceModel'])?$refDataJson1['deviceModel']:'GT06N',
            'regNo' => isset($refDataJson1['regNo'])?$refDataJson1['regNo']:'XXXXX',
            'vehicleMake' => isset($refDataJson1['vehicleMake'])?$refDataJson1['vehicleMake']:' ',
            'vehicleType' =>  isset($refDataJson1['vehicleType'])?$refDataJson1['vehicleType']:'Bus',
            'oprName' => isset($refDataJson1['oprName'])?$refDataJson1['oprName']:'airtel',
            'mobileNo' =>isset($refDataJson1['mobileNo'])?$refDataJson1['mobileNo']:'0123456789',
            'overSpeedLimit' => isset($refDataJson1['overSpeedLimit'])?$refDataJson1['overSpeedLimit']:'60',
            'odoDistance' => isset($refDataJson1['odoDistance'])?$refDataJson1['odoDistance']:'0',
            'driverName' => isset($refDataJson1['driverName'])?$refDataJson1['driverName']:'XXX',
            'gpsSimNo' => isset($refDataJson1['gpsSimNo'])?$refDataJson1['gpsSimNo']:'0123456789',
            'email' => isset($refDataJson1['email'])?$refDataJson1['email']:'',
            'orgId' =>isset($refDataJson1['orgId'])?$refDataJson1['orgId']:'',
            'altShortName'=>isset($refDataJson1['altShortName'])?$refDataJson1['altShortName']:'nill',
            'fuel'=>isset($refDataJson1['fuel'])?$refDataJson1['fuel']:'no',
            'fuelType'=>isset($refDataJson1['fuelType'])?$refDataJson1['fuelType']:' ',
            'isRfid'=>isset($refDataJson1['isRfid'])?$refDataJson1['isRfid']:'no',
            'rfidType'=>isset($refDataJson1['rfidType'])?$refDataJson1['rfidType']:'no',
            'Licence'=>$Licence,
            'OWN'=>isset($refDataJson1['OWN'])?$refDataJson1['OWN']:'OWN',
            'Payment_Mode'=>isset($refDataJson1['Payment_Mode'])?$refDataJson1['Payment_Mode']:'',
            'descriptionStatus'=>isset($refDataJson1['descriptionStatus'])?$refDataJson1['descriptionStatus']:'',
            'ipAddress'=>isset($refDataJson1['ipAddress'])?$refDataJson1['ipAddress']:'',
            'portNo'=>isset($refDataJson1['portNo'])?$refDataJson1['portNo']:'0',
            'analog1'=>isset($refDataJson1['analog1'])?$refDataJson1['analog1']:'',
            'analog2'=>isset($refDataJson1['analog2'])?$refDataJson1['analog2']:'',
            'digital1'=>isset($refDataJson1['digital1'])?$refDataJson1['digital1']:'',
            'digital2'=>isset($refDataJson1['digital2'])?$refDataJson1['digital2']:'',
            'serial1'=>isset($refDataJson1['serial1'])?$refDataJson1['serial1']:'',
            'serial2'=>isset($refDataJson1['serial2'])?$refDataJson1['serial2']:'',
            'digitalout'=>isset($refDataJson1['digitalout'])?$refDataJson1['digitalout']:'',
            'mintemp'=>isset($refDataJson1['mintemp'])?$refDataJson1['mintemp']:'',
            'maxtemp'=>isset($refDataJson1['maxtemp'])?$refDataJson1['maxtemp']:'',
            'routeName'=>isset($refDataJson1['routeName'])?$refDataJson1['routeName']:'',
            'sendGeoFenceSMS' => isset($refDataJson1['sendGeoFenceSMS'])?$refDataJson1['sendGeoFenceSMS']:'no',
            'morningTripStartTime' => isset($refDataJson1['morningTripStartTime'])?$refDataJson1['morningTripStartTime']:'',
            'eveningTripStartTime' => isset($refDataJson1['eveningTripStartTime'])?$refDataJson1['eveningTripStartTime']:'',
            'parkingAlert' => isset($refDataJson1['parkingAlert'])?$refDataJson1['parkingAlert']:'no',
            'paymentType'=>isset($refDataJson1['paymentType'])?$refDataJson1['paymentType']:' ',
            'expiredPeriod'=>isset($refDataJson1['expiredPeriod'])?$refDataJson1['expiredPeriod']:' ',
            'vehicleExpiry' =>isset($refDataJson1['vehicleExpiry'])?$refDataJson1['vehicleExpiry']:'null',
            'onboardDate' =>isset($refDataJson1['onboardDate'])?$refDataJson1['onboardDate']:'null',
            'safetyParking'=>isset($refDataJson1['safetyParking'])?$refDataJson1['safetyParking']:'no',
            'tankSize'=>isset($refDataJson1['tankSize'])?$refDataJson1['tankSize']:'',
            'licenceissuedDate'=>isset($refDataJson1['licenceissuedDate'])?$refDataJson1['licenceissuedDate']:'',
            'VolIg'=>isset($refDataJson1['VolIg'])?$refDataJson1['VolIg']:'',
            'batteryvolt'=>isset($refDataJson1['batteryvolt'])?$refDataJson1['batteryvolt']:'',
            'ac'=>isset($refDataJson1['ac'])?$refDataJson1['ac']:'',
            'acVolt'=>isset($refDataJson1['acVolt'])?$refDataJson1['acVolt']:'',
            'distManipulationPer'=>isset($refDataJson1['distManipulationPer'])?$refDataJson1['distManipulationPer']:'',
            'assetTracker'=>isset($refDataJson1['assetTracker'])?$refDataJson1['assetTracker']:'no',
            'communicatingPortNo'=>isset($refDataJson1['communicatingPortNo'])?$refDataJson1['communicatingPortNo']:'',
            'onboardDate'  =>isset($refDataJson1['onboardDate'])?$refDataJson1['onboardDate']:'',
            'driverName'    =>isset($refDataJson1['driverName'])?$refDataJson1['driverName']:'',
            'driverMobile' =>isset($refDataJson1['driverMobile'])?$refDataJson1['driverMobile']:'',
            'date' =>isset($refDataJson1['date'])?$refDataJson1['date']:'',
            );
             $refDataJson = json_encode ( $refData );
             $redis->hmset ( 'H_RefData_' .$fcode,$vehicleId,$refDataJson);

          }
        }
    }
    Session::flash ( 'message','Franchise LicenceId Type successfully modified !');
    return Redirect::to ( 'vdmFranchises' );

  }


public static function dbIp()
{
        $dbIp   = array();
        $redis  = Redis::connection ();
        $ipList = $redis->smembers('S_DB_Details');
        foreach ($ipList as  $key => $ip) {
        $dbIp = array_add($dbIp, $key, $ip);
        }
        return $dbIp;
}


public static function backTypeC()
{
        $backType=array();
        $backType=array_add($backType, 'sqlite','Sqlite');
        $backType=array_add($backType, 'mysql','Mysql');
        return $backType;
}

public static function smsP()
{
        $redis = Redis::connection ();
        $smsProvider = $redis->lrange('L_SMS_Provider', 0, -1);
        $smsP=array();
        foreach ($smsProvider as $sms) {
        $smsP=array_add($smsP, $sms, $sms);
        }
        return $smsP;
}

public static function getProtocal($Ltype){
        log::info('licenece type ----->'.$Ltype);
        $redis = Redis::connection ();
        if($Ltype == 'Basic'){
                $protocal = $redis->lrange('L_Protocal', 0, -1);
        }else if($Ltype == 'Advance')
        {
                $protocal = $redis->lrange('L_Protocal', 0, -1);
        }else if($Ltype == 'Premium'){
                $protocal = $redis->lrange('L_Protocal', 0, -1);
        }else if($Ltype == 'PremiumPlus'){
                $protocal = $redis->lrange('L_Protocal', 0, -1);
        }else{
                $protocal = $redis->lrange('L_Protocal', 0, -1);
        }
        $getProtocal=array();
        foreach ($protocal as $pro) {
                $value  = explode(":",$pro);
                $check=isset($getProtocal[$value[0]]);
                $len=strlen($check);
            if($len==0)
            {
                        $getProtocal=array_add($getProtocal, $value[0], $value[0].' ('.$value[1].') ');
            }
            else
            {
                    $getProtocal=array_add($getProtocal, $value[0].'1', $value[0].' ('.$value[1].') ');
                        }
    }
        return $getProtocal;
}

public static function timeZoneC()
{
                $timeZoneC=array();
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+12','Etc/GMT+12');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+11','Etc/GMT+11');
                $timeZoneC=array_add($timeZoneC, 'MIT','MIT');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Apia','Pacific/Apia');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Midway','Pacific/Midway');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Niue','Pacific/Niue');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Pago_Pago','Pacific/Pago_Pago');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Samoa','Pacific/Samoa');
                $timeZoneC=array_add($timeZoneC, 'US/Samoa','US/Samoa');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Apia','Pacific/Apia');
                $timeZoneC=array_add($timeZoneC, 'America/Adak','America/Adak');
                $timeZoneC=array_add($timeZoneC, 'America/Atka','America/Atka');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+10','Etc/GMT+10');
                $timeZoneC=array_add($timeZoneC, 'HST','HST');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Fakaofo','Pacific/Fakaofo');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Honolulu','Pacific/Honolulu');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Johnston','Pacific/Johnston');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Rarotonga','Pacific/Rarotonga');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Tahiti','Pacific/Tahiti');
                $timeZoneC=array_add($timeZoneC, 'SystemV/HST10','SystemV/HST10');
                $timeZoneC=array_add($timeZoneC, 'US/Aleutian','US/Aleutian');
                $timeZoneC=array_add($timeZoneC, 'US/Hawaii','US/Hawaii');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Marquesas','Pacific/Marquesas');
                $timeZoneC=array_add($timeZoneC, 'AST','AST');
                $timeZoneC=array_add($timeZoneC, 'America/Anchorage','America/Anchorage');
                $timeZoneC=array_add($timeZoneC, 'America/Juneau','America/Juneau');
                $timeZoneC=array_add($timeZoneC, 'America/Nome','America/Nome');
                $timeZoneC=array_add($timeZoneC, 'America/Yakutat','America/Yakutat');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+9','Etc/GMT+9');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Gambier','Pacific/Gambier');
                $timeZoneC=array_add($timeZoneC, 'SystemV/YST9','SystemV/YST9');
                $timeZoneC=array_add($timeZoneC, 'SystemV/YST9YDT','SystemV/YST9YDT');
                $timeZoneC=array_add($timeZoneC, 'US/Alaska','US/Alaska');
                $timeZoneC=array_add($timeZoneC, 'America/Dawson','America/Dawson');
                $timeZoneC=array_add($timeZoneC, 'America/Ensenada','America/Ensenada');
                $timeZoneC=array_add($timeZoneC, 'America/Los_Angeles','America/Los_Angeles');
                $timeZoneC=array_add($timeZoneC, 'America/Tijuana','America/Tijuana');
                $timeZoneC=array_add($timeZoneC, 'America/Vancouver','America/Vancouver');
                $timeZoneC=array_add($timeZoneC, 'America/Whitehorse','America/Whitehorse');
                $timeZoneC=array_add($timeZoneC, 'Canada/Pacific','Canada/Pacific');
                $timeZoneC=array_add($timeZoneC, 'Canada/Yukon','Canada/Yukon');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+8','Etc/GMT+8');
                $timeZoneC=array_add($timeZoneC, 'Mexico/BajaNorte','Mexico/BajaNorte');
                $timeZoneC=array_add($timeZoneC, 'PST','PST');
                $timeZoneC=array_add($timeZoneC, 'PST8PDT','PST8PDT');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Pitcairn','Pacific/Pitcairn');
                $timeZoneC=array_add($timeZoneC, 'SystemV/PST8','SystemV/PST8');
                $timeZoneC=array_add($timeZoneC, 'SystemV/PST8PDT','SystemV/PST8PDT');
                $timeZoneC=array_add($timeZoneC, 'US/Pacific','US/Pacific');
                $timeZoneC=array_add($timeZoneC, 'US/Pacific-New','US/Pacific-New');
                $timeZoneC=array_add($timeZoneC, 'America/Boise','America/Boise');
                $timeZoneC=array_add($timeZoneC, 'America/Cambridge_Bay','America/Cambridge_Bay');
                $timeZoneC=array_add($timeZoneC, 'America/Chihuahua','America/Chihuahua');
                $timeZoneC=array_add($timeZoneC, 'America/Dawson_Creek','America/Dawson_Creek');
                $timeZoneC=array_add($timeZoneC, 'America/Denver','America/Denver');
                $timeZoneC=array_add($timeZoneC, 'America/Edmonton','America/Edmonton');
                $timeZoneC=array_add($timeZoneC, 'America/Hermosillo','America/Hermosillo');
                $timeZoneC=array_add($timeZoneC, 'America/Inuvik','America/Inuvik');
                $timeZoneC=array_add($timeZoneC, 'America/Mazatlan','America/Mazatlan');
                $timeZoneC=array_add($timeZoneC, 'America/Phoenix','America/Phoenix');
                $timeZoneC=array_add($timeZoneC, 'America/Shiprock','America/Shiprock');
                $timeZoneC=array_add($timeZoneC, 'America/Yellowknife','America/Yellowknife');
                $timeZoneC=array_add($timeZoneC, 'Canada/Mountain','Canada/Mountain');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+7','Etc/GMT+7');
                $timeZoneC=array_add($timeZoneC, 'MST','MST');
                $timeZoneC=array_add($timeZoneC, 'MST7MDT','MST7MDT');
                $timeZoneC=array_add($timeZoneC, 'Mexico/BajaSur','Mexico/BajaSur');
                $timeZoneC=array_add($timeZoneC, 'Navajo','Navajo');
                $timeZoneC=array_add($timeZoneC, 'PNT','PNT');
                $timeZoneC=array_add($timeZoneC, 'SystemV/MST7','SystemV/MST7');
                $timeZoneC=array_add($timeZoneC, 'SystemV/MST7MDT','SystemV/MST7MDT');
                $timeZoneC=array_add($timeZoneC, 'US/Arizona','US/Arizona');
                $timeZoneC=array_add($timeZoneC, 'US/Mountain','US/Mountain');
                $timeZoneC=array_add($timeZoneC, 'America/Belize','America/Belize');
                $timeZoneC=array_add($timeZoneC, 'America/Cancun','America/Cancun');
                $timeZoneC=array_add($timeZoneC, 'America/Chicago','America/Chicago');
                $timeZoneC=array_add($timeZoneC, 'America/Costa_Rica','America/Costa_Rica');
                $timeZoneC=array_add($timeZoneC, 'America/El_Salvador','America/El_Salvador');
                $timeZoneC=array_add($timeZoneC, 'America/Guatemala','America/Guatemala');
                $timeZoneC=array_add($timeZoneC, 'America/Managua','America/Managua');
                $timeZoneC=array_add($timeZoneC, 'America/Menominee','America/Menominee');
                $timeZoneC=array_add($timeZoneC, 'America/Merida','America/Merida');
                $timeZoneC=array_add($timeZoneC, 'America/Mexico_City','America/Mexico_City');
                $timeZoneC=array_add($timeZoneC, 'America/Monterrey','America/Monterrey');
                $timeZoneC=array_add($timeZoneC, 'America/North_Dakota/Center','America/North_Dakota/Center');
                $timeZoneC=array_add($timeZoneC, 'America/Rainy_River','America/Rainy_River');
                $timeZoneC=array_add($timeZoneC, 'America/Rankin_Inlet','America/Rankin_Inlet');
                $timeZoneC=array_add($timeZoneC, 'America/Regina','America/Regina');
                $timeZoneC=array_add($timeZoneC, 'America/Swift_Current','America/Swift_Current');
                $timeZoneC=array_add($timeZoneC, 'America/Tegucigalpa','America/Tegucigalpa');
                $timeZoneC=array_add($timeZoneC, 'America/Winnipeg','America/Winnipeg');
                $timeZoneC=array_add($timeZoneC, 'CST','CST');
                $timeZoneC=array_add($timeZoneC, 'CST6CDT','CST6CDT');
                $timeZoneC=array_add($timeZoneC, 'Canada/Central','Canada/Central');
                $timeZoneC=array_add($timeZoneC, 'Canada/East-Saskatchewan','Canada/East-Saskatchewan');
                $timeZoneC=array_add($timeZoneC, 'Canada/Saskatchewan','Canada/Saskatchewan');
                $timeZoneC=array_add($timeZoneC, 'Chile/EasterIsland','Chile/EasterIsland');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+6','Etc/GMT+6');
                $timeZoneC=array_add($timeZoneC, 'Mexico/General','Mexico/General');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Easter','Pacific/Easter');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Galapagos','Pacific/Galapagos');
                $timeZoneC=array_add($timeZoneC, 'SystemV/CST6','SystemV/CST6');
                $timeZoneC=array_add($timeZoneC, 'SystemV/CST6CDT','SystemV/CST6CDT');
                $timeZoneC=array_add($timeZoneC, 'US/Central','US/Central');
                $timeZoneC=array_add($timeZoneC, 'America/Bogota','America/Bogota');
                $timeZoneC=array_add($timeZoneC, 'America/Cayman','America/Cayman');
                $timeZoneC=array_add($timeZoneC, 'America/Detroit','America/Detroit');
                $timeZoneC=array_add($timeZoneC, 'America/Eirunepe','America/Eirunepe');
                $timeZoneC=array_add($timeZoneC, 'America/Fort_Wayne','America/Fort_Wayne');
                $timeZoneC=array_add($timeZoneC, 'America/Grand_Turk','America/Grand_Turk');
                $timeZoneC=array_add($timeZoneC, 'America/Guayaquil','America/Guayaquil');
                $timeZoneC=array_add($timeZoneC, 'America/Havana','America/Havana');
                $timeZoneC=array_add($timeZoneC, 'America/Indiana/Indianapolis','America/Indiana/Indianapolis');
                $timeZoneC=array_add($timeZoneC, 'America/Indiana/Knox','America/Indiana/Knox');
                $timeZoneC=array_add($timeZoneC, 'America/Indiana/Marengo','America/Indiana/Marengo');
                $timeZoneC=array_add($timeZoneC, 'America/Indiana/Vevay','America/Indiana/Vevay');
                $timeZoneC=array_add($timeZoneC, 'America/Indianapolis','America/Indianapolis');
                $timeZoneC=array_add($timeZoneC, 'America/Iqaluit','America/Iqaluit');
                $timeZoneC=array_add($timeZoneC, 'America/Jamaica','America/Jamaica');
                $timeZoneC=array_add($timeZoneC, 'America/Kentucky/Louisville','America/Kentucky/Louisville');
                $timeZoneC=array_add($timeZoneC, 'America/Kentucky/Monticello','America/Kentucky/Monticello');
                $timeZoneC=array_add($timeZoneC, 'America/Knox_IN','America/Knox_IN');
                $timeZoneC=array_add($timeZoneC, 'America/Lima','America/Lima');
                $timeZoneC=array_add($timeZoneC, 'America/Louisville','America/Louisville');
                $timeZoneC=array_add($timeZoneC, 'America/Montreal','America/Montreal');
                $timeZoneC=array_add($timeZoneC, 'America/Nassau','America/Nassau');
                $timeZoneC=array_add($timeZoneC, 'America/New_York','America/New_York');
                $timeZoneC=array_add($timeZoneC, 'America/Nipigon','America/Nipigon');
                $timeZoneC=array_add($timeZoneC, 'America/Panama','America/Panama');
                $timeZoneC=array_add($timeZoneC, 'America/Pangnirtung','America/Pangnirtung');
                $timeZoneC=array_add($timeZoneC, 'America/Port-au-Prince','America/Port-au-Prince');
                $timeZoneC=array_add($timeZoneC, 'America/Porto_Acre','America/Porto_Acre');
                $timeZoneC=array_add($timeZoneC, 'America/Rio_Branco','America/Rio_Branco');
                $timeZoneC=array_add($timeZoneC, 'America/Thunder_Bay','America/Thunder_Bay');
                $timeZoneC=array_add($timeZoneC, 'America/Toronto','America/Toronto');
                $timeZoneC=array_add($timeZoneC, 'Brazil/Acre','Brazil/Acre');
                $timeZoneC=array_add($timeZoneC, 'Canada/Eastern','Canada/Eastern');
                $timeZoneC=array_add($timeZoneC, 'Cuba','Cuba');
                $timeZoneC=array_add($timeZoneC, 'EST','EST');
                $timeZoneC=array_add($timeZoneC, 'EST5EDT','EST5EDT');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+5','Etc/GMT+5');
                $timeZoneC=array_add($timeZoneC, 'IET','IET');
                $timeZoneC=array_add($timeZoneC, 'Jamaica','Jamaica');
                $timeZoneC=array_add($timeZoneC, 'SystemV/EST5','SystemV/EST5');
                $timeZoneC=array_add($timeZoneC, 'SystemV/EST5EDT','SystemV/EST5EDT');
                $timeZoneC=array_add($timeZoneC, 'US/East-Indiana','US/East-Indiana');
                $timeZoneC=array_add($timeZoneC, 'US/Eastern','US/Eastern');
                $timeZoneC=array_add($timeZoneC, 'US/Indiana-Starke','US/Indiana-Starke');
                $timeZoneC=array_add($timeZoneC, 'US/Michigan','US/Michigan');
                $timeZoneC=array_add($timeZoneC, 'America/Anguilla','America/Anguilla');
                $timeZoneC=array_add($timeZoneC, 'America/Antigua','America/Antigua');
                $timeZoneC=array_add($timeZoneC, 'America/Aruba','America/Aruba');
                $timeZoneC=array_add($timeZoneC, 'America/Asuncion','America/Asuncion');
                $timeZoneC=array_add($timeZoneC, 'America/Barbados','America/Barbados');
                $timeZoneC=array_add($timeZoneC, 'America/Boa_Vista','America/Boa_Vista');
                $timeZoneC=array_add($timeZoneC, 'America/Campo_Grande','America/Campo_Grande');
                $timeZoneC=array_add($timeZoneC, 'America/Caracas','America/Caracas');
                $timeZoneC=array_add($timeZoneC, 'America/Cuiaba','America/Cuiaba');
                $timeZoneC=array_add($timeZoneC, 'America/Curacao','America/Curacao');
                $timeZoneC=array_add($timeZoneC, 'America/Dominica','America/Dominica');
                $timeZoneC=array_add($timeZoneC, 'America/Glace_Bay','America/Glace_Bay');
                $timeZoneC=array_add($timeZoneC, 'America/Goose_Bay','America/Goose_Bay');
                $timeZoneC=array_add($timeZoneC, 'America/Grenada','America/Grenada');
                $timeZoneC=array_add($timeZoneC, 'America/Guadeloupe','America/Guadeloupe');
                $timeZoneC=array_add($timeZoneC, 'America/Guyana','America/Guyana');
                $timeZoneC=array_add($timeZoneC, 'America/Halifax','America/Halifax');
                $timeZoneC=array_add($timeZoneC, 'America/La_Paz','America/La_Paz');
                $timeZoneC=array_add($timeZoneC, 'America/Manaus','America/Manaus');
                $timeZoneC=array_add($timeZoneC, 'America/Martinique','America/Martinique');
                $timeZoneC=array_add($timeZoneC, 'America/Montserrat','America/Montserrat');
                $timeZoneC=array_add($timeZoneC, 'America/Port_of_Spain','America/Port_of_Spain');
                $timeZoneC=array_add($timeZoneC, 'America/Porto_Velho','America/Porto_Velho');
                $timeZoneC=array_add($timeZoneC, 'America/Puerto_Rico','America/Puerto_Rico');
                $timeZoneC=array_add($timeZoneC, 'America/Santiago','America/Santiago');
                $timeZoneC=array_add($timeZoneC, 'America/Santo_Domingo','America/Santo_Domingo');
                $timeZoneC=array_add($timeZoneC, 'America/St_Kitts','America/St_Kitts');
                $timeZoneC=array_add($timeZoneC, 'America/St_Lucia','America/St_Lucia');
                $timeZoneC=array_add($timeZoneC, 'America/St_Thomas','America/St_Thomas');
                $timeZoneC=array_add($timeZoneC, 'America/St_Vincent','America/St_Vincent');
                $timeZoneC=array_add($timeZoneC, 'America/Thule','America/Thule');
                $timeZoneC=array_add($timeZoneC, 'America/Tortola','America/Tortola');
                $timeZoneC=array_add($timeZoneC, 'America/Virgin','America/Virgin');
                $timeZoneC=array_add($timeZoneC, 'Antarctica/Palmer','Antarctica/Palmer');
                $timeZoneC=array_add($timeZoneC, 'Atlantic/Bermuda','Atlantic/Bermuda');
                $timeZoneC=array_add($timeZoneC, 'Atlantic/Stanley','Atlantic/Stanley');
                $timeZoneC=array_add($timeZoneC, 'Brazil/West','Brazil/West');
                $timeZoneC=array_add($timeZoneC, 'Canada/Atlantic','Canada/Atlantic');
                $timeZoneC=array_add($timeZoneC, 'Chile/Continental','Chile/Continental');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+4','Etc/GMT+4');
                $timeZoneC=array_add($timeZoneC, 'PRT','PRT');
                $timeZoneC=array_add($timeZoneC, 'SystemV/AST4','SystemV/AST4');
                $timeZoneC=array_add($timeZoneC, 'SystemV/AST4ADT','SystemV/AST4ADT');
                $timeZoneC=array_add($timeZoneC, 'America/St_Johns','America/St_Johns');
                $timeZoneC=array_add($timeZoneC, 'CNT','CNT');
                $timeZoneC=array_add($timeZoneC, 'Canada/Newfoundland','Canada/Newfoundland');
                $timeZoneC=array_add($timeZoneC, 'AGT','AGT');
                $timeZoneC=array_add($timeZoneC, 'America/Araguaina','America/Araguaina');
                $timeZoneC=array_add($timeZoneC, 'America/Bahia','America/Bahia');
                $timeZoneC=array_add($timeZoneC, 'America/Belem','America/Belem');
                $timeZoneC=array_add($timeZoneC, 'America/Buenos_Aires','America/Buenos_Aires');
                $timeZoneC=array_add($timeZoneC, 'America/Catamarca','America/Catamarca');
                $timeZoneC=array_add($timeZoneC, 'America/Cayenne','America/Cayenne');
                $timeZoneC=array_add($timeZoneC, 'America/Cordoba','America/Cordoba');
                $timeZoneC=array_add($timeZoneC, 'America/Fortaleza','America/Fortaleza');
                $timeZoneC=array_add($timeZoneC, 'America/Godthab','America/Godthab');
                $timeZoneC=array_add($timeZoneC, 'America/Jujuy','America/Jujuy');
                $timeZoneC=array_add($timeZoneC, 'America/Maceio','America/Maceio');
                $timeZoneC=array_add($timeZoneC, 'America/Mendoza','America/Mendoza');
                $timeZoneC=array_add($timeZoneC, 'America/Miquelon','America/Miquelon');
                $timeZoneC=array_add($timeZoneC, 'America/Montevideo','America/Montevideo');
                $timeZoneC=array_add($timeZoneC, 'America/Paramaribo','America/Paramaribo');
                $timeZoneC=array_add($timeZoneC, 'America/Recife','America/Recife');
                $timeZoneC=array_add($timeZoneC, 'America/Rosario','America/Rosario');
                $timeZoneC=array_add($timeZoneC, 'America/Sao_Paulo','America/Sao_Paulo');
                $timeZoneC=array_add($timeZoneC, 'Antarctica/Rothera','Antarctica/Rothera');
                $timeZoneC=array_add($timeZoneC, 'BET','BET');
                $timeZoneC=array_add($timeZoneC, 'Brazil/East','Brazil/East');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+3','Etc/GMT+3');
                $timeZoneC=array_add($timeZoneC, 'America/Noronha','America/Noronha');
                $timeZoneC=array_add($timeZoneC, 'Atlantic/South_Georgia','Atlantic/South_Georgia');
                $timeZoneC=array_add($timeZoneC, 'Brazil/DeNoronha','Brazil/DeNoronha');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+2','Etc/GMT+2');
                $timeZoneC=array_add($timeZoneC, 'America/Scoresbysund','America/Scoresbysund');
                $timeZoneC=array_add($timeZoneC, 'Atlantic/Azores','Atlantic/Azores');
                $timeZoneC=array_add($timeZoneC, 'Atlantic/Cape_Verde','Atlantic/Cape_Verde');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+1','Etc/GMT+1');
                $timeZoneC=array_add($timeZoneC, 'Africa/Abidjan','Africa/Abidjan');
                $timeZoneC=array_add($timeZoneC, 'Africa/Accra','Africa/Accra');
                $timeZoneC=array_add($timeZoneC, 'Africa/Bamako','Africa/Bamako');
                $timeZoneC=array_add($timeZoneC, 'Africa/Banjul','Africa/Banjul');
                $timeZoneC=array_add($timeZoneC, 'Africa/Bissau','Africa/Bissau');
                $timeZoneC=array_add($timeZoneC, 'Africa/Casablanca','Africa/Casablanca');
                $timeZoneC=array_add($timeZoneC, 'Africa/Conakry','Africa/Conakry');
                $timeZoneC=array_add($timeZoneC, 'Africa/Dakar','Africa/Dakar');
                $timeZoneC=array_add($timeZoneC, 'Africa/El_Aaiun','Africa/El_Aaiun');
                $timeZoneC=array_add($timeZoneC, 'Africa/Freetown','Africa/Freetown');
                $timeZoneC=array_add($timeZoneC, 'Africa/Lome','Africa/Lome');
                $timeZoneC=array_add($timeZoneC, 'Africa/Monrovia','Africa/Monrovia');
                $timeZoneC=array_add($timeZoneC, 'Africa/Nouakchott','Africa/Nouakchott');
                $timeZoneC=array_add($timeZoneC, 'Africa/Ouagadougou','Africa/Ouagadougou');


                $timeZoneC=array_add($timeZoneC, 'Africa/Sao_Tome','Africa/Sao_Tome');
                $timeZoneC=array_add($timeZoneC, 'Africa/Timbuktu','Africa/Timbuktu');
                $timeZoneC=array_add($timeZoneC, 'America/Danmarkshavn','America/Danmarkshavn');
                $timeZoneC=array_add($timeZoneC, 'Atlantic/Canary','Atlantic/Canary');
                $timeZoneC=array_add($timeZoneC, 'Atlantic/Faeroe','Atlantic/Faeroe');
                $timeZoneC=array_add($timeZoneC, 'Atlantic/Madeira','Atlantic/Madeira');
                $timeZoneC=array_add($timeZoneC, 'Atlantic/Reykjavik','Atlantic/Reykjavik');
                $timeZoneC=array_add($timeZoneC, 'Atlantic/St_Helena','Atlantic/St_Helena');
                $timeZoneC=array_add($timeZoneC, 'Eire','Eire');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT','Etc/GMT');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT+0','Etc/GMT+0');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-0','Etc/GMT-0');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT0','Etc/GMT0');
                $timeZoneC=array_add($timeZoneC, 'Etc/Greenwich','Etc/Greenwich');
                $timeZoneC=array_add($timeZoneC, 'Etc/UCT','Etc/UCT');
                $timeZoneC=array_add($timeZoneC, 'Etc/UTC','Etc/UTC');
                $timeZoneC=array_add($timeZoneC, 'Etc/Universal','Etc/Universal');
                $timeZoneC=array_add($timeZoneC, 'Etc/Zulu','Etc/Zulu');
                $timeZoneC=array_add($timeZoneC, 'Europe/Belfast','Europe/Belfast');
                $timeZoneC=array_add($timeZoneC, 'Europe/Dublin','Europe/Dublin');
                $timeZoneC=array_add($timeZoneC, 'Europe/Lisbon','Europe/Lisbon');
                $timeZoneC=array_add($timeZoneC, 'Europe/London','Europe/London');
                $timeZoneC=array_add($timeZoneC, 'GB','GB');
                $timeZoneC=array_add($timeZoneC, 'GB-Eire','GB-Eire');
                $timeZoneC=array_add($timeZoneC, 'GMT','GMT');
                $timeZoneC=array_add($timeZoneC, 'GMT0','GMT0');
                $timeZoneC=array_add($timeZoneC, 'Greenwich','Greenwich');
                $timeZoneC=array_add($timeZoneC, 'Iceland','Iceland');
                $timeZoneC=array_add($timeZoneC, 'Portugal','Portugal');
                $timeZoneC=array_add($timeZoneC, 'UCT','UCT');
                $timeZoneC=array_add($timeZoneC, 'UTC','UTC');
                $timeZoneC=array_add($timeZoneC, 'Universal','Universal');
                $timeZoneC=array_add($timeZoneC, 'WET','WET');
                $timeZoneC=array_add($timeZoneC, 'Zulu','Zulu');
                $timeZoneC=array_add($timeZoneC, 'Africa/Algiers','Africa/Algiers');
                $timeZoneC=array_add($timeZoneC, 'Africa/Bangui','Africa/Bangui');
                $timeZoneC=array_add($timeZoneC, 'Africa/Brazzaville','Africa/Brazzaville');
                $timeZoneC=array_add($timeZoneC, 'Africa/Ceuta','Africa/Ceuta');
                $timeZoneC=array_add($timeZoneC, 'Africa/Douala','Africa/Douala');
                $timeZoneC=array_add($timeZoneC, 'Africa/Kinshasa','Africa/Kinshasa');
                $timeZoneC=array_add($timeZoneC, 'Africa/Lagos','Africa/Lagos');
                $timeZoneC=array_add($timeZoneC, 'Africa/Libreville','Africa/Libreville');
                $timeZoneC=array_add($timeZoneC, 'Africa/Luanda','Africa/Luanda');
                $timeZoneC=array_add($timeZoneC, 'Africa/Malabo','Africa/Malabo');
                $timeZoneC=array_add($timeZoneC, 'Africa/Ndjamena','Africa/Ndjamena');
                $timeZoneC=array_add($timeZoneC, 'Africa/Niamey','Africa/Niamey');
                $timeZoneC=array_add($timeZoneC, 'Africa/Porto-Novo','Africa/Porto-Novo');
                $timeZoneC=array_add($timeZoneC, 'Africa/Tunis','Africa/Tunis');
                $timeZoneC=array_add($timeZoneC, 'Africa/Windhoek','Africa/Windhoek');
                $timeZoneC=array_add($timeZoneC, 'Arctic/Longyearbyen','Arctic/Longyearbyen');
                $timeZoneC=array_add($timeZoneC, 'Atlantic/Jan_Mayen','Atlantic/Jan_Mayen');
                $timeZoneC=array_add($timeZoneC, 'CET','CET');
                $timeZoneC=array_add($timeZoneC, 'ECT','ECT');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-1','Etc/GMT-1');
                $timeZoneC=array_add($timeZoneC, 'Europe/Amsterdam','Europe/Amsterdam');
                $timeZoneC=array_add($timeZoneC, 'Europe/Andorra','Europe/Andorra');
                $timeZoneC=array_add($timeZoneC, 'Europe/Belgrade','Europe/Belgrade');
                $timeZoneC=array_add($timeZoneC, 'Europe/Berlin','Europe/Berlin');
                $timeZoneC=array_add($timeZoneC, 'Europe/Bratislava','Europe/Bratislava');
                $timeZoneC=array_add($timeZoneC, 'Europe/Brussels','Europe/Brussels');
                $timeZoneC=array_add($timeZoneC, 'Europe/Budapest','Europe/Budapest');
                $timeZoneC=array_add($timeZoneC, 'Europe/Copenhagen','Europe/Copenhagen');
                $timeZoneC=array_add($timeZoneC, 'Europe/Gibraltar','Europe/Gibraltar');
                $timeZoneC=array_add($timeZoneC, 'Europe/Ljubljana','Europe/Ljubljana');
                $timeZoneC=array_add($timeZoneC, 'Europe/Luxembourg','Europe/Luxembourg');
                $timeZoneC=array_add($timeZoneC, 'Europe/Madrid','Europe/Madrid');
                $timeZoneC=array_add($timeZoneC, 'Europe/Malta','Europe/Malta');
                $timeZoneC=array_add($timeZoneC, 'Europe/Monaco','Europe/Monaco');
                $timeZoneC=array_add($timeZoneC, 'Europe/Oslo','Europe/Oslo');
                $timeZoneC=array_add($timeZoneC, 'Europe/Paris','Europe/Paris');
                $timeZoneC=array_add($timeZoneC, 'Europe/Prague','Europe/Prague');
                $timeZoneC=array_add($timeZoneC, 'Europe/Rome','Europe/Rome');
                $timeZoneC=array_add($timeZoneC, 'Europe/San_Marino','Europe/San_Marino');
                $timeZoneC=array_add($timeZoneC, 'Europe/Sarajevo','Europe/Sarajevo');
                $timeZoneC=array_add($timeZoneC, 'Europe/Skopje','Europe/Skopje');
                $timeZoneC=array_add($timeZoneC, 'Europe/Stockholm','Europe/Stockholm');
                $timeZoneC=array_add($timeZoneC, 'Europe/Tirane','Europe/Tirane');
                $timeZoneC=array_add($timeZoneC, 'Europe/Vaduz','Europe/Vaduz');
                $timeZoneC=array_add($timeZoneC, 'Europe/Vatican','Europe/Vatican');
                $timeZoneC=array_add($timeZoneC, 'Europe/Vienna','Europe/Vienna');
                $timeZoneC=array_add($timeZoneC, 'Europe/Warsaw','Europe/Warsaw');
                $timeZoneC=array_add($timeZoneC, 'Europe/Zagreb','Europe/Zagreb');
                $timeZoneC=array_add($timeZoneC, 'Europe/Zurich','Europe/Zurich');
                $timeZoneC=array_add($timeZoneC, 'MET','MET');
                $timeZoneC=array_add($timeZoneC, 'Poland','Poland');
                $timeZoneC=array_add($timeZoneC, 'ART','ART');
                $timeZoneC=array_add($timeZoneC, 'Africa/Blantyre','Africa/Blantyre');
                $timeZoneC=array_add($timeZoneC, 'Africa/Bujumbura','Africa/Bujumbura');
                $timeZoneC=array_add($timeZoneC, 'Africa/Cairo','Africa/Cairo');
                $timeZoneC=array_add($timeZoneC, 'Africa/Gaborone','Africa/Gaborone');
                $timeZoneC=array_add($timeZoneC, 'Africa/Harare','Africa/Harare');
                $timeZoneC=array_add($timeZoneC, 'Africa/Johannesburg','Africa/Johannesburg');
                $timeZoneC=array_add($timeZoneC, 'Africa/Kigali','Africa/Kigali');
                $timeZoneC=array_add($timeZoneC, 'Africa/Lubumbashi','Africa/Lubumbashi');
                $timeZoneC=array_add($timeZoneC, 'Africa/Lusaka','Africa/Lusaka');
                $timeZoneC=array_add($timeZoneC, 'Africa/Maputo','Africa/Maputo');
                $timeZoneC=array_add($timeZoneC, 'Africa/Maseru','Africa/Maseru');
                $timeZoneC=array_add($timeZoneC, 'Africa/Mbabane','Africa/Mbabane');
                $timeZoneC=array_add($timeZoneC, 'Africa/Tripoli','Africa/Tripoli');
                $timeZoneC=array_add($timeZoneC, 'Asia/Amman','Asia/Amman');
                $timeZoneC=array_add($timeZoneC, 'Asia/Beirut','Asia/Beirut');
                $timeZoneC=array_add($timeZoneC, 'Asia/Damascus','Asia/Damascus');
                $timeZoneC=array_add($timeZoneC, 'Asia/Gaza','Asia/Gaza');
                $timeZoneC=array_add($timeZoneC, 'Asia/Istanbul','Asia/Istanbul');
                $timeZoneC=array_add($timeZoneC, 'Asia/Jerusalem','Asia/Jerusalem');
                $timeZoneC=array_add($timeZoneC, 'Asia/Nicosia','Asia/Nicosia');
                $timeZoneC=array_add($timeZoneC, 'Asia/Tel_Aviv','Asia/Tel_Aviv');
                $timeZoneC=array_add($timeZoneC, 'CAT','CAT');
                $timeZoneC=array_add($timeZoneC, 'EET','EET');
                $timeZoneC=array_add($timeZoneC, 'Egypt','Egypt');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-2','Etc/GMT-2');
                $timeZoneC=array_add($timeZoneC, 'Europe/Athens','Europe/Athens');
                $timeZoneC=array_add($timeZoneC, 'Europe/Bucharest','Europe/Bucharest');
                $timeZoneC=array_add($timeZoneC, 'Europe/Chisinau','Europe/Chisinau');
                $timeZoneC=array_add($timeZoneC, 'Europe/Helsinki','Europe/Helsinki');
                $timeZoneC=array_add($timeZoneC, 'Europe/Istanbul','Europe/Istanbul');
                $timeZoneC=array_add($timeZoneC, 'Europe/Kaliningrad','Europe/Kaliningrad');
                $timeZoneC=array_add($timeZoneC, 'Europe/Kiev','Europe/Kiev');
                $timeZoneC=array_add($timeZoneC, 'Europe/Minsk','Europe/Minsk');
                $timeZoneC=array_add($timeZoneC, 'Europe/Nicosia','Europe/Nicosia');
                $timeZoneC=array_add($timeZoneC, 'Europe/Riga','Europe/Riga');
                $timeZoneC=array_add($timeZoneC, 'Europe/Simferopol','Europe/Simferopol');
                $timeZoneC=array_add($timeZoneC, 'Europe/Sofia','Europe/Sofia');
                $timeZoneC=array_add($timeZoneC, 'Europe/Tallinn','Europe/Tallinn');
                $timeZoneC=array_add($timeZoneC, 'Europe/Tiraspol','Europe/Tiraspol');
                $timeZoneC=array_add($timeZoneC, 'Europe/Uzhgorod','Europe/Uzhgorod');
                $timeZoneC=array_add($timeZoneC, 'Europe/Vilnius','Europe/Vilnius');
                $timeZoneC=array_add($timeZoneC, 'Europe/Zaporozhye','Europe/Zaporozhye');
                $timeZoneC=array_add($timeZoneC, 'Israel','Israel');
                $timeZoneC=array_add($timeZoneC, 'Libya','Libya');
                $timeZoneC=array_add($timeZoneC, 'Turkey','Turkey');
                $timeZoneC=array_add($timeZoneC, 'Africa/Addis_Ababa','Africa/Addis_Ababa');
                $timeZoneC=array_add($timeZoneC, 'Africa/Asmera','Africa/Asmera');
                $timeZoneC=array_add($timeZoneC, 'Africa/Dar_es_Salaam','Africa/Dar_es_Salaam');
                $timeZoneC=array_add($timeZoneC, 'Africa/Djibouti','Africa/Djibouti');
                $timeZoneC=array_add($timeZoneC, 'Africa/Kampala','Africa/Kampala');
                $timeZoneC=array_add($timeZoneC, 'Africa/Khartoum','Africa/Khartoum');
                $timeZoneC=array_add($timeZoneC, 'Africa/Mogadishu','Africa/Mogadishu');
                $timeZoneC=array_add($timeZoneC, 'Africa/Nairobi','Africa/Nairobi');
                $timeZoneC=array_add($timeZoneC, 'Antarctica/Syowa','Antarctica/Syowa');
                $timeZoneC=array_add($timeZoneC, 'Asia/Aden','Asia/Aden');
                $timeZoneC=array_add($timeZoneC, 'Asia/Baghdad','Asia/Baghdad');
                $timeZoneC=array_add($timeZoneC, 'Asia/Bahrain','Asia/Bahrain');
                $timeZoneC=array_add($timeZoneC, 'Asia/Kuwait','Asia/Kuwait');
                $timeZoneC=array_add($timeZoneC, 'Asia/Qatar','Asia/Qatar');
                $timeZoneC=array_add($timeZoneC, 'Asia/Riyadh','Asia/Riyadh');
                $timeZoneC=array_add($timeZoneC, 'EAT','EAT');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-3','Etc/GMT-3');
                $timeZoneC=array_add($timeZoneC, 'Europe/Moscow','Europe/Moscow');
                $timeZoneC=array_add($timeZoneC, 'Indian/Antananarivo','Indian/Antananarivo');
                $timeZoneC=array_add($timeZoneC, 'Indian/Comoro','Indian/Comoro');
                $timeZoneC=array_add($timeZoneC, 'Indian/Mayotte','Indian/Mayotte');
                $timeZoneC=array_add($timeZoneC, 'W-SU','W-SU');
                $timeZoneC=array_add($timeZoneC, 'Asia/Riyadh87','Asia/Riyadh87');
                $timeZoneC=array_add($timeZoneC, 'Asia/Riyadh88','Asia/Riyadh88');
                $timeZoneC=array_add($timeZoneC, 'Asia/Riyadh89','Asia/Riyadh89');
                $timeZoneC=array_add($timeZoneC, 'Mideast/Riyadh87','Mideast/Riyadh87');
                $timeZoneC=array_add($timeZoneC, 'Mideast/Riyadh88','Mideast/Riyadh88');
                $timeZoneC=array_add($timeZoneC, 'Mideast/Riyadh89','Mideast/Riyadh89');
                $timeZoneC=array_add($timeZoneC, 'Asia/Tehran','Asia/Tehran');
                $timeZoneC=array_add($timeZoneC, 'Iran','Iran');
                $timeZoneC=array_add($timeZoneC, 'Asia/Aqtau','Asia/Aqtau');
                $timeZoneC=array_add($timeZoneC, 'Asia/Baku','Asia/Baku');
                $timeZoneC=array_add($timeZoneC, 'Asia/Dubai','Asia/Dubai');
                $timeZoneC=array_add($timeZoneC, 'Asia/Muscat','Asia/Muscat');
                $timeZoneC=array_add($timeZoneC, 'Asia/Oral','Asia/Oral');
                $timeZoneC=array_add($timeZoneC, 'Asia/Tbilisi','Asia/Tbilisi');
                $timeZoneC=array_add($timeZoneC, 'Asia/Yerevan','Asia/Yerevan');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-4','Etc/GMT-4');
                $timeZoneC=array_add($timeZoneC, 'Europe/Samara','Europe/Samara');
                $timeZoneC=array_add($timeZoneC, 'Indian/Mahe','Indian/Mahe');
                $timeZoneC=array_add($timeZoneC, 'Indian/Mauritius','Indian/Mauritius');
                $timeZoneC=array_add($timeZoneC, 'Indian/Reunion','Indian/Reunion');
                $timeZoneC=array_add($timeZoneC, 'NET','NET');
                $timeZoneC=array_add($timeZoneC, 'Asia/Kabul','Asia/Kabul');
                $timeZoneC=array_add($timeZoneC, 'Asia/Aqtobe','Asia/Aqtobe');
                $timeZoneC=array_add($timeZoneC, 'Asia/Ashgabat','Asia/Ashgabat');
                $timeZoneC=array_add($timeZoneC, 'Asia/Ashkhabad','Asia/Ashkhabad');
                $timeZoneC=array_add($timeZoneC, 'Asia/Bishkek','Asia/Bishkek');
                $timeZoneC=array_add($timeZoneC, 'Asia/Dushanbe','Asia/Dushanbe');
                $timeZoneC=array_add($timeZoneC, 'Asia/Karachi','Asia/Karachi');
                $timeZoneC=array_add($timeZoneC, 'Asia/Samarkand','Asia/Samarkand');
                $timeZoneC=array_add($timeZoneC, 'Asia/Tashkent','Asia/Tashkent');
                $timeZoneC=array_add($timeZoneC, 'Asia/Yekaterinburg','Asia/Yekaterinburg');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-5','Etc/GMT-5');
                $timeZoneC=array_add($timeZoneC, 'Indian/Kerguelen','Indian/Kerguelen');
                $timeZoneC=array_add($timeZoneC, 'Indian/Maldives','Indian/Maldives');
                $timeZoneC=array_add($timeZoneC, 'PLT','PLT');
                $timeZoneC=array_add($timeZoneC, 'Asia/Calcutta','Asia/Calcutta');
                $timeZoneC=array_add($timeZoneC, 'IST','IST');
                $timeZoneC=array_add($timeZoneC, 'Asia/Katmandu','Asia/Katmandu');
                $timeZoneC=array_add($timeZoneC, 'Antarctica/Mawson','Antarctica/Mawson');
                $timeZoneC=array_add($timeZoneC, 'Antarctica/Vostok','Antarctica/Vostok');
                $timeZoneC=array_add($timeZoneC, 'Asia/Almaty','Asia/Almaty');
                $timeZoneC=array_add($timeZoneC, 'Asia/Colombo','Asia/Colombo');
                $timeZoneC=array_add($timeZoneC, 'Asia/Dacca','Asia/Dacca');
                $timeZoneC=array_add($timeZoneC, 'Asia/Dhaka','Asia/Dhaka');
                $timeZoneC=array_add($timeZoneC, 'Asia/Novosibirsk','Asia/Novosibirsk');
                $timeZoneC=array_add($timeZoneC, 'Asia/Omsk','Asia/Omsk');
                $timeZoneC=array_add($timeZoneC, 'Asia/Qyzylorda','Asia/Qyzylorda');
                $timeZoneC=array_add($timeZoneC, 'Asia/Thimbu','Asia/Thimbu');
                $timeZoneC=array_add($timeZoneC, 'Asia/Thimphu','Asia/Thimphu');
                $timeZoneC=array_add($timeZoneC, 'BST','BST');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-6','Etc/GMT-6');
                $timeZoneC=array_add($timeZoneC, 'Indian/Chagos','Indian/Chagos');
                $timeZoneC=array_add($timeZoneC, 'Asia/Rangoon','Asia/Rangoon');
                $timeZoneC=array_add($timeZoneC, 'Indian/Cocos','Indian/Cocos');
                $timeZoneC=array_add($timeZoneC, 'Antarctica/Davis','Antarctica/Davis');
                $timeZoneC=array_add($timeZoneC, 'Asia/Bangkok','Asia/Bangkok');
                $timeZoneC=array_add($timeZoneC, 'Asia/Hovd','Asia/Hovd');
                $timeZoneC=array_add($timeZoneC, 'Asia/Jakarta','Asia/Jakarta');
                $timeZoneC=array_add($timeZoneC, 'Asia/Krasnoyarsk','Asia/Krasnoyarsk');
                $timeZoneC=array_add($timeZoneC, 'Asia/Phnom_Penh','Asia/Phnom_Penh');
                $timeZoneC=array_add($timeZoneC, 'Asia/Pontianak','Asia/Pontianak');
                $timeZoneC=array_add($timeZoneC, 'Asia/Saigon','Asia/Saigon');
                $timeZoneC=array_add($timeZoneC, 'Asia/Vientiane','Asia/Vientiane');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-7','Etc/GMT-7');
                $timeZoneC=array_add($timeZoneC, 'Indian/Christmas','Indian/Christmas');
                $timeZoneC=array_add($timeZoneC, 'VST','VST');
                $timeZoneC=array_add($timeZoneC, 'Antarctica/Casey','Antarctica/Casey');
                $timeZoneC=array_add($timeZoneC, 'Asia/Brunei','Asia/Brunei');
                $timeZoneC=array_add($timeZoneC, 'Asia/Chongqing','Asia/Chongqing');
                $timeZoneC=array_add($timeZoneC, 'Asia/Chungking','Asia/Chungking');
                $timeZoneC=array_add($timeZoneC, 'Asia/Harbin','Asia/Harbin');
                $timeZoneC=array_add($timeZoneC, 'Asia/Hong_Kong','Asia/Hong_Kong');
                $timeZoneC=array_add($timeZoneC, 'Asia/Irkutsk','Asia/Irkutsk');
                $timeZoneC=array_add($timeZoneC, 'Asia/Kashgar','Asia/Kashgar');
                $timeZoneC=array_add($timeZoneC, 'Asia/Kuala_Lumpur','Asia/Kuala_Lumpur');
                $timeZoneC=array_add($timeZoneC, 'Asia/Kuching','Asia/Kuching');
                $timeZoneC=array_add($timeZoneC, 'Asia/Macao','Asia/Macao');
                $timeZoneC=array_add($timeZoneC, 'Asia/Macau','Asia/Macau');
                $timeZoneC=array_add($timeZoneC, 'Asia/Makassar','Asia/Makassar');
                $timeZoneC=array_add($timeZoneC, 'Asia/Manila','Asia/Manila');
                $timeZoneC=array_add($timeZoneC, 'Asia/Shanghai','Asia/Shanghai');
                $timeZoneC=array_add($timeZoneC, 'Asia/Singapore','Asia/Singapore');
                $timeZoneC=array_add($timeZoneC, 'Asia/Taipei','Asia/Taipei');
                $timeZoneC=array_add($timeZoneC, 'Asia/Ujung_Pandang','Asia/Ujung_Pandang');
                $timeZoneC=array_add($timeZoneC, 'Asia/Ulaanbaatar','Asia/Ulaanbaatar');
                $timeZoneC=array_add($timeZoneC, 'Asia/Ulan_Bator','Asia/Ulan_Bator');
                $timeZoneC=array_add($timeZoneC, 'Asia/Urumqi','Asia/Urumqi');
                $timeZoneC=array_add($timeZoneC, 'Australia/Perth','Australia/Perth');
                $timeZoneC=array_add($timeZoneC, 'Australia/West','Australia/West');
                $timeZoneC=array_add($timeZoneC, 'CTT','CTT');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-8','Etc/GMT-8');
                $timeZoneC=array_add($timeZoneC, 'Hongkong','Hongkong');
                $timeZoneC=array_add($timeZoneC, 'PRC','PRC');
                $timeZoneC=array_add($timeZoneC, 'Singapore','Singapore');
                $timeZoneC=array_add($timeZoneC, 'Asia/Choibalsan','Asia/Choibalsan');
                $timeZoneC=array_add($timeZoneC, 'Asia/Dili','Asia/Dili');
                $timeZoneC=array_add($timeZoneC, 'Asia/Jayapura','Asia/Jayapura');
                $timeZoneC=array_add($timeZoneC, 'Asia/Pyongyang','Asia/Pyongyang');
                $timeZoneC=array_add($timeZoneC, 'Asia/Seoul','Asia/Seoul');
                $timeZoneC=array_add($timeZoneC, 'Asia/Tokyo','Asia/Tokyo');
                $timeZoneC=array_add($timeZoneC, 'Asia/Yakutsk','Asia/Yakutsk');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-9','Etc/GMT-9');
                $timeZoneC=array_add($timeZoneC, 'JST','JST');
                $timeZoneC=array_add($timeZoneC, 'Japan','Japan');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Palau','Pacific/Palau');
                $timeZoneC=array_add($timeZoneC, 'ROK','ROK');
                $timeZoneC=array_add($timeZoneC, 'ACT','ACT');
                $timeZoneC=array_add($timeZoneC, 'Australia/Adelaide','Australia/Adelaide');
                $timeZoneC=array_add($timeZoneC, 'Australia/Broken_Hill','Australia/Broken_Hill');
                $timeZoneC=array_add($timeZoneC, 'Australia/Darwin','Australia/Darwin');
                $timeZoneC=array_add($timeZoneC, 'Australia/North','Australia/North');
                $timeZoneC=array_add($timeZoneC, 'Australia/South','Australia/South');
                $timeZoneC=array_add($timeZoneC, 'Australia/Yancowinna','Australia/Yancowinna');
                $timeZoneC=array_add($timeZoneC, 'AET','AET');
                $timeZoneC=array_add($timeZoneC, 'Antarctica/DumontDUrville','Antarctica/DumontDUrville');
                $timeZoneC=array_add($timeZoneC, 'Asia/Sakhalin','Asia/Sakhalin');
                $timeZoneC=array_add($timeZoneC, 'Asia/Vladivostok','Asia/Vladivostok');
                $timeZoneC=array_add($timeZoneC, 'Australia/ACT','Australia/ACT');
                $timeZoneC=array_add($timeZoneC, 'Australia/Brisbane','Australia/Brisbane');
                $timeZoneC=array_add($timeZoneC, 'Australia/Canberra','Australia/Canberra');
                $timeZoneC=array_add($timeZoneC, 'Australia/Hobart','Australia/Hobart');
                $timeZoneC=array_add($timeZoneC, 'Australia/Lindeman','Australia/Lindeman');
                $timeZoneC=array_add($timeZoneC, 'Australia/Melbourne','Australia/Melbourne');
                $timeZoneC=array_add($timeZoneC, 'Australia/NSW','Australia/NSW');
                $timeZoneC=array_add($timeZoneC, 'Australia/Queensland','Australia/Queensland');
                $timeZoneC=array_add($timeZoneC, 'Australia/Sydney','Australia/Sydney');
                $timeZoneC=array_add($timeZoneC, 'Australia/Tasmania','Australia/Tasmania');
                $timeZoneC=array_add($timeZoneC, 'Australia/Victoria','Australia/Victoria');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-10','Etc/GMT-10');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Guam','Pacific/Guam');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Port_Moresby','Pacific/Port_Moresby');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Saipan','Pacific/Saipan');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Truk','Pacific/Truk');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Yap','Pacific/Yap');
                $timeZoneC=array_add($timeZoneC, 'Australia/LHI','Australia/LHI');
                $timeZoneC=array_add($timeZoneC, 'Australia/Lord_Howe','Australia/Lord_Howe');
                $timeZoneC=array_add($timeZoneC, 'Asia/Magadan','Asia/Magadan');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-11','Etc/GMT-11');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Efate','Pacific/Efate');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Guadalcanal','Pacific/Guadalcanal');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Kosrae','Pacific/Kosrae');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Noumea','Pacific/Noumea');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Ponape','Pacific/Ponape');
                $timeZoneC=array_add($timeZoneC, 'SST','SST');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Norfolk','Pacific/Norfolk');
                $timeZoneC=array_add($timeZoneC, 'Antarctica/McMurdo','Antarctica/McMurdo');
                $timeZoneC=array_add($timeZoneC, 'Antarctica/South_Pole','Antarctica/South_Pole');
                $timeZoneC=array_add($timeZoneC, 'Asia/Anadyr','Asia/Anadyr');
                $timeZoneC=array_add($timeZoneC, 'Asia/Kamchatka','Asia/Kamchatka');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-12','Etc/GMT-12');
                $timeZoneC=array_add($timeZoneC, 'Kwajalein','Kwajalein');
                $timeZoneC=array_add($timeZoneC, 'NST','NST');
                $timeZoneC=array_add($timeZoneC, 'NZ','NZ');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Auckland','Pacific/Auckland');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Fiji','Pacific/Fiji');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Funafuti','Pacific/Funafuti');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Kwajalein','Pacific/Kwajalein');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Majuro','Pacific/Majuro');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Nauru','Pacific/Nauru');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Tarawa','Pacific/Tarawa');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Wake','Pacific/Wake');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Wallis','Pacific/Wallis');
                $timeZoneC=array_add($timeZoneC, 'NZ-CHAT','NZ-CHAT');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Chatham','Pacific/Chatham');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-13','Etc/GMT-13');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Enderbury','Pacific/Enderbury');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Tongatapu','Pacific/Tongatapu');
                $timeZoneC=array_add($timeZoneC, 'Etc/GMT-14','Etc/GMT-14');
                $timeZoneC=array_add($timeZoneC, 'Pacific/Kiritimati','Pacific/Kiritimati');
                $timeZoneC=array_add($timeZoneC, 'Asia/Kolkata','Asia/Kolkata');



        return $timeZoneC;
}

        public function fransearch()
        {
                Log::info('------- inside fransearch--------');
        if (! Auth::check ()) {
            return Redirect::to ( 'login' );
        }
        $username = Auth::user ()->username;
        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        Log::info(' inside multi ' );
        $fransId1 = array('' =>'-- SELECT --');
        $prePaidFransId = array(''  =>'-- SELECT --');
        $fransId = $redis->smembers('S_Franchises');
        foreach($fransId as $fran) {
          $franDetails_json = $redis->hget ( 'H_Franchise', $fran);
          $franchiseDetails=json_decode($franDetails_json,true);
          $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
          if($prepaid == 'yes') {
             $prePaidFransId = array_add($prePaidFransId, $fran,$fran);
          }
          else {
           $fransId1 = array_add($fransId1, $fran,$fran);
          }
        }

        //$fransId = $orgArr;

        //$count = count($redis->keys ( 'NoData:*' ));
                $apnKey = $redis->exists('Update:Apn');
        $timezoneKey = $redis->exists('Update:Timezone');
        array_multisort($fransId1);
        array_multisort($prePaidFransId);
        return view ( 'vdm.franchise.frans' )->with('fransId',$fransId1)->with('prePaidFransId',$prePaidFransId)->with('apnKey',$apnKey)->with('timezoneKey',$timezoneKey);

        }




public function users()
        {
                  Log::info('------- inside fransearch--------');
        if (! Auth::check ()) {
            return Redirect::to ( 'login' );
        }
        $username = Auth::user ()->username;
        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        Log::info(' inside multi ' );

        $userId = $redis->smembers('S_Franchises');
                $orgArr = array();
        foreach($userId as $org) {
                $temp=$redis->smembers( 'S_Users_' . $org);
                foreach ($temp as $key) {
                         $orgArr = array_add($orgArr, $key,$key);
                }


        }
        $userId = $orgArr;
                //$count = count($redis->keys ( 'NoData:*' ));
                $apnKey = $redis->exists('Update:Apn');
        $timezoneKey = $redis->exists('Update:Timezone');
        return view ( 'vdm.franchise.users' )->with('userId',$userId)->with('apnKey',$apnKey)->with('timezoneKey',$timezoneKey);

        }


        public function buyAddress()
        {
                  Log::info('------- inside fransearch--------');
        if (! Auth::check ()) {
            return Redirect::to ( 'login' );
        }
        $username = Auth::user ()->username;
        $redis = Redis::connection ();
        $addressCount=$redis->get('VAMOSADDRESS_CONTROL');
        if($addressCount==null)
        {
                $addressCount=0;
        }
                //$count = count($redis->keys ( 'NoData:*' ));
                $apnKey = $redis->exists('Update:Apn');
        $timezoneKey = $redis->exists('Update:Timezone');
        return view ( 'vdm.franchise.buyAddress' )->with('addressCount',$addressCount)->with('apnKey',$apnKey)->with('timezoneKey',$timezoneKey);

        }


        public function updateAddCount() {
                    log::info( '-----------List----------- ::');
                    if (! Auth::check () ) {
                                    return Redirect::to ( 'login' );
                    }
                     $redis = Redis::connection ();
                   $redis->set('VAMOSADDRESS_CONTROL',Input::get ( 'addressCount' ));
                return Redirect::to ( 'vdmFranchises' );
    }



                public function findFransList() {
                                log::info( '-----------List----------- ::');
                                if (! Auth::check () ) {
                                                return Redirect::to ( 'login' );
                                }
                                $redis = Redis::connection ();
                               $username = Input::get ( 'frans' );
                                                           if($username == null) {
                               $username = Input::get ('prePaidFrans');
                               }
                                                        $franDetails_json = $redis->hget ( 'H_Franchise', $username);
                                                        $franchiseDetails=json_decode($franDetails_json,true);

                                                                if(isset($franchiseDetails['userId'])==1)
                                                                        $username=$franchiseDetails['userId'];
                                                                else
                                                                        $username=null;

                                                   if($username==null)
                                {
                                                log::info( '--------use one----------' );
                                                $username = Session::get('page');
                                }
                                else{
                                                log::info( '--------use two----------'.$username);
                                                Session::put('page',$username);
                                }

                                                try{
                                                                 $user=User::where('username', '=', $username)->firstOrFail();
                                                                                                log::info( '--------new name----------' .$user);
                                                                        Auth::login($user);
                                                }catch(\Exception $e)
                                                                                   {
                                                                                                return Redirect::to ( 'vdmFranchises/fransearch' );
                                                                                   }
                                 //$user = User::find(10);
                                 Session::put('frnSwt','fransswitch');
                                 if(Session::get('frnSwt')=='fransswitch'){
                                        log::info('Inside the vamos admin session');
                                 }




                                return Redirect::to ( 'Business' );
                }





                public function findUsersList() {
                                log::info( '-----------List----------- ::');
                                if (! Auth::check () ) {
                                                return Redirect::to ( 'login' );
                                }
                                 $redis = Redis::connection ();
                               $username = Input::get ( 'users' );

                                                  if($username==null)
                                {
                                                log::info( '--------use one----------' );
                                                $username = Session::get('page');
                                }
                                else{
                                                log::info( '--------use two----------'.$username);
                                                Session::put('page',$username);
                                }

                                                try{
                                                                 $user=User::where('username', '=', $username)->firstOrFail();
                                                                                                log::info( '--------new name----------' .$user);
                                                                        Auth::login($user);
                                                }catch(\Exception $e)
                                                                                   {
                                                                                                return Redirect::to ( 'vdmFranchises/users' );
                                                                                   }
                                 //$user = User::find(10);




                                return Redirect::to ( 'Business' );
                }
        /**

                Frabchise is created by VAMOS Admin
                Franchise name
                Franchise ID (company ID)
                Franchise description
                Franchise full address
                Franchise landline no
                Franchise mobile number 1
                Franchise mobile number 2
                Franchise email id1
                Franchise email id2
                Franchise other details
                Franchise login details

         *
         * @return Response
         */
        public function store() {
                Log::info("reached franchise store");
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $username = Auth::user ()->username;
                $redis = Redis::connection ();

                $rules = array (
                                'fname' => 'required',
                                'fcode' => 'required',
                                'description' => 'required',
                                'fullAddress' => 'required',
                                'landline' => 'required',
                                'mobileNo1' => 'required',
                                'email1' => 'required|email',
                                'email2' => 'required|email',
                                'userId' => 'required',
                                'website' => 'required',
                                'trackPage' => 'required'
                );
                $validator = Validator::make ( Input::all (), $rules );
                $userId = Input::get ('userId');
                $fcode = Input::get ( 'fcode' );
                $dbIpindex=Input::get ( 'ipadd' );
                Log::info("dbIpindex..".$dbIpindex);
                $val = $redis->sismember('S_Franchises',$fcode);
                $val1= $redis->sismember ( 'S_Users_' . $fcode, $userId );


                if ($validator->fails ()) {
                        return Redirect::to ( 'vdmFranchises/create' )->withErrors ( $validator )->withInput();
                }else if($val==1 ) {
                        Session::flash ( 'message', $fcode . 'Franchise already exist ' . '!' );
                        return Redirect::to ( 'vdmFranchises/create' )->withInput();
                }
                else if($val1==1) {
                        Session::flash ( 'message', $userId . ' already exist. Please use different id ' . '!' )->withInput();
                        return Redirect::to ( 'vdmFranchises/create' );
                }
                else {
                        // store
                        $dbIpAr=VdmFranchiseController::dbIp();
                        $dbIp=$dbIpAr[$dbIpindex];
                        $fname = Input::get ( 'fname' );
                        $fcode1 = Input::get ( 'fcode' );
                        $description = Input::get ( 'description' );
                        $fullAddress = Input::get ( 'fullAddress' );
                        $landline = Input::get ( 'landline' );
                        $mobileNo1 = Input::get ( 'mobileNo1' );
                        $mobileNo2 = Input::get ( 'mobileNo2' );
                        $website = Input::get ( 'website' );
                        $website2 = Input::get ( 'website2' );
                        $website3 = Input::get ( 'website3' );
                        $website4 = Input::get ( 'website4' );
                        $trackPage = Input::get ( 'trackPage' );
                        $email1 = Input::get ( 'email1' );
                        $email2 = Input::get ( 'email2' );
                        $userId = Input::get ('userId');
                        $otherDetails = Input::get ('otherDetails');
                        $smsSender=Input::get ('smsSender');
                        $smsProvider=Input::get ('smsProvider');
                        $providerUserName=Input::get ('providerUserName');
                        $providerPassword=Input::get ('providerPassword');
                        $backUpDays=Input::get('backUpDays');
                        //$eFDSchedular=Input::get ('eFDSchedular');
                        $timeZone=Input::get ('timeZone');
                        $apiKey=Input::get('apiKey');
                        $apiKey2=Input::get('apiKey2');
                        $apiKey3=Input::get('apiKey3');
                        $apiKey4=Input::get('apiKey4');
                        $mapKey=Input::get('mapKey');
                        $addressKey=Input::get('addressKey');
                        $notificationKey=Input::get('notificationKey');
                        $dbType=Input::get('dbType');
                        $zoho=Input::get('zoho');
                        $auth=Input::get('auth');
                        $gpsvtsAppKey=Input::get('gpsvtsAppKey');
                        $subDomain=Input::get('subDomain');
                $userId1=$userId;
            $fcode=preg_replace('/\s/', '', $fcode1);
                if($fcode!=preg_match('/[^A-Za-z]+/', $fcode)) {
                                return Redirect::back()->withInput()->withErrors ('Numeric value not allowed in fcode');
                        }
                        // $refDataArr = array('regNo'=>$regNo,'vehicleMake'=>$vehicleMake,'vehicleType'=>$vehicleType,'oprName'=>$oprName,
                        // 'mobileNo'=>$mobileNo,'vehicleCap'=>$vehicleCap,'deviceModel'=>$deviceModel);


                        $redis->sadd('S_Franchises',$fcode);
                        if (strpos($userId1, 'admin') !== false || strpos($userId1, 'Admin') !== false || strpos($userId1, 'ADMIN') !== false) {
                                $userId=$userId1;
                        }else{
                                        $userId =preg_replace('/'.$userId1.'/',$userId1.'admin', $userId1);
                        }
                        if(($website==$website2&&($website!=null&&$website2!=null)) || ($website==$website3&&($website!=null&&$website3!=null))|| ($website==$website4&&($website!=null&&$website4!=null)) || ($website2==$website3&&($website2!=null&&$website3!=null)) || ($website2==$website4&&($website2!=null&&$website4!=null)) || ($website3==$website4&&($website3!=null&&$website4!=null)))
                        {
                                return Redirect::to ( 'vdmFranchises/create' )->withInput()->withErrors('Website names should not be same');
                        }
                        if(($apiKey==$apiKey2&&($apiKey!=null&&$apiKey2!=null)) || ($apiKey==$apiKey3&&($apiKey!=null&&$apiKey3!=null)) || ($apiKey==$apiKey4&&($apiKey!=null&&$apiKey4!=null)) || ($apiKey2==$apiKey3&&($apiKey2!=null&&$apiKey3!=null)) || ($apiKey2==$apiKey4&&($apiKey2!=null&&$apiKey4!=null)) || ($apiKey3==$apiKey4&&($apiKey3!=null&&$apiKey4!=null)))
                        {
                                return Redirect::to ( 'vdmFranchises/create' )->withInput()->withErrors('Map / API keys should not be same');
                        }
                        $pattern = "/[^0-9\,]/";
                        if ((preg_match($pattern, $mobileNo1)) || (preg_match($pattern, $mobileNo2))) {
                                return Redirect::to ( 'vdmFranchises/'.$fcode.'/edit' )->withInput()->withErrors('Only Numeric values are allowed in Mobile Numbers');
                        }
                        $prepaid=Input::get('prepaid');
                        if($prepaid=='yes'){
                                $numberofBasicLicence=Input::get ('numberofBasicLicence');
                                $numberofAdvanceLicence=Input::get ('numberofAdvanceLicence');
                                $numberofPremiumLicence=Input::get ('numberofPremiumLicence');
                $numberofPremPlusLicence=Input::get ('numberofPremPlusLicence');

                                $availableBasicLicence          =$numberofBasicLicence;
                                $availableAdvanceLicence        =$numberofAdvanceLicence;
                                $availablePremiumLicence        =$numberofPremiumLicence;
                $availablePremPlusLicence   =$numberofPremPlusLicence;
                if($availableBasicLicence==null){
                    $availableBasicLicence=0;
                }
                if($availableAdvanceLicence==null){
                    $availableAdvanceLicence=0;
                }
                if($availablePremiumLicence==null){
                    $availablePremiumLicence=0;
                }
                if($availablePremPlusLicence==null){
                    $availablePremPlusLicence=0;
                }
                if($numberofPremPlusLicence==null){
                    $numberofPremPlusLicence=0;
                }
                if($numberofPremiumLicence==null){
                    $numberofPremiumLicence=0;
                }
                if($numberofAdvanceLicence==null){
                    $numberofAdvanceLicence=0;
                }
                if($numberofBasicLicence==null){
                    $numberofBasicLicence=0;
                }
                                $details = array (
                                        'fname' => $fname,
                                        'description' => $description,
                                        'landline' => $landline,
                                        'mobileNo1' => $mobileNo1,
                                        'mobileNo2' => $mobileNo2,
                                        'prepaid'       => $prepaid,
                                        'email1' => $email1,
                                        'email2' => $email2,
                                        'userId' => $userId,
                                        'fullAddress' => $fullAddress,
                                        'otherDetails' => $otherDetails,
                                        'numberofBasicLicence'  => $numberofBasicLicence,
                                        'numberofAdvanceLicence' => $numberofAdvanceLicence,
                                        'numberofPremiumLicence' => $numberofPremiumLicence,
                    'numberofPremPlusLicence' => $numberofPremPlusLicence,
                                        'availableBasicLicence' => $availableBasicLicence,
                                        'availableAdvanceLicence' => $availableAdvanceLicence,
                                        'availablePremiumLicence'       => $availablePremiumLicence,
                    'availablePremPlusLicence'  => $availablePremPlusLicence,
                                        'website'=>$website,
                                        'website2'=>$website2,
                                        'website3'=>$website3,
                                        'website4'=>$website4,
                                        'trackPage'=>$trackPage,
                                        'smsSender'=>$smsSender,
                                        'smsProvider'=>$smsProvider,
                                        'providerUserName'=>$providerUserName,
                                        'providerPassword'=>$providerPassword,
                                        'timeZone'=>$timeZone,
                                        'apiKey'=>$apiKey,
                                        'apiKey2'=>$apiKey2,
                                        'apiKey3'=>$apiKey3,
                                        'apiKey4'=>$apiKey4,
                                        'mapKey'=>$mapKey,
                                        'addressKey'=>$addressKey,
                                        'notificationKey'=>$notificationKey,
                                        'gpsvtsApp'=>$gpsvtsAppKey,
                                        'backUpDays'=>$backUpDays,
                                        'dbType'=>$dbType,
                                        'zoho'=>$zoho,
                                        'auth'=>$auth,
                                        'subDomain'=>$subDomain,
                                        //'eFDSchedular'=>$eFDSchedular
                                );

                        }else{
                                $numberofLicence = Input::get ('numberofLicence');

                                $details = array (
                                        'fname' => $fname,
                                        'description' => $description,
                                        'landline' => $landline,
                                        'mobileNo1' => $mobileNo1,
                                        'mobileNo2' => $mobileNo2,
                                        'prepaid'       =>$prepaid,
                                        'email1' => $email1,
                                        'email2' => $email2,
                                        'userId' => $userId,
                                        'fullAddress' => $fullAddress,
                                        'otherDetails' => $otherDetails,
                                        'numberofLicence' => $numberofLicence,
                                        'availableLincence'=>$numberofLicence,
                                        'website'=>$website,
                                        'website2'=>$website2,
                                        'website3'=>$website3,
                                        'website4'=>$website4,
                                        'trackPage'=>$trackPage,
                                        'smsSender'=>$smsSender,
                                        'smsProvider'=>$smsProvider,
                                        'providerUserName'=>$providerUserName,
                                        'providerPassword'=>$providerPassword,
                                        'timeZone'=>$timeZone,
                                        'apiKey'=>$apiKey,
                                        'apiKey2'=>$apiKey2,
                                        'apiKey3'=>$apiKey3,
                                        'apiKey4'=>$apiKey4,
                                        'mapKey'=>$mapKey,
                                        'addressKey'=>$addressKey,
                                        'notificationKey'=>$notificationKey,
                                        'gpsvtsApp'=>$gpsvtsAppKey,
                                        'backUpDays'=>$backUpDays,
                                        'dbType'=>$dbType,
                                        'zoho'=>$zoho,
                                        'auth'=>$auth,
                                        'subDomain'=>$subDomain,
                                        //'eFDSchedular'=>$eFDSchedular
                                );
                        }


                        /*$redis->hmset ( 'H_Franchise', $fcode.':fname',$fname,$fcode.':description',$description,
                                        $fcode.':landline',$landline,$fcode.':mobileNo1',$mobileNo1,$fcode.':mobileNo2',$mobileNo2,
                                        $fcode.':email1',$email1,$fcode.':email2',$email2,$fcode.':userId',$userId);*///ram what to do migration
      $geolocation=VdmFranchiseController::geocode($fullAddress);
      if($geolocation!=null){
        $LatLong=implode(":",$geolocation);
      }else
      {
        $LatLong='0:0';
      }
      log::info('Franchise LatLong '.$LatLong);
      $redis->hset('H_Franchise_LatLong',$fcode,$LatLong);

                        $redis->hmset('H_Franchise_Mysql_DatabaseIP',$fcode,$dbIp);
                        $detailsJson = json_encode ( $details );
                        $redis->hmset ( 'H_Franchise', $fcode,$detailsJson);

            log::info('before table insert');
            try{
                            $mysqlDetails =array();
                            $status =array();
                                $status=array(
                                            'username'=>$username,
                                            'status' => Config::get('constant.created'),
                                            'fcode' => $fcode,
                                            'addBasicLicence' => Input::get('numberofBasicLicence'),
                                            'addAdvanceLicence'=>Input::get('numberofAdvanceLicence') ,
                                                'addPremiumLicence'=>Input::get('numberofPremiumLicence'),
                                                'addLicence'=> Input::get ('numberofLicence'),
                                                'userIpAddress'=>Session::get('userIP'),
                                        );
                                        $mysqlDetails   = array_merge($status,$details);
                                        //$table=new AuditFrans->getTable();
                                        $modelname = new AuditFrans();
                                    $table = $modelname->getTable();
                                        //return $table;
                                        //return $mysqlDetails;
                                        $db=Config::get('constant.VAMOSdb');
                                    AuditTables::ChangeDB($db);
                                        $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
                                        //return $tableExist;
                                        if(count($tableExist)>0){
                                                AuditFrans::create($mysqlDetails);
                                        }
                                        else{
                                                AuditTables::CreateAuditFrans();
                                                AuditFrans::create($mysqlDetails);
                                        }

                }
                        catch (Exception $e) {
                          log::info('Error inside Audit_Frans Create'.$e->getMessage());
                          log::info($mysqlDetails);
                        }
            //AuditFrans::create($details);
                        log::info('successfully inserted into table Audit_Frans ');


                        $redis->hmset('H_Fcode_Timezone_Schedular',$fcode,$timeZone);
                        $redis->sadd ( 'S_Users_' . $fcode, $userId );
                        $password='awesome';
                        $redis->hmset ( 'H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo1,$userId.':email',$email1 ,$userId.':password',$password,$userId. ':zoho',$zoho, $userId. 'auth',$auth, $userId. ':OWN','admin');
                        $defaultReports = $redis->smembers('S_Default_ReportList');
                foreach ($defaultReports as $key => $value) {
                    $redis-> sadd('S_Users_Reports_Admin_'.$fcode , $value);
                }
                        $user = new User;
                        $user->name = $fname;
                        $user->username=$userId;
                        $user->email=$email1;
                        $user->mobileNo=$mobileNo1;
                        //$user->zoho=$zoho;
                        $user->password=Hash::make($password);
                        $user->save();

                        Log::info("going to email..");

                        /**
                         * Add vamos admin user for each franchise
                         *
                         */
                        $user = new User;
                        $vamosid='vamos'.$fcode;
                        $user->name = 'vamos'.$fname;
                        $user->mobileNo='1234567890';
                        $user->username=$vamosid;
                        $user->email='support@vamosys.com';
                        $user->password=Hash::make($password);
                        $user->save();
                        $redis->sadd ( 'S_Users_' . $fcode, $vamosid );
                        $redis->hmset ( 'H_UserId_Cust_Map', $vamosid . ':fcode', $fcode);
                        $redis->hset ( 'H_Google_Android_ServerKey',$fcode, $notificationKey );
                        $redis->hset ( 'H_MobileAppId',$fcode, $gpsvtsAppKey );
                        $servername=$redis->hget('H_Franchise_Mysql_DatabaseIP',$fcode);
                 if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
                 return 'Ipaddress Failed !!!';
            }
            $usernamedb  =  "root";
            $password    =  "#vamo123";
            $dbname      =  $fcode;
                $conn  =  mysqli_connect($servername, $usernamedb, $password);
            $create_db = "CREATE DATABASE IF NOT EXISTS $dbname";
            if($conn->query($create_db)) {
                $conn  =  mysqli_connect($servername, $usernamedb, $password, $dbname);
             if(!$conn) {
                                die('Could not connect:'.mysqli_connect_error());
                                return 'Please Update One more time Connection failed';

            }
            else {
                log::info(' created connection ');
                        $create="CREATE TABLE IF NOT EXISTS BatchMove (id INT  NOT NULL PRIMARY KEY AUTO_INCREMENT,Device_Id VARCHAR(50),Dealer_Name varchar(50),Vehicle_Id varchar(50),Date_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";
            $renewal="CREATE TABLE IF NOT EXISTS Renewal_Details(ID INT AUTO_INCREMENT PRIMARY KEY,User_Name varchar(50),Licence_Id varchar(50),Vehicle_Id varchar(50),Device_Id varchar(50),Type ENUM('Basic', 'Advance', 'Premium', 'PremiumPlus') NOT NULL,Processed_Date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,Status ENUM('Migration', 'Rename', 'OnBoard','Cancel','Renew') NOT NULL)";
            $result=$conn->query($renewal);
           // log::onfo('Renewal_Details table was created in '.$fcode.'Database'.$result);
                        if($conn->query($create)){
                             $ins="INSERT INTO BatchMove (Device_Id,Vehicle_Id) values ('DEVICE_POWER','VEHICLE_POWER')";
                             $conn->multi_query($ins);
                        }
                        else{
                                log::info('Database not connected...');
                        }
                        }
                }

        /*
                        Mail::queue('emails.welcome', array('fname'=>$fname,'userId'=>$userId,'password'=>$password), function($message)
                        {
                                Log::info("Inside email :" . Input::get ( 'email1' ));

                                $message->to(Input::get ( 'email1' ))->subject('Welcome to VAMO Systems');
                        });
                        */

                        // redirect
                        Session::flash ( 'message', 'Successfully created ' . $fname . '!' );
                        return Redirect::to ( 'vdmFranchises' );
                }
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return Response
         */
        public function show($id) {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $username = Auth::user ()->username;

                $fcode=$id;
                $redis = Redis::connection ();


                /*$franDetails = $redis->hmget ( 'H_Franchise', $fcode.':fname',$fcode.':descrption:',
                                $fcode.':landline',$fcode.':mobileNo1',$fcode.':mobileNo2',
                                $fcode.':email1',$fcode.':email2',$fcode.':userId');*/
            $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                $franchiseDetails=json_decode($franDetails_json,true);
                $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
                if(isset($franchiseDetails['description'])==1)
                        $description=$franchiseDetails['description'];
                else
                        $description='';
                if(isset($franchiseDetails['landline'])==1)
                        $landline=$franchiseDetails['landline'];
                else
                        $landline='';
                if(isset($franchiseDetails['mobileNo1'])==1)
                        $mobileNo1=$franchiseDetails['mobileNo1'];
                else
                        $mobileNo1='';
                // zoho ram
                if(isset($franchiseDetails['zoho'])==1)
                        $zoho=$franchiseDetails['zoho'];
                else
                        $zoho='-';
                if(isset($franchiseDetails['auth'])==1)
                        $auth=$franchiseDetails['auth'];
                else
                        $auth='-';
                //
                if(isset($franchiseDetails['mobileNo2'])==1)
                        $mobileNo2=$franchiseDetails['mobileNo2'];
                else
                        $mobileNo2='';

                if(isset($franchiseDetails['email1'])==1)
                        $email1=$franchiseDetails['email1'];
                else
                        $email1='';
                if(isset($franchiseDetails['email2'])==1)
                        $email2=$franchiseDetails['email2'];
                else
                        $email2='';
                if(isset($franchiseDetails['userId'])==1)
                        $userId=$franchiseDetails['userId'];
                else
                        $userId='';
                if(isset($franchiseDetails['fullAddress'])==1)
                        $fullAddress=$franchiseDetails['fullAddress'];
                else
                        $fullAddress='';
                if(isset($franchiseDetails['otherDetails'])==1)
                        $otherDetails=$franchiseDetails['otherDetails'];
                else
                        $otherDetails='';
                if(isset($franchiseDetails['website'])==1)
                        $website=$franchiseDetails['website'];
                else
                        $website='';
                if(isset($franchiseDetails['website2'])==1)
                        $website2=$franchiseDetails['website2'];
                else
                        $website2='';
                if(isset($franchiseDetails['website3'])==1)
                        $website3=$franchiseDetails['website3'];
                else
                        $website3='';
                if(isset($franchiseDetails['website4'])==1)
                        $website4=$franchiseDetails['website4'];
                else
                        $website4='';
                if(isset($franchiseDetails['trackPage'])==1)
                        $trackPage=$franchiseDetails['trackPage'];
                else
                        $trackPage='Normal View';
                if(isset($franchiseDetails['smsSender'])==1)
                        $smsSender=$franchiseDetails['smsSender'];
                else
                        $smsSender='';
                if(isset($franchiseDetails['smsProvider'])==1)
                        $smsProvider=$franchiseDetails['smsProvider'];
                else
                        $smsProvider='nill';
                if(isset($franchiseDetails['providerUserName'])==1)
                        $providerUserName=$franchiseDetails['providerUserName'];
                else
                        $providerUserName='';
                if(isset($franchiseDetails['providerPassword'])==1)
                        $providerPassword=$franchiseDetails['providerPassword'];
                else
                        $providerPassword='';
                // if(isset($franchiseDetails['eFDSchedular'])==1)
                //      $eFDSchedular=$franchiseDetails['eFDSchedular'];
                // else
                //      $eFDSchedular='';
                if(isset($franchiseDetails['timeZone'])==1)
                        $timeZone=$franchiseDetails['timeZone'];
                else
                        $timeZone='Asia/Kolkata';
                if($timeZone=='')
                        $timeZone='Asia/Kolkata';
                if(isset($franchiseDetails['apiKey'])==1)
                        $apiKey=$franchiseDetails['apiKey'];
                else
                        $apiKey='Nill';
                if(isset($franchiseDetails['apiKey2'])==1)
                        $apiKey2=$franchiseDetails['apiKey2'];
                else
                        $apiKey2='Nill';
                if(isset($franchiseDetails['apiKey3'])==1)
                        $apiKey3=$franchiseDetails['apiKey3'];
                else
                        $apiKey3='Nill';
                if(isset($franchiseDetails['apiKey4'])==1)
                        $apiKey4=$franchiseDetails['apiKey4'];
                else
                        $apiKey4='Nill';
            if(isset($franchiseDetails['mapKey'])==1)
                    $mapKey=$franchiseDetails['mapKey'];
                         else
                    $mapKey='Nill';
        if(isset($franchiseDetails['addressKey'])==1)
                        $addressKey=$franchiseDetails['addressKey'];
                else
                        $addressKey='Nill';
                if(isset($franchiseDetails['notificationKey'])==1)
                        $notificationKey=$franchiseDetails['notificationKey'];
                else
                        $notificationKey=$redis->hget ( 'H_Google_Android_ServerKey',$fcode);
                if(isset($franchiseDetails['backUpDays'])==1)
                        $backUpDays=$franchiseDetails['backUpDays'];
                else
                        $backUpDays='60';
                if(isset($franchiseDetails['dbType'])==1)
                        $dbType=$franchiseDetails['dbType'];
                else
                        $dbType='mysql';
                if(isset($franchiseDetails['gpsvtsApp'])==1)
                        $gpsvtsAppKey=$franchiseDetails['gpsvtsApp'];
                else
                        $gpsvtsAppKey=$redis->hget ( 'H_MobileAppId',$fcode);
                $dbIp=$redis->hget('H_Franchise_Mysql_DatabaseIP',$fcode);
                if($dbIp=='')
                {
                        $dbIp='188.166.244.126';
                }
                //$key = array_search($dbIp, VdmFranchiseController::dbIp());
                //$dbIp=$key;
                Log::info("dbIp..".$dbIp);

                if($prepaid == 'yes'){
                        $numberofBasicLicence=isset($franchiseDetails['numberofBasicLicence'])?$franchiseDetails['numberofBasicLicence']:'0';

                        $numberofAdvanceLicence=isset($franchiseDetails['numberofAdvanceLicence'])?$franchiseDetails['numberofAdvanceLicence']:'0';
                $numberofPremiumLicence=isset($franchiseDetails['numberofPremiumLicence'])?$franchiseDetails['numberofPremiumLicence']:'0';
            $numberofPremPlusLicence=isset($franchiseDetails['numberofPremPlusLicence'])?$franchiseDetails['numberofPremPlusLicence']:'0';
                $availableBasicLicence          =isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
                        $availableAdvanceLicence        =isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
                        $availablePremiumLicence        =isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
            $availablePremPlusLicence =isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
            if($availableBasicLicence==null){
                $availableBasicLicence=0;
            }
            if($availableAdvanceLicence==null){
                $availableAdvanceLicence=0;
            }
            if($availablePremiumLicence==null){
                $availablePremiumLicence=0;
            }
            if($availablePremPlusLicence==null){
                $availablePremPlusLicence=0;
            }
            if($numberofPremPlusLicence==null){
                $numberofPremPlusLicence=0;
            }
            if($numberofPremiumLicence==null){
                $numberofPremiumLicence=0;
            }
            if($numberofAdvanceLicence==null){
                $numberofAdvanceLicence=0;
            }
            if($numberofBasicLicence==null){
                $numberofBasicLicence=0;
            }
                        $numberofLicence=0;
                        $availableLincence=0;
                 }else{
                        $numberofLicence=isset($franchiseDetails['numberofLicence'])?$franchiseDetails['numberofLicence']:'0';
                        $availableLincence=isset($franchiseDetails['availableLincence'])?$franchiseDetails['availableLincence']:'0';
                        $numberofBasicLicence=0;
                        $numberofAdvanceLicence=0;
                        $numberofPremiumLicence=0;
            $numberofPremPlusLicence=0;
                        $availableBasicLicence=0;
                        $availablePremiumLicence=0;
                        $availableAdvanceLicence=0;
            $availablePremPlusLicence=0;
                 }

                 $websites1 = array('1' => $website,
                                                   '2'  =>$website2,
                                                        '3'     => $website3,
                                                        '4'     =>$website4);
                 $websites=implode(",",$websites1);
                 $apiKeys1 = array('1' => $apiKey,
                                                   '2'  =>$apiKey2,
                                                        '3'     => $apiKey3,
                                                        '4'     =>$apiKey4);
                 $apiKeys=implode(",",$apiKeys1);

                //$franchiseDetails=$franDetails;
                log::info($franchiseDetails);
                //return view ( 'vdm.franchise.show', array ('fname' => $franDetails['fname'] ) )->with ( 'fcode', $fcode )->with ( 'franchiseDetails', $franchiseDetails );
                //$count = count($redis->keys ( 'NoData:*' ));
                $apnKey = $redis->exists('Update:Apn');
        $timezoneKey = $redis->exists('Update:Timezone');
                return view ( 'vdm.franchise.show', array (
                                'fname' => $franchiseDetails['fname']
                ) )->with ( 'fcode', $fcode )->with ( 'franchiseDetails', $franchiseDetails )
                ->with('description',$description)
                ->with('landline',$landline)
                ->with('mobileNo1',$mobileNo1)
                ->with('mobileNo2',$mobileNo2)
                ->with('email1',$email1)
                ->with('zoho',$zoho)
                ->with('auth',$auth)
                ->with('email2',$email2)
                ->with('userId',$userId)
                ->with('fullAddress',$fullAddress)
                ->with('otherDetails',$otherDetails)
                ->with('numberofBasicLicence',$numberofBasicLicence)
                ->with('numberofAdvanceLicence',$numberofAdvanceLicence)
                ->with('numberofPremiumLicence',$numberofPremiumLicence)
        ->with('numberofPremPlusLicence',$numberofPremPlusLicence)
                ->with('availableBasicLicence',$availableBasicLicence)
                ->with('availableAdvanceLicence',$availableAdvanceLicence)
                ->with('availablePremiumLicence',$availablePremiumLicence)
        ->with('availablePremPlusLicence',$availablePremPlusLicence)
                ->with('websites',$websites)
                ->with('trackPage',$trackPage)
                ->with('smsSender',$smsSender)
                ->with('smsProvider',$smsProvider)
                ->with('providerUserName',$providerUserName)
                ->with('providerPassword',$providerPassword)
                ->with('timeZone',$timeZone)
                ->with('apiKeys', $apiKeys)
                ->with('mapKey', $mapKey)
                ->with('addressKey', $addressKey)
                ->with('notificationKey', $notificationKey)
                ->with('gpsvtsAppKey',$gpsvtsAppKey)
                ->with('dbIp', $dbIp)
                ->with('dbType', $dbType)
                ->with('backUpDays', $backUpDays)
                ->with('dbIpAr', VdmFranchiseController::dbIp())
                ->with('smsP',VdmFranchiseController::smsP())
            ->with('timeZoneC',VdmFranchiseController::timeZoneC())
        ->with('backType',VdmFranchiseController::backTypeC())
        ->with('numberofLicence',$numberofLicence)
        ->with('availableLincence',$availableLincence)
                ->with('prepaid',$prepaid)
                ->with('apnKey',$apnKey)
                ->with('timezoneKey',$timezoneKey);



        }

public function prePaidCnv($fcode){
        if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                log::info('inside the Normal franchises to prepaid conversion');
                $username = Auth::user ()->username;
                $redis = Redis::connection ();
                $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                $franchiseDetails=json_decode($franDetails_json,true);
                log::info('before convertion');
                log::info($franchiseDetails);
                $fname=isset($franchiseDetails['fname'])?$franchiseDetails['fname']:'';
        $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
        $ablLicence=isset($franchiseDetails['availableLincence'])?$franchiseDetails['availableLincence']:'';
        if($prepaid=='yes'){
            Session::flash ( 'message', $fname.' Franchise is already prepaid');
                    return Redirect::to ( 'vdmFranchises' );
        }

                $details = array (
                                'fname' => isset($franchiseDetails['fname'])?$franchiseDetails['fname']:'',
                                'description' => isset($franchiseDetails['description'])?$franchiseDetails['description']:'',
                                'landline' =>isset($franchiseDetails['landline'])?$franchiseDetails['landline']:'',
                                'mobileNo1' => isset($franchiseDetails['mobileNo1'])?$franchiseDetails['mobileNo1']:'',
                                'mobileNo2' =>isset($franchiseDetails['mobileNo2'])?$franchiseDetails['mobileNo2']:'',
                                'prepaid'       => isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'yes',
                                'email1' => isset($franchiseDetails['email1'])?$franchiseDetails['email1']:'',
                                'email2' => isset($franchiseDetails['email2'])?$franchiseDetails['email2']:'',
                                'userId' => isset($franchiseDetails['userId'])?$franchiseDetails['userId']:'',
                                'fullAddress' => isset($franchiseDetails['fullAddress'])?$franchiseDetails['fullAddress']:'',
                                'otherDetails' => isset($franchiseDetails['otherDetails'])?$franchiseDetails['otherDetails']:'',
                                'numberofBasicLicence'  =>isset($franchiseDetails['numberofBasicLicence'])?$franchiseDetails['numberofBasicLicence']:'0',
                                'numberofAdvanceLicence' => isset($franchiseDetails['numberofAdvanceLicence'])?$franchiseDetails['numberofAdvanceLicence']:'0',
                                'numberofPremiumLicence' => isset($franchiseDetails['numberofPremiumLicence'])?$franchiseDetails['numberofPremiumLicence']:'0',
                'numberofPremPlusLicence' => isset($franchiseDetails['numberofPremPlusLicence'])?$franchiseDetails['numberofPremPlusLicence']:'0',
                                'availableBasicLicence' => isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:$ablLicence,
                                'availableAdvanceLicence' => isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0',
                                'availablePremiumLicence'       =>isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0',
                'availablePremPlusLicence' =>isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0',
                                'website'=>isset($franchiseDetails['website'])?$franchiseDetails['website']:'',
                                'website2'=>isset($franchiseDetails['website2'])?$franchiseDetails['website2']:'',
                                'website3'=>isset($franchiseDetails['website3'])?$franchiseDetails['website3']:'',
                                'website4'=>isset($franchiseDetails['website4'])?$franchiseDetails['website4']:'',
                                'trackPage'=>isset($franchiseDetails['trackPage'])?$franchiseDetails['trackPage']:'',
                                'smsSender'=>isset($franchiseDetails['smsSender'])?$franchiseDetails['smsSender']:'',
                                'smsProvider'=>isset($franchiseDetails['smsProvider'])?$franchiseDetails['smsProvider']:'',
                                'providerUserName'=>isset($franchiseDetails['providerUserName'])?$franchiseDetails['providerUserName']:'',
                                'providerPassword'=>isset($franchiseDetails['providerPassword'])?$franchiseDetails['providerPassword']:'',
                                'timeZone'=>isset($franchiseDetails['timeZone'])?$franchiseDetails['timeZone']:'',
                                'apiKey'=>isset($franchiseDetails['apiKey'])?$franchiseDetails['apiKey']:'',
                                'apiKey2'=>isset($franchiseDetails['apiKey2'])?$franchiseDetails['apiKey2']:'',
                                'apiKey3'=>isset($franchiseDetails['apiKey3'])?$franchiseDetails['apiKey3']:'',
                                'apiKey4'=>isset($franchiseDetails['apiKey4'])?$franchiseDetails['apiKey4']:'',
                                'mapKey'=>isset($franchiseDetails['mapKey'])?$franchiseDetails['mapKey']:'',
                                'addressKey'=>isset($franchiseDetails['addressKey'])?$franchiseDetails['addressKey']:'',
                                'notificationKey'=>isset($franchiseDetails['notificationKey'])?$franchiseDetails['notificationKey']:'',
                                'gpsvtsApp'=>isset($franchiseDetails['gpsvtsApp'])?$franchiseDetails['gpsvtsApp']:'',
                                'backUpDays'=>isset($franchiseDetails['backUpDays'])?$franchiseDetails['backUpDays']:'',
                                'dbType'=>isset($franchiseDetails['dbType'])?$franchiseDetails['dbType']:'',
                                'zoho'=>isset($franchiseDetails['zoho'])?$franchiseDetails['zoho']:'',
                                'auth'=>isset($franchiseDetails['auth'])?$franchiseDetails['auth']:'',
                                'subDomain'=>isset($franchiseDetails['subDomain'])?$franchiseDetails['subDomain']:''
                        );
                $detailsJson = json_encode ( $details );
                $redis->hmset ( 'H_Franchise', $fcode,$detailsJson);
                //databases creation
                $servername=$redis->hget('H_Franchise_Mysql_DatabaseIP',$fcode);
        if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
            return 'Ipaddress Failed !!!';
        }
        $usernamedb  =  "root";
        $password    =  "#vamo123";
        $dbname      =  $fcode;
        $conn  =  mysqli_connect($servername, $usernamedb, $password);
        $create_db = "CREATE DATABASE IF NOT EXISTS $dbname";
        $conn  =  mysqli_connect($servername, $usernamedb, $password, $dbname);
        if( !$conn ) {
            die('Could not connect: ' . mysqli_connect_error());
            return 'Please Update One more time Connection failed';
        }else{
                if(!$conn) {
                        die('Could not connect:'.mysqli_connect_error());
                        return 'Please Update One more time Connection failed';
                }else {
                log::info(' created connection ');
                        $renewal="CREATE TABLE IF NOT EXISTS Renewal_Details(ID INT AUTO_INCREMENT PRIMARY KEY,User_Name varchar(50),Licence_Id varchar(50),Vehicle_Id varchar(50),Device_Id varchar(50),Type ENUM('Basic', 'Advance', 'Premium') NOT NULL,Processed_Date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,Status ENUM('Migration', 'Rename', 'OnBoard','Cancel','Renew') NOT NULL)";
                $result=$conn->query($renewal);
                        }
                        $conn->close();
        }
                $dealerList=$redis->smembers('S_Dealers_'.$fcode);
                foreach ($dealerList as $key => $dealerId) {
                   $detailJson=$redis->hget ( 'H_DealerDetails_'.$fcode, $dealerId);
                   $detail=json_decode($detailJson,true);
                   log::info('before dealer deati to prepaid conversion');
                   log::info($detail);
                   $preOn=$redis->scard('S_Pre_Onboard_Dealer_'.$dealerId.'_'.$fcode);
                   $onb=$redis->scard('S_Vehicles_Dealer_'.$dealerId.'_'.$fcode);
                   $totalLicence=$preOn+$onb;
                   $dealDetail=array(
                                        'email' => isset($detail['email'])?$detail['email']:'',
                                        'mobileNo' => isset($detail['mobileNo'])?$detail['mobileNo']:'',
                                        'website'=>isset($detail['website'])?$detail['website']:'',
                                        'smsSender'=>isset($detail['smsSender'])?$detail['smsSender']:'',
                                        'smsProvider'=>isset($detail['smsProvider'])?$detail['smsProvider']:'',
                                        'providerUserName'=>isset($detail['providerUserName'])?$detail['providerUserName']:'',
                                        'providerPassword'=>isset($detail['providerPassword'])?$detail['providerPassword']:'',
                                        'numofBasic'    =>isset($detail['numofBasic'])?$detail['numofBasic']:'0',
                                        'numofAdvance'  =>isset($detail['numofAdvance'])?$detail['numofAdvance']:'0',
                                        'numofPremium'  =>isset($detail['numofPremium'])?$detail['numofPremium']:'0',
                    'numofPremiumPlus' =>isset($detail['numofPremiumPlus'])?$detail['numofPremiumPlus']:'0',
                                        'avlofBasic'    =>isset($detail['avlofBasic'])?$detail['avlofBasic']:$preOn,
                                        'avlofAdvance'  =>isset($detail['avlofAdvance'])?$detail['avlofAdvance']:'0',
                                        'avlofPremium'  =>isset($detail['avlofPremium'])?$detail['avlofPremium']:'0',
                    'avlofPremiumPlus' =>isset($detail['avlofPremiumPlus'])?$detail['avlofPremiumPlus']:'0',
                                        'zoho'=>isset($detail['zoho'])?$detail['zoho']:'',
                                        'mapKey'=>isset($detail['mapKey'])?$detail['mapKey']:'',
                                        'addressKey'=>isset($detail['addressKey'])?$detail['addressKey']:'',
                                        'notificationKey'=>isset($detail['notificationKey'])?$detail['notificationKey']:'',
                                        'gpsvtsApp'=>isset($detail['gpsvtsApp'])?$detail['gpsvtsApp']:'',
                    'LicenceissuedDate'=>isset($detail['LicenceissuedDate'])?$detail['LicenceissuedDate']:'',
                        );
                        $dealDetail=json_encode($dealDetail);
                        $redis->hset ( 'H_DealerDetails_' . $fcode, $dealerId, $dealDetail );
                        $vehicleListId=$redis->smembers('S_Vehicles_Dealer_'.$dealerId.'_'.$fcode);
            $bc=0;$ad=0;
                        foreach ($vehicleListId as $key => $vehicleId) {
                $exsPre=$redis->sismember('S_Pre_Onboard_Dealer_'.$dealerId.'_'.$fcode,$vehicleId);
                                if($exsPre!=1){
                                $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );

                        $vehicleRefData=json_decode($vehicleRefData,true);
                        //$type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'Advance';
						$type='Basic';
                        log::info('vehicle Id ;'.$vehicleId.' Licence Type'.$type);
                        $onboardDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:'';
                        if($type=='Basic'){
                                $LicenceId =strtoupper('BC'.uniqid());
                                $bc=$bc+1;
                        }else if($type=='Advance'){
                                $LicenceId = strtoupper('AV'.uniqid());
                                $ad=$ad+1;
                        }
                        log::info('vehicle Id ;'.$vehicleId.' generated Licence Id'.$LicenceId);
                        $deviceid=$redis->hget('H_Vehicle_Device_Map_' . $fcode,$vehicleId);
                        $LicenceissuedDate=$onboardDate;
                        $LicenceOnboardDate=$onboardDate;
                        $toDate = strtotime($onboardDate);
                                $LicenceExpiryDate = date("d-m-Y", strtotime("+12 month", $toDate));
                        log::info('LicenceExpiryDate_'.$LicenceExpiryDate);
                        $LiceneceDataArr=array(
                        'LicenceissuedDate'=>$LicenceissuedDate,
                        'LicenceOnboardDate' =>$LicenceOnboardDate,
                        'LicenceExpiryDate' =>$LicenceExpiryDate,
                        );
                        $LicenceExpiryJson = json_encode ( $LiceneceDataArr );
                $redis->hset ( 'H_LicenceExpiry_' . $fcode, $LicenceId,$LicenceExpiryJson);
                $redis->hset('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId,$vehicleId);
                $redis->hset('H_Vehicle_LicenceId_Map_'.$fcode,$vehicleId,$LicenceId);
                $redis->sadd('S_LicenceList_'.$fcode,$LicenceId);
                $redis->sadd('S_LicenceList_dealer_'.$dealerId.'_'.$fcode,$LicenceId);
                 try {
                                $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
                                                $servername = $franchiesJson;
                                                //$servername="209.97.163.4";
                                                if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
                                                return 'Ipaddress Failed !!!';
                                        }
                                        $usernamedb = "root";
                                        $password = "#vamo123";
                                        $dbname = $fcode;
                                        $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
                                        if( !$conn ) {
                                        die('Could not connect: ' . mysqli_connect_error());
                                        return 'Please Update One more time Connection failed';
                                        } else {
                                        log::info('inside mysql to onboard');
                                                        $ins="INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$dealerId','$LicenceId','$vehicleId','$deviceid','$type','OnBoard')";
                                        if($conn->multi_query($ins)){
                                                log::info('Successfully inserted');
                                        }else{
                                                log::info('not inserted');
                                        }
                                                        $conn->close();
                                        }

                } catch (Exception $e) {
                                log::info('Mysql issues');
                }
                        }}
                        $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode,$dealerId );
            $dealersDetails=json_decode($dealersDetails_json,true);
            $dealersDetails['numofBasic']= $dealersDetails['numofBasic']+$bc;
            $dealersDetails['numofAdvance']=$dealersDetails['numofAdvance']+$ad;
            $detailsJsonDeal = json_encode ( $dealersDetails );
                $redis->hmset ( 'H_DealerDetails_'.$fcode,$dealerId,$detailsJsonDeal);
            log::info('after dealer deati to prepaid conversion');
                        log::info($detailsJsonDeal);
                }

                $adminVehId=$redis->smembers('S_Vehicles_Admin_'.$fcode);
                $adBc=0;$adAd=0;
                $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                $franchiseDetails=json_decode($franDetails_json,true);
                $userId=isset($franchiseDetails['userId'])?$franchiseDetails['userId']:'';
                foreach ($adminVehId as $key => $vehicle) {
                        $deviceid=$redis->hget('H_Vehicle_Device_Map_' . $fcode,$vehicle);
                        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicle );
                $vehicleRefData=json_decode($vehicleRefData,true);
                $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'Advance';
                $onboardDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:'';
                if($type=='Basic'){
                        $LicenceId =strtoupper('BC'.uniqid());
                        $adBc=$adBc+1;
                }else if($type=='Advance'){
                        $LicenceId = strtoupper('AV'.uniqid());
                        $adAd=$adAd+1;
                }
                log::info('vehicle Id ;'.$vehicle.' generated Licence Id'.$LicenceId);
                $LicenceissuedDate=$onboardDate;
                $LicenceOnboardDate=$onboardDate;
                $data = str_replace('-', '/', $onboardDate);
                $LicenceExpiryDate=date('m-d-Y',strtotime($data . "+ 1 year"));
                $LiceneceDataArr=array(
                'LicenceissuedDate'=>$LicenceissuedDate,
                'LicenceOnboardDate' =>$LicenceOnboardDate,
                'LicenceExpiryDate' =>$LicenceExpiryDate,
                );
                $LiceneceDataArr=json_encode ($LiceneceDataArr);
                $redis->hset('H_LicenceExpiry_'.$fcode,$LicenceId,$LiceneceDataArr);
                $redis->sadd('S_LicenceList_admin_'.$fcode,$LicenceId);
                $redis->sadd('S_LicenceList_'.$fcode,$LicenceId);
                $redis->hset('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId,$vehicle);
            $redis->hset('H_Vehicle_LicenceId_Map_'.$fcode,$vehicle,$LicenceId);
            try {
                $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
                                $servername = $franchiesJson;
                                //$servername="209.97.163.4";
                                if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
                                return 'Ipaddress Failed !!!';
                        }
                        $usernamedb = "root";
                        $password = "#vamo123";
                        $dbname = $fcode;
                        $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
                         if( !$conn ) {
                        die('Could not connect: ' . mysqli_connect_error());
                        return 'Please Update One more time Connection failed';
                        } else {
                         log::info('inside mysql to onboard');
                                         $ins="INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$userId','$LicenceId','$vehicle','$deviceid','$type','OnBoard')";
                        if($conn->multi_query($ins)){
                                log::info('Successfully inserted');
                        }else{
                                log::info('not inserted');
                        }
                                        $conn->close();
                        }
            } catch (Exception $e) {
                log::info('Mysql issues');

            }

                }
                $ablLicence=$ablLicence+$adBc;
                $franchiseDetails['numberofBasicLicence']=$franchiseDetails['numberofBasicLicence']+$ablLicence;
                $franchiseDetails['numberofAdvanceLicence']=$franchiseDetails['numberofAdvanceLicence']+$adAd;
                $detailsJson = json_encode ( $franchiseDetails );
        $redis->hmset ( 'H_Franchise', $fcode,$detailsJson);

                Session::flash ( 'message', $fname.' Franchise has been Coverted into Prepaid Franchise !');
                return Redirect::to ( 'vdmFranchises' );
}


        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return Response
         */
        public function edit($id) {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $username = Auth::user ()->username;

                $redis = Redis::connection ();
                $fcode = $id;

                /*$franchiseDetails = $redis->hmget ( 'H_Franchise', $fcode.':fname',$fcode.':description',
                                $fcode.':landline',$fcode.':mobileNo1',$fcode.':mobileNo2',
                                $fcode.':email1',$fcode.':email2',$fcode.':userId',$fcode.':fullAddress',$fcode.':otherDetails');*/


                $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                                $franchiseDetails=json_decode($franDetails_json,true);
                $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
                if(isset($franchiseDetails['description'])==1)
                        $description=$franchiseDetails['description'];
                else
                        $description='';
                if(isset($franchiseDetails['landline'])==1)
                        $landline=$franchiseDetails['landline'];
                else
                        $landline='';
                if(isset($franchiseDetails['mobileNo1'])==1)
                        $mobileNo1=$franchiseDetails['mobileNo1'];
                else
                        $mobileNo1='';
                // zoho ram
                if(isset($franchiseDetails['zoho'])==1)
                        $zoho=$franchiseDetails['zoho'];
                else
                        $zoho='';
                if(isset($franchiseDetails['auth'])==1)
                        $auth=$franchiseDetails['auth'];
                else
                        $auth='';
                //
                if(isset($franchiseDetails['mobileNo2'])==1)
                        $mobileNo2=$franchiseDetails['mobileNo2'];
                else
                        $mobileNo2='';

                if(isset($franchiseDetails['email1'])==1)
                        $email1=$franchiseDetails['email1'];
                else
                        $email1='';
                if(isset($franchiseDetails['email2'])==1)
                        $email2=$franchiseDetails['email2'];
                else
                        $email2='';
                if(isset($franchiseDetails['userId'])==1)
                        $userId=$franchiseDetails['userId'];
                else
                        $userId='';
                if(isset($franchiseDetails['fullAddress'])==1)
                        $fullAddress=$franchiseDetails['fullAddress'];
                else
                        $fullAddress='';
                if(isset($franchiseDetails['otherDetails'])==1)
                        $otherDetails=$franchiseDetails['otherDetails'];
                else
                        $otherDetails='';
                if(isset($franchiseDetails['website'])==1)
                        $website=$franchiseDetails['website'];
                else
                        $website='';
                if(isset($franchiseDetails['website2'])==1)
                        $website2=$franchiseDetails['website2'];
                else
                        $website2='';
                if(isset($franchiseDetails['website3'])==1)
                        $website3=$franchiseDetails['website3'];
                else
                        $website3='';
                if(isset($franchiseDetails['website4'])==1)
                        $website4=$franchiseDetails['website4'];
                else
                        $website4='';
                if(isset($franchiseDetails['trackPage'])==1)
                        $trackPage=$franchiseDetails['trackPage'];
                else
                        $trackPage='Normal View';
                if(isset($franchiseDetails['smsSender'])==1)
                        $smsSender=$franchiseDetails['smsSender'];
                else
                        $smsSender='';
                if(isset($franchiseDetails['smsProvider'])==1)
                        $smsProvider=$franchiseDetails['smsProvider'];
                else
                        $smsProvider='nill';
                if(isset($franchiseDetails['providerUserName'])==1)
                        $providerUserName=$franchiseDetails['providerUserName'];
                else
                        $providerUserName='';
                if(isset($franchiseDetails['providerPassword'])==1)
                        $providerPassword=$franchiseDetails['providerPassword'];
                else
                        $providerPassword='';
                // if(isset($franchiseDetails['eFDSchedular'])==1)
                //      $eFDSchedular=$franchiseDetails['eFDSchedular'];
                // else
                //      $eFDSchedular='';
                if(isset($franchiseDetails['timeZone'])==1)
                        $timeZone=$franchiseDetails['timeZone'];
                else
                        $timeZone='Asia/Kolkata';
                if($timeZone=='')
                        $timeZone='Asia/Kolkata';
                if(isset($franchiseDetails['apiKey'])==1)
                        $apiKey=$franchiseDetails['apiKey'];
                else
                        $apiKey='';
                if(isset($franchiseDetails['apiKey2'])==1)
                        $apiKey2=$franchiseDetails['apiKey2'];
                else
                        $apiKey2='';
                if(isset($franchiseDetails['apiKey3'])==1)
                        $apiKey3=$franchiseDetails['apiKey3'];
                else
                        $apiKey3='';
                if(isset($franchiseDetails['apiKey4'])==1)
                        $apiKey4=$franchiseDetails['apiKey4'];
                else
                        $apiKey4='';
            if(isset($franchiseDetails['mapKey'])==1)
                    $mapKey=$franchiseDetails['mapKey'];
                         else
                    $mapKey='';
        if(isset($franchiseDetails['addressKey'])==1)
                        $addressKey=$franchiseDetails['addressKey'];
                else
                        $addressKey='';
                if(isset($franchiseDetails['notificationKey'])==1)
                        $notificationKey=$franchiseDetails['notificationKey'];
                else
                        $notificationKey=$redis->hget ( 'H_Google_Android_ServerKey',$fcode);
                if(isset($franchiseDetails['backUpDays'])==1)
                        $backUpDays=$franchiseDetails['backUpDays'];
                else
                        $backUpDays='60';
                if(isset($franchiseDetails['dbType'])==1)
                        $dbType=$franchiseDetails['dbType'];
                else
                        $dbType='mysql';
                if(isset($franchiseDetails['gpsvtsApp'])==1)
                        $gpsvtsAppKey=$franchiseDetails['gpsvtsApp'];
                else
                        $gpsvtsAppKey=$redis->hget ( 'H_MobileAppId',$fcode);
                if(isset($franchiseDetails['subDomain'])==1)
                        $subDomain=$franchiseDetails['subDomain'];
                else
                        $subDomain='';
                $dbIp=$redis->hget('H_Franchise_Mysql_DatabaseIP',$fcode);
                if($dbIp=='')
                {
                        $dbIp='188.166.244.126';
                }
                $key = array_search($dbIp, VdmFranchiseController::dbIp());
                $dbIp=$key;
                Log::info("dbIp..".$dbIp);

                if($prepaid == 'yes'){
                        $numberofBasicLicence=isset($franchiseDetails['numberofBasicLicence'])?$franchiseDetails['numberofBasicLicence']:'0';

                        $numberofAdvanceLicence=isset($franchiseDetails['numberofAdvanceLicence'])?$franchiseDetails['numberofAdvanceLicence']:'0';
                $numberofPremiumLicence=isset($franchiseDetails['numberofPremiumLicence'])?$franchiseDetails['numberofPremiumLicence']:'0';
            $numberofPremPlusLicence=isset($franchiseDetails['numberofPremPlusLicence'])?$franchiseDetails['numberofPremPlusLicence']:'0';
                $availableBasicLicence          =isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
                        $availableAdvanceLicence        =isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
                        $availablePremiumLicence        =isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
            $availablePremPlusLicence =isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
            if($availableBasicLicence==null){
                $availableBasicLicence=0;
            }
            if($availableAdvanceLicence==null){
                $availableAdvanceLicence=0;
            }
            if($availablePremiumLicence==null){
                $availablePremiumLicence=0;
            }
            if($availablePremPlusLicence==null){
                $availablePremPlusLicence=0;
            }
            if($numberofPremPlusLicence==null){
                $numberofPremPlusLicence=0;
            }
            if($numberofPremiumLicence==null){
                $numberofPremiumLicence=0;
            }
            if($numberofAdvanceLicence==null){
                $numberofAdvanceLicence=0;
            }
            if($numberofBasicLicence==null){
                $numberofBasicLicence=0;
            }
                        $numberofLicence=0;
                        $availableLincence=0;
                 }else{
                        $numberofLicence=isset($franchiseDetails['numberofLicence'])?$franchiseDetails['numberofLicence']:'0';
                        $availableLincence=isset($franchiseDetails['availableLincence'])?$franchiseDetails['availableLincence']:'0';
                        $numberofBasicLicence=0;
                        $numberofAdvanceLicence=0;
                        $numberofPremiumLicence=0;
            $numberofPremPlusLicence=0;
                        $availableBasicLicence=0;
                        $availablePremiumLicence=0;
                        $availableAdvanceLicence=0;
            $availablePremPlusLicence=0;
                 }


                //$count = count($redis->keys ( 'NoData:*' ));
                $apnKey = $redis->exists('Update:Apn');
        $timezoneKey = $redis->exists('Update:Timezone');

                return view ( 'vdm.franchise.edit', array (
                                'fname' => $franchiseDetails['fname']
                ) )->with ( 'fcode', $fcode )->with ( 'franchiseDetails', $franchiseDetails )
                ->with('description',$description)
                ->with('landline',$landline)
                ->with('mobileNo1',$mobileNo1)
                ->with('mobileNo2',$mobileNo2)
                ->with('email1',$email1)
                ->with('zoho',$zoho)
                ->with('auth',$auth)
                ->with('email2',$email2)
                ->with('userId',$userId)
                ->with('fullAddress',$fullAddress)
                ->with('otherDetails',$otherDetails)
                ->with('numberofBasicLicence',$numberofBasicLicence)
                ->with('numberofAdvanceLicence',$numberofAdvanceLicence)
                ->with('numberofPremiumLicence',$numberofPremiumLicence)
        ->with('numberofPremPlusLicence',$numberofPremPlusLicence)
                ->with('availableBasicLicence',$availableBasicLicence)
                ->with('availableAdvanceLicence',$availableAdvanceLicence)
                ->with('availablePremiumLicence',$availablePremiumLicence)
        ->with('availablePremPlusLicence',$availablePremPlusLicence)
                ->with('website',$website)
                ->with('website2',$website2)
                ->with('website3',$website3)
                ->with('website4',$website4)
                ->with('trackPage',$trackPage)
                ->with('smsSender',$smsSender)
                ->with('smsProvider',$smsProvider)
                ->with('providerUserName',$providerUserName)
                ->with('providerPassword',$providerPassword)
                ->with('timeZone',$timeZone)
                ->with('apiKey', $apiKey)
                ->with('apiKey2', $apiKey2)
                ->with('apiKey3', $apiKey3)
                ->with('apiKey4', $apiKey4)
                ->with('mapKey', $mapKey)
                ->with('addressKey', $addressKey)
                ->with('notificationKey', $notificationKey)
                ->with('gpsvtsAppKey',$gpsvtsAppKey)
                ->with('dbIp', $dbIp)
                ->with('dbType', $dbType)
                ->with('backUpDays', $backUpDays)
                ->with('dbIpAr', VdmFranchiseController::dbIp())
                ->with('smsP',VdmFranchiseController::smsP())
            ->with('timeZoneC',VdmFranchiseController::timeZoneC())
        ->with('backType',VdmFranchiseController::backTypeC())
        ->with('numberofLicence',$numberofLicence)
        ->with('availableLincence',$availableLincence)
        ->with('numberofLicenceO',$numberofLicence)
                ->with('availableLincenceO',$availableLincence)
                ->with('prepaid',$prepaid)
                ->with('timezoneKey',$timezoneKey)
                ->with('apnKey',$apnKey)
                ->with('subDomain',$subDomain);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param int $id
         * @return Response
         */
        public function update($id) {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $fcode = $id;
                $username = Auth::user ()->username;
                $redis = Redis::connection ();

                $rules = array (

                                'description' => 'required',
                                'fullAddress' => 'required',
                                'landline' => 'required',
                                'mobileNo1' => 'required',
                                'email1' => 'required|email',
                                'email2' => 'required|email',
                                'website' => 'required',
                                'trackPage' => 'required'
                );
                $validator = Validator::make ( Input::all (), $rules );


                if ($validator->fails ()) {
                        Log::info(" failed ");
                        Session::flash ( 'message', 'Update failed. Please enter all required fields. !' );

                //      return Redirect::to ( 'vdmFranchises' )->withErrors ( $validator );

                        return Redirect::to ( 'vdmFranchises/'.$fcode.'/edit' )->withErrors ( $validator );
                }
                else {
                        // store

                        $dbIpindex=Input::get ( 'ipadd' );
                        $dbIpAr=VdmFranchiseController::dbIp();
                        $dbIp=$dbIpAr[$dbIpindex];
                        $description = Input::get ( 'description' );
                        $fullAddress = Input::get ( 'fullAddress' );
                        $landline = Input::get ( 'landline' );
                        $mobileNo1 = Input::get ( 'mobileNo1' );
                        $zoho = Input::get ('zoho');
                        $auth =Input::get ('auth');
                        $mobileNo2 = Input::get ( 'mobileNo2' );
                        $email1 = Input::get ( 'email1' );
                        $email2 = Input::get ( 'email2' );
                        $otherDetails = Input::get ('otherDetails');
                        $website= Input::get ('website');
                        $website2= Input::get ('website2');
                        $website3= Input::get ('website3');
                        $website4= Input::get ('website4');
                        $trackPage= Input::get ('trackPage');
                        $smsSender=Input::get ('smsSender');
                        $smsProvider=Input::get ('smsProvider');
                        $providerUserName=Input::get ('providerUserName');
                        $providerPassword=Input::get ('providerPassword');
                        $timeZone=Input::get ('timeZone');
                        $apiKey=Input::get('apiKey');
                        $apiKey2=Input::get('apiKey2');
                        $apiKey3=Input::get('apiKey3');
                        $apiKey4=Input::get('apiKey4');
                        $mapKey=Input::get('mapKey');
                        $addressKey=Input::get('addressKey');
                        $notificationKey=Input::get('notificationKey');
                        $gpsvtsAppKey=Input::get('gpsvtsAppKey');
                        $backUpDays=Input::get('backUpDays');
                        $dbType=Input::get('dbType');
                        $subDomain=Input::get('subDomain');
                        $redis = Redis::connection ();

                        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                        $franchiseDetails=json_decode($franDetails_json,true);

                        $userId = $franchiseDetails['userId'];
                        $fname =$franchiseDetails['fname'];
                        if(($website==$website2&&($website!=null&&$website2!=null)) || ($website==$website3&&($website!=null&&$website3!=null))|| ($website==$website4&&($website!=null&&$website4!=null)) || ($website2==$website3&&($website2!=null&&$website3!=null)) || ($website2==$website4&&($website2!=null&&$website4!=null)) || ($website3==$website4&&($website3!=null&&$website4!=null)))
                        {
                                return Redirect::to ( 'vdmFranchises/'.$fcode.'/edit' )->withInput()->withErrors('Website names should not be same');
                        }
                        if(($apiKey==$apiKey2&&($apiKey!=null&&$apiKey2!=null)) || ($apiKey==$apiKey3&&($apiKey!=null&&$apiKey3!=null)) || ($apiKey==$apiKey4&&($apiKey!=null&&$apiKey4!=null)) || ($apiKey2==$apiKey3&&($apiKey2!=null&&$apiKey3!=null)) || ($apiKey2==$apiKey4&&($apiKey2!=null&&$apiKey4!=null)) || ($apiKey3==$apiKey4&&($apiKey3!=null&&$apiKey4!=null)))
                        {
                                return Redirect::to ( 'vdmFranchises/'.$fcode.'/edit' )->withInput()->withErrors('Map / API keys should not be same');
                        }
                        $pattern = "/[^0-9\,]/";
                        if ((preg_match($pattern, $mobileNo1)) || (preg_match($pattern, $mobileNo2))) {
                                return Redirect::to ( 'vdmFranchises/'.$fcode.'/edit' )->withInput()->withErrors('Only Numeric values are allowed in Mobile Numbers');
                        }
                        $prepaid=Input::get('prepaid');
                        if($prepaid == 'yes'){
                                $addBasicLicence=Input::get ('addBasicLicence');
                                $addAdvanceLicence=Input::get ('addAdvanceLicence');
                                $addPremiumLicence=Input::get ('addPremiumLicence');
                $addPrePlusLicence=Input::get ('addPrePlusLicence');

                                $availableBasicLicence=Input::get ('availableBasicLicence');
                                $availableAdvanceLicence=Input::get ('availableAdvanceLicence');
                                $availablePremiumLicence=Input::get ('availablePremiumLicence');

                                //basic
                                if($addBasicLicence==null){
                                        $numberofBasicLicence=Input::get ('numberofBasicLicence');
                                        $availableBasicLicence=Input::get ('availableBasicLicence');
                                }else if($addBasicLicence>0) {
                   $numberofBasicLicence=Input::get('numberofBasicLicence')+$addBasicLicence;
                  $availableBasicLicence=Input::get('availableBasicLicence')+$addBasicLicence;
                        }else{
                        $avl=Input::get('availableBasicLicence');
                        $abl=(-1)*$addBasicLicence;
                        if($avl >= $abl){
                                $numberofBasicLicence=Input::get('numberofBasicLicence')-$abl;
                                $availableBasicLicence=$avl-$abl;
                        }else{
                                return Redirect::back()->withErrors ( 'Please enter valid Basic Licence !' )->withInput();
                        }
                }
                         //advance
                                if($addAdvanceLicence==null){
                                        $numberofAdvanceLicence=Input::get ('numberofAdvanceLicence');
                                        $availableAdvanceLicence=Input::get ('availableAdvanceLicence');
                                }else if($addAdvanceLicence>0) {
                         $numberofAdvanceLicence=Input::get('numberofAdvanceLicence')+$addAdvanceLicence;
                         $availableAdvanceLicence=Input::get('availableAdvanceLicence')+$addAdvanceLicence;
                        }else {
                        $avl=Input::get('availableAdvanceLicence');
                        $aal=(-1)*$addAdvanceLicence;
                        if($avl >= $aal){
                                $numberofAdvanceLicence=Input::get('numberofAdvanceLicence')-$aal;
                                $availableAdvanceLicence=$avl-$aal;
                        }else{
                                return Redirect::back()->withErrors ( 'Please enter valid Advance Licence !' )->withInput();
                        }
                         }
                         //Premium
                                if($addPremiumLicence==null){
                                        $numberofPremiumLicence=Input::get ('numberofPremiumLicence');
                                        $availablePremiumLicence=Input::get ('availablePremiumLicence');
                                }else if($addPremiumLicence>0) {
                        $numberofPremiumLicence=Input::get('numberofPremiumLicence')+$addPremiumLicence;
                    $availablePremiumLicence=Input::get('availablePremiumLicence')+$addPremiumLicence;
                        }else {
                        $avl=Input::get('availablePremiumLicence');
                        $apl=(-1)*$addPremiumLicence;
                        if($avl >= $apl){
                                $numberofPremiumLicence=Input::get('numberofPremiumLicence')-$apl;
                                $availablePremiumLicence=$avl-$apl;
                        }else{
                                return Redirect::back()->withErrors ( 'Please enter valid Premium Licence !' )->withInput();
                        }
                }

                //Premium Plus
               if($addPrePlusLicence==null){
                    $numberofPremPlusLicence=Input::get ('numberofPremPlusLicence');
                    $availablePremPlusLicence=Input::get ('availablePremPlusLicence');
               }else if($addPrePlusLicence>0) {
                    $numberofPremPlusLicence=Input::get('numberofPremPlusLicence')+$addPrePlusLicence;
                    $availablePremPlusLicence=Input::get('availablePremPlusLicence')+$addPrePlusLicence;
               }else {
                    $avl=Input::get('availablePremPlusLicence');
                    $apl=(-1)*$addPrePlusLicence;
                    if($avl >= $apl){
                        $numberofPremPlusLicence=Input::get('numberofPremPlusLicence')-$apl;
                        $availablePremPlusLicence=$avl-$apl;
                    }else{
                        return Redirect::back()->withErrors ( 'Please enter valid Premium Licence !' )->withInput();
                    }
              }
            if($availableBasicLicence==null){
                $availableBasicLicence=0;
            }
            if($availableAdvanceLicence==null){
                $availableAdvanceLicence=0;
            }
            if($availablePremiumLicence==null){
                $availablePremiumLicence=0;
            }
            if($availablePremPlusLicence==null){
                $availablePremPlusLicence=0;
            }
            if($numberofPremPlusLicence==null){
                $numberofPremPlusLicence=0;
            }
            if($numberofPremiumLicence==null){
                $numberofPremiumLicence=0;
            }
            if($numberofAdvanceLicence==null){
                $numberofAdvanceLicence=0;
            }
            if($numberofBasicLicence==null){
                $numberofBasicLicence=0;
            }

                //prepaid store
                $details = array (
                                        'fname' => $fname,
                                        'description' => $description,
                                        'landline' => $landline,
                                        'mobileNo1' => $mobileNo1,
                                        'zoho' => $zoho,
                                        'auth' => $auth,
                                        'mobileNo2' => $mobileNo2,
                                        'email1' => $email1,
                                        'email2' => $email2,
                                        'userId' => $userId,
                                        'fullAddress' => $fullAddress,
                                        'otherDetails' => $otherDetails,
                                        'numberofBasicLicence'  => $numberofBasicLicence,
                                        'availableBasicLicence' => $availableBasicLicence,
                                        'numberofAdvanceLicence'=> $numberofAdvanceLicence,
                                        'availableAdvanceLicence' => $availableAdvanceLicence,
                                        'numberofPremiumLicence'  => $numberofPremiumLicence,
                                        'availablePremiumLicence' => $availablePremiumLicence,
                    'numberofPremPlusLicence' => $numberofPremPlusLicence,
                    'availablePremPlusLicence'  =>$availablePremPlusLicence,
                                        'website'=>$website,
                                        'website2'=>$website2,
                                        'website3'=>$website3,
                                        'website4'=>$website4,
                                        'trackPage'=>$trackPage,
                                        'smsSender'=>$smsSender,
                                        'smsProvider'=>$smsProvider,
                                        'providerUserName'=>$providerUserName,
                                        'providerPassword'=>$providerPassword,
                                        //'eFDSchedular'=>$eFDSchedular,
                                        'timeZone'=>$timeZone,
                                        'apiKey'=>$apiKey,
                                        'apiKey2'=>$apiKey2,
                                        'apiKey3'=>$apiKey3,
                                        'apiKey4'=>$apiKey4,
                                        'mapKey'=>$mapKey,
                                        'addressKey'=>$addressKey,
                                        'notificationKey'=>$notificationKey,
                                        'gpsvtsApp'=>$gpsvtsAppKey,
                                        'backUpDays'=>$backUpDays,
                                        'dbType'=>$dbType,
                                        'prepaid' =>$prepaid,
                                         'subDomain'=>$subDomain,
                                );
                        }else{
                                $numberofLicence = Input::get ('addLicence');

                                if($numberofLicence==null){
                                        $numberofLicence=0;
                                        $availableLincence=Input::get('availableLincenceO');
                    $numberofLicence=Input::get('numberofLicenceO');
                                }else if($numberofLicence>0) {
                                   log::info('--------------inside add-----------');
                    $availableLincence=Input::get('availableLincenceO')+$numberofLicence;
                    $numberofLicence=Input::get('numberofLicenceO')+$numberofLicence;
                    }
                                else if($numberofLicence<0){
                                  log::info('--------------inside remove-----------');
                  $noofLic=(-1)*$numberofLicence;
                                   if(Input::get('availableLincenceO')<$noofLic){
                     if(Input::get('availableLincenceO')== 0){
                                        Session::flash ( 'message', 'Update Failed !' );
                                    return Redirect::to ( 'vdmFranchises' );
                                         } else{
                                                $availableLic=(Input::get('availableLincenceO')-Input::get('availableLincenceO'));
                                                 $numberofLic=(Input::get('numberofLicenceO')-Input::get('availableLincenceO'));
                     }
                   }
                                   if(Input::get('availableLincenceO')>=$noofLic){
                                          $availableLic=Input::get('availableLincenceO')-$noofLic;
                      $numberofLic=Input::get('numberofLicenceO')-$noofLic;
                    }
                    if($availableLic>=0){
                                         $availableLincence=$availableLic;
                        }else {
                                        $availableLincence=0;
                                        }
                                        if($numberofLic>=0) {
                        $numberofLicence=$numberofLic;
                     }else {
                                          $numberofLicence=0;
                                         }
                                }

                                //normal store

                                $details = array (
                                        'fname' => $fname,
                                        'description' => $description,
                                        'landline' => $landline,
                                        'mobileNo1' => $mobileNo1,
                                        'zoho' => $zoho,
                                        'auth' => $auth,
                                        'mobileNo2' => $mobileNo2,
                                        'email1' => $email1,
                                        'email2' => $email2,
                                        'userId' => $userId,
                                        'fullAddress' => $fullAddress,
                                        'otherDetails' => $otherDetails,
                                        'numberofLicence' => $numberofLicence,
                                        'availableLincence'=>$availableLincence,
                                        'website'=>$website,
                                        'website2'=>$website2,
                                        'website3'=>$website3,
                                        'website4'=>$website4,
                                        'trackPage'=>$trackPage,
                                        'smsSender'=>$smsSender,
                                        'smsProvider'=>$smsProvider,
                                        'providerUserName'=>$providerUserName,
                                        'providerPassword'=>$providerPassword,
                                        //'eFDSchedular'=>$eFDSchedular,
                                        'timeZone'=>$timeZone,
                                        'apiKey'=>$apiKey,
                                        'apiKey2'=>$apiKey2,
                                        'apiKey3'=>$apiKey3,
                                        'apiKey4'=>$apiKey4,
                                        'mapKey'=>$mapKey,
                                        'addressKey'=>$addressKey,
                                        'notificationKey'=>$notificationKey,
                                        'gpsvtsApp'=>$gpsvtsAppKey,
                                        'backUpDays'=>$backUpDays,
                                        'dbType'=>$dbType,
                                        'prepaid'=> $prepaid,
                                         'subDomain'=>$subDomain,

                        );


                        }

      $geolocation=VdmFranchiseController::geocode($fullAddress);
          if($geolocation !=null){
                $LatLong=implode(":",$geolocation);
          }else{
                  $LatLong='0:0';
          }
      log::info('Franchise LatLong '.$LatLong);
      $redis->hset('H_Franchise_LatLong',$fcode,$LatLong);



                                /*if($numberofLicence<Session::get('available'))
                                {
                                        log::info('--------------inside less value-----------');
                                        return Redirect::to ( 'vdmFranchises/update' )->withErrors ( 'Please check the License count' );
                                }
                                else{*/

                                //}

                        // $refDataArr = array('regNo'=>$regNo,'vehicleMake'=>$vehicleMake,'vehicleType'=>$vehicleType,'oprName'=>$oprName,
                        // 'mobileNo'=>$mobileNo,'vehicleCap'=>$vehicleCap,'deviceModel'=>$deviceModel);

                        $getOldIp       = $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);


                        $val = $redis->sadd('S_Franchises',$fcode);

                        log::info(" redis return code :"+$val);
                        //TODO
                        /**
                        * If code is not unique this method fail and return - suggesting
                        * that the ID should be unique.
                        *
                        * Possible improvement..implement ajax call - verify the code while typing itself
                        *
                        */


                        /*$redis->hmset ( 'H_Franchise', $fcode.':description',$description,
                                        $fcode.':landline',$landline,$fcode.':mobileNo1',$mobileNo1,$fcode.':mobileNo2',$mobileNo2,
                                        $fcode.':email1',$email1,$fcode.':email2',$email2);*/


                        $detailsJson = json_encode ( $details );
                        log::info($detailsJson);
                        log::info($apiKey);
                        $redis->hmset('H_Franchise_Mysql_DatabaseIP',$fcode,$dbIp);
                        $redis->hmset ( 'H_Franchise', $fcode,$detailsJson);

            log::info('before table update');
            try{
                            $mysqlDetails =array();
                            $status =array();
                                $status=array(
                                            'username'=>$username,
                                            'status' => Config::get('constant.updated'),
                                                'fcode' => $fcode,
                                                'addBasicLicence'=> Input::get ('addBasicLicence'),
                                            'addAdvanceLicence'=>Input::get ('addAdvanceLicence'),
                                                'addPremiumLicence'=>Input::get ('addPremiumLicence'),
                                'addPrePlusLicence'=>Input::get ('addPrePlusLicence'),
                                                'addLicence'=>Input::get ('addLicence'),
                                                'userIpAddress'=>Session::get('userIP'),

                                        );
                                        $mysqlDetails   = array_merge($status,$details);
                            $modelname = new AuditFrans();
                                    $table = $modelname->getTable();
                                        //return $table;
                                        //return $mysqlDetails;
                                        $db=Config::get('constant.VAMOSdb');
                                    AuditTables::ChangeDB($db);
                                        $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
                                        //return $tableExist;
                                        if(count($tableExist)>0){
                                                AuditFrans::create($mysqlDetails);
                                        }
                                        else{
                                                AuditTables::CreateAuditFrans();
                                                AuditFrans::create($mysqlDetails);
                                        }
                                }
                                catch (Exception $e) {
                                  log::info('Error inside Audit_Frans Update'.$e->getMessage());
                                  log::info($mysqlDetails);
                                }
                        //AuditFrans::create($details);
                        log::info('successfully updated into table Audit_Frans ');

                        $redis->hmset('H_Fcode_Timezone_Schedular',$fcode,$timeZone);

                        $redis->sadd ( 'S_Users_' . $fcode, $userId );
                        $redis->hmset ( 'H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo',
                                         $mobileNo1,$userId.':email',$email1 );
                        $redis->hset ( 'H_Google_Android_ServerKey',$fcode, $notificationKey );
            $redis->hset ( 'H_MobileAppId',$fcode, $gpsvtsAppKey );

                        DB::table('users')
                        ->where('username', $userId)
                        ->update(array('email' => $email1));

                        $mappingFn = array (
                                        'fname' => 'Franchise Name',
                                        'description' => 'Description',
                                        'landline' => 'Landline Number',
                                        'mobileNo1' => 'Mobile Number1',
                                        'zoho' => 'Zoho Organisation',
                                        'auth' => 'Zoho Authentication',
                                        'mobileNo2' => 'Mobile Number2',
                                        'email1' => 'Email 1',
                                        'email2' => 'Email 2',
                                        'userId' => 'User Id',
                                        'fullAddress' => 'Full Address',
                                        'otherDetails' => 'Other Details',
                                        'numberofLicence' => 'Number of Licence',
                                        'availableLincence'=>'Available licence',
                                        'website'=>'website',
                                        'website2'=>'website2',
                                        'website3'=>'websit3',
                                        'website4'=>'website4',
                                        'trackPage'=>'TrackPage',
                                        'smsSender'=>'smsSender',
                                        'smsProvider'=>'smsProvider',
                                        'providerUserName'=>'SMS Provider User Name',
                                        'providerPassword'=>'SMS Provider Password',
                                        //'eFDSchedular'=>$eFDSchedular,
                                        'timeZone'=>'Time Zone',
                                        'apiKey'=>'Api Key',
                                        'apiKey2'=>'Api Key2',
                                        'apiKey3'=>'Api Key3',
                                        'apiKey4'=>'Api Key4',
                                        'mapKey'=>'Map Key',
                                        'addressKey'=>'Address Key',
                                        'notificationKey'=>'Notification Key',
                                        'gpsvtsApp'=>'App Key',
                                        'backUpDays'=>'DB BackupDays',
                                        'dbType'=>'DB Type',
                                        'dbIP'=>'DB IP',
                                        'subDomain'=>'SubDomain Name',

                        );
                        $oldFranch = array();
                        $NewFranch = array();

                        try {

                                $fransNew       =       json_decode($detailsJson,true);

                                if(($franchiseDetails !== $fransNew) || ($getOldIp != $dbIp)){

                                        log::info(' change arun ');
                                        if($getOldIp != $dbIp){
                                                $oldFranch      = array_add($oldFranch, $mappingFn['dbIP'], $getOldIp);
                                                $NewFranch      = array_add($NewFranch, $mappingFn['dbIP'], $dbIp);
                                        }
                                        foreach ($fransNew as $key => $value) {
                                                if(isset($franchiseDetails[$key]) != 1){
                                                        log::info(' object key null ');
                                                        $oldFranch      = array_add($oldFranch, $mappingFn[$key], '');
                                                        $NewFranch      = array_add($NewFranch, $mappingFn[$key], $value);

                                                } else if($franchiseDetails[$key] != $value){

                                                        log::info(' object value change  ');
                                                        $oldFranch      = array_add($oldFranch, $mappingFn[$key], $franchiseDetails[$key]);
                                                        $NewFranch      = array_add($NewFranch, $mappingFn[$key], $value);

                                                }

                                        }


                                }

                                Session::put('email', $email2);
                                $response=Mail::send('emails.franchies', array('url'=>$fname, 'old'=>$oldFranch, 'new'=>$NewFranch), function($message){
                                        $message->to(Session::pull ( 'email' ))->subject('Franchises Details Updated !');
                                });

                        } catch (Exception $e) {

                                log::info(' mail error ');
                                log::info($e);
                        }


                }

                // redirect
                Session::flash ( 'message', 'Successfully updated ' . $fname . '!' );
                return Redirect::to ( 'vdmFranchises' );

        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return Response
         */
        public function destroy($id) {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $username = Auth::user ()->username;
                $redis = Redis::connection ();

                $fcode = $id;

                //GeoMaxLimit
        $redis->del('GeoCodeMaxLimit_'.$fcode);
        $redis->del('GeoLocationVehicle_'.$fcode);
                //grop remove
                $group=$redis->smembers('S_Groups_'.$fcode);
                foreach ($group as $key => $getGrpName)
          {
                 $veh=$redis->smembers($getGrpName);
                 foreach ($veh as $key => $vehL)
             {

               $redis->srem($getGrpName,$vehL);
             }
             $redis->srem('S_Groups_'.$fcode,$getGrpName);
                  }
                  $redis->del('S_Groups_Admin_'.$fcode);
                  $redis->del('S_Groups_'.$fcode);

                 //users remove
                $usr=$redis->smembers('S_Users_'.$fcode);
                foreach ($usr as $key => $username1)
        {
                //reports Delete under the user
                $redis->del('S_Users_Reports_'.$username1.'_'.$fcode);
                $redis->hdel ( 'H_UserId_Cust_Map', $username1 . ':fcode', $username1 . ':mobileNo',$username1.':email',$username1.':password',$username1.
                                ':zoho', $username1.':OWN',$username1 . ':companyName');
                $redis->hdel("H_Notification_Map_User",$username1);

                DB::table('users')->where('username', $username1)->delete();
                $redis->srem('S_Users_'.$fcode,$username1);


                }
                $redis->del('S_Users_Admin_'.$fcode);
                $redis->del('S_Users_'.$fcode);

                //vehicle remove
                $vehicleList=$redis->smembers('S_Vehicles_'.$fcode);
                foreach ($vehicleList as $key => $vehicleId)
        {
                $redis->hdel('H_RefData_'.$fcode,$vehicleId);
                $redis->hdel('H_ProData_'.$fcode,$vehicleId);
                $Lhist=$redis->keys('L_Hist_'. $vehicleId .'_'. $fcode .'_*');
                foreach ($Lhist as $key => $LhistID)
                {
                        $redis->del($LhistID);
                }

                $Lsensor=$redis->keys('L_Sensor_Hist_'.$vehicleId .'_'. $fcode .'_*');

                foreach ($Lsensor as $key => $LsensorId)
                {
                        $redis->del($LsensorId);
                }

                $Zsensor=$redis->del('Z_Sensor_'. $vehicleId .'_'. $fcode);
                 /*$setZsensor='Z_Sensor_*'. $fcode;*/
                $nodata=$redis->del('NoData24:'. $fcode .':'. $vehicleId);


                 DB::table('Vehicle_details')->where('vehicle_id', $vehicleId)->where('fcode', $fcode)->delete();


                $redis->srem('S_Vehicles_'.$fcode,$vehicleId);

        }
        $redis->del('H_RefData_'.$fcode);
        $redis->del('H_ProData_'.$fcode);
        $redis->del('S_Vehicles_Admin_'.$fcode);
                $redis->del('S_Vehicles_'.$fcode);
        $redis->del('H_LicenceExpiry_'.$fcode);
        $licenceKeys=$redis->keys('S_LicenceList_*_'.$fcode);
        foreach ($licenceKeys as $key => $name){
                $redis->del($name);
        }
        $redis->del('S_ExpiredLicence_Vehicle'.$fcode);
        $redis->del('H_Vehicle_LicenceId_Map_'.$fcode);


                //Device Remove
                $DeviceList=$redis->smembers('S_Device_'.$fcode);
        foreach ($DeviceList as $key => $DeviceId)
        {
                $redis->hdel('H_Device_Cpy_Map',$DeviceId);
                $redis->srem('S_Device_'.$fcode,$DeviceId);

        }
                $redis->del('S_Device_'.$fcode);
        //Expire Key Remove
        $redis->del('H_Expire_'.$fcode);

        //org Email alerts
        $redis->del('H_EMAIL_'.$fcode);

        //Report remove admin
        $redis->del('S_Users_Reports_Admin_'.$fcode);


         //Dealers Remove
         $dealerLi=$redis->smembers('S_Dealers_'.$fcode);
         foreach ($dealerLi as $key => $dealerID)
        {

                /*$redis->del('S_Users_Reports_'.$username1.'_'.$fcode);*/

                $redis->hdel('H_UserId_Cust_Map', $dealerID . ':fcode',$dealerID.':email',$dealerID.':mobileNo',$dealerID.':password',$dealerID.
                                ':zoho', $dealerID.':OWN',$dealerID . ':companyName');
                $redis->hdel ( 'H_DealerDetails_' . $fcode, $dealerID);
                $redis->hdel ( 'H_Google_Android_ServerKey',$fcode.':'.$dealerID);
                $redis->del('S_Vehicles_Dealer_'.$dealerID.'_'.$fcode);
                        DB::table('users')->where('username', $dealerID)->delete();

                $groups=$redis->smembers('S_Groups_Dealer_'.$dealerID.'_'.$fcode);
                foreach ($groups as $key => $value1) {
                 $redis->del($value1);
                 $redis->del('S_'.$value1);
                 $redis->srem('S_Groups_Dealer_'.$dealerID.'_'.$fcode,$value1);
            //$redis->sadd('S_Groups_Admin_'.$fcode,$value1);
           }

           $orgDel=$redis->smembers('S_Organisations_Dealer_'.$dealerID.'_'.$fcode);
                foreach ($orgDel as $key => $value2) {
            /*Log::info(" org " .$value2);*/
                $redis->hdel('H_Organisations_'.$fcode,$value2);
                $redis->srem('S_Organisations_Dealer_'.$dealerID.'_'.$fcode,$value2);
            //$redis->sadd('S_Organisations_Admin'.$fcode,$value2);
                $redis->del('H_Site_'.$value2.'_'.$fcode);
                $redis->del('S_'.$value2);
                $redis->hdel('H_Scheduler_'.$fcode, 'getReadyForMorningSchoolTrips_'.$value2);
                         $redis->hdel('H_Scheduler_'.$fcode, 'getReadyForAfternoonSchoolTrips_'.$value2);
                $redis->hdel('H_Scheduler_'.$fcode, 'getReadyForEveningSchoolTrips_'.$value2);
                $redis->del('H_Poi_'.$value2.'_'.$fcode);
                $redis->del('S_Poi_'.$value2.'_'.$fcode);
                $redis->del('S_RouteList_'. $value2.'_'.$fcode);

                }
                $users=$redis->smembers('S_Users_Dealer_'.$dealerID.'_'.$fcode);
                 foreach ($users as $key => $value3) {
                $redis->del($value3);
                $redis->srem('S_Users_Dealer_'.$dealerID.'_'.$fcode,$value3);
                $redis->hdel ( 'H_UserSmsInfo_' . $fcode,$value3);
                $redis->srem('S_Users_Virtual_' . $fcode, $value3);
                $redis->srem ( 'S_Users_AssetTracker_' . $fcode, $value3);
                $redis->del('S_Orgs_' .$value3 . '_' . $fcode);
                        $redis->hdel('H_Notification_Map_User', $value3);
            //$redis->sadd('S_Users_Admin_'.$fcode,$value3);
                $redis->hdel ( 'H_UserId_Cust_Map', $value3 . ':fcode', $value3 . ':mobileNo',$value3.':email',$value3.':password' );

                 DB::table('users')->where('username', $value3)->delete();

        }
        $redis->del('S_Pre_Onboard_Dealer_'.$dealerID.'_'.$fcode);
        $redis->del('H_Pre_Onboard_Dealer_'.$dealerID.'_'.$fcode);
        $redis->del('H_VehicleName_Mobile_Dealer_'.$dealerID.'_Org_'.$fcode);
        $redis->hdel ( 'H_Google_Android_ServerKey',$fcode.':'.$dealerID );


        $redis->srem('S_Dealers_'.$fcode,$dealerID);

        }
        $redis->del('S_Dealers_'.$fcode);

        //vehicle->device map key Delete
        $redis->del('H_Vehicle_Device_Map_'.$fcode);

        $redis->del ( 'S_Users_Virtual_' . $fcode);

        $redis->del ( 'S_Users_AssetTracker_' . $fcode);

        //ORG LIST
        $orgList=$redis->smembers('S_Organisations_'.$fcode);
        foreach ($orgList as $key => $orgName) {
                $redis->del ( 'S_Vehicles_'.$orgName.'_' . $fcode);
                $redis->del ( 'S_Organisation_Route_'.$orgName.'_' . $fcode);


        }
        $orgAdminList=$redis->smembers('S_Organisations_Admin_'.$fcode);
        foreach ($orgAdminList as $key => $orgAdminName) {
                $redis->del ( 'S_Vehicles_'.$orgAdminName.'_' . $fcode);
                $redis->del ( 'S_Organisation_Route_'.$orgAdminName.'_' . $fcode);

                        $redis->hdel('H_Org_Company_Map',$orgAdminName);
        }
        $redis->del('H_Scheduler_'.$fcode);
        $redis->del('H_Organisations_'.$fcode);
        $setkeys=$redis->keys( 'S_*_'.$fcode);
        foreach ($setkeys as $key => $value) {
                $redis->del($value);
        }
        $hashkeys=$redis->keys( 'H_*_'.$fcode);
        foreach ($hashkeys as $key => $value) {
                $redis->del($value);
        }
        $zkeys=$redis->keys( 'Z_*_'.$fcode);
        foreach ($zkeys as $key => $value) {
                $redis->del($value);
        }
        $lkeys=$redis->keys( 'L_*_'.$fcode);
        foreach ($lkeys as $key => $value) {
                $redis->del($value);
        }
        $gkeys=$redis->keys( '*:'.$fcode);
        foreach ($gkeys as $key => $value) {
                $redis->del($value);
        }

        $tempKey=$redis->keys('Temp_Towing_*'.$fcode);
        foreach ($tempKey as $key => $value) {
                $redis->del($value);
        }

        $Evekey=$redis->keys('K_Evening_StopSeq_*'.$fcode);
        foreach ($Evekey as $key => $value) {
                $redis->del($value);
        }
        $Morkey=$redis->keys('K_Morning_StopSeq_*'.$fcode);
        foreach ($Morkey as $key => $value) {
                $redis->del($value);
        }
        $FuelKey=$redis->keys('LHistForFuel_*'.$fcode);
        foreach ($FuelKey as $key => $value) {
                $redis->del($value);
        }



                $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                                $franchiseDetails=json_decode($franDetails_json,true);

                        $userId = $franchiseDetails['userId'];
                        $fname =$franchiseDetails['fname'];
                        $email1 = $franchiseDetails['email1'];


                /*$userId = $redis->hget('H_Franchise',$fcode.':userId');
                $fname = $redis->hget('H_Franchise',$fcode.':fname');

                $email1 = $redis->hget('H_Franchise', $fcode.':email1');*/


        $fDetails    =   $redis->hget ( 'H_Franchise' , $fcode );
                $redis->del('back_H_RefData_'.$fcode);
                $redis->hdel('H_Franchise_LatLong',$fcode);

                $redis->hdel ( 'H_Franchise', $fcode);




                $redis->srem('S_Franchises',$fcode);

                $redis->srem ( 'S_Users_' . $fcode, $userId );
                $redis->hdel ( 'H_UserId_Cust_Map', $userId . ':fcode', $userId . ':mobileNo', $userId.':email');

        Log::info("before audit Frans entry");
         try{
                    $details = json_decode($fDetails, TRUE);
                    $mysqlDetails =array();
                    $status =array();
                    $status=array(
                        'username'=>$username,
                        'status' => Config::get('constant.deleted'),
                        'fcode' => $fcode,
                        'userIpAddress'=>Session::get('userIP'),
                    );
                    $mysqlDetails       = array_merge($status,$details);
                    $modelname = new AuditFrans();
                            $table = $modelname->getTable();
                                //return $table;
                                //return $mysqlDetails;
                                $db=Config::get('constant.VAMOSdb');
                            AuditTables::ChangeDB($db);
                                $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
                                //return $tableExist;
                                if(count($tableExist)>0){
                                        AuditFrans::create($mysqlDetails);
                                }
                                else{
                                        AuditTables::CreateAuditFrans();
                                        AuditFrans::create($mysqlDetails);
                                }

                        }catch (Exception $e) {
                                  log::info('Error inside Audit_Frans Delete'.$e->getMessage());
                                  log::info($mysqlDetails);
                                }
                        Log::info(" after audit Frans entry");




                Log::info(" about to delete user" .$userId);

                DB::table('users')->where('username', $userId)->delete();

                $vamosid = 'vamos'.$fcode;


                $redis->srem ( 'S_Users_' . $fcode, $vamosid );
                $redis->hdel ( 'H_UserId_Cust_Map', $vamosid . ':fcode');
        $servername=$redis->hget('H_Franchise_Mysql_DatabaseIP',$fcode);
                $redis->hdel('H_Franchise_Mysql_DatabaseIP',$fcode);
                $delnotify=$redis->hdel ( 'H_Google_Android_ServerKey',$fcode);
                $delappkey=$redis->hdel ( 'H_MobileAppId',$fcode);

                ///***********MYSQL********************
         //$servername = "209.97.163.4";
                 if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
                 return 'Ipaddress Failed !!!';
            }
            $usernamedb  =  "root";
            $password    =  "#vamo123";
            $dbname      =  $fcode;
            log::info($dbname);
            log::info($servername);
                $conn  =  mysqli_connect($servername, $usernamedb, $password);
                        $drp = "DROP DATABASE `$dbname`";
                if ($conn->query($drp) == TRUE) {
                    log::info('Sucessfully dropped database'.$dbname.'!');
                } else {
                    log::info('Error dropping database: ' . $conn->error);
                }
                $conn->close();


                Session::put('email1',$email1);
                Log::info("Email Id :" . Session::get ( 'email1' ));

                Mail::queue('emails.welcome', array('fname'=>$fname,'userId'=>$userId), function($message)
                {
                        Log::info("Inside email :" . Session::get ( 'email1' ));

                        $message->to(Session::pull ( 'email1' ))->subject('User Id deleted');
                });


                Session::flash ( 'message', 'Successfully deleted ' . 'fname:'.$fcode . '!' );
                return Redirect::to ( 'vdmFranchises' );
        }
/**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return Response
         */



    public function reports($id) {
        if (! Auth::check () ) {
            return Redirect::to ( 'login' );
        }
        $totalReportList = array();
        $list = array();
        $totalList = array();
        $reportsList = array();
        $username = Auth::user ()->username;
        $fcode = $id;
        $redis = Redis::connection ();
        $totalReport = null;
        $totalReport = $redis->smembers("S_TotalReports");
                foreach ($totalReport as $key => $getReport)
                {
                        $specReports = $redis->smembers($getReport);
                        $reportType = explode("_",$getReport);
                        foreach ($specReports as $key => $ReportName) {
                                $report[]=$ReportName.':'.$reportType[0];
                        }
                        $reportsList[] = $getReport;
                        $totalReportList[] = $report;
                        $totalList[$getReport] = $report;
                        $report = null;
                }
                $userReports = $redis->smembers("S_Users_Reports_Admin_".$fcode);
                return view ( 'vdm.franchise.reports', array (
                                'fcode' => $fcode
                ) )->with ( 'reportsList', $reportsList )
                ->with('totalReportList',$totalReportList)
                ->with('totalList',$totalList)
                ->with('userReports',$userReports);
        }


    public function updateReports() {
        if (! Auth::check ()) {
            return Redirect::to ( 'login' );
        }
        $fcode = Input::get('fcode');
        $username = Auth::user ()->username;
        $reportName = Input::get('reportName');
        $redis = Redis::connection ();

                if($reportName != null)
                                {
                                $prevReportList = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
                                $redis->del('S_Users_Reports_Admin_'.$fcode);
                                foreach ($reportName as $key => $value) {
                                        $redis->sadd('S_Users_Reports_Admin_'.$fcode, $value);
                                }
                        $defaultReports = $redis->smembers('S_Default_ReportList');
                        foreach ($defaultReports as $key => $value) {
                                $redis-> sadd('S_Users_Reports_Admin_'.$fcode , $value);
                        }
                        $removeList = '';
                        $currentReportList = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
                        foreach ($prevReportList as $key => $value) {
                                if (in_array($value, $currentReportList)) {
                                        log::info("GotErix");
                                }else
                                {
                                        $removeList = $value.','.$removeList;
                                        log::info("GotErix2");
                                }
                        }
                        $addedList = '';
                        $currentReportList = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
                        foreach ($currentReportList as $key => $value) {
                                if (in_array($value,$prevReportList)) {
                                        log::info("GotErix");
                                }else
                                {
                                        $addedList = $value.','.$addedList;
                                        log::info("GotErix2");
                                }
                        }
                 $parameters = 'fcode='.$fcode . '&removeList='.$removeList;
                 /*$ipaddress = $redis->get('ipaddress');
                 $url = 'http://'.$ipaddress.':9000/getRemoveReports?' . $parameters;
                 $url=htmlspecialchars_decode($url);
                 log::info( ' url :' . $url);
                 $ch = curl_init();
                 curl_setopt($ch, CURLOPT_URL, $url);
                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                 curl_setopt($ch, CURLOPT_TIMEOUT, 3);
                 curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                $response = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);*/
                        // if($removeList != null)
                        // {
                                // $dealerList[] = $redis->smembers("S_Dealers_".$fcode);
                                // $dealerList[] = $redis->smembers("S_Users_".$fcode);
                                // log::info($dealerList);
                                // // foreach ($dealerList as $key => $dealer) {
                                //      $redis->srem("S_Users_Reports_Dealer_".$dealer.'_'.$fcode, $removeList);
                                // }
                                // $userList = $redis->smembers("S_Users_".$fcode);
                                // foreach ($userList as $key => $user) {
                                //      $redis->srem("S_Users_Reports_".$dealer.'_'.$fcode, $removeList);
                                // }
                 $availableReports='';
                 foreach ($reportName as $key => $value) {
                                 $availableReports=$availableReports.','.$value;

                                    }
                 log::info('before audit entry for Edit Reports');
                 try{
                            $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                                            $franDetails=json_decode($franDetails_json,true);
                                    $mysqlDetails =array();
                                    $status =array();
                                        $status=array(
                                                    'username'=>$username,
                                                    'status' => Config::get('constant.reportsEdited'),
                                                        'fcode' => $fcode,
                                                        'addBasicLicence'=> Input::get ('addBasicLicence'),
                                                    'addAdvanceLicence'=>Input::get ('addAdvanceLicence'),
                                                        'addPremiumLicence'=>Input::get ('addPremiumLicence'),
                                                        'addLicence'=>Input::get ('addLicence'),
                                                        'reports'=>$availableReports,
                                                        'removedReports'=>$removeList,
                                                        'addedReports'=>$addedList,
                                                        'userIpAddress'=>Session::get('userIP'),

                                                );
                                                log::info(implode(",", $reportName));
                                                $mysqlDetails   = array_merge($status,$franDetails);
                                    $modelname = new AuditFrans();
                                            $table = $modelname->getTable();
                                                //return $table;
                                                //return $mysqlDetails;
                                                $db=Config::get('constant.VAMOSdb');
                                            AuditTables::ChangeDB($db);
                                                $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
                                                //return $tableExist;
                                                if(count($tableExist)>0){
                                                        AuditFrans::create($mysqlDetails);
                                                }
                                                else{
                                                        AuditTables::CreateAuditFrans();
                                                        AuditFrans::create($mysqlDetails);
                                                }
                                        }catch (Exception $e) {
                                           log::info('Error inside Audit_Frans Create'.$e->getMessage());
                                           log::info($mysqlDetails);
                                        }
                                        //AuditFrans::create($details);
                                        log::info('after audit entry for Edit Reports');


                }
                else {
                        $redis->del('S_Users_Reports_Admin_'.$fcode);
                        $defaultReports = $redis->smembers('S_Default_ReportList');
                        foreach ($defaultReports as $key => $value) {
                        $redis-> sadd('S_Users_Reports_Admin_'.$fcode , $value);
                        }

                }

                return Redirect::to ( 'vdmFranchises' );
        }

                public function disable($id) {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $username = Auth::user ()->username;

                $fcode=$id;
                $redis = Redis::connection ();

                                $re=$redis->sadd ( 'S_Franchises_Disable', $fcode );
                                if($re==1){
                                        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                                    $franDetails=json_decode($franDetails_json,true);
                                    $fname=$franDetails['fname'];
                    log::info('before audit entry for Franchise Disable');
                    try{
                                            $mysqlDetails =array();
                                            $status =array();
                                                $status=array(
                                                            'username'=>$username,
                                                            'status' => Config::get('constant.disabled'),
                                                                'fcode' => $fcode,
                                                                'addBasicLicence'=> Input::get ('addBasicLicence'),
                                                            'addAdvanceLicence'=>Input::get ('addAdvanceLicence'),
                                                                'addPremiumLicence'=>Input::get ('addPremiumLicence'),
                                                                'addLicence'=>Input::get ('addLicence'),
                                                                'userIpAddress'=>Session::get('userIP'),

                                                        );
                                                        $mysqlDetails   = array_merge($status,$franDetails);
                                            $modelname = new AuditFrans();
                                                    $table = $modelname->getTable();
                                                        //return $table;
                                                        //return $mysqlDetails;
                                                        $db=Config::get('constant.VAMOSdb');
                                                    AuditTables::ChangeDB($db);
                                                        $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
                                                        //return $tableExist;
                                                        if(count($tableExist)>0){
                                                                AuditFrans::create($mysqlDetails);
                                                        }
                                                        else{
                                                                AuditTables::CreateAuditFrans();
                                                                AuditFrans::create($mysqlDetails);
                                                        }
                                                }catch (Exception $e) {
                                           log::info('Error inside Audit_Frans Disable'.$e->getMessage());
                                           log::info($mysqlDetails);
                                        }
                                        //AuditFrans::create($details);
                                        log::info('after audit entry for Franchise Disable');
                                        Session::flash ( 'message', $fname.' Franchise has been disabled successfully !');
                                        return Redirect::to ( 'vdmFranchises' );

                                }
                                else{

                                        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                                    $franDetails=json_decode($franDetails_json,true);
                                    $fname=$franDetails['fname'];

                                        Session::flash ( 'message', $fname.' Franchise has been Already Disabled !');
                                        return Redirect::to ( 'vdmFranchises' );

                                }
        }
        public function Enable($id) {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $username = Auth::user ()->username;

                $fcode=$id;
                $redis = Redis::connection ();

                                $re=$redis->srem( 'S_Franchises_Disable', $fcode );
                                if($re==1){
                                        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                                    $franDetails=json_decode($franDetails_json,true);
                                    $fname=$franDetails['fname'];
                    log::info('before audit entry for Franchise Disable');
                    try{
                                    $mysqlDetails =array();
                                    $status =array();
                                        $status=array(
                                                    'username'=>$username,
                                                    'status' => Config::get('constant.enabled'),
                                                        'fcode' => $fcode,
                                                        'addBasicLicence'=> Input::get ('addBasicLicence'),
                                                    'addAdvanceLicence'=>Input::get ('addAdvanceLicence'),
                                                        'addPremiumLicence'=>Input::get ('addPremiumLicence'),
                                                        'addLicence'=>Input::get ('addLicence'),
                                                        'userIpAddress'=>Session::get('userIP'),

                                                );
                                                $mysqlDetails   = array_merge($status,$franDetails);
                                    $modelname = new AuditFrans();
                                            $table = $modelname->getTable();
                                                //return $table;
                                                //return $mysqlDetails;
                                                $db=Config::get('constant.VAMOSdb');
                                            AuditTables::ChangeDB($db);
                                                $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
                                                //return $tableExist;
                                                if(count($tableExist)>0){
                                                        AuditFrans::create($mysqlDetails);
                                                }
                                                else{
                                                        AuditTables::CreateAuditFrans();
                                                        AuditFrans::create($mysqlDetails);
                                                }
                                        }catch (Exception $e) {
                                           log::info('Error inside Audit_Frans Enable'.$e->getMessage());
                                           log::info($mysqlDetails);
                                        }
                                        //AuditFrans::create($details);
                                        log::info('after audit entry for Franchise Disable');
                                        Session::flash ( 'message', $fname.' Franchise has been enabled successfully !');
                                        return Redirect::to ( 'vdmFranchises' );

                                }else{
                                        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                                    $franDetails=json_decode($franDetails_json,true);
                                    $fname=$franDetails['fname'];

                                        Session::flash ( 'message', $fname.' Franchise has been already Enabled !');
                                        return Redirect::to ( 'vdmFranchises' );
                                }

        }
    public function loadRemove($id){
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $username = Auth::user ()->username;
                $redis = Redis::connection ();
                $redis->srem('Sensor_IndivdiualReportsList','FUEL_MACHINE');
                $redis->sadd('Sensor_IndivdiualReportsList','FUEL_ANALYTICS');
                $fcode = $id;
                $chk=$redis->sismember('S_Users_Reports_Admin_'.$fcode,'FUEL_MACHINE:Sensor');
                if($chk==1){
                        $redis->srem('S_Users_Reports_Admin_'.$fcode,'FUEL_MACHINE:Sensor');
                        $redis->sadd('S_Users_Reports_Admin_'.$fcode,'FUEL_ANALYTICS:Sensor');
                }
                $usr=$redis->smembers('S_Users_'.$fcode);
                foreach ($usr as $key => $username1){
                        $chk1=$redis->sismember('S_Users_Reports_'.$username1.'_'.$fcode,'FUEL_MACHINE:Sensor');
                        if($chk1==1){
                                $redis->srem('S_Users_Reports_'.$username1.'_'.$fcode,'FUEL_MACHINE:Sensor');
                                $redis->srem('S_Users_Reports_'.$username1.'_'.$fcode,'FUEL_ANALYTICS:Sensor');
                        }
                }

                 $dealerLi=$redis->smembers('S_Dealers_'.$fcode);
         foreach ($dealerLi as $key => $dealerID){
                $users=$redis->smembers('S_Users_Dealer_'.$dealerID.'_'.$fcode);
                 foreach ($users as $key => $username) {
                        $chk2=$redis->sismember('S_Users_Reports_'.$username.'_'.$fcode,'FUEL_MACHINE:Sensor');
                        if($chk2==1){
                         $redis->srem('S_Users_Reports_'.$username.'_'.$fcode,'FUEL_MACHINE:Sensor');
                         $redis->sadd('S_Users_Reports_'.$username.'_'.$fcode,'FUEL_ANALYTICS:Sensor');
                        }
                 }
                 $chk3=$redis->sismember('S_Users_Reports_Dealer_'.$dealerID.'_'.$fcode,'FUEL_MACHINE:Sensor');
                 if($chk3==1){
                         $redis->srem('S_Users_Reports_Dealer_'.$dealerID.'_'.$fcode,'FUEL_MACHINE:Sensor');
                         $redis->sadd('S_Users_Reports_Dealer_'.$dealerID.'_'.$fcode,'FUEL_ANALYTICS:Sensor');
                 }

        }
        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                $franchiseDetails=json_decode($franDetails_json,true);
                $fname =$franchiseDetails['fname'];
        Session::flash ( 'message', $fname.' Franchise has Fuel Machine renamed as Fuel Analitical !');

                return Redirect::to ( 'vdmFranchises' );


        }
/*****************ALH REMOVE CODE****************/
        /*public function orgremove($id){
                $fcode=$id;
                                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $username = Auth::user ()->username;
                $redis = Redis::connection ();
    $orglist=null;
                log::info('Fcode------->'.$fcode);
                $orglist1=$redis->smembers('S_Organisations_'.$fcode);
    $orglist = array();
    $i=0;
    foreach ( $orglist1 as $org ) {
      $org1=strtoupper($org);
                        if($org==$org1) {
            $orglist = array_add($orglist, $i,$org);
                        }
                        $i=$i+1;
   }
    array_multisort($orglist);
                return view ( 'vdm.franchise.orgremove' )->with('orglist',$orglist)->with('fcode',$fcode);
        }
        public function orgremoveUpdate(){
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $username = Auth::user ()->username;
                $redis = Redis::connection ();
                $orgplace = Input::get ( 'orglist' );
                $fcode  = Input::get ( 'fcode' );
    $orglist1=$redis->smembers('S_Organisations_'.$fcode);
    $orglist = array();
    $i=0;
    foreach ( $orglist1 as $org ) {
      $org1=strtoupper($org);
                        if($org==$org1) {
            $orglist = array_add($orglist, $i,$org);
                        }
                        $i=$i+1;
   }
    array_multisort($orglist);
                $LicenceCount=0;
                $orgVehicleList=null;
                $selectorgList =null;
                foreach ($orgplace as $key => $data) {
                foreach ($orglist as $keys => $value) {
                 if($keys==$data){
            $selectorgList=array_add($selectorgList,$value,$value);
                        $vehiclelist=$redis->smembers('S_Vehicles_'.$fcode);
                        $LicenceCount1=$redis->scard('S_Vehicles_'.$fcode);
                        foreach ($vehiclelist as $keys => $vehicleId) {
            $details = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
                        $details1=json_decode($details, true);
                        if($value == $details1['orgId']){
                                $orgVehicleList=array_add($orgVehicleList,$vehicleId,$vehicleId);
                        }}
                 }
                }}
                $vehiclelists=$redis->smembers('S_Vehicles_'.$fcode);
                foreach ($vehiclelists as $key => $vehicleId){
          if (in_array($vehicleId, $orgVehicleList)){

          }else{

            $vehGroup=$redis->del('S_'.$vehicleId.'_'.$fcode);


              $group=$redis->smembers('S_Groups_Admin_'.$fcode);
                        foreach ($group as $key => $getGrpName) {
                $veh=$redis->srem($getGrpName,$vehicleId);
                $ckEmpty=$redis->smembers($getGrpName);
                if($ckEmpty == null){
                  $users=$redis->smembers('S_Users_'.$fcode);
                  foreach ($users as $key => $userID) {
                    $redis->srem($userID,$getGrpName);
                    $ckEmpty1=$redis->smembers($userID);
                    if($ckEmpty1 == null){
                        $redis->del($userID);
                         $dealers=$redis->smembers('S_Dealers_'.$fcode);
                          foreach($dealers as $key => $dealerID){
                          $csk=$redis->sismember('S_Users_Dealer_'.$dealerID.'_'.$fcode,$userID);
                          if($csk == 1){
                              log::info("Users under the Dealer Removed".$dealerID);
                              $redis->srem('S_Users_Dealer_'.$dealerID.'_'.$fcode,$userID);
                                $redis->hdel ( 'H_UserSmsInfo_' . $fcode,$userID);
                                $redis->srem('S_Users_Virtual_' . $fcode, $userID);
                                $redis->srem ( 'S_Users_AssetTracker_' . $fcode, $userID);
                                  $redis->del('S_Orgs_' .$userID . '_' . $fcode);
                                          $redis->hdel('H_Notification_Map_User', $userID);
                                $redis->hdel ( 'H_UserId_Cust_Map', $userID . ':fcode', $userID . ':mobileNo',$userID.':email',$userID.':password' );
                                 //DB::table('users')->where('username', $userID)->delete();
                                                                 DB::table('users')->whereRaw("BINARY `username`= ?",[$userID])->delete();
                          }
                           $delEmpty=$redis->smembers('S_Users_Dealer_'.$dealerID.'_'.$fcode);
                           if($delEmpty==null){
                               log::info("Dealer Removed".$dealerID);
                               $redis->del('S_Pre_Onboard_Dealer_'.$dealerID.'_'.$fcode);
                               $redis->del('H_Pre_Onboard_Dealer_'.$dealerID.'_'.$fcode);
                               $redis->del('H_VehicleName_Mobile_Dealer_'.$dealerID.'_Org_'.$fcode);
                               $redis->hdel ( 'H_Google_Android_ServerKey',$fcode.':'.$dealerID );
                               //DB::table('users')->where('username', $dealerID)->delete();
                                                           DB::table('users')->whereRaw("BINARY `username`= ?",[$dealerID])->delete();
                               $redis->srem('S_Dealers_'.$fcode,$dealerID);
                               $redis->srem('S_Dealers_Admin_'.$fcode,$dealerID);
                           }
                          }
                         log::info("Users Removed".$userID);
                        $redis->srem('S_Users_'.$fcode,$userID);
                        $redis->srem('S_Users_Admin_'.$fcode,$userID);
                                                DB::table('users')->whereRaw("BINARY `username`= ?",[$userID])->delete();
                        //DB::table('users')->where('username', $userID)->delete();
                    }
                  }
                  log::info("group removed".$getGrpName);
                  $redis->srem('S_Groups_'.$fcode,$getGrpName);
                 $del=$redis->srem('S_Groups_Admin_'.$fcode,$getGrpName);
                 log::info($del);
                 $redis->del($getGrpName);
                 $redis->del('S_'.$getGrpName);
                }

                    }
                        //device remove
                        $DeviceList=$redis->smembers('S_Device_'.$fcode);
            $details = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
                          $details1=json_decode($details, true);
                        foreach ($DeviceList as $key => $DeviceId) {
                        if($details1['deviceId']==$DeviceId){
                                        $admindeal=$vehicleId.':'.$DeviceId.':'.$details1['shortName'].':'.$details1['orgId'].':'.$details1['gpsSimNo'].':'.$details1['OWN'];
                                        $admindeal1=strtoupper($admindeal);
                                        $admindeal2=str_replace(' ', '', $admindeal1);
                                        $redis->hdel('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode,$admindeal2);
                                        $dea=$vehicleId.':'.$DeviceId.':'.$details1['shortName'].':'.$details1['orgId'].':'.$details1['gpsSimNo'];
                                        $dea1=strtoupper($dea);
                                        $dea2=str_replace(' ', '', $dea1);



                        $redis->hdel('H_Device_Cpy_Map',$DeviceId);
                        $redis->srem('S_Device_'.$fcode,$DeviceId);
                }}
            $redis->srem('S_Vehicles_'.$fcode,$vehicleId);
            $redis->srem('S_Vehicles_Admin_'.$fcode,$vehicleId);
            $redis->del('S_'.$vehicleId.'_'.$fcode);
            $redis->del('L_HistforOutOfOrderData_'. $vehicleId .'_'. $fcode .'_*');
            $redis->hdel('H_RefData_'.$fcode,$vehicleId);
                $redis->hdel('H_ProData_'.$fcode,$vehicleId);
                $Lhist=$redis->keys('L_Hist_'. $vehicleId .'_'. $fcode .'_*');
                foreach ($Lhist as $key => $LhistID){
                        $redis->del($LhistID);
                }
                $Lsensor=$redis->keys('L_Sensor_Hist_'.$vehicleId .'_'. $fcode .'_*');
                        foreach ($Lsensor as $key => $LsensorId){
                        $redis->del($LsensorId);
                }
                $Zsensor=$redis->del('Z_Sensor_'. $vehicleId .'_'. $fcode);
                $nodata=$redis->del('NoData24:'. $fcode .':'. $vehicleId);

                              $redis->srem('S_Vehicles_'.$fcode,$vehicleId);
                          $redis->srem('S_Vehicles_Admin_'.$fcode,$vehicleId );
                          $redis->hdel('H_Vehicle_Device_Map_'.$fcode,$vehicleId);
                                 //DB::table('Vehicle_details')->whereRaw("BINARY `vehicle_id`= ?",[$vehicleId])->whereRaw("BINARY `fcode`= ?",[$fcode])->first()->delete();

                        //DB::table('Vehicle_details')->where('vehicle_id', $vehicleId)->where('fcode', $fcode)->delete();
          }
      }
      $orglists=$redis->smembers('S_Organisations_'.$fcode);
      foreach ($orglists as $key => $orgName){
      if (in_array($orgName, $selectorgList)){

      }else{
            log::info('org Removed'.$orgName);
            $redis->del('L_Suggest_*_'.$orgName.'_'.$fcode);
            $redis->del('H_Stopseq_'.$orgName.'_'.$fcode);
                $redis->del ( 'S_Vehicles_'.$orgName.'_' . $fcode);
                $redis->del ( 'S_Organisation_Route_'.$orgName.'_' . $fcode);
            $redis->srem('S_Organisations_Admin_'.$fcode,$orgName);
                        $redis->hdel('H_Org_Company_Map',$orgName);
            $redis->srem('S_Organisations_'.$fcode,$orgName);

      }}
           $LicenceCount2=$redis->scard('S_Vehicles_'.$fcode);
           $LicenceCount=$LicenceCount1-$LicenceCount2;
           $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
       $franchiseDetails=json_decode($franDetails_json,true);
       $franchiseDetails['availableLincence']=$franchiseDetails['availableLincence']+$LicenceCount;
       $detailsJson = json_encode ( $franchiseDetails );
       $redis->hmset ( 'H_Franchise', $fcode,$detailsJson);

          Session::flash ( 'message', ' Successfully REMOVED!');
          return Redirect::to ( 'vdmFranchises' )->with('orlist');
        }       */


public static function geocode($address){
    // url encode the address
try{
    $address = urlencode($address);
    // google map geocode api url
    $url = 'https://maps.google.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyDYlJJZZPkYLGKL1KbLGSETApseUTUiF3w';
    // get the json response
    $resp_json = file_get_contents($url);
    // decode the json
    $resp = json_decode($resp_json, true);
    // response status will be 'OK', if able to geocode given address
    if($resp['status']=='OK'){
        // get the important data
        $lati = $resp['results'][0]['geometry']['location']['lat'];
        $longi = $resp['results'][0]['geometry']['location']['lng'];
        $formatted_address = $resp['results'][0]['formatted_address'];
        // verify if data is complete
        if($lati && $longi && $formatted_address){
            // put the data in the array
            $data_arr = array();
            array_push(
                $data_arr,
                    $lati,
                    $longi,
                    $formatted_address
                );
            return $data_arr;
        }else{
            return false;
        }
    }else{
        return false;
    }
   }catch(\Exception $e){
         return false;
    }
}


 public function fransOnboard(){
          log::info( '-----------List----------- ::');
      if (! Auth::check () ) {
         return Redirect::to ( 'login' );
      }
     $redis = Redis::connection ();
     $fcodeList=array(''=>'-- Select --');
     $fcodeList1=$redis->smembers('S_Franchises');
     foreach ($fcodeList1 as $key => $fcode) {
        $fcodeList=array_add($fcodeList,$fcode,$fcode);
     }
        //$count = count($redis->keys ( 'NoData:*' ));
        $apnKey = $redis->exists('Update:Apn');
    $timezoneKey = $redis->exists('Update:Timezone');
    return view ( 'vdm.franchise.fransOnboard')->with('fcodeList',$fcodeList)->with('apnKey',$apnKey)->with('timezoneKey',$timezoneKey);
}
public function fransOnUpdate(){
  log::info('Inside the Communication device downlaod');
  if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    log::info( 'User name  ::' . $username);
    $redis = Redis::connection ();
    $rules = array (
                'fcodeList' => 'required',
                'frmDate' => 'required'
        );
    $validator = Validator::make ( Input::all (), $rules );
    if ($validator->fails ()) {
            return Redirect::back()->withErrors ($validator);
    }
    $fcode=Input::get('fcodeList');
    //$fcode = 'VAM';
    $fromDate=Input::get('frmDate');
    //$toDate=Input::get('toDate');
    $file = Excel::create('Onboarded List '.$fcode, function ($excel) use ($redis,$fcode,$fromDate) {

          $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
          $servername = $franchiesJson;
          //$servername="209.97.163.4";
          if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
            return 'Ipaddress Failed !!!';
          }
          $usernamedb = "root";
          $password = "#vamo123";
          $dbname = $fcode;
          //log::info('franci. ---- '.$fcode.' ip---- '.$servername);
          $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
          if( !$conn ) {
                 die('Could not connect: ' . mysqli_connect_error());
                    return 'Please Update One more time Connection failed';
          } else {
              $fromDate = date('Y-m-d', strtotime($fromDate));
              //$toDate   = date('Y-m-d', strtotime($toDate));
              log::info(' created connection ');
              $qry="select * from communicating_Devices where lastUpdated between '".$fromDate." 00:00:00' and '".$fromDate." 23:59:59'";
              log::info($qry);
              $results = mysqli_query($conn,$qry);
              $conn->close();
            }

            $excel->sheet('Onboarded List '.$fcode, function ($sheet)  use ($redis,$results) {
              $communicating = 0;
              $nonCommunicating = 0;
              $k=5;
              $sheet->row(5,['VehicleId','DelearName','IMEI','OnboardedDate', 'VehicleName','GPSSimNo','Communicating PortNo','LastCommunicated Date','Last communicated (In Days)','30 days before Onboarded',' Expiry Date ']);
              $sheet->getStyle('A5:U5')->getAlignment()->applyFromArray(
    array('horizontal' => 'center','text-transform'=>'uppercase'));
              $sheet->cells('A5:U5',function($cells) {
              $cells->setBackground('#362673');
              $cells->setFontColor('#ffffff'); });
              $sheet->cells('A1:C3',function($cells) {
              $cells->setFontColor('#362673'); });
              if ($results != null) {
              while ($row = mysqli_fetch_array($results)) {
                     if (($row['communicated'] < 30) && ($row['status'] != 'N'))
                     {
                                                $communicating = $communicating +1;
                    }
                     else
                     {
                                                 $nonCommunicating = $nonCommunicating +1 ;
                     }
                     $k++;
                     $sheet->row($k, [$row['vehicleId'],$row['dealerName'],$row['deviceId'],$row['onboardedDate'],$row['vehicleName'],$row['gpsSimNo'],$row['communicatingPort'],$row['lastCommunicated'],$row['communicated'],$row['30DaysBeforeOnboarded'],$row['expiryDatelastUpdated']]);
             }
             $total = $communicating+$nonCommunicating;
             $sheet->mergeCells('A1:C1');
             $sheet->mergeCells('A2:C2');
             $sheet->mergeCells('A3:C3');
             $sheet->row(1,['TOTAL DEVICES  :','','',$total]);
             $sheet->row(2,['CURRENT COMMUNICATING DEVICES  :','','',$communicating]);
             $sheet->row(3,['NON COMMUNICATED DEVICES FOR LATEST 30 DAYS :','','',$nonCommunicating]);

             }
             else
             {
               log::info('No data found for communicating device download');
             }
        });
    })->download('xls');
}

public function premiumPlusIndexset(){
    if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcodeArray = $redis->smembers('S_Franchises');
    foreach ($fcodeArray as $key => $fcode) {
        $details = $redis->hget('H_Franchise',$fcode);
        $details_json=json_decode($details,true);
        $isPrePaid1=ucfirst(isset( $details_json['prepaid'])? $details_json['prepaid']:'No');
        if($isPrePaid1=="Yes")  {
                 $dealerList = $redis->smembers('S_Dealers_' . $fcode);
                 foreach ($dealerList as $key => $dealerId) {
                        $dealerData  = $redis->hget( 'H_DealerDetails_'.$fcode, $dealerId );
                                $detail=json_decode($dealerData,true);
                                log::info('BEfore Update');
                                log::info($detail);
                        $details=array(
                                        'email' =>isset($detail['email'])?$detail['email']:'0',
                                        'mobileNo' => isset($detail['mobileNo'])?$detail['mobileNo']:'0',
                                        'zoho'   => isset($detail['zoho'])?$detail['zoho']:'0',
                                        'mapKey' => isset($detail['mapKey'])?$detail['mapKey']:'0',
                                        'addressKey' => isset($detail['addressKey'])?$detail['addressKey']:'0',
                                        'notificationKey' => isset($detail['notificationKey'])?$detail['notificationKey']:'0',
                                        'gpsvtsApp' => isset($detail['gpsvtsApp'])?$detail['gpsvtsApp']:'0',
                                        'website' => isset($detail['website'])?$detail['website']:'0',
                                        'smsSender'=>isset($detail['smsSender'])?$detail['smsSender']:'0',
                                        'smsProvider'=>isset($detail['smsProvider'])?$detail['smsProvider']:'0',
                                        'providerUserName'=>isset($detail['providerUserName'])?$detail['providerUserName']:'0',
                                        'providerPassword'=>isset($detail['providerPassword'])?$detail['providerPassword']:'0',
                                        'numofBasic'    =>isset($detail['numofBasic'])?$detail['numofBasic']:'0',
                                        'numofAdvance'  =>isset($detail['numofAdvance'])?$detail['numofAdvance']:'0',
                                        'numofPremium'  =>isset($detail['numofPremium'])?$detail['numofPremium']:'0',
                    'numofPremiumPlus'  =>isset($detail['numofPremiumPlus'])?$detail['numofPremiumPlus']:'0',
                                        'avlofBasic'    =>isset($detail['avlofBasic'])?$detail['avlofBasic']:'0',
                                        'avlofAdvance'  =>isset($detail['avlofAdvance'])?$detail['avlofAdvance']:'0',
                                        'avlofPremium'  =>isset($detail['avlofPremium'])?$detail['avlofPremium']:'0',
                    'avlofPremiumPlus'  =>isset($detail['avlofPremiumPlus'])?$detail['avlofPremiumPlus']:'0',
                    'LicenceissuedDate' =>isset($detail['LicenceissuedDate'])?$detail['LicenceissuedDate']:'0',
                                );
                                $detailJson=json_encode($details);
                                $redis->hset ( 'H_DealerDetails_' . $fcode, $dealerId, $detailJson );
                                log::info('after Update');
                                log::info($details);
                 }
        }
    }
    Session::flash ( 'message','Franchise LicenceId Type successfully modified !');
    return Redirect::to ( 'vdmFranchises' );

}


public function auditShow($model)
                {

                    try{
                    $username = Auth::user ()->username;
                    $redis = Redis::connection ();
                    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
                    $modelname = new $model();
                    $table = $modelname->getTable();
                    log::info($fcode);
                    log::info($table);
                    if($fcode=='vamos')$fcode=Config::get('constant.VAMOSdb');
                    AuditTables::ChangeDB($fcode);
                        //AuditTables::CreateAuditFrans();
                    $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$fcode)->where('table_name','=',$table)->get();
                    log::info('---------------------------------------------------------------------');
                    log::info(count($tableExist));
                    //$nn=FranchisesTable::excecute();

                 //    return $view;

                        // $columns = Schema::getColumnListing('Audit_Frans');
                        if(count($tableExist)>0&&($model::first()!=null)){
                        $columns = array_keys($model::first()->toArray());
                        $rows=$model::all();

                }
                else {

                        $columns=[];
                        $rows=[];
                  //   return "<div class='alert alert-danger'>
                                //      <p> No data Found </p>
                                // </div>";

                }
                AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                // \Config::set('database.connections.mysql.database', 'VAMOSYS');
                //      DB::purge('mysql');
                //      foreach($columns as $key => $value) {

                //     //echo $value." ";

                // }
                // foreach($rows as $row) {

                // foreach($columns as $key => $value) {

                //     echo $row[$value]." ";

                // }

                //    echo $row." -------------------------------------------------";



                // }


                }
                catch (customException $e) {
                  //display custom message
                  log::info($e->errorMessage());
                }
                unset( $columns[array_search( 'id', $columns )],$columns[array_search( 'fcode', $columns )] );
                //unset( $columns[array_search( 'fcode', $columns )] );
         //unset( $columns[array_search( 'addLicence', $columns )] );
            $userName = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
                    $details = $redis->hget('H_Franchise',$userName);
                        $details_json=json_decode($details,true);
                        $isPrePaid1=ucfirst(isset( $details_json['prepaid'])? $details_json['prepaid']:'No');
                        if($isPrePaid1=="Yes")  {
                                $isPrePaid="Prepaid";
                                //unset( $columns[array_search( 'addLicence', $columns )] );
                        }
                        else {
                                $isPrePaid="Normal";
                        }


                return view ( 'vdm.franchise.audit', array ('columns' => $columns,'rows' => $rows ) );

                }
        public function dateModification(){
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $username = Auth::user ()->username;
                $redis = Redis::connection ();
                $fcodeArray = $redis->smembers('S_Franchises');
                foreach ($fcodeArray as $key => $fcode) {
                        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                        $franchiseDetails=json_decode($franDetails_json,true);
                        $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
                        if($prepaid=='yes'){
                                $licenceList=$redis->hkeys('H_LicenceExpiry_'.$fcode);
                                foreach ($licenceList as $key => $LicenceId) {
                                        $LicenceRef=$redis->hget('H_LicenceExpiry_'.$fcode,$LicenceId);
                                        $LicenceRefData=json_decode($LicenceRef,true);
                                        log::info('befor Licence Data '.$LicenceId);
                                        log::info($LicenceRefData);
                                        $LicenceissuedDate=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:'';
                                        $LicenceOnboardDate=isset($LicenceRefData['LicenceOnboardDate'])?$LicenceRefData['LicenceOnboardDate']:'';
                                        $LicenceExpiryDate = date("d-m-Y", strtotime(date("d-m-Y", strtotime($LicenceissuedDate)) . " + 1 year"));
                                        $vehicleExpiry = date("Y-m-d", strtotime(date("Y-m-d", strtotime($LicenceissuedDate)) . " + 1 year"));
                                        $LiceneceDataArr=array(
                                'LicenceissuedDate'=>$LicenceissuedDate,
                                'LicenceOnboardDate' =>$LicenceOnboardDate,
                                'LicenceExpiryDate' =>$LicenceExpiryDate,
                        );
                        $LicenceExpiryJson = json_encode ( $LiceneceDataArr );
                        log::info('after Licence Data '.$LicenceId);
                        log::info($LiceneceDataArr);
                        $redis->hmset ( 'H_LicenceExpiry_'.$fcode, $LicenceId,$LicenceExpiryJson);
                        $vehicleId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId);
                        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
                                $vehicleRefData=json_decode($vehicleRefData,true);
                                log::info('before date modified vehicle ref data'.$vehicleId);
                                log::info($vehicleRefData);
                                $vehicleRefData[""]=$vehicleExpiry;
                                $refDataJson=json_encode ( $vehicleRefData );
                                log::info('after date modified vehicle ref data'.$vehicleId);
                                log::info($vehicleRefData);
                                $redis->hmset ( 'H_RefData_'.$fcode, $vehicleId,$refDataJson);

                                }

                }
                }

        Session::flash ( 'message',' successfully modified !!!');
                return Redirect::to ( 'vdmFranchises' );

}
public function liveVehicleCount()
{
        log::info('inside the get live vehicle count');
        if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
        }
        $redis = Redis::connection ();
        $count = count($redis->keys ( 'NoData:*' ));
        return Redirect::to ( 'vdmFranchises' );

}

public function enableVehicleExpiry($fcode,$option)
{
        log::info('inside the enableVehicleExpiry fcode '.$fcode.' option '.$option);
        if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
        }
        $redis = Redis::connection ();
        $username = Auth::user ()->username;
        $msg='not successfully modified';
        if($option=='no')       {
                $redis->sadd('S_EnableVehicleExpiry',$fcode);
                $msg='successfully enabled';
        }       elseif($option=='yes')  {
                $redis->srem('S_EnableVehicleExpiry',$fcode);
                $msg='successfully disabled';
        }
        Session::flash ( 'message',$msg);
        return Redirect::to ( 'vdmFranchises' );
}
public function updatetimezone()
{
        log::info('inside the updatetimezone fcode ');
        if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
        }
        $redis = Redis::connection ();
        $username = Auth::user ()->username;
        $timezonekeys=$redis->keys( 'S_Timezone_Updated_*');
  foreach ($timezonekeys as $key => $value) {
     $redis->del($value);
  }
  $validity=1*86400;
  $redis->set('Update:Timezone','24hrs');
  $redis->expire('Update:Timezone',$validity);

        Session::flash ( 'message','TimeZone updated Successfully');
        return Redirect::to ( 'vdmFranchises' );
}

public function updateapn()
{
        log::info('inside the updateapn fcode ');
        if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
        }
        $redis = Redis::connection ();
        $username = Auth::user ()->username;
        $apnkeys=$redis->keys( 'S_Apn_Updated_*');
  foreach ($apnkeys as $key => $value) {
     $redis->del($value);
  }
  $validity=1*86400;
  $redis->set('Update:Apn','24hrs');
  $redis->expire('Update:Apn',$validity);

        Session::flash ( 'message','APN Updated successfully');
        return Redirect::to ( 'vdmFranchises' );
}

}
