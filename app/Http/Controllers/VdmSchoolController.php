<?php
namespace App\Http\Controllers;

use Request;
use App;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

class VdmSchoolController extends Controller {
 
 /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if(!Auth::check()) {
            return Redirect::to('login');
        }
        $username = Auth::user()->username;
        $redis = Redis::connection();
        
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        
         return view('vdm.schools.create');
    }
    
    public function store()
    {
        
        if(!Auth::check()) {
            return Redirect::to('login');
        }
        $username = Auth::user()->username;
        $rules = array(
                'schoolId'       => 'required',
                'routes' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('vdmSchools/create')
            ->withErrors($validator);
            
        } else {
            // store
            
            $schoolId       = Input::get('schoolId');
            $routes      = Input::get('routes');
            
            $redis = Redis::connection();
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            $redis->sadd('S_Schools_'. $fcode, $schoolId);
            
            $routesArr = explode(",",$routes);
            
            foreach($routesArr as $route) {
                if(empty($route)) {
                    continue;
                }
                $redis->sadd('S_School_Route_'.$schoolId .'_'. $fcode, $route);
            }

            // redirect
            Session::flash('message', 'Successfully created ' . $schoolId . '!');
            return Redirect::to('vdmSchools');
        }
        
    }

    public function index() {
        if (! Auth::check ()) {
            return Redirect::to ( 'login' );
        }
        $username = Auth::user ()->username;
        
        log::info( 'User name  ::' . $username);
        
        
        $redis = Redis::connection ();
        
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        
        Log::info('fcode=' . $fcode);
        
        $schoolListId = 'S_Schools_' . $fcode;
        
        Log::info('schoolListId=' . $schoolListId);

        $schoolList = $redis->smembers ( $schoolListId);
        
       
        
        foreach ( $schoolList as $school ) {
            
        
            //TODO --- more details obtained here
        }
        
       Log::info('Forwarding to schools index view');
         
        return view ( 'vdm.schools.index', array (
                'schoolList' => $schoolList 
        ) );
    }
    
 
    
}
