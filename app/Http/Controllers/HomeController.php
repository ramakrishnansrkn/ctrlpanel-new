<?php

namespace App\Http\Controllers;

//use App\Http\Requests\Request;
//use App\Http\Requests;
//use Illuminate\Http\Request;
use Request;
use App;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use Config,DB, Lang;

class HomeController extends Controller {

public function showLogin(){
  $mobileNo = Input::get('mobileNo');
  if(is_null($mobileNo)) $mobileNo="";
  $current_link = $_SERVER['HTTP_HOST'];
  $current_link1=HomeController::get_domain($current_link);
  $url='no';
  $webLogo=null;
  if(strpos(URL::current(),'gpsvts.net') == true || strpos(URL::current(),'gpsvts.vamosys.com') == true){
    $url='yes';
    return view('loginNew',['mobileNo'=>$mobileNo])->with('mobileNo',$mobileNo)->with('url',$url)->with('webLogo',$webLogo);
  }
  Config::set('constant.bucket','vamosys');
  $s3 = App::make('aws')->createClient('s3', [
      'credentials'=>['key'=>'DO006TFWNVG9EVBYDXQH','secret'=>'j0nlvDa2JH1vrrfjyMCbTTpRBdDkT5VUp6fYzzup/QI'],
                        'endpoint' => 'https://sgp1.digitaloceanspaces.com',
                ]);
                $objects = $s3->getIterator('ListObjects', array(
                'Bucket' => Config::get('constant.bucket'),
                                'Prefix' => 'picture/'.$current_link1,
       ));
  
  foreach ($objects as $object) {
    if (preg_match("/".$current_link1.".(jpg|png|gif|jpeg|JPG|PNG|GIF|JPEG)/", $object['Key'])){
      $webLogo=Config::get('constant.endpoint').'/'.Config::get('constant.bucket').'/'.$object['Key'];
    }
  }
  try {

      $login_data = DB::table('login_Customisation')->where('Domain',$current_link1)->get();
      $template=$login_data[0]->Template;
      $bcolor =$login_data[0]->bcolor;
      $bcolor1 =$login_data[0]->bcolor1;
      $fcolor = $login_data[0]->fcolor;
      $logo = $login_data[0]->logo;
	  $themecolor = $login_data[0]->themecolor;
        if($themecolor == null){
            $themecolor = "";
        }
      $backgrounds = $login_data[0]->background;
      if($template==1){
         return view('vdm.login.template1',['mobileNo'=>$mobileNo])->with('bcolor',$bcolor)->with('bcolor1',$bcolor1)->with('fcolor',$fcolor)->with('backgrounds',$backgrounds)->with('logo',$logo)->with('url',$url)->with('themecolor',$themecolor);
      }else if($template==2){
         return view('vdm.login.template2',['mobileNo'=>$mobileNo])->with('bcolor',$bcolor)->with('bcolor1',$bcolor1)->with('fcolor',$fcolor)->with('backgrounds',$backgrounds)->with('logo',$logo)->with('url',$url)->with('themecolor',$themecolor);
      }else if($template==3){
         return view('vdm.login.loginTemp3',['mobileNo'=>$mobileNo])->with('bcolor',$bcolor)->with('fcolor',$fcolor)->with('logo',$logo)->with('url',$url)->with('themecolor',$themecolor);
      }else if($template==6){
		return view('vdm.login.template6',['mobileNo'=>$mobileNo])->with('bcolor',$bcolor)->with('bcolor1',$bcolor1)->with('fcolor',$fcolor)->with('backgrounds',$backgrounds)->with('logo',$logo)->with('url',$url)->with('themecolor',$themecolor);
	 }else{
        return view('login',['mobileNo'=>$mobileNo])->with('url',$url)->with('webLogo',$webLogo);
      }
       

  }catch(\Exception $e){
      log::info('Exception');
      log::info($e);
      if (Auth::check()) {
          $username = Auth::user ()->username;
          $redis = Redis::connection ();
          $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
          if($fcode=="GPSVA"){
              $d=$redis->sismember ('S_Dealers_' . $fcode, $username);
              if($d==1 || $username=='gpsvtsadmin'){
                  return  Redirect::to('Business');
              }
          }else if(strpos($username,'admin')!==false ) {
        return  Redirect::to('Business');
          }else {
          $redis = Redis::connection ();
              $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
              $val1= $redis->sismember ( 'S_Dealers_' . $fcode, $username );
              $val = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
              if($val1==1 && isset($val)) {
                  Log::info('---------------is dealer adminauth:--------------');
          return  Redirect::to('DashBoard');
              }
          }
          return Redirect::to ('track');
      }
    //return view('login')->with('url',$url)->with('webLogo',$webLogo);
	if(strpos(URL::current(),'gpsvts.net') == true || strpos(URL::current(),'gpsvts.vamosys.com') == true){ 
		return view('loginNew',['mobileNo'=>$mobileNo])->with('url',$url)->with('webLogo',$webLogo);
	} else {
		return view('login',['mobileNo'=>$mobileNo])->with('url',$url)->with('webLogo',$webLogo);
	}
    }
}

public function doLoginWithMobile($mobileNo)
{
  $curl = curl_init();
  $totp = Input::get('password');
  $userId = Input::get('userName');
  $redis = Redis::connection ();
  $ip = $redis->get("ipaddress");
  $datas =[
    "mobileNumber"=>$mobileNo,
    "totp"=>$totp,
    "userId"=>$userId
  ];
  curl_setopt_array($curl, array(
    CURLOPT_URL => "$ip:9000/checkTotpForAuthenticator?".http_build_query($datas),
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
  ));

  $response = curl_exec($curl);
  curl_close($curl);
  $data = json_decode($response,true);
  $response  = isset($data['response'])?$data['response']:'';
  $password ="";
  if($response === "failure"){
    $error = $data['error'];
    return Redirect::back()->with('flash_notice',$error)->withInput(Input::except('password'));
  }
  if($response === "Success"){
    $password = $data['data'];
    $password = base64_decode($password);
    $userdata = array(
      'userName' 	=> $userId,
      'password' 	=> $password
    );
    return HomeController::doLogin($userdata);
  }
  return Redirect::back()->with('flash_notice',"Something Went Wrong")->withInput(Input::except('password'));
}

 /* public function showLogin()
    {
        if (Auth::check()) {
            return redirect('Business');
        }
        log::info('homecontroller.showlogin');
        $url='no';
        if(strpos(URL::current(),'localhost') == true)
        {
            $url='yes';
        }
        $webLogo=null;
        // show the form
        return view('login')->with('url',$url)->with('webLogo',$webLogo);
    } */


        public function admin()
	{
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		
		$username = Auth::user ()->username;
		if($username !='vamos') {
			return Redirect::to('login')->with('flash_notice', 'Unauthorized user. Futher attempts will be
					treated as hacking, will be prosecuted under Cyber laws.');
		
		}
		else {
			$redis = Redis::connection ();
			$apnKey = $redis->exists('Update:Apn');
            $timezoneKey = $redis->exists('Update:Timezone');
            $count = VdmFranchiseController::liveVehicleCountV2();
			return view('admin')->with('apnKey',$apnKey)->with('timezoneKey',$timezoneKey)->with('count',$count);
		}
	}


    public function ipAddressManager()
	{
		
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		
		$username = Auth::user ()->username;
		if($username !='vamos') {
			return Redirect::to('login')->with('flash_notice', 'Unauthorized user. Futher attempts will be
					treated as hacking, will be prosecuted under Cyber laws.');
		
		}

		$redis = Redis::connection ();
		$ipAddress = $redis->hget('H_IP_Address','ipAddress');
		$gt06nCount = $redis->hget('H_IP_Address','gt06nCount');
		$tr02Count = $redis->hget('H_IP_Address','tr02Count');
		$gt03aCount = $redis->hget('H_IP_Address','gt03aCount');
		$apnKey = $redis->exists('Update:Apn');
        $timezoneKey = $redis->exists('Update:Timezone');
		$count = VdmFranchiseController::liveVehicleCountV2();
		return view ( 'IPAddress', array (
				'ipAddress' => $ipAddress ) )->with ( 'gt06nCount', $gt06nCount )->with('tr02Count',$tr02Count)->with('gt03aCount',$gt03aCount)->with('apnKey',$apnKey)->with('timezoneKey',$timezoneKey)->with('count',$count);
		
		
	}

        public function saveIpAddress() {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }

                $username = Auth::user ()->username;
                if($username !='vamos') {
                        return Redirect::to('login')->with('flash_notice', 'Unauthorized user. Futher attempts will be
                                        treated as hacking, will be prosecuted under Cyber laws.');

                }

                $rules = array (
                                'ipAddress' => 'required',
                                'gt06nCount' => 'numeric',
                                'tr02Count' => 'numeric',
                                'gt03aCount' => 'numeric'

                );
                $validator = Validator::make ( Input::all (), $rules );
                if ($validator->fails ()) {
                        return Redirect::to ( 'ipAddressManager' )->withErrors ( $validator );
                } else {
                        $redis = Redis::connection ();
                        $ipAddress= Input::get ( 'ipAddress' );
                        $gt06nCount= Input::get ( 'gt06nCount' );
                        $tr02Count= Input::get ( 'tr02Count' );
                        $gt03aCount= Input::get ( 'gt03aCount' );
                        $redis->hmset('H_IP_Address','ipAddress',$ipAddress,'gt06nCount',$gt06nCount,
                                'tr02Count',$tr02Count,'gt03aCount',$gt03aCount);

             $init=$redis->lindex('L_GT06N_AVBL_PORTS',0);
             $currentCount   = isset($init)?$init:10000;

             $endCount = $currentCount+$gt06nCount;

             for($count=$currentCount;$count<=$endCount;$count++) {
                   $redis->rpush('L_GT06N_AVBL_PORTS',$count);
             }
                }
                Session::flash ( 'message', 'Successfully added ipAddress details'. '!' );

                return Redirect::to ( 'admin' );

        }

        public function reverseGeoLocation()
        {
                Log::info("Into reverse Geo Location");
                $lat = Input::get('lat');
                $lng=Input::get('lng');

                Log::info("lat" . $lat . 'lng : ' . $lng);
                //https://maps.google.com/maps/api/geocode/json?latlng

                $url = "https://maps.google.com/maps/api/geocode/json?latlng=".$lat.",".$lng."&key=AIzaSyBQFgD9_Pm59zGz0ZfLYCUiH_7zbuZ_bFM";
                $data = @file_get_contents($url);
                $jsondata = json_decode($data,true);
                if(is_array($jsondata) && $jsondata['status'] == "OK")
                {
                        Log::info("address:" . $jsondata['results']['1']['formatted_address']);
                        echo $jsondata['results']['1']['formatted_address'];
                }
                else {
                        Log::info("empty");
                }

        }

        public function livelogin()
        {
                // show the form
                return view('livelogin');
        }

public function get_domain($url){
   $nowww = str_replace('www.','',$url);
   $domain = parse_url($nowww);
   if(!empty($domain["host"])){
                return $domain["host"];
   } else{
        return $domain["path"];
   }
}

	public function doLogin($userdata = null){
		Session::put('lang',Input::get('lang'));
		$rules = array(
			'userName'    => 'required', 
			'password' => 'required|min:3'
		);
		Log::info('do Login');
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);
		$remember = (Input::has('remember')) ? true : false;
		// if the validator fails, redirect back to the form
		if (is_null($userdata) && $validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		}	else {
			// create our user data for the authentication
      if(is_null($userdata)){
				$userdata = array(
					'userName' 	=> Input::get('userName'),
					'password' 	=> Input::get('password')
				);
			}
			Log::info('Login details ' . Input::get('userName') .' '. Input::get('password') );
			// attempt to do the login
			if (Auth::attempt($userdata,$remember)) {
				log::info(' inside the login ');
                $username = Auth::user()->username;
                $redis = Redis::connection();
                $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
                $url = Request::url();
                Log::info('Login details $url ' . $url);
				        if($redis->sismember('S_Franchises_Disable',$fcode)) {
                  Auth::logout();
                    return Redirect::to('login')
               			 ->withInput(Input::except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
                }
                $lock = $redis->hget('H_UserId_Cust_Map', $username . ':lock');
                if(!is_null($lock) && strtolower($lock) == "disabled"){
                  Auth::logout();
                    return Redirect::to('login')
                    ->withInput(Input::except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
                }
                $disableDeler=$redis->smembers('S_Dealers_'.$fcode);
                foreach ($disableDeler as $key => $dealerId) {
                    $lock= $redis->hget ( 'H_UserId_Cust_Map', $dealerId . ':lock');
                    if($lock=='Disabled'){
    				        $dealerusersList = 'S_Users_Dealer_'.$dealerId.'_'.$fcode;
                   	    if($redis->sismember($dealerusersList,$username)){
							Auth::logout();
                   	        return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
                   	    }
                    }
                }
                $current_link = $_SERVER['HTTP_HOST'];
    	        $domainName=HomeController::get_domain($current_link);
				$val1= $redis->sismember ( 'S_Dealers_' . $fcode, $username );
		        $val = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
				if($domainName=="cpanel.gpsvts.net"  || $domainName=="cpanel.vamosys.com" ){
                  	if($username=='vamos'){	
                   		Auth::logout(); 		
				   		$byPass='no';		
				        return View::make('vdm.login.otplogin')->with('username',$username)->with('byPass',$byPass);  
				   	}
                  	if(strpos($username,'admin')!==false ){
                  		log::info( 'inside if filter adminauth' . $username) ;
                    	if($username=='smpadmin' || $username=='vamoadmin' || $username=='SPVAMOadmin'){	
							Auth::logout(); 		
				        	$byPass='no';		
				            return View::make('vdm.login.otplogin')->with('username',$username)->with('byPass',$byPass);             						
			         	}
                    	return  Redirect::to('Business');
                  	}else if($val1==1 && isset($val)){
			            Log::info('---------------is dealer adminauth:--------------'.$username);			
                        $detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $username);
                        $detail=json_decode($detailJson,true);
						/*    if(isset($detail['website'])==1){
                              $website=$detail['website'];
                            }else{
                              $website='';
                            } */
							$web=isset($detail['website'])?preg_replace('/www./', '', $detail['website'], 1):'gpsvts.net';
							if($web==null && $web=='')	{
								$web='gpsvts.net';
							}
              if($web=='gpsvts.net'){
                $web='gpsvts.vamosys.com';
              }
							if($domainName !== $web )	{
									log::info('----- Domain Name Fails ------------'.$username);
									Auth::logout();
									return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
							}
                            $domain=HomeController::get_domain($web);
                            if($domain=='gpsvts.vamosys.com'){
                              Session::put('curdomin','gps');
                            }else{
                              Session::put('curdomin','notgps');
                            }
                         	return  Redirect::to('DashBoard');
                  		}else{
                  			if (Auth::check()){
      							Auth::logout();
    						}
                  			return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Entered Username is not an admin User !!');
                  		}
                }
                else if((strpos($username,'admin')!==false) || ($username=='vamos')) {
					Log::info('---------------is adminauth &:--------------'.$username);
					$addetails=$redis->hget('H_Franchise',$fcode);
					$refData    = json_decode($addetails, true);
			        //$web=isset($refData['website'])?$refData['website']:'';
			        $web=isset($refData['website'])?preg_replace('/www./', '', $refData['website'], 1):'gpsvts.net';
					if($web==null && $web=='')	{
						$web='gpsvts.net';
				    }
          if($web=='gpsvts.net'){
              $web='gpsvts.vamosys.com';
            }
					log::info($domainName);
					log::info($web);
					$exists = strpos($domainName,$web);
					if($exists !== 0) {
						log::info('----- admin Domain Name Fails ------------'.$username);
				        Auth::logout();
						return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
					} else {
         //     $key=md5(microtime(true).mt_Rand());
         //     $redis->set ($username.':key',$key);
          //      $redis->EXPIRE($username.':key',60);
		 //   $userNAme = HomeController::encrypt($username,"XSBRMLT");
          //    $pwd=HomeController::encrypt(Input::get('password'),$key);
           //   $dmn=HomeController::encrypt($domainName,$key);
		//	  $lng = Lang::locale();
		  $key = md5(microtime(true) . mt_Rand());
                    $redis->set($key, $userdata['userName']);
                    $redis->expire($key, 160);
              Auth::logout();
             return Redirect::to('https://cpanel.vamosys.com/login/loginpass:' . $key);
             // return Redirect::to('https://cpanel.vamosys.com/cpanel/public/loginpass/'.$userNAme.'/'.$pwd.'/'.$dmn.'?lang='.$lng);
			  
					}
				}
				else if ($val1==1 && isset($val)) {
					Log::info('---------------is dealer &:--------------'.$username);	
					$detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $username);
					$detail=json_decode($detailJson,true);
					$web=isset($detail['website'])?preg_replace('/www./', '', $detail['website'], 1):'gpsvts.net';
					if($web==null && $web=='')	{
						$web='gpsvts.net';
					}
          if($web=='gpsvts.net'){
            $web='gpsvts.vamosys.com';
          }
					log::info($domainName);
					log::info($web);
					$exists = strpos($domainName,$web);
					//if($domainName !== $web )	{
					if($exists !== 0) {
						log::info('----- Domain Name Fails ------------'.$username);
						Auth::logout();
						return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
					}
					$subdomain = "cpanel.vamosys.com";
					if(isset($detail['subDomain'])==1) {      
						$subdomain=isset($detail['subDomain'])?$detail['subDomain']:'cpanel.vamosys.com';
						log::info($subdomain);
					}
					if ($subdomain == null || $subdomain == '')  {
						$subdomain='cpanel.vamosys.com';
					}
				/*	$key=md5(microtime(true).mt_Rand());
					$redis->set ($username.':key',$key);
					$redis->EXPIRE($username.':key',60);
					Auth::logout();
					$userNAme = HomeController::encrypt($username,"XSBRMLT");
					$pwd=HomeController::encrypt(Input::get('password'),$key);
					$dmn=HomeController::encrypt($domainName,$key); */
					
					$key = md5(microtime(true) . mt_Rand());
                    $redis->set($key, $userdata['userName']);
                    $redis->expire($key, 160);
					Auth::logout();
            			 			 
					//return Redirect::to('http://cpanel.gpsvts.net/cpanel/public/loginpass/'.$userNAme.'/'.$pwd.'/'.$dmn);
				//	$lng = Lang::locale();
          if($domainName == "gpsvts.vamosys.com"){
					  return Redirect::to('https://'.$subdomain.'/login/loginpass:' . $key);
					           }else{
            return Redirect::to('http://'.$subdomain.'/login/loginpass:' . $key);
			          }
          }else {
					Log::info('---------------is enduserauth:--------------'.$username);
					
					$userDomainName = HomeController::getUserDomainName($fcode,$username);
                    log::info('domainName '.$domainName.' userDomainName '.$userDomainName);
					//if(strpos($domainName,$userDomainName) !== false )	{
					if(strpos('true',$userDomainName) !== false )	{
						if($fcode == 'GPSVA') {
							Auth::logout();
							return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'You are restricted in web application. Please use Mobile application');
						}
						log::info('domainName '.$domainName.' userDomainName '.$userDomainName);
						$userPrePage=$redis->hget ( 'H_UserId_Cust_Map', $username . ':userPre');
						$groups = $redis->smembers($username);
						if($groups == null ) {
							log::info('Empty user or groups '.$username);
							Auth::logout();
						return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Empty Username or Groupname, Please enter valid username');
						}
						$vehicles = $redis->smembers($groups[0]);
						if($vehicles == null)  {
							log::info('Empty groups '.$username);
							Auth::logout();
						return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Empty Username or Groups, Please enter valid username');
						}
						$vehicles = $redis->smembers($groups[0]);
						Session::put('group',$groups[0]);
						Session::put('vehicle',$vehicles[0]);
						Session::put('userPrePage',$userPrePage);
            if(strpos(URL::current(),'gpsvts.net') == true ){
              $key=md5(microtime(true).mt_Rand());
              $redis->set ($username.':key',$key);
              $redis->EXPIRE($username.':key',60);
              Auth::logout();
              $userNAme = HomeController::encrypt($username,"XSBRMLT");
              $pwd=HomeController::encrypt(Input::get('password'),$key);
              $dmn=HomeController::encrypt($domainName,$key);
			  $lng = Lang::locale();
              return Redirect::to('https://gpsvts.vamosys.com/gps/public/loginpass/'.$userNAme.'/'.$pwd.'/'.$dmn.'?lang='.$lng);
            }else{
                $password = is_null(Input::get('password'))?$userdata["password"]:Input::get('password');
                
                if(strlen($password) <6){
                  return Redirect::to('passWd?userlevel=fromLogin');
                }
              if($userPrePage==""||$userPrePage==null){
                return  Redirect::to('track');
              }else{
                return  Redirect::to('userPage');
              }
            }
						
					}	else {
						log::info('----- Domain Name Fails ------------');
						Auth::logout();
						return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
					}	
                    
				}
                    
			} 	else {	 
				// validation not successful, send back to form
				return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
			}
      
		}
	}




/*      public function getApi()
        {

                log::info(' inside the api key ');
                $username = Input::get ( 'id' );
                $redis = Redis::connection ();
                $fcodeKey = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
                $franchiseDetails = $redis->hget( 'H_Franchise', $fcodeKey);
                $getFranchise=json_decode($franchiseDetails,true);
                // log::info($getFranchise);

                if(isset($getFranchise['apiKey'])==1)
                        $apiKey=$getFranchise['apiKey'];
                else
                        $apiKey='';

         return $apiKey;
        }
*/

    public function getUserIP() {

    log::info('inside the getUserIP');
    $userIP = Input::get ('userIP');
    Session::put('userIP',$userIP);
   return $userIP;
  }
    public function getFcode() {

	  log::info('inside the fcode');
		$usernames = Input::get ('id');
    $userIP = Input::get ('userIP');
    Session::put('userIP',$userIP);
		$redis    = Redis::connection();
	  //log::info('user names.......'.$usernames);
		$fcodeKeys   = $redis->hget ( 'H_UserId_Cust_Map',$usernames.':fcode' );

	 return $fcodeKeys;
	}

    public function getApi()
{
log::info(' inside the api key ');
    $apiKey = '';
$username = Input::get ('id');
$redis    = Redis::connection();
$fcodeKey   = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
$dealerName = $redis->hget ( 'H_UserId_Cust_Map', $username . ':OWN' );
log::info('------------------- Dealer Name : '.$dealerName.'--------------------------------' );
if( $dealerName !='' && $dealerName !='admin'  ){
log::info('-------------- inside dealer ------------------');  
$detailJson     = $redis->hget ( 'H_DealerDetails_' . $fcodeKey, $dealerName);
$detailsDealer  = json_decode($detailJson,true); 
// log::info( $detailsDealer );
if (strpos($_SERVER['SERVER_NAME'], 'www.') !== false) {
          // $hostName=str_replace("www.","",$_SERVER['SERVER_NAME']);
          $hostName=preg_replace('/www./', '', $_SERVER['SERVER_NAME'], 1);
       }else{
          $hostName = $_SERVER['SERVER_NAME'];
       }
      log::info('dealer  Website:'.$hostName);
      if($hostName==preg_replace('/www./', '', $detailsDealer['website'], 1)){
                   if(isset($detailsDealer['mapKey'])==1) $apiKey=$detailsDealer['mapKey'];
        }else if($hostName==preg_replace('/www./', '', $detailsDealer['website1'], 1)){
                    if(isset($detailsDealer['mapKey1'])==1) $apiKey=$detailsDealer['mapKey1'];
        }else{
                  $apiKey=HomeController::getFranchiseApi($fcodeKey);
              }

      if( $apiKey == '') {
          $apiKey=HomeController::getFranchiseApi($fcodeKey);
        }
// if(isset($detailsDealer['mapKey'])==1){
// log::info('-------------- inside dealer if...------------------');  
// $apiKey = $detailsDealer['mapKey'];
// //log::info( $detailsDealer );
// if( $apiKey == '') {
// $apiKey=HomeController::getFranchiseApi($fcodeKey);
// }
//    } else{
// $apiKey=HomeController::getFranchiseApi($fcodeKey);
// }
} else {
$apiKey=HomeController::getFranchiseApi($fcodeKey);
}
log::info('------------------------- return api value starts -------------------------------');
log::info($apiKey);
log::info('------------------------- return api value ends -------------------------------');
return $apiKey;
}

	public function getFranchiseApi($fcodeKey)
	{
		log::info('-------------- getFranchiseApi ------------------'.$fcodeKey);
		//strpos(URL::current(),$web) !== false)
		//log::info(URL::current());
		$redis = Redis::connection ();
		//$hostName = $_SERVER['SERVER_NAME'];
    // if (strpos($_SERVER['SERVER_NAME'], 'www.') !== false) {
    //   $hostName=str_replace("www.","",$_SERVER['SERVER_NAME']);
    //  }
		if (strpos($_SERVER['SERVER_NAME'], 'www.') !== false) {
          // $hostName=str_replace("www.","",$_SERVER['SERVER_NAME']);
          $hostName=preg_replace('/www./', '', $_SERVER['SERVER_NAME'], 1);
       }else{
          $hostName = $_SERVER['SERVER_NAME'];
       }
      
    // if (strpos($_SERVER['SERVER_NAME'], 'www.') !== false) {
    //   $hostName=str_replace("www.","",$_SERVER['SERVER_NAME']);
    //  }
		log::info('Franchise Website '. $hostName);
		$franchiseDetails = $redis->hget( 'H_Franchise', $fcodeKey);
		$getFranchise=json_decode($franchiseDetails,true);
		$websites=array(isset($getFranchise['website'])?preg_replace('/www./', '', $getFranchise['website'], 1):'',isset($getFranchise['website2'])?preg_replace('/www./', '', $getFranchise['website2'], 1):'',isset($getFranchise['website3'])?preg_replace('/www./', '', $getFranchise['website3'], 1):'',isset($getFranchise['website4'])?preg_replace('/www./', '', $getFranchise['website4'], 1):'');	
		//$franchiseDetails = $redis->hget( 'H_Franchise', $fcodeKey);
		//$getFranchise=json_decode($franchiseDetails,true);
		//$websites=array(isset($getFranchise['website'])?$getFranchise['website']:'',isset($getFranchise['website2'])?$getFranchise['website2']:'',isset($getFranchise['website3'])?$getFranchise['website3']:'',isset($getFranchise['website4'])?$getFranchise['website4']:'');
		if (in_array($hostName, $websites))	{
			$key = array_search($hostName, $websites);
			if($key == 0)  {
				$apiKey=$getFranchise['apiKey'];
			}
			else  {
				$apiIndex=(int)$key+1;
				$apiKey=$getFranchise['apiKey'.$apiIndex];
			}
		}
		else{
			$apiKey='';
		}
		log::info($apiKey);
		return $apiKey;
	}

	public function track(){
		
		return view('track');
	}

	
    public function adhocMail() {
        return view('vls.adhocmail');
    }

    public function sendAdhocMail() {
        
        
        Log::info(" inside send adhoc mail");
        $userId  = Input::get('toAddress');
        $ccAddress  = Input::get('ccAddress');
        $subject  = Input::get('subject');
        $body  = Input::get('body');
        
         Mail::queue('emails.welcome', array('fname'=>$userId,'userId'=>$userId,'password'=>$password), function($message)
            {
                $message->to(Input::get('toAddress'))->subject(Input::get('subject')) ;
            });
            
        
        Session::flash ( 'message', 'Mail sent ' . $to . '!' );    
        return view('vls.adhocmail');
    }


/*    public function authName() {

    	log::info(' inside the api key ');
    	$assetValue = array();
		$username = Auth::user()->username;
		$redis = Redis::connection ();
		$fcodeKey = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$franchiseDetails = $redis->hget( 'H_Franchise', $fcodeKey);
		$getFranchise=json_decode($franchiseDetails,true);
		// log::info($getFranchise);
		
		if(isset($getFranchise['apiKey'])==1)
			$apiKey=$getFranchise['apiKey'];
		else
			$apiKey='';

		$assetValue[] = $apiKey;
		$assetValue[] = $username;

        return $assetValue;
     } 

    */

    public function authName() {
		log::info(' inside the api key ');
		$assetValue = array();
		$username   = Auth::user()->username;
		$redis      = Redis::connection();
		$fcodeKey   = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$dealerName = $redis->hget ( 'H_UserId_Cust_Map', $username . ':OWN' );
		log::info('------------------- Dealer Name : '.$dealerName.'--------------------------------' );
		if( $dealerName !='' && $dealerName !='admin'  ){
			log::info('-------------- inside dealer ------------------');
			$detailJson     = $redis->hget ( 'H_DealerDetails_' . $fcodeKey, $dealerName);
			$detailsDealer  = json_decode($detailJson,true); 
			// log::info( $detailsDealer );
			if(isset($detailsDealer['mapKey'])==1){
				log::info('-------------- inside dealer if...------------------');  
				$apiKey = $detailsDealer['mapKey'];
				//log::info( $detailsDealer );
				if( $apiKey !='') {
					$assetValue[] = $apiKey;
					$assetValue[] = $username;
				}
				else{
					$apiKey=HomeController::getFranchiseApi($fcodeKey);
					$assetValue[] = $apiKey;
					$assetValue[] = $username;
				}
			}
			else{
				$apiKey=HomeController::getFranchiseApi($fcodeKey);
		        $assetValue[] = $apiKey;
		        $assetValue[] = $username;
		    }
        }else{
			$apiKey=HomeController::getFranchiseApi($fcodeKey);
			$assetValue[] = $apiKey;
		    $assetValue[] = $username;

       }
	   log::info('------------------------- return api value starts -------------------------------');
       log::info($assetValue); 
       log::info('------------------------- return api value ends -------------------------------');
       return $assetValue;
    }


public function getDealerName() {
    $username   = Auth::user()->username;
		$redis      = Redis::connection();

	    $dealerName = $redis->hget ( 'H_UserId_Cust_Map', $username . ':OWN' );
		
     return $dealerName;
	}
	public function getDealer() {

		log::info('------------------------- Get Dealer Name  -------------------------------');

    $username    =  Input::get('id');
		$redis       =  Redis::connection();
		$dealerName  =  $redis->hget('H_UserId_Cust_Map', $username.':OWN');

     return $dealerName;
	}


	public function doLogout()
	{
    Session::put('curdomin','notgps');
		if(Session::get('curSwt')=='switch'){
			if (! Auth::check ()) {
        			return Redirect::to ( 'login' );
    	    }
    		$username = Auth::user ()->username;
			$redis = Redis::connection ();
    		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    		$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
            $franchiseDetails=json_decode($franDetails_json,true);
            $userId=isset($franchiseDetails['userId'])?$franchiseDetails['userId']:'';
            if($userId!=null){
            	 $user=User::whereRaw('BINARY username = ?', [$userId])->firstOrFail();
        		log::info( '--------new name----------' .$user);
        		Auth::login($user);
        		Session::put('curSwt','switchLog');
        		return Redirect::to ( 'Business' );
            }else{
            	Auth::logout(); 
            	return Redirect::to('login');
            }

		}
    if(Session::get('frnSwt')=='fransswitch'){
	    	$user=User::whereRaw('BINARY username = ?', ['vamos'])->firstOrFail();
        	Auth::login($user);
        	Session::put('curSwt','switchLog');
        	Session::put('frnSwt','switchL0G');
        	return Redirect::to ( 'vdmFranchises' );
	   }
     
    /* if(Session::get('userSwith')!=null){
      $session_Data=explode(":",Session::get('userSwith'));
      Session::put('userSwith',null);
      // $username=Session::get('userSwith');
      $username=$session_Data[0];
      Session::put('curSwt',$session_Data[1]);
      Session::put('frnSwt',$session_Data[2]);
      $user=User::whereRaw('BINARY username = ?', [$username])->firstOrFail();
      Auth::login($user);
      log::info("BALA____".Session::get('userSwith'));
      if(strpos($username,'admin')!==false ){
        log::info( 'inside if filter adminauth' . $username) ;
          return  Redirect::to('Business');
      }else{
          $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
          $detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $username);
          $detail=json_decode($detailJson,true);
          if(isset($detail['website'])==1){
            $website=$detail['website'];
          }else{
            $website='';
          }
          $domain=HomeController::get_domain($website);
          if($domain=='gpsvts.net'){
            Session::put('curdomin','gps');
          }else{
            Session::put('curdomin','notgps');
          }
          return  Redirect::to('Business');
      }
    }*/


		Auth::logout(); 
		return Redirect::to('login');
	}

	public function fileUpload() {

       Log::info("file upload function..");

        if( 0 < $_FILES['file']['error'] ) {

           echo 'Error : ' . $_FILES['file']['error'] . '<br>';
       
        } else {

            move_uploaded_file( $_FILES['file']['tmp_name'], '/var/www/gps/public/uploads/'.$_FILES['file']['name'] );

          //move_uploaded_file( $_FILES['file']['tmp_name'], '/home/vamo/raj/script/'.$_FILES['file']['name'] );
            //move_uploaded_file( $_FILES['file']['tmp_name'], '/Applications/XAMPP/xamppfiles/htdocs/vamo/public/'.$_FILES['file']['name'] );

             Log::info("File uploaded successfully..");
        }
    }

    public function getFileNames() {

       Log::info("get file names..");
     //$dir     =  '/home/vamo/raj/script';
       $dir     =  '/var/www/gps/public/uploads';
       $files1  =  scandir($dir);
     //$files2  =  scandir($dir, 1);

			//print_r($files1);
			//print_r($files2);
             
          /* for ($x = 0; $x <= $files1.l; $x++) {
                   echo "The number is: $x <br>";
             } */

             $retArr=array();

              foreach ($files1 as $key => $value) {

			    if(strpos($value,'.xlsx') || strpos($value,'.xls')) {
			      //log::info($value);
			    	array_push($retArr,$value);
		
			    } else {

                     log::info('errrorr....');
			    }
			  }

             log::info($retArr);

       return $retArr; 
    }
    public function mobileVerify(){
    	$mobileNo1=Input::get('val');
        $username=Input::get('usr');
     	$redis = Redis::connection();
     	$pin = mt_rand(100000, 999999);
     	$string = str_shuffle($pin);
     	$otp=$string;
	 	  $mobileNos=$redis->hget( 'H_UserId_Cust_Map',$username.':mobileOtp');
	 	  $mobileNo2=explode(":",$mobileNos);
	 	  if (in_array($mobileNo1, $mobileNo2)){
 		  foreach($mobileNo2 as $key => $num)
      	{
      		log::info($num);
      		log::info($mobileNo1);
			  if($num==$mobileNo1){
  			  $mobileNo=$num;
        
       		$exp=$redis->exists('H_Vamos_OTP');
        	if($exp!='1'){
           	$re=$redis->set('H_Vamos_OTP',$otp);
        		$data=$redis->get('H_Vamos_OTP');
            $current = Carbon::now();
			      $ch = curl_init();
            $user="pk@vamosys.com:321vamos321";
            
            $receipientno=$mobileNo; 
     		    $senderID="VAMOSS"; 
         	  $msgtxt='Your OTP number is '.$data.' .'; 
           	curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
            $buffer = curl_exec($ch);
            if(empty ($buffer))
            { 
                       
                log::info('buffer is empty ');
        	  }else { 
              log::info('SMS Curl Status---->'.$buffer);
              log::info('SMS send Date---->'.$current->format('d-m-Y'));
              log::info('SMS send Date---->'.$current->format('H:i'));
       		  }
       		  curl_close($ch);    
        		$redis->EXPIRE('H_Vamos_OTP',400);
        		return 'success';
             }else{
                $redis->del('H_Vamos_OTP');
                $re=$redis->set('H_Vamos_OTP',$otp);                 
                $data=$redis->get('H_Vamos_OTP');                        
            $current = Carbon::now();
			      $ch = curl_init();
            $user="pk@vamosys.com:321vamos321";
            
            $receipientno=$mobileNo; 
       	  	$senderID="VAMOSS"; 
           	$msgtxt='Your OTP number is '.$data.' .'; 
           	curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
            $buffer = curl_exec($ch);
            if(empty ($buffer))
            { 
                       
                log::info('buffer is empty ');
        	}else { 
              log::info('SMS Curl Status---->'.$buffer);
              log::info('SMS send Date---->'.$current->format('d-m-Y'));
              log::info('SMS send Date---->'.$current->format('H:i'));
       		}
       		curl_close($ch);                
        		$redis->EXPIRE('H_Vamos_OTP',400);
				return 'success';          	    		
            } }
        }
 		}else{
 			return 'false'; 
 		}

    }
    
    public function otpverify(){
    $otp=Input::get('val'); 
	$mobileNo=Input::get('valu');
    $username=Input::get('usr'); 
    $redis = Redis::connection();
    $data=$redis->get('H_Vamos_OTP');
   	if($data==$otp){
      $userdata = array(
				  'userName' 	=> $username,
				  'password' 	=> $redis->hget('H_UserId_Cust_Map', $username.':password')
			  );
	    $fcode=$redis->hget('H_UserId_Cust_Map', $username.':fcode');
		$remember = (Input::has('remember')) ? true : false;
       	if (Auth::attempt($userdata,$remember)) {
			log::info(' inside the login ');
			$username = Auth::user()->username;
			$current = Carbon::now();	 
            DB::table('onetimepass')->insert(array('Phone_num' => $mobileNo,'OTP'=>$data,'User_Name'=>$username));
			/*$franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
			$servername = $franchiesJson;
			//$servername="209.97.163.4";
			if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
				return 'Ipaddress Failed !!!';
			}
            $usernamedb = "root";
            $password = "#vamo123";
            $dbname = "VAMOSYS";
            $date=$current->format('Y-m-d H:i:s');
            $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);  
            if( !$conn ) {
                die('Could not connect: ' . mysqli_connect_error());
                return 'Please Update One more time Connection failed';
            } else {            
                $insertval ="INSERT INTO onetimepass(Phone_num,OTP,User_Name) VALUES ('$mobileNo','$data','$username')";
               $conn->multi_query($insertval);
               $conn->close();
            }*/
			return 'success'; 
        }else{
          Auth::logout(); 
	        return 'failed';
        }
    	
    }else{
    	Auth::logout(); 
	      return 'failed'; 
   	}
    
 }
    public function otpcancel(){
        log::info('--------------log out ----');
      	Auth::logout(); 
       return 'success'; 
       
    }
    
   public function resendftn(){
    $mobileNo1=Input::get('valu');
    $username=Input::get('usr');
   	$redis = Redis::connection();
		$exp=$redis->exists('H_Vamos_OTP');
		$mobileNos=$redis->hget( 'H_UserId_Cust_Map',$username.':mobileOtp');
		$mobileNo2=explode(":",$mobileNos);
		foreach($mobileNo2 as $key => $num)
		{
			if($num==$mobileNo1){
				$mobileNo=$num;	
  		  		if($exp!='1'){
   	        		$re=$redis->set('H_Vamos_OTP',$otp);
            		$data=$redis->get('H_Vamos_OTP');
                $ch = curl_init();
                $user="pk@vamosys.com:321vamos321";    
                $receipientno=$mobileNo; 
       		      $senderID="VAMOSS"; 
             	  $msgtxt='Your OTP number is '.$data.' .'; 
               	curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
                $buffer = curl_exec($ch);
                if(empty ($buffer)) { 
                   
                   log::info('buffer is empty ');
        	    }else { 
                log::info('SMS Curl Status---->'.$buffer);
       		    }
       		  curl_close($ch); 
          			 $redis->EXPIRE('H_Vamos_OTP',400);
           			return 0;
     	  		}else{
				  $data=$redis->get('H_Vamos_OTP');                          
                  //$this->smssend($mobileNo1,$data);
                $ch = curl_init();
                $user="pk@vamosys.com:321vamos321";    
                $receipientno=$mobileNo; 
       		      $senderID="VAMOSS"; 
             	  $msgtxt='Your OTP number is '.$data.' .'; 
               	curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
                $buffer = curl_exec($ch);
                curl_close($ch);
                $redis->EXPIRE('H_Vamos_OTP',400);
                if(empty ($buffer)){     
                    log::info('buffer is empty ');
                    return 'failed';   
        	     }else { 
                	log::info('SMS Curl Status---->'.$buffer);  
                	return 'success';   
       		    }
       		  	  
			  	  
                  return 0;
       		}}
		}
				
    }		
	public function getVehicle(){
		$vehNameArray = array();
		$vehicles = array();
		$redis           =  Redis::connection ();
		$username                =       Input::get( 'username' );
		$grpName                =       Input::get( 'groupName' );
		$fcode                  =       $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		if($grpName!=""){
			$vehicles = $redis->smembers($grpName.':'.strtoupper($fcode));
		}
		else{
			$groups = $redis->smembers($username);
			$vehicles = $redis->smembers($groups[0]);
		}
		foreach ($vehicles as $key => $value) {
			$vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $value );
			$vehicleRefData=json_decode($vehicleRefData,true);
			$vehdetail = array("vehicleId"=>$value, "shortName"=>$vehicleRefData['shortName']);
			$vehNameArray[$key] = $vehdetail;
		}
		return $vehNameArray;
	}
public function byPassUsers($id,$id2,$id3){
    $userName=HomeController::decrypts($id,"XSBRMLT");
    $redis = Redis::connection ();
    $key=$redis->get($userName.':key');
    $redis->del($userName.':key');
    if($key==null){
    	return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Unable to login . Please try again !!!');
    }
    Session::put('userSwithDmn',HomeController::decrypts($id3,$key));
    $password=HomeController::decrypts($id2,$key);
    $userdata = array(
				'userName' 	=> $userName,
				'password' 	=> $password
			);
    $remember = (Input::has('remember')) ? true : false;    
    if (Auth::attempt($userdata,$remember)) {
				log::info(' inside the login ');
                $username = Auth::user()->username;
                  $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
                  $url = Request::url();
                  Log::info('Login details $url ' . $url);
				  if($redis->sismember('S_Franchises_Disable',$fcode)) {
                       return Redirect::to('login')
               			 ->withInput(Input::except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
                  }
                  $disableDeler=$redis->smembers('S_Dealers_'.$fcode);
                  foreach ($disableDeler as $key => $dealerId) {
                    $lock= $redis->hget ( 'H_UserId_Cust_Map', $dealerId . ':lock');
                    if($lock=='Disabled'){
    				    $dealerusersList = 'S_Users_Dealer_'.$dealerId.'_'.$fcode;
                   	    if($redis->sismember($dealerusersList,$username)){
                   	        return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
                   	    }
                    }
                  }
			    if($fcode=="GPSVA"){
                    $d=$redis->sismember ('S_Dealers_' . $fcode, $username);
                    if($d==1 || $username=='gpsvtsadmin'){
                        return  Redirect::to('Business');
                    }else{
                        $d=$redis->smembers ('S_Dealers_' . $fcode);
                        foreach($d as $key => $org){
                            $user=$redis->sismember ( 'S_Users_Dealer_'.$org.'_'.$fcode,$username);
                            $user1=$redis->sismember ('S_Users_Admin_'.$fcode,$username);
                            if($user1==1 || $user==1){
                                return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'You are restricted in web application. Please use Mobile application');
                            }   
                        }
                    }      
                }else if(strpos($username,'admin')!==false ) {
                    //do nothing
			        log::info( '---------- inside if filter adminauth----------' . $username) ;
                    if($username=='smpadmin' || $username=='vamoadmin' || $username=='SPVAMOadmin'){	
      	                 Auth::logout(); 		
				        $byPass='yes';		
				        return view('vdm.login.otplogin')->with('username',$username)->with('byPass',$byPass);             						
			         }
			        //Auth::session(['cur' => 'admin']);
                    return  Redirect::to('Business');
                }else {
                    $redis = Redis::connection ();
		            $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		            $val1= $redis->sismember ( 'S_Dealers_' . $fcode, $username );
		            $val = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		            if($val1==1 && isset($val)) {
			             Log::info('---------------is dealer adminauth:--------------');	
                   $detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $username);
                           $detail=json_decode($detailJson,true);
                           if(isset($detail['website'])==1){
                              $website=$detail['website'];
                            }else{
                              $website='';
                            }
                            $domain=HomeController::get_domain($website);
                            if($domain=='gpsvts.net'){
                              Session::put('curdomin','gps');
                            }else{
                              Session::put('curdomin','notgps');
                            }		
                         return  Redirect::to('DashBoard');
		            }
                }
                if($username=='vamos'){	
                    Auth::logout(); 		
				    $byPass='yes';		
				    return view('vdm.login.otplogin')->with('username',$username)->with('byPass',$byPass);            						
			     }	
			     $userPrePage=null;
		         if($username!='vamos'){
		              $userPrePage=$redis->hget ( 'H_UserId_Cust_Map', $username . ':userPre');
		              Log::info('userPrePage '.$userPrePage);
		              $groups = $redis->smembers($username);
			          $vehicles = $redis->smembers($groups[0]);
			          Session::put('group',$groups[0]);
			          Session::put('vehicle',$vehicles[0]);
		              Session::put('userPrePage',$userPrePage);
		          }
              $password = is_null(Input::get('password'))?$userdata["password"]:Input::get('password');
              if(strlen($password) <6){
                return Redirect::to('passWd?userlevel=fromLogin');
              }
		          if($userPrePage==""||$userPrePage==null){
		              return  Redirect::to('track');
		          }else{
		       	      return  Redirect::to('userPage');
		          }
            } else {	 	
				return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
				
			}
}
public function decrypts($crypttext, $salt){
    $crypttext = str_replace(array('-','_'),array('+','/'),$crypttext);
    $decoded_64=base64_decode($crypttext);
    $td = mcrypt_module_open('cast-256', '', 'ecb', '');
    $iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    mcrypt_generic_init($td, $salt, $iv);
    $decrypted_data = mdecrypt_generic($td, $decoded_64);
    mcrypt_generic_deinit($td);
    mcrypt_module_close($td); 
    return trim($decrypted_data);
}    
public function encrypt($plaintext, $salt) {
   $td = mcrypt_module_open('cast-256', '', 'ecb', '');
    $iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    mcrypt_generic_init($td, $salt, $iv);
    $encrypted_data = mcrypt_generic($td, $plaintext);
    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);
    $encoded_64 = base64_encode($encrypted_data);
    $data = str_replace(array('+','/'),array('-','_'),$encoded_64);
    return trim($data);
}
public function doLogin1(){
    $current_link = $_SERVER['HTTP_HOST'];             
    $domainName=HomeController::get_domain($current_link);
    log::info('domain-------------------->'.$domainName);
    Session::put('lang',Input::get('lang'));
    $rules = array(
      'userName'    => 'required', 
      'password' => 'required|min:3'
    );
    Log::info('do Login');
     $validator = Validator::make ( Input::all (), $rules );  
      $remember = (Input::has('remember')) ? true : false;
    if ($validator->fails()) {
      return Redirect::to('login')->withErrors($validator)->withInput(Input::except('password')); 
    }else{
      $userdata = array(
        'userName'  => strtoupper(Input::get('userName')),
        'password'  => Input::get('password')
      );
      Log::info('Login details ' . Input::get('userName') .' '. Input::get('password') );
      if (Auth::attempt($userdata,$remember)) {
        log::info(' inside the login ');
        $username = Auth::user()->username;
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $url = Request::url();
        Log::info('Login details $url ' . $url);
        if($redis->sismember('S_Franchises_Disable',$fcode)) {
          return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
        }
        $disableDeler=$redis->smembers('S_Dealers_'.$fcode);
        foreach ($disableDeler as $key => $dealerId) {
          $lock= $redis->hget ( 'H_UserId_Cust_Map', $dealerId . ':lock');
          if($lock=='Disabled'){
            $dealerusersList = 'S_Users_Dealer_'.$dealerId.'_'.$fcode;
            if($redis->sismember($dealerusersList,$username)){
              return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
            }
          }
        }
       if(strpos($username,'admin')!==false ) {
            $key=md5(microtime(true).mt_Rand());
+           $redis->set ($username.':key',$key);
+           $redis->EXPIRE($username.':key',60);
            Auth::logout();
            $userNAme = HomeController::encrypt($username,"XSBRMLT");
            $pwd=HomeController::encrypt(Input::get('password'),$key);
            $dmn=HomeController::encrypt($domainName,$key);
            $datas=array (
                'val' =>$userNAme,
                'val1' =>'admin',
                'val2' =>$pwd,
                'dmn' =>$dmn,
            );
            $data = json_encode ( $datas);
            return Response::json($data); 
            //return Redirect::to('http://209.97.163.4/gps/public/loginpass/'.$userNAme.'/'.$pwd);        
        }else {
          $redis = Redis::connection ();
          $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
          $val1= $redis->sismember ( 'S_Dealers_' . $fcode, $username );
          $val = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
          if($val1==1 && isset($val)) {
              Log::info('---------------is dealer adminauth:--------------');
              $key=md5(microtime(true).mt_Rand());
+             $redis->set ($username.':key',$key);
+             $redis->EXPIRE($username.':key',60);
              Auth::logout();
              $userNAme = HomeController::encrypt($username,"XSBRMLT");
              $pwd=HomeController::encrypt(Input::get('password'),$key);
              $dmn=HomeController::encrypt($domainName,$key);
              $datas=array (
                'val' =>$userNAme,
                'val1' =>'admin',
                'val2' =>$pwd,
                'dmn' =>$dmn,
              );
              $data = json_encode ( $datas);
              return Response::json($data); 
              //return Redirect::to('http://209.97.163.4/gps/public/loginpass/'.$userNAme.'/'.$pwd);       
         }
        }
        if($username=='vamos'){
              $key=md5(microtime(true).mt_Rand());
+             $redis->set ($username.':key',$key);
+             $redis->EXPIRE($username.':key',60);
              Auth::logout();
              $userNAme = HomeController::encrypt($username,"XSBRMLT");
              $pwd=HomeController::encrypt(Input::get('password'),$key);
              $dmn=HomeController::encrypt($domainName,$key);
              $datas=array (
                'val' =>$userNAme,
                'val1' =>'admin',
                'val2' =>$pwd,
                'dmn' =>$dmn,
              );
              $data = json_encode ( $datas);
              return Response::json($data);                 
          }
          $userPrePage=null;
          if($username!='vamos'){
            $userPrePage=$redis->hget ( 'H_UserId_Cust_Map', $username . ':userPre');
            Log::info('userPrePage '.$userPrePage);
            $groups = $redis->smembers($username);
            $vehicles = $redis->smembers($groups[0]);
            Session::put('group',$groups[0]);
            Session::put('vehicle',$vehicles[0]);
            // $user_email = "<script>document.write(localStorage.setItem('defaultPage',"+$userPrePage+"));</script>";
            // $password = "<script>document.write(localStorage.getItem('defaultPage'));</script>";
            Session::put('userPrePage',$userPrePage);
          }
         //Session::get('userPrePage');
         //return $_SESSION["userPrePage"];
        if($userPrePage==""||$userPrePage==null){
             $key=md5(microtime(true).mt_Rand());
+           $redis->set ($username.':key',$key);
+           $redis->EXPIRE($username.':key',60);
            Auth::logout();
            $userNAme = HomeController::encrypt($username,"XSBRMLT");
            $pwd=HomeController::encrypt(Input::get('password'),$key);
            $datas=array (
                'val' =>$userNAme,
                'val1' =>'users',
                'val2' =>$pwd,
                'loc' =>'track',
            );
             
            $data = json_encode ( $datas);
            return Response::json($data);  
          //  return  Redirect::to('track');
        }else{
            $key=md5(microtime(true).mt_Rand());
+           $redis->set ($username.':key',$key);
+           $redis->EXPIRE($username.':key',60);
            Auth::logout();
            $userNAme = HomeController::encrypt($username,"XSBRMLT");
            $pwd=HomeController::encrypt(Input::get('password'),$key);
            $datas=array (
                'val' =>$userNAme,
                'val1' =>'users',
                'val2' =>$pwd,
                'loc' =>'userPage',
            );
            $data = json_encode ($datas);
            return Response::json($data);  



          //return  Redirect::to('userPage');
          /*$datas=array (
                'val' =>'userPage',
                'val1' =>'users',
                'val2' =>'null',
              );
              $data = json_encode ( $datas);
              return Response::json($data);*/
        }  
        //endof attempt
      }else {   
        return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
        // validation not successful, send back to form 
        //return Redirect::to('login');
      }

    }
}

    public function switchLogin($username){
        $redis = Redis::connection ();
        Auth::logout();
        $user=User::whereRaw('BINARY username = ?', [$username])->firstOrFail();
        Auth::login($user);
        $userPrePage=null;
        if($username!='vamos'){
            $userPrePage=$redis->hget ( 'H_UserId_Cust_Map', $username . ':userPre');
            Log::info('userPrePage '.$userPrePage);
            $groups = $redis->smembers($username);
            $vehicles = $redis->smembers($groups[0]);
            Session::put('group',$groups[0]);
            Session::put('vehicle',$vehicles[0]);
            Session::put('userPrePage',$userPrePage);
            Session::put('swUser',$username);
        }
        if($userPrePage==""||$userPrePage==null){
            return  Redirect::to('track');
        }else{
            return  Redirect::to('userPage');
        }
    }

	public function getUserDomainName($fcode,$username)	{
		log::info('----- Getuserdomainname ------'.$username);
		$redis = Redis::connection ();
		$adminUser=$redis->sismember('S_Users_Admin_'.$fcode,$username);
		$own=$redis->hget('H_UserId_Cust_Map',$username.":OWN");
		$web=null;
		$dealerName1=null;
		$current_link = $_SERVER['HTTP_HOST']; 
		$domainName=HomeController::get_domain($current_link);
		$web1='empty';
		$web2='empty';
		$web3='empty';
		$isWebsite='false';
		$admin='false';
		if ($own == "admin"){
			log::info('Admin user login');
			$addetails=$redis->hget('H_Franchise',$fcode);
			$refData    = json_decode($addetails, true);
			//$web=isset($refData['website'])?$refData['website']:'';
			//$web=isset($refData['website'])?preg_replace('/www./', '', $refData['website'], 1):'gpsvts.net';
			$web=isset($refData['website'])?preg_replace('/www./', '', $refData['website'], 1):'gpsvts.net';
			$web1=isset($refData['website2'])?preg_replace('/www./', '', $refData['website2'], 1):'gpsvts.net';
			$web2=isset($refData['website3'])?preg_replace('/www./', '', $refData['website3'], 1):'gpsvts.net';
			$web3=isset($refData['website4'])?preg_replace('/www./', '', $refData['website4'], 1):'gpsvts.net';
		                                    
		}	else 	{
			log::info('Dealer user login');
			$dealers=$redis->smembers('S_Dealers_'.$fcode);
			foreach ($dealers as $key => $value) {
				$dealerUser=$redis->sismember('S_Users_Dealer_'.$value.'_'.$fcode,$username);
				if($dealerUser==1)	{
					$dealerName1=$value;
					break;
				}
			}	
			$addetails=$redis->hget('H_DealerDetails_'.$fcode,$dealerName1);
			$refData    = json_decode($addetails, true);
			//$web=isset($refData['website'])?$refData['website']:'gpsvts.net';
			//$web=isset($refData['website'])?preg_replace('/www./', '', $refData['website'], 1):'gpsvts.net';
			$web=isset($refData['website'])?preg_replace('/www./', '', $refData['website'], 1):'gpsvts.net';
			$web1=isset($refData['website1'])?preg_replace('/www./', '', $refData['website1'], 1):'gpsvts.net';
			$web2='empty';
			if($fcode=='VAM' && $domainName=='gpsvts.vamosys.com'){
				$admin='true';
			}
	
		}
		if($web==null && $web=='')	{
			$web='gpsvts.net';
		}
		//if(($domainName=='gpsvts.vamosys.com' && $fcode != 'VAM') || ($admin=='true' && $fcode=='VAM')){
    if($domainName=='gpsvts.vamosys.com' && $fcode != 'VAM'){
      //  $domainName='gpsvts.net';
    }
    if($domainName=="fe-prd3.vamosys.com"){
      return 'true';
    }
		log::info('web '.$web.' web1 '.$web1.' web2 '.$web2);
		//if($web!=null && $web1!=null && $web2!=null) {  
			if(($web!=null && strpos($domainName,trim(strtolower($web))) !== false)  || ($web1!=null && strpos($domainName,trim(strtolower($web1))) !== false) || ($web2!=null && strpos($domainName,trim(strtolower($web2))) !== false) || ($web3!=null && strpos($domainName,trim(strtolower($web3))) !== false) )     {
				$isWebsite='true';
			}
		
	return $isWebsite;
	}

  public static function byPassUser($uid)
    {
      $redis = Redis::connection();

        // $redis = app("redis");
        $username = $redis->get($uid);
        $redis->del($uid);
        if(is_null($username)){
            return Redirect::to('login');
        }
        $username = trim($username);
        $fcode = $redis->hget("H_UserId_Cust_Map",$username.":fcode");
        if(is_null($fcode)){
            return Redirect::to('login');
        }
        $email = $redis->hget("H_UserId_Mail_Map_".$fcode,$username);
        $pass = $redis->hget("H_UserId_Cust_Map",$username.":password");
        return HomeController::doLogin(['userName'=>$username,'password'=>$pass,'email'=>$email]);
    }

    public function switchUserWithToken($token,$userId)
    {
        $redis = Redis::connection();
        $fcode = $redis->hget("H_UserId_Cust_Map",$userId.":fcode");
        if(is_null($fcode)){
            return Redirect::to('login')->withErrors("Invalid UserId");
        }
		$dealer = $redis->hget("H_Switch_User_Token_".$fcode,$token);
		if(is_null($dealer)){
			return Redirect::to('login')->withErrors("Invalid token");
		}
    //dd([$userId,$dealer]);
		if($redis->sismember("S_Users_Dealer_".$dealer."_".$fcode,$userId) || $dealer == $userId){
			// $email = $redis->hget("H_UserId_Mail_Map_".$fcode,$userId);
			$pass = $redis->hget("H_UserId_Cust_Map",$userId.":password");
			return HomeController::doLogin(['userName'=>$userId,'password'=>$pass]);
		}else{
			return Redirect::to('login')->withErrors("Invalid values");
		}
    }
    
}

