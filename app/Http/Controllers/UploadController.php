<?php 
namespace App\Http\Controllers;

use Request;
use App;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

class UploadController extends Controller {
	/**
   * Display a listing of the resource.
   *
   * @return Response
   */

public function get_domain($url){
   $nowww = str_replace('www.','',$url);
   $domain = parse_url($nowww);
   if(!empty($domain["host"])){
   		return $domain["host"];
   } else{
   	return $domain["path"];
   }
}



	public function loginCustom($dealer){
		log::info('dealerName '.$dealer);
		$dealerName=$dealer;
		return view('vdm.login.frame')->with('dealerName',$dealerName);
	}

	public function template1 ($dealerName){
		log::info('dealerName '.$dealerName);
		return view('vdm.login.template1_design')->with('dealerName',$dealerName);
	}

	public function template2 ($dealerName){
		log::info('dealerName '.$dealerName);
		return view('vdm.login.template2_design')->with('dealerName',$dealerName);
	}
	public function template3($dealerName){
		log::info('dealerName '.$dealerName);
		return view('vdm.login.template3_design')->with('dealerName',$dealerName);
	}
	public function template4($dealerName){
		log::info('dealerName '.$dealerName);
		 return view('vdm.login.template4')->with('dealerName',$dealerName);
	}
	public function template5($dealerName){
		log::info('dealerName '.$dealerName);
		 return view('vdm.login.template5')->with('dealerName',$dealerName);
	}



	public function view() {
		return view('imageUpload');
	}

	public function upload() {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		$redis = Redis::connection ();	
		$s3 = App::make('aws')->get('s3');	
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		if(Session::get('cur')=='admin'){
			$detailJson = $redis->hget ( 'H_Franchise', $fcode);
			$detail=json_decode($detailJson,true);
		}else if(Session::get('cur')=='dealer'){
			$detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $username);
			$detail=json_decode($detailJson,true);
		}
		if(isset($detail['website'])==1){
			$website=$detail['website'];
		}else{
			$website='';
		}
		if($website==''){
			return Redirect::back()->withErrors('These Dealer Does not have websit '.$username);
		}
		$rules = array (
        		'background' => 'mimes:jpeg,bmp,png',
        		'logo' => 'mimes:jpeg,bmp,png'
        	); 
		$current_link = $_SERVER['HTTP_HOST'];
      	$current_link1=UploadController::get_domain($current_link);
		$validator = Validator::make ( Input::all (), $rules );  
        if ($validator->fails ()) {
          	return Redirect::back()->withInput()->withErrors ( $validator );
        }
        $template_name	=	Input::get('template_name');
        $websits=UploadController::get_domain($website);
        $folder_name=$websits;
        $upload_folder = public_path().'/temp/'; 
		DB::table('login_Customisation')->where('Domain', $websits)->delete();
		log::info('Folder name '.$upload_folder);
		
	    if($template_name=='3'){
	    	if (!file_exists($upload_folder)) {
		 		$result = File::makeDirectory($upload_folder, 0777,true);
		 		log::info('Folder name Result'.$result);
	    	}
			$file  = Input::file('logo');
			$file->move($upload_folder, "logo.".$file->getClientOriginalExtension()); 
			$file_name="logo.".$file->getClientOriginalExtension();
			$s3->putObject(array(
            			 'Bucket' => Config::get('constant.bucket'), 
            			 'Key' => "picture/".$folder_name.'/logo.'.$file->getClientOriginalExtension(), 
            			 'SourceFile' => $upload_folder."logo.".$file->getClientOriginalExtension(),
            			 'ACL' => 'public-read' 
              		));
			$logo_loc=Config::get('constant.endpoint').'/'.Config::get('constant.bucket').'/picture/'.$folder_name.'/logo.'.$file->getClientOriginalExtension();



			log::info('Before Audit_LoginCustomisation Entry');
            try{
		            $mysqlDetails =array();
		       		$mysqlDetails=array(
					    'username'=>$username,
					    'status' => Config::get('constant.updated'),
						'fcode' => $fcode,
						'Domain' => $websits, 
	                  	'Template' => $template_name,
	                  	'bcolor' =>Input::get('bgcolor'),
	                  	'bcolor1'=>'',
	                  	'fcolor' => Input::get('fontcolor'),
	                  	'logo' => $logo_loc,
	                  	'background' => '',
	                  	'userIpAddress'=>Session::get('userIP'),
						
					);
		            $modelname = new loginCustomisation();   
				    $table = $modelname->getTable();
					$tableExist = DB::table('information_schema.tables')->where('table_schema','=','VAMOSYS')->where('table_name','=',$table)->get();
					//return $tableExist;
					if(count($tableExist)>0){
						loginCustomisation::create($mysqlDetails);
					}
					else{
						AuditTables::CreateLoginCustomisation();
						loginCustomisation::create($mysqlDetails);
					}
				}
				catch (Exception $e) {
				  log::info('Error inside Audit_LoginCustomisation Update'.$e->getMessage());
				  log::info($mysqlDetails);
				}
			//loginCustomisation::create($details);
			log::info('After Audit_LoginCustomisation Entry');
			DB::table('login_Customisation')->insert(
               array(
                  'Domain' => $websits, 
                  'Template' => $template_name,
                  'bcolor' =>Input::get('bgcolor'),
                  'bcolor1'=>'',
                  'fcolor' => Input::get('fontcolor'),
                  'logo' => $logo_loc,
                  'background' => '',
                  'fcode'   =>$fcode,
                  'UserName' =>$username,
                 )
         	);
			File::deleteDirectory($upload_folder);
		}else{
			if (!file_exists($upload_folder)) {
		 		$result = File::makeDirectory($upload_folder, 0777,true);
		 		log::info('Folder name Result'.$result);
	    	}
			$file  = Input::file('logo');
			$file->move($upload_folder, "logo.".$file->getClientOriginalExtension()); 
			$s3->putObject(array(
            			 'Bucket' => Config::get('constant.bucket'), 
            			 'Key' => "picture/".$folder_name.'/logo.'.$file->getClientOriginalExtension(), 
            			 'SourceFile' => $upload_folder."logo.".$file->getClientOriginalExtension(),
            			 'ACL' => 'public-read' 
              		));
			$logo_loc=Config::get('constant.endpoint').'/'.Config::get('constant.bucket').'/picture/'.$folder_name.'/logo.'.$file->getClientOriginalExtension();
			//$file = $request->file('background');
			$file = Input::file('background');
			log::info('FILE DEATILS');
			log::info($file);
			$file->move($upload_folder, "background.".$file->getClientOriginalExtension()); 
			$s3->putObject(array(
            			 'Bucket' => Config::get('constant.bucket'), 
            			 'Key' => "picture/".$folder_name.'/background.'.$file->getClientOriginalExtension(), 
            			 'SourceFile' => $upload_folder."background.".$file->getClientOriginalExtension(),
            			 'ACL' => 'public-read' 
              		));
			$bg_loc=Config::get('constant.endpoint').'/'.Config::get('constant.bucket').'/picture/'.$folder_name.'/background.'.$file->getClientOriginalExtension();

           log::info('Before Audit_LoginCustomisation Entry');
            try{
		            $mysqlDetails =array();
		       		$mysqlDetails=array(
					    'username'=>$username,
					    'status' => Config::get('constant.updated'),
						'fcode' => $fcode,
						'Domain' => $websits, 
	                  	'Template' => $template_name,
	                  	'bcolor' =>Input::get('bgcolor'),
	                  	'bcolor1'=>Input::get('bgcolor1'),
	                  	'fcolor' => Input::get('fontcolor'),
	                  	'logo' => $logo_loc,
	                  	'background' => $bg_loc,
	                  	'userIpAddress'=>Session::get('userIP'),
						
					);
		            $modelname = new loginCustomisation();   
				    $table = $modelname->getTable();
					$tableExist = DB::table('information_schema.tables')->where('table_schema','=','VAMOSYS')->where('table_name','=',$table)->get();
					//return $tableExist;
					if(count($tableExist)>0){
						loginCustomisation::create($mysqlDetails);
					}
					else{
						AuditTables::CreateLoginCustomisation();
						loginCustomisation::create($mysqlDetails);
					}
				}
				catch (Exception $e) {
				  log::info('Error inside Audit_LoginCustomisation Update'.$e->getMessage());
				  log::info($mysqlDetails);
				}
			//loginCustomisation::create($details);
			log::info('After Audit_LoginCustomisation Entry');

            
			DB::table('login_Customisation')->insert(
               array(
                  'Domain' => $websits, 
                  'Template' => $template_name,
                  'bcolor' =>Input::get('bgcolor'),
                  'bcolor1'=>Input::get('bgcolor1'),
                  'fcolor' => Input::get('fontcolor'),
                  'logo' => $logo_loc,
                  'background' => $bg_loc,
                  'fcode'   =>$fcode,
                  'UserName' =>$username,
                 )
         	);
			File::deleteDirectory($upload_folder);
		}

		
		$txt1="Theme applied Sucessfully";
		echo "<h2 style='     position: absolute;top: 50%;left: 46%; margin-top: -25px;margin-left: -50px;'>" . $txt1 . "</h2>";

	}
	public function sourceData(){
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$s3 = App::make('aws')->get('s3');	
		$username = Auth::user ()->username;
		$redis = Redis::connection ();		
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		if(Session::get('cur')=='admin'){
			$detailJson = $redis->hget ( 'H_Franchise', $fcode);
		}else if(Session::get('cur')=='dealer'){
			$detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $username);
		}
		$detail=json_decode($detailJson,true);
		if(isset($detail['website'])==1){
			$website=$detail['website'];
		}else{
			$website='';
		}
		if($website==''){
			return Redirect::back()->withErrors('These dealer Does not have websit '.$username);
		}	
		$websits=UploadController::get_domain($website);
		$content = Input::get('sourceCode');
		log::info($content);
		$folder_name=$websits;
		$upload_folder = public_path().'/temp/'; 
		log::info('Folder name '.$upload_folder);
		if (!file_exists($upload_folder)) {
		 	$result = File::makeDirectory($upload_folder, 0777,true);
		 	log::info('Folder name Result'.$result);
	    }
	    $file  = Input::file('logo');
		$file->move($upload_folder, "logo.".$file->getClientOriginalExtension()); 
		$s3->putObject(array(
            			 'Bucket' => Config::get('constant.bucket'), 
            			 'Key' => "picture/temp/".$folder_name.'/logo.'.$file->getClientOriginalExtension(), 
            			 'SourceFile' => $upload_folder."logo.".$file->getClientOriginalExtension(),
            			 'ACL' => 'public-read' 
            ));
		log::info($file);
		$file = Input::file('background');
		$file->move($upload_folder, "background.".$file->getClientOriginalExtension());
		$s3->putObject(array(
            			 'Bucket' => Config::get('constant.bucket'), 
            			 'Key' => "picture/temp/".$folder_name.'/background.'.$file->getClientOriginalExtension(), 
            			 'SourceFile' => $upload_folder."background.".$file->getClientOriginalExtension(),
            			 'ACL' => 'public-read' 
            ));
	    File::put($upload_folder."sourceData.txt","$content");
	    $s3->putObject(array(
            			 'Bucket' => Config::get('constant.bucket'), 
            			 'Key' => "picture/temp/".$folder_name.'/sourceData.txt', 
            			 'SourceFile' => $upload_folder."sourceData.txt",
            			 'ACL' => 'public-read' 
            ));
	    File::deleteDirectory($upload_folder);
	    $txt1="Theme Updated Successfully. ";
	    $txt2="Will get reflected within 24 hrs once approved by Super admin.";
	    $tex3="For further details Contact support@gpsvts.freshdesk.com";
	    try{
        	Mail::send('emails.tem4Email', array('domain'=>$websits,'fcode'=>$fcode,'userId'=>$username), function($message) use ($username){
            		Log::info("Inside email :" . Session::get ( 'email' ));
            		$message->to("support@gpsvts.freshdesk.com")->subject('Login page customisation -'.$username);
            		$message->cc("support@vamosys.freshdesk.com ");
        	});
    	}catch (\Exception $e){
       		 Log::info('Mail Error ');
    	}

    	log::info('Before Audit_LoginCustomisation Entry');
            try{
		            $mysqlDetails =array();
		       		$mysqlDetails=array(
					    'username'=>$username,
					    'status' => Config::get('constant.updated'),
						'fcode' => $fcode,
						'Domain' => $websits, 
	                  	'Template' => '4',
	                  	'logo' => Input::file('logo'),
	                  	'background' => Input::file('background'),
	                  	'userIpAddress'=>Session::get('userIP'),
						
					);
		            $modelname = new loginCustomisation();   
				    $table = $modelname->getTable();
					$tableExist = DB::table('information_schema.tables')->where('table_schema','=','VAMOSYS')->where('table_name','=',$table)->get();
					//return $tableExist;
					if(count($tableExist)>0){
						loginCustomisation::create($mysqlDetails);
					}
					else{
						AuditTables::CreateLoginCustomisation();
						loginCustomisation::create($mysqlDetails);
					}
				}
				catch (Exception $e) {
				  log::info('Error inside Audit_LoginCustomisation Update'.$e->getMessage());
				  log::info($mysqlDetails);
				}
			//loginCustomisation::create($details);
			log::info('After Audit_LoginCustomisation Entry');
		echo "<h2 style='position: absolute;top: 50%;left: 35%; margin-top: -25px;margin-left: -50px;text-align: -webkit-center;'>" . $txt1."<br/>".$txt2."<br/>".$tex3. "</h2>";
	}

	 public function UploadPDFfile(){

        if(Input::file('file')==null){
                 Session::flash ( 'message', 'Please Select File ... ' ); 
               return Redirect::back();
            }
          else{
            //$path=Input::file('file')->move(__DIR__.'/views/ReleaseNotes/',Input::file('file')->getClientOriginalName());
            
		    $s3 = App::make('aws')->get('s3');	
             $upload_folder = public_path().'/temp/'; 
            if (!file_exists($upload_folder)) {
                $result = File::makeDirectory($upload_folder, 0777,true);
                log::info('Folder name Result'.$result);
            }
            $file  = Input::file('file');
            $file->move($upload_folder, $file->getClientOriginalName()); 
            $file_name=$file->getClientOriginalName();
            $s3->putObject(array(
                         'Bucket' => Config::get('constant.bucket'), 
                         'Key' => "ReleaseNotes/".$file->getClientOriginalName(), 
                         'SourceFile' => $upload_folder.$file->getClientOriginalName(),
                         'ACL' => 'public-read' 
                    ));
            File::deleteDirectory($upload_folder);
             Session::flash ( 'message', 'Uploaded Successfully... ' ); 
              return Redirect::back();
           }
    }

}


