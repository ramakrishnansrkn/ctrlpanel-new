<?php
namespace App\Http\Controllers;

use Request;
use App;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

class KtotController extends Controller {

	public function index() {
    	if (! Auth::check ()) {
      		return Redirect::to ( 'login' );
    	}
    	log::info("ktot index");
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$deviceMap=array();						
		$devicesList=$redis->smembers( 'S_PreOnboard_Devices_' . $fcode); 
		log::info($devicesList);
		log::info( '------device list size---------- '.count($devicesList));
		for($i =0;$i<count($devicesList);$i++){
			$deviceDetails=$redis->hget ( 'H_PreOnboard_Devices_' . $fcode, $devicesList[$i] );
			if($deviceDetails!==null)
			{
				$refData	= json_decode($deviceDetails,true); log::info($refData);
				$deviceModel 		= isset($refData['deviceModel'])?$refData['deviceModel']:' '; 
				$deviceType 		= isset($refData['deviceType'])?$refData['deviceType']:' ';
				$mobileNo 		= isset($refData['mobileNo'])?$refData['mobileNo']:' ';
				$date 		= isset($refData['date'])?$refData['date']:' ';
				log::info('  dealer name   ');
				
				//$deviceMap 	= array_add($deviceMap,$i,$vechicle.','.$orgL[$i].','.$orgId);
				 $deviceMap  = array_add($deviceMap,$i,$devicesList[$i].','.$deviceModel.','.$deviceType.','.$mobileNo.','.$date);
			}
		}
		 return view ( 'vdm.ktot.index', array (
                                'deviceMap' => $deviceMap ) );

    }
    public function addDevices($numberofdevice,$availableLincence){
    	log::info("ktrack add Devices");
    	if (! Auth::check ()) {
      		return Redirect::to ( 'login' );
    	}
    	//$protocol=array(''=>'-- Select --');
    	$protocol = BusinessController::getProtocal();
		return view ( 'vdm.ktot.addDevices' )->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', $numberofdevice )->with('protocol',$protocol);
    }
    public function Store()
    {	
    	log::info("store ktot");
    	if (! Auth::check ()) {
      		return Redirect::to ( 'login' );
    	}
    	$username = Auth::user ()->username;
    	$redis = Redis::connection ();
    	$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    	$mobileNo = Input::get ( 'mobileNo');
    	$numberofdevice = Input::get ( 'numberofdevice' );
    	$current = Carbon::now();
		$date=$current->format('Y-m-d');
		$count=0;
    	for($i =1;$i<=$numberofdevice;$i++)
		{

			$deviceId = Input::get ( 'deviceId'.$i);
    		$deviceModel = Input::get ( 'deviceModel'.$i);
    		$deviceType = Input::get ( 'deviceType'.$i);
    		$refDataArr = array (
                'deviceId' => $deviceId,          
                'deviceModel' => $deviceModel,
                'deviceType' => $deviceType,
                'mobileNo'   => $mobileNo,
                'date' => $date, 
            );
            $refDataJson = json_encode ( $refDataArr );
            log::info($refDataJson);
            $redis->hset ( 'H_PreOnboard_Devices_'.$fcode,$deviceId,$refDataJson);
            $redis->sadd ( 'S_PreOnboard_Devices_'.$fcode,$deviceId);
			$count++;
		}
		$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);
        $franchiseDetails['availableLincence']=$franchiseDetails['availableLincence']-$count;
        $detailsJson = json_encode ( $franchiseDetails );
        $redis->hmset ( 'H_Franchise', $fcode,$detailsJson);
		$error = 'Successfully updated';
		return Redirect::to ( 'Business' )->withErrors($error);
    }
}
