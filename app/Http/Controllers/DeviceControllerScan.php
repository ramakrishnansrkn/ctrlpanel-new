<?php
use Maatwebsite\Excel\Facades\Excel;
class DeviceControllerScan extends \BaseController {

        /**
         * Display a listing of the resource.
         *
         * @return Response
         */
public function index(){ 
        if (! Auth::check ()) {
            return Redirect::to ( 'login' );
        }
        $username = Auth::user ()->username;
        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);
        $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';

       $orgLis = [];
       $devicesList=[];
       $text='Search Devices';
       return View::make('vdm.business.scan')->with('deviceMap', $orgLis)->with('devicesList',$devicesList)->with('text',$text)->with('prepaid',$prepaid);
    }
     public function store() {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }

                $username = Auth::user ()->username;
                $redis = Redis::connection ();
                $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );

                $devicesList=$redis->smembers( 'S_Device_' . $fcode); 
                $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                $franchiseDetails=json_decode($franDetails_json,true);
                $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
         


                $orgArr = array();
               foreach($devicesList as $org) {
                 $orgArr = array_add($orgArr, $org,$org);
               }
               $devicesList1 = $orgArr;//log::info($devicesList);
                 $ram = Input::get('value'); 
                 $text_word = Input::get('text_word');
                 $cou = $redis->SCARD('S_Device_' . $fcode); log::info($cou);
                 $orgLi = $redis->sScan( 'S_Device_' . $fcode, 0,  'count', $cou, 'match', '*'.$text_word.'*'); log::info($orgLi);
                 $orgL = $orgLi[1];
				 if($orgL==null)  {
                  $orgL[0]=$text_word;
                 }
                $temp=0;
                $deviceMap=array();
                $vechicle=null;
                for($i =0;$i<count($orgL);$i++){
                        if($redis->sismember( 'S_Device_' . $fcode,$orgL[$i]) == 1)
                        {
							$device=$orgL[$i];
							$vechicle=$redis->hget ( 'H_Vehicle_Device_Map_' . $fcode, $orgL[$i] );
                        }
                        else if($redis->sismember( 'S_Vehicles_' . $fcode,$orgL[$i]) == 1) {
							$vechicle=$orgL[$i]; 
							$device=$redis->hget ( 'H_Vehicle_Device_Map_' . $fcode, $orgL[$i] );
                        }
                        if($vechicle!==null)
                        {
                                $refData        = $redis->hget ( 'H_RefData_' . $fcode, $vechicle );  //        log::info($refData);
                                $refData        = json_decode($refData,true); log::info($refData);
                                $orgId          = isset($refData['OWN'])?$refData['OWN']:' '; //log::info($orgL[$i]);

                                
                                $onboardDate=isset($refData['onboardDate'])?$refData['onboardDate']:'';
                                $lic= isset($refData['licenceissuedDate'])?$refData['licenceissuedDate']:'';
                                //$onboardDate=isset($refData['onboardDate'])?$refData['onboardDate']:'null';
                                $vehicleExpiry=isset($refData['vehicleExpiry'])?$refData['vehicleExpiry']:'';
                                $type=isset($refData['Licence'])?$refData['Licence']:'Advance';
                                if($prepaid == 'yes'){
                                    log::info('prepaid franchise');
                                    $LicenceId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$vechicle);
                                    $LicenceRef=$redis->hget ( 'H_LicenceExpiry_' . $fcode, $LicenceId);
                                    $LicenceRef = json_decode($LicenceRef,true);
                                    $lic        = isset($LicenceRef['LicenceissuedDate'])?$LicenceRef['LicenceissuedDate']:' ';
                                    $LicenceExpiryDate=isset($LicenceRef['LicenceExpiryDate'])?$LicenceRef['LicenceExpiryDate']:' ';
                                    $deviceMap  = array_add($deviceMap,$i,$vechicle.','.$device.','.$orgId.','.$devicesList[$i].','.$lic.','.$onboardDate.','.$vehicleExpiry.','.$type.','.$LicenceId.','.$LicenceExpiryDate);
                                     log::info( '------device map---------- 1'.$lic.'  '.count($deviceMap));
                                     log::info($deviceMap);

                                }else{
                                     $deviceMap  = array_add($deviceMap,$i,$vechicle.','.$device.','.$orgId.','.$devicesList[$i].','.$lic.','.$onboardDate.','.$vehicleExpiry.','.$type);
                                      log::info( '------device map---------- 2'.count($deviceMap));
                                     log::info($deviceMap);

                                    
                                 }
                                
                                // log::info(isset($refData['OWN']));
                        }

                        $temp++;
                }
               
                //log::info($orgL);
               log::info($text_word);
                return View::make ( 'vdm.business.scan', array (
                                'deviceMap' => $deviceMap ) )->with ( 'devicesList', $devicesList1 )->with ('text', $text_word)->with('prepaid',$prepaid);

        }


    public function userNoti() {
        log::info('inside------------------------->');
        $username= Auth::user ()->username;
        $redis= Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username. ':fcode' );
        log::info($fcode);
        return Redirect::to ( 'Business' );

    }
    public function sendExcel()
        {
          log::info('insite excel');
        $username = Auth::user ()->username;
        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );                    
        $devicesList=$redis->smembers( 'S_Device_' . $fcode);
        $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);
        $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
        $file = Excel::create('Device List', function($excel) 
            {
              $excel->sheet('Sheetname', function($sheet)  
               {
                $redis = Redis::connection ();
                $username = Auth::user ()->username;
                $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );  
                $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                $franchiseDetails=json_decode($franDetails_json,true);
                $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
                $devicesList=$redis->smembers( 'S_Device_' . $fcode);
                $temp=0;
                $deviceMap=array();
                  for($i =0;$i<count($devicesList);$i++)
                  {
                   $vechicle=$redis->hget ( 'H_Vehicle_Device_Map_' . $fcode, $devicesList[$i] );
                    if($vechicle!==null)
                    {
                     $refData    = $redis->hget ( 'H_RefData_' . $fcode, $vechicle );
                     $refData    = json_decode($refData,true);
                     $orgId      = isset($refData['OWN'])?$refData['OWN']:' ';
                     $onboardDate=isset($refData['onboardDate'])?$refData['onboardDate']:'null';
                     $vehicleExpiry=isset($refData['vehicleExpiry'])?$refData['vehicleExpiry']:'null'; 
                     if($prepaid=='yes'){
                        $type=isset($refData['Licence'])?$refData['Licence']:'';
                        $LicenceId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$vechicle);
                        $LicencerefData=$redis->hget('H_LicenceExpiry_'.$fcode,$LicenceId);
                        $LicencerefData    = json_decode($LicencerefData,true);
                        $LicenceissuedDate=isset($LicencerefData['LicenceissuedDate'])?$LicencerefData['LicenceissuedDate']:'';
                        $LicenceOnboardDate=isset($LicencerefData['LicenceOnboardDate'])?$LicencerefData['LicenceOnboardDate']:'';
                        $LicenceExpiryDate=isset($LicencerefData['LicenceExpiryDate'])?$LicencerefData['LicenceExpiryDate']:'';
                     }
                    }
            
                    $temp++;
                    $condevmap=count($deviceMap);
                    $devLi=count($devicesList);           
                    
                    $j=$i+2;
                    if($prepaid=='yes'){

                        $sheet->row(1, array('LicenceId','Device ID','Vechicle ID','OrgId','type','Licence issued date','Licence OnboardDate','Licence Expery Date'));
                        $sheet->row($j, array($LicenceId,$devicesList[$i],$vechicle,$orgId,$type,$LicenceissuedDate,$LicenceOnboardDate,$LicenceExpiryDate));

                    }else{
                         $sheet->row(1, array('Device ID','Vechicle ID','OrgId','OnboardDate','VehicleExpiry'));
                         $sheet->row($j, array($devicesList[$i],$vechicle,$orgId,$onboardDate,$vehicleExpiry));
                    }
                      

                  }
               }); 

              });//->download('xls');  
              $emailFcode=$redis->hget('H_Franchise', $fcode);
              $emailFile=json_decode($emailFcode,true);
              $email1=$emailFile['email2'];
              $email2=$emailFile['email1'];
                $data[]='get all de';
                log::info('--------------email outsite------------------>');
                $mymail=Mail::send( 'vdm.business.empty',$data,function($message) use($file,$email1)
                 {
                  $message->to($email1);
		              //$message->to('ramakrishnan.vamosys@gmail.com');
		              $message->subject('Welcome to Vamosys');
                  $message->attach($file->store("xls",false,true)['full']);
                  log::info('-----------email send------------------>');
                 });
        Session::flash('message','Onboarded Device List Send to '.$email1.' Id Successfully !!!');
        return Redirect::to('DeviceScan')->with('prepaid',$prepaid);
        } 
		
		public function movedVehicle($id)
		{
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }

                $username = Auth::user ()->username;
                $redis = Redis::connection ();
                $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
                $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                $franchiseDetails=json_decode($franDetails_json,true);
                $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';

                $devicesList=$redis->smembers( 'S_Device_' . $fcode); 
                $orgArr = array();
               foreach($devicesList as $org) {
                 $orgArr = array_add($orgArr, $org,$org);
               }
               $devicesList1 = $orgArr;//log::info($devicesList);
                log::info( '------device list size---------- '.count($devicesList));
                 //$ram = Input::get('value'); log::info($ram);
                 $text_word = $id; 
                 log::info('text word---->'.$text_word);
                 $cou = $redis->SCARD('S_Device_' . $fcode); log::info($cou);
                 $orgLi = $redis->sScan( 'S_Device_' . $fcode, 0,  'count', $cou, 'match', '*'.$text_word.'*'); log::info($orgLi);
                 $orgL = $orgLi[1];

                $temp=0;
                $deviceMap=array();
                for($i =0;$i<count($orgL);$i++){
                        $vechicle=$redis->hget ( 'H_Vehicle_Device_Map_' . $fcode, $orgL[$i] );
                        log::info($vechicle);
                        if($vechicle!==null)
                        {
                                $refData        = $redis->hget ( 'H_RefData_' . $fcode, $vechicle );  //        log::info($refData);
                                $refData        = json_decode($refData,true); log::info($refData);
                             
                                
                                $orgId          = isset($refData['OWN'])?$refData['OWN']:' '; //log::info($orgL[$i]);
                                $lic        = isset($refData['licenceissuedDate'])?$refData['licenceissuedDate']:'';
                                $date=isset($refData['date'])?$refData['date']:'';
                                if($date=='' || $date==' ')
                                {
                                    $date1='';
                                }
                                else
                                {
                                    $date1=date("d-m-Y", strtotime($date));
                                }
                                $onDate=isset($refData['onboardDate'])?$refData['onboardDate']:$date1;
                                $nullval=strlen($onDate);
                                if($nullval==0 || $onDate=="null" || $onDate==" ")
                                {
                                    $onboardDate=$date1;
                                }
                                else
                                {
                                    $onboardDate=$onDate;
                                }
                                $vehicleExpiry=isset($refData['vehicleExpiry'])?$refData['vehicleExpiry']:'';
                                $type=isset($refData['Licence'])?$refData['Licence']:'Advance';
                                     
                                     $deviceMap  = array_add($deviceMap,$i,$vechicle.','.$orgL[$i].','.$orgId.','.$devicesList[$i].','.$lic.','.$onboardDate.','.$vehicleExpiry.','.$type);
                                // log::info(isset($refData['OWN']));
                                 
                                //$deviceMap= array_add($deviceMap,$i,$vechicle.','.$orgL[$i].','.$orgId.','.$lic.','.$onboardDate.','.$vehicleExpiry);
                        }

                        $temp++;
                }
                log::info( '------device map---------- '.count($deviceMap));
                //log::info($orgL);
               log::info($text_word);
                return View::make ( 'vdm.business.scan', array (
                                'deviceMap' => $deviceMap ) )->with ( 'devicesList', $devicesList1 )->with ('text', $text_word)->with('prepaid',$prepaid);

        }



    }
