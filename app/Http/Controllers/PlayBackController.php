<?php
namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;


class PlayBackController  extends \BaseController {

    public function replay()
	{
		
		
		
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		} else {
			return view('replay');
		}
		
		
	}

}