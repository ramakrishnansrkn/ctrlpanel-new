<?php
namespace App\Http\Controllers;

use Request;
use App;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

class DeviceController extends Controller {
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index() {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );

		$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
      	$franchiseDetails=json_decode($franDetails_json,true);
      	$prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';

								
		$devicesList=$redis->smembers( 'S_Device_' . $fcode);
		log::info( '------device list size---------- '.count($devicesList));
		$temp=0;
		$deviceMap=array();
		for($i =0;$i<count($devicesList);$i++){
			$vechicle=$redis->hget ( 'H_Vehicle_Device_Map_' . $fcode, $devicesList[$i] );
			

			if($vechicle!==null)
			{
				$refData 	= $redis->hget ( 'H_RefData_' . $fcode, $vechicle );
				$refData	= json_decode($refData,true);
				$orgId 		= isset($refData['OWN'])?$refData['OWN']:' ';
				$date=isset($refData['date'])?$refData['date']:'';
				if($date==''|| $date==' ')
				{
					$date1='';
				}
				else
				{
					$date1=date("d-m-Y", strtotime($date));
				}
				$onDate=isset($refData['onboardDate'])?$refData['onboardDate']:$date1;
				$nullval=strlen($onDate);
			
				if($nullval==0 || $onDate=="null" || $onDate==" ")
				{
					$onboardDate=$date1;
				}
				else
				{
					$onboardDate=$onDate;
				}
				//$onboardDate=isset($refData['onboardDate'])?$refData['onboardDate']:'null';
				$vehicleExpiry=isset($refData['vehicleExpiry'])?$refData['vehicleExpiry']:'';
        		$type=isset($refData['Licence'])?$refData['Licence']:'Advance';
        		if($prepaid=='yes'){
        			$LicenceId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$vechicle);
        			$LicenceRef=$redis->hget ( 'H_LicenceExpiry_'.$fcode, $LicenceId);
        			$LicenceRef	= json_decode($LicenceRef,true);
					$lic 		= isset($LicenceRef['LicenceissuedDate'])?$LicenceRef['LicenceissuedDate']:' ';
                    $LicenceExpiryDate=isset($LicenceRef['LicenceExpiryDate'])?$LicenceRef['LicenceExpiryDate']:' ';
        			$deviceMap 	= array_add($deviceMap,$i,$vechicle.','.$devicesList[$i].','.$orgId.','.$lic.','.$onboardDate.','.$vehicleExpiry.','.$type.','.$LicenceId.','.$LicenceExpiryDate);
        		}else{
        			$lic 		= isset($refData['licenceissuedDate'])?$refData['licenceissuedDate']:'';
        			$deviceMap 	= array_add($deviceMap,$i,$vechicle.','.$devicesList[$i].','.$orgId.','.$lic.','.$onboardDate.','.$vehicleExpiry.','.$type);
        		}
        		
				// log::info(isset($refData['OWN']));
				// log::info($orgId);
				// log::info('  dealer name   ');
                // log::info($vehicleExpiry);
				
			}
			
			$temp++;
		}
		log::info( '------device map---------- '.count($deviceMap));
		return view ( 'vdm.business.device', array (
				'deviceMap' => $deviceMap ) )->with('prepaid',$prepaid);
		
	}
	
	
	}

