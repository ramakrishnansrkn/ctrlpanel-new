<?php
namespace App\Http\Controllers;

use Request;
use App, DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Response;
use Carbon\Carbon;

class BusinessController extends Controller {
  
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index() {
    if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }

        $username = Auth::user ()->username;

        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
       
    $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
    
    if(Session::get('cur')=='dealer')
      {
        log::info( '------login 1---------- '.Session::get('cur'));
         $tmpOrgList = $redis->smembers('S_Organisations_Dealer_'.$username.'_'.$fcode);
      }
      else if(Session::get('cur')=='admin')
      {
         $tmpOrgList = $redis->smembers('S_Organisations_Admin_'.$fcode);
      }
      $orgList=null;
      $orgList=array_add($orgList,'','select');
    $orgList=array_add($orgList,'Default','Default');
        foreach ( $tmpOrgList as $org ) {
                $orgList = array_add($orgList,$org,$org);
                
            }
      if(Session::get('cur')=='admin'){
      
     $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
         $franchiseDetails=json_decode($franDetails_json,true);
         $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
         if($prepaid == 'yes'){
            $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
            $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
            $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
            $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
            if($availableBasicLicence==null){
                $availableBasicLicence=0;
            }
            if($availableAdvanceLicence==null){
                $availableAdvanceLicence=0;
            }
            if($availablePremiumLicence==null){
                $availablePremiumLicence=0;
            }
            if($availablePremPlusLicence==null){
                $availablePremPlusLicence=0;
            }
           log::info($availableBasicLicence.' '.$availableAdvanceLicence.' '.$availablePremiumLicence);
           $availableLincence=$availableBasicLicence+$availableAdvanceLicence+$availablePremiumLicence+$availablePremPlusLicence;
         }else{
          $availableLincence=isset($franchiseDetails['availableLincence'])?$franchiseDetails['availableLincence']:'0';
          $availableBasicLicence=0;
          $availableAdvanceLicence=0;
          $availablePremiumLicence=0;
          $availablePremPlusLicence=0;
         }

         $dealerId=null;$userList=null; $numberofdevice=0;
     return view ( 'vdm.business.deviceAddCopy' )->with ( 'orgList', $orgList )->with ( 'availableBasicLicence', $availableBasicLicence )->with ( 'availableAdvanceLicence', $availableAdvanceLicence )->with ( 'dealerId', $dealerId )->with ( 'userList', $userList )->with('availablePremiumLicence',$availablePremiumLicence)->with('availableLincence',$availableLincence)->with('numberofdevice',$numberofdevice)->with('availablePremPlusLicence',$availablePremPlusLicence);
      
    }
    else if(Session::get('cur')=='dealer')
    {   //prepaid dealer
        if(Session::get('cur1')=='prePaidAdmin'){
            $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
            log::info('user name'.$username);
            $dealersDetails=json_decode($dealersDetails_json,true);
            $availableBasicLicence=isset($dealersDetails['avlofBasic'])?$dealersDetails['avlofBasic']:'0';
            $availableAdvanceLicence=isset($dealersDetails['avlofAdvance'])?$dealersDetails['avlofAdvance']:'0';
            $availablePremiumLicence=isset($dealersDetails['avlofPremium'])?$dealersDetails['avlofPremium']:'0';
            $availablePremPlusLicence=isset($dealersDetails['avlofPremiumPlus'])?$dealersDetails['avlofPremiumPlus']:'0';
            
           if($availableBasicLicence==null){
                $availableBasicLicence=0;
            }
            if($availableAdvanceLicence==null){
                $availableAdvanceLicence=0;
            }
            if($availablePremiumLicence==null){
                $availablePremiumLicence=0;
            }
            if($availablePremPlusLicence==null){
                $availablePremPlusLicence=0;
            }
            $availableLincence=$availableBasicLicence+$availableAdvanceLicence+$availablePremiumLicence+$availablePremPlusLicence;
            

            $dealerId=null;$userList=null; $numberofdevice=0;
            return view ( 'vdm.business.deviceAddCopy' )->with ( 'orgList', $orgList )->with ( 'availableBasicLicence', $availableBasicLicence )->with ( 'availableAdvanceLicence', $availableAdvanceLicence )->with ( 'dealerId', $dealerId )->with ( 'userList', $userList )->with('availablePremiumLicence',$availablePremiumLicence)->with('availableLincence',$availableLincence)->with('numberofdevice',$numberofdevice)->with('availablePremPlusLicence',$availablePremPlusLicence);

        }
        
      $key='H_Pre_Onboard_Dealer_'.$username.'_'.$fcode;


        $details=$redis->hgetall($key);
        $devices=null;
        $devicestypes=null;
        $i=0;
        foreach($details as $key => $value)
        {
          $valueData=json_decode($value,true);
          $devices = array_add($devices, $i,$valueData['deviceid']);
          $devicestypes = array_add($devicestypes,$i,$valueData['deviceidtype']);
          Log::info('i=' . $i);
          $i++;
        }
        
        
        Log::info(' inside multi ' );
        
        $dealerId = $redis->smembers('S_Dealers_'. $fcode);
        
      
        
        $orgArr = array();
            // $orgArr = array_add($orgArr, 'OWN','OWN');
    if($dealerId!=null)
    {
      foreach($dealerId as $org) {
            $orgArr = array_add($orgArr, $org,$org);
      
        }
    $dealerId = $orgArr;
    }
    else{
      $dealerId=null;
      //$orgArr = array_add($orgArr, 'OWN','OWN');
      $dealerId = $orgArr;
    }

          $redisUserCacheId = 'S_Users_' . $fcode; //dummy installation
    $redisGrpCacheId='S_Users_';
    
    if(Session::get('cur')=='dealer')
    {
      log::info( '------login 1---------- '.Session::get('cur'));
      
      $redisUserCacheId = 'S_Users_Dealer_'.$username.'_'.$fcode;
      
    }
    else if(Session::get('cur')=='admin')
    {
      $redisUserCacheId = 'S_Users_Admin_'.$fcode;
    }
  
    $userList = $redis->smembers ( $redisUserCacheId);
    //get the intersection of users set and groups set
    //$userList=$redis->sinter($redisUserCacheId,'S_Organisations_'.$fcode);
    
    $orgArra = array();
    $orgArra =array_add($orgArra, 'select','select');
      foreach($userList as $org) {
           if(!$redis->sismember ( 'S_Users_Virtual_' . $fcode, $org )==1)
     {
       $orgArra = array_add($orgArra, $org,$org);
     }  
      
        }
      $userList=$orgArra;
    
    
      return view ( 'vdm.business.business1')->with('devices',$devices)->with('devicestypes',$devicestypes)->with('dealerId',$dealerId)->with('userList',$userList)->with ( 'orgList', $orgList );
    
    }
        
  }
  
  
public function create() {
    DatabaseConfig::checkDb();
    if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }
        $username = Auth::user ()->username;
        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        //get the Org list
        $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
    
    //log::info( '------login 1---------- '.date('m'));
    if(Session::get('cur')=='dealer'){
        log::info( '------login 1---------- '.Session::get('cur'));
         $tmpOrgList = $redis->smembers('S_Organisations_Dealer_'.$username.'_'.$fcode);
         if(Session::get('cur1')=='prePaidAdmin'){
            $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
            log::info('user name'.$username);
            $dealersDetails=json_decode($dealersDetails_json,true);
            $availableBasicLicence=isset($dealersDetails['avlofBasic'])?$dealersDetails['avlofBasic']:'0';
            $availableAdvanceLicence=isset($dealersDetails['avlofAdvance'])?$dealersDetails['avlofAdvance']:'0';
            $availablePremiumLicence=isset($dealersDetails['avlofPremium'])?$dealersDetails['avlofPremium']:'0';
            $availablePremPlusLicence=isset($dealersDetails['avlofPremiumPlus'])?$dealersDetails['avlofPremiumPlus']:'0';
           if($availableBasicLicence==null){
                $availableBasicLicence=0;
            }
            if($availableAdvanceLicence==null){
                $availableAdvanceLicence=0;
            }
            if($availablePremiumLicence==null){
                $availablePremiumLicence=0;
            }
            if($availablePremPlusLicence==null){
                $availablePremPlusLicence=0;
            }
            $availableLincence=$availableBasicLicence+$availableAdvanceLicence+$availablePremiumLicence+$availablePremPlusLicence;
        }

      }
      else if(Session::get('cur')=='admin'){
         $tmpOrgList = $redis->smembers('S_Organisations_Admin_'.$fcode);
         $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
         $franchiseDetails=json_decode($franDetails_json,true);
         $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
         if($prepaid == 'yes'){    
            $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
            $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
            $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
            $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
            if($availableBasicLicence==null){
                $availableBasicLicence=0;
            }
            if($availableAdvanceLicence==null){
                $availableAdvanceLicence=0;
            }
            if($availablePremiumLicence==null){
                $availablePremiumLicence=0;
            }
            if($availablePremPlusLicence==null){
                $availablePremPlusLicence=0;
            }
            log::info($availableBasicLicence.' '.$availableAdvanceLicence.' '.$availablePremiumLicence.' '.$availablePremPlusLicence);
            $availableLincence=$availableBasicLicence+$availableAdvanceLicence+$availablePremiumLicence+$availablePremPlusLicence;
        }else{
          $availableLincence=isset($franchiseDetails['availableLincence'])?$franchiseDetails['availableLincence']:'0';
          $availableBasicLicence=0;
          $availableAdvanceLicence=0;
          $availablePremiumLicence=0;
          $availablePremPlusLicence=0;
         }
      }
      
    
        $orgList=null;
        $orgList=array_add($orgList,'Default','Default');
        foreach ( $tmpOrgList as $org ) {
                $orgList = array_add($orgList,$org,$org);
                
        }
        $dealerId=null;$userList=null;$numberofdevice=0;
    return view ( 'vdm.business.deviceAdd' )->with ( 'orgList', $orgList )->with ( 'availableBasicLicence', $availableBasicLicence )->with ( 'availableAdvanceLicence', $availableAdvanceLicence )->with ( 'dealerId', $dealerId )->with ( 'userList', $userList )->with('availablePremiumLicence',$availablePremiumLicence)->with('availablePremPlusLicence',$availablePremPlusLicence)->with('availableLincence',$availableLincence)->with('numberofdevice',$numberofdevice);

}
public function checkUser(){
    log::info( 'ahan'.'-------- laravel test ::----------'.Input::get('id'));
    if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }
    $error=' ';
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        $fcode1=strtoupper($fcode);
    $userId = Input::get ( 'id');
    $userId1 =strtoupper($userId);    
      $val = $redis->hget ( 'H_UserId_Cust_Map', $userId . ':fcode' );
	  $valFirst = $redis->hget ( 'H_UserId_Cust_Map',  ucfirst(strtolower($userId)) . ':fcode' );
      $valUpper = $redis->hget ( 'H_UserId_Cust_Map', strtolower($userId) . ':fcode' );
      $val1= $redis->sismember ( 'S_Users_' . $fcode, $userId );
      $valOrg= $redis->sismember('S_Organisations_'. $fcode, $userId);  
       $valOrg1=$redis->sismember('S_Organisations_Admin_'.$fcode,$userId);
       $valGroup=$redis->sismember('S_Groups_' . $fcode, $userId1 . ':' . $fcode1);
       $valGroup1=$redis->sismember('S_Groups_Admin_'.$fcode,$userId1 . ':' . $fcode1);
        
        if($valGroup==1 || $valGroup1==1 ) {
          log::info('id group exist '.$userId);
          $error= 'Name already exist' ;
        }
        if($valOrg==1 || $valOrg1==1 ) {
          log::info('id org exist '.$userId);
          $error='Name already exist' ;
        }
        if($val1==1 || isset($val) || isset($valFirst) || isset($valUpper)) {
          log::info('id already exist '.$userId);
          $error= 'User Id already exist' ;
        }
        if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) {
          $error= 'Name with admin not acceptable' ;
        }

        $refDataArr = array (

      'error' => $error

      );
    $refDataJson = json_encode ( $refDataArr );

    log::info('changes value '.$error);            
    return Response::json($refDataArr); 
  }
  public function checkDevice()
  {
    log::info( 'ahan'.'-------- laravel test ::----------'.Input::get('id'));
    if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }

    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );

    $deviceid = Input::get ( 'id');

    $vehicleU = strtoupper($deviceid);
    $dev=$redis->hget('H_Device_Cpy_Map',$deviceid);
     $vehicleIdCheck = $redis->sismember('S_Vehicles_' . $fcode, $deviceid);
    $vehicleIdCheck2 = $redis->sismember('S_KeyVehicles', $vehicleU);
    $error=' ';
    if($dev!==null || $vehicleIdCheck==1 || $vehicleIdCheck2==1)
    {
      $error='Device Id already present '.$deviceid;
    }
    $refDataArr = array (

      'error' => $error

      );
    $refDataJson = json_encode ( $refDataArr );

    log::info('changes value '.$error);            
    return Response::json($refDataArr);
  }
  


public function checkvehicle()
  {
    
    if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }

    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );

    $vehicleId = Input::get ( 'id');
    $vehicleU = strtoupper($vehicleId);
  log::info( $fcode.'-------- laravel test ::----------'.$vehicleId);
    $vehicleIdCheck = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
    $vehicleIdCheck2 = $redis->sismember('S_KeyVehicles', $vehicleU);
    $dev=$redis->hget('H_Device_Cpy_Map',$vehicleId);
    $error=' ';
    if($vehicleIdCheck==1 || $vehicleIdCheck2==1 || $dev!=null) 
    {
      $error='Vehicle Id already present '.$vehicleId;
    }
    $refDataArr = array (

      'error' => $error

      );
    $refDataJson = json_encode ( $refDataArr );

    log::info('changes value '.$vehicleIdCheck);
        log::info('changes value keyVehi '.$vehicleIdCheck2);   
    return Response::json($refDataArr);
  }
  
public function getGroup()
  {
    
    if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }

    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );

    $userId = Input::get ( 'id');
log::info( $fcode.'-------- laravel test1 ::----------'.$userId);
$vehRfidYesList=array();
  $vehicleGroups = $redis->smembers ( $userId );
    
    //$vehicleGroups = implode ( '<br/>', $vehicleGroups );


 foreach ( $vehicleGroups as $vehicle ) {
$vehRfidYesList = array_add($vehRfidYesList,$vehicle,$vehicle);
 }$error=' ';
if(count($vehicleGroups)==0)
{
  $error='No groups available ,Please select another user';
}

    $refDataArr = array (

      'groups' => $vehRfidYesList,
      'error'=>$error,
      );

  $refDataJson = json_encode ( $refDataArr );
                     
    return Response::json($refDataArr);
  }
  

public function store() {
    if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }
    
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
      $prepaid='no';
    if(Session::get('cur1')=='prePaidAdmin'){
        $rules = array (
        'LicenceType' => 'required'  
        );           
        $validator = Validator::make ( Input::all (), $rules );  
        if ($validator->fails ()) {
          return Redirect::back()->withInput()->withErrors ( $validator );
        }
        $prepaid='yes';
        $onBasicdevice = Input::get ( 'onBasicdevice' );
        $onAdvancedevice=Input::get('onAdvancedevice');
        $onPremiumdevice =Input::get('onPremiumdevice');
        $onPremiumPlusdevice=Input::get('onPremiumPlusdevice');

        if(Session::get('cur')=='admin'){
          $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
          $franchiseDetails=json_decode($franDetails_json,true);  
          $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
          $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
          $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
          $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
        }else if(Session::get('cur')=='dealer'){
            $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
            log::info('user name'.$username);
            $dealersDetails=json_decode($dealersDetails_json,true);
            $availableBasicLicence=isset($dealersDetails['avlofBasic'])?$dealersDetails['avlofBasic']:'0';
            $availableAdvanceLicence=isset($dealersDetails['avlofAdvance'])?$dealersDetails['avlofAdvance']:'0';
            $availablePremiumLicence=isset($dealersDetails['avlofPremium'])?$dealersDetails['avlofPremium']:'0';
            $availablePremPlusLicence=isset($dealersDetails['avlofPremiumPlus'])?$dealersDetails['avlofPremiumPlus']:'0';

        }
        if($onBasicdevice==null && $onAdvancedevice==null && $onPremiumdevice==null && $onPremiumPlusdevice==null){
           return Redirect::back()->withInput()->withErrors('Please Enter valid Licence Quantity!');
        }else if($availableBasicLicence<$onBasicdevice){
            return Redirect::back()->withInput()->withErrors('Basic Licence Type is not available');
        }else if($availableAdvanceLicence<$onAdvancedevice){
          return Redirect::back()->withInput()->withErrors('Advance Licence Type is not available');
        }else if($availablePremiumLicence<$onPremiumdevice){
          return Redirect::back()->withInput()->withErrors('Premium Licence Type is not available');
        }else if($availablePremPlusLicence<$onPremiumPlusdevice){
          return Redirect::back()->withInput()->withErrors('Premium Plus Licence Type is not available');
        }else{

          $LicenceType=Input::get('LicenceType');
          log::info('licence type'.$LicenceType);
          if($LicenceType=='Basic'){
            log::info('basic count'.$onBasicdevice);
            $numberofdevice=$onBasicdevice;
             $protocol = VdmFranchiseController::getProtocal($LicenceType);
           }else if($LicenceType=='Advance'){
           log::info('Advance count'.$onAdvancedevice);
           $numberofdevice=$onAdvancedevice;
           $protocol = VdmFranchiseController::getProtocal($LicenceType);
          }else if($LicenceType=='Premium'){
            log::info('Premium count'.$onPremiumdevice);
            $numberofdevice=$onPremiumdevice;
            $protocol = VdmFranchiseController::getProtocal($LicenceType);
         }else if($LicenceType=='PremiumPlus'){
            log::info('PremiumPlus count'.$onPremiumPlusdevice);
            $numberofdevice=$onPremiumPlusdevice;
            $protocol = VdmFranchiseController::getProtocal($LicenceType);
         }else{
              return Redirect::to ('Business')->withErrors('Licence Type is Missing');
          }
         return BusinessController::prepaid($numberofdevice,$protocol,$LicenceType,$prepaid);
        }
    }else{
        $rules = array (
          'numberofdevice' => 'required|numeric'      
        );
       $prepaid='no';
       $validator = Validator::make ( Input::all (), $rules );
       log::info( '-------- store in  ::----------');
       $availableLincence=Input::get ( 'availableLincence' );
       if ($validator->fails ()) {
         return view ( 'vdm.business.deviceAddCopy' )->withErrors ( $validator )->with ( 'orgList', null )->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', null )->with ( 'dealerId', null )->with ( 'userList', null )->with('orgList',null);
       } 
      else{
        $numberofdevice = Input::get ( 'numberofdevice' );
        // store
        log::info( '-------- av license in  ::----------'.$availableLincence);
    if($numberofdevice>$availableLincence)
    {
      return view ( 'vdm.business.deviceAddCopy' )->withErrors ( "Your license count is less" )->with ( 'orgList', null )->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', null )->with ( 'dealerId', null )->with ( 'userList', null )->with('orgList',null);
    }
   }
    if(Session::get('cur2')=='ktrackAdmin') {
      $Obj = new KtotController;
      return $Obj->addDevices($numberofdevice,$availableLincence);
    }
    else{
		return BusinessController::notprepaid($numberofdevice,$availableLincence,$prepaid);
    }
   }
  }
  
  public function prepaid($numberofdevice,$protocol,$LicenceType,$prepaid){
        $username = Auth::user ()->username;
        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        $dealerId = $redis->smembers('S_Dealers_'. $fcode);  
        $orgArr = array();
       if($dealerId!=null){
         foreach($dealerId as $org) {
            $orgArr = array_add($orgArr, $org,$org);   
        }
        $dealerId = $orgArr;
       }else{
          $dealerId=null;
         $dealerId = $orgArr;
      }
      $userList=array();
      $userList=BusinessController::getUser();
      $orgList=array();
      $orgList=BusinessController::getOrg();
   
      $Payment_Mode1 =array();
      $Payment_Mode = DB::select('select type from Payment_Mode');
      foreach($Payment_Mode as  $org1) {
         $Payment_Mode1 = array_add($Payment_Mode1, $org1->type,$org1->type);
      }
      $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
    $servername = $franchiesJson;
      //$servername="209.97.163.4";
      if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
        return 'Ipaddress Failed !!!';
      }
      $usernamedb = "root";
      $password = "#vamo123";
      $dbname = $fcode;
      log::info('franci..----'.$fcode);
      log::info('ip----'.$servername);
      $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
      if( !$conn ) {
        die('Could not connect: ' . mysqli_connect_error());
            return 'Please Update One more time Connection failed';
      } else { 
          log::info(' created connection ');
          $max="select id from BatchMove order by id desc limit 1";
          $results = mysqli_query($conn,$max);
          while ($row = mysqli_fetch_array($results)) {
            log::info('clearr');
            log::info($row);
            $maxcount = $row[0];
          }
          $conn->close();
      }
      $rowcount=$maxcount+1;
      $deviceId=array();
      $vehicleId=array();
      $LicenceId=array();
      $n=2;               
      for($i=1;$i<=$numberofdevice;$i++){ 
        $j=$rowcount+400000;
        log::info($j);
        $fid=strtoupper($fcode);
        $devices="GPSNEW_".$j."_".$fid;
        $vehicles=$devices."1";
        $dev=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$devices);
        $veh=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$vehicles);                  
        //$guid='bala8561@gmail.com';
        //$encoded = strtoupper(base64_encode($guid));
        // $decode=base64_decode($encoded);
        for($m=1;$m<=$n;$m++){ 
          if ($dev==null && $veh==null) {  
              $deviceCheck=$devices;
              $vehicles=$deviceCheck."1";
              $deviceId = array_add($deviceId, $i,$deviceCheck);
              $vehicleId = array_add($vehicleId, $i,$vehicles);
              if($LicenceType=='Basic'){
                $unique_id = strtoupper('BC'.uniqid());
              }else if($LicenceType=='Advance'){
                $unique_id = strtoupper('AV'.uniqid());
              }else if($LicenceType=='Premium'){
                $unique_id = strtoupper('PM'.uniqid());
              }else if($LicenceType=='PremiumPlus'){
                $unique_id = strtoupper('PL'.uniqid());
              }
                $LicenceId=array_add($LicenceId,$i,$unique_id);
                $rowcount=$rowcount+1;
                break;
          }else{
              $j=$rowcount+400000+1;
              $devices="GPSNEW_".$j."_".$fid;
              $vehicles=$devices."1";
              $dev=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$devices);
              $veh=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$vehicles);
              $n++;
              $rowcount=$rowcount+1;
          }
        }
      }
      $hidLicence=$LicenceType;
      log::info($LicenceId);
      $availableLincence=0;
      $Licence1=$LicenceType;
      return view ( 'vdm.business.createCopy' )->with ( 'orgList', $orgList )->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', $numberofdevice )->with ( 'dealerId', $dealerId )->with ( 'userList', $userList )->with('orgList',$orgList)->with('Licence',$Licence1)->with('Payment_Mode',$Payment_Mode1)->with('protocol', $protocol)->with('devices', $deviceId)->with('vehicles', $vehicleId)->with('LicenceId',$LicenceId)->with('hidLicence',$hidLicence);

  }
  
   public function notprepaid($numberofdevice,$availableLincence,$prepaid){
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );

      $dealerId = $redis->smembers('S_Dealers_'. $fcode);  
      $orgArr = array();
            // $orgArr = array_add($orgArr, 'OWN','OWN');
      if($dealerId!=null){
         foreach($dealerId as $org) {
            $orgArr = array_add($orgArr, $org,$org);
        }
        $dealerId = $orgArr;
      }else{
        $dealerId=null;
        $dealerId = $orgArr;
      }
      $userList=array();
      $userList=BusinessController::getUser();
      $orgList=array();
      $orgList=BusinessController::getOrg();
      $protocol = BusinessController::getProtocal();
      $Payment_Mode1 =array();
      $Payment_Mode = DB::select('select type from Payment_Mode');
    //log::info( '-------- av  in  ::----------'.count($Payment_Mode));
      foreach($Payment_Mode as  $org1) {
          $Payment_Mode1 = array_add($Payment_Mode1, $org1->type,$org1->type);
        }
      $Licence1 =array();
      $Licence = DB::select('select type from Licence');
      foreach($Licence as  $org) {
         $Licence1 = array_add($Licence1, $org->type,$org->type);
      }
      $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
      $servername = $franchiesJson;
      //$servername="209.97.163.4";
      if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
        return 'Ipaddress Failed !!!';
      }
                $usernamedb = "root";
                $password = "#vamo123";
                $dbname = $fcode;
                log::info('franci..----'.$fcode);
                log::info('ip----'.$servername);
                $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
        if( !$conn ) {
                 die('Could not connect: ' . mysqli_connect_error());
                    return 'Please Update One more time Connection failed';
        } else { 
              log::info(' created connection ');
              $max="select id from BatchMove order by id desc limit 1";
              $results = mysqli_query($conn,$max);

              while ($row = mysqli_fetch_array($results)) {
                     log::info('clearr');
                     log::info($row);
                     $maxcount = $row[0];
             }
                    $conn->close();
       }
      $rowcount=$maxcount+1;
      $deviceId=array();
      $vehicleId=array();
      $n=2;
      for($i=1;$i<=$numberofdevice;$i++) { 
        $j=$rowcount+300000;
        log::info($j);
        $fid=strtoupper($fcode);
        $devices="GPSNEW_".$j."_".$fid;
        $vehicles=$devices."1";
        $dev=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$devices);
        $veh=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$vehicles);          
        for($m=1;$m<=$n;$m++){ 
          if ($dev==null && $veh==null) {  
            $deviceCheck=$devices;
            $vehicles=$deviceCheck."1";
            $deviceId = array_add($deviceId, $i,$deviceCheck);
            $vehicleId = array_add($vehicleId, $i,$vehicles);
            $rowcount=$rowcount+1;
            break;
          }else{
            $j=$rowcount+300000+1;
            $devices="GPSNEW_".$j."_".$fid;
            $vehicles=$devices."1";
            $dev=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$devices);
            $veh=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$vehicles);
            $n++;
            $rowcount=$rowcount+1;
          }
        }
                                  
    }
    $LicenceId=0;$hidLicence=0;
     return view ( 'vdm.business.createCopy' )->with ( 'orgList', $orgList )->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', $numberofdevice )->with ( 'dealerId', $dealerId )->with ( 'userList', $userList )->with('orgList',$orgList)->with('Licence',$Licence1)->with('Payment_Mode',$Payment_Mode1)->with('protocol', $protocol)->with('devices', $deviceId)->with('vehicles', $vehicleId)->with('LicenceId',$LicenceId)->with('hidLicence',$hidLicence);
  }
  
  public static function getProtocal(){

  $redis = Redis::connection ();
  $protocal = $redis->lrange('L_Protocal', 0, -1);
  $getProtocal=array();
  foreach ($protocal as $pro) {
    $value  = explode(":",$pro);
    $check=isset($getProtocal[$value[0]]);
    $len=strlen($check);
            if($len==0)
            { 
      $getProtocal=array_add($getProtocal, $value[0], $value[0].' ('.$value[1].') ');
            }    
            else
            {
        $getProtocal=array_add($getProtocal, $value[0].'1', $value[0].' ('.$value[1].') ');
      }
    }
  return $getProtocal;
}



public static function getUser()
{
  $username = Auth::user ()->username;
      $redis = Redis::connection ();
      $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
  $redisUserCacheId = 'S_Users_' . $fcode; //dummy installation
    $redisGrpCacheId='S_Users_';
    
    if(Session::get('cur')=='dealer')
    {
      log::info( '------login 1---------- '.Session::get('cur'));
      
      $redisUserCacheId = 'S_Users_Dealer_'.$username.'_'.$fcode;
      
    }
    else if(Session::get('cur')=='admin')
    {
      $redisUserCacheId = 'S_Users_Admin_'.$fcode;
    }
    $userList=$redis->smembers($redisUserCacheId);
    $orgArra = array();
    $orgArra = array_add($orgArra, 'select','select');
      foreach($userList as $key => $org) {
         if(!$redis->sismember ( 'S_Users_Virtual_' . $fcode, $org )==1)
     {
       $orgArra = array_add($orgArra, $org,$org);
     }  
        }
      $userList=$orgArra;

      return $userList;
}

public static function getdealer()
{
  $username = Auth::user ()->username;
      $redis = Redis::connection ();
      $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
  $dealerId = $redis->smembers('S_Dealers_'. $fcode);  
        $orgArr = array();
            // $orgArr = array_add($orgArr, 'OWN','OWN');
    if($dealerId!=null)
    {
      foreach($dealerId as $org) {
            $orgArr = array_add($orgArr, $org,$org);
      
        }
    $dealerId = $orgArr;
    }
    else{
      $dealerId=null;
      //$orgArr = array_add($orgArr, 'OWN','OWN');
      $dealerId = $orgArr;
    }

    return $dealerId;
}




public static function getOrg()
{
  $username = Auth::user ()->username;
      $redis = Redis::connection ();
      $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
  $orgList=null;
      $orgListId = 'S_Organisations_' . $fcode;
      if(Session::get('cur')=='dealer')
      {
        log::info( '------login 1---------- '.Session::get('cur'));
         $orgListId = 'S_Organisations_Dealer_'.$username.'_'.$fcode;
      }
      else if(Session::get('cur')=='admin')
      {
         $orgListId = 'S_Organisations_Admin_'.$fcode;
      }
        Log::info('orgListId=' . $orgListId);
        $orgList = $redis->smembers ( $orgListId);
        $orgArray = array();  
        $orgArray = array_add($orgArray, '','select');
        foreach ( $orgList as $org ) {
            
            $orgArray = array_add($orgArray, $org,$org);
        }
        
        $orgList=$orgArray;

return $orgList;
} 

public function adddevice() {
   if (! Auth::check ()) {
      return Redirect::to ( 'login' );
      }
      //dd(Input::get());
      $username = Auth::user ()->username;
      $redis = Redis::connection ();
      $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
      $fcode1=strtoupper($fcode);
      $ownerShip      = Input::get('dealerId');
      $userId      = Input::get('userId');
      $userId1 = strtoupper($userId);
      $mobileNoUser      = Input::get('mobileNoUser');
      $emailUser      = Input::get('emailUser');
      $password      = Input::get('password');
      $type      = Input::get('type');
      $type1      = Input::get('type1');
      
      $numberofdevice = Input::get ( 'numberofdevice1' );
      $hidLicence=Input::get('hidLicence');
      // $orgList=array();
      // $orgList=BusinessController::getOrg();
      $groupnameId=array();
      $groupnameId[0]=$userId;
      $userList=array();
      $userList=BusinessController::getUser();
      $orgList=array();
      $orgList=BusinessController::getOrg();
      $dealerId=array();
      $dealerId=BusinessController::getdealer();
      $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
      $franchiseDetails=json_decode($franDetails_json,true);
      $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
    $availableLincence=isset($franchiseDetails['availableLincence'])?$franchiseDetails['availableLincence']:'0';
    
      if($prepaid=='yes'){
           $protocol = VdmFranchiseController::getProtocal($hidLicence);
      }else{
           $protocol = BusinessController::getProtocal();
      }
     
                         $Payment_Mode1 =array();
                         $Payment_Mode = DB::select('select type from Payment_Mode');
                         foreach($Payment_Mode as  $org1) {
                                 $Payment_Mode1 = array_add($Payment_Mode1, $org1->type,$org1->type);
                                 }
                                         $Licence1 =array();
                                         $Licence = DB::select('select type from Licence');
                                         foreach($Licence as  $org) {
                                 $Licence1 = array_add($Licence1, $org->type,$org->type);
                                 }
        //thiru
      if( $type1 != null && $type1 =='new')
      {
        if(Session::get('cur')=='dealer')   
            {   
                log::info( '------login 1---------- '.Session::get('cur'));   
                
                $totalReports = $redis->smembers('S_Users_Reports_Dealer_'.$username.'_'.$fcode);   
            }   
            else if(Session::get('cur')=='admin')   
            {   
          $totalReports = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
            }
            if($totalReports != null)
            {
              foreach ($totalReports as $key => $value) {
                $redis-> sadd('S_Users_Reports_'.$userId.'_'.$fcode, $value);
              }
            }
        // addReportsForNewUser($userId);
      }
      if($type1=='existing')
      {
        $userId      = Input::get('userIdtemp');
        if($userId==null ||$userId=='select')
        {
          Session::flash ( 'message', 'Please select user Id !' );
          return Redirect::to ( 'Business' )->withErrors ( 'Please select user Id !' );
        } 
      }
      if($type==null && Session::get('cur')=='admin')
      {
        log::info($ownerShip.'valuse ----------->'.Input::get('userIdtemp'));
        Session::flash ( 'message', 'select the sale!' );
        //return view ( 'vdm.business.store');
        return Redirect::back()->withInput()->withErrors('select the sale!');
        //return view ( 'vdm.business.create' )->withErrors ( "select the sale!" )->with ( 'orgList', null )->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', $numberofdevice )->with ( 'dealerId', $dealerId )->with ( 'userList', $userList )->with('orgList',$orgList);

      } 
      if($type=='Sale' && Session::get('cur')!='dealer')
      {
        log::info($ownerShip.'1 ----------->'.$type);

        $ownerShip      = 'OWN';
      }
      if(Session::get('cur')=='dealer' ){
        log::info($ownerShip.'2 --a--------->'.Session::get('cur'));
        if($type1==null)
        {
          //return Redirect::to ( 'Business' )->withErrors ( 'select the sale' );
          return Redirect::back()->withErrors('select the sale!');
        }
          $type='Sale';
          $ownerShip = $username;
          $mobArr = explode(',', $mobileNoUser);
      }
      if($type=='Sale' && $type1==null)
      {
        //return Redirect::to ( 'Business' )->withErrors ( 'Select the user' );
        return Redirect::back()->withErrors('Select the user !');
      }
       if($type=='Sale' && $type1=='new')
      {
        log::info($ownerShip.'3----a------->'.Session::get('cur'));
         $rules = array (
        'userId' => 'required|alpha_dash',
        'emailUser' => 'required|email',
        'mobileNoUser' => 'required|numeric',
        'password' => 'required',
        
        );             
                
        $validator = Validator::make ( Input::all (), $rules );        
        if ($validator->fails ()) {
          return Redirect::back()->withErrors ( $validator );
          //return Redirect::back()->withErrors('Select the user !');
        }else {
            $val = $redis->hget ( 'H_UserId_Cust_Map', $userId . ':fcode' );
            $val1= $redis->sismember ( 'S_Users_' . $fcode, $userId );
            $valOrg= $redis->sismember('S_Organisations_'. $fcode, $userId);  
             $valOrg1=$redis->sismember('S_Organisations_Admin_'.$fcode,$userId);
             $valGroup=$redis->sismember('S_Groups_' . $fcode, $userId1 . ':' . $fcode1);
             $valGroup1=$redis->sismember('S_Groups_Admin_'.$fcode,$userId1 . ':' . $fcode1);
        }
         $Licence1= $hidLicence;
        if($valGroup==1 || $valGroup1==1 || $valOrg==1 || $valOrg1==1 ) {
      if(Session::get('cur1')=='prePaidAdmin') {
        return Redirect::back()->withErrors ( 'User Name already exist' );
      }
      else {
        
         return view ( 'vdm.business.create' )->withErrors ( "User Name already exist" )
         ->with ( 'orgList', null )->with ( 'availableLincence', $availableLincence )
         ->with ( 'numberofdevice', $numberofdevice )->with ( 'dealerId', $dealerId )
         ->with ( 'userList', $userList )->with('Licence',$Licence1)->with('Payment_Mode',$Payment_Mode1)
         ->with('protocol', $protocol)->with('devices', null)->with('vehicles', null)
         ->with('LicenceId',null)->with('hidLicence',$hidLicence);

      }
    }
        if($val1==1 || isset($val)) {
      if(Session::get('cur1')=='prePaidAdmin') {
        return Redirect::back()->withErrors ( 'User Id already exist' );
      }
      else {
        return view ( 'vdm.business.create' )->withErrors ( "User Id already exist" )
         ->with ( 'orgList', null )->with ( 'availableLincence', $availableLincence )
         ->with ( 'numberofdevice', $numberofdevice )->with ( 'dealerId', $dealerId )
         ->with ( 'userList', $userList )->with('Licence',$Licence1)->with('Payment_Mode',$Payment_Mode1)
         ->with('protocol', $protocol)->with('devices', null)->with('vehicles', null)
         ->with('LicenceId',null)->with('hidLicence',$hidLicence);
        
      }
    }
        if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) {
          //return Redirect::back()->withErrors ( 'Name with admin not acceptable' );
      if(Session::get('cur1')=='prePaidAdmin') {
        return Redirect::back()->withErrors ( 'User Name with admin not acceptable' );
      }
      else {
        return view ( 'vdm.business.create' )->withErrors ( "User Name with admin not acceptable" )
         ->with ( 'orgList', null )->with ( 'availableLincence', $availableLincence )
         ->with ( 'numberofdevice', $numberofdevice )->with ( 'dealerId', $dealerId )
         ->with ( 'userList', $userList )->with('Licence',$Licence1)->with('Payment_Mode',$Payment_Mode1)
         ->with('protocol', $protocol)->with('devices', null)->with('vehicles', null)
         ->with('LicenceId',null)->with('hidLicence',$hidLicence);
      }
    }
        

          $mobArr = explode(',', $mobileNoUser);
        foreach($mobArr as $mob){
          $val1= $redis->sismember ( 'S_Users_' . $fcode, $userId );
        if($val1==1 ) 
        {
                  log::info('id alreasy exist '.$mob);
                  //return Redirect::back()->withErrors ( );
          return view ( 'vdm.business.create' )->withErrors ( $mob . ' User Id already exist')
         ->with ( 'orgList', null )->with ( 'availableLincence', $availableLincence )
         ->with ( 'numberofdevice', $numberofdevice )->with ( 'dealerId', $dealerId )
         ->with ( 'userList', $userList )->with('Licence',$Licence1)->with('Payment_Mode',$Payment_Mode1)
         ->with('protocol', $protocol)->with('devices', $deviceId)->with('vehicles', null)
         ->with('LicenceId',null)->with('hidLicence',$hidLicence);
                 }
        } 


      }
        if($type=='Sale')
        {
          
          $groupname1      = Input::get('groupname');
          $groupname       = strtoupper($groupname1);
          $groupnameId     = explode(':',$groupname);
          log::info($userId.'-------------- groupname- 1-------------'.$groupname);
          if($type1=='existing' && ($groupname==null || $groupname=='' || $groupname=='0'))
          {
      return view ( 'vdm.business.create' )->withErrors ( $groupname . ' Invalid GroupName ')
                                ->with ( 'orgList', null )->with ( 'availableLincence', $availableLincence )
                                ->with ( 'numberofdevice', $numberofdevice )->with ( 'dealerId', $dealerId )
                                ->with ( 'userList', $userList )->with('Licence',$Licence1)->with('Payment_Mode',$Payment_Mode1)
                                ->with('protocol', $protocol)->with('devices', $deviceId)->with('vehicles', null)
                                ->with('LicenceId',null)->with('hidLicence',$hidLicence);
          }

          
          
        }
      log::info('value type---->'.$type);
      $organizationId=$userId;
      $orgId=$organizationId;
      if($groupnameId[0]== 0 || $groupnameId[0]== '0' )
      {
      $groupId=$userId;
      }
      else
      {
        $groupId=$groupnameId[0];
      }
      $groupId1=strtoupper($groupId);
      if($ownerShip=='OWN' && $type!='Sale')
      {
        $orgId='Default';
        
      }
      if($userId==null)
      {
        $orgId='Default';
        $organizationId='Default';
      }
                        



$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
$franchiseDetails=json_decode($franDetails_json,true);
if(isset($franchiseDetails['availableLincence'])==1)
$availableLincence=$franchiseDetails['availableLincence'];
else
$availableLincence='0';
$count=0;
$numberofdevice = Input::get ( 'numberofdevice1' );
log::info( '--------number of  device::----------'.$numberofdevice);
$vehicleIdarray=array();
$deviceidarray=array();
$KeyVehicles=array();
$SpecialdChar=array();
$emptyChar=array();
$vehDevSame=null;
$current = Carbon::now();
$trkpt=$current->addYears(1)->format('Y-m-d');
log::info($trkpt);
if($type=='Sale' && $type1!=='new')
{
  $orgId=Input::get ( 'orgId');
  if($orgId==null && $redis->sismember('S_Organisations_'.$fcode, $userId) && $userId !==null){
          $orgId=$userId;
  }
}
//ram vehicleExpiry default date

   $vehicleExpiry=Input::get ( 'vehicleExpiry');
   log::info('vehicle expired date--------------->');
   log::info($vehicleExpiry);
   if($fcode == 'TRKPT') {
      $vehicleExpiry1=!empty($vehicleExpiry) ? $vehicleExpiry : $trkpt;
      $vehicleExpiry=$vehicleExpiry1;
   }else if($prepaid=='yes'){
     /*$vehicleExpiry=!empty($vehicleExpiry) ? $vehicleExpiry : '';*/  
        $vehicleExpiry = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 1 year"));
   }else{
      $vehicleExpiry=!empty($vehicleExpiry) ? $vehicleExpiry :$trkpt;
   }
$current = Carbon::now();

if($type=='Sale')
{
  $mailSubject = 'Licences Onboarded to User - '.$userId;
  $dealerOrUser = 'User Name';
  $onboardDate=$current->format('d-m-Y');
  if($prepaid == 'yes'){
  $LicenceOnboardDate=$current->format('d-m-Y');
  $LicenceExpiryDate = date('d-m-Y',strtotime(date("d-m-Y", time()) . "+ 1 year"));
  }else{
    $LicenceExpiryDate='';
    $LicenceOnboardDate='';
  }

}
else{
$mailSubject = 'Licences Added to Dealer - '.$ownerShip;
$dealerOrUser = 'Dealer Name';
$onboardDate='';
$vehicleExpiry='';
$LicenceExpiryDate='';
$LicenceOnboardDate='';
}
$licissueddate=$current->format('d-m-Y');

//
$devicearray=array();
$dbarray=array();
$dbtemp=0;
log::info($numberofdevice);
$typeship=$type;
for($i =1;$i<=$numberofdevice;$i++)
{
  //$vehi=Input::get ( 'vehicleIdSale43');
  if($typeship=='Sale')
  { 
      
    if ($i >= 44)
    {
    // $j=$i-1;
    //  $vehicleId43 = $vehicleId1;
    //  $mobArr = explode('_D1', $vehicleId43);
    //  $ram1 = explode('GPSTEST_', $mobArr[0]);
    //  $addLic=$numberofdevice-$j;
    //  $totalLic=$ram1[1]+$addLic;
    //  $autoGeNumber=$ram1[1];
    //  $autoGeNumber1=$autoGeNumber+1;
      
    //  $deviceid ='GPSTEST_'.$autoGeNumber1.'_D';
    //  $vehicleId1='GPSTEST_'.$autoGeNumber1.'_D1';
    $typeship='Move';
    
    }
    else
    {
    $deviceid = Input::get ( 'deviceid'.$i);
    $vehicleId1 = Input::get ( 'vehicleId'.$i);
    
    }
  }
 else
  {
    if ($i >= 44)
    {
      $j=$i-1;
      $vehicleId43 = $vehicleId1;
      $fid=strtoupper($fcode);
      $sep=$fid."1";
      $mobArr = explode($sep, $vehicleId43);
      $ram1 = explode('GPSNEW_', $mobArr[0]);
      $addLic=$numberofdevice-$j;
      $totalLic=$ram1[1]+$addLic;
      $autoGeNumber=$ram1[1];
      $autoGeNumber1=$autoGeNumber+1;
      
      $deviceid ='GPSNEW_'.$autoGeNumber1.'_'.$fid;
      $vehicleId1='GPSNEW_'.$autoGeNumber1.'_'.$sep;
      
    }
    else
    {
    $deviceid = Input::get ( 'deviceidSale'.$i);
    $vehicleId1 = Input::get ( 'vehicleIdSale'.$i);
       
    }
    
 }
  //
   $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/';
     if (preg_match($pattern, $vehicleId1))
     {
    $vehcle=str_replace('.', '^', $vehicleId1);
        $SpecialdChar=array_add($SpecialdChar,$vehcle,$vehcle);
        // return Redirect::back()->withErrors ('Vehicle ID should be in alphanumeric with _ or - Characters '.$vehicleId1);
     }
     else if (preg_match($pattern, $deviceid))
     {
    $devid=str_replace('.', '^', $deviceid);
        $SpecialdChar=array_add($SpecialdChar,$devid,$devid);
    //return Redirect::back()->withErrors ( 'Device ID should be in alphanumeric with _ or - Characters '.$deviceid);
     }
    else if ($vehicleId1==null || $deviceid==null)
     {
    if($vehicleId1==null) {
      $emptyId=$deviceid;
        }  else {
         $emptyId=$vehicleId1;
        }
        $emptyChar=array_add($emptyChar,$emptyId,$emptyId);
    //return Redirect::back()->withErrors ( 'Device Id or Vehicle Id is empty');
     }else if($vehicleId1==$deviceid){
        $vehDevSame=array_add($vehDevSame,$vehicleId1,$vehicleId1);
     }

 //  $vehicleId2 = str_replace('.', '-', $vehicleId1);
  else {
    $vehicleId = strtoupper($vehicleId1);
  
   $vehicleId=!empty($vehicleId) ? $vehicleId : 'GPSVTS_'.substr($deviceid, -7);
   //isset($vehicleRefData['shortName'])?$vehicleRefData['shortName']:'nill';
   log::info( Input::get('deviceidtype50').'--------number of  name::----------'.$i);
   $deviceid=str_replace(' ', '', $deviceid);
   $vehicleId=str_replace(' ', '', $vehicleId);
   
   $devicearray= array_add($devicearray,$vehicleId1,$deviceid);
   $shortName1=Input::get ( 'shortName'.$i);
   $shortName = strtoupper($shortName1);
   $shortName=!empty($shortName) ? $shortName : $vehicleId;        
   $regNo=Input::get ( 'regNo'.$i);  
   $regNo=!empty($regNo) ? $regNo : 'XXXX';
  
   $orgId=!empty($orgId) ? $orgId : 'Default';
  /* if($ownerShip!=='OWN')
   {
     $orgId='Default';
   }*/
   $vehicleType=Input::get ( 'vehicleType'.$i);  
   $vehicleType=!empty($vehicleType) ? $vehicleType : 'Bus';
   if($prepaid == 'yes'){
    $LicenceId=Input::get('LicenceId'.$i);
   }
   $oprName=Input::get ( 'oprName'.$i);
   $oprName=!empty($oprName) ? $oprName : 'Airtel';
   $mobileNo=Input::get ( 'mobileNo'.$i);
   $mobileNo=!empty($mobileNo) ? $mobileNo : '0123456789';
   $odoDistance=Input::get ( 'odoDistance'.$i);  
   $odoDistance=!empty($odoDistance) ? $odoDistance : '0';
   $overSpeedLimit=Input::get ( 'overSpeedLimit'.$i);
   $overSpeedLimit=!empty($overSpeedLimit) ? $overSpeedLimit : '60'; 
   $driverName=Input::get ( 'driverName'.$i);  
   $driverName=!empty($driverName) ? $driverName : ''; 
   $email=Input::get ( 'email'.$i);  
   $email=!empty($email) ? $email : '';  
   //
  
   $altShortName=Input::get ( 'altShortName'.$i);  
   $altShortName=!empty($altShortName) ? $altShortName : '';           
   $sendGeoFenceSMS=Input::get ( 'sendGeoFenceSMS'.$i);
   $sendGeoFenceSMS=!empty($sendGeoFenceSMS) ? $sendGeoFenceSMS : 'no';  
   $morningTripStartTime=Input::get ( 'morningTripStartTime'.$i);
   $morningTripStartTime=!empty($morningTripStartTime) ? $morningTripStartTime : ''; 
   $eveningTripStartTime=Input::get ( 'eveningTripStartTime'.$i);  
   $eveningTripStartTime=!empty($eveningTripStartTime) ? $eveningTripStartTime : '';
   $gpsSimNo=Input::get ( 'gpsSimNo'.$i);  
   $gpsSimNo=!empty($gpsSimNo) ? $gpsSimNo : '0123456789';
   $Licence=Input::get ( 'Licence'.$i);  
   log::info('licence type --------->'.$Licence);
   //$Licence=!empty($Licence) ? $Licence : 'Advance';
   $descriptionStatus=Input::get ( 'descr'.$i);  
   $descriptionStatus=!empty($descriptionStatus) ? $descriptionStatus : '';

   $Payment_Mode=Input::get ( 'Payment_Mode'.$i);  
   $Payment_Mode=!empty($Payment_Mode) ? $Payment_Mode : 'Monthly';
  
  if(($type=='Sale') && ($i < 44 ))
  {
   $deviceidtype=Input::get('deviceidtype'.$i);
   $deviceModel=$deviceidtype;
   $licence_id = DB::select('select licence_id from Licence where type = :type', ['type' => $Licence]);
   $payment_mode_id = DB::select('select payment_mode_id from Payment_Mode where type = :type', ['type' => $Payment_Mode]);
    
    
   $licence_id=$licence_id[0]->licence_id;
   $payment_mode_id=$payment_mode_id[0]->payment_mode_id;
  }
  else{
   $deviceidtype='BSTPL';
    $deviceModel=$deviceidtype;
      $licence_id='1';
      $payment_mode_id='4';
    
  }
   log::info( $deviceid.'-------- av  in  ::----------'.$deviceidtype.' '.$licence_id.' '.$payment_mode_id);
   if($deviceid!==null && $deviceidtype!==null && $licence_id!==null && $payment_mode_id!==null )
   {
     log::info('------temp-----a----- ');
     $dev=$redis->hget('H_Device_Cpy_Map',$deviceid);
     $dev1=$redis->hget('H_Device_Cpy_Map',$vehicleId);
     $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
     $back=$redis->hget($vehicleDeviceMapId, $deviceid);
     $back = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
     $devBack = $redis->sismember('S_Vehicles_' . $fcode, $deviceid);
     $back1 = $redis->sismember('S_KeyVehicles', $vehicleId);
     log::info('------keyvehicle---------- '.$back1);
     $query = DB::table('Vehicle_details')->select('vehicle_id')->where('vehicle_id',$vehicleId)->get();
     if($back==1 || $devBack==1 || $dev1!=null || $query!=null)
     { 
       $vehicleIdarray=array_add($vehicleIdarray,$vehicleId,$vehicleId);
       log::info($vehicleIdarray);
     }
     if($back1==1)
     { 
       $KeyVehicles=array_add($KeyVehicles,$vehicleId,$vehicleId);
     }
     if(Session::get('cur')=='dealer')
     {         
       $tempdev=$redis->hget('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceid);
     }
     else if(Session::get('cur')=='admin')
     {
       $tempdev=$redis->hget('H_Pre_Onboard_Admin_'.$fcode,$deviceid);
     }
     if($dev1==null && $dev==null && $tempdev==null && $back==0 && $back1==0 && $devBack==0 && $query==null)
     {
       log::info('------temp---------- ');
       $count++;
       $deviceDataArr = array (
         'deviceid' => $deviceid,
         'deviceidtype' => $deviceidtype
       );
       $deviceDataJson = json_encode ( $deviceDataArr );
      
       $deviceDataArr = array (
           'deviceid' => $deviceid,
          'deviceidtype' => $deviceModel,
         );
       $deviceDataJson = json_encode ( $deviceDataArr );
       $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
       $shortName=str_replace(' ', '', $shortName);
       $orgId1=strtoupper($orgId);
       if($ownerShip!=='OWN')
         {
           $redis->sadd('S_Vehicles_Dealer_'.$ownerShip.'_'.$fcode,$vehicleId);
           $redis->srem('S_Vehicles_Admin_'.$fcode,$vehicleId);
           $redis->hset('H_VehicleName_Mobile_Dealer_'.$ownerShip.'_Org_'.$fcode, $vehicleId.':'.$deviceid.':'.$shortName.':'.$orgId1.':'.$gpsSimNo, $vehicleId );
          
          $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
          $servername = $franchiesJson;
           //$servername="209.97.163.4";
           if (strlen($servername) > 0 && strlen(trim($servername) == 0))
           {
             // $servername = "188.166.237.200";
           return 'Ipaddress Failed !!!';
           }
           $usernamedb = "root";
           $password = "#vamo123";
           $dbname = $fcode;
           log::info('franci..----'.$fcode);
           log::info('ip----'.$servername);
           $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
           if( !$conn ) {
             die('Could not connect: ' . mysqli_connect_error());
             return 'Please Update One more time Connection failed';
           } else { 
                     log::info(' created connection ');
                      $max="SELECT COUNT(id) FROM BatchMove";
                      $results = mysqli_query($conn,$max);
                      while ($row = mysqli_fetch_array($results)) 
                     {
                      $maxcount = $row[0];
                      }
          $j=$maxcount+$i;
          $dateTime=Carbon::now();
                    $insertval = "INSERT INTO BatchMove (Device_Id, Dealer_Name, Vehicle_Id) VALUES ('$deviceid','$ownerShip','$vehicleId')"; 
                    $conn->multi_query($insertval);         
                    $conn->close();
          
                 }
        
         
        
         }
         else if($ownerShip=='OWN')
         {
           $redis->sadd('S_Vehicles_Admin_'.$fcode,$vehicleId);
           $redis->srem('S_Vehicles_Dealer_'.$ownerShip.'_'.$fcode,$vehicleId);
           $redis->hset('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode, $vehicleId.':'.$deviceid.':'.$shortName.':'.$orgId1.':'.$gpsSimNo.':OWN', $vehicleId );
         }   
       
       $back=$redis->hget($vehicleDeviceMapId, $deviceid);
       if($back!==null)
       {
         log::info('------temp---------- ');
         $vehicleId=$back;
       }
       else
       {
           if(Session::get('cur')=='dealer')
         {         
           $redis->sadd('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceid);
           $redis->hset('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceid,$deviceDataJson);
         }
         else if(Session::get('cur')=='admin')
         {
           $redis->sadd('S_Pre_Onboard_Admin_'.$fcode,$deviceid);
           $redis->hset('H_Pre_Onboard_Admin_'.$fcode,$deviceid,$deviceDataJson);



         }

           $v=idate("d") ;
           $monthTemp=idate("m") ;
           //log::info($monthTemp.'------monthTemp---------- ');
           $paymentmonth=11;
           if($v>15)
           {
             log::info('inside if');
             $paymentmonth=$paymentmonth+1;    
           }
           if($monthTemp==1)
           {
             if($v==29 || $v==30 || $v==31)
            {
               $paymentmonth=0;  
               $new_date = 'February '.(date('Y', strtotime("0 month"))+1);
               $new_date2 = 'February'.(date('Y', strtotime("0 month"))+1);
               //log::info($new_date.'------new_date feb---------- '.$new_date2);
             }
           }
           for ($m = 1; $m <=$paymentmonth; $m++){

             $new_date = date('F Y', strtotime("$m month"));
               $new_date2 = date('FY', strtotime("$m month"));
               //log::info($new_date.'------ownership---------- '.$m);
             }
             $new_date1 = date('F d Y', strtotime("+0 month"));
             $refDataArr = array (
                 'deviceId' => $deviceid,          
                 'deviceModel' => $deviceModel,
                 'shortName' => $shortName,
                'regNo' => $regNo,
                 'orgId'=>$orgId,
                 'vehicleType' => $vehicleType,
                 'oprName' => $oprName,
                 'mobileNo' => $mobileNo,
                 'odoDistance' => $odoDistance,
                 'gpsSimNo' => $gpsSimNo,
                 'paymentType'=>'yearly',
                 'OWN'=>$ownerShip,
                 'expiredPeriod'=>$new_date,         
                 'overSpeedLimit' => $overSpeedLimit,          
                 'driverName' => $driverName,
                 'email' => $email,
                 //'vehicleExpiry' => $vehicleExpiry,
                 'altShortName'=>$altShortName,
                 'sendGeoFenceSMS' => $sendGeoFenceSMS,
                 'morningTripStartTime' => $morningTripStartTime,
                 'eveningTripStartTime' => $eveningTripStartTime,
                 'parkingAlert' => 'no',
                 'vehicleMake' => '',
                 'Licence'=>$Licence,
                 'Payment_Mode'=>$Payment_Mode,
                 'descriptionStatus'=>$descriptionStatus,
                 'vehicleExpiry' => $vehicleExpiry,
                 'onboardDate' => $onboardDate,
                 'tankSize'=>'0',
                 'licenceissuedDate'=>$licissueddate,
                 'communicatingPortNo'=>'',
               );
             $refDataJson = json_encode ( $refDataArr );
             log::info($refDataJson);
             $expireData=$redis->hget ( 'H_Expire_' . $fcode, $new_date2);
             $redis->hset ( 'H_RefData_' . $fcode, $vehicleId, $refDataJson );

             log::info('before AuditVehicle table insert');
          try{
            $mysqlDetails =array();
            $status =array();
            $status=array(
              'fcode' => $fcode,
              'vehicleId' => $vehicleId,
              'userName'=>$username,
              'status' => Config::get('constant.created'),
              'userIpAddress'=>Session::get('userIP'),
            );
            $mysqlDetails   = array_merge($status,$refDataArr);
            $modelname = new AuditVehicle();   
            $table = $modelname->getTable();
                $db=$fcode;
            AuditTables::ChangeDB($db);
          $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
          if(count($tableExist)>0){
            AuditVehicle::create($mysqlDetails);
          }
          else{
            AuditTables::CreateAuditVehicle();
            AuditVehicle::create($mysqlDetails);
          }
          AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
          }catch (Exception $e) {
                           AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                           log::info('Error inside AuditVehicle Create'.$e->getMessage());
                           log::info($mysqlDetails);
                        }
          log::info('successfully inserted into AuditVehicle');
      
      
        
        if($prepaid == 'yes'){
              $redis->hset('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId,$vehicleId);
             $redis->hset('H_Vehicle_LicenceId_Map_'.$fcode,$vehicleId,$LicenceId);
             
             log::info('Licence Expiry Date ---->'.$LicenceExpiryDate);
             $LiceneceDataArr=array(
                'LicenceissuedDate'=>$licissueddate,
                'LicenceOnboardDate' =>$LicenceOnboardDate,
                'LicenceExpiryDate' =>$LicenceExpiryDate,
             );
             $LicenceExpiryJson = json_encode ( $LiceneceDataArr );
             $redis->hset ( 'H_LicenceExpiry_'.$fcode, $LicenceId,$LicenceExpiryJson);
            $redis->sadd('S_LicenceList_'.$fcode,$LicenceId);
             if(Session::get('cur') =='dealer'){
                $redis->sadd('S_LicenceList_dealer_'.$username.'_'.$fcode,$LicenceId);
             }else if(Session::get('cur') =='admin'){
                 $redis->sadd('S_LicenceList_admin_'.$fcode,$LicenceId);
             }
             
       
       $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
       $servername = $franchiesJson;
       //$servername="209.97.163.4";
           if (strlen($servername) > 0 && strlen(trim($servername) == 0))
           {
             // $servername = "188.166.237.200";
           return 'Ipaddress Failed !!!';
           }
           $usernamedb = "root";
           $password = "#vamo123";
           $dbname = $fcode;
           log::info('franci..----'.$fcode);
           log::info('ip----'.$servername);
           $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
           if( !$conn ) {
             die('Could not connect: ' . mysqli_connect_error());
             return 'Please Update One more time Connection failed';
           } else {    
             log::info('inside mysql to onboard');
             $ins="INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$username','$LicenceId','$vehicleId','$deviceid','$Licence','OnBoard')";
            //$conn->multi_query($ins);
              if($conn->multi_query($ins)){
                  log::info('Successfully inserted');
              }else{
              log::info('not inserted');
              }
        $conn->close();
           }
       
       
       
        }

      
             //$redis->hset ( 'H_VehicleName_Mobile_Org_' .$fcode, $vehicleId.':'.$deviceid.':'.$shortName.':'.$orgId1.':'.$gpsSimNo, $vehicleId );
             ///$redis->hset('H_Vehicle_Map_Uname_' . $fcode, $vehicleId.'/'.$groupId . ':' . $fcode, $userId);
             ///ram noti
             $newOne=$redis->sadd('S_'.$vehicleId.'_'.$fcode, 'S_'.$groupId . ':' . $fcode);
             $newOneUser=$redis->sadd('S_'.$groupId . ':' . $fcode, $userId);
             ///
             $cpyDeviceSet = 'S_Device_' . $fcode;
             $redis->sadd ( $cpyDeviceSet, $deviceid );
             $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
             $redis->hmset ( $vehicleDeviceMapId, $vehicleId , $deviceid, $deviceid, $vehicleId );   
             $redis->sadd ( 'S_Vehicles_' . $fcode, $vehicleId );
             $redis->hset('H_Device_Cpy_Map',$deviceid,$fcode);
             $redis->sadd('S_Vehicles_'.$orgId.'_'.$fcode , $vehicleId);
             if($expireData==null)
             {
               $redis->hset ( 'H_Expire_' . $fcode, $new_date2,$vehicleId);
             }else{
                $redis->hset ( 'H_Expire_' . $fcode, $new_date2,$expireData.','.$vehicleId);
             }
             $time =microtime(true);
             $redis->sadd('S_Organisation_Route_'.$orgId.'_'.$fcode,$shortName);
             $time = round($time * 1000);
             $tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,' .$odoDistance. ',S,N';
           $LatLong=$redis->hget('H_Franchise_LatLong',$fcode);
             if($LatLong != null){
               $data_arr=explode(":",$LatLong);
               $latitude=$data_arr[0];
               $longitude=$data_arr[1];
               $tmpPositon =  $latitude.','.$longitude.',0,N,' . $time . ',0.0,N,N,ON,' .$odoDistance. ',S,N';
			   // newday check
			   $redis->sadd ( 'S_NoDataVehicle',$vehicleId.','.$fcode);
			   $redis->hset ( 'H_NoDataVehicleProcess_Time',$vehicleId.','.$fcode,$time);
             }
             $redis->hset ( 'H_ProData_' . $fcode, $vehicleId, $tmpPositon );
 
			
         }
         
         $details=$redis->hget('H_Organisations_'.$fcode,$organizationId);
         //Log::info($details.'before '.$ownerShip);



         if($type=='Sale')
         {
           // DB::table('Vehicle_details')->insert(
           //     array('vehicle_id' => $vehicleId, 
          //      'fcode' => $fcode,
           //      'sold_date' =>Carbon::now(),
           //      'renewal_date'=>Carbon::now(),
           //      'sold_time_stamp' => round(microtime(true) * 1000),
           //      'month' => date('m'),
           //      'year' => date('Y'),
           //      'payment_mode_id' => $payment_mode_id,
           //      'licence_id' => $licence_id,
           //      'belongs_to'=>'OWN',
           //      'device_id'=>$deviceid,
           //      'status'=>$descriptionStatus)
           // );

           $dbarray[$dbtemp++]=array('vehicle_id' => $vehicleId, 
                 'fcode' => $fcode,
                'sold_date' =>Carbon::now(),
                 'renewal_date'=>Carbon::now(),
                 'sold_time_stamp' => round(microtime(true) * 1000),
                 'month' => date('m'),
                 'year' => date('Y'),
                'payment_mode_id' => $payment_mode_id,
                 'licence_id' => $licence_id,
                 'belongs_to'=>'OWN',
                 'device_id'=>$deviceid,
                 'status'=>$descriptionStatus,
                 'orgId'=>$orgId);

           if($details==null)
           {
             Log::info('new organistion going to create');
             $redis->sadd('S_Organisations_'. $fcode, $organizationId);      
             if($ownerShip!='OWN')
             {
               log::info( '------login 1---------- '.Session::get('cur'));
               $redis->sadd('S_Organisations_Dealer_'.$ownerShip.'_'.$fcode,$organizationId);
             }
             else if($ownerShip=='OWN')
             {
               $redis->sadd('S_Organisations_Admin_'.$fcode,$organizationId);
             }
               $orgDataArr = array (
              'mobile' => '1234567890',
               'description' => '',
               'email' => '',
               'vehicleExpiry' => '',
               'onboardDate' => '',
               'address' => '',
               'mobile' => '',
               'startTime' => '',
               'endTime'  => '',
               'atc' => '',
               'etc' =>'',
               'mtc' =>'',
               'parkingAlert'=>'',
               'idleAlert'=>'',
               'parkDuration'=>'',
               'idleDuration'=>'',
               'overspeedalert'=>'',
               'sendGeoFenceSMS'=>'',
               'radius'=>''
               );
                $orgDataJson = json_encode ( $orgDataArr );
             $redis->hset('H_Organisations_'.$fcode,$organizationId,$orgDataJson );
             $redis->hset('H_Org_Company_Map',$organizationId,$fcode);
             log::info('before Audit Entry for Org-Create');
                        try{
                                $mysqlDetails =array();
                                $status =array();
                                $status=array(
                                  'fcode' => $fcode,
                                  'organizationName' => $organizationId,
                                  'userName'=>$username,
                                  'status' => Config::get('constant.created'),
                                  'userIpAddress'=>Session::get('userIP'),
                                );
                                $mysqlDetails   = array_merge($status,$orgDataArr);
                                $modelname = new AuditOrg();   
                                $table = $modelname->getTable();
                                $db=$fcode;
                                AuditTables::ChangeDB($db);
                                $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
                              if(count($tableExist)>0){
                                AuditOrg::create($mysqlDetails);
                              }
                              else{
                                AuditTables::CreateAuditOrg();
                                AuditOrg::create($mysqlDetails);
                              }
                              AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                          }catch (Exception $e) {
                                               AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                                               log::info('Error inside AuditOrg Create'.$e->getMessage());
                                               log::info($mysqlDetails);
                                            }
                                log::info('After Audit Entry for Org-Create');
            
           }
         }
         if($type=='Sale')
         {
          
           $groupname1      = Input::get('groupname');
           $groupname       = strtoupper($groupname1);
           log::info($userId.'-------------- groupname- out-------------'.$groupId);
           if($type1=='existing' && $groupname!==null && $groupname!=='')
           {
            
             $groupId=explode(":",$groupname)[0];
             log::info('-------------- groupname--------------'.$groupId);
           }

           $redis->sadd($groupId1 . ':' . $fcode1,$vehicleId);
           // log::info('###################################################');
           if($type1=='existing' && $groupname!==null && $groupname!=='')
           {
             
            log::info('before audit entry for group update');
            try{
            $vehicleIDList=$redis->smembers($groupId1. ':' .$fcode1);
            // log::info($vehicleID);
            $mysqlDetails=array(
                'userName'=>$username,
                'status' => Config::get('constant.new_vehicle_added'),
                'fcode' => $fcode,
                'groupName' => $groupname,
                'vehicleID'=>implode(',', $vehicleIDList),
                'userIpAddress'=>Session::get('userIP'),
                // 'vehicleName'=>$vehicleName
            );
            //$table=new AuditGroup->getTable();
            $modelname = new AuditGroup();   
            $table = $modelname->getTable();
            //return $table;
            //return $mysqlDetails;
            $db=$fcode;
            AuditTables::ChangeDB($db);
            $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
            //return $tableExist;
            if(count($tableExist)>0){
                AuditGroup::create($mysqlDetails);
            }
            else{
                AuditTables::CreateAuditGroup();
                AuditGroup::create($mysqlDetails);
            }
            AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
            }catch (Exception $e) {
                           AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                           log::info('Error inside AuditVehicle New Vehicle Added'.$e->getMessage());
                           log::info($mysqlDetails);
                        }
            //AuditGroup::create($details);
            log::info('after audit entry for group update');
           }
          
         }
         if(Session::get('cur')=='dealer')
         {         
           $redis->srem('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceid);
           $redis->hdel('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceid);
         }
         else if(Session::get('cur')=='admin')
         {
           $redis->srem('S_Pre_Onboard_Admin_'.$fcode,$deviceid);
           $redis->hdel('H_Pre_Onboard_Admin_'.$fcode,$deviceid);
           if($ownerShip!=='OWN')
           {
             $redis->sadd('S_Pre_Onboard_Dealer_'.$ownerShip.'_'.$fcode,$deviceid);
             $redis->hset('H_Pre_Onboard_Dealer_'.$ownerShip.'_'.$fcode,$deviceid,$deviceDataJson);
           }
          
         }               
    
   }


   if($dev==null && $tempdev==null && $devBack!=1)
   {

   }
   else{
   log::info('--------------already present--------------'.$deviceid);

   $vehicleIdarray=array_add($vehicleIdarray,$deviceid,$deviceid);

   }
   }
    }
   $deviceid=null;
   $deviceidtype=null;



}
 log::info('--------------count($dbarray--------------'.count($dbarray));
 if(count($dbarray)!==0)
 {
  
   log::info('--------------before--------------'.time());
   try {
    log::info('inside the mysql insertion');
    DB::table('Vehicle_details')->insert(
               $dbarray
           );
  }
   catch(\Exception $e)
   {
   log::info('Mysql Integrity constraint violation');
   log::info($e);

   }


   log::info('--------------after--------------'.time());
 }




 if($type=='Sale' )
 {
  
   log::info( '------sale-2--------- '.$ownerShip);
   $redis->sadd('S_Groups_' . $fcode, $groupId1 . ':' . $fcode1);
   if($ownerShip!='OWN')
   {
     log::info( '------login 1---------- '.Session::get('cur'));
     $redis->sadd('S_Groups_Dealer_'.$ownerShip.'_'.$fcode,$groupId1 . ':' . $fcode1);
     $redis->sadd('S_Users_Dealer_'.$ownerShip.'_'.$fcode,$userId);
   }
   else if($ownerShip=='OWN')
   {
     $redis->sadd('S_Groups_Admin_'.$fcode,$groupId1 . ':' . $fcode1);
     $redis->sadd('S_Users_Admin_'.$fcode,$userId);
   }
   $redis->sadd ( $userId, $groupId1 . ':' . $fcode1 );
   $redis->sadd ( 'S_Users_' . $fcode, $userId );        
   if(Session::get('cur')=='dealer')
   {
     log::info( '------login 1---------- '.Session::get('cur'));
     $OWN=$username;
   }
   else if(Session::get('cur')=='admin')
   {
     $OWN='admin';
   }
            
   if($type1=='new')
   {
     log::info( '------sale--3-------- '.$ownerShip);
     $password=Input::get ( 'password' );
     if($password==null)
     {
       $password='awesome';
     }
     $redis->hmset ( 'H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNoUser,$userId.':email',$emailUser ,$userId.':password',$password,$userId.':OWN',$OWN);            
     $user = new User; 
     $user->name = $userId;
     $user->username=$userId;
     $user->email=$emailUser;
     $user->mobileNo=$mobileNoUser;
     $user->password=Hash::make($password);
     $user->save();
     $groupNameNew=$groupId1. ':' .$fcode1;
     // log::info($groupId1. ':' .$fcode1);
     $vehicleID=$redis->smembers($groupId1. ':' .$fcode1);
     // log::info($vehicleID);

        log::info('before audit entry for group create');
        try{
            $mysqlDetails=array(
                'userName'=>$username,
                'status' => Config::get('constant.created'),
                'fcode' => $fcode,
                'groupName' => $groupNameNew,
                'vehicleID'=>implode(",",$vehicleID),
                'vehicleName'=>$shortName,
                'userIpAddress'=>Session::get('userIP'),
            );
            //$table=new AuditGroup->getTable();
            $modelname = new AuditGroup();   
            $table = $modelname->getTable();
            //return $table;
            //return $mysqlDetails;
            $db=$fcode;
            AuditTables::ChangeDB($db);
            $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
            //return $tableExist;
            if(count($tableExist)>0){
                AuditGroup::create($mysqlDetails);
            }
            else{
                AuditTables::CreateAuditGroup();
                AuditGroup::create($mysqlDetails);
            }
            AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
          }catch (Exception $e) {
                           AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                           log::info('Error inside AuditGroup Create'.$e->getMessage());
                           log::info($mysqlDetails);
                        }
            //AuditGroup::create($details);
     log::info('after audit entry for group create');
     log::info("before audit user create entry");
     try{
      $details = array (
      'fcode' => $fcode,
      'userId' => $userId,
      'userName'=>$username,
      'status' => Config::get('constant.created'),
      'email'=>$emailUser,
      'mobileNo'=>$mobileNoUser,
      'password'=>Hash::make($password),
      'zoho'=> '-',
      'companyName' => '-',
      'cc_email' => '-',
      'groups' =>$groupId1. ':' .$fcode1,
      'vehicles' =>implode(",",$vehicleID),
      'userIpAddress'=>Session::get('userIP'),
      );
        $modelname = new AuditUser();   
        $table = $modelname->getTable();
            $db=$fcode;
        AuditTables::ChangeDB($db);
      $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
      if(count($tableExist)>0){
        AuditUser::create($details);
      }
      else{
        AuditTables::CreateAuditUser();
        AuditUser::create($details);
      }
      AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
      }catch (Exception $e) {
                           AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                           log::info('Error inside AuditVehicle Create'.$e->getMessage());
                           log::info($details);
                        }
      log::info("before audit user create entry");
       
       
     //mobile number dont need user
     // foreach($mobArr as $mob)
     // {           
     //  if($mob!=='')
     //  {
     //    log::info( '------mobile number---------- '.$mob);
     //    if($ownerShip!='OWN')
   //              {
   //                      log::info( '------login 1---------- '.Session::get('cur'));
   //                      $redis->sadd('S_Users_Dealer_'.$ownerShip.'_'.$fcode,$mob);
   //              }
     //    else if($ownerShip=='OWN')
     //    {
     //        $redis->sadd('S_Users_Admin_'.$fcode,$mob);
     //    }
     //    log::info(' mobile number saved successfully');
     //    $redis->sadd ( $mob, $groupId . ':' . $fcode );
   //             $redis->sadd ( 'S_Users_' . $fcode, $mob );   
     //    $password=Input::get ( 'password' );
     //    if($password==null)
     //    {
     //        $password='awesome';
     //    }
     //    $redis->hmset ( 'H_UserId_Cust_Map', $mob . ':fcode', $fcode, $mob . ':mobileNo', $mobileNo,$mob.' :email',$email,$mob.':password',$password,$mob.':OWN',$OWN);

     //          $user = new User;

     //          $user->name = $mob;
     //          $user->username=$mob;
     //          $user->email=$email;
     //          $user->mobileNo=$mobileNo;
     //          $user->password=Hash::make($password);
     //          $user->save();
     //    }
   //           }          
     }
   }
     if(Session::get('cur') =='admin' && $count>0){
        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);
        if($prepaid== 'yes'){
          if($Licence=='Basic'){
            $franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']-$count;
            log::info('inside count present'.$franchiseDetails['availableBasicLicence']);
          }else if($Licence=='Advance'){
            $franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']-$count;
            log::info('inside count present'.$franchiseDetails['availableAdvanceLicence']);
          }else if($Licence=='Premium'){
             $franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']-$count;
              log::info('inside count present'.$franchiseDetails['availablePremiumLicence']);
          }else if($Licence=='PremiumPlus'){
             $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']-$count;
             log::info('inside count present'.$franchiseDetails['availablePremPlusLicence']);
          }
        }else{ 
          $franchiseDetails['availableLincence']=$franchiseDetails['availableLincence']-$count;
          log::info('inside count present'.$franchiseDetails['availableLincence']);
        }
        $detailsJson = json_encode ( $franchiseDetails );
        $redis->hmset ( 'H_Franchise', $fcode,$detailsJson);
      }else if(Session::get('cur') =='dealer' && $count>0 && Session::get('cur1') =='prePaidAdmin'){
            $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
            $dealersDetails=json_decode($dealersDetails_json,true);
            if($Licence=='Basic'){
              $dealersDetails['avlofBasic']= $dealersDetails['avlofBasic']-$count;
            }else if($Licence=='Advance'){
              $dealersDetails['avlofAdvance']=$dealersDetails['avlofAdvance']-$count;
            }else if($Licence=='Premium'){
              $dealersDetails['avlofPremium']=$dealersDetails['avlofPremium']-$count;
            }else if($Licence=='PremiumPlus'){
              $dealersDetails['avlofPremiumPlus']=$dealersDetails['avlofPremiumPlus']-$count;
            }
            $detailsJson = json_encode ( $dealersDetails );
            $redis->hmset ( 'H_DealerDetails_'.$fcode,$username,$detailsJson);
      }


    $error='';
     if(count($vehicleIdarray)!==0 || count($SpecialdChar)!==0 || count($emptyChar)!==0 || count($vehDevSame)!=0)
     {
       $error2=' ';
       $error='';
       $error3=' ';
       $error4='';
        if(count($vehicleIdarray)!==0)
       {
      $error0=implode(",",$vehicleIdarray);
      $error[]= 'These names already exists '.$error0;
       }
       if(count($SpecialdChar)!==0)
       {
      $error2=implode(",", $SpecialdChar);
      $error2=str_replace('^', '.', $error2);
      $error[]= 'Special Characters are used : '.$error2;
       }
       if(count($emptyChar)!==0)
       {
            $error3=implode(",", $emptyChar);
      $error[]= 'Empty Device or Vehicle ID Found: '.$error3;
       }
       if(count($vehDevSame)!=0){
         $error4=implode(",", $vehDevSame);
          $error[]= 'DeviceId and Vehicle Id should not be same: '.$error4;
       }
       if($count>0) {
      $error[]= $count.' Successfully Added';
       }
      } 
      else {
        if($count>0) {
      $error[]= $count.' Successfully Added';
       }
      }
        $emailFcode=$redis->hget('H_Franchise', $fcode);
        $emailFile=json_decode($emailFcode, true);
        $email1=$emailFile['email2'];
        $email2=$emailFile['email1'];
        $onboardDate=$current->format('d-m-Y');
        $i=0;
              log::info('--------------email outsite------------------>');
              $response=Mail::send('emails.onboardDetails', array('fcode'=>$fcode,'i'=>$i,'nod'=>$count,'date'=>$onboardDate,'Own'=>$ownerShip,'Devices'=>$devicearray,'usercheck'=>$dealerOrUser,'username'=>$userId), function($message) use ($email1,$mailSubject)
              {
          $message->to($email1);
          $message->subject($mailSubject);
          log::info('-----------email send------------------>');
              });
        if($ownerShip!='OWN')
              {
              $emailFcode=$redis->hget('H_DealerDetails_'.$fcode,$ownerShip);
              $emailFile=json_decode($emailFcode, true);
              $email1=$emailFile['email'];
              $ownerShip1='OWN';
              $i=0;
              log::info('--------------email outsite------------------>');
        $response=Mail::send('emails.dealeronboard', array('dealerName'=>$ownerShip,'i'=>$i,'nod'=>$count,'Own'=>$ownerShip1,'date'=>$onboardDate,'user'=>$userId,'Devices'=>$devicearray), function($message) use ($email1,$ownerShip)
        {
          $message->to($email1);
          $message->subject('Onboarded Licences - '.$ownerShip);
          log::info('-----------email send------------------>');
        }); 
             }
   return Redirect::to ( 'Business' )->withErrors($error);
    
}

  public function deviceDetails() {
    if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }
    
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
                
    $devicesList=$redis->smembers( 'S_Device_' . $fcode);
    log::info( '------device list size---------- '.count($devicesList));
    $temp=0;
    $deviceMap=array();
    for($i =0;$i<count($devicesList);$i++){
      $vechicle=$redis->hget ( 'H_Vehicle_Device_Map_' . $fcode, $devicesList[$i] );
      $deviceMap = array_add($deviceMap,$i,$vechicle.','.$devicesList[$i]);
      $temp++;
    }
    log::info( '------device map---------- '.count($deviceMap));
    return view ( 'vdm.business.device', array (
        'deviceMap' => $deviceMap ) );
    
  }
  
  public function batchSale()
  {
    
  
    
    if(!Auth::check()) {
      return Redirect::to('login');
    }
    log::info( '------batch Sale---------- ');
    $username = Auth::user()->username;
      $redis = Redis::connection();
      $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
      $fcode1=strtoupper($fcode);
      $deviceList      = Input::get('vehicleList');
      $deviceType      = Input::get('deviceType');
      $ownerShip      = Input::get('dealerId');
      $userId      = Input::get('userId');
      $userId1    = strtoupper($userId);
      $mobileNo      = Input::get('mobileNo');
      $email      = Input::get('email');
      $password      = Input::get('password');
      $type      = Input::get('type');
      $type1      = Input::get('type1');
    $current = Carbon::now();
      $onboardDate=$current->format('d-m-Y');
    $devicearray=array();
      log::info($ownerShip.'type ----------->'.$type1);
      log::info($ownerShip.'valuse ----------->'.Input::get('userIdtemp'));
      if($type1=='existing')
      {
        $userId      = Input::get('userIdtemp');
        if($userId==null || $userId=='select')
        {
          return Redirect::to ( 'Business' )->withErrors ( 'Invalid user Id' );
        }
             
      }
           
        if($type1==null)
        {
          return Redirect::to ( 'Business' )->withErrors ( 'select the sale' );
        }
          $type='Sale';
          $ownerShip = $username;
              $mobArr = explode(',', $mobileNo);
      
      if( $type1==null)
      {
        return Redirect::to ( 'Business' )->withErrors ( 'Select the user' );
      }
       if($type1=='new')
            {
                if(Session::get('cur')=='dealer'){
                    $totalReports = $redis->smembers('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
                }
                if($totalReports != null)
                {
                   foreach ($totalReports as $key => $value) {
                   $redis-> sadd('S_Users_Reports_'.$userId.'_'.$fcode, $value);
                  }
                }
        log::info($ownerShip.'3----a------->'.Session::get('cur'));
         $rules = array (
        'userId' => 'required|alpha_dash',
        'email' => 'required|email',

        );             
                
        $validator = Validator::make ( Input::all (), $rules );
         
        if ($validator->fails ()) {
          return Redirect::to ( 'Business' )->withErrors ( $validator );
        }else {
            $val = $redis->hget ( 'H_UserId_Cust_Map', $userId . ':fcode' );
            $val1= $redis->sismember ( 'S_Users_' . $fcode, $userId );
            $valOrg= $redis->sismember('S_Organisations_'. $fcode, $userId);  
             $valOrg1=$redis->sismember('S_Organisations_Admin_'.$fcode,$userId);
             $valGroup=$redis->sismember('S_Groups_' . $fcode, $userId1 . ':' . $fcode1);
             $valGroup1=$redis->sismember('S_Groups_Admin_'.$fcode,$userId1 . ':' . $fcode1);
			 $valFirst = $redis->hget ( 'H_UserId_Cust_Map', ucfirst(strtolower($userId)) . ':fcode' );
          $valUpper = $redis->hget ( 'H_UserId_Cust_Map', strtolower($userId) . ':fcode' );
        }
        if($valGroup==1 || $valGroup1==1 ) {
          log::info('id group exist '.$userId);
          return Redirect::to ( 'Business' )->withErrors ( 'Name already exist' );
        }
        if($valOrg==1 || $valOrg1==1 ) {
          log::info('id org exist '.$userId);
          return Redirect::to ( 'Business' )->withErrors ( 'Name already exist' );
        }
        if($val1==1 || isset($val) || isset($valFirst) || isset($valUpper)) {
          log::info('id already exist '.$userId);
          return Redirect::to ( 'Business' )->withErrors ( 'User Id already exist' );
        }
        if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) {
          return Redirect::to ( 'Business' )->withErrors ( 'Name with admin not acceptable' );
        }

          $mobArr = explode(',', $mobileNo);
        foreach($mobArr as $mob){
          $val1= $redis->sismember ( 'S_Users_' . $fcode, $userId );
          if($val1==1 ) {
                                          log::info('id alreasy exist '.$mob);
                                          return Redirect::to ( 'Business' )->withErrors ($mob . ' User Id already exist' );
                                  }
        } 


      }
      log::info('value type---->'.$type);
      $organizationId=$userId;
      $orgId=$organizationId;
      $groupId0=$orgId;
      $groupId=strtoupper($groupId0);
      if($ownerShip=='OWN' && $type!='Sale')
      {
        $orgId='Default';
        
      }
      if($userId==null)
      {
        $orgId='Default';
        $organizationId='Default';
      }
      if($type=='Sale' && $type1!=='new')
      {
        $orgId=Input::get ( 'orgId');
        $orgId=!empty($orgId) ? $orgId : 'Default';
        //$organizationId=$orgId;
      }
      if($deviceList!=null)
      {
        $temp=0;
        $dbarray=array();
        $dbtemp=0;
          foreach($deviceList as $device) {
          log::info( '------ownership---------- '.$ownerShip);
          $myArray = explode(',', $device);
          //$vehicleId='GPSVTS_'.substr($myArray[0], -6);
          $deviceId=$myArray[0];
      $vehicleId=$redis->hget('H_Vehicle_Device_Map_' . $fcode,$deviceId);
      $devicearray=array_add($devicearray,$vehicleId,$deviceId);
          $deviceDataArr = array (
              'deviceid' => $deviceId,
              'deviceidtype' => $myArray[1],
            );
            $deviceDataJson = json_encode ( $deviceDataArr );
          $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
          $back=$redis->hget($vehicleDeviceMapId, $deviceId);
              if($back!==null)
              {
                $vehicleId=$back;


                $refDataJson1=$redis->hget ( 'H_RefData_' . $fcode, $vehicleId);
                $refDataJson1=json_decode($refDataJson1,true);
                $temOrg=isset($refDataJson1['orgId'])?$refDataJson1['orgId']:'default';
            


    $licenceId=isset($refDataJson1['Licence'])?$refDataJson1['Licence']:'Advance';
        if($licenceId== null) {
        $licenceId='Advance';
        }
        $paymentId=isset($refDataJson1['Payment_Mode'])?$refDataJson1['Payment_Mode']:'Monthly';
        if($paymentId== null) {
        $paymentId='Monthly';
        }
        $licence_id = DB::select('select licence_id from Licence where type = :type', ['type' => $licenceId]);
        $payment_mode_id = DB::select('select payment_mode_id from Payment_Mode where type = :type', ['type' => $paymentId]);
        log::info( $licence_id[0]->licence_id.'-------- av  in  ::----------'.$payment_mode_id[0]->payment_mode_id);
    
        $licence_id=$licence_id[0]->licence_id;
        $payment_mode_id=$payment_mode_id[0]->payment_mode_id;
try{

  // DB::table('Vehicle_details')->insert(
  //            array('vehicle_id' => $vehicleId, 
  //              'fcode' => $fcode,
  //              'sold_date' =>Carbon::now(),
  //              'renewal_date'=>Carbon::now(),
  //              'sold_time_stamp' => round(microtime(true) * 1000),
  //              'month' => date('m'),
  //              'year' => date('Y'),
  //              'payment_mode_id' => $payment_mode_id,
  //              'licence_id' => $licence_id,
  //              'belongs_to'=>$username,
  //              'device_id'=>$deviceId,
  //              'status'=>isset($refDataJson1['descriptionStatus'])?$refDataJson1['descriptionStatus']:'')
  //        );
$dbarray[$dbtemp++]= array('vehicle_id' => $vehicleId, 
                'fcode' => $fcode,
                'sold_date' =>Carbon::now(),
                'renewal_date'=>Carbon::now(),
                'sold_time_stamp' => round(microtime(true) * 1000),
                'month' => date('m'),
                'year' => date('Y'),
                'payment_mode_id' => $payment_mode_id,
                'licence_id' => $licence_id,
                'belongs_to'=>$username,
                'device_id'=>$deviceId,
                'orgId'=>$orgId,
                'status'=>isset($refDataJson1['descriptionStatus'])?$refDataJson1['descriptionStatus']:'');

                    // if($temOrg=='Default' || $temOrg=='default')
        }catch(\Exception $e)
        {

        }           // {// ram testing


       $refDataArr = array (
                  'deviceId' => $deviceId,
                  'shortName' => isset($refDataJson1['shortName'])?$refDataJson1['shortName']:'',
                  'deviceModel' => isset($refDataJson1['deviceModel'])?$refDataJson1['deviceModel']:'GT06N',
                  'regNo' => isset($refDataJson1['regNo'])?$refDataJson1['regNo']:'XXXXX',
                  'vehicleMake' => isset($refDataJson1['vehicleMake'])?$refDataJson1['vehicleMake']:' ',
                  'vehicleType' =>  isset($refDataJson1['vehicleType'])?$refDataJson1['vehicleType']:'Bus',
                  'oprName' => isset($refDataJson1['oprName'])?$refDataJson1['oprName']:'airtel',
                  'mobileNo' =>isset($refDataJson1['mobileNo'])?$refDataJson1['mobileNo']:'0123456789',
                  'overSpeedLimit' => isset($refDataJson1['overSpeedLimit'])?$refDataJson1['overSpeedLimit']:'60',
                  'odoDistance' => isset($refDataJson1['odoDistance'])?$refDataJson1['odoDistance']:'0',
                  'driverName' => isset($refDataJson1['driverName'])?$refDataJson1['driverName']:'XXX',
                  'gpsSimNo' => isset($refDataJson1['gpsSimNo'])?$refDataJson1['gpsSimNo']:'0123456789',
                  'email' => isset($refDataJson1['email'])?$refDataJson1['email']:' ',
                  'orgId' =>$orgId,
                  'sendGeoFenceSMS' => isset($refDataJson1['sendGeoFenceSMS'])?$refDataJson1['sendGeoFenceSMS']:'no',
                  'morningTripStartTime' => isset($refDataJson1['morningTripStartTime'])?$refDataJson1['morningTripStartTime']:' ',
                  'eveningTripStartTime' => isset($refDataJson1['eveningTripStartTime'])?$refDataJson1['eveningTripStartTime']:' ',
                  'parkingAlert' => isset($refDataJson1['parkingAlert'])?$refDataJson1['parkingAlert']:'no',
                  'altShortName'=>isset($refDataJson1['altShortName'])?$refDataJson1['altShortName']:'',
                  'paymentType'=>isset($refDataJson1['paymentType'])?$refDataJson1['paymentType']:' ',
                  'expiredPeriod'=>isset($refDataJson1['expiredPeriod'])?$refDataJson1['expiredPeriod']:' ',
                  'fuel'=>isset($refDataJson1['fuel'])?$refDataJson1['fuel']:'no',
                  'fuelType'=>isset($refDataJson1['fuelType'])?$refDataJson1['fuelType']:' ',
                  'isRfid'=>isset($refDataJson1['isRfid'])?$refDataJson1['isRfid']:'no',
                  'OWN'=>$ownerShip,

                   'Licence'=>isset($refDataJson1['Licence'])?$refDataJson1['Licence']:'',
                   'Payment_Mode'=>isset($refDataJson1['Payment_Mode'])?$refDataJson1['Payment_Mode']:'',
                   'descriptionStatus'=>isset($refDataJson1['descriptionStatus'])?$refDataJson1['descriptionStatus']:'',
                   'vehicleExpiry'=>isset($refDataJson1['vehicleExpiry'])?$refDataJson1['vehicleExpiry']:'',
                   'onboardDate'=>$onboardDate,  
                   'tankSize'=>isset($refDataJson1['tankSize'])?$refDataJson1['tankSize']:'0',
                   'licenceissuedDate'=>isset($refDataJson1['licenceissuedDate'])?$refDataJson1['licenceissuedDate']:'', 
                   'communicatingPortNo'=>isset($refDataJson1['communicatingPortNo'])?$refDataJson1['communicatingPortNo']:'',
                  );

              $shortName=isset($refDataJson1['shortName'])?$refDataJson1['shortName']:'';
              $refDataJson = json_encode ( $refDataArr );
              
             // $redis->hdel ( 'H_RefData_' . $fcode, $vehicleIdOld );
              $redis->hset ( 'H_RefData_' . $fcode, $vehicleId, $refDataJson );

              log::info('before AuditVehicle table insert');
              try{
              $mysqlDetails =array();
              $status =array();
              $status=array(
                'fcode' => $fcode,
                'vehicleId' => $vehicleId,
                'userName'=>$username,
                'status' => Config::get('constant.created'),
                'userIpAddress'=>Session::get('userIP'),
              );
              $mysqlDetails   = array_merge($status,$refDataArr);
              $modelname = new AuditVehicle();   
              $table = $modelname->getTable();
              $db=$fcode;
              AuditTables::ChangeDB($db);
              $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
              if(count($tableExist)>0){
                AuditVehicle::create($mysqlDetails);
              }else{
                AuditTables::CreateAuditVehicle();
                AuditVehicle::create($mysqlDetails);
              }
              AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
              }catch (Exception $e) {
                           AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                           log::info('Error inside AuditVehicle Create'.$e->getMessage());
                           log::info($mysqlDetails);
                        }
              log::info('successfully inserted into AuditVehicle');
      



//}ram testing

              }
                  log::info( '------login 1---------- '.Session::get('cur'));
                  $redis->sadd('S_Vehicles_Dealer_'.$ownerShip.'_'.$fcode,$vehicleId);
                  $redis->srem('S_Vehicles_Admin_'.$fcode,$vehicleId);
                                 $refDataJson=json_decode($refDataJson,true);
                                 $shortName1=isset($refDataJson['shortName'])?$refDataJson['shortName']:'default';
                                 $mobileNo1=isset($refDataJson['mobileNo'])?$refDataJson['mobileNo']:'0123456789';
                 $gpsSimNo1=isset($refDataJson['gpsSimNo1'])?$refDataJson['gpsSimNo1']:'0123456789';
                                 $orgIdOld1=isset($refDataJson['orgId'])?$refDataJson['orgId']:'default';
                                 $orgIdOld=strtoupper($orgIdOld1);
                                 $orgId1=strtoupper($orgId);
                                 $redis->hset('H_VehicleName_Mobile_Dealer_'.$ownerShip.'_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortName1.':'.$orgId1.':'.$gpsSimNo1, $vehicleId );
                                 $redis->hdel('H_VehicleName_Mobile_Dealer_'.$ownerShip.'_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortName1.':DEFAULT:'.$gpsSimNo1, $vehicleId );                 
                /// $redis->hdel('H_Vehicle_Map_Uname_' . $fcode, $vehicleId.'/:'.$fcode);
                /// $redis->hset('H_Vehicle_Map_Uname_' . $fcode, $vehicleId.'/'.$groupId . ':' . $fcode, $userId);       
                ///ram noti
                 $redis->srem('S_'.$vehicleId.'_'.$fcode, 'S_:' . $fcode);
                 $redis->sadd('S_'.$vehicleId.'_'.$fcode, 'S_'.$groupId .':' . $fcode);
                 $redis->del('S_:' . $fcode);
                 $newOneUser=$redis->sadd('S_'.$groupId . ':' . $fcode, $userId);
                ///ram noti       
                        $details=$redis->hget('H_Organisations_'.$fcode,$organizationId);
                        Log::info($details.'before '.$ownerShip);
                        if($type=='Sale' && $type1=='new' && $organizationId!=='default' && $organizationId!=='Default')
                        {
                          if($details==null)
                          {
                            Log::info('new organistion going to create');
                    $redis->sadd('S_Organisations_'. $fcode, $organizationId);  
                    log::info( '------login ---------- '.Session::get('cur'));
                    $redis->sadd('S_Organisations_Dealer_'.$ownerShip.'_'.$fcode,$organizationId);
                        
                          $orgDataArr = array (
                          'mobile' => '1234567890',
                          'description' => '',
                          'email' => '',
                          'address' => '',
                          'mobile' => '',
                          'startTime' => '',
                          'endTime'  => '',
                          'atc' => '',
                          'etc' =>'',
                          'mtc' =>'',
                        'parkingAlert'=>'',
                          'idleAlert'=>'',
                          'parkDuration'=>'',
                          'idleDuration'=>'',
                          'overspeedalert'=>'',
                          'sendGeoFenceSMS'=>'',
                          'radius'=>''
                          );
                               $orgDataJson = json_encode ( $orgDataArr );
                            $redis->hset('H_Organisations_'.$fcode,$organizationId,$orgDataJson );
                            $redis->hset('H_Org_Company_Map',$organizationId,$fcode);
                            log::info('before Audit Entry for Org-Create');
                          try{
                                $mysqlDetails =array();
                                $status =array();
                                $status=array(
                                  'fcode' => $fcode,
                                  'organizationName' => $organizationId,
                                  'userName'=>$username,
                                  'status' => Config::get('constant.created'),
                                  'userIpAddress'=>Session::get('userIP'),
                                );
                                $mysqlDetails   = array_merge($status,$orgDataArr);
                                $modelname = new AuditOrg();   
                                $table = $modelname->getTable();
                                $db=$fcode;
                                AuditTables::ChangeDB($db);
                                $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
                              if(count($tableExist)>0){
                                AuditOrg::create($mysqlDetails);
                              }
                              else{
                                AuditTables::CreateAuditOrg();
                                AuditOrg::create($mysqlDetails);
                              }
                              AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                          }catch (Exception $e) {
                                               AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                                               log::info('Error inside AuditOrg Create'.$e->getMessage());
                                               log::info($mysqlDetails);
                                            }
                                log::info('After Audit Entry for Org-Create');
                            
                          }
                        }
                      if($type=='Sale')
                      {
                        log::info( '------sale-1--------- '.$ownerShip);
                  


                        $groupname1      = Input::get('groupname');
                        $groupname       = strtoupper($groupname1);
                      log::info($userId.'-------------- groupname- out-------------'.$groupId);
                      if($type1=='existing' && $groupname!==null && $groupname!=='')
                      {
                        
                        $groupId1=explode(":",$groupname)[0];
                        $groupId=strtoupper($groupId1);
                        log::info('-------------- groupname--------------'.$groupId);
                      }
                      $redis->sadd($groupId . ':' . $fcode1,$vehicleId);
                      if($type1=='existing' && $groupname!==null && $groupname!==''){
                          log::info('before audit entry for group create');
                          try{
                          $vehicleIDList=$redis->smembers($groupId. ':' .$fcode1);
                          // log::info($vehicleID);
                          $mysqlDetails=array(
                            'userName'=>$username,
                            'status' => Config::get('constant.new_vehicle_added'),
                            'fcode' => $fcode,
                            'groupName' => $groupname,
                            'vehicleID'=>implode(',', $vehicleIDList),
                            'userIpAddress'=>Session::get('userIP'),
                            // 'vehicleName'=>$vehicleName
                          );
                          //$table=new AuditGroup->getTable();
                          $modelname = new AuditGroup();   
                          $table = $modelname->getTable();
                          //return $table;
                          //return $mysqlDetails;
                          $db=$fcode;
                          AuditTables::ChangeDB($db);
                          $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
                          //return $tableExist;
                          if(count($tableExist)>0){
                               AuditGroup::create($mysqlDetails);
                          }else{
                               AuditTables::CreateAuditGroup();
                               AuditGroup::create($mysqlDetails);
                          }
                          //AuditGroup::create($details);
                           AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                           }catch (Exception $e) {
                            AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                           log::info('Error inside AuditVehicle New Vehicle Added'.$e->getMessage());
                           log::info($mysqlDetails);
                        }
                          log::info('after audit entry for group create');
                      }


                        
                      }
                            
                $redis->srem('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceId);
                $redis->hdel('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceId);
              
          $temp++;
          }
        
log::info('-------------- count($dbarray)--------------'.count($dbarray));
if(count($dbarray)!==0)
{
  $query = DB::table('Vehicle_details')->select('vehicle_id')->where('vehicle_id',$vehicleId)->get();
  if($query==null)  {
    DB::table('Vehicle_details')->insert(
              $dbarray
          );
  }
    else
    {
      log::info('aldready exist in DataBase');    
    } 
}

          if($type=='Sale' )
          {
            log::info( '------sale-2--------- '.$ownerShip);
            
              $redis->sadd('S_Groups_' . $fcode, $groupId . ':' . $fcode1);
              log::info( '------login 1---------- '.Session::get('cur'));
              $redis->sadd('S_Groups_Dealer_'.$ownerShip.'_'.$fcode,$groupId . ':' . $fcode1);
              $redis->sadd('S_Users_Dealer_'.$ownerShip.'_'.$fcode,$userId);
              
            $redis->sadd ( $userId, $groupId . ':' . $fcode1 );
            $redis->sadd ( 'S_Users_' . $fcode, $userId );
              $OWN=$username;
            
            if($type1=='new')
            {
              log::info( '------sale--3-------- '.$ownerShip);
              $password=Input::get ( 'password' );
              if($password==null)
              {
                $password='awesome';
              }
              $redis->hmset ( 'H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo,$userId.':email',$email ,$userId.':password',$password,$userId.':OWN',$OWN);
              
              $user = new User;
              
              $user->name = $userId;
              $user->username=$userId;
              $user->email=$email;
              $user->mobileNo=$mobileNo;
              $user->password=Hash::make($password);
              $user->save();

              $groupNameNew=$groupId. ':' .$fcode1;
              // log::info($groupId1. ':' .$fcode1);
              $vehicleID=$redis->smembers($groupId. ':' .$fcode1);
              // log::info($vehicleID);

            log::info('before audit entry for group create');
            try{
            $mysqlDetails=array(
                'userName'=>$username,
                'status' => Config::get('constant.created'),
                'fcode' => $fcode,
                'groupName' => $email,
                'vehicleID'=>implode(",",$vehicleID),
                'vehicleName'=>$shortName,
                'userIpAddress'=>Session::get('userIP'),
            );
            //$table=new AuditGroup->getTable();
            $modelname = new AuditGroup();   
            $table = $modelname->getTable();
            //return $table;
            //return $mysqlDetails;
            $db=$fcode;
            AuditTables::ChangeDB($db);
            $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
            //return $tableExist;
            if(count($tableExist)>0){
                AuditGroup::create($mysqlDetails);
            }
            else{
                AuditTables::CreateAuditGroup();
                AuditGroup::create($mysqlDetails);
            }
            //AuditGroup::create($details);
            AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
            }catch (Exception $e) {
                           AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                           log::info('Error inside AuditGroup Create'.$e->getMessage());
                           log::info($mysqlDetails);
                        }
            log::info('after audit entry for group create');
      log::info("before audit user create entry");
      try{
      $details = array (
      'fcode' => $fcode,
      'userId' => $userId,
      'userName'=>$username,
      'status' => Config::get('constant.created'),
      'email'=>$email,
      'mobileNo'=>$mobileNo,
      'password'=>Hash::make($password),
      'zoho'=> '-',
      'companyName' => '-',
      'cc_email' => '-',
      'groups' =>$groupId. ':' .$fcode1,
      'vehicles' =>implode(",",$vehicleID),
      'userIpAddress'=>Session::get('userIP'),
      );
        $modelname = new AuditUser();   
        $table = $modelname->getTable();
            $db=$fcode;
        AuditTables::ChangeDB($db);
      $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
      if(count($tableExist)>0){
        AuditUser::create($details);
      }
      else{
        AuditTables::CreateAuditUser();
        AuditUser::create($details);
      }
      AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
      }catch (Exception $e) {
                           AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                           log::info('Error inside AuditUser Create'.$e->getMessage());
                           log::info($details);
                        }
      log::info("before audit user create entry");



              ///mobile number dont need user
              //  foreach($mobArr as $mob){
                 
              //   if($mob!=='')
              // {
                
              
                 
              // log::info( '------mobile number---------- '.$mob);
       //                             log::info( '------login 1---------- '.Session::get('cur'));
       //                             $redis->sadd('S_Users_Dealer_'.$ownerShip.'_'.$fcode,$mob);
                           
             //  log::info(' mobile number saved successfully');
             //  $redis->sadd ( $mob, $groupId . ':' . $fcode );
       //                    $redis->sadd ( 'S_Users_' . $fcode, $mob ); 
              
             //  $password=Input::get ( 'password' );
              // if($password==null)
              // {
              //    $password='awesome';
              // }

              // $redis->hmset ( 'H_UserId_Cust_Map', $mob . ':fcode', $fcode, $mob . ':mobileNo', $mobileNo,$mob.' :email',$email,$mob.':password',$password,$mob.':OWN',$OWN);

              // $user = new User;

              // $user->name = $mob;
              // $user->username=$mob;
              // $user->email=$email;
              // $user->mobileNo=$mobileNo;
              // $user->password=Hash::make($password);
              // $user->save();
              // }
       //                   }
              
            }
                            
              

               

          }
        
        
      }
    
              $date=date('F d Y', strtotime("+0 month"));
              $emailFcode=$redis->hget('H_DealerDetails_'.$fcode,$username);
              $emailFile=json_decode($emailFcode, true);
              $email1=$emailFile['email'];
              $emailFcode1=$redis->hget('H_Franchise', $fcode);
              $emailFile1=json_decode($emailFcode1, true);
              $email2=$emailFile1['email2'];
              if(Session::get('cur')=='dealer'){
              $emails=array($email1,$email2);
              }
               else if(Session::get('cur')=='admin'){
              $emails=array($email2);
              }
              $count=sizeof($devicearray);
              $i=0;
              $ownerShip=$username;
              log::info('--------------email outsite------------------>');
              $response=Mail::send('emails.dealeronboard', array('dealerName'=>$username,'Own'=>$ownerShip,'i'=>$i,'nod'=>$count,'date'=>$date,'user'=>$userId,'Devices'=>$devicearray), function($message) use ($emails,$ownerShip)
              {
                $message->to($emails);
                $message->subject('Onboarded Vehicles - '.$ownerShip);
                log::info('-----------email send------------------>');
              });  
      Session::flash('message','Device Successfully added to User - '.$userId);
      return Redirect::to('Business');
      
  }
  
  
      protected function schedule(Schedule $schedule){
            $schedule->call(function () {
            //DB::table('recent_users')->delete();
          })->everyMinute();
        }
   public function addvehicle($id) {
    if (! Auth::check ()) {
      return Redirect::to ( 'login' );
    }
    
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $numberofdevice = $id;
    log::info('$numberofdevice'.$numberofdevice);
    log::info( '-------- store in  ::----------');
        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
      $franchiseDetails=json_decode($franDetails_json,true);
      $availableLincence=$franchiseDetails['availableLincence'];

      log::info( '-------- av license in  ::----------'.$availableLincence);
      if($numberofdevice>$availableLincence)
      {
        return view ( 'vdm.business.deviceAddCopy' )->withErrors ( "Your license count is less" )->with ( 'orgList', null )->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', null )->with ( 'dealerId', null )->with ( 'userList', null )->with('orgList',null);
      }
    $dealerId = $redis->smembers('S_Dealers_'. $fcode);  
        $orgArr = array();
            // $orgArr = array_add($orgArr, 'OWN','OWN');
    if($dealerId!=null)
    {
      foreach($dealerId as $org) {
            $orgArr = array_add($orgArr, $org,$org);
      
        }
    $dealerId = $orgArr;
    }
    else{
      $dealerId=null;
      //$orgArr = array_add($orgArr, 'OWN','OWN');
      $dealerId = $orgArr;
    }
    $userList=array();
    $userList=BusinessController::getUser();
    $orgList=array();
    $orgList=BusinessController::getOrg();
    $protocol = BusinessController::getProtocal();
    $Payment_Mode1 =array();
    $Payment_Mode = DB::select('select type from Payment_Mode');
    //log::info( '-------- av  in  ::----------'.count($Payment_Mode));
    foreach($Payment_Mode as  $org1) {
        $Payment_Mode1 = array_add($Payment_Mode1, $org1->type,$org1->type);
        }
    $Licence1 =array();
    $Licence = DB::select('select type from Licence');
    foreach($Licence as  $org) {
        $Licence1 = array_add($Licence1, $org->type,$org->type);
        }
    $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
    $servername = $franchiesJson;
    //$servername = "209.97.163.4";
      if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
        return 'Ipaddress Failed !!!';
      }
                $usernamedb = "root";
                $password = "#vamo123";
                $dbname = $fcode;
                log::info('franci..----'.$fcode);
                log::info('ip----'.$servername);
                $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
        if( !$conn ) {
                 die('Could not connect: ' . mysqli_connect_error());
                    return 'Please Update One more time Connection failed';
                } else 
                { 
                  log::info(' created connection ');
                  $max="select id from BatchMove order by id desc limit 1";
                         $results = mysqli_query($conn,$max);
          while ($row = mysqli_fetch_array($results)) {
                     log::info('clearr');
                     log::info($row);
                     $maxcount = $row[0];
          }
                    $conn->close();
                }
                $rowcount=$maxcount+1;
                $deviceId=array();
                $vehicleId=array();
                $n=2;
    $fid=strtoupper($fcode);  
              for($i=1;$i<=$numberofdevice;$i++)
               { 
                  $j=$rowcount+300000;
                  log::info($j);
                  $devices="GPSNEW_".$j."_".$fid;
                  $vehicles=$devices."1";
                  $dev=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$devices);
                  $veh=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$vehicles);
                  
                  for($m=1;$m<=$n;$m++)
                  { 
                    if ($dev==null && $veh==null) 
                    {  
                    $deviceCheck=$devices;
                    $vehicles=$deviceCheck."1";
                    $deviceId = array_add($deviceId, $i,$deviceCheck);
                    $vehicleId = array_add($vehicleId, $i,$vehicles);
                    $rowcount=$rowcount+1;
                    break;
                    }
                    else
                    {
                     $j=$rowcount+300000+1;
                     $devices="GPSNEW_".$j."_".$fid;
                     $vehicles=$devices."1";
                     $dev=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$devices);
                     $veh=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$vehicles);
                     $n++;
                     $rowcount=$rowcount+1;
                    }
                 }
                                  
             }
    
     return view ( 'vdm.business.createCopy' )->with ( 'orgList', $orgList )->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', $numberofdevice )->with ( 'dealerId', $dealerId )->with ( 'userList', $userList )->with('orgList',$orgList)->with('Licence',$Licence1)->with('Payment_Mode',$Payment_Mode1)->with('protocol', $protocol)->with('devices', $deviceId)->with('vehicles', $vehicleId);
    
    
    
  }

  
  
  
  
  }
