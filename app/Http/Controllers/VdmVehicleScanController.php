<?php
namespace App\Http\Controllers;

use Request;
use App;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

class VdmVehicleScanController extends Controller {
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	
	 */	
public function index()
    {
        log::info(' reach the road speed function ');
         if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;

    log::info( 'User name  ::' . $username);
    Session::forget('page');
    Session::put('vCol',1);
    $redis = Redis::connection ();
    log::info( 'User 1  ::' );
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info('fcode=' . $fcode);
    $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
    $franchiseDetails=json_decode($franDetails_json,true);
    $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
        $orgLis = [];
            return view('vdm.vehicles.vehicleScan')->with('vehicleList', $orgLis);
    }   
public function store() {
    if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;

    log::info( 'User name  ::' . $username);
    Session::forget('page');
    Session::put('vCol',1);
    $redis = Redis::connection ();
    log::info( 'User 1  ::' );
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info('fcode=' . $fcode);
	$franDetails_json = $redis->hget( 'H_Franchise', $fcode);
    $franchiseDetails=json_decode($franDetails_json,true);
    $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
    if(Session::get('cur')=='dealer')
    {
        $vehicleListId='S_Vehicles_Dealer_'.$username.'_'.$fcode;
        $vehicleNameMob='H_VehicleName_Mobile_Dealer_'.$username.'_Org_'.$fcode;
    }
    else if(Session::get('cur')=='admin')
    {
        $vehicleListId='S_Vehicles_'.$fcode;
        $vehicleNameMob='H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode;
    }
    else{
        $vehicleListId = 'S_Vehicles_' . $fcode;
        $vehicleNameMob='H_VehicleName_Mobile_Org_'.$fcode;
    }
        $text_word1 = Input::get('text_word');
        $text_trim= str_replace(' ', '', $text_word1);
        $text_word = strtoupper($text_trim);
		log::info('Vehicle scan '.$text_word);
        $vehicleList = $redis->smembers ( $vehicleListId); //log::info($vehicleList);
    	$cou = $redis->scard($vehicleListId);
		if(Session::get('cur')=='admin')
        {
			//log::info('inside admin search');
			//log::info($text_word);
			$check=$redis->hget('H_RefData_'.$fcode, $text_word);
	
			if($check!== null)
			{ $orgLl=array();
			$orgLl=array_add($orgLl,$text_word,$text_word);
                $orgL = $orgLl;
		    }
        else{
			$orgLi = $redis->HScan( $vehicleNameMob, 0,  'count', $cou, 'match', '*'.$text_word.'*');
            $orgL = $orgLi[1];
       }
      }
		else
       {
		$orgLi = $redis->HScan( $vehicleNameMob, 0,  'count', $cou, 'match', '*'.$text_word.'*');
        $orgL = $orgLi[1];
       }
        
    $deviceList = null;
    $deviceId = null;
    $shortName =null;
    $shortNameList = null;
    $portNo =null;
    $portNoList = null;
    $mobileNo =null;
    $mobileNoList = null;
    $orgIdList = null;
    $deviceModelList = null;
    $expiredList = null;
    $statusList = null;
	$onboardDateList = null;
	$owner=null;
	$licenceissuedDateList = null;
	$commList=null;
	$licenceIdList=null;
    $LicexpiredPeriodList=null;
	$timezoneList=null;
	$apnList=null;
    foreach ( $orgL as $key => $vehicle  ) {

        //Log::info($key.'$vehicle ' .$vehicle);
        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicle );

        if(isset($vehicleRefData)) {
            //Log::info('$vehicle ' .$vehicleRefData);
        }else {
            continue;
        }
        $vehicleRefData=json_decode($vehicleRefData,true);
        $deviceId =isset($vehicleRefData['deviceId'])?$vehicleRefData['deviceId']:'';
        //$deviceId = $vehicleRefData['deviceId'];
        if((Session::get('cur')=='dealer' &&  $redis->sismember('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $deviceId)==0) || Session::get('cur')=='admin')
        {
			
			 if($prepaid=='yes'){
                $LicenceId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$vehicle);
                $licenceIdList=array_add($licenceIdList,$vehicle,$LicenceId);
                $LicenceRef=$redis->hget('H_LicenceExpiry_'.$fcode,$LicenceId);
                $LicenceRefData=json_decode($LicenceRef,true);
                $licenceissued=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:'';
                $licenceissuedDateList = array_add($licenceissuedDateList,$vehicle,$licenceissued);
                $LicenceexpiredPeriod=isset($LicenceRefData['LicenceExpiryDate'])?$LicenceRefData['LicenceExpiryDate']:'';
                $LicexpiredPeriodList = array_add($LicexpiredPeriodList,$vehicle,$LicenceexpiredPeriod);
            }else{
                $licenceissued=isset($vehicleRefData['licenceissuedDate'])?$vehicleRefData['licenceissuedDate']:'';
                $licenceissuedDateList = array_add($licenceissuedDateList,$vehicle,$licenceissued);
            }
        $expiredPeriod=isset($vehicleRefData['vehicleExpiry'])?$vehicleRefData['vehicleExpiry']:'-';
        $nullval=strlen($expiredPeriod); 
        if($nullval==0 || $expiredPeriod=="null"){
            $expiredPeriod='-';
        }
        $expiredList = array_add($expiredList,$vehicle,$expiredPeriod);
            //Log::info(Session::get('cur').'in side ' .$redis->sismember('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $deviceId));
        $deviceList = array_add ( $deviceList, $vehicle,$deviceId );
        $shortName = isset($vehicleRefData['shortName'])?$vehicleRefData['shortName']:'nill';
        $shortNameList = array_add($shortNameList,$vehicle,$shortName);
        $portNo=isset($vehicleRefData['portNo'])?$vehicleRefData['portNo']:9964;
        $portNoList = array_add($portNoList,$vehicle,$portNo);
        $mobileNo=isset($vehicleRefData['gpsSimNo'])?$vehicleRefData['gpsSimNo']:99999;
        $mobileNoList = array_add($mobileNoList,$vehicle,$mobileNo);
        $orgId=isset($vehicleRefData['orgId'])?$vehicleRefData['orgId']:'Default';
        $orgIdList = array_add($orgIdList,$vehicle,$orgId);
        $deviceModel=isset($vehicleRefData['deviceModel'])?$vehicleRefData['deviceModel']:'nill';
        $deviceModelList = array_add($deviceModelList,$vehicle,$deviceModel);
		
		//timezone and apn
        $timezoneData = $redis->hget ( 'H_DeviceDetail_' . $fcode, $vehicle );
        $timezoneRefData=json_decode($timezoneData,true);
        $timezone=isset($timezoneRefData['timezone'])?$timezoneRefData['timezone']:'-';
        $timezoneList = array_add($timezoneList,$vehicle,$timezone);
		$apn=isset($timezoneRefData['apn'])?$timezoneRefData['apn']:'-';
        $apnList = array_add($apnList,$vehicle,$apn);
		
        $statusVehicle = $redis->hget ( 'H_ProData_' . $fcode, $vehicle );
        $statusSeperate = explode(',', $statusVehicle);
		$statusSeperate1=isset($statusSeperate[7])?$statusSeperate[7]:'N';
        $statusList = array_add($statusList, $vehicle, $statusSeperate1);
		$lastComm=isset($statusSeperate[36])?$statusSeperate[36]:'';
        if($lastComm=='' || $lastComm==null) {
			$setted='';
            }
        else{
            $sec=$lastComm/1000;
            $datt=date("Y-m-d",$sec);
            $time1=date("H:i:s",$sec);
            //$endTime = strtotime("+330 minutes", strtotime($time1));
            //$time2=date('G:i:s', $endTime);
            $setted=$datt.' '.$time1;
        }
        $commList = array_add($commList, $vehicle, $setted);
		$date=isset($vehicleRefData['date'])?$vehicleRefData['date']:'';
        
                if($date=='' || $date==' ')
				{
					$date1='';
				}
				else
				{
					$date1=date("d-m-Y", strtotime($date));
				}
				$onDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:$date1;
				$nullval1=strlen($onDate);
			
				if($nullval1==0 || $onDate=="null" || $onDate==" ")
				{
					$onboardDate=$date1;
				}
				else
				{
					$onboardDate=$onDate;
				}
		//$onboardDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:$date1;
        $onboardDateList = array_add($onboardDateList,$vehicle,$onboardDate);
		$owntype = isset($vehicleRefData['OWN'])?$vehicleRefData['OWN']:'';
        $owner = array_add($owner,$vehicle,$owntype);
        }
        else
        {
             Log::info('$inside remove ' .$vehicle);
            unset($orgL[$key]);
        }
    }
    $demo='ahan';
    $user=null;
    $user1= new VdmDealersController;
    $user=$user1->checkuser();
    $dealerId = $redis->smembers('S_Dealers_'. $fcode);
    $orgArr = array();
    foreach($dealerId as $org) {
        $orgArr = array_add($orgArr, $org,$org);
    }
    $dealerId = $orgArr;
	sort($orgL,SORT_NATURAL | SORT_FLAG_CASE);
    return view ( 'vdm.vehicles.vehicleScan', array (
        'vehicleList' => $orgL
        ) )->with ( 'deviceList', $deviceList )->with ( 'owntype', $owner )->with('shortNameList',$shortNameList)->with('portNoList',$portNoList)->with('mobileNoList',$mobileNoList)->with('demo',$demo)->with ( 'user', $user )->with ( 'orgIdList', $orgIdList )->with ( 'deviceModelList', $deviceModelList )->with ( 'expiredList', $expiredList )->with ( 'tmp', 0 )->with ('statusList', $statusList)->with('dealerId',$dealerId)->with('onboardDateList', $onboardDateList)->with ( 'licenceissuedDate', $licenceissuedDateList )->with ( 'commList', $commList )->with('licenceIdList',$licenceIdList)->with('LicexpiredPeriodList',$LicexpiredPeriodList)->with('timezoneList',$timezoneList)->with('apnList',$apnList); 
}
public function sendExcel()
{
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );                    
    $devicesList=$redis->smembers( 'S_Device_' . $fcode);
    $file = Excel::create('Vehicles List', function($excel) 
    {
        $excel->sheet('Sheetname', function($sheet)  
        {   
            $redis = Redis::connection ();
            $username = Auth::user ()->username;
            log::info('inside the vehiclescan sendExcel---');
            $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
            $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
            $franchiseDetails=json_decode($franDetails_json,true);
            $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
            if(Session::get('cur')=='dealer')
            {
             $vehicleListId='S_Vehicles_Dealer_'.$username.'_'.$fcode;
    		 }
            else if(Session::get('cur')=='admin')
            {
            $vehicleListId='S_Vehicles_Admin_'.$fcode;
            }
            else{
            $vehicleListId = 'S_Vehicles_' . $fcode;
            }
                               
            $vehicleList=$redis->smembers($vehicleListId);
            $temp=0;
            //$vehicleMap=array();
            $statusList = null;
            $deviceList = null;
            $deviceId = null;
            $shortName =null;
            $shortNameList = null;
            $portNo =null;
            $portNoList = null;
            $mobileNo =null;
            $mobileNoList = null;
            $orgIdList = null;
            $deviceModelList = null;
            $expiredList = null;
            for($i =0;$i<count($vehicleList);$i++)
            {
                $refData    = $redis->hget ( 'H_RefData_' . $fcode, $vehicleList[$i] );
                $refData    = json_decode($refData,true);
                $deviceId =isset($refData['deviceId'])?$refData['deviceId']:''; 
		        $shortName = isset($refData['shortName'])?$refData['shortName']:'nill';
                $orgId=isset($refData['orgId'])?$refData['orgId']:'Default';
                $gpsNo=isset($refData['gpsSimNo'])?$refData['gpsSimNo']:99999;
				//device versoin
				$versionofVehicle=$redis->hget ( 'H_Version_' . $fcode,  $vehicleList[$i] );
                $version=isset($versionofVehicle)?$versionofVehicle:'-';
				
                if($prepaid=='yes'){
                        $type=isset($refData['Licence'])?$refData['Licence']:'';
                        $LicenceId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$vehicleList[$i]);
                }


                $statusVehicle = $redis->hget ( 'H_ProData_' . $fcode, $vehicleList[$i] );
                $statusSeperate = explode(',', $statusVehicle);
                $statusList1 = isset($statusSeperate[7])?$statusSeperate[7]:'';
                if($statusList1=='P')
                {
                    $statusList='Parking';
                }
                else if($statusList1=='M')
                {
                   	$statusList='Moving';
                }
                else if($statusList1=='S')
                {
                   	$statusList='Idle';
                }
                else if($statusList1=='N')
                {
                  	$statusList='New Device';
                }
                else 
                {
                   	$statusList='No data';
                }
                $deviceModel=isset($refData['deviceModel'])?$refData['deviceModel']:'nill';
                $vehLi=count($vehicleList);           
                $j=$i+2;
                if($prepaid=='yes'){
                    $sheet->setWidth(array(
                                     'A'     =>  25,
                                     'C'    =>   30,
                                     'D'    =>   35,
                                     'E'    =>   15,
                                     'F'    =>   15,
                                     'G'    =>   15,
                                     'B'     =>  30,
                                     'H'    =>   30,
                                     'I'    =>   30
                             ));
                }else{
                    $sheet->setWidth(array(
                                     'A'     =>  25,
                                     'C'    =>   30,
                                     'D'    =>   35,
                                     'E'    =>   15,
                                     'F'    =>   15,
                                     'G'    =>   15,
                                     'B'     =>  30
                             ));

                }
                          

                $sheet->row(1,function ($row) 
				{
             	 $row->setFontWeight('bold');
                 $row->setAlignment('center');

                });
                  
                $sheet->row(2,function ($row) {
                $row->setAlignment('left');
                });
                    //$sheet->setAutoSize(true) ;
                if($prepaid=='yes'){
                    $sheet->row(1, array('Licence Id','Asset Id','Vehicle Name','Organization Name','Device Id','GPS Sim No','Status','Device Model','Device Version','Type'));
                    $sheet->row($j, array($LicenceId,$vehicleList[$i],$shortName,$orgId,$deviceId,$gpsNo,$statusList,$deviceModel,$version,$type));

                }else{
                    $sheet->row(1, array('Asset Id','Vehicle Name','Organization Name','Device Id','GPS Sim No','Status','Device Model','Device Version'));
                $sheet->row($j, array($vehicleList[$i],$shortName,$orgId,$deviceId,$gpsNo,$statusList,$deviceModel,$version));
                    }
                }
                }); 

            });//->download('xls');
			  if(Session::get('cur')=='admin') {
              $emailFcode=$redis->hget('H_Franchise', $fcode);
              $emailFile=json_decode($emailFcode,true);
              $email1=$emailFile['email2'];
              }
              else if(Session::get('cur')=='dealer')
               {
               $emailFcode=$redis->hget('H_DealerDetails_'.$fcode,$username);
               $emailFile=json_decode($emailFcode, true);
               $email1=$emailFile['email'];
               }
              $data[]='get all de';
              log::info('--------------email outsite------------------>');
              $mymail=Mail::send( 'vdm.business.empty',$data,function($message) use($file,$email1)
              {
                $message->to($email1);
                //$message->to('ramakrishnan.vamosys@gmail.com');
                $message->subject('Vehicles List');
                $message->attach($file->store("xls",false,true)['full']);
                log::info('-----------email send------------------>');
              });
            Session::flash('message', 'Vehicles List Sent to '.$email1.' Id Successfully');
    return Redirect::back();
}

public function scanNew($id) {
    if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    log::info('inside the vehiclescan store--------------------------------------');
    log::info( 'User name  ::' . $username);
    Session::forget('page');
    Session::put('vCol',1);
    $redis = Redis::connection ();
    log::info( 'User 1  ::' );
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
	$franDetails_json = $redis->hget( 'H_Franchise', $fcode);
    $franchiseDetails=json_decode($franDetails_json,true);
    $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
    Log::info('fcode=' . $fcode);
    if(Session::get('cur')=='dealer')
    {
        $vehicleListId='S_Vehicles_Dealer_'.$username.'_'.$fcode;
        $vehicleNameMob='H_VehicleName_Mobile_Dealer_'.$username.'_Org_'.$fcode;
    }
    else if(Session::get('cur')=='admin')
    {
        $vehicleListId='S_Vehicles_'.$fcode;
        $vehicleNameMob='H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode;
    }
    else{
        $vehicleListId = 'S_Vehicles_' . $fcode;
        $vehicleNameMob='H_VehicleName_Mobile_Org_'.$fcode;
    }
        $text_word1 = $id;
        $text_trim= str_replace(' ', '', $text_word1);
        $text_word = strtoupper($text_trim);
		log::info('Vehicle scan '.$text_word);
        $vehicleList = $redis->smembers ( $vehicleListId); //log::info($vehicleList);
    	//$cou = $redis->scard($vehicleListId);
        $cou=$redis->hlen($vehicleNameMob);
		if(Session::get('cur')=='admin')
        {
			//log::info('inside admin search');
			//log::info($text_word);
			$check=$redis->hget('H_RefData_'.$fcode, $text_word);
	
			if($check!== null)
			{ $orgLl=array();
			$orgLl=array_add($orgLl,$text_word,$text_word);
                $orgL = $orgLl;
		    }
        else{
			$orgLi = $redis->HScan( $vehicleNameMob, 0,  'count', $cou, 'match', '*'.$text_word.'*');
            $orgL = $orgLi[1];
       }
      }
		else
       {
		$orgLi = $redis->HScan( $vehicleNameMob, 0,  'count', $cou, 'match', '*'.$text_word.'*');
        $orgL = $orgLi[1];
       }
    $deviceList = null;
    $deviceId = null;
    $shortName =null;
    $shortNameList = null;
    $portNo =null;
    $portNoList = null;
    $mobileNo =null;
    $mobileNoList = null;
    $orgIdList = null;
    $deviceModelList = null;
    $expiredList = null;
    $statusList = null;
    //$onboardDate = null;
    $onboardDateList = null;
	$owner=null;
	$licenceissuedDateList = null;
	$commList=null;
	$licenceIdList=null;
    $LicexpiredPeriodList=null;
	$timezoneList=null;
	$apnList=null;
	
    foreach ( $orgL as $key => $vehicle  ) {

        //Log::info($key.'$vehicle ' .$vehicle);
        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicle );

        if(isset($vehicleRefData)) {
            //Log::info('$vehicle ' .$vehicleRefData);
        }else {
            continue;
        }
        $vehicleRefData=json_decode($vehicleRefData,true);
        $deviceId =isset($vehicleRefData['deviceId'])?$vehicleRefData['deviceId']:'';
        //$deviceId = $vehicleRefData['deviceId'];
        if((Session::get('cur')=='dealer' &&  $redis->sismember('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $deviceId)==0) || Session::get('cur')=='admin')
        {
			if($prepaid=='yes'){
                $LicenceId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$vehicle);
                $licenceIdList=array_add($licenceIdList,$vehicle,$LicenceId);
                $LicenceRef=$redis->hget('H_LicenceExpiry_'.$fcode,$LicenceId);
                $LicenceRefData=json_decode($LicenceRef,true);
                $licenceissued=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:'';
                $licenceissuedDateList = array_add($licenceissuedDateList,$vehicle,$licenceissued);
                $LicenceexpiredPeriod=isset($LicenceRefData['LicenceExpiryDate'])?$LicenceRefData['LicenceExpiryDate']:'';
                $LicexpiredPeriodList = array_add($LicexpiredPeriodList,$vehicle,$LicenceexpiredPeriod);
            }else{
                $licenceissued=isset($vehicleRefData['licenceissuedDate'])?$vehicleRefData['licenceissuedDate']:'';
                $licenceissuedDateList = array_add($licenceissuedDateList,$vehicle,$licenceissued);
            }
           // Log::info(Session::get('cur').'in side ' .$redis->sismember('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $deviceId));
        $deviceList = array_add ( $deviceList, $vehicle,$deviceId );
        $shortName = isset($vehicleRefData['shortName'])?$vehicleRefData['shortName']:'nill';
        $shortNameList = array_add($shortNameList,$vehicle,$shortName);
        $portNo=isset($vehicleRefData['portNo'])?$vehicleRefData['portNo']:9964;
        $portNoList = array_add($portNoList,$vehicle,$portNo);
        $mobileNo=isset($vehicleRefData['gpsSimNo'])?$vehicleRefData['gpsSimNo']:99999;
        $mobileNoList = array_add($mobileNoList,$vehicle,$mobileNo);
        $orgId=isset($vehicleRefData['orgId'])?$vehicleRefData['orgId']:'Default';
        $orgIdList = array_add($orgIdList,$vehicle,$orgId);
        $deviceModel=isset($vehicleRefData['deviceModel'])?$vehicleRefData['deviceModel']:'nill';
        $deviceModelList = array_add($deviceModelList,$vehicle,$deviceModel);
        $expiredPeriod=isset($vehicleRefData['vehicleExpiry'])?$vehicleRefData['vehicleExpiry']:'-';
        $nullval=strlen($expiredPeriod);
        if($nullval==0 || $expiredPeriod=="null")
        {
            $expiredPeriod='-';
        }

        $expiredList = array_add($expiredList,$vehicle,$expiredPeriod);
        $date=isset($vehicleRefData['date'])?$vehicleRefData['date']:'';
        
                if($date=='' || $date==' ')
				{
					$date1='';
				}
				else
				{
					$date1=date("d-m-Y", strtotime($date));
				}
				$onDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:$date1;
				$nullval1=strlen($onDate);
			
				if($nullval1==0 || $onDate=="null" || $onDate==" ")
				{
					$onboardDate=$date1;
				}
				else
				{
					$onboardDate=$onDate;
				}
        //$onboardDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:$date1;
        $onboardDateList = array_add($onboardDateList,$vehicle,$onboardDate);
        $statusVehicle = $redis->hget ( 'H_ProData_' . $fcode, $vehicle );
		$statusSeperate = explode(',', $statusVehicle);
        $statusSeperate1=isset($statusSeperate[7])?$statusSeperate[7]:'N';
        $statusList = array_add($statusList, $vehicle, $statusSeperate1);
		$lastComm=isset($statusSeperate[36])?$statusSeperate[36]:'';
        if($lastComm=='' || $lastComm==null) {
			$setted='';
            }
        else{
            $sec=$lastComm/1000;
            $datt=date("Y-m-d",$sec);
            $time1=date("H:i:s",$sec);
            //$endTime = strtotime("+330 minutes", strtotime($time1));
            //$time2=date('G:i:s', $endTime);
            $setted=$datt.' '.$time1;
        }
        $commList = array_add($commList, $vehicle, $setted);
        $owntype = isset($vehicleRefData['OWN'])?$vehicleRefData['OWN']:'';
        $owner = array_add($owner,$vehicle,$owntype);
		
		//timezone and apn
        $timezoneData = $redis->hget ( 'H_DeviceDetail_' . $fcode, $vehicle );
        $timezoneRefData=json_decode($timezoneData,true);
        $timezone=isset($timezoneRefData['timezone'])?$timezoneRefData['timezone']:'-';
        $timezoneList = array_add($timezoneList,$vehicle,$timezone);
		$apn=isset($timezoneRefData['apn'])?$timezoneRefData['apn']:'-';
        $apnList = array_add($apnList,$vehicle,$apn);
		
        }
        else
        {
             Log::info('$inside remove ' .$vehicle);
            unset($orgL[$key]);
        }
    }
    $demo='ahan';
    $user=null;
    $user1= new VdmDealersController;
    $user=$user1->checkuser();
    $dealerId = $redis->smembers('S_Dealers_'. $fcode);
    $orgArr = array();
    foreach($dealerId as $org) {
        $orgArr = array_add($orgArr, $org,$org);
    }
    $dealerId = $orgArr;
	sort($orgL,SORT_NATURAL | SORT_FLAG_CASE);
    return view ( 'vdm.vehicles.vehicleScan', array (
        'vehicleList' => $orgL
        ) )->with ( 'deviceList', $deviceList )->with ( 'owntype', $owner )->with('shortNameList',$shortNameList)->with('portNoList',$portNoList)->with('mobileNoList',$mobileNoList)->with('demo',$demo)->with ( 'user', $user )->with ( 'orgIdList', $orgIdList )->with ( 'deviceModelList', $deviceModelList )->with ( 'expiredList', $expiredList )->with ( 'tmp', 0 )->with ('statusList', $statusList)->with('dealerId',$dealerId)->with ( 'onboardDateList', $onboardDateList )->with ( 'licenceissuedDate', $licenceissuedDateList )->with ( 'commList', $commList )->with('licenceIdList',$licenceIdList)->with('LicexpiredPeriodList',$LicexpiredPeriodList)->with('timezoneList',$timezoneList)->with('apnList',$apnList); 
}

/*
* Show the form for creating a new resource.
* @return Response
*/	
}