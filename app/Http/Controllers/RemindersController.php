<?php
namespace App\Http\Controllers;

use Request;
use App, DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use Exception;

class RemindersController extends Controller {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
		return view('password.remind');
	}
    public function popupModel()
	{
		return view('vdm.login.popupModel');
	}


	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
		public function postRemind()
	{
		log::info(" postRemind ");
		 $data = Input::all();
		//var_dump($data);
		$redis = Redis::connection ();
		$username=Input::get ('uname');
		log::info(" user ");
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );	
		if($fcode == null){
			$username=strtoupper(Input::get ('uname'));
			$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		}
		$web='gpsvts.net';
		$ipaddress='gpsvts.net';
		if(($fcode!=null && $username!=null) || $username=='vamos')
		{
			try
	   {
			log::info("valid user ".$username);
			$emailTemp=$redis->hget ( 'H_UserId_Cust_Map', $username . ':email');
				log::info($emailTemp);	
				Session::put('email',$emailTemp);
				//$ipaddress = $redis->get('ipaddress');
				//$userOwn=$redis->hget ( 'H_UserId_Cust_Map', $username . ':OWN');
				//$ipaddress='188.166.244.126';
				$ad=$redis->hget('H_Franchise',$fcode);
        $refData11    = json_decode($ad, true);
        $userID=isset($refData11['userId'])?$refData11['userId']:'';
				$adminuser=$redis->sismember('S_Users_Admin_'.$fcode,$username);
        $deal=$redis->sismember('S_Dealers_'.$fcode,$username);
                
        $dealerL=$redis->smembers('S_Dealers_'.$fcode);
      	foreach ($dealerL as $key => $value1) {
                $dealeruser=$redis->sismember('S_Users_Dealer_'.$value1.'_'.$fcode,$username);
                if($dealeruser==1) {
					          break;
				        }
        }
        if($userID==$username || $adminuser==1)  {
        
				$addetails=$redis->hget('H_Franchise',$fcode);
				$refData    = json_decode($addetails, true);
                    		$web=isset($refData['website'])?$refData['website']:'gpsvts.net';
                       $ipaddress=isset($refData['website'])?$refData['website']:'gpsvts.net';
                       
				}
        
				else if($deal==1) {
        	$addetails=$redis->hget('H_DealerDetails_'.$fcode,$username);
                                $refData    = json_decode($addetails, true);
                                log::info($refData['website']);
                                $web=isset($refData['website'])?$refData['website']:'gpsvts.net';
                                $ipaddress=isset($refData['website'])?$refData['website']:'gpsvts.net';
                                              
				}
        
       	else if($dealeruser==1) {
          $addetails=$redis->hget('H_DealerDetails_'.$fcode,$value1);
                                $refData    = json_decode($addetails, true);
                                $web=isset($refData['website'])?$refData['website']:'gpsvts.net';
                                $ipaddress=isset($refData['website'])?$refData['website']:'gpsvts.net';
				
				 }  
           
			  if($web=='')  {
				$web='gpsvts.net';
				}
       if($ipaddress=='')
        {
        $ipaddress='gpsvts.net';
        }
     		$temp=$username.$fcode.time().$ipaddress;				
				$hashurl=Hash::make($temp);
				$hashurl=str_replace("/","a",$hashurl);
				log::info($emailTemp."valid user ".$username.' token '.$temp." hash url ".$hashurl);
				$resetLink='http://' .$ipaddress. '/gps/public/password/reset/'.$hashurl;
				log::info($resetLink);
				$ipaddress = $redis->get('ipaddress');
				//$ipaddress='188.166.244.126';	
				/* $url = 'http://' .$ipaddress. ':9000/sentResetPasswordLink?strMsg=' .$resetLink.'&sender='.$web. '&mailId=' . Session::pull ( 'email' ) . '&subject=PASSWORD RESET!';	 
				log::info( ' url :' . $url);
				$url = str_replace(" ", '%20', $url);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_URL, $url);
				$response = curl_exec($ch);
				curl_close($ch);
				log::info('response '.$response); */
				
			try{
				$req = [
                    "toStr"=>session()->pull('email') ,
                    "subject" => "PASSWORD RESET!",
                    "body"=>$resetLink,
                    "bccstr"=>"",
                    "sender"=>$web,
					"senderType"=>"notification"
                ];
                $toJson = json_encode($req);
                $redis->lpush("L_Mail_Q",$toJson);
				
				$redis->set($hashurl, $username);
				$redis->expire($hashurl, 3600);
				
				$emailT=explode('@',$emailTemp);
				$mailId= substr($emailT[0], 0, -3).'***';
				$emailTemp=$mailId.'@'.$emailT[1];
        
			}
			catch(\Exception $e)
			   {
				log::info($e);
			   }
				
		return Redirect::to('login')->with('flash_notice','Please check '.$emailTemp.' mail for password details.');	
	   }
	   catch(\Exception $e)
	   {
		return Redirect::to('login')->with('flash_notice','Invalid mail Id.'); 
	   }
			
			
		}
		else
		{
			return Redirect::to('login')->with('flash_notice','Invalid user please check the Username.');
		}
		
	}
	
	public function request()
	{
	//	$credentials = array('email' => Input::get('email'), 'password' => Input::get('password'));
	
	//	return Password::remind($credentials);
	
		$redis = Redis::connection ();
		$userId = Input::only('userId');
		$email=null;
		
		$response = Password::remind($email, function($message)
		{
			$message->subject('Password Reminder');
		});
		
		switch ($response)
		{
			case Password::INVALID_USER:
				return Redirect::back()->with('error', Lang::get($response));
		
			case Password::REMINDER_SENT:
				return Redirect::to('login')->with('flash_notice','Please check your mail for password details.');
			//	return Redirect::to('/')->with('flash_notice', 'Please check your mail for password details');
		}
		
	}
	
	public function reset($token)
	{
		$redis = Redis::connection ();
		$username=$redis->get($token);
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$ad=$redis->sismember('S_Franchises',$username);
                $deal=$redis->sismember('S_Dealers_'.$fcode,$username);
		$web='';
    $dealeruser='';
        $ad=$redis->hget('H_Franchise',$fcode);
        $refData11    = json_decode($ad, true);
        $userID=isset($refData11['userId'])?$refData11['userId']:'';
				$adminuser=$redis->sismember('S_Users_Admin_'.$fcode,$username);
        $deal=$redis->sismember('S_Dealers_'.$fcode,$username);
        
        $dealerL=$redis->smembers('S_Dealers_'.$fcode);
      	foreach ($dealerL as $key => $value1) {
                $dealeruser=$redis->sismember('S_Users_Dealer_'.$value1.'_'.$fcode,$username);
                if($dealeruser==1) {
					          break;
				        }
        }
        
        if($userID==$username || $adminuser==1)  {
        
				$addetails=$redis->hget('H_Franchise',$fcode);
				$refData    = json_decode($addetails, true);
                    		$web=isset($refData['website'])?$refData['website']:'gpsvts.net';
                      
				}
        
				else if($deal==1) {
        $addetails=$redis->hget('H_DealerDetails_'.$fcode,$username);
                                $refData    = json_decode($addetails, true);
                                log::info($refData['website']);
                                $web=isset($refData['website'])?$refData['website']:'gpsvts.net';
                                            
				}
        
       	else if($dealeruser==1) {
          $addetails=$redis->hget('H_DealerDetails_'.$fcode,$value1);
                                $refData    = json_decode($addetails, true);
                                $web=isset($refData['website'])?$refData['website']:'gpsvts.net';
                               
				 }  
        else  {
        $web='gpsvts.net';
        }
		if($username!=null)
		{
			return view('password.reset')->with('token', $token)->with('userId', $username)->with('web',$web);
		}
		else{
			return view('password.expire');
		}
	
		
	}
	
	
	public function update()
	{
		//$credentials = array('email' => Input::get('email'));
		log::info(" update" );
		$redis = Redis::connection ();
		$userId=Input::get('userId');
		$token=Input::get('token');
		$password=Input::get('password');
		$passwordCon=Input::get('password_confirmation');
		log::info(" user name ". $userId.' token '.$token.'password 1 '.$password.' password conf '.$passwordCon);
		if(strlen($password)<6){
			return Redirect::back()->withErrors('Password must contain atleast 6 characters');
		}
		if($password==null || $passwordCon==null)
		{
			return Redirect::back()->withErrors('Enter Both password');
		}
		else if($password!=$passwordCon)
		{
			return Redirect::back()->withErrors('Password not match');
		}
		//$id = DB::table('users')->where('username', $userId)->pluck('id');
		$id=User::whereRaw('BINARY username = ?', [$userId])->pluck('id');
		DB::table('users')
            ->where('id', $id)
            ->update(array('password' => Hash::make($password)));
			$redis->hmset ( 'H_UserId_Cust_Map', $userId . ':password',$password);
			$redis->del($token);

			try{
				$token = uniqid()."-".uniqid();
				$redis->set($token,"$userId:$password");
				$redis->expire($token,60);
				
				$url = "https://cpanel.vamosys.com/api/changePassword/token:$token";
				$url = htmlspecialchars_decode($url);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_TIMEOUT, 3);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
				$response = curl_exec($ch);
				$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				curl_close($ch);
			}catch(Exception $e){

			}
	return Redirect::to('login')->with('flash_notice', 'Your password has been reset');	
			
			
		
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) App::abort(404);

		return view('password.reset')->with('token', $token);
	}

	

	/*
		check old password
	*/

	public function menuResetPassword(){

		log::info(' menu reset reminder');
		$username 	= Auth::user()->username;
		$tableValue = DB::select('select username, password from users where username = :username',['username' => $username]);
		$oldPwd 	= Input::get('pwd');
		
		foreach($tableValue as $key=>$value)
		{
			log::info(array_values(get_object_vars($value))[0]);
			log::info(array_values(get_object_vars($value))[1]);
			if(Hash::check($oldPwd, array_values(get_object_vars($value))[1]) == 1){

				log::info(' Sucess  ');
				return 'sucess';
			} else {

				log::info(' fail  ');
				return 'fail';
			}
			
		}

		// log::info(array_values(DB::table('users')->where('username', 'MSS')));

		
		// foreach($Licence as $mob){	
			// log::info('for');
			// if(array_values(get_object_vars($mob))[0] == 'MSS'){
			// log::info(' Licence ');
			// log::info(Hash::make(array_values(get_object_vars($mob))[1]));
			// log::info(array_values(get_object_vars($mob))[1]);
			// $pwd = array_values(get_object_vars($mob))[1];
			// log::info(Hash::check($oldPwd, array_values(get_object_vars($mob))[1]));
			// log::info('end if');

 			// }     	
		// }
	}


	/*
		update password
	*/

	public function menuUpdatePassword(){

		log::info(' menu update password');
		// try {
		$redis 			= Redis::connection ();
		$username 		= Auth::user()->username;
		$password 		= Input::get('pwd');
		$oldpassword 	= Input::get('old');
		// log::info($password);
		// log::info($oldpassword);
		try {
			
			$tableValue = DB::select('select username, password, email, id from users where username = :username',['username' => $username]);
			foreach($tableValue as $key=>$value)
			{
				// log::info(array_values(get_object_vars($value))[0]);
				// log::info(array_values(get_object_vars($value))[1]);
				// log::info(array_values(get_object_vars($value))[2]);
				// log::info(array_values(get_object_vars($value))[3]);
				if(Hash::check($oldpassword, array_values(get_object_vars($value))[1]) == 1){
					log::info('  old password correct  ');
					DB::table('users') ->where('id', array_values(get_object_vars($value))[3]) ->update(array('password' => Hash::make($password)));
					$redis->hmset ( 'H_UserId_Cust_Map', $username . ':password',$password);

					
					// return 'sucess'; Session::pull ( 'email' )
				} else {
					log::info('  old password wrong  ');
					
					return 'oldPwd';
				}
				
			}
			return 'sucess';
		} catch (Exception $e) {
			log::info($e);
			return '';
		}

		// 	$redis = Redis::connection ();
		// 	$mailId = DB::select('select email from users where username = :username',['username' => $username]);
		// 	$id = DB::table('users')->where('username', $username)->pluck('id');
		// 	DB::table('users') ->where('id', $id) ->update(array('password' => Hash::make($password)));
		// 	// $mailId ='';
		// 	foreach($mailId as $key=>$value)
		// 	{
		// 		$mailId 	= array_values(get_object_vars($value))[0];
		// 	}
		// 	log::info(gettype($mailId));
		// 	log::info($mailId);
		// 	try {
		// 		// log::info($mailId);
		// 		Session::put('email',$mailId);
		// 		$response=Mail::send('emails.menuReset', array('url'=>$username), function($message){
		// 			$message->to(Session::pull ( 'email' ))->subject('PASSWORD RESET!');
		// 		});	

		// 	} catch (Exception $e) {
					
		// 		log::info(' error ');
		// 		log::info($e);
		// 	}
			
		// 	$redis->hmset ( 'H_UserId_Cust_Map', $username . ':password',$password);

		// 	return 'sucess';
		// } catch (Exception $exce) {
		// 	log::info($exce);
		// 	return 'fail';
		// }	




	}

}
