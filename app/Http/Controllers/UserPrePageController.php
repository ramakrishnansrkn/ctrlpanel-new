<?php
namespace App\Http\Controllers;

use App, Config, CustomizeLandingPage, languageSuggession;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserPrePageController extends Controller {

    public function userPreference()
    {
     if (! Auth::check ()) {
        return Redirect::to ( 'login' );
     }
     $totalReportList = array();
     $list = array();
     $totalList = array();
     $reportsList = array();
     $username = Auth::user ()->username;
     $user = $username;
     $redis = Redis::connection ();
     $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
     $userPrePage=$redis->hget ( 'H_UserId_Cust_Map', $user . ':userPre');
     $totalReport = null;
    
             $dealerOrUser = $redis->sismember('S_Dealers_'.$fcode, $user);
             if($dealerOrUser)
                {
                        $totalReports = $redis->smembers("S_Users_Reports_Dealer_".$user.'_'.$fcode);
                }else
                {
                        $totalReports = $redis->smembers("S_Users_Reports_".$user.'_'.$fcode);
                }

        if($totalReports != null){
                foreach ($totalReports as $key => $value) {
                  
                                $rep=explode(":",$value)[0];
                                if($rep=='AC'){
                                   $totalReport[explode(":",$value)[1]][] = $value;
                                   $totalReport[explode(":",$value)[1]][] = 'SEC_ENGINE_ON:sensor';
                                }
                                else if($rep=='IGNITION'){
                                  $totalReport[explode(":",$value)[1]][] = $value;
                                   $totalReport['Sensor'][] = 'PRI_ENGINE_ON:sensor';
                                }
                                else if($rep=='RFID'){
                                  $totalReport[explode(":",$value)[1]][] = $value;
                                   $totalReport['Sensor'][] = 'RFID_NEW:sensor';
                                }
                                else if($rep=='FUEL'){
                                   $totalReport['Sensor'][] = 'FUEL_ANALYTICS:sensor';
                                }
                                else if($rep=='FUEL_CONSOLIDATE_REPORT'){
                                   $totalReport[explode(":",$value)[1]][] = $value;
                                   $totalReport['Sensor'][] = 'VEHICLE_WISE_FUEL:sensor';
                                   $totalReport['Sensor'][] = 'VEHICLE_WISE_INTRA_FUEL:sensor';
                                }
                                else if($rep=='LOAD'){
                                  log::info(' load report removed');
                                }
                                else{
                                  $totalReport[explode(":",$value)[1]][] = $value;
                                }
                                
                       
                }
                $totalList = $totalReport;
                }
            //return $totalList;

                $reports = Config::get('constant.reports');
      if($totalList==null && $totalList==null )
                {
                Session::flash ( 'message', ' No Reports Found' . '!' );
                //return $userPrePage;
                return view ( 'vdm.users.UserPreReports', array (
                                'userId' => $user
                ) )
                ->with('totalList',$totalList)
                ->with('userPrePage',$userPrePage)
                ->with('reports',$reports);
            }else{
                
                return view ( 'vdm.users.UserPreReports', array (
                                'userId' => $user
                ) )
                ->with('totalList',$totalList)
                ->with('userPrePage',$userPrePage)
                ->with('reports',$reports);
            }
                
                
        }

        public function updateUserPreference()
        {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $userId = Input::get('userId');
                log::info($userId);
               $userPrePage = Input::get('userPrePage');
               $list = array();
               $totalList = array();
               $reportsList = array();
               $username = Auth::user ()->username;
               $user = $username;
               $redis = Redis::connection ();
               $oldUserPrePage=$redis->hget ( 'H_UserId_Cust_Map', $user . ':userPre');
               $redis->hmset ( 'H_UserId_Cust_Map', $userId . ':userPre',$userPrePage );
               $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
               $userPrePage=$redis->hget ( 'H_UserId_Cust_Map', $user . ':userPre');
               $totalReport = null;
    
             $dealerOrUser = $redis->sismember('S_Dealers_'.$fcode, $user);
             if($dealerOrUser)
                {
                        $totalReports = $redis->smembers("S_Users_Reports_Dealer_".$user.'_'.$fcode);
                }else
                {
                        $totalReports = $redis->smembers("S_Users_Reports_".$user.'_'.$fcode);
                }

        if($totalReports != null){
                foreach ($totalReports as $key => $value) {
                  
                                $rep=explode(":",$value)[0];
                                if($rep=='AC'){
                                   $totalReport[explode(":",$value)[1]][] = $value;
                                   $totalReport[explode(":",$value)[1]][] = 'SEC_ENGINE_ON:sensor';
                                }
                                else if($rep=='IGNITION'){
                                  $totalReport[explode(":",$value)[1]][] = $value;
                                   $totalReport['Sensor'][] = 'PRI_ENGINE_ON:sensor';
                                }
                                else if($rep=='RFID'){
                                  $totalReport[explode(":",$value)[1]][] = $value;
                                   $totalReport['Sensor'][] = 'RFID_NEW:sensor';
                                }
                                else if($rep=='FUEL'){
                                   $totalReport['Sensor'][] = 'FUEL_ANALYTICS:sensor';
                                }
                                else if($rep=='FUEL_CONSOLIDATE_REPORT'){
                                   $totalReport[explode(":",$value)[1]][] = $value;
                                   $totalReport['Sensor'][] = 'VEHICLE_WISE_FUEL:sensor';
                                   $totalReport['Sensor'][] = 'VEHICLE_WISE_INTRA_FUEL:sensor';
                                }
                                else if($rep=='LOAD'){
                                  log::info(' load report removed');
                                }
                                else{
                                  $totalReport[explode(":",$value)[1]][] = $value;
                                }
                                
                       
                }
                $totalList = $totalReport;
                }
            //return $totalList;
      if($totalList==null && $totalList==null )
                {
                Session::flash ( 'message', ' No Reports Found' . '!' );
                //return $userPrePage;
                return view ( 'vdm.users.UserPreReports', array (
                                'userId' => $user
                ) )
                ->with('totalList',$totalList)
                ->with('userPrePage',$userPrePage);
            }
                else
                {
                //return $userPrePage;  
                    Session::flash ( 'success', 'Successfully Updated!...' );
                    log::info('before entry in CustomizeLandingPage ');
                    try{
                    //$userIpAddress=$_SERVER['REMOTE_ADDR'];
                    if(!$oldUserPrePage)$oldUserPrePage='Track';
                    if(!$userPrePage)$userPrePage='Track';
                    $userIpAddress=Session::get('userIP');
                    $mysqlDetails =array();
                    $mysqlDetails=array(
                      'fcode' => $fcode,
                      'userName'=>$username,
                      'userIpAddress'=>$userIpAddress,
                      'status' => Config::get('constant.updated'),
                      'oldLandingPage'=> $oldUserPrePage,
                      'newLandingPage'=>$userPrePage,
                    );
                    $modelname = new CustomizeLandingPage();   
                    $table = $modelname->getTable();
                    $db=$fcode;
                    AuditTables::ChangeDB($db);
                    $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
                    if(count($tableExist)>0){
                        CustomizeLandingPage::create($mysqlDetails);
                    }
                    else{
                        AuditTables::CreateCustomizeLandingPage();
                        CustomizeLandingPage::create($mysqlDetails);
                    }
                    AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
                    }
                   catch (Exception $e) {
                          AuditTables::ChangeDB(Config::get('constant.VAMOSdb')); 
                          log::info('Error inside CustomizeLandingPage Update'.$e->getMessage());
                          log::info($mysqlDetails);
                        }
                    log::info('entry added in CustomizeLandingPage');
                    return view ( 'vdm.users.UserPreReports', array (
                                'userId' => $user
                ) )
                ->with('totalList',$totalList)
                ->with('userPrePage',$userPrePage);
                   
                }
           }



        public function auditShow($model)
                {
           return 'hello';
                    try{
                    $username = Auth::user ()->username;
                    $redis = Redis::connection ();
                    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
                    $modelname = new $model();
                    $table = $modelname->getTable();
                    log::info($fcode);
                    log::info($table);
                    AuditTables::ChangeDB($fcode);
                        //AuditTables::CreateAuditFrans();
                    $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$fcode)->where('table_name','=',$table)->get();
                    log::info('---------------------------------------------------------------------');
                    log::info(count($tableExist));
                    //$nn=FranchisesTable::excecute();

                 //    return $view;

                        // $columns = Schema::getColumnListing('Audit_Frans');
                        if(count($tableExist)>0){
                        $columns = array_keys($model::first()->toArray());
                        $rows=$model::all();

                }
                else {

                        $columns=[];
                        $rows=[];
                  //   return "<div class='alert alert-danger'>
                                //      <p> No data Found </p>
                                // </div>";

                }
                AuditTables::ChangeDB('VAMOSYS');
                // \Config::set('database.connections.mysql.database', 'VAMOSYS');
                //      DB::purge('mysql');
                //      foreach($columns as $key => $value) {

                //     //echo $value." ";

                // }
                // foreach($rows as $row) {

                // foreach($columns as $key => $value) {

                //     echo $row[$value]." ";

                // }

                //    echo $row." -------------------------------------------------";



                // }


                }
                catch (customException $e) {
                  //display custom message
                  log::info($e->errorMessage());
                }
                return view ( 'vdm.franchise.audit', array ('columns' => $columns,'rows' => $rows ) );

                }
  // public function  getFileNames(){
  //       $fileNames=[];
  //       $dir = "../app/views/ReleaseNotes/";

  //       // Open a directory, and read its contents
  //       if (is_dir($dir)){
  //        if ($dh = opendir($dir)){
  //          while (($file = readdir($dh)) !== false){
  //           if(strpos($file, '.pdf') !== false){
  //            array_push($fileNames,$file);
  //           }
  //          }
  //          closedir($dh);
  //          return $fileNames;
  //    }

  //    }
  //  }

    public function  getFileNames(){
        $fileNames=[];
        /*$s3 = App::make('aws')->get('s3');
        $objects = $s3->getListObjectsIterator(array(
            'Bucket' => Config::get('constant.bucket'),
            'Prefix' => 'ReleaseNotes/'
        )); */
        $s3 = App::make('aws')->createClient('s3', [
                        'endpoint' => 'https://sgp1.digitaloceanspaces.com',
                ]);
                $objects = $s3->getIterator('ListObjects', array(
                'Bucket' => Config::get('constant.bucket'),
                                'Prefix' => 'ReleaseNotes/',
                ));
        foreach ($objects as $object) {
            //echo $object['Key'] . "\n";
            if(strpos($object['Key'], '.pdf') !== false){
                $fileName = str_replace('ReleaseNotes/', '', $object['Key']);
                array_push($fileNames,$fileName);
            }
        }
        return $fileNames;

    }

     public function sendSuggession()
        {
                    $username = Auth::user ()->username;
                    $redis = Redis::connection ();
                    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
                    $wordsList = Input::get('wordList');
                    $mysqlDetails=array(
                    'userName'=>$username,
                    'fcode' => $fcode,
                    );
                    log::info($mysqlDetails);
                    $modelname = new languageSuggession();
                    $table = $modelname->getTable();
                    $db=Config::get('constant.VAMOSdb');
                    $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
                    //return $tableExist;
                    if(count($tableExist)>0){
                        foreach($wordsList as $word)
                            {   $langDetails=[];
                                $langDetails = array_add($mysqlDetails, 'wordsList', $word);
                                languageSuggession::create($langDetails);
                            }
                    }
                    else{
                        AuditTables::CreatelanguageSuggession();
                        foreach($wordsList as $word)
                            {   $langDetails=[];
                                $langDetails = array_add($mysqlDetails, 'wordsList', $word);
                                languageSuggession::create($langDetails);
                            }

                    }
                    return 'success';
        }

public function showLanguageSuggession()
        {

         try{
            $username = Auth::user ()->username;
            $redis = Redis::connection ();
            $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
            $modelname = new languageSuggession();
            $table = $modelname->getTable();
            log::info($fcode);
            log::info($table);
            $fcode=Config::get('constant.VAMOSdb');
            //AuditTables::CreateAuditFrans();
            $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$fcode)->where('table_name','=',$table)->get();
            log::info('---------------------------------------------------------------------');
            log::info(count($tableExist));

            if(count($tableExist)>0&&(languageSuggession::first()!=null)){
            $columns = array_keys(languageSuggession::first()->toArray());
            $rows=languageSuggession::all();

        }
        else {
         $rows=[];
         $columns=array (
            0 => 'id',
            1 => 'fcode',
            2 => 'userName',
            3 => 'wordsList',
            4 => 'flag',
          );

        }
        }
        catch (customException $e) {
          //display custom message
          log::info($e->errorMessage());
        }
        unset( $columns[array_search( 'fcode', $columns )],$columns[array_search( 'flag', $columns )] );
        //return 'hello';
        $count = VdmFranchiseController::liveVehicleCountV2();
        $timezoneKey = $redis->exists('Update:Timezone');
        $apnKey = $redis->exists('Update:Apn');
        return view ( 'vdm.franchise.viewLangSugg', array ('columns' => $columns,'rows' => $rows,'count'=>$count) )->with('timezoneKey',$timezoneKey)->with('apnKey',$apnKey);

        }

        public function acceptWord($id)
                {
                    $task = languageSuggession::where('id', $id)
            ->update(array('flag' => 1));

                    Session::flash('message', 'Word Accepted Successfully!');

                    return Redirect::to ( 'showLnSugg' );
                }
        public function rejectWord($id)
                {
                    $task = languageSuggession::where('id', $id)
            ->update(array('flag' => 0));

                    Session::flash('errormessage', 'Word Ignored Successfully!');

                    return Redirect::to ( 'showLnSugg' );
                }
                public function getUserName()
                {
                    $redis = Redis::connection ();
                    $username = Auth::user ()->username;
                    $newname = $redis->hget ( 'H_Newusernames', $username );
                    if($newname){
                    $names=$username.':'.$newname;
                    return $names;
                    }

                }

}
