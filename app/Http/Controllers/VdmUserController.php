<?php
namespace App\Http\Controllers;

use Request;
use App;
use App\User;
use Config, AuditUser, AuditDealer, DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

class VdmUserController extends Controller {
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		Log::info('username:' . $username . '  :: fcode' . $fcode);
		$redisUserCacheId = 'S_Users_' . $fcode;
		if(Session::get('cur')=='dealer')
		{
			log::info( '------login 1---------- '.Session::get('cur'));
			
			$redisUserCacheId = 'S_Users_Dealer_'.$username.'_'.$fcode;
		}
		else if(Session::get('cur')=='admin')
		{
			$redisUserCacheId = 'S_Users_Admin_'.$fcode;
		}
	
		$userList = $redis->smembers ( $redisUserCacheId);
		
		$userGroups = null;
		$userGroupsArr = null;
		foreach ( $userList as $key => $value ) {
			
			
			$userGroups = $redis->smembers ( $value);
			
			$userGroups = implode ( '<br/>', $userGroups );
			
			$userGroupsArr = array_add ( $userGroupsArr, $value, $userGroups );
		}
		
		return view ( 'vdm.users.index' )->with ( 'fcode', $fcode )->with ( 'userGroupsArr', $userGroupsArr )->with ( 'userList', $userList );
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function search()		
    {		
        log::info(' reach the road speed function ');		
        $orgLis = [];		
            return view('vdm.users.scan')->with('userList', $orgLis);		
    }		
	public function scan() {		
       if (! Auth::check ()) {		
            return Redirect::to ( 'login' );		
        }		
        $username = Auth::user ()->username;		
        $redis = Redis::connection ();		
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );		
        Log::info('username:' . $username . '  :: fcode' . $fcode);		
        $redisUserCacheId = 'S_Users_' . $fcode;		
        if(Session::get('cur')=='dealer')		
        {		
            log::info( '------login 1---------- '.Session::get('cur'));		
            		
            $redisUserCacheId = 'S_Users_Dealer_'.$username.'_'.$fcode;		
        }		
        else if(Session::get('cur')=='admin')		
        {		
            $redisUserCacheId = 'S_Users_Admin_'.$fcode;		
        }		
        $userList = $redis->smembers ( $redisUserCacheId);		
        $text_word = Input::get('text_word');		
        $cou = $redis->SCARD($redisUserCacheId); // log::info($cou);		
        $orgLi = $redis->sScan( $redisUserCacheId, 0, 'count', $cou, 'match', '*'.$text_word.'*'); // log::info($orgLi);		
        $orgL = $orgLi[1];		
        $userGroups = null;		
        $userGroupsArr = null;		
        foreach ( $orgL as $key => $value ) {                   		
            $userGroups = $redis->smembers ( $value);           		
            $userGroups = implode ( '<br/>', $userGroups );   		
            $userGroupsArr = array_add ( $userGroupsArr, $value, $userGroups );		
        }   		
        return view ( 'vdm.users.scan' )->with ( 'fcode', $fcode )->with ( 'userGroupsArr', $userGroupsArr )->with ( 'userList', $orgL );		
    }		
    /**		
     * Show the form for creating a new resource.		
     * @return Response		
     */
	public function create() {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		Log::info('---------------users:--------------');
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		
		$redisGrpId = 'S_Groups_' . $fcode;
		if(Session::get('cur')=='dealer')
		{
			log::info( '------login 1---------- '.Session::get('cur'));
			$redisGrpId = 'S_Groups_Dealer_'.$username.'_'.$fcode;
		}
		else if(Session::get('cur')=='admin')
		{
			$redisGrpId = 'S_Groups_Admin_'.$fcode;
		}
		
		// $vehicleGroups=array("No groups found");
		$vehicleGroups = null;
		$size = $redis->scard ( $redisGrpId );
		if ($size > 0) {
			
			$groups= $redis->smembers ( $redisGrpId );
			
			foreach ( $groups as $key => $value ) {
				$vehicleGroups = array_add ( $vehicleGroups, $value, $value );
			}
		}
        
        $size = $redis->scard('S_Organisations_'.$fcode);
        $orgsList=array();
        if ($size > 0) {
            $orgs = $redis->smembers('S_Organisations_'.$fcode);
            foreach ( $orgs as $key => $value ) {
                $orgsList = array_add ( $orgsList, $value, $value );
            }
        }
        $user=null;
		$userName=null;
		$user1= new VdmDealersController;
		$user=$user1->checkuser();
		return view ( 'vdm.users.create' )->with ( 'vehicleGroups', $vehicleGroups )->with('orgsList',$orgsList)->with ( 'user', $user )->with('userName',$userName);
	}
	public function createNew($id)
        {
        if (! Auth::check()) {
            return redirect('login');
        }
        Log::info('---------------users:--------------');
        $username = Auth::user()->username;
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $userName=$id;
        
        $redisGrpId = 'S_Groups_' . $fcode;
        if (Session::get('cur')=='dealer') {
            log::info('------login 1---------- '.Session::get('cur'));
            $redisGrpId = 'S_Groups_Dealer_'.$username.'_'.$fcode;
        } else if (Session::get('cur')=='admin') {
            $redisGrpId = 'S_Groups_Admin_'.$fcode;
        }
        
        // $vehicleGroups=array("No groups found");
        $vehicleGroups = null;
        $size = $redis->scard($redisGrpId);
        if ($size > 0) {
            $groups= $redis->smembers($redisGrpId);
            
            foreach ($groups as $key => $value) {
                $vehicleGroups = array_add($vehicleGroups, $value, $value);
            }
        }
        
        $size = $redis->scard('S_Organisations_'.$fcode);
        $orgsList=[];
        if ($size > 0) {
            $orgs = $redis->smembers('S_Organisations_'.$fcode);
            foreach ($orgs as $key => $value) {
                $orgsList = array_add($orgsList, $value, $value);
            }
        }
        $user=null;
        
        $user1= new VdmDealersController;
        $user=$user1->checkuser();
        return view('vdm.users.create')->with('vehicleGroups', $vehicleGroups)->with('orgsList', $orgsList)->with('user', $user)->with('userName',$userName);
    }
	/**
	 * Store a newly created resource in storage.
	 * TODO validations should be improved to prevent any attacks
	 * 
	 * @return Response
	 */
	public function store() {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		
		
		$rules = array (
				'userId' => 'required|alpha_dash',
				'email' => 'required|email',
				'vehicleGroups' => 'required'  
				);
                
                
		$validator = Validator::make ( Input::all (), $rules );
       
		if ($validator->fails ()) {
			return Redirect::to ( 'vdmUsers/create' )->withErrors ( $validator );
		}else {
		      $userId = Input::get ( 'userId' );
              $val = $redis->hget ( 'H_UserId_Cust_Map', $userId . ':fcode' );
              $val1= $redis->sismember ( 'S_Users_' . $fcode, $userId );
		}
		if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) 
		{
			return Redirect::to ( 'vdmUsers/create' )->withErrors ( 'Name with admin not acceptable' );
		}
		if($val1==1 || isset($val)) {
			Session::flash ( 'message', $userId . ' already exist. Please use different id ' . '!' );
			return Redirect::to ( 'vdmUsers/create' );
		}
		else {
			// store
			
			$userId = Input::get ( 'userId' );
			$email = Input::get ( 'email' );
			$cc_email = Input::get ( 'cc_email' );
			$vehicleGroups = Input::get ( 'vehicleGroups' ); log::info($vehicleGroups);
			$mobileNo = Input::get ( 'mobileNo' );
			$zoho = Input::get ( 'zoho' ); 
			$companyName = Input::get('companyName');
			$enabledebug=Input::get ( 'enable' );
            foreach ( $vehicleGroups as $grp ) {
				$redis->sadd ( $userId, $grp );
				///ram noti
				 $redis->sadd ( 'S_'.$grp, $userId );
				 ///
				// log::info($grp);  
				// $grpVehi=$redis->smembers($grp);
				// foreach ($grpVehi as $keyV => $valueV) 
				// {
				// 	$checkU=$redis->hget('H_Vehicle_Map_Uname_'.$fcode, $valueV.'/'.$grp);
				// 	if(empty($checkU)) 
				// 	{
				// 	   log::info("vehi data empty");
    //                    $redis->hset('H_Vehicle_Map_Uname_'.$fcode, $valueV.'/'.$grp, $userId);               
    //            	    } 
    //            		else {
    //            		   $redis->hset('H_Vehicle_Map_Uname_'.$fcode, $valueV.'/'.$grp, $checkU.'/'.$userId);
    //            		}
                        
    //            	}
				///
			}
            // thirumani set Reports
                        if(Session::get('cur')=='dealer')
                        {
                        log::info( '------login 1---------- '.Session::get('cur'));

                        $totalReports = $redis->smembers('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
                        }
                        else if(Session::get('cur')=='admin')
                        {
                                        $totalReports = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
                        }
                        if($totalReports != null)
                        {
                                foreach ($totalReports as $key => $value) {
                                        $redis-> sadd('S_Users_Reports_'.$userId.'_'.$fcode, $value);
                                }
                        }

            $virtualaccount=Input::get ( 'virtualaccount' );
			      $assetuser=Input::get ( 'assetuser' );

			//$notificationset = 'S_VAMOS_NOTIFICATION';
			//$groupList = $redis->smembers ( $notificationset);
			$notificationset = 'S_VAMOS_NOTIFICATION_'.$fcode;
			$notificationset1 = 'S_VAMOS_NOTIFICATION';
			$groupList0 = $redis->smembers ( $notificationset);
			if($groupList0==null)
			{
				$groupList = $redis->smembers ($notificationset1);
			}
			else
			{
				$groupList=$groupList0;
			}
			$notiString=implode(",", $groupList);
			$redis->hset("H_Notification_Map_User",$userId,$notiString);   
			log::info(Input::get ( 'virtualaccount' ). '------login 1---------- ');
            $userType='';
			if($virtualaccount=='value')
			{
				$redis->sadd ( 'S_Users_Virtual_' . $fcode, $userId );
				if($userType!="")
				    $userType="Virtual Account".','.$userType;
			    else
			    	$userType="Virtual Account";
			}
			if($assetuser=='value')
			{
				$redis->sadd ( 'S_Users_AssetTracker_' . $fcode, $userId );
				if($userType!="")
				    $userType="Asset User".','.$userType;
			    else 
                   $userType="Asset User"; 
			}
			$redis->sadd ( 'S_Users_' . $fcode, $userId );
			if(Session::get('cur')=='dealer')
			{
				log::info( '------login 1---------- '.Session::get('cur'));
				$redis->sadd('S_Users_Dealer_'.$username.'_'.$fcode,$userId);
				$OWN=$username;
			}
			else if(Session::get('cur')=='admin')
			{
				$redis->sadd('S_Users_Admin_'.$fcode,$userId);
				$OWN='admin';
			}
			
			$password=Input::get ( 'password' );
			if($password==null)
			{
				$password='awesome';
			} 
			log::info( '------login 8---------- ' );
			$redis->hmset ( 'H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo,$userId.':email',$email ,$userId.':password',$password,$userId.
				':zoho',$zoho, $userId.':OWN',$OWN,$userId . ':companyName', $companyName,$userId.':cc_email',$cc_email);
			log::info( '------login 9---------- ' );
			$user = new User;
			
			$user->name = $userId;
			$user->username=$userId;
			$user->email=$email;
			$user->mobileNo=$mobileNo;
		
			$user->password=Hash::make($password);
			$user->save();



			log::info("before audit user create entry");
			try{
					$availableReports="";
					if($totalReports!=null){
						log::info($totalReports);
					$availableReports=implode(', ', $totalReports);
				    }
				 //    if($notiString!=null){
					// 	log::info($notiString);
					// $availableNotification=implode(', ', $notiString);
				 //    }
		            $vehicleList = array();
					foreach ($vehicleGroups as $key => $gropName) {
						$gpVeh=$redis->smembers($gropName);
						foreach ($gpVeh as $key => $vehicle) {
							$vehicleList=array_add($vehicleList,$vehicle,$vehicle);
						}
					}
					$details = array();
					$details = array (
					'fcode' => $fcode,
		            'userId' => $userId,
		            'userName'=>$username,
		            'status' => Config::get('constant.created'),
					'email'=>$email,
					'mobileNo'=>$mobileNo,
				    'password'=>Hash::make($password),
				    'zoho'=> $zoho,
					'companyName' => $companyName,
					'cc_email' => $cc_email,
		            'groups'	=>implode(",",$vehicleGroups),
		            'vehicles' =>implode(",",$vehicleList),
		            'reports'=> $availableReports,
		            'Notification'=> $notiString,
		            'userType' => $userType,
		            'userIpAddress'=>Session::get('userIP'),
					);
		            $modelname = new AuditUser();   
				    $table = $modelname->getTable();
		            $db=$fcode;
				    AuditTables::ChangeDB($db);
					$tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
					if(count($tableExist)>0){
						AuditUser::create($details);
					}
					else{
						AuditTables::CreateAuditUser();
						AuditUser::create($details);
					}
					
				}catch (Exception $e) {
					       AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				           log::info('Error inside AuditUser Create'.$e->getMessage());
				           log::info($details);
				        }
			log::info("before audit user create entry");


			//$notificationset = 'S_VAMOS_NOTIFICATION';
			//$notificationGroups =  $redis->smembers ( $notificationset);
			$notificationset = 'S_VAMOS_NOTIFICATION_'.$fcode;
			$notificationset1 = 'S_VAMOS_NOTIFICATION';
			$groupList0 = $redis->smembers ( $notificationset);
			if($groupList0==null)
			{
				$notificationGroups = $redis->smembers ($notificationset1);
			}
			else
			{
				$notificationGroups=$groupList0;
			}
			if(count($notificationGroups)>0)
			{
				$notification=implode(",",$notificationGroups);
				$redis->hset("H_Notification_Map_User",$userId,$notification);
			}
			if($enabledebug=='Enable'){
				$redis->hmset ( 'H_UserId_Cust_Map', $userId . ':Enable','oneDay');
				$validity=1*86400;
				$grouplist=$redis->smembers($userId);
				foreach ($grouplist as $key => $groupId)
				{
					$vehicleList=$redis->smembers($groupId);
					foreach ($vehicleList as $key => $vehicleId)
					{
						$redis->set('EnableLog:'.$vehicleId.':'.$fcode,$userId);
						$redis->expire('EnableLog:'.$vehicleId.':'.$fcode,$validity);
						$deviceId=$redis->hget('H_Vehicle_Device_Map_' . $fcode,$vehicleId);
						$pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
					}
				}
			}
			else{
				$redis->hmset ( 'H_UserId_Cust_Map', $userId . ':Enable','0');
			}
			
			
			// redirect
			Session::flash ( 'message', 'Successfully created ' . $userId . '!' );
			return Redirect::to ( 'vdmUserScan/user'.$userId );
		}
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function show($id) {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		
		$redis = Redis::connection ();
		$userId = $id;
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$mobileNo = $redis->hget ( 'H_UserId_Cust_Map', $userId . ':mobileNo' );
		$email = $redis->hget ( 'H_UserId_Cust_Map', $userId . ':email' );
		$vehicleGroups = $redis->smembers ( $userId );
		//$redis->sadd ( 'S_Users_Virtual_' . $fcode, $userId );
		$value=false;
		 if($redis->sismember ( 'S_Users_Virtual_' . $fcode, $userId )==1)
		 {
		 	$value=true;
		 }

		$vehicleGroups = implode ( '<br/>', $vehicleGroups );
		
		return view ( 'vdm.users.show', array (
				'userId' => $userId 
		) )->with ( 'vehicleGroups', $vehicleGroups )->with('mobileNo',$mobileNo)->with('email',$email)->with('value',$value);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function edit($id) {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$userId = $id;
		
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		
		$mobileNo = $redis->hget ( 'H_UserId_Cust_Map', $userId . ':mobileNo' );
		$email = $redis->hget ( 'H_UserId_Cust_Map', $userId . ':email' );
		$cc_email = $redis->hget ( 'H_UserId_Cust_Map', $userId . ':cc_email' );
		$zoho = $redis->hget ( 'H_UserId_Cust_Map', $userId . ':zoho' );
		$companyName = $redis->hget('H_UserId_Cust_Map', $userId . ':companyName');
		//Enable log
        $enablelogLits=$redis->keys('EnableUserLog:'.$userId.':'.$fcode);
        if ($enablelogLits != null) {
				$enable='Enable';
		}else{
			$enable='Disable';
		}
        
        
		$redisGrpId = 'S_Groups_' . $fcode;
		if(Session::get('cur')=='dealer')
		{
			log::info( '------login 1---------- '.Session::get('cur'));
			$redisGrpId = 'S_Groups_Dealer_'.$username.'_'.$fcode;
		}
		else if(Session::get('cur')=='admin')
		{
			$redisGrpId = 'S_Groups_Admin_'.$fcode;
		}
		
	
		$groupList = $redis->smembers ( $redisGrpId);
		
		$vehicleGroups = null;
		
		$selectedGroups = $redis->smembers($userId); 
		
		
		foreach ( $groupList as $key => $value ) {
			$vehicleGroups = array_add ( $vehicleGroups, $value, $value );
		}
        
        $size = $redis->scard('S_Organisations_'.$fcode);
       
        $orgsList=array();
        if ($size > 0) {
            $orgs = $redis->smembers('S_Organisations_'.$fcode);
            foreach ( $orgs as $key => $value ) {
                $orgsList = array_add ( $orgsList, $value, $value );
            }
        }
        
        $size = $redis->scard('S_Orgs_' .$userId . '_' . $fcode);
        
        $selectedOrgsList=array();
        if($size >0) {
             $orgs = $redis->smembers('S_Orgs_' .$userId . '_' . $fcode);
            foreach ( $orgs as $key => $value ) {
                $selectedOrgsList = array_add ( $selectedOrgsList, $value, $value );
            }
            
        }
  		$value=false;
		 if($redis->sismember ( 'S_Users_Virtual_' . $fcode, $userId )==1)
		 {
		 	$value=true;
		 }
		$assetuser=false;
		 if($redis->sismember ( 'S_Users_AssetTracker_' . $fcode, $userId )==1)
		 {
		 	$assetuser=true;
		 }
		return view ( 'vdm.users.edit', array (
				'userId' => $userId 
		) )->with ( 'vehicleGroups', $vehicleGroups )->with ( 'mobileNo', $mobileNo )->
		with('email',$email)->with ('zoho', $zoho )->with('selectedGroups',$selectedGroups)->with('orgsList',$orgsList)->with('selectedOrgsList',$selectedOrgsList)->with('value',$value)->with('assetuser',$assetuser)->with('companyName', $companyName)->with('cc_email',$cc_email)->with('enable',$enable);
	}




	public function notification($id) {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$userId = $id;
		
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		
		$notificationset = 'S_VAMOS_NOTIFICATION_'.$fcode;
		$notificationset1 = 'S_VAMOS_NOTIFICATION';
		
		$notification=$redis->hget("H_Notification_Map_User",$userId);
		$notificationArray=array();
		if($notification!==null)
		{
			$notificationArray=explode(",",$notification);
		}
		//$groupList = $redis->smembers ( $notificationset);
		$groupList0 = $redis->smembers ( $notificationset);
		if($groupList0==null)
		{
			$groupList = $redis->smembers ($notificationset1);
		}
		else
		{
			$groupList=$groupList0;
		}
		$notificationGroups = null;
		
		 
		
		
		foreach ( $groupList as $key => $value ) {
			$notificationGroups = array_add ( $notificationGroups, $value, $value );
		}
        
       
        
      
  		
		return view ( 'vdm.users.notification', array (
				'userId' => $userId 
		) )->with ( 'notificationGroups', $notificationGroups )
		->with('notificationArray',$notificationArray);
	}

public function updateFn()
{
	$userId = Input::get ( 'userId' );
	if (! Auth::check ()) {
		return Redirect::to ( 'login' );
	}
	$username = Auth::user ()->username;
	$redis = Redis::connection ();
	$oldNotification=$redis->hget("H_Notification_Map_User",$userId);
		$oldNotificationArray=array();
		$notificationGroups=array();
		if($oldNotification!==null)
		{
			$oldNotificationArray=explode(",",$oldNotification);
		}
	$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
	$notificationGroups = Input::get ( 'notificationGroups' );
	if(count($notificationGroups)>0)
	{
		$notification=implode(",",$notificationGroups);
		$availableNotification=implode(", ",$notificationGroups);
		$redis->hset("H_Notification_Map_User",$userId,$notification);
	} else {
		$availableNotification="";
		$redis->hset("H_Notification_Map_User",$userId,'');
	}
            log::info("before audit User-edit Notification entry");
           try{
             $addedNoti=[];
             $removedNoti=[];
             if(count($notificationGroups)>0){    
	        $addedNoti=array_diff($notificationGroups,$oldNotificationArray);
            $removedNoti=array_diff($oldNotificationArray,$notificationGroups);
            }
            
            if(count($removedNoti)!=0)$removedNotifications=implode( ", ", $removedNoti );
            else $removedNotifications='';
            if(count($addedNoti)!=0)$addedNotifications=implode( ", ", $addedNoti );
            else $addedNotifications='';
                 $details = array();
	             $details = array (
				     'fcode' => $fcode,
	                 'userId' => $userId,
	                 'userName'=>$username,
	                 'status' =>  Config::get('constant.notificationEdited'),
				     'Notification'=> $availableNotification,
					 'removedNotification'=>$removedNotifications,
					 'addedNotification'=>$addedNotifications,
					 'userIpAddress'=>Session::get('userIP'),
				);
	            $modelname = new AuditUser();   
			    $table = $modelname->getTable();
	            $db=$fcode;
			    AuditTables::ChangeDB($db);
				$tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
				if(count($tableExist)>0){
					AuditUser::create($details);
				}
				else{
					AuditTables::CreateAuditUser();
					AuditUser::create($details);
				}
				AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				}catch (Exception $e) {
					       AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				           log::info('Error inside AuditUser Notification Edit'.$e->getMessage());
				           log::info($details);
				        }
	            log::info("after  User-edit Notification audit entry");


	log::info($userId);
}


	


public function updateNotification() {
	
	// $userId = Input::get ( 'userId' );
	// if (! Auth::check ()) {
	// 	return Redirect::to ( 'login' );
	// }
	$notify= new VdmUserController;
	$notify->updateFn();
	// Session::flash ( 'message', 'Successfully updated  Notification' . $userId . '!' );
	return Redirect::to ( 'vdmUsers' );
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function update($id) {
	
		$userId = $id;
		
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$username 	= Auth::user ()->username;
		$redis 		= Redis::connection ();
		$fcode 		= $redis->hget ( 'H_UserId_Cust_Map', $userId . ':fcode' );
		$oldMob 	= $redis->hget ( 'H_UserId_Cust_Map', $userId . ':mobileNo' );
		$oldmail	= $redis->hget ( 'H_UserId_Cust_Map', $userId . ':email' );
		$old_cc		= $redis->hget ( 'H_UserId_Cust_Map', $userId . ':cc_email' );
		$oldzoho       = $redis->hget ( 'H_UserId_Cust_Map', $userId .  ':zoho' );
		$oldcompanyName       = $redis->hget('H_UserId_Cust_Map', $userId .  ':companyName');
		$oldVirtual = $redis->sismember ( 'S_Users_Virtual_'. $fcode,  $userId);
		$oldAsset = $redis->sismember ( 'S_Users_AssetTracker_'. $fcode,  $userId);
		$oldGroup	= (array) $redis->smembers( $userId );


		$rules = array (
				'mobileNo' => 'required',
				'email' => 'required',
				'email' => 'required',
				'vehicleGroups' => 'required' 
		);
		$validator = Validator::make ( Input::all(), $rules );
		if ($validator->fails ()) {
			Log::error('VDM User Controller update validation failed');
			Session::flash ( 'message', 'Update failed. Please check logs for more details' . '!' );
			return Redirect::to ( 'vdmUsers/'.$id.'/edit' )->withErrors ( $validator );
		} else {
			// store
			
			
			$vehicleGroups = Input::get ( 'vehicleGroups' );
			///ram noti
			$result=array_diff($oldGroup,$vehicleGroups);
			foreach ( $result as $delGrp )
			{
		        $delVehiList=$redis->smembers('S_'.$delGrp);
		        log::info($delVehiList);
		        $checkUD=$redis->srem('S_'.$delGrp, $userId);
		    }
		    $resultAdd=array_diff($vehicleGroups,$oldGroup);
			foreach ( $resultAdd as $addGrp )
			{
			    $addVehiList=$redis->smembers('S_'.$addGrp);
			    log::info($addVehiList);
			    $addU=$redis->sadd('S_'.$addGrp, $userId);
            }
            ///
			$mobileNo = Input::get ( 'mobileNo' );
			$email = Input::get ( 'email' );
			$cc_email= Input::get('cc_email');
			$zoho = Input::get ( 'zoho' );
			$companyName = Input::get('companyName');
			$redis->del ( $userId );
			foreach ( $vehicleGroups as $grp ) {
				$redis->sadd ( $userId, $grp );
			}
			$redis->hmset ( 'H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo,$userId.':email',$email ,$userId . ':zoho',$zoho, $userId . ':companyName', $companyName,$userId.':cc_email',$cc_email);
            
            log::info("before audit user update entry");
            try{
            $vehicleList = array();
			foreach ($vehicleGroups as $key => $gropName) {
				$gpVeh=$redis->smembers($gropName);
				foreach ($gpVeh as $key => $vehicle) {
					$vehicleList=array_add($vehicleList,$vehicle,$vehicle);
				}
			}
			 $details = array();
             $details = array (
			     'fcode' => $fcode,
                 'userId' => $userId,
                 'userName'=>$username,
                 'status' => Config::get('constant.updated'),
			     'email'=>$email,
			     'mobileNo'=>$mobileNo,
			     'zoho'=> $zoho,
			     'companyName' => $companyName,
			     'cc_email' => $cc_email,
                 'groups'	=>implode(",",$vehicleGroups),
			     'vehicles' =>implode(",",$vehicleList),
			     'addedGroups' =>implode(",",$resultAdd),
			     'removedGroups' => implode(",",$result),
			     'userIpAddress'=>Session::get('userIP'),
			);
            $modelname = new AuditUser();   
		    $table = $modelname->getTable();
            $db=$fcode;
		    AuditTables::ChangeDB($db);
			$tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
			if(count($tableExist)>0){
				AuditUser::create($details);
			}
			else{
				AuditTables::CreateAuditUser();
				AuditUser::create($details);
			}
			AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
			}catch (Exception $e) {
				           AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				           log::info('Error inside AuditUser Update'.$e->getMessage());
				           log::info($details);
				        }
            log::info("after audit user update entry");


			 $virtualaccount=Input::get ( 'virtualaccount' );
        $assetuser=Input::get ( 'assetuser' );

			 if($virtualaccount=='value')
			 {
			 	$redis->sadd ( 'S_Users_Virtual_' . $fcode, $userId );
			 }else
			 {
			 	$redis->srem ( 'S_Users_Virtual_' . $fcode, $userId );
			 }
        if($assetuser=='value')
			 {
			 	$redis->sadd ( 'S_Users_AssetTracker_' . $fcode, $userId );
			 }else
			 {
			 	$redis->srem ( 'S_Users_AssetTracker_' . $fcode, $userId );
			 }
			

			$mailId = array();
        	$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
        	$franchiseDetails=json_decode($franDetails_json,true);
        	if(isset($franchiseDetails['email1'])==1){
                $mailId[]               = $franchiseDetails['email2'];
        		log::info(array_values($mailId));
        	}
        	
        	if(Session::get('cur')=='dealer')
        	{
        		log::info( '------login 1---------- '.$redis->hget ( 'H_UserId_Cust_Map', $username . ':email' ));
                $mailId[] =   $redis->hget ( 'H_UserId_Cust_Map', $username . ':email' );
        		
        	}

        	$oldList = array();
        	$newList = array();

        	if($mobileNo != $oldMob){
				$oldList = array_add($oldList, 'Mobile No ', $oldMob);
        		$newList = array_add($newList, 'Mobile No ', $mobileNo);        		
        	}
			if ($companyName != $oldcompanyName) {
                $oldList = array_add($oldList, 'Company Name ', $oldcompanyName);
                $newList = array_add($newList, 'Company Name ', $companyName);
            }
        	if($oldmail != $email){
        		$oldList = array_add($oldList, 'Email ', $oldmail);
        		$newList = array_add($newList, 'Email ', $email);        			
        	}
			if($old_cc != $cc_email){
        		$oldList = array_add($oldList, 'Cc Email ', $old_cc);
        		$newList = array_add($newList, 'Cc Email ', $cc_email);        			
        	}
            
            
            if($zoho != $oldzoho){
				$oldList = array_add($oldList, 'Zoho', $oldzoho);
        		$newList = array_add($newList, 'Zoho ', $zoho);        		
        	}

        	if($redis->sismember ( 'S_Users_Virtual_'. $fcode,  $userId) != $oldVirtual){
        		$oldList = array_add($oldList, 'Virtual Account ', (($oldVirtual == 1) ? true : false));
        		$newList = array_add($newList, 'Virtual Account ', (($redis->sismember ( 'S_Users_Virtual_'. $fcode,  $userId) ==1 )? true : false));       	
        	}
          if($redis->sismember ( 'S_Users_AssetTracker_'. $fcode,  $userId) != $oldAsset){
        		$oldList = array_add($oldList, 'Asset User ', (($oldAsset == 1) ? true : false));
        		$newList = array_add($newList, 'Asset User ', (($redis->sismember ( 'S_Users_AssetTracker_'. $fcode,  $userId) ==1 )? true : false));       	
        	}
        	if($vehicleGroups != $oldGroup){
        		$oldList = array_add($oldList, 'Group List ', implode(",",$vehicleGroups));
        		$newList = array_add($newList, 'Group List ', implode(",",$oldGroup));
        	}
        	
        	
        	// log::info(array_values($oldList));
        	// log::info(array_values($newList));
        	// log::info(array_values($mailId));
        	if((count($oldList) >0) && (count($newList) >0))
	        	if(sizeof($mailId) > 0)
	        		try{
	        			log::info(' inside the try function ');
	        			$caption = "User Id";
			        	Mail::queue('emails.user', array('username'=>$fcode, 'groupName'=>$id, 'oldVehi'=>$newList, 'newVehi'=> $oldList, 'cap'=>$caption), function($message) use ($mailId, $id)
			        	{
			                //Log::info("Inside email :" . Session::get ( 'email' ));
			        		$message->to($mailId)->subject('User Id Updated -' . $id);
			        	});
			        } catch(\Swift_TransportException $e){
				        log::info($e->getMessage());
				    }
					//enable log
					$enabledebug=Input::get ( 'enable' ); 
					 if($enabledebug=='Enable'){
						$validity=86400;
						$grouplist=$redis->smembers($userId);
						foreach ($grouplist as $key => $groupId){
							$vehicleList=$redis->smembers($groupId);
							foreach ($vehicleList as $key => $vehicleId){
								$redis->set('EnableLog:'.$vehicleId.':'.$fcode,$userId);
								$redis->expire('EnableLog:'.$vehicleId.':'.$fcode,$validity);
								$deviceId=$redis->hget('H_Vehicle_Device_Map_' . $fcode,$vehicleId);
								$pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
							}
						}
                        $redis->set('EnableUserLog:'.$userId.':'.$fcode,'yes');
                        $redis->expire('EnableUserLog:'.$userId.':'.$fcode,$validity);
					}
					if($enabledebug=='Disable'){						
						$grouplist=$redis->smembers($userId);
                        $redis->del('EnableUserLog:'.$userId.':'.$fcode);                                      
						foreach ($grouplist as $key => $groupId){
							$vehicleList=$redis->smembers($groupId);
							foreach ($vehicleList as $key => $vehicleId){
								$redis->del('EnableLog:'.$vehicleId.':'.$fcode);
								$deviceId=$redis->hget('H_Vehicle_Device_Map_' . $fcode,$vehicleId);
								$pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
							}
						}
					}
           // 
           /* $orgsList = Input::get ( 'orgsList' );
            // $orgs = $redis->smembers('S_Orgs_' .$userId . '_' . $fcode);
            $redis->del('S_Orgs_' .$userId . '_' . $fcode);
            
           if(empty($orgsList)) {
               
           }
           else {
               foreach ( $orgsList as $org ) {
                    $redis->sadd ( 'S_Orgs_' .$userId . '_' . $fcode, $org );
                }
            }*/
                
                
			// redirect
			Session::flash ( 'message', 'Successfully updated ' . $userId . '!' );
			return Redirect::to ( 'vdmUserScan/user'.$userId );
		}
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id        	
	 * @return Response
	 */
	public function destroy($id) {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		$userId = $id;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		
		$redis->srem ( 'S_Users_' . $fcode, $userId );
		$redis->srem('S_Users_Dealer_'.$username.'_'.$fcode,$userId);
		$redis->srem('S_Users_Admin_'.$fcode,$userId);
		///ram noti
		$getUser=$redis->smembers($userId);
		foreach ($getUser as $key => $getU) {
			 $redis->srem('S_'.$getU, $userId);
		}
		///
		
		$redis->del ( $userId );
         $redis->del('S_Orgs_' .$userId . '_' . $fcode);

		$email=$redis->hget('H_UserId_Cust_Map',$userId.':email');
		$redis->hdel ( 'H_UserId_Cust_Map', $userId . ':fcode', $userId . ':mobileNo', $userId.':email',$userId.':password',$userId.':companyName');
		$redis->hdel ( 'H_Notification_Map_User', $userId);
		
		Log::info(" about to delete user" .$userId);
		
		DB::table('users')->where('username', $userId)->delete();
		 

		 log::info("before audit user delete entry");
		 try{
		  $details = array();
		  $details = array (
			'fcode' => $fcode,
            'userId' => $userId,
            'userName'=>$username,
            'status' => Config::get('constant.deleted'),
			'email'=>$email,
			'userIpAddress'=>Session::get('userIP'),
			);
            $modelname = new AuditUser();   
		    $table = $modelname->getTable();
            $db=$fcode;
		    AuditTables::ChangeDB($db);
			$tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
			if(count($tableExist)>0){
				AuditUser::create($details);
			}
			else{
				AuditTables::CreateAuditUser();
				AuditUser::create($details);
			}
			AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
			}catch (Exception $e) {
						   AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				           log::info('Error inside AuditUser Delete'.$e->getMessage());
				           log::info($details);
				        }
          log::info("after audit user delete entry");
		
		Session::put('email',$email);
		Log::info("Email Id :" . Session::get ( 'email1' ));
		
	//	Email Service activeted - 06-jul-2018 
	
		
		$emailFcode=$redis->hget('H_DealerDetails_'.$fcode,$username);
        $emailFile=json_decode($emailFcode, true);
        $email1=$emailFile['email'];
   
		$emailFcode1=$redis->hget('H_Franchise', $fcode);
        $emailFile1=json_decode($emailFcode1, true);
        $email2=$emailFile1['email2'];
              
        if(Session::get('cur')=='dealer'){
			$emails=array($email1);
        }
        else if(Session::get('cur')=='admin'){
            $emails=array($email2);
        }
		
		Mail::queue('emails.welcome', array('fname'=>$fcode,'userId'=>$userId), function($message) use ($emails,$userId)
		{
			Log::info("Inside email :" . Session::get ( 'email' ));
		
			$message->to($emails)->subject('User Id : '.$userId.' deleted');
		});

		Session::flash ( 'message', 'Successfully deleted ' . $userId . '!' );
		return Redirect::to ( 'vdmUsers' );
	}

	public function userIdCheck(){
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$userId = Input::get ( 'id' );
		log::info(' inside the function userIdCheck'.$userId);
		$valFirst = $redis->hget ( 'H_UserId_Cust_Map', ucfirst(strtolower($userId)) . ':fcode' );
		$valUpper = $redis->hget ( 'H_UserId_Cust_Map', strtolower($userId) . ':fcode' );
      	$val1= $redis->sismember ( 'S_Users_' . $fcode, $userId );
		$val = $redis->hget ( 'H_UserId_Cust_Map', $userId . ':fcode' );
      	$val1= $redis->sismember ( 'S_Users_' . $fcode, $userId );
      	if($val1==1 || isset($val) || isset($valFirst) || isset($valUpper)) {
      		return 'Already exist. Please use different id ';
      	}
      	if($userId== 'ADMIN' || $userId == 'Admin' || $userId == 'admin'){
      		return 'Name with admin not acceptable ';
      	}
	}


	public function notificationFrontend() {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		//$notificationset = 'S_VAMOS_NOTIFICATION';
		$notification=$redis->hget("H_Notification_Map_User",$username);
		$notificationArray=array();
		if($notification!==null)
		{
			$notificationArray=explode(",",$notification);
		}
		$notificationset = 'S_VAMOS_NOTIFICATION_'.$fcode;
		$notificationset1 = 'S_VAMOS_NOTIFICATION';
			$groupList0 = $redis->smembers ( $notificationset);
			if($groupList0==null)
			{
			$groupList = $redis->smembers ($notificationset1);
			}
			else
			{
			$groupList=$groupList0;
			}
		//$groupList = $redis->smembers ( $notificationset);
		$notificationGroups = array();
		foreach ( $groupList as $key => $value ) {

			$notificationGroups = array_add ( $notificationGroups, $value, $value );
			$notificationGroups[$value] = 'false';
			
			if(sizeof($notificationArray) > 0)
			{
				foreach ($notificationArray as $ky => $val) {
				
					if($val ==  $value){
						$notificationGroups[$value] = 'true';
					}
				}
			}

		}
        
        
		return (sizeof($notificationGroups) > 0) ? $notificationGroups : 'fail';
	}

	public function notificationFrontendUpdate(){

		try{
			$notify= new VdmUserController;
			$notify->updateFn();
			return 'success';
		}catch(\Exception $e) {
			return 'fail';
		}
	}

    public function reports($id)
    {
     if (! Auth::check ()) {
        return Redirect::to ( 'login' );
     }
     $totalReportList = array();
     $list = array();
     $totalList = array();
     $reportsList = array();
     $username = Auth::user ()->username;
     $user = $id;
     $redis = Redis::connection ();
     $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
     $totalReport = null;
     if(Session::get('cur')=='dealer')
        {
            log::info( '------login 1---------- '.Session::get('cur'));
            $dealeReportLen = $redis->scard('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
            $adminLength =      $redis->scard('S_Users_Reports_Admin_'.$fcode);
            if($adminLength >= $dealeReportLen)
            {
                $totalReports = $redis->smembers('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
            }else{
                $totalReports = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
                log::info('its incorrect report');
                $redis->del('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
                foreach ($totalReports as $key => $value) {
                        $redis->sadd('S_Users_Reports_Dealer_'.$username.'_'.$fcode, $value);
                }
            }
        }
        else if(Session::get('cur')=='admin')
        {
            $totalReports = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
        }
        $isVirtualuser = $redis->sismember("S_Users_Virtual_".$fcode, $user);
        $virtualReports = array();
        log::info(gettype($virtualReports));
        if($isVirtualuser)
         {
           $virtualReports = $redis->smembers('S_UserVirtualReports');
         }
        if($totalReports != null){
                foreach ($totalReports as $key => $value) {
                        if(in_array($value, $virtualReports))
                        {
                                log::info('checking');
                        }else{

                                $totalReport[explode(":",$value)[1]][] = $value;
                        }
                }
                $totalList = $totalReport;
                }
        if($totalList == null)
        {
                $totalReport = $redis->keys("*_Reports");
                $report[] = array();
                        foreach ($totalReport as $key => $getReport) {
                        $specReports = $redis->smembers($getReport);
                        foreach ($specReports as $key => $ReportName) {
                                // $report[]=$ReportName.':'.$reportType[0];
                        }
                        // log::info($report);
                        $reportsList[] = $getReport;
                        $totalReportList[] = $report;
                        $totalList[$getReport] = $report;
                        $report = null;
                $dealerOrUser = $redis->sismember('S_Dealers_'.$fcode, $user);
                if($dealerOrUser)
                        {
                                return Redirect::to ( 'vdmDealers' );
                        }else
                        {
                                return Redirect::to ( 'vdmUsers' );
                        }
                        // log::info('mmmmm');
                        // log::info($totalReport);
                }
        }
                $dealerOrUser = $redis->sismember('S_Dealers_'.$fcode, $user);
                if($dealerOrUser)
                {
                        $userReports = $redis->smembers("S_Users_Reports_Dealer_".$user.'_'.$fcode);
                }else
                {
                        $userReports = $redis->smembers("S_Users_Reports_".$user.'_'.$fcode);
                }
                //after hidden fuel fill, fuel theft & fuel
                //log::info('BEFORE TOTAL_REPORT_LIST');
                //log::info($totalList);
                $totalListsa=null;
                $an=0;
                $bn=0;
                $value1=null;
                $value2=null;
                foreach ($totalList as $key => $value) {
                	/*if($key=='Analytics'){
                		foreach ($value as $keys => $values) {
                			if($keys!=6){
                				log::info('inside the 6 '.$values);
                				$value1=array_add($value1,$an,$values);
                				$an=$an+1;
                			}
                		}
                		$totalListsa=array_add($totalListsa,$key,$value1);
                	}else if($key=='Sensor'){
                		foreach ($value as $keys => $values) {
                			if($keys ==0 || $keys==9){

                			}else{
                				$value2=array_add($value2,$bn,$values);
                				$bn=$bn+1;
                			}
                		}
                		$totalListsa=array_add($totalListsa,$key,$value2);
                	}else{ */
                		$totalListsa=array_add($totalListsa,$key,$value);
                	//}
                	
                }
                $totalList=$totalListsa;
                //log::info('AFTER HIDDEN TOTAL_REPORT_LIST');
                //log::info($totalList);
        //fuel hidden end
        
				if($userReports==null && $totalReportList==null )
                {
                Session::flash ( 'message', ' No Reports Found' . '!' );
                return view ( 'vdm.users.reports', array (
                                'userId' => $user
                ) )->with ( 'reportsList', $reportsList )
                ->with('totalReportList',$totalReportList)
                ->with('totalList',$totalList)
                ->with('userReports',$userReports);
                
                }
                else
                {
                return view ( 'vdm.users.reports', array (
                                'userId' => $user
                ) )->with ( 'reportsList', $reportsList )
                ->with('totalReportList',$totalReportList)
                ->with('totalList',$totalList)
                ->with('userReports',$userReports);
				}
        }

        public function updateReports()
        {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                log::info('Entry');
                $userId = Input::get('userId');
                log::info($userId);
                $username = Auth::user ()->username;
                log::info($username);
                $reportName = Input::get('reportName');
                $redis = Redis::connection ();
                $fcode = $redis->hget ( 'H_UserId_Cust_Map', $userId . ':fcode' );
                $dealerOrUser = false;
                $dealerOrUser = $redis->sismember('S_Dealers_'.$fcode, $userId);
                $removedReports='';
                $addedReports='';
                $availableReports='';
                if($dealerOrUser)
                        {
                $deleteDealerReports = $redis->smembers('S_Users_Reports_Dealer_'.$userId.'_'.$fcode);
            }else{
                $deleteUserReports = $redis->smembers("S_Users_Reports_".$userId.'_'.$fcode);
            }
				if($reportName != null)
                {
                        log::info($dealerOrUser);
                        if($dealerOrUser)
                        {
                                log::info('Dealer');
                                $prevReportList = $redis->smembers('S_Users_Reports_Dealer_'.$userId.'_'.$fcode);
                                $redis->del("S_Users_Reports_Dealer_".$userId.'_'.$fcode);
                                foreach ($reportName as $key => $value) {
                                        $redis->sadd("S_Users_Reports_Dealer_".$userId.'_'.$fcode, $value);
                                }
                                $removeList = array();
                                $addList=array();
                                $currentReportList = $redis->smembers('S_Users_Reports_Dealer_'.$userId.'_'.$fcode);
                                foreach ($prevReportList as $key => $value) {
                                if (in_array($value, $currentReportList)) {
                                        $addList[]=$value;
                                        log::info("GotErix");
                                }else
                                {
                                        $removeList[] = $value;
                                        $removedReports=$removedReports.','.$value;
                                        log::info("GotErix2");
                                }
                            }
                            // added for added reports 
		                      foreach ($currentReportList as $key => $value) {
		                                if (in_array($value, $prevReportList)) {
		                                        //$addList[]=$value;
		                                        log::info("GotErix");
		                                }else
		                                {
		                                        $addedReports=$addedReports.','.$value;
		                                        log::info("GotErix2");
			                                }
			                            }
                        $userList = $redis->smembers("S_Users_Dealer_".$userId.'_'.$fcode);
                        log::info($userList);
                        foreach ($userList as $key => $dealer) {
                                $userReport=$redis->smembers("S_Users_Reports_".$dealer.'_'.$fcode);
                                {
                                        if($userReport==null)
                                        {
                                        
                                        foreach ($reportName as $key => $value) {
                                        $redis->sadd("S_Users_Reports_".$dealer.'_'.$fcode,$value );
                                         }
                                        }
                                        
                                   
                                }
                               }
                                
                        log::info($removeList);
                        if($removeList != null)
                        {
                                $userList = $redis->smembers("S_Users_Dealer_".$userId.'_'.$fcode);
                                log::info($userList);
                                foreach ($userList as $key => $dealer) {
                                        $redis->srem("S_Users_Reports_".$dealer.'_'.$fcode, $removeList);
                                }

                        }
                        }
                
                        else if(!$dealerOrUser)
                        {       
                                log::info('user define');
	                            $prevReportList = $redis->smembers('S_Users_Reports_'.$userId.'_'.$fcode);
                                $redis->del("S_Users_Reports_".$userId.'_'.$fcode);
                                $redis->sadd("S_Users_Reports_".$userId.'_'.$fcode, $reportName);
                                $currentReportList = $redis->smembers("S_Users_Reports_".$userId.'_'.$fcode);
                            // added for removed reports
                                foreach ($prevReportList as $key => $value) {
	                                if (in_array($value, $currentReportList)) {
	                                        //$addList[]=$value;
	                                        log::info("GotErix");
	                                }else
	                                {
	                                        $removedReports=$removedReports.','.$value;
	                                        log::info("GotErix2");
		                                }
	                            }

                             // added for added reports 
                               foreach ($currentReportList as $key => $value) {
	                                if (in_array($value, $prevReportList)) {
	                                        //$addList[]=$value;
	                                        log::info("GotErix");
	                                }else
	                                {
	                                        $addedReports=$addedReports.','.$value;
	                                        log::info("GotErix2");
		                                }
	                            }



                        }
                    



                }
                else {
                        log::info('jjjjjjjj');
             
						if($dealerOrUser)
                        {       
                        	   $currentReportList = $redis->smembers("S_Users_Reports_Dealer_".$userId.'_'.$fcode);
                                 foreach ($currentReportList as $key => $value) {
                                 $removeList[] = $value;
                                       
	                            }
                                $redis->del("S_Users_Reports_Dealer_".$userId.'_'.$fcode);
								$userList = $redis->smembers("S_Users_Dealer_".$userId.'_'.$fcode);
                                foreach ($userList as $key => $dealer) {
                                $redis->del("S_Users_Reports_".$dealer.'_'.$fcode);
                                }
                        }else
                        {      
                                log::info(" correct identification ");
                                 $currentReportList = $redis->smembers("S_Users_Reports_".$userId.'_'.$fcode);
                                 foreach ($currentReportList as $key => $value) {
                                 $removedReports=$removedReports.','.$value;
                                       
	                            }
                                 $redis->del("S_Users_Reports_".$userId.'_'.$fcode);
                        }

                }
                log::info($reportName);
            if($dealerOrUser){
		            log::info('before entry in AuditDealer ');
		            if($reportName != null)
                   {  
		                 foreach ($reportName as $key => $value) {
                                 $availableReports=$availableReports.','.$value;
                                       
	                            }
	                        }
	               else{
	                  	foreach ($deleteDealerReports as $key => $value) {
                                 $removedReports=$removedReports.','.$value;
                                       
	                            }
	                  }
		            try{
					$mysqlDetails =array();
		            $mysqlDetails=array(
		              'fcode' => $fcode,
		              'dealerId' => $userId,
			          'userName'=>$username,
			          'status' =>  Config::get('constant.reportsEdited'),
					  'reports'=> $availableReports,
					  'removedReports'=>$removedReports,
					  'addedReports'=>$addedReports,
					  'userIpAddress'=>Session::get('userIP'),
		            );
		            //AuditDealer::create($mysqlDetails);
		            $modelname = new AuditDealer();   
				    $table = $modelname->getTable();
					$db=$fcode;
				    AuditTables::ChangeDB($db);
					$tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
					if(count($tableExist)>0){
						AuditDealer::create($mysqlDetails);
					}
					else{
						AuditTables::CreateAuditDealer();
						AuditDealer::create($mysqlDetails);
					}
					AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
					}
				    catch (Exception $e) {
				    	  AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
						  log::info('Error inside AuditDealer Reports Edit '.$e->getMessage());
						  log::info($mysqlDetails);
						}
		            log::info('entry added in AuditDealer');
		        }else {
                log::info("before audit user-edit reports entry");
                try{
                	if($reportName != null)
                   { 
	                      foreach ($reportName as $key => $value) {
	                                 $availableReports=$availableReports.','.$value;
	                                       
		                            }
		                        }
		            else{
		                  	foreach ($deleteUserReports as $key => $value) {
	                                 $removedReports=$removedReports.','.$value;
	                                       
		                            }
	                  }
	             $details = array();
	             $details = array (
				     'fcode' => $fcode,
	                 'userId' => $userId,
	                 'userName'=>$username,
	                 'status' =>  Config::get('constant.reportsEdited'),
				     'reports'=> $availableReports,
					 'removedReports'=>$removedReports,
					 'addedReports'=>$addedReports,
					 'userIpAddress'=>Session::get('userIP'),
				);
	            $modelname = new AuditUser();   
			    $table = $modelname->getTable();
	            $db=$fcode;
			    AuditTables::ChangeDB($db);
				$tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
				if(count($tableExist)>0){
					AuditUser::create($details);
				}
				else{
					AuditTables::CreateAuditUser();
					AuditUser::create($details);
				}
				AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				}catch (Exception $e) {
					       AuditTables::ChangeDB(Config::get('constant.VAMOSdb'));
				           log::info('Error inside AuditUser Reports Edit'.$e->getMessage());
				           log::info($details);
				        }
	            log::info("after audit user-edit reports entry");
	        }

                //$redis->sadd('S_Reports_'.$userId.'_'.$fcode, 'SINGLE_TRACK:Tracking', 'HISTORY:Tracking','MULTITRACK:Tracking','CURRENT_STATUS:Consolidatedvehicles','MOVEMENT:Analytics','OVERSPEED:Analytics','PARKED:Analytics','IDLE:Analytics','DAILY:Statistics', 'DAILY_PERFORMANCE:Performance','PAYMENT_REPORTS:Useradmin','RESET_PASSWORD:Useradmin');
                if($dealerOrUser)
                {
                        return Redirect::to ( 'vdmDealers' );
                }else
                {
                        return Redirect::to ( 'vdmUserScan/user'.$userId );
                }
        }
     public function scanNew($id) {		
       if (! Auth::check ()) {		
            return Redirect::to ( 'login' );		
        }		
        $username = Auth::user ()->username;		
        $redis = Redis::connection ();		
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );		
        Log::info('username:' . $username . '  :: fcode' . $fcode);		
        $redisUserCacheId = 'S_Users_' . $fcode;		
        if(Session::get('cur')=='dealer')		
        {		
            log::info( '------login 1---------- '.Session::get('cur'));		
            		
            $redisUserCacheId = 'S_Users_Dealer_'.$username.'_'.$fcode;		
        }		
        else if(Session::get('cur')=='admin')		
        {		
            $redisUserCacheId = 'S_Users_Admin_'.$fcode;		
        }		
        $userList = $redis->smembers ( $redisUserCacheId);
        $text_word1 = $id;
        $text_word= str_replace(' ', '', $text_word1);
		$text_word2 = strtoupper($text_word1);	
		log::info($text_word2);	
        $cou = $redis->SCARD($redisUserCacheId); // log::info($cou);		
        $orgLi = $redis->sScan( $redisUserCacheId, 0, 'count', $cou, 'match', '*'.$text_word.'*'); 
        $orgL=$orgLi[1];
        $userGroups = null;		
        $userGroupsArr = null;	
        	
        foreach ( $orgL as $key => $value ) { 

            $userGroups = $redis->smembers ( $value);
            log::info($userGroups); 
            log::info($value.'--------------');          		
            $userGroups = implode ( '<br/>', $userGroups );   		
            $userGroupsArr = array_add ( $userGroupsArr, $value, $userGroups );	

        }   		
        return view ( 'vdm.users.scan' )->with ( 'fcode', $fcode )->with ( 'userGroupsArr', $userGroupsArr )->with ( 'userList', $orgL );		
    }	

			public function sendExcel()
    {
        log::info('inside excel');
        $username = Auth::user()->username;
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $devicesList=$redis->smembers('S_Device_' . $fcode);
	      if (Session::get('cur')=='dealer') {
            $emailFcode=$redis->hget('H_DealerDetails_'.$fcode,$username);
            $emailFile=json_decode($emailFcode, true);
            $email1=$emailFile['email'];
	          $owntype=$username;
	      }
	      else if (Session::get('cur')=='admin') {
          $emailFcode=$redis->hget('H_Franchise', $fcode);
          $emailFile=json_decode($emailFcode, true);
          $email1=$emailFile['email2'];
	        $owntype='OWN';
	      }
       else {
       $owntype='';
       }

        $file = Excel::create('Users List', function ($excel) {
        
            $excel->sheet('Short Name', function ($sheet) {
                $redis = Redis::connection();
                $username = Auth::user()->username;
                log::info('inside the vehiclescan sendExcel---');
                $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
                if (Session::get('cur')=='dealer') {
                    $vehicleListId='S_Users_Dealer_'.$username.'_'.$fcode;
                } else if (Session::get('cur')=='admin') {
                    $vehicleListId='S_Users_Admin_'.$fcode;
                } 
                               
                $userList=$redis->smembers($vehicleListId);
                log::info($userList);
                
                $userGroups = null;
                $userGroupsArr = null;
                foreach ($userList as $key => $value) {
                        $userGroups = $redis->smembers($value);
                        log::info($userGroups);
                        $userGroups = implode(',', $userGroups);
                        $userGroupsArr = array_add($userGroupsArr, $value, $userGroups);
                }
                
                    $sheet->setWidth([
                                     'A'     =>  7,
                                     'B'    =>   30,
                                     'C'    =>   35
                                     
                             ]);
                $sheet->loadView('emails.userDetails',['userList'=>$userList,'userGroupsArr'=>$userGroupsArr]);
         
            });
        });//->download('xls');
              
              $data[]='get all de';
              $mymail=Mail::send('emails.userL', $data, function ($message) use ($file, $email1,$owntype) {
                $message->to($email1);
                //$message->to('nithya.vamosys@gmail.com');
                $message->subject('Users List - '.$owntype);
                $message->attach($file->store("xls", false, true)['full']);
                
              });
        Session::flash('message', 'Users List Sent to '.$email1.' Id Successfully');
        return Redirect::back();
    }
    		
}
	
