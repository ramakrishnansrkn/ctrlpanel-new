<?php
namespace App\Http\Controllers;

use Request;
use App;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

class VdmDealersScanController extends Controller {
	

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function dealerSearch()
    {
        log::info(' reach the road speed function ');
        $orgLis = [];
            return view('vdm.dealers.index1')->with('dealerlist', $orgLis);
    }   
    public function dealerScan() {
       if (! Auth::check ()) {
            return Redirect::to ( 'login' );
        }
        $username = Auth::user ()->username;
        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        Log::info('username:' . $username . '  :: fcode' . $fcode);
        $redisDealerCacheID = 'S_Dealers_' . $fcode;
        $dealerlist = $redis->smembers ( $redisDealerCacheID);
        $text_word = Input::get('text_word');
        $cou = $redis->SCARD($redisDealerCacheID); // log::info($cou);
        $orgLi = $redis->sScan( $redisDealerCacheID, 0, 'count', $cou, 'match', '*'.$text_word.'*'); //log::info($orgLi);
        $orgL = $orgLi[1];
        $userGroups = null;
        $userGroupsArr = null;
		$dealerWeb=null;
        foreach ( $orgL as $key => $value ) { 
            $userGroups = $redis->smembers ( $value);
            $userGroups = implode ( '<br/>', $userGroups );
            $detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $value);
            $detail=json_decode($detailJson,true);
            $userGroupsArr = array_add ( $userGroupsArr, $value, $detail['mobileNo'] );
			$dealerWeb = array_add ( $dealerWeb, $value, $detail['website'] );
        }
        return view ( 'vdm.dealers.index1' )->with ( 'fcode', $fcode )->with ( 'userGroupsArr', $userGroupsArr )->with ( 'dealerlist', $orgL )->with ( 'dealerWeb', $dealerWeb );
    }
	public function dealerScanNew($id) {
       if (! Auth::check ()) {
            return Redirect::to ( 'login' );
        }
        $username = Auth::user ()->username;
        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        Log::info('username:' . $username . '  :: fcode' . $fcode);
        $redisDealerCacheID = 'S_Dealers_' . $fcode;
        $dealerlist = $redis->smembers ( $redisDealerCacheID);
        $text_word = $id;
        $cou = $redis->SCARD($redisDealerCacheID); // log::info($cou);
        $orgLi = $redis->sScan( $redisDealerCacheID, 0, 'count', $cou, 'match','*'.$text_word.'*'); //log::info($orgLi);
        $orgL = $orgLi[1];
        $userGroups = null;
        $userGroupsArr = null;
		$dealerWeb=null;
        foreach ( $orgL as $key => $value ) { 
            $userGroups = $redis->smembers ( $value);
            $userGroups = implode ( '<br/>', $userGroups );
            $detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $value);
            $detail=json_decode($detailJson,true);
            $userGroupsArr = array_add ( $userGroupsArr, $value, $detail['mobileNo'] );
			$dealerWeb = array_add ( $dealerWeb, $value, $detail['website'] );
        }
        return view ( 'vdm.dealers.index1' )->with ( 'fcode', $fcode )->with ( 'userGroupsArr', $userGroupsArr )->with ( 'dealerlist', $orgL )->with ( 'dealerWeb', $dealerWeb );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
	*/
}