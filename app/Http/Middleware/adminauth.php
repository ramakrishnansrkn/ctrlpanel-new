<?php namespace App\Http\Middleware;

use Closure;

use Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;


class adminauth {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{   
		log::info("admin auth");
	
    if (! Auth::check ()) {
            return Redirect::to ( 'login' );
      }

      $username = Auth::user ()->username;
      Session::put('user',$username);
       $uri = \Route::current()->getName();
      Log::info('URL  '. $uri);
      Log::info('URL  '. $uri);
      $redis = Redis::connection ();
      $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
      if(strpos($username,'admin')!==false ) {

            //Auth::session(['cur' => 'admin']);
          Session::put('cur','admin');
		  Session::put('cur2','notKtrackAdmin');
		  $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
          $franchiseDetails=json_decode($franDetails_json,true);
          $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
          if($prepaid == 'yes') {
             Session::put('cur1','prePaidAdmin');
           }else{
              Session::put('cur1','norPaidAdmin');
			  if($fcode == 'KTOT') {
                Session::put('cur2','ktrackAdmin');
              }
           }
      }
	 
      else {
         $redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		log::info( '-----------------inside else filter adminauth-------------' . $username .$fcode) ;
		$val1= $redis->sismember ( 'S_Dealers_' . $fcode, $username );
		$val = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		  if($val1==1 && isset($val)) {
			Log::info('---------------is dealer adminauth:--------------');
			$breadcrumb=null;
              $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
                $franchiseDetails=json_decode($franDetails_json,true);
                $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
                if($prepaid == 'yes') {
                     Session::put('cur1','prePaidAdmin');
                }else{
                	Session::put('cur1','norPaidAdmin');
                }
			
			Session::put('cur','dealer');
			
			
		}
		else{
			return Redirect::to ( 'live' ); //TODO should be replaced with aunthorized page - error
		}
           
      }

		return $next($request);
	}

}

