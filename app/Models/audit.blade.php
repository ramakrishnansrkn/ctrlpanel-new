@extends('includes.adminheader')
@section('mainContent')
<div class="row" style="margin-top: -2%;">
	<div class="col-lg-9">
	<h1>Franchises Management</h1>
	</div>
	<!-- <div class="col-lg-3">
	<input class="form-control" style="margin-top: 10%;" id="myInput" type="text" placeholder="Search..">
	</div> -->
</div>
<table class="table table-bordered">
	<thead>
		<tr class="uppercase">
			@foreach($columns as $key => $value)
			<th style="text-transform: capitalize;">{{$value}}</th>
			@endforeach

		</tr>
	</thead>
	<tbody id="myTable">
	@foreach($rows as $row)
	@if($row['status']==Config::get('constant.created'))
		<tr class="alert alert-success">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.updated'))
		<tr class="alert alert-info">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.deleted'))
		<tr class="alert alert-danger">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['Status']=='OnBoard')
		<tr class="alert alert-success">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['Status']=='Migration')
		<tr class="alert alert-warning">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['Status']=='Renew')
		<tr class="alert alert-info">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@else
		<tr class="alert alert-info">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@endif

	@endforeach
	</tbody>
</table>
@stop
