<?php

use Illuminate\Database\Eloquent\Model;

class AuditVehicle extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'Audit_Vehicle';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 *
    
    /**
     * Indicates primary key of the table
     *
     * @var bool
     */
    public $primaryKey = 'id';
    
    /**
     * Indicates if the model should be timestamped
     * 
     * default value is 'true'
     * 
     * If set 'true' then created_at and updated_at columns 
     * will be automatically managed by Eloquent
     * 
     * If created_at and updated_at columns are not in your table
     * then set the value to 'false'
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */

    protected $fillable = array('fcode','vehicleId','userName', 'status','deviceId' , 'deviceModel','shortName', 'regNo','orgId','vehicleType', 'oprName' , 'mobileNo', 'overSpeedLimit','odoDistance',   'gpsSimNo','paymentType',  'OWN', 'expiredPeriod' ,'driverName','driverMobile','email', 'altShortName', 'sendGeoFenceSMS', 'morningTripStartTime' , 'eveningTripStartTime', 'parkingAlert','vehicleMake','Licence','Payment_Mode','descriptionStatus','vehicleExpiry','onboardDate' ,'tankSize','licenceissuedDate','communicatingPortNo','oldVehicleName','oldDeviceId','oldVehicleId','userIpAddress','country');
    
    /**
     * The attributes that aren't mass assignable
     *
     * @var array
     */
    protected $guarded = array();

}
