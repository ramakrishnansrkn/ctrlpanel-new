<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
<style>
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}

body{
font-family: 'Lato', sans-serif;
/*font-weight: bold;
font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
} 

.col-md-12 {
    width: 98% !important;
    left: 15px !important;
    padding-left: 20px !important;
}

</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134334975-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134334975-1');
</script>

</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">
      <?php include('sidebarList.php');?>

      <div id="testLoad"></div>
        
      <div id="page-content-wrapper">
         <div class="container-fluid">
            <div class="panel panel-default"></div>   
          </div>
      </div>
 
    <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: 10px;">
        <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                    <h3 class="box-title">{{reportNam}}</h3>
                </div>
               <div class="row">
                    <div class="col-md-2" align="center">
                        <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
                          <h5 style="color: grey;">{{shortNam}}</h5>
                        </div>
                        <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                          <h5 style="color: red;"><?php echo Lang::get('content.no_vehicle_selected'); ?></h5>
                        </div>
                  </div>
                   <?php include('dateTime.php');?>
                    <div class="col-md-1" align="center"></div>
                     <div class="col-md-1" align="center">
                        <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                </div>

              <!--  </div> -->
              <div class="row">
              <div class="col-md-1" align="center"></div>
                <div class="col-md-2" align="center">
                  <div class="form-group"></div>
                </div>
              </div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="box box-primary" style="min-height:570px;">
                <div>
                    <div class="pull-right" style="margin-top: 10px;margin-right: 5px;margin-bottom:10px;">
                    <button type="button" class="btn btn-success" ng-click="durationFilter('today')" ng-disabled="todayDisabled">Today</button>
                    <button type="button" class="btn btn-primary" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled">Yesterday</button>
                    <button type="button" class="btn btn-success" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled">Last Week</button>
                    <button type="button" class="btn btn-info" ng-click="durationFilter('month')" ng-disabled="monthDisabled">Month</button>
                    
                        <img style="cursor: pointer;" ng-click="exportData(dowloadId)"  src="../app/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV(dowloadId)"  src="../app/views/reports/image/csv.jpeg" />
                        <img style="cursor: pointer;" onclick="generatePDF()" id="downloadPDF" src="../app/views/reports/image/Adobe.png" />
                    </div>
                    <div id="formConfirmation">
            <div class="box-body" id="{{dowloadId}}">
              
                <table ng-if="acReportShow" class="table table-bordered table-striped table-condensed table-hover" ng-show="!errMsg">
                  <thead>
                    <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
                      <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(gName)}}</th>
                      <th style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.veh_name'); ?></th>
                      <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{acData2.vehicleName}}</th>         
                    </tr>
                    <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.id'); ?></th>
                        <th colspan="2" style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{acData2.vehicleId}}</th>
                        <th  style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total_duration'); ?></th>
                        <th  style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{durVarTot2}}</th>
                    </tr>
                    <tr style="text-align:center;" >
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.date_time'); ?></th>
                      <th colspan="2" style="background-color:#f9f9f9;font-weight:unset;font-size:12px;">{{acData2.fromTime | date:'yyyy-MM-dd'}}&nbsp;&nbsp;&nbsp;{{ acData2.fromTime | date:'HH:mm:ss'}}</th>
                      <th style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.date_time'); ?></th>
                      <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{acData2.toTime | date:'yyyy-MM-dd'}}&nbsp;&nbsp;&nbsp;{{acData2.toTime | date:'HH:mm:ss'}}</th>
                    </tr>
                  </thead>
              </table>
              
              <table ng-if="acReportShow" class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
                <thead>
                  <tr style="text-align:center">
                    <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.date_time'); ?></th>
                    <th width="8%" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;">{{reportTdNam}}</th>
                    <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.duration'); ?></th>
                    <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.address'); ?></th>
                    <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.gmap'); ?></th>
                  </tr>
                  </thead>
                 <tbody>
            <!-- <tr ng-repeat="ac in acData.alarmList | orderBy:natural(sort.sortingOrder):sort.reverse" class="active" style="text-align:center">-->
                  <tr ng-repeat="ac in acData" class="active" style="text-align:center">
                    <td ng-if="ac.alarmType == 'A/C ON' || ac.alarmType == 'A/C OFF'">{{ac.alarmTime | date:'yyyy-MM-dd'}}&nbsp;&nbsp;&nbsp;{{ac.alarmTime | date:'HH:mm:ss'}}</td>
                    <td ng-if="ac.alarmType == 'A/C ON' || ac.alarmType == 'A/C OFF'">
                        <span ng-if="ac.alarmType == 'A/C ON'" >
                          <span ng-if="!drSenReport" style="color: #05c505;">ON</span>
                          <span ng-if="drSenReport" style="color: red;">Open</span>
                        </span>
                        <span ng-if="ac.alarmType == 'A/C OFF'" >
                          <span ng-if="!drSenReport" style="color: red;">OFF</span>
                          <span ng-if="drSenReport" style="color: #05c505;">Close</span>
                        </span>
                    </td>
                    <!-- <td rowspan="2" ng-if="ac.alarmType == 'A/C ON'" style="text-align: center;padding-top: 15px;">{{msToTime(acData[$index+1].alarmTime-ac.alarmTime)}} </td> -->
                    <td rowspan="2"  ng-if="(ac.alarmType == 'A/C ON')&&($index+1<acData.length)" style="text-align: center;padding-top: 36px;">{{msToTime(acData[$index+1].alarmTime-ac.alarmTime)}} </td>
                    <td rowspan="2"  ng-if="(ac.alarmType == 'A/C ON')&&($index+1==acData.length)" style="text-align: center;padding-top: 36px;">{{msToTime(todaymillisec-ac.alarmTime)}} </td>
                    <td ng-if="ac.alarmType == 'A/C ON' || ac.alarmType == 'A/C OFF'"><p>{{ac.address}}</p></td>
                    <td ng-if="ac.alarmType == 'A/C ON' || ac.alarmType == 'A/C OFF'"><a href="https://www.google.com/maps?q=loc:{{ac.lat}},{{ac.lng}}" target="_blank">Link</a></td>
                  </tr> 
                  <tr ng-if="acData == null || acData == undefined || acData.length==0" align="center">
                    <td colspan="8" class="err" ng-if="!errMsg"><h5><?php echo Lang::get('content.zero_records'); ?></h5></td>
                    <td colspan="8" class="err" ng-if="errMsg"><h5>{{errMsg}}</h5></td>
                  </tr>
                  </tbody>
              </table>   
            <!-- </div>
            <div id="formConfirmation"> -->
              <table ng-if="prmEngineShow" id="table" class="table table-bordered table-striped table-condensed table-hover" ng-show="!errMsg">
                  <thead>
                    <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
                      <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(gName)}}</th>
                      <th style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.veh_name'); ?> / <?php echo Lang::get('content.id'); ?></th>
                      <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{acData2.shortName}} / {{acData2.vehicleId}} </th>         
                    </tr>
                    <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.engine_on'); ?></th>
                        <th colspan="2" style="background-color:#ecf7fb;font-weight: unset;font-size:12px;" ng-if="(acData2.engineType=='Voltage_Ignition')">Voltage+Ignition<span ng-if="acData2.engineType.includes('Voltage')"> ( {{acData2.voltageValue}} )</span></th>
                        <th colspan="2" style="background-color:#ecf7fb;font-weight: unset;font-size:12px;" ng-if="(acData2.engineType!='Voltage_Ignition')">{{acData2.engineType}}<span ng-if="acData2.engineType.includes('Voltage')"> ( {{acData2.voltageValue}} )</span></th>
                        <th  style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total_duration'); ?></th>
                        <th  style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{durVarTot}}</th>
                    </tr>
                    <tr style="text-align:center;" >
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.date_time'); ?></th>
                      <th ng-if="acData2.fromDateTimeUTC!==0" colspan="2" style="background-color:#f9f9f9;font-weight:unset;font-size:12px;">{{acData2.fromDateTimeUTC | date:'yyyy-MM-dd'}}&nbsp;&nbsp;&nbsp;{{ acData2.fromDateTimeUTC | date:'HH:mm:ss'}} </th>
                      <th ng-if="acData2.fromDateTimeUTC==0" colspan="2" style="background-color:#f9f9f9;font-weight:unset;font-size:12px;">-</th>
                      <th style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.date_time'); ?></th>
                      <th ng-if="acData2.toDateTimeUTC!==0" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{acData2.toDateTimeUTC | date:'yyyy-MM-dd'}}&nbsp;&nbsp;&nbsp;{{acData2.toDateTimeUTC | date:'HH:mm:ss'}}</th>
                      <th ng-if="acData2.toDateTimeUTC==0" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">-</th>
                    </tr>
                  </thead>
              </table>   

            <table ng-if="prmEngineShow" class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
                <thead>
                  <tr style="text-align:center">
                    <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.date_time'); ?></th>
                    <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;">{{reportTdNam}}</th>
                    <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.duration'); ?></th>
                    <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.address'); ?></th>
                    <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.gmap'); ?></th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr ng-repeat="ac in acData" style="text-align:center">
                   
                     <td ng-if="ac.ignitionStatus == 'ON' || ac.ignitionStatus == 'OFF'">{{ac.date | date:'yyyy-MM-dd'}}&nbsp;&nbsp;{{ac.date | date:'HH:mm:ss'}}</td>
                     <td ng-if="ac.ignitionStatus == 'ON' || ac.ignitionStatus == 'OFF'">{{ac.ignitionStatus}}</td>
                     <!-- <td rowspan="2" ng-if="ac.ignitionStatus == 'ON'">{{msToTime(acData[$index+1].date-ac.date)}}</td> -->
                      <td rowspan="2" style="padding-top: 36px;" ng-if="(ac.ignitionStatus == 'ON')&&($index+1<acData.length)">{{msToTime(acData[$index+1].date-ac.date)}}</td>
                     <td rowspan="2" style="padding-top: 36px;" ng-if="(ac.ignitionStatus == 'ON')&&($index+1==acData.length)">{{msToTime(todaymillisec-ac.date)}}</td>
                     <td ng-if="ac.ignitionStatus == 'ON' || ac.ignitionStatus == 'OFF'"><p>{{ac.address}}</p></td>
                     <td ng-if="ac.ignitionStatus == 'ON' || ac.ignitionStatus == 'OFF'"><a href="https://www.google.com/maps?q=loc:{{ac.latitude}},{{ac.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>

                  </tr> 
                  <tr ng-if="acData == null || acData == undefined || acData.length==0" align="center">
                    <td  colspan="8" class="err" ng-if="!errMsg"><h5><?php echo Lang::get('content.zero_records'); ?></h5></td>
                    <td colspan="8" class="err" ng-if="errMsg"><h5>{{errMsg}}</h5></td>
                  </tr>
              </table> 
            </div>

                       </div>
                    </div>
                </div>
            </div>

          </div>
        </div>

    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js"></script>
    <script src="../app/views/reports/customjs/html5csv.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
    <script src="../app/views/reports/customjs/FileSaver.js"></script>
    <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../app/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!--<script src="assets/js/naturalSortVersionDates.js"></script>-->
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/acReportNew.js?v=<?php echo Config::get('app.version');?>"></script>
    
    <script>

        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false,
                    maxDate: new Date,
                    minDate: new Date(2015, 12, 1)
                });
               
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

         var generatePDF = function() {
  kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
    var tab = getParameterByName('tn');
    console.log(tab);
    if( tab == "ac" ){
    kendo.drawing.pdf.saveAs(group, "AcReport.pdf");
  } else if( tab == "engine" ){
    console.log(tab);
     kendo.drawing.pdf.saveAs(group, "SecondaryEngineReport.pdf");
   }
   else if( tab == "prmEngine" ){
    console.log(tab);
     kendo.drawing.pdf.saveAs(group, "PrimaryEngineOnReport.pdf");
   }
   else if( tab == "drSensor" ){
    console.log(tab);
     kendo.drawing.pdf.saveAs(group, "Door_Sensor_Report.pdf");
   }

  });
}

// $("body").on("click", "#downloadPDF", function () {
//             html2canvas($('#table')[0], {
//                 onrendered: function (canvas) {
//                     var data = canvas.toDataURL();
//                     var docDefinition = {
//                         content: [{
//                             image: data,
//                             width: 500
//                         }]
//                     };
//                     pdfMake.createPdf(docDefinition).download("ConsolidateReport.pdf");
//                 }
//             });
//         });
 </script>
    
</body>
</html>
