<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">

<style>
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > tdt, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}

body{
font-family: 'Lato', sans-serif;
/*font-weight: bold;
font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
} 

.col-md-12 {
    width: 98% !important;
    left: 15px !important;
    padding-left: 20px !important;
}

/*
.chart {
    min-width: 320px;
    max-width: 800px;
    height: 280px;
    margin: 0 auto;
}

#container {
    width: 80%;
    height: unset !important; 
    max-width: 80%;
    max-height: unset !important; 
    min-height: 500px !important;
    margin-top: 10px !important;
}
*/
</style>

</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">
        <?php include('sidebarList.php');?> 
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default"></div>   
            </div>
        </div>
 
    <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: -20px;">
        <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                    <h3 class="box-title"><?php echo Lang::get('content.fuel_theft'); ?> <?php echo Lang::get('content.report'); ?></h3>
                </div>
               <div class="row">
                    <div class="col-md-2" align="center">
                        <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
                          <h5 style="color: grey;">{{shortNam}}</h5>
                        </div>
                        <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                          <h5 style="color: red;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.not_found'); ?></h5>
                        </div>
                  </div>
                    <?php include('dateTime.php');?>

                     <div class="col-md-1" align="center">
                        <button style="margin-left: -42%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                </div>

     <!-- </div> -->
          <div class="row">
              <div class="col-md-1" align="center"></div>
                <div class="col-md-2" align="center">
                  <div class="form-group"></div>
                </div>
              </div>
          </div>

        </div>

        <div class="col-md-12" style="border-color: unset!important;">
            <div class="box box-primary" style="min-height:570px;background-color:#fdfdfd;">
                <div>
                    <div class="pull-right" style="margin-top: 10px;margin-right: 5px;margin-bottom:10px;">
                        <img style="cursor: pointer;" ng-click="exportData('fuelTheftReport')"  src="../app/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('fuelTheftReport')"  src="../app/views/reports/image/csv.jpeg" />
                    </div>
           
          <div class="box-body">
            
              <div style="margin-top: 20px;" id="fuelTheftReport">
                <table class="table table-bordered table-striped table-condensed table-hover">
                  <thead>
                    <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
                      <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(gName)}}</th>
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle_id'); ?></th>
                      <th ng-if="fuelData.vehicleId!=null" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.vehicleId}}</th>   
                      <th ng-if="fuelData.vehicleId==null" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{vehIds}}</th>   
                    </tr>
                     <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.veh_name'); ?></th>
                      <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{shortNam}}</th>
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_theft_fuel'); ?><?php echo Lang::get('content.ltrs'); ?></th>
                      <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{roundOff(fuelTheft.totalTheftFuel)}}</th>   
                    </tr>
                  </thead>
              </table>

              <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
                <thead>
                  <tr style="text-align:center">
                    <th width="10%" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.start_time'); ?></th>
                    <th width="10%" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.end_time'); ?></th>
                    <th width="10%" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.pre_fuel'); ?><?php echo Lang::get('content.ltrs'); ?></th>
                    <th width="8%"  style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.cur_fuel'); ?><?php echo Lang::get('content.ltrs'); ?></th>
                    <th width="8%"  style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.theft_fuel'); ?><?php echo Lang::get('content.ltrs'); ?></th>
                    <th width="10%" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.loc'); ?></th>
                    <th width="10%" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.gmap'); ?></th>
                  </tr>
                  </thead>

                  <tbody ng-repeat="fuel in fuelTheft.getData">
                  <tr>
                    <!-- <td>{{fuel.date | date:'HH:mm:ss'}}&nbsp;&nbsp;&nbsp;{{fuel.date | date:'dd-MM-yyyy'}}</td> -->
                    <td>{{fuel.startDate | date:'HH:mm:ss'}}&nbsp;&nbsp;&nbsp;{{fuel.startDate | date:'dd-MM-yyyy'}}</td>
                    <td>{{fuel.date | date:'HH:mm:ss'}}&nbsp;&nbsp;&nbsp;{{fuel.date | date:'dd-MM-yyyy'}}</td>
                    <td>{{fuel.prevLitr}}</td>
                    <td>{{fuel.currLitr}}</td>
                    <td>{{fuel.theftLitr}}</td>
                    <td>{{fuel.theftLoc}}</td>  
                    <td> <a href="https://www.google.com/maps?q=loc:{{fuel.latitude}},{{fuel.longitude}}" target="_blank">Link</a> </td>            
                  </tr> 
                </tbody>

                  <tr align="center">
                      <td ng-if="fuelTheft.getData == null" colspan="7" class="err" style="text-align: center;">
                           <h5><?php echo Lang::get('content.no_data'); ?></h5>
                      </td>
                  </tr>
              </table>   
            </div>

            </div>
           </div>
          
          </div>
        </div>

      </div>
   </div>

    <script src="assets/js/static.js"></script>
<!--<script src="assets/js/jquery-1.11.0.js"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!--<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>-->
<!--<script src="assets/js/bootstrap.min.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js"></script>
    <script src="../app/views/reports/customjs/html5csv.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
    <script src="../app/views/reports/customjs/FileSaver.js"></script>
    <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../app/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
<!--<script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>-->
    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!--<script src="assets/js/naturalSortVersionDates.js"></script>-->
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/fuelTheft.js"></script>
    
    <script>

        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false,
                    maxDate: new Date,
                    minDate: new Date(2015, 12, 1)
                });
               
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

 </script>
    
 </body>
</html>
