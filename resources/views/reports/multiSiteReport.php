<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
<style>
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}
body{
font-family: 'Lato', sans-serif;
/*font-weight: bold;*/  

/* font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
} 
</style>
</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div id="wrapper" ng-controller="mainCtrl" class="ng-cloak">
        <?php include('sidebarList.php');?>
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>
 
    <!-- AdminLTE css box-->

    <div class="col-md-12">
       <div class="box box-primary">
        
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                    <h3 class="box-title">{{ caption | translate }}</h3>
                </div>
                <div class="row">
                    <!-- <div class="col-md-1" align="center"></div> -->
                    <div class="col-md-2" align="center">
                        <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
                          <h5 style="color: grey;">{{shortNam}}</h5>
                        </div>
                     
                  </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dateFrom"  placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromtime" class="form-control placholdercolor" id="timeFrom" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.totime" class="form-control placholdercolor" id="timeTo" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                            </div>
                        </div>
                    </div>
                   <!--  <div class="col-md-1" align="center">
                        <div class="form-group">
                            
                              
                           
                        </div>
                    </div> -->
                    <!-- <div class="col-md-1" align="center"></div> -->
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <!-- <div class="input-group"> -->
                              <!--  <select class="input-sm form-control" ng-model="interval">
                                     <option value="">Interval</option>
                                     <option label="1 mins">1</option>
                                     <option label="2 mins">2</option>
                                     <option label="5 mins">5</option>
                                     <option label="10 mins">10</option>
                                     <option label="15 mins">15</option>
                                     <option label="30 mins">30</option>
                                </select> -->
                            <!-- </div> -->
                        </div>
                        
                    </div>
                     
                </div>

              <div class="row">
                    <div class="col-md-2" align="center"></div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <!-- <div class="input-group"> -->
                                <select class="input-sm form-control" ng-model="_site1" ng-options="site.siteName for site in siteName | orderBy: 'siteName':false">
                                    <option value=""><?php echo Lang::get('content.select_sites'); ?></option>
                                </select>
                            <!-- </div> -->
                        </div>
                        
                    </div>
                    <!-- <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromtime" class="form-control placholdercolor" id="timeFrom" placeholder="From time">
                                <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="From date">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div> -->
                   <!--  <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.totime" class="form-control placholdercolor" id="timeTo" placeholder="From time">
                                <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            
                                <select class="input-sm form-control" ng-model="_site2" ng-options="sites.siteName for sites in siteName | orderBy: 'siteName':false">
                                    <option value=""><?php echo Lang::get('content.select_sites'); ?></option>
                                </select>
                        </div>
                    </div>
                    <div class="col-md-2" align="center" ng-hide="hideShow">
                        <div class="form-group">
                            
                                <select class="input-sm form-control" ng-model="interval">
                                     <option value=""><?php echo Lang::get('content.interval'); ?></option>
                                     <option label="1 mins">1</option>
                                     <option label="2 mins">2</option>
                                     <option label="5 mins">5</option>
                                     <option label="10 mins">10</option>
                                     <option label="15 mins">15</option>
                                     <option label="30 mins">30</option>
                                </select>
                           
                        </div>
                    </div>
                    <!-- <div class="col-md-1" align="center"></div> -->
                    <div class="col-md-2" align="center">
                    <div class="form-group">
                        <button class="btn btn-primary" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                        
                    </div>
                </div>
            </div>
            
          
        </div>

        <div class="col-md-12" ng-hide="hideShow">
            <div class="box box-primary">
                <div>
                    <div class="pull-right">
                        <img style="cursor: pointer;" ng-click="exportData('multipleSite')"  src="../app/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('multipleSite')"  src="../app/views/reports/image/csv.jpeg" />


                      
                    
                    </div>
                    <div class="box-body" id="multipleSite">
                <div class="empty" align="center"></div> <p style="margin:0" class="page-header">{{ caption | translate }} <span style="float: right;"><?php echo Lang::get('content.from'); ?> : {{uiDate.fromdate}} {{uiDate.fromtime}} - <?php echo Lang::get('content.to'); ?> : {{uiDate.todate}} {{uiDate.totime}}</span></p> 
                            
                                <table class="table table-bordered table-striped table-condensed table-hover">
                                    <thead>
                                        <tr style="text-align:center; font-weight: bold;">
                                            <td>{{ vehiLabel | translate }} <?php echo Lang::get('content.group'); ?></td>
                                            <td  colspan="2">{{uiGroup}}</td>
                                            <td>{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></td>
                                            <td>{{shortNam}}</td>   
                                        </tr>
                                        <tr><td colspan="5"></td></tr>
                                        <tr style="text-align:center;font-weight: bold;">
                                            <th class="id" custom-sort order="'startTime'" sort="sort" style="text-align:center;" width="15%"><?php echo Lang::get('content.date_time'); ?></th>
                                            <!-- <th class="id" custom-sort order="'distanse'" sort="sort" style="text-align:center;" width="15%">Distance</th> -->
                                            <th class="id" custom-sort order="'tripDistance'" sort="sort" style="text-align:center;" width="15%"><?php echo Lang::get('content.tripdistance'); ?></th>
                                            <th class="id" custom-sort order="'odoDistance'" sort="sort" style="text-align:center;" width="15%"><?php echo Lang::get('content.odo_meter'); ?></th>
                                            <th class="id" custom-sort order="'duration'" sort="sort" style="text-align:center;" width="15%"><?php echo Lang::get('content.duration'); ?> (h:m:s)</th>
                                            <th class="id" custom-sort order="'address'" sort="sort" style="text-align: center;" width="35%"><?php echo Lang::get('content.sitename'); ?></th>
                                            <!-- <th class="id" style="text-align: center;" width="20%">G-Map</th> -->
                                        </tr>
                                </thead>
                                <tbody>
                                    <tr class="active" style="text-align:center" ng-repeat="multipleSite in siteData  track by $index| orderBy:sort.sortingOrder:sort.reverse">
                                        <td>{{multipleSite.startTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                        <!-- <td>{{ignition.distanse}}</td> -->
                                        <td>{{multipleSite.tripDistance}}</td>
                                        <td>{{multipleSite.odoDistance}}</td>
                                        <td>
                                        {{msToTime(multipleSite.duration)}}
                                        </td>
                                        <td>
                                        {{multipleSite.address}} 
                                            <!-- <p ng-if="ignitionignitionignition.address!=null">{{ignition.address}}</p>
                                            <p ng-if="ignitionignition.address==null && addressFuel[$index]!=null">{{addressFuel[$index]}}</p>
                                            <p ng-if="ignition.address==null && addressFuel[$index]==null"><img src="assets/imgs/loader.gif" align="middle"></p> -->
                                        </td> 
                                        <!-- <td><a href="https://www.google.com/maps?q=loc:{{ignition.latitude}},{{ignition.longitude}}" target="_blank">Link</a></td> -->
                                    </tr>
                                    <tr ng-if="siteData==null || siteData.length==0"  align="center">
                                        <td colspan="5"class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>


            <div class="col-md-12" ng-show="hideShow">
            <div class="box box-primary">
                <div>
                    <div class="pull-right" style="margin-top: 1%;">
                    <button type="button" class="btn btn-success" ng-click="durationFilter('today')" ng-disabled="todayDisabled">Today</button>
                      <button type="button" class="btn btn-primary" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled">Yesterday</button>
                      <button type="button" class="btn btn-success" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled">Last Week</button>
                      <button type="button" class="btn btn-info" ng-click="durationFilter('month')" ng-disabled="monthDisabled">Month</button>
                      
                        <img style="cursor: pointer;" ng-click="exportData('siteTrip')"  src="../app/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('siteTrip')"  src="../app/views/reports/image/csv.jpeg" />
                        <img style="cursor: pointer;" onclick="generatePDF()"  src="../app/views/reports/image/Adobe.png" />


                      
                    
                    </div>
                    <div class="col-md-12" >
                    <div class="box-body" id="siteTrip">
                <div class="empty" align="center"></div> <p style="margin:0" class="page-header">{{ caption | translate }} <span style="float: right;"><?php echo Lang::get('content.from'); ?> : {{uiDate.fromdate}} {{uiDate.fromtime}} - <?php echo Lang::get('content.to'); ?> : {{uiDate.todate}} {{uiDate.totime}}</span></p> 
                            <div id="formConfirmation">
                                <table class="table table-bordered table-striped table-condensed table-hover">
                                     <thead>
                                        <tr style="text-align:center; font-weight: bold;">
                                            <th style="background-color:#ecf7fb;">{{ vehiLabel | translate }} <?php echo Lang::get('content.group'); ?></th>
                                            <th colspan="3" style="background-color:#f9f9f9;">{{uiGroup}}</th>
                                            <th style="background-color:#ecf7fb;">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>
                                            <th colspan="4" style="background-color:#f9f9f9;">{{shortNam}}</th> 
                                            <!-- <th>Trip Count</th>
                                            <th colspan="2">{{siteData.tripCount}}</th> -->
                                          </tr>
                                        </thead>
                                    </table>
                                <div class="col-md-12" style="height: 8px;"></div>
                                    <table class="table table-bordered table-striped table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th style="background-color:#ecf7fb;"><?php echo Lang::get('content.trip_start'); ?></th>
                                            <th ng-if="siteData.endTime==0" colspan="2" style="background-color:#f9f9f9;">-</th>
                                            <th ng-if="siteData.endTime!=0" colspan="2" style="background-color:#f9f9f9;">{{siteData.startTime | date:'yyyy-MM-dd HH:mm:ss' }}</th>
                                            <th style="background-color:#ecf7fb;"><?php echo Lang::get('content.trip_end'); ?></th>
                                            <th ng-if="siteData.endTime==0" colspan="2" style="background-color:#f9f9f9;">-</th>
                                            <th ng-if="siteData.endTime!=0" colspan="2" style="background-color:#f9f9f9;">{{siteData.endTime | date:'yyyy-MM-dd HH:mm:ss'}}</th>
                                            <th style="background-color:#ecf7fb;"><?php echo Lang::get('content.total_trip'); ?></th>
                                            <th colspan="2" style="background-color:#f9f9f9;">{{siteData.tripCount}}</th>
                                        </tr>
                                        </thead>
                                    </table>
                                <div class="col-md-12" style="height: 18px;"></div>
                                    <table class="table table-bordered table-striped table-condensed table-hover">
                                      <thead>
                                        <tr style="text-align:center;font-weight: bold;">
                                            <th width="15%" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.start_loc'); ?></th>
                                            <th width="10%" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.start_time'); ?></th>                                              
                                            <th width="15%" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.end_loc'); ?></th>
                                            <th width="10%" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.end_time'); ?></th>
                                            <th width="10%" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.duration'); ?> (h:m:s)</th>
                                            <th width="10%" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.dist_cov'); ?></th>
                                            <th width="10%" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.moving'); ?> (h:m:s)</th>
                                            <th width="10%" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.stoppage'); ?> (h:m:s)</th>
                                            <th width="10%" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.avspd'); ?> (Kms)</th>
                                            <th width="10%" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.speed'); ?> <?php echo Lang::get('content.violations'); ?></th>
                                        </tr>
                                     </thead>
                                  <tbody ng-repeat="multipleSite in siteData.history  track by $index | orderBy:sort.sortingOrder:sort.reverse">
                                    <tr class="active" style="text-align:center" ng-repeat="multiple in multipleSite" ng-if="$even">
                                        <td ng-if-start="multiple.state != 'TST'">{{multiple.state | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                        <td>{{multiple.startTime | date:'yyyy-MM-dd HH:mm:ss'}}</td> 
                                        <td>{{multipleSite[$index+1].state}}</td>
                                        <td>{{multipleSite[$index+1].endTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                        <td>
                                            <div ng-if="multipleSite[$index+1].detentionTime == 0">{{multipleSite[$index+1].detentionTime}}</div>
                                            {{msToTime(multipleSite[$index+1].detentionTime)}}
                                        </td>
                                        <td>{{multipleSite[$index+1].distanceCovered}}</td>
                                        <td>
                                            <div ng-if="multipleSite[$index+1].runningTime == 0">{{multipleSite[$index+1].runningTime}}</div>
                                            {{msToTime(multipleSite[$index+1].runningTime)}}
                                        </td>
                                        <td>
                                            <div ng-if="multipleSite[$index+1].stoppageTime == 0">{{multipleSite[$index+1].stoppageTime}}</div>
                                            {{msToTime(multipleSite[$index+1].stoppageTime)}}
                                        </td>
                                        <td> {{multipleSite[$index+1].avgSpeed}} </td>
                                        <td ng-if-end> {{multipleSite[$index+1].speedViolation}} </td>
                                        <td colspan="2" ng-if-start="multiple.state == 'TST'" class="bg table-success"><?php echo Lang::get('content.summary'); ?></td>
                                        <td colspan="2" class="bg table-success"><?php echo Lang::get('content.total_count'); ?></td>
                                        <td colspan="2" class="bg table-success">{{multiple.tripCount}}</td>
                                        <td colspan="2" class="bg table-success"><?php echo Lang::get('content.total_distance'); ?></td>
                                        <td colspan="2" ng-if-end class="bg table-success">{{multiple.singleTripdistance}} (Kms)</td>
                                    </tr>
                                   
                                </tbody>
                                 <tr align="center" ng-if="siteData.history.length == 0 || siteData.history == null" ng-show="mSiteError&&!errMsg">
                                    <td colspan="10" class="err" ng-if="!errMsg"><h5><?php echo Lang::get('content.no_data'); ?> {{siteData.history.length}}</h5></td>
                                    <td colspan="10" class="err" ng-if="errMsg"><h5>{{errMsg}}</h5></td>
                                </tr>
                                 <tr align="center"  ng-if="errMsg">
                                    <td colspan="10" class="err"><h5>{{errMsg}}</h5></td>
                                </tr>
                            </table>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

        </div>

    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="assets/js/ui-bootstrap-0.6.0.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places" type="text/javascript"></script>
    <script src="../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js"></script>
    <script src="../app/views/reports/customjs/html5csv.js"></script>
    <script src="../app/views/reports/customjs/FileSaver.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
    <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../app/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/siteReport.js?v=<?php echo Config::get('app.version');?>"></script>
    <script>

        // $("#example1").dataTable();
        // $("#menu-toggle").click(function(e) {
        //     e.preventDefault();
        //     $("#wrapper").toggleClass("toggled");
        // });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

var generatePDF = function() {
  kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
    kendo.drawing.pdf.saveAs(group, "SiteTripReport.pdf");
  });
}

  </script>
    
</body>
</html>
