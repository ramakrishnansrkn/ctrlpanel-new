<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>

<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">

<style>
body {
      font-family: 'Lato', sans-serif;
   /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
  }
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}

</style>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134334975-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134334975-1');
</script>
</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app = "mapApp">
    <div ng-controller = "mainCtrl" class="ng-cloak">
        <div id="wrapper">
            <?php include('sidebarList.php');?>
            <div id="testLoad"></div>

<!--<div ng-show="reportBanShow" class="modal fade" id="allReport" role="dialog" style="top: 100px">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-body">
                  <p class="err" style="text-align: center;"> You are not a premium user !... </p>
              </div>
          </div>
      </div>
      </div> -->
                                 

<!--         <div class="box-tools pull-right">
                               <span class="pull-right"><div class="btn-group" style="margin-top: -85px;margin-right:10px">
                                <a class="btn btn-circle green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="margin-bottom: 10px"> 
                                                    <i class="icon-home"></i> {{trimColon(gName)}}
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                
                                                <ul class="dropdown-menu pull-right" role="menu" >
                                                    
                                                    <li class="nav-item start" ng-class="(grp.grpId == groupid)?'active':''" ng-repeat="grp in group_list | filter:grpName"  ng-click="groupSelection(grp.grpName, grp.grpId)">
                                        <a href="javascript:void(0);" class="nav-link " >
                                          <span class="title" style="overflow: hidden;text-overflow: ellipsis;"  ng-bind="grp.grpName"></span>
                                        </a>
                                                 </li>
                                             </ul>
                                            </div>
                                        </span>        
          </div> -->


         <!--  <div class="box-tools pull-right">
                               <span class="pull-right"><div class="btn-group" style="margin-top: -96px;margin-right: 0px">
                                <a class="btn btn-circle green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
                                                    <i class="fa fa-car"></i> {{shortNam}}
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                            <ul class="dropdown-menu pull-right" role="menu" >
                                <li ng-repeat="loc in vehicle_list[groupid].vehicleLocations | orderBy:natural('shortName')" ng-class="{active:vehiname ==loc.vehicleId}"><a href="javascript:void(0);" ng-class="{red:loc.status=='OFF'}" ng-click="genericFunction(loc.vehicleId, $index)" ng-clotnak>
                                <img ng-src="assets/imgs/sideMarker/@{{loc.vehicleType}}_@{{loc.color}}.png" fall-back-src="assets/imgs/Car_P.png" width="16" height="16"/>
                                <span>&nbsp;&nbsp;&nbsp;{{loc.shortName}}</span></a>
                               </li>
                            </ul>
                        </div>
                    </span>        
                </div>
                 <div class="box-tools pull-right">
                               <span class="pull-right"><div class="btn-group" style="margin-top: -85px;margin-right:10px">
                                <a class="btn btn-circle green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="margin-bottom: 10px"> 
                                                    <i class="icon-home"></i> {{trimColon(gName)}}
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                
                                                <ul class="dropdown-menu pull-right" role="menu" >
                                                    
                                                    <li class="nav-item start" ng-class="(grp.grpId == groupid)?'active':''" ng-repeat="grp in group_list | filter:grpName"  ng-click="groupSelection(grp.grpName, grp.grpId)">
                                        <a href="javascript:void(0);" class="nav-link " >
                                          <span class="title" style="overflow: hidden;text-overflow: ellipsis;"  ng-bind="grp.grpName"></span>
                                        </a>
                                                 </li>
                                             </ul>
                                            </div>
                                        </span>        
                    </div> -->


           <div class="row">
                            <div class="col-lg-12 col-xs-12 col-sm-12">
                             
                              <div class="portlet light " style="margin-top: -35px !important;">
                                   
   <!-- view fms -->  
   <div class="col-lg-6 col-xs-12 col-sm-12" style="margin-top: 70px;
    width: 50%;">
                                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                    <h3 class="box-title" style="margin-left: 42px;"><?php echo Lang::get('content.FMS_report'); ?></h3>
                </div>
                                <ul class="nav nav-tabs " style="margin-left: 35px">
                                  <li id="fleet">
                                    <a href="#tab_1_1" data-toggle="tab"  active="tabactive"   ng-click="changeFMS('FleetManagement')" ><span class="caption-subject font-black-soft bold uppercase"><?php echo Lang::get('content.fleet_management'); ?></span> </a>
                                  </li>
                                  <li id="serv">
                                    <a href="#tab_1_1" data-toggle="tab"  active="tabactive"  ng-click="changeFMS('Services')"> <span class="caption-subject font-black-soft bold uppercase"><?php echo Lang::get('content.services'); ?></span> </a>  </li>
                                  <li id="remind">
                                    <a href="#tab_1_1" data-toggle="tab"  active="tabactive"   ng-click="changeFMS('Reminders')"><span class="caption-subject font-black-soft bold uppercase"><?php echo Lang::get('content.reminder'); ?></span></a>
                                  </li>
                                </ul>
                                
            <!-- <div class="portlet light bordered" > -->
            <!-- <div class="col-md-6 boxing_size" style="top: -15px;"> -->
               <h5 style="color: red">{{error2}}</h5>
                <div class="portlet-body" style="margin-left: 35px">
                  <div class="tab-content">
                  <div class="tab-pane active" id="tab_1_1">
                    <div class="box-tools" style="width: 7%;float: right;" ng-hide="reminder">
                            <span class="pull-right input-group-addon" style="margin-left: 5px;width: 100%" ng-click="addFMS(data.vehicleId)"><a class="glyphicon glyphicon-plus" style="text-decoration: none; cursor: pointer;"></a> </span>
                      </div>
                    <table class="table table-striped table-condensed table-hover">
                      <thead>
                            <tr style="text-align:center; font-weight: bold;font-size: 8px">
                                <td colspan="2"></td>
                                <!-- <td >Vehicle Name</td> -->
                                <td colspan="2">{{data.vehicleName}}</td> 
                                <!-- <td >Vehicle Id</td>
                                <td colspan="2">@{{data.vehicleId}}</td> -->   
                            </tr>
                            
                            <tr style="text-align:center;font-weight: bold;">
                                <th><?php echo Lang::get('content.document_type'); ?></th>
                                <th ng-if="(fms=='Services')"><?php echo Lang::get('content.date'); ?></th>
                                <th ng-if="(fms!='Services')"><?php echo Lang::get('content.date'); ?></th>
                                <th><?php echo Lang::get('content.company_name'); ?></th>
                                <th><?php echo Lang::get('content.description'); ?></th>
                                <th><?php echo Lang::get('content.amount'); ?></th>
                                <th ng-hide="reminder"><?php echo Lang::get('content.edit'); ?></th>
                                <th ng-hide="reminder"><?php echo Lang::get('content.delete'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="fmsData in fmsDataList" >
                                <td>{{fmsData.documentType}}</td>
                                <td>{{fmsData.dueDate }}</td>
                                <td>{{fmsData.companyName}}</td>
                                <td>{{fmsData.description}}</td>
                                <td>{{fmsData.amount}}</td>
                                <td ng-hide="reminder"><button class="btn btn-primary btn-xs" ng-click="editFMS(data.vehicleId,fmsData)"><span class="glyphicon glyphicon-floppy-disk"></span></button></td>
                                <td ng-hide="reminder"><button class="btn btn-danger btn-xs"  ng-click="deleteFMS(data.vehicleId,fmsData.documentType)"><span class="glyphicon glyphicon-trash"></span></button></td>
                            </tr>
                            <tr ng-if="fmsDataList =='' || fmsDataList.length==0 || fmsDataList == null" align="center">
                                <td colspan="7" class="err"><h5><?php echo Lang::get('content.no_data_found'); ?></h5></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>


      <!-- add fms -->
      <div class="col-lg-6 col-xs-12 col-sm-12" ng-show="addedit" style="float: right;margin-top: 84px;padding: 0 3%">
        <h5 style="color: red">{{error}}</h5>
            <div class="modal-content" style="height: 400px;">
               <div class="portlet-title">
                                        <div class="modal-header" style="background-color: #196481;color: #fff;">
                                            <div class="modal-group">
                                            <i class="icon-bar-chart font-dark hide"></i>
                                            <b><span class="caption-subject font-dark bold uppercase" ng-bind="action"></span></b>
                                            </div>
                                        </div>
                                        <div class="actions">
                                             <div  class="pull-right" style="margin-top: -42px" ><span class="input-group datecomp" style="margin-right: 45px; margin-top: 0px;" ng-show="(FMSoperation=='add'||FMSoperation=='save')">
                                                    <button type="button" class="btn btn-default " ng-click="updateFMS(data.vehicleId,'save','add')"><?php echo Lang::get('content.save'); ?></button></span>
                                               </div>
                                               <div  class="pull-right"style="margin-top: -42px;" ><span class="input-group datecomp" style="margin-right: 45px; margin-top: 0px;">
                                                    <button type="button" class="btn green " ng-click="updateFMS(data.vehicleId,'update','edit')" ng-show="(FMSoperation=='edit'||FMSoperation=='update')"><?php echo Lang::get('content.update'); ?></button></span>
                                               </div>
                                        </div>
                                      </div>
      <!-- <div class="col-md-6 boxing_size" ng-show="addedit" style="top: 10px;">   -->
          
          <div class="form-body" style="top: 10px;">
                <label class="col-md-3 control-label" style="margin-top: 2px"><?php echo Lang::get('content.document_type'); ?>
                                                </label>
                                                <div class="col-md-5 form-group" style="margin-top: 4px">
                                                    <select class="form-control" ng-model="documentType" placeholder="Select Type" name="type" ng-options="type.type for type in docTypeList" ng-change="changedValue()">
                                                      <option value="" ng-bind="(type)?type:'---Document Type---'">---documentType---</option>
                                                    </select> 
                                                </div>
                                                 <label class="col-md-2 control-label" ng-hide="Services" style="margin-left: -18px;margin-top: 2px"><?php echo Lang::get('content.month_interval'); ?>
                                                </label>
                                                <div class="col-md-2  form-group" style="margin-top: 3px">
                                                    <input type="text" class="form-control" placeholder="" ng-model="monInterval" readonly="readonly" ng-hide="Services">
                                                </div>
                                                
                                                <div class="col-md-12"></div>
                                                <label class="col-md-3 control-label" ng-hide="Services"><?php echo Lang::get('content.due_date'); ?> </label>
                                                <label class="col-md-3 control-label" ng-show="Services"><?php echo Lang::get('content.date'); ?> </label>
                                                <div class="col-md-3 form-group">
                                                  <div class="input-group datecomp">
                                                  <input type="text" ng-model="duedate" class="form-control placholdercolor" id="duedate" placeholder="Date" style="    width: 145px;">
                                                </div>
                                                </div>
                                                <div class="col-md-12"></div>
                                                <label class="col-md-3 control-label" ng-hide="Services"><?php echo Lang::get('content.notify_interval'); ?> </label>
                                                <label class="col-md-3 control-label" ng-show="Services"><?php echo Lang::get('content.odometer_due'); ?> </label>
                                                <div class="col-md-4 form-group">
                                                    <input type="text" class="form-control" id="dueInterval" placeholder="Notify Interval" ng-model="notifyInterval" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
                                                </div>
                                                <div class="col-md-12"></div>
                                                <label class="col-md-3 control-label"><?php echo Lang::get('content.company_name'); ?></label>
                                                <div class="col-md-8 form-group">
                                                    <input type="text" class="form-control" placeholder="Company Name" ng-model="companyName">
                                                </div>
                                                <div class="col-md-12"></div>
                                                <label class="col-md-3 control-label"><?php echo Lang::get('content.amount'); ?></label>
                                                <div class="col-md-4 form-group">
                                                    <input type="text" class="form-control" placeholder="Amount" ng-model="amount" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
                                                </div>
                                                <div class="col-md-12"></div>
                                              <label class="col-md-3 control-label" ><?php echo Lang::get('content.description'); ?></label>
                                              <div class="col-md-8 form-group">
                                              <textarea class="form-control form-control-solid placeholder-no-fix form-group"  rows="3" placeholder="Enter some text here" ng-model="desc"></textarea> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    
                </div>
              </div>
                 </div>
               </div>
<script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js"></script>
    <script src="../app/views/reports/customjs/html5csv.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
    <script src="../app/views/reports/customjs/FileSaver.js"></script>
    <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../app/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/fms.js"></script>

  <script>

   
        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $(document).ready(function(){
      var currentURL = window.location.href;
      console.log(currentURL);
    $("#bala5").addClass("active");
  
    });
      $('#duedate').datetimepicker({
                   format:'YYYY-MM-DD',
                   useCurrent:true,
                   pickTime: false,
                   minDate: new Date
               });

  </script>

</body>
</html>
