<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="Satheesh">
<title><?php echo Lang::get('content.gps'); ?></title>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="../app/views/reports/datepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="http://www.highcharts.com/joomla/media/com_demo/highslide.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
<!--  <link rel="stylesheet" href="/resources/demos/style.css">  -->
 
  <!-- popup code -->
<style type="text/css">
body {
	font-family: 'Lato', sans-serif;
	/*font-weight: bold;
	font-family: 'Lato', sans-serif;
	font-family: 'Roboto', sans-serif;
	font-family: 'Open Sans', sans-serif;
	font-family: 'Raleway', sans-serif;
	font-family: 'Faustina', serif;
	font-family: 'PT Sans', sans-serif;
	font-family: 'Ubuntu', sans-serif;
	font-family: 'Droid Sans', sans-serif;
	font-family: 'Source Sans Pro', sans-serif;
	*/
}
.ng-modal-overlay {
  /* A dark translucent div that covers the whole screen */
  position:absolute;
  z-index:9999;
  top:0;
  left:0;
  width:100%;
  height:100%;
  background-color:grey;
  opacity: 0.8;
}
.ng-modal-dialog {
  /* A centered div above the overlay with a box shadow. */
  z-index:10000;
  position: absolute;
  width: 50%; /* Default */

  /* Center the dialog */
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);

  background-color: #fff;
   box-shadow: 4px 4px 80px #000; -->
}
.ng-modal-dialog-content {
  padding:10px;
  text-align: left;
}
.ng-modal-close {
  position: absolute;
  top: 3px;
  right: 5px;
  padding: 5px;
  cursor: pointer;
  font-size: 120%;
  display: inline-block;
  font-weight: bold;
  font-family: 'arial', 'sans-serif';
}
.modal-body {
    max-height: calc(100vh - 210px);
    overflow-y: auto;
}

</style>

</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>
<body ng-app="mapApp">
	<div id="wrapper" ng-controller="mainCtrl" class="ng-cloak">
        <?php include('sidebarList.php');?>

        <div id="page-content-wrapper">
            <div class="container-fluid">
            	<div class="panel panel-default">
            	</div>   		
        	</div>
       	</div>
		
		<!-- loading menu -->
		<div id="testLoad"></div>

		<div class="col-md-12">
	       	<div class="box box-primary" ng-show="monthly">
				<div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
		            <h3 class="box-title">Fuel Performance Report</h3>
		        </div>

			    <div class="row">
					<div class="col-md-1" align="center"></div>
	    			<div class="col-md-2" align="center">
	                	<div class="form-group">
							<div class="input-group datecomp">
								<input type="text" ng-model="fromdate" class="form-control placholdercolor" id="dateFrom" placeholder="From date">
								<!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
							</div>
	                  	</div>
	                </div>
	                <div class="col-md-1" align="center"></div>
	                 <div class="col-md-2" align="center">
	                    <button ng-click = "submitButton()" style="margin-left: -100%; padding : 5px"><?php echo Lang::get('content.submit'); ?></button>
	                </div>
	            </div>
	        </div>
    <!--Group chart-->
      <div>
        <!-- <div id="container1"></div>
      </div>
    
      <div ng-hide="group"> -->
        <div id="container" ng-hide="donut"></div>
      </div>



      <hr>
    <div class="box-body" ng-class="overallEnable?'col-md-9':'col-md-12'" id="statusreport">

        <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">
          <!-- <button type="button" class="btn btn-info" ng-click="durationFilter('month')" ng-disabled="monthDisabled">Last Month</button>  -->
            <img style="cursor: pointer;" ng-click="exportData(dowloadId)"  src="../app/views/reports/image/xls.png" />
                <img style="cursor: pointer;" ng-click="exportDataCSV(dowloadId)"  src="../app/views/reports/image/csv.jpeg" />
                <img style="cursor: pointer;" onclick="generatePDF()"  src="../app/views/reports/image/Adobe.png" />
            </div>
    
      <!-- <h4 style="margin:0" class="page-header"></h4> -->
          <div id="{{dowloadId}}">
            <div id="formConfirmation">
        <table id="example" class="table table-striped table-bordered">
        <tr style="text-align:center">
        <th width="10%" class="id" custom-sort order="'vehicleName'" sort="sort"   style="text-align:center;background-color:#d3e0f1;font-size:12px;">{{vehiLabel}} <?php echo Lang::get('content.name'); ?></th>
        <th width="5%" class="id" custom-sort order="'vehicleId'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.vehicle_id'); ?></th>
        <th width="5%" class="id" custom-sort order="'kmpl'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.kmpl'); ?></th>
        <th width="5%" class="id" custom-sort order="'fuelConsumption'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.fuel_consumption'); ?></th>
        <th width="5%" class="id" custom-sort order="'distance'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.distance'); ?></th>
        </tr>
          <tbody>
          <tr ng-if="fuelConFuelData[0].error=='-'" ng-repeat="data in fuelConFuelData  | orderBy:natural(sort.sortingOrder):sort.reverse"  style="text-align:center" >
            <td>{{data.vehicleName}}</td>
            <td>{{data.vehicleId}}</td>
            <td>{{data.kmpl}}</td>
            <td>{{data.fuelConsumption}}</td>
            <td>{{data.distance}}</td>
          </tr>
          </tbody>
          <tr ng-if="fuelConFuelData.length==0" align="center">
            <td colspan="5" class="err" ng-if="!error"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>

          </tr>
          <tr ng-if="fuelConFuelData[0].error!=null" style="text-align: center">
            <td colspan="20" class="err"><h5>{{fuelConFuelData[0].error}}</h5></td>
          </tr>
        </table>
      </div>
        </div>
        <!-- <button type="button" class="btn btn-info btn-lg" data-target="#myModal" data-toggle="modal">Open Modal</button> -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">{{titleName}} :{{id}}</h4>
                </div>
                <div class="modal-body">
                  <table class="table table-striped table-bordered table-condensed table-hover">
            <tr style="text-align:center">
              <th style="text-align:center;" colspan="4"><?php echo Lang::get('content.speed_analysis'); ?></th>
            </tr>
            <tr style="text-align:center">
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.status'); ?></th>
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.count'); ?></th>
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
            </tr>
            <tr style="text-align:center" class="active">
              <td><?php echo Lang::get('content.excellent'); ?></td>
              <td>{{excellentCount}}</td>
              <td>{{excellentSpeed}}</td>
            </tr>
            <tr style="text-align:center" class="active">
              <td><?php echo Lang::get('content.nest'); ?></td>
              <td>{{bestCount}}</td>
              <td>{{bestSpeed}}</td>
            </tr>
            <tr style="text-align:center" class="active">
              <td><?php echo Lang::get('content.average'); ?></td>
              <td>{{averageCount}}</td>
              <td>{{averageSpeed}}</td>
            </tr>
            <tr style="text-align:center" class="active">
              <td><?php echo Lang::get('content.aggressive'); ?></td>
              <td>{{worstCount}}</td>
              <td>{{worstSpeed}}</td>
            </tr>
            <tr style="text-align:center" class="active">
              <td><?php echo Lang::get('content.redliner'); ?></td>
              <td>{{redlinerCount}}</td>
              <td>{{redlinerSpeed}}</td>
            </tr>
          </table>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Lang::get('content.close'); ?></button>
                </div>
              </div>
              
            </div>
        </div>

        <div class="modal fade" id="myModal1" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">{{titleName}} :{{id}}</h4>
                </div>
                <div class="modal-body">
                  <table class="table table-striped table-bordered table-condensed table-hover">
            <tr style="text-align:center">
              <th style="text-align:center;" colspan="4"><?php echo Lang::get('content.normal'); ?> : {{normal}}</th>
            </tr>
            <tr style="text-align:center">
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
              <th width="10%" style="text-align:center;">{{high}}</th>
              <th width="10%" style="text-align:center;">{{low}}</th>
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
            </tr>
            <tr ng-repeat="info in Values" style="text-align:center" class="active">
              <td><a href="https://www.google.com/maps?q=loc:{{info.latitude}},{{info.longutide}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
              <td>{{info.speed1}}</td>
              <td>{{info.slow}}</td>
              <td>{{info.time | date:"MM/dd/yyyy 'at' h:mma"}}</td>
            </tr>
            <tr style="text-align:center">
              <th style="text-align:center;" colspan="4"><?php echo Lang::get('content.aggressive'); ?> : {{aggressiveCount}}</th>
            </tr>
            <tr style="text-align:center">
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
              <th width="10%" style="text-align:center;">{{high}}</th>
              <th width="10%" style="text-align:center;">{{low}}</th>
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
            </tr>
            <tr ng-repeat="info in aggressive" style="text-align:center" class="active">
              <td><a href="https://www.google.com/maps?q=loc:{{info.latitude}},{{info.longutide}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
              <td>{{info.speed1}}</td>
              <td>{{info.slow}}</td>
              <td>{{info.time | date:"MM/dd/yyyy 'at' h:mma"}}</td>
            </tr>
            <tr style="text-align:center">
              <th style="text-align:center;" colspan="4"><?php echo Lang::get('content.harsh'); ?> : {{harshCount}}</th>
            </tr>
            <tr style="text-align:center">
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
              <th width="10%" style="text-align:center;">{{high}}</th>
              <th width="10%" style="text-align:center;">{{low}}</th>
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
            </tr>
            <tr ng-repeat="info in harsh" style="text-align:center" class="active">
              <td><a href="https://www.google.com/maps?q=loc:{{info.latitude}},{{info.longutide}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
              <td>{{info.speed1}}</td>
              <td>{{info.slow}}</td>
              <td>{{info.time | date:"MM/dd/yyyy 'at' h:mma"}}</td>
            </tr>
            <tr style="text-align:center">
              <th style="text-align:center;" colspan="4"><?php echo Lang::get('content.very_harsh'); ?> : {{veryHarshCount}}</th>
            </tr>
            <tr style="text-align:center">
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
              <th width="10%" style="text-align:center;">{{high}}</th>
              <th width="10%" style="text-align:center;">{{low}}</th>
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
            </tr>
            <tr ng-repeat="info in veryharsh" style="text-align:center" class="active">
              <td><a href="https://www.google.com/maps?q=loc:{{info.latitude}},{{info.longutide}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
              <td>{{info.speed1}}</td>
              <td>{{info.slow}}</td>
              <td>{{info.time | date:"MM/dd/yyyy 'at' h:mma"}}</td>
            </tr>
            <tr style="text-align:center">
              <th style="text-align:center;" colspan="4"><?php echo Lang::get('content.worst'); ?> : {{worstCount}}</th>
            </tr>
            <tr style="text-align:center">
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
              <th width="10%" style="text-align:center;">{{high}}</th>
              <th width="10%" style="text-align:center;">{{low}}</th>
              <th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
            </tr>
            <tr ng-repeat="info in worst" style="text-align:center" class="active">
              <td><a href="https://www.google.com/maps?q=loc:{{info.latitude}},{{info.longutide}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
              <td>{{info.speed1}}</td>
              <td>{{info.slow}}</td>
              <td>{{info.time | date:"MM/dd/yyyy 'at' h:mma"}}</td>
            </tr>
          </table>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Lang::get('content.close'); ?></button>
                </div>
              </div>
              
            </div>
        </div>
      </div>
    </div>

    </div>
  </div>
  <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> 
  <script src="assets/js/ui-bootstrap-tpls-0.13.3.js"></script>
  <script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
  <script src="assets/js/angular-ui-bootstrap-modal.js"></script>
  <script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js"></script>
  <script src="../app/views/reports/customjs/FileSaver.js"></script>
  <script src="../app/views/reports/customjs/html5csv.js"></script>
  <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
  <script src="assets/js/infobubble.js" type="text/javascript"></script>
    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
    <!-- <script src="assets/js/naturalSortVersionDates.js"></script>-->
    <script src="assets/js/static.js"></script>
    <script src="assets/js/vamoApp.js"></script>
  <script src="assets/js/highcharts-bar.js"></script>
  <script src="assets/js/services.js"></script>
    <script src="assets/js/fuelperformance.js?v=<?php echo Config::get('app.version');?>"></script>
  <script type="text/javascript">
  // For demo to fit into DataTables site builder...
  $('#example')
    .removeClass( 'display' )
    .addClass('table table-striped table-bordered');
  </script>
  <script>
    $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
      });

    var generatePDF = function() {
  kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
    kendo.drawing.pdf.saveAs(group, "FuelPerformanceReport.pdf");
  });
}

      function getTodayDate(date) {
          var date = new Date(date);
       return date.getFullYear()+'/'+("0" + (date.getMonth() + 1)).slice(-2)+'/'+("0" + (date.getDate())).slice(-2);
         }
      
      $(function () {

          $('#dateFrom').datetimepicker({
        pickTime: false,
        format: "MM,YYYY",
        viewMode: "months", 
        minViewMode: "months"
      });

      var dateObj    =  new Date();
        var fromNowTSS   =  new Date(dateObj.setDate(dateObj.getDate()-1));
      //console.log(getTodayDate(fromNowTSS));
        var fromNowTimes =  getTodayDate(fromNowTSS);

          $('#datef').datetimepicker({
        format:'YYYY-MM-DD',
        useCurrent:true,
        pickTime: false,
        defaultDate: fromNowTimes,
      });

    /*  $('#timeFrom').datetimepicker({
        pickDate: false
      });
      $('#timeTo').datetimepicker({
        useCurrent:true,
        pickDate: false
      }); */
      });      
  </script>
  
</body>
</html>

