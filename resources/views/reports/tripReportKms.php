<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link rel="stylesheet" href="assets/css/popup.bootstrap.min.css">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
<style>
<style>
body{
font-family: 'Lato', sans-serif;
/*font-weight: bold;*/  
/* font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
}
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}

.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}
</style>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134334975-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134334975-1');
</script>
</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div id="wrapper" ng-controller="mainCtrl" class="ng-cloak">
        
        <?php include('sidebarList.php');?> 
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>
 
    <!-- AdminLTE css box-->

    <div class="col-md-12">
       <div class="box box-primary">
        <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                    <h3 class="box-title"><?php echo Lang::get('content.trip_summary'); ?></h3>
                </div>
                <div class="row">
                   <!-- <div class="col-md-1" align="center"></div>-->
                    <div class="col-md-2" align="center">
                                    <div class="form-group" >
                                        <h5 style="color: grey;">{{shortNam}}</h5>
                                    </div>
                                   <!-- <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                                        <h5 style="color: red;">{{ vehiLabel | translate }} not found </h5>
                                    </div> -->
                    </div>
                    <?php include('dateTime.php');?>
                    <div class="col-md-1" align="center"></div>
                     <div class="col-md-1" align="center">
                        <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                </div>

              <!--  </div> -->
            </div>
            
        </div>

        <div class="col-md-12">
            <div class="box box-primary">
                <div style="overflow-x: auto;">
                    <div class="col-md-12">
                    <div class="pull-right" style="margin-top: 1%;">
                    <button type="button" class="btn btn-success" ng-click="durationFilter('today')" ng-disabled="todayDisabled">Today</button>
                      <button type="button" class="btn btn-primary" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled">Yesterday</button>
                      <button type="button" class="btn btn-success" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled">Last Week</button>
                      <button type="button" class="btn btn-info" ng-click="durationFilter('month')" ng-disabled="monthDisabled">Month</button>
                      

                        <img style="cursor: pointer;" ng-click="exportData('tripreportkms')"  src="../app/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('tripreportkms')"  src="../app/views/reports/image/csv.jpeg" />
                        <img style="cursor: pointer;" onclick="generatePDF()"  src="../app/views/reports/image/Adobe.png" />
                    </div>
                    <div class="box-body" id="tripreportkms" >
                <div class="empty" align="center"></div> <p style="margin:1px;font-size:16px;"><?php echo Lang::get('content.trip_summary_report'); ?> <span style="float: right;font-size:14px;padding-right: 10px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{uiDate.fromdate}} &nbsp;{{convert_to_24hrs(uiDate.fromtime)}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{uiDate.todate}} &nbsp;{{convert_to_24hrs(uiDate.totime)}}</span></p> 
                            <br>
                            <div id="formConfirmation">
                                <table class="table table-bordered table-striped table-condensed table-hover">
                                    <thead style="font-weight: bold;">
                                        
                                    <tr class="active">
                                         <td colspan="2"> <div class="col-md-12"><?php echo Lang::get('content.start_time'); ?></div> </td>
                                         <td colspan="2"> <div class="col-md-12" >{{siteData.fromTime |  date:'yyyy-MM-dd HH:mm:ss'}}</div>  </td>
                                         <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.end_time'); ?></div></td>
                                         <td colspan="2"> <div class="col-md-12">{{siteData.toTime |  date:'yyyy-MM-dd HH:mm:ss'}}</div>  </td>
                                           
                                            <!-- <td colspan="1" rowspan="3"><div class="col-md-12">Last Sighted Location</div></td> -->
                                            <td colspan="4" rowspan="3">
                                                <div class="col-md-12"><?php echo Lang::get('content.loc'); ?> 
                                                    <p style="font-weight: normal">{{siteData.lastLocation}} </p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="success">
                                            <td colspan="2"><div class="col-md-12">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></div></td>
                    <td colspan="2"><div class="col-md-12" ng-show="siteData.historyConsilated.length != 0 ">{{siteData.vehicleName}}  </div><div class="col-md-12" ng-show="siteData.historyConsilated.length == 0">{{vehTripName}}</div><div class="col-md-12" ng-show="siteData.historyConsilated == null">{{vehTripName}}</div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12">{{uiGroup}}</div></td>
                                        </tr>
                                         <tr class="danger">
                                            <td colspan="2"><div class="col-md-12"> {{ vehiLabel | translate }}  <?php echo Lang::get('content.id'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12">{{shortNam}}</div></td>
                                            <td colspan="1"><div class="col-md-12"><?php echo Lang::get('content.KMPL'); ?></div></td>
                                            <td colspan="1"><div class="col-md-12">{{siteData.kmpl}}</div></td>
                                            <td colspan="1"><div class="col-md-12"><?php echo Lang::get('content.trip_distance'); ?></div></td>
                                            <td colspan="1"><div class="col-md-12">{{siteData.totalTripLength}} <?php echo Lang::get('content.kms'); ?></div></td>                                            
                                        </tr>
                                        <tr><td colspan="11"></td></tr>
                                        <tr class="active">
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.status'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.duration'); ?> (h:m:s)</div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.fuel_details'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.liters'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.statistics'); ?></div></td>
                                            <td><div class="col-md-12"><?php echo Lang::get('content.occurance'); ?></div></td>
                                        </tr>
                                        <tr class="warning">    
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.moving'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12">{{msToTime(siteData.totalMovingTime)}}</div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.initiallevel'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12">{{siteData.initialFuel}}</div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.fuel_fill_count'); ?></div></td>
                                            <td><div class="col-md-12">{{siteData.fuelFillCount}}</div></td>
                                        </tr>
                                        <tr class="info">
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.idle'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12">{{msToTime(siteData.totalIdleTime)}}</div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.finallevel'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12">{{siteData.finalFuel}}</div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.moving'); ?> <?php echo Lang::get('content.count'); ?></div></td>
                                            <td><div class="col-md-12">{{siteData.moveCount}}</div></td>
                                        </tr>
                                        <tr class="success">
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.parked'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12">{{msToTime(siteData.totalParkingTime)}}</div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.filling'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12">{{siteData.totalFuelFilled}}</div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.parking'); ?> <?php echo Lang::get('content.count'); ?></div></td>
                                            <td><div class="col-md-12">{{siteData.parkCount}}</div></td>
                                        </tr>
                                        <tr class="danger">
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.data1'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12">{{msToTime(siteData.totalNoDataTime)}}</div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.consumption'); ?></div></td>
                                            <td colspan="2"><div class="col-md-12">{{siteData.totalFuelConsume}}</div></td>
                                            <td colspan="2"><div class="col-md-12"><?php echo Lang::get('content.idle'); ?> <?php echo Lang::get('content.count'); ?></div></td>
                                            <td><div class="col-md-12">{{siteData.idleCount}}</div></td>
                                        </tr>                                      
                                        
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="11"></td>
                                    </tr>
                                    <tr>
                                        <td class="id" custom-sort order="'startTime'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;"><?php echo Lang::get('content.start_time'); ?></td>
                                        <td class="id" custom-sort order="'endTime'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;"><?php echo Lang::get('content.end_time'); ?></td>
                                        <td class="id" custom-sort order="'duration'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;"><?php echo Lang::get('content.duration'); ?> (h:m:s)</td>
                                        <td class="id" custom-sort order="'tripDistance'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;"><?php echo Lang::get('content.distance'); ?> <?php echo Lang::get('content.KMS'); ?></td>
                                   <!-- <td ng-hide="siteData.totalFuelConsume==0" class="id" custom-sort order="'intLoc'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;">Start Loc</td>-->
                    <!--          <td ng-show="siteData.totalFuelConsume==0" colspan="3" class="id" custom-sort order="'intLoc'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;">Start Loc</td> -->
                                        <td colspan="3" class="id" custom-sort order="'intLoc'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;"><?php echo Lang::get('content.start_loc'); ?></td> 
                                   <!-- <td ng-hide="siteData.totalFuelConsume==0" class="id" custom-sort order="'finLoc'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;">End Loc</td> -->
                    <!--                <td ng-show="siteData.totalFuelConsume==0" colspan="3" class="id" custom-sort order="'finLoc'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;">End Loc</td> -->
                                        <td colspan="3" class="id" custom-sort order="'finLoc'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;"><?php echo Lang::get('content.end_loc'); ?></td>
                                        <td class="id" custom-sort order="'position'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;"><?php echo Lang::get('content.position'); ?></td>
                                      <!--  <td ng-hide="siteData.totalFuelConsume==0" class="id" custom-sort order="'fuelConsume'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;">Fuel Consume (ltr)</td>
                                        <td ng-hide="siteData.totalFuelConsume==0" class="id" custom-sort order="'intFuel'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;">Intial Fuel (ltr)</td>
                                        <td ng-hide="siteData.totalFuelConsume==0" class="id" custom-sort order="'finFuel'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;">End Fuel (ltr)</td>
                                        <td ng-hide="siteData.totalFuelConsume==0" class="id" custom-sort order="'filledFuel'" sort="sort" style="text-align:center;background-color:#d2dff7;font-weight: bold;">Fuel Filled (ltr) </td> -->
                                    </tr>
                                    <tr>
                                        <td colspan="11"></td>
                                    </tr>
                                    <tr ng-repeat="tripsummary in siteData.historyConsilated | orderBy:sort.sortingOrder:sort.reverse" style="text-align : center;">
                                        <!-- ng-click="getInput(tripsummary, siteData)" data-toggle="modal" data-target="#mapmodals" -->
                                        <td class="col-md-1">{{tripsummary.startTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                        <td class="col-md-1">{{tripsummary.endTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                        <td class="col-md-1">{{msToTime(tripsummary.duration)}}</td>
                                        <td class="col-md-1">{{tripsummary.tripDistance}}</td>
                                   <!-- <td class="col-md-1" ng-switch on="tripsummary.intLoc" ng-hide="siteData.totalFuelConsume==0">
                                            <div ng-switch-when="undefined"><a href="https://www.google.com/maps?q=loc:{{tripsummary.intLat}},{{tripsummary.intLon}}" target="_blank">Link</a></div>
                                            <div ng-switch-default>
                                                <a ng-click="getInput(tripsummary, siteData)" ng-if="tripsummary.position=='M'" data-toggle="modal" data-target="#mapmodals" >{{tripsummary.intLoc}}</a>
                                                <p ng-if="tripsummary.position!= 'M'">{{tripsummary.intLoc}}</p>
                                            </div>
                                        </td> -->

                   <!--                 <td ng-switch on="tripsummary.intLoc" ng-show="siteData.totalFuelConsume==0" colspan="3">
                                            <div ng-switch-when="undefined"><a href="https://www.google.com/maps?q=loc:{{tripsummary.intLat}},{{tripsummary.intLon}}" target="_blank">Link</a></div>
                                            <div ng-switch-default>
                                                <a ng-click="getInput(tripsummary, siteData)" ng-if="tripsummary.position=='M'" data-toggle="modal" data-target="#mapmodals" >{{tripsummary.intLoc}}</a>
                                                <p ng-if="tripsummary.position!= 'M'">{{tripsummary.intLoc}}</p>
                                            </div>
                                        </td> -->

                                         <td ng-switch on="tripsummary.intLoc" colspan="3">
                                            <div ng-switch-when="undefined"><a href="https://www.google.com/maps?q=loc:{{tripsummary.intLat}},{{tripsummary.intLon}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></div>
                                            <div ng-switch-default>
                                                <a ng-click="getInput(tripsummary, siteData)" ng-if="tripsummary.position=='M'" data-toggle="modal" data-target="#mapmodals" >{{tripsummary.intLoc}}</a>
                                                <p ng-if="tripsummary.position!= 'M'">{{tripsummary.intLoc}}</p>
                                            </div>
                                        </td>

                                   <!-- <td class="col-md-1" ng-switch on="tripsummary.finLoc" ng-hide="siteData.totalFuelConsume==0">
                                            <div ng-switch-when="undefined"><a href="https://www.google.com/maps?q=loc:{{tripsummary.finLat}},{{tripsummary.finLon}}" target="_blank">Link</a></div>
                                            <div ng-switch-default>
                                                <a ng-click="getInput(tripsummary, siteData)" ng-if="tripsummary.position=='M'" data-toggle="modal" data-target="#mapmodals" >{{tripsummary.finLoc}}</a>
                                                <p ng-if="tripsummary.position!= 'M'">{{tripsummary.finLoc}}</p>
                                            </div>
                                        </td> -->
                   <!--                 <td ng-switch on="tripsummary.finLoc" ng-show="siteData.totalFuelConsume==0" colspan="3">
                                            <div ng-switch-when="undefined"><a href="https://www.google.com/maps?q=loc:{{tripsummary.finLat}},{{tripsummary.finLon}}" target="_blank">Link</a></div>
                                            <div ng-switch-default>
                                                <a ng-click="getInput(tripsummary, siteData)" ng-if="tripsummary.position=='M'" data-toggle="modal" data-target="#mapmodals" >{{tripsummary.finLoc}}</a>
                                                <p ng-if="tripsummary.position!= 'M'">{{tripsummary.finLoc}}</p>
                                            </div>
                                        </td> -->
                                        <td ng-switch on="tripsummary.finLoc" colspan="3">
                                            <div ng-switch-when="undefined"><a href="https://www.google.com/maps?q=loc:{{tripsummary.finLat}},{{tripsummary.finLon}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></div>
                                            <div ng-switch-default>
                                                <a ng-click="getInput(tripsummary, siteData)" ng-if="tripsummary.position=='M'" data-toggle="modal" data-target="#mapmodals" >{{tripsummary.finLoc}}</a>
                                                <p ng-if="tripsummary.position!= 'M'">{{tripsummary.finLoc}}</p>
                                            </div>
                                        </td> 
                                        <td class="col-md-1" ng-switch on="tripsummary.position" >
                                            <span ng-switch-when="S" style="color : #ff480b"><?php echo Lang::get('content.idle'); ?></span>
                                            <span ng-switch-when="M" style="color : #00d736"><?php echo Lang::get('content.moving'); ?></span>
                                            <span ng-switch-when="P" style="color : #080808"><?php echo Lang::get('content.parked'); ?></span>
                                            <span ng-switch-when="U" style="color : #fc00bd"><?php echo Lang::get('content.data1'); ?></span>
                                        </td>
                                    <!-- <td class="col-md-1" ng-hide="siteData.totalFuelConsume==0">{{tripsummary.fuelConsume}}</td>
                                        <td class="col-md-1" ng-hide="siteData.totalFuelConsume==0">{{tripsummary.intFuel}}</td>
                                        <td class="col-md-1" ng-hide="siteData.totalFuelConsume==0">{{tripsummary.finFuel}}</td>
                                        <td class="col-md-1" ng-hide="siteData.totalFuelConsume==0">{{tripsummary.filledFuel}}</td>
                                    </tr> -->
                                    <tr  ng-if="siteData.historyConsilated==null || siteData.historyConsilated.length== 0" align="center">
                                        <td colspan="11" class="err" ng-if="!errMsg"><h5><?php echo Lang::get('content.no_trip'); ?></h5></td>
                                        <td colspan="11" class="err" ng-if="errMsg"><h5>{{errMsg}}</h5></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        </div>

                    </div>
                </div>
            </div>

       <!--  <div class="modal fade" id="myModal" style=" top :70px">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Trip Summary Routes</h4>
                    </div>
                    <div class="modal-body" id="map_canvas" style="width: 100%; height: 500px;"></div>
                   
                </div>
            </div>
        </div> -->
        <div class="modal fade" id="mapmodals">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myCity"><?php echo Lang::get('content.trip_summary'); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="map_container">
                        <div id="map_canvas" class="map_canvas" style="width: 100%; height: 500px;"></div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
</div>
    <script>

/*  var apikey_url = JSON.parse(localStorage.getItem('apiKey'));
    var url = "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places";

    if(apikey_url != null ||  apikey_url != undefined)
        url = "https://maps.googleapis.com/maps/api/js?key="+apikey_url+"&libraries=places"; */
    
   function loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback) {
     if (scriptsCollection[startIndex]) {
       var fileref = document.createElement('script');
       fileref.setAttribute("type","text/javascript");
       fileref.setAttribute("src", scriptsCollection[startIndex]);
       fileref.onload = function(){
         startIndex = startIndex + 1;
         loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback)
       };
 
       document.getElementsByTagName("head")[0].appendChild(fileref)
     }
     else {
       librariesLoadedCallback();
     }
   }
 
   // An array of scripts you want to load in order
   var scriptLibrary = [];
   
   scriptLibrary.push("assets/js/static.js");
   scriptLibrary.push("assets/js/jquery-1.11.0.js");
   scriptLibrary.push("assets/js/bootstrap.min.js");
   scriptLibrary.push("https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js");
   scriptLibrary.push("../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js");
   scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js");
   scriptLibrary.push("../app/views/reports/customjs/html5csv.js");
   scriptLibrary.push("../app/views/reports/customjs/moment.js");
   scriptLibrary.push("../app/views/reports/customjs/FileSaver.js");
   scriptLibrary.push("../app/views/reports/datepicker/bootstrap-datetimepicker.js");
   scriptLibrary.push("../app/views/reports/datatable/jquery.dataTables.js");
 //scriptLibrary.push(url);
   scriptLibrary.push("https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min.js");
   scriptLibrary.push("https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js");
 //scriptLibrary.push("assets/js/infobubble.js");
 //scriptLibrary.push("assets/js/moment.js");
 //scriptLibrary.push("assets/js/bootstrap-datetimepicker.js");
 //scriptLibrary.push("assets/js/infobox.js");
   scriptLibrary.push("assets/js/naturalSortVersionDatesCaching.js");
 //scriptLibrary.push("assets/js/naturalSortVersionDates.js");
   scriptLibrary.push("assets/js/vamoApp.js");
   scriptLibrary.push("assets/js/services.js");
   scriptLibrary.push("assets/js/siteReport.js?v=<?php echo Config::get('app.version');?>");

// Pass the array of scripts you want loaded in order and a callback function to invoke when its done
   loadJsFilesSequentially(scriptLibrary, 0, function(){
       // application is "ready to be executed"
       // startProgram();
   });
        
    //     $("#menu-toggle").click(function(e) {
    //     e.preventDefault();
    //     $("#wrapper").toggleClass("toggled");
    // });

    var generatePDF = function() {
  kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
    kendo.drawing.pdf.saveAs(group, "TripSummaryReport.pdf");
  });
}

  
  </script>
    
</body>
</html>
