@include('includes.header_create')
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.btn {
    background-color: DodgerBlue;
border: none;
color: white;
font-size: 16px;
cursor: pointer;
}
.btn:hover {
    background-color: RoyalBlue;
}
</style>
</head>
<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
    		<div class="col-lg-12">
        		<div class="hpanel">
                <div class="panel-heading">
                   <h4><b>Create Group  </b></h4> 
                </div>
                <div class="panel-body">
                	<div class="row">
						@if(Session::has('message'))
							<p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('message') }}</p>
						@endif
                		{{ HTML::ul($errors->all()) }}
						{{ Form::open(array('url' => 'vdmGroups','id'=>'formid')) }}
						<div class="col-md-12">
							<span id="validation" style="color: red; font-weight: bold"></span>
						</div>
  	              				<div class="row">
  	              					<div class="col-md-12">
									<h5><font color='red', size='1px'>{{ Form::label('#Group name is case sensitive and space is not allowed') }}</font></h5>
		  	              				<div class="col-md-2">
		  	              					<h5>{{ Form::label('groupId', 'Group Name / Group ID') }}</h5>
		  	              				</div>
										<div class="col-md-4">{{ Form::text('groupId', $groupName1, array('class' => 'form-control', 'placeholder'=>'Group Id','required'=>'required','id'=>'groupName','onkeyup' => 'caps(this)')) }}</div>
		                				<div class="col-md-3" style="text-align: right">
		                					{{ Form::submit('Submit', array('class' => 'btn btn-primary','id'=>'checkGroup')) }}
		                				</div>
									</div>
								</div>
								<hr> 
								<div class="row">
									 <div class="col-md-12">
									 <h5><font>{{ Form::label('vehicleList', 'Select the vehicles:') }}</font></h5>
									

									 </div>
									 <div class="col-md-5">
									<h5> {{ Form::label('Filter', 'Search : ') }}</h5>
									{{ Form::input('text', 'searchtext', null, ['class'=>'searchkey','placeholder'=>'Search..','id'=>'search'])}}
		            			    <button class="btn" type="button" id="btnsearch"><i class="fa fa-search"></i></button>
									</div>
									 <div class="col-md-5"><h5>{{Form::label('Select All :')}} {{Form::checkbox('$userVehicles', 'value', false, ['class' => 'check'])}} </h5></div>
								</div>
								<br/>
								<div class="row">
				            		<div class="col-md-12" id="selectedItems" aligin="center"></div>
				            		<br>
				            		<div class="col-md-12" id="unSelectedItems">
				            		@if(isset($userVehicles))		  
											@foreach($userVehicles as $key => $value)
												<div class="col-md-3 vehiclelist"> 
												{{ Form::checkbox('vehicleList[]', $key.' || ' . array_get($shortNameList, $value) , null, ['class' => 'field', 'id' => 'questionCheckBox']) }}
												{{ Form::label(array_get($shortNameList, $value)) }}
												
												</div>
														
											@endforeach
										@endif
								</div>
		            		</div>


								<!-- @if(isset($userVehicles))		  
									@foreach($userVehicles as $key => $value)
										<div class="col-md-3 vehiclelist"> 
										{{ Form::checkbox('vehicleList[]', $key, null, ['class' => 'field', 'id' => 'questionCheckBox']) }}
										{{ Form::label($value) }}
										{{ Form::label('( ' . array_get($shortNameList, $value) . ' )') }}
										</div>
												
									@endforeach
								@endif -->
								
								<script>
									list = [];
                  					var value = <?php echo json_encode($userVehicles ); ?>;
								</script>
			              		
			              		
               	{{ Form::close() }}	
    		</div>
		</div>
	</div>
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script type="text/javascript">
	$('#groupName').on('change', function() {
		$('#validation').text('');
		var postValue = {
			'id': $(this).val()

			};
		// alert($('#groupName').val());
		$.post('{{ route("ajax.groupIdCheck") }}',postValue)
			.done(function(data) {
				if(data=='fail')
				$('#validation').text('Group Id already exist. Please use different Id');
        		console.log("Sucess-------->"+data);
        		
      		}).fail(function() {
        		console.log("fail");
      });

		
	});

function caps(element){
    element.value = element.value.toUpperCase();
 }

</script>
@include('includes.js_footer')
@include('includes.js_create')
</body>
</html>
