<div id="wrapper">
<div class="content animate-panel">
	<div class="row">
    	<div class="col-lg-12">
       		<div class="hpanel">
       			 	<h6><b><font color="red"> {{ HTML::ul($errors->all()) }}</font></b></h6>
               		<div class="panel-heading">
                   	<center><h4><b><font>ADD DEVICE</font></b></h4></center>
                	</div>
                	<div class="panel-body">
					<h4><font color="#196481" style="margin-left: 5%;">Available Licences :  {{$availableLincence}}
					</font></h4>
					
                		<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                		<div class="row">
                				<div class="col-sm-12">
								{{ Form::open(array('url' => 'Business')) }}
								{{ Form::hidden('availableBasicLicence', $availableBasicLicence, array('class' => 'form-control','id'=>'availableBasicLicence')) }}
								{{ Form::hidden('availableAdvanceLicence', $availableAdvanceLicence, array('class' => 'form-control', 'id' => 'availableAdvanceLicence')) }}
								{{ Form::hidden('availablePremiumLicence', $availablePremiumLicence, array('class' => 'form-control' ,'id'=>'availablePremiumLicence')) }}
                                {{ Form::hidden('availablePremPlusLicence', $availablePremPlusLicence, array('class' => 'form-control' ,'id'=>'availablePremPlusLicence')) }}
								<div class="row">
										<div class="col-md-12">
										<div class="col-md-8" style="left: 8%;">
											<div class="row"  id='type'>
												<div class="col-md-5">
												<label for="LicenceType" style="padding-top: 5%;">Licence Type :</label>
												</div>
												<div class="col-md-5">
                                                    <select class="form-control" id="LicenceType" required="required" data-live-search="true" name="LicenceType" style="width: 100%;">
           	 											<option value="Basic">Basic</option>
           	 											<option value="Advance">Advance</option>
           	 											<option value="Premium">Premium</option>
           	 											<option value="PremiumPlus">Premium Plus</option>
           	 										</select>                   
												 </div>
										</div><br/>
										<div class="row"  id='basic'>
											<div class="col-md-5">
											<label for="basicdevice" style="padding-top: 5%;">Number Of Basic Devices :</label>
											</div>
											<div class="col-md-5">
											<div style="font-size: 9px;margin-left: 68%;margin-top: -3%;font-family: cursive;color: #64cb31;"><b>(Available:</b> {{$availableBasicLicence}})</div>
                  @if($numberofdevice!=0)
											<input class="form-control" placeholder="Quantity" min="1" name="onBasicdevice" type="number" id="basicdevice" style=" min-width: 100%;" value="<?php echo htmlspecialchars($numberofdevice); ?>">
                   @else
                     <input class="form-control" placeholder="Quantity" min="1" name="onBasicdevice" type="number" id="basicdevice" style=" min-width: 100%;">
                   @endif
                     
											</div>
										</div>
										<div class="row" id='advance'>
											<div class="col-md-5">
											<label for="advancedevice" style="padding-top: 5%;">Number Of Advance Devices :</label>
											</div>
											<div class="col-md-5">
											<div style="font-size: 9px;margin-left: 68%;margin-top: -3%;font-family: cursive;color: #64cb31;"><b>(Available:</b> {{$availableAdvanceLicence}})</div>
											<input class="form-control" placeholder="Quantity" min="1" name="onAdvancedevice" type="number" id="advancedevice" style=" min-width: 100%;">
											</div>
										</div>
										<div class="row" id='premium'>
											<div class="col-md-5">
											<label for="premiumdevice" style="padding-top: 5%;">Number Of Premium Devices :</label>
											</div>
											<div class="col-md-5">
											<div style="font-size: 9px;margin-left: 68%;margin-top: -3%;font-family: cursive;color: #64cb31;"><b>(Available:</b> {{$availablePremiumLicence}})</div>
											<input class="form-control" placeholder="Quantity" min="1" name="onPremiumdevice" type="number" id="premiumdevice" style=" min-width: 100%;">
											</div>
										</div>
                                        <div class="row" id='premiumPlus'>
											<div class="col-md-5">
											<label for="premiumPlusdevice" style="padding-top: 5%;">Number Of Premium Plus Devices :</label>
											</div>
											<div class="col-md-5">
											<div style="font-size: 9px;margin-left: 68%;margin-top: -3%;font-family: cursive;color: #64cb31;"><b>(Available:</b> {{$availablePremPlusLicence}})</div>
											<input class="form-control" placeholder="Quantity" min="1" name="onPremiumPlusdevice" type="number" id="premiumPlusdevice" style=" min-width: 100%;">
											</div>
										</div>
                                            
                                            
										</div>
										<div class="col-md-4">
										
										</div>
										</div>
                				</div><br/>
                				<div class="col-sm-4" style="margin-left: 20%;">
									{{ Form::submit('Submit', array('class' => 'btn btn-primary','id'=>'sub')) }}
									{{ Form::close() }}</div>
            					</div>
        				</div>
    	  				</div>
					</div>
			</div>
		</div>
	</div>
</div>
</div>
 <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
	$('#LicenceType').on('change', function() {
		var licence={
    	  'Type':$('#LicenceType').val()
 		 }
 		 if(licence.Type == 'Basic'){
 		 	document.getElementById("basic").style.display ="block";
 		 	document.getElementById("advance").style.display ="none";
 		 	document.getElementById("premium").style.display ="none";
 		 	document.getElementById("premiumPlus").style.display ="none";

 		 	document.getElementById("basicdevice").required ="true";
 		 	document.getElementById("premiumdevice").required ="";
 		 	document.getElementById("advancedevice").required ="";
 		 	document.getElementById("premiumPlusdevice").required ="";
 		 	
 		 }else if(licence.Type == 'Advance'){
 		 	document.getElementById("advance").style.display ="block";
 		 	document.getElementById("premium").style.display ="none";
 		 	document.getElementById("basic").style.display ="none";
 		 	document.getElementById("premiumPlus").style.display ="none";

 		 	document.getElementById("advancedevice").required ="true";
 		 	document.getElementById("basicdevice").required ="";
 		 	document.getElementById("premiumdevice").required ="";
 		 	document.getElementById("premiumPlusdevice").required ="";
 		 }else if(licence.Type == 'Premium'){
 		 	document.getElementById("premium").style.display ="block";
 		 	document.getElementById("basic").style.display ="none";
 		 	document.getElementById("advance").style.display ="none";
 		 	document.getElementById("premiumPlus").style.display ="none";

 		 	document.getElementById("premiumdevice").required ="true";
 		 	document.getElementById("advancedevice").required ="";
 		 	document.getElementById("basicdevice").required ="";
 		 	document.getElementById("premiumPlusdevice").required ="";
 		 }else if(licence.Type == 'PremiumPlus'){
 		 	document.getElementById("premium").style.display ="none";
 		 	document.getElementById("basic").style.display ="none";
 		 	document.getElementById("advance").style.display ="none";
 		 	document.getElementById("premiumPlus").style.display ="block";

 		 	document.getElementById("premiumdevice").required ="";
 		 	document.getElementById("advancedevice").required ="";
 		 	document.getElementById("basicdevice").required ="";
 		 	document.getElementById("premiumPlusdevice").required ="true";
 		 }
 
	});
	window.onload = function(){
		var licence={
    	  'Type':$('#LicenceType').val()
 		 }
 		 if(licence.Type == 'Basic'){
 		 	document.getElementById("basic").style.display ="block";
 		 	document.getElementById("advance").style.display ="none";
 		 	document.getElementById("premium").style.display ="none";
 		 	document.getElementById("premiumPlus").style.display ="none";

 		 	document.getElementById("basicdevice").required ="true";
 		 	document.getElementById("premiumdevice").required ="";
 		 	document.getElementById("advancedevice").required ="";
 		 	document.getElementById("premiumPlusdevice").required ="";
 		 	
 		 }else if(licence.Type == 'Advance'){
 		 	document.getElementById("advance").style.display ="block";
 		 	document.getElementById("premium").style.display ="none";
 		 	document.getElementById("basic").style.display ="none";
 		 	document.getElementById("premiumPlus").style.display ="none";

 		 	document.getElementById("advancedevice").required ="true";
 		 	document.getElementById("basicdevice").required ="";
 		 	document.getElementById("premiumdevice").required ="";
 		 	document.getElementById("premiumPlusdevice").required ="";
 		 }else if(licence.Type == 'Premium'){
 		 	document.getElementById("premium").style.display ="block";
 		 	document.getElementById("basic").style.display ="none";
 		 	document.getElementById("advance").style.display ="none";
 		 	document.getElementById("premiumPlus").style.display ="none";

 		 	document.getElementById("premiumdevice").required ="true";
 		 	document.getElementById("advancedevice").required ="";
 		 	document.getElementById("basicdevice").required ="";
 		 	document.getElementById("premiumPlusdevice").required ="";
 		 }else if(licence.Type == 'PremiumPlus'){
 		 	document.getElementById("premium").style.display ="none";
 		 	document.getElementById("basic").style.display ="none";
 		 	document.getElementById("advance").style.display ="none";
 		 	document.getElementById("premiumPlus").style.display ="block";

 		 	document.getElementById("premiumdevice").required ="";
 		 	document.getElementById("advancedevice").required ="";
 		 	document.getElementById("basicdevice").required ="";
 		 	document.getElementById("premiumPlusdevice").required ="true";
 		 }
	}

</script>