<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
    		<div class="col-lg-12">
        		<div class="hpanel">
               		 <div class="panel-heading">
                  		<h4><b><font> Add Device</font></b></h4>
                	</div>
               		 <div class="panel-body" style="border: 7px solid #b1dfe01f; border-radius: 10px; background: #0b70751c; text-shadow: 0px 0px;">
						{{ Form::open(array('id'=>'submit')) }}
						{{ HTML::ul($errors->all()) }}
						<span id="error" style="color:red;font-weight:bold"></span> 
						<div class="col-md-12">
							{{ Form::hidden('numberofdevice1', $numberofdevice, array('class' => 'form-control')) }}
							{{ Form::hidden('availableLincence', $availableLincence, array('class' => 'form-control')) }}
						</div>
						<div class="row">
							<div class="col-md-12"><h6><font style="font-size: 17px; color: #0a4d88;font-weight: bold;">{{ Form::label('Business','BUSINESS :') }}</font></h6></div>
						</div>
						<br>
						<div class="row">	
							<!-- <div class="col-md-2"></div>
							<div class="col-md-1"><div id="hide" style="border-radius: -25px; height:0px; margin: 0; width : 0px; padding: 0px; border: 0px">{{ Form::radio('type', 'Move') }}</div></div>
							<div class="col-md-1">Batch Move</div> -->
							
							<table class="col-md-12">
								<tr>
									<td class="col-md-4" style="color: #6a6c6f;font-size: 15px; font-weight: bold;"><span style=" border-radius: 0px; width : 0px; height:0px; padding: 0px;border: 0px; position: absolute;" id="hide">{{ Form::radio('type', 'Move') }} </span>&nbsp;&nbsp;&nbsp; Batch Move</td>
									<td class="col-md-4" style="color: #6a6c6f;font-size: 15px; font-weight: bold;"><span style=" border-radius: 0px; width : 0px; height:0px; padding: 0px;border: 0px; position: absolute" id="show">{{ Form::radio('type', 'Sale') }} </span>&nbsp;&nbsp;&nbsp; Batch Sale</td>
									<td class="col-md-4"><div>{{ Form::submit('Submit', array('id'=>'Subtest','class' => 'btn btn-primary')) }}</div></td>
								</tr>
							</table>
						</div>
						<br>
						<div class="col-md-12">
							<!-- <div class="col-md-2"></div> -->
							<!-- <div class="col-md-1"><div id="show" style="border-radius: -25px; height:0px; margin: 0; width : 0px; padding: 0px; border: 0px">{{ Form::radio('type', 'Sale') }}</div></div>
							<div class="col-md-2">Batch Sale</div> -->
							<div class="col-md-2"id="p1">{{ Form::select('dealerId', $dealerId, Input::old('	'), array('class' => 'selectpicker show-menu-arrow','data-live-search '=> 'true', 'data-toggle' => 'dropdown')) }}</div>
						<!-- 	<div class="col-md-2"></div>
							<div class="col-md-2">{{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}</div> -->
						</div>
						
						<hr id="hr">
						<div class="row">
							<div class="col-md-2"></div>
							<table class="col-md-8" id="p">
								<tr>
									<td class="col-md-4" style="color: #16564b;font-size: 15px; font-weight: bold;"><span id="hide1"  style=" border-radius: 0px; width : 0px; height:0px; padding: 0px;border: 0px; position: absolute;" id="hide">{{ Form::radio('type1', 'new') }} </span>&nbsp;&nbsp;&nbsp; New User</td>
									<td class="col-md-4" style="color: #16564b;font-size: 15px; font-weight: bold;"><span id="show1" style=" border-radius: 0px; width : 0px; height:0px; padding: 0px;border: 0px; position: absolute" id="show">{{ Form::radio('type1', 'existing') }} </span>&nbsp;&nbsp;&nbsp; Existing User</td>
									<!-- <td class="col-md-4"><div>{{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}</div></td> -->
								</tr>
							</table>
						</div>
						
						<div class="col-md-12" id="t">
							<br>
						
							<div class="col-md-2"></div>
							<div class="col-md-2">{{ Form::label('ExistingUser', 'Existing User') }}{{ Form::select('userIdtemp', $userList,'select', array('id'=>'userIdtemp1','class' => 'selectpicker show-menu-arrow form-control', 'data-live-search '=> 'true')) }}</div>
							<div class="col-md-2">{{ Form::label('Group', 'Group name') }}{{ Form::select('groupname', array(''=>'--select--'),Input::old('groupname'), array('id'=>'groupname','class' => 'form-control selectpicker show-menu-arrow', 'data-live-search '=> 'true')) }}</div>
							<div class="col-md-2">{{ Form::label('orgId', 'Org/College Name') }}{{ Form::select('orgId',  $orgList, Input::old('orgId'), array('class' => 'selectpicker show-menu-arrow form-control', 'data-live-search '=> 'true')) }} </div>
							
						</div>
						
						<script>
                         function caps(element){
                          element.value = element.value.toUpperCase();
                         }                         
                         </script>
						<div class="row" Id="t1">
							<br>
						
							<div class="col-md-2"></div>
							<div class="col-md-2">{{ Form::text('userId', Input::old('userId'), array('id'=>'userIdtempNew','class' => 'form-control','onkeyup' => 'caps(this)','placeholder'=>'User Name')) }}</div>
							<div class="col-md-2">{{ Form::text('mobileNoUser', Input::old('mobileNoUser'),array('class' => 'form-control','placeholder'=>'Mobile Number')) }}</div>
							<div class="col-md-2">{{ Form::Email('emailUser', Input::old('emailUser'),array('class' => 'form-control','placeholder'=>'Email')) }}</div>
							<div class="col-md-2">{{ Form::text('password', Input::old('password'),array('class' => 'form-control','placeholder'=>'Password'))}}</div>

						</div>
						<hr id="h2" style="width: 100%;">
						<div class="row">
							<div class="col-md-2"></div>
							<table class="col-md-8" id="m">
								<tr>
									<td class="col-md-4" style="color: #2e0b4c;font-size: 15px; font-weight: bold;"><span id="excel1"  style=" border-radius: 0px; width : 0px; height:0px; padding: 0px;border: 0px; position: absolute;" >{{ Form::radio('type3', 'excel') }} </span>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;Upload Devices</td>
									<td class="col-md-4" style="color: #2e0b4c;font-size: 15px; font-weight: bold;"><span id="de1" style=" border-radius: 0px; width : 0px; height:0px; padding: 0px;border: 0px; position: absolute;" >{{ Form::radio('type3', 'detail',array('value'=>'true','id'=>'ondetails')) }} </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Device Details</td>
									<!-- <td class="col-md-4"><div>{{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}</div></td> -->
								</tr>
							</table>
							
						</div>
						
						<div class="row" Id="n1">
							<br>
				
							<div class="col-md-2"></div>
									<div class="col-md-2"><a href="{{ URL::to('downloadExcel/xls') }}"><button class="btn" style="background-color:#1e59bf;border-color: #4575b5;color:#ffffff;" id="dexcel">Download Template</button></a></div>
							
							<div class="col-md-2" style="width: 28%;">
							<div class="input-group" style="width: 110%;">
                				<label class="input-group-btn" >
                   				 <span class="btn btn-primary">
                        		Browse&hellip; <input type="file" name="import_file"  id="Upload" style="display: none;" multiple>
                    			</span>
                			</label>
                			<input type="text" class="form-control" readonly>
            			</div>
            		</div>
							<div class="col-md-2" style="margin-left: 30px;"><a class="btn" style="background-color:#2d86a7;border-color: #0e75a0; color:#ffffff;" id="import">IMPORT EXCEL</a></div>
							

						</div>
						<br>
						<div class="row" id="details">
							<div class="col-md-1"></div>
							<div class="col-md-10">
								<table class="table table-bordered dataTable" style="z-index: -10px; position: relative;border: 5px solid #f1f1f1;border-radius: 10px;">
									<thead>
										<tr>
											<th style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">No</th>
											<th style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">Device ID</th>
											<th style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">Device Type</th>
											<th style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">Vehicle Id</th>
											<th style="text-align: center;color: #15386d;font-size: 15px; font-weight: bold;">Action</th>
										</tr>
									</thead>
									<tbody>
										@for($i=1;$i<=$numberofdevice;$i++)
										<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>


						<script>
						var alerttest;
						$(document).ready(function() {
						$("#td{{$i}}").hide();
						$("#show").click(function(){
                      
							$('#deviceid{{$i}}').on('change', function() {
							var data = {
								'id': $(this).val()

							};
                         var splChars = /[ !@#$%^&*() +\=\[\]{};':"\\|,.<>\/?]/;
                         if((data.id).match(splChars))
                         {
                         alerttest=true;
                         alert('Please Enter valid Id');
                         return false;
                         }
						$.post('{{ route("ajax.checkDevice") }}', data, function(data, textStatus, xhr) {
							/*optional stuff to do after success */
											// console.log('ahan ram'+data.rfidlist);
											$('#error').text(data.error);
											if(data.error!=' ')
											{
												alert(data.error);
											alerttest=true;
											}
											});
											//alert("The text has been changeded.");
											});
                      });
                      $("#hide").click(function(){
											$('#deviceidSale{{$i}}').on('change', function() {
											console.log('ahan1');
											var data = {
											'id': $(this).val()

											};
                       var splChars = /[ !@#$%^&*() +\=\[\]{};':"\\|,.<>\/?]/;
                       if((data.id).match(splChars))
                       {
                        alerttest=true;
                       alert('Please Enter valid Id');
                      ('#deviceidSale{{$i}}').focus();
                       return false;
                       }
											console.log('ahan'+data);
											$.post('{{ route("ajax.checkDevice") }}', data, function(data, textStatus, xhr) {
											/*optional stuff to do after success */
											// console.log('ahan ram'+data.rfidlist);
											$('#error').text(data.error);
											if(data.error!=' ')
											{
												alert(data.error);
											alerttest=true;
											}
											});
											//alert("The text has been changeded.");
											});
										});



											$('#userIdtemp1').on('change', function() {
											console.log('test vamos');
											var data = {
											'id': $(this).val(),
											"_token": "{{ csrf_token() }}",
											};
                     
											console.log('ahan'+data);
											$.post('{{ route("ajax.getGroup") }}', data, function(data, textStatus, xhr) {

											 			 $('#groupname').empty();
											 			 $('#error').text(data.error);
											 			 if(data.error!=' ')
														{
															alert(data.error);
														}
														 			 //onsole.log('ahan1'+data.groups);
														$.each(data.groups, function(key, value) {   
														     $('#groupname')
														         .append($("<option></option>")
														         .attr("value",key)
														         .text(value)); 
														});
												 $('#groupname').selectpicker('refresh');


											});
											//alert("The text has been changeded.");
											});


											$('#userIdtempNew').on('change', function() {
											//alert("hai");
											$('#error').text('');
											var data = {
											'id': $(this).val()

											};
											console.log('ahan'+data);
											$.post('{{ route("ajax.checkUser") }}', data, function(data, textStatus, xhr) {

											 			 $('#error').text(data.error);
											 			 if(data.error!=' ')
															{
																// alert(data.error);
															}
											 			 //onsole.log('ahan1'+data.groups);
														


											});
											//alert("The text has been changeded.");
											});


											$("#show").click(function(){
                      
											$('#vehicleId{{$i}}').on('change', function() {                                                                 
											$('#error').text('');
											var data = {
											'id': $(this).val()

											};
                                             
										var splChars = /[ !@#$%^&*() +\=\[\]{};':"\\|,.<>\/?]/;
										if((data.id).match(splChars))
										{
										alerttest=true;
										alert('Please Enter valid Id');
										return false;
											}
                       
											
											$.post('{{ route("ajax.checkvehicle") }}', data, function(data, textStatus, xhr) {
											/*optional stuff to do after success */
											// console.log('ahan ram'+data.rfidlist);
											$('#error').text(data.error);
											if(data.error!=' ')
											{
											 alert(data.error);
											alerttest=true;
											}

											});
											//alert("The text has been changeded.");
											});
										});
             
											$("#hide").click(function(){ 
											$('#vehicleIdSale{{$i}}').on('change', function() {
											$('#error').text('');
											var data = {
											'id': $(this).val()

											};
                        var splChars = /[ !@#$%^&*() +\=\[\]{};':"\\|,.<>\/?]/;
                       if((data.id).match(splChars))
                       {
                       alerttest=true;
                       alert('Please Enter valid Id');
                       ('#vehicleIdSale{{$i}}').focus();
                       return false;
                       }
											console.log('ahan'+data);
											$.post('{{ route("ajax.checkvehicle") }}', data, function(data, textStatus, xhr) {
											/*optional stuff to do after success */
											// console.log('ahan ram'+data.rfidlist);
											$('#error').text(data.error);
											if(data.error!=' ')
											{
											 alert(data.error);
                        alerttest=true;
											}

											});
											//alert("The text has been changeded.");
											});
                       });

											$("#refData{{$i}}").click(function(){
											$("#td{{$i}}").toggle(500);
											});

											}


											);
											function caps(element){
												element.value = element.value.toUpperCase();
											}  
                       $("#Subtest").click(function(){
                       if(alerttest==true)
                        {
                         alert('Please Enter valid Id');
                         alerttest=false;
                          //location.reload();
                         return false;
                        
                        }                       
                       });                                                               
                      
											</script>	
										<tr style="text-align: center;">
											<td>{{ $i }}</td>
											<td class="batchSale">{{ Form::text('deviceid'.$i, Input::old('deviceid'), array('id' => 'deviceid'.$i, 'class' => 'form-control batchSale')) }}</td>
                       <td class="batchMove">{{ Form::text('deviceidSale'.$i, $devices[$i], array('id' => 'deviceidSale'.$i, 'class' => 'form-control batchMove')) }}</td>
                     
                      <td>{{ Form::select('deviceidtype' .$i, $protocol, Input::old('deviceidtype'), array('class' => 'form-control')) }}</td>
                      <td  class="batchSale">{{ Form::text('vehicleId'.$i, Input::old('vehicleId'), array('id' => 'vehicleId'.$i, 'class' => 'form-control batchSale','onkeyup' => 'caps(this)')) }}</td>
                      <td  class="batchMove">{{ Form::text('vehicleIdSale'.$i, $vehicles[$i], array('id' => 'vehicleIdSale'.$i, 'class' => 'form-control batchMove','onkeyup' => 'caps(this)')) }}</td>
                      
                      <td><a id="refData{{$i}}" class="btn btn-sm btn-success" >Details</a></td>
										</tr>
                
										<tr>
											<td id="td{{$i}}" colspan="6">
												<table>
													<tr style="height: 50px">
														<td class="col-md-2">{{ Form::text('shortName'.$i, Input::old('shortName'), array('class' => 'form-control','placeholder'=>'Vehicle Name')) }} </td>
														<td class="col-md-2">{{ Form::text('regNo'.$i, Input::old('regNo'), array('class' => 'form-control','placeholder'=>'Reg No')) }}</td>
														<td class="col-md-2">{{ Form::select('vehicleType'.$i, array( 'Ambulance'=>'Ambulance', 'Bike' => 'Bike','Bull' => 'Bull','Bus' => 'Bus','Car' => 'Car','heavyVehicle'=>'Heavy Vehicle', 'Truck' => 'Truck', 'Van' => 'Van'), Input::old('vehicleType'), array('class' => 'form-control')) }}</td>
														<td class="col-md-2">{{ Form::select('oprName'.$i, array('airtel'=>'Operator', 'airtel' => 'Airtel','idea' => 'Idea', 'reliance' => 'Reliance','vodafone'=>'Vodafone'), Input::old('oprName'), array('class' => 'form-control')) }}</td>
														<td class="col-md-2">{{ Form::select('fuel'.$i,  array( 'no' => 'Fuel No','yes' => 'Fuel Yes' ), Input::old('fuel'), array('class' => 'form-control')) }}</td>
														<td class="col-md-2">{{ Form::text('altShortName'.$i, Input::old('altShortName'), array('class' => 'form-control','placeholder'=>'Alternate Short Name')) }}</td>

													</tr>
													<tr style="height: 50px">
														<td class="col-md-2">{{ Form::text('eveningTripStartTime'.$i, Input::old('eveningTripStartTime'), array('class' => 'form-control','placeholder'=>'Evening Trip Start Time')) }}</td>
														<td class="col-md-2">{{ Form::number('overSpeedLimit'.$i, Input::old('overSpeedLimit'), array('class' => 'form-control','min'=>'1')) }} </td>
														<td class="col-md-2">{{ Form::text('mobileNo'.$i, Input::old('mobileNo'), array('class' => 'form-control','placeholder'=>'Alert Mobile No')) }}</td>
														<td class="col-md-2">{{ Form::text('odoDistance'.$i, Input::old('odoDistance'), array('class' => 'form-control','placeholder'=>'Odometer')) }}</td>
														<td class="col-md-2">{{ Form::text('driverName'.$i, Input::old('driverName'), array('class' => 'form-control','placeholder'=>'Driver Name')) }}</td>
														<td class="col-md-2">{{ Form::text('gpsSimNo'.$i, Input::old('gpsSimNo'), array('class' => 'form-control','placeholder'=>'Sim No','maxlength' => 15,'minlength'=>10)) }}</td>
													</tr>
													<tr style="height: 50px">	
														<td class="col-md-2">{{ Form::text('email'.$i, Input::old('email'), array('class' => 'form-control','placeholder'=>'Alert Email')) }}</td>
														</td>
														<td class="col-md-2">{{ Form::select('isRfid'.$i,  array( 'no' => 'Rfid No','yes' => 'Rfid Yes' ), Input::old('isRfid'), array('class' => 'form-control')) }}</td>
														<td class="col-md-2">{{ Form::select('Licence'.$i,  array($Licence), 'Basic', array('class' => 'form-control')) }}</td>
														<td class="col-md-2">{{ Form::select('Payment_Mode'.$i,$Payment_Mode, 'Yearly', array('class' => 'form-control')) }}</td>
														<td class="col-md-2">{{ Form::text('descr'.$i, Input::old('descr'), array('class' => 'form-control','placeholder'=>'Description')) }}</td>
														<td class="col-md-2">
                                                          <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
                                                          <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
                                                        <script>   
                                                            $(function() {
                                                             $( "#calendar" ).datepicker({ dateFormat: 'yy-mm-dd' });
                                                               }); 
                                                        </script>
                                                           <input type="text" name="vehicleExpiry" placeholder="vehicleExpiry" id="calendar" />
                                                     </td>
													</tr>
												</table>							
											</td>
										</tr>
										@endfor
									</tbody>
								</table>
							</div>
						</div>
						{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>


$("#Subtest").click(function() {

	var formAction = 'Business/adddevice';
	
		$('#submit').attr('action', formAction);
    	$('#submit').submit();
	
});

$("#import").click(function() {

	var formAction1 = 'import/Excel';
	var excel = document.getElementById("Upload").value;
	var enc='multipart/form-data';
	//enctype="multipart/form-data"
		$('#submit').attr('action', formAction1);
		$('#submit').attr('enctype', enc);
    	$('#submit').submit();
});


$("#dexcel").click(function(){

var formAction = 'downloadExcel/xls';

$('#submit').attr('action', formAction);
    //submit form
    $('#submit').submit();
});


$("#hide").click(function(){
	$("#p").hide();
	$("#p1").show();
	$('#m').show();
	$("#t").hide();
	$("#t1").hide();
	$('#hide').attr('disabled', true);
  $('.batchSale').hide();
  $('.batchMove').show();
  $(".batchMove").prop('required',true); 
  $(".batchSale").prop('required', false);
  $('#de1').click();
  var inputUser = $(":input[name=type1]:checked").val();
   $(":input[name=type1]:checked").attr('value', '');
  
});
$("#show").click(function(){
$('#hr').show();
	$('#m').hide();
	$('#n1').hide();
	$("#details").hide();
	$("#h1").hide();
	$("#p").show();
	$("#p1").hide();
	$('#show').attr('disabled', true);
 
  $('.batchSale').show();
  $('.batchMove').hide();
  $(".batchSale").prop('required',true);
  $(".batchMove").prop('required',false); 
 // var inputUser = $(":input[name=type1]:checked").val();
 //  $(":input[name=type1]:checked").attr('value', inputUser);
  //document.getElementById("Subtest").value="SUBMIT";
   
});

$("#excel1").click(function(){
$("#n1").show();
$('#h2').show(); 
$("#details").hide();
document.getElementById("Subtest").value="IMPORT EXCEL";

$('#Subtest').hide();
$('#import').show();
   
});

$("#de1").click(function(){
document.getElementById('de1').checked=true;
document.getElementById('excel1').checked=false;
$("#details").show();
$("#n1").hide();
document.getElementById("Subtest").value="SUBMIT";
$('#import').hide();
$('#Subtest').show();

});
$("#hide1").click(function(){
	$("#t").hide();
	$("#t1").show();
	$('#h1').show();
	$('#h2').show(); 
	$('#m').show();  
   $('#de1').click(); 
	//document.getElementById('userIdtempNew').required=true;
	
});


$("#show1").click(function(){
	$("#t").show();
	$('#h1').show();
	$('#h2').show();
	$("#t1").hide();
	$('#m').show();
  $('#de1').click();
	$('#show1').attr('disabled', true);
	document.getElementById('userIdtempNew').required="";
});

    		$("#p").hide();
    		$("#p1").hide();
    		$("#t").hide();
    		$("#t1").hide();
       $('.batchMove').hide();
       $('#m').hide();
       $('#n1').hide();
       $('#details').hide();
       $('#h1').hide();
       $('#h2').hide(); 
       $('#import').hide();
       $('#Subtest').hide();
       $('#hr').hide();


// When the user clicks anywhere outside of the modal, close it
window.onload = function()
    {
    	document.getElementById("Subtest").value="SUBMIT";
    	
}  

$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
  	 var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });
  


</script>
