@include('includes.header_index')
<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
			<div class="col-lg-12">
				<div class="hpanel">
					<div class="panel-heading">
						<h4><font><b> Dash Board </b> </font></h4>
					</div>
					<div class="panel-body">
					<span id="error" style="color:red;font-weight:bold">{{ HTML::ul($errors->all()) }}</span> 
                        @if(Session::has('message'))
						  <p  n class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
					    @endif
						{{ Form::open(array('id' => 'fromSub')) }} 
						<div class="form-group">
							<h5><font><b>Batch Sale</b></font></h5>
							<!-- <div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-1"><div id="hide1" style="border-radius: -25px; height:0px; margin: 0; width : 0px; padding: 0px; border: 0px">{{ Form::radio('type1', 'new') }}</div></div>
								<div class="col-md-1">New User</div>
								 <div class="col-md-2"></div> 
							</div> -->
							<br>
							<div>
								<table class="col-md-12">
									<tr>
										<td class="col-md-2"><span style=" border-radius: 0px; width : 0px; height:0px; padding: 0px;border: 0px; position: absolute;" id="hide1">{{ Form::radio('type1', 'new') }} </span>&nbsp;&nbsp;&nbsp; New User</td>
										<td class="col-md-2"><span style=" border-radius: 0px; width : 0px; height:0px; padding: 0px;border: 0px; position: absolute" id="show1">{{ Form::radio('type1', 'existing',array('value'=>'true','id'=>'onexist')) }} </span>&nbsp;&nbsp;&nbsp; Existing User</td>
										
										<td class="col-md-2"><div>{{ Form::submit('Submit', array('class' => 'btn btn-sm btn-info','id'=>'subSmt')) }}</div></td>
									</tr>
								</table>
								
							</div>

							<br>
							<div id="t">
								<br>
								<hr>
								<div class="col-md-2"></div>
								<div class="col-md-2">{{ Form::label('ExistingUser', 'Existing User') }}{{ Form::select('userIdtemp', $userList,'select', array('id'=>'valSelected','class' => 'selectpicker show-menu-arrow form-control','data-live-search '=> 'true')) }}</div>
								<div class="col-md-2">{{ Form::label('Group', 'Group name') }}{{ Form::select('groupname', array(null),Input::old('groupname'), array('id'=>'groupname','class' => 'selectpicker show-menu-arrow form-control','data-live-search '=> 'true')) }}</div>
								<div class="col-md-2">{{ Form::label('orgId', 'Org/College Name') }}{{ Form::select('orgId',  array_merge(['' => 'Please Select'], $orgList), Input::old('orgId'), array('class' => 'selectpicker show-menu-arrow form-control','data-live-search '=> 'true')) }}</div>
							</div>
							<div id="t1">
								<br>
								<hr>
								<div class="col-md-2"></div>
								<div class="col-md-2">{{ Form::text('userId', Input::old('userId'),array('id'=>'userIdtempNew','placeholder'=>'User Name','class' => 'form-control','onkeyup' => 'caps(this)')) }}</div>
								<div class="col-md-2">{{ Form::number('mobileNo', Input::old('mobileNo'), array('placeholder'=>'Mobile No','class' => 'form-control')) }}</div>
								<div class="col-md-2">{{ Form::email('email', Input::old('email'), array('placeholder'=>'Email','class' => 'form-control')) }}</div>
								<div class="col-md-2">{{ Form::text('password', Input::old('password'), array('placeholder'=>'Password','class' => 'form-control')) }}</div>
							</div>


							<hr id="h2" style="width: 100%;margin-top: 88px;">
						<div class="row">
							<div class="col-md-2"></div>
							<table class="col-md-8" id="m">
								<tr>
									<td class="col-md-4" style="color: #2e0b4c;font-size: 15px; font-weight: bold;"><span id="excel1"  style=" border-radius: 0px; width : 0px; height:0px; padding: 0px;border: 0px; position: absolute;" id="excel">{{ Form::radio('type3', 'excel') }} </span>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;Upload Details</td>
									<td class="col-md-4" style="color: #2e0b4c;font-size: 15px; font-weight: bold;"><span id="de1" style=" border-radius: 0px; width : 0px; height:0px; padding: 0px;border: 0px; position: absolute;" id="de">{{ Form::radio('type3', 'detail',array('value'=>'true','id'=>'ondetails')) }} </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Device Details</td>
									</tr>
							</table>
							
						</div>

							<div class="row" Id="n1">
							<br>
							<hr>
							<div class="col-md-2"></div>
							<div class="col-md-2"><a href="{{ URL::to('downloadDealerExcel/xls') }}"><button class="btn" style="background-color:#1e59bf;border-color: #4575b5;color:#ffffff;" id="dexcel">Download Template</button></a></div>
							
							<div class="col-md-2" style="width: 28%;">
							<div class="input-group" style="width: 110%;">
                				<label class="input-group-btn" >
                   				 <span class="btn btn-primary">
                        		Browse&hellip; <input type="file" name="import_file"  id="Upload" style="display: none;" multiple>
                    			</span>
                			</label>
                			<input type="text" class="form-control" readonly>
            			</div>
            		</div>
							<div class="col-md-2" style="margin-left: 30px;"><a class="btn" style="background-color:#2d86a7;border-color: #0e75a0; color:#ffffff;" id="import">IMPORT EXCEL</a></div>
						

						</div>


							<span id="error" style="color:red;font-weight:bold"></span>
							<div class="row" id="tab">
								<div class="col-md-12">
									<hr>
									<table id="example1" class="table table-bordered dataTable">
										<thead>
											<tr>
												<th style="text-align: center;">ID</th>
												<th style="text-align: center;">Choose</th>
												<th style="text-align: center;">Device ID</th>
												<th style="text-align: center;">Device Type</th>
												<th style="text-align: center;">Action</th>
												
											</tr>
										</thead>
										<tbody>
											@if(isset($devices))
											@foreach($devices as $key => $value)
											<tr style="text-align: center;">
												<td>{{ ++$key }}</td>
												<td>{{ Form::checkbox('vehicleList[]', $devices[$key-1].','.$devicestypes[$key-1], null, ['class' => 'field']) }}</td>
												
												<td>{{ array_get($devices, $key-1)}}</td>
												<td>{{ array_get($devicestypes, $key-1)}}</td>
												<td>
													
													<a class="btn btn-sm btn-success" href="{{ URL::to('vdmVehicles/create/'. $devices[$key-1]) }}">Details</a>
													
													
												</td>

											</tr>
											@endforeach
											@endif

										</tbody>
									</table>
								</div>
							</div> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script>
function caps(element){
    element.value = element.value.toUpperCase();
  }
$(document).ready(function(){


	$("#hide").click(function(){
		$("#p").hide();
		$("#p1").show();
		$("#t").hide();
		$("#t1").hide();
		$('#hide').attr('disabled', true);
	});
	$("#show").click(function(){
		$("#p").show();
		$("#p1").hide();
		$('#show').attr('disabled', true);
	});
	

	 $("#hide1").click(function(){
	 	$("#t").hide();
	 	$("#h2").show();
	 	$("#t1").show();
		$("#m").show();
	 	$('#hide1').attr('disabled', true);
	 });
	 $("#show1").click(function(){
	 	$("#t").show();
	 	$("#h2").show();
	 	$("#t1").hide();
		$("#m").show();
	 	$('#show1').attr('disabled', true);
	 });

	 $("#excel1").click(function(){
	console.log('+++++++++++++++');
$("#n1").show();
$('#h2').show(); 
$("#tab").hide();
document.getElementById("subSmt").value="IMPORT EXCEL";
document.getElementById('excel1').checked=true;
document.getElementById('de1').checked=false;
$('#subSmt').hide();
$('#import').show();
   
});

$("#de1").click(function(){
$("#tab").show();
$("#n1").hide();
document.getElementById("subSmt").value="SUBMIT";
document.getElementById('de1').checked=true;
document.getElementById('excel1').checked=false;
$('#import').hide();
$('#subSmt').show();
});


	// $("#p").hide();
	// $("#p1").hide();
	//$("#h2").hide();
	//$("#t").hide();
	$("#t1").hide(); //newuser
	//$("#m").hide();
    $("#n1").hide();
    //$("#tab").hide();
    //$("#subSmt").hide();
    


$('#valSelected').on('change', function() {
	console.log('test vamos');
	var data = {
		'id': $(this).val(),
		"_token": "{{ csrf_token() }}",

	};
	console.log('ahan'+data);
	$.post('{{ route("ajax.getGroup") }}', data, function(data, textStatus, xhr) {

		$('#groupname').empty();
 			 //onsole.log('ahan1'+data.groups);
 			 $('#error').text(data.error);
 			 if(data.error!=' ')
 			 {
 			 	alert(data.error);
 			 }
 			 $.each(data.groups, function(key, value) {   
 			 	$('#groupname')
 			 	.append($("<option></option>")
 			 		.attr("value",key)
 			 		.text(value)); 
 			 });
			  $('#groupname').selectpicker('refresh');
 			});
});


$('#userIdtempNew').on('change', function() {

	var data = {
		'id': $(this).val()

	};
	console.log('ahan'+data);

	$.post('{{ route("ajax.checkUser") }}', data, function(data, textStatus, xhr) {

		$('#error').text(data.error);
		if(data.error!=' ')
		{
			alert(data.error);
		}
	});
});
});

$("#dexcel").click(function(){

var formAction = 'downloadDealerExcel/xls';

$('#fromSub').attr('action', formAction);
    //submit form
    $('#fromSub').submit();
});

$("#import").click(function() {

	console.log('importtttttt');
	var formAction1 = 'import/DealerExcel';
	var excel = document.getElementById("Upload").value;
	console.log(excel);
	var enc='multipart/form-data';
	//enctype="multipart/form-data"
		$('#fromSub').attr('action', formAction1);
		$('#fromSub').attr('enctype', enc);
    	$('#fromSub').submit();
}); 
$("#subSmt").click(function(){

var formAction = 'Business/batchSale';

$('#fromSub').attr('action', formAction);
    //submit form
    $('#fromSub').submit();
});

$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
  	console.log('fileeeeeeeeeeeeeee');
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });
  
</script>
@include('includes.js_index')
</body>
</html>
