<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login V13</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
  <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../vendor/bootstrap13/css/bootstrap.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../fonts/font-awesome-4.7.13.013/css/font-awesome.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../fonts/Linearicons-Free-v1.0.13.013/icon-font.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../fonts/iconic13/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../vendor/animate13/animate.css">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="../../../vendor/css-hamburgers13/hamburgers.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../vendor/animsition13/css/animsition.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../vendor/select213/select2.min.css">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="../../../vendor/daterangepicker13/daterangepicker.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../css/util13.css">
  <link rel="stylesheet" type="text/css" href="../../../css/main13.css">
<!--===============================================================================================-->
<style type="text/css">
  .img-circular{
   display: table-cell;
    height: 300px;
    text-align: center;
    width: 300px;
    vertical-align: middle;
    margin-left: 25%;
}
#col-1 {
  position: relative;
  width: 30%;
  float: left;
  height: 100%;
  z-index: 1010101010;
   margin-left: 5%;
}

#col-2 {
  position: relative;
  width: 65%;
  float: left;
  height: 100%;
  z-index: 1010101010;
  overflow: auto;
}
.btn1 {
    border: 2px solid gray;
    color: gray;
    background-color: white;
    padding: 7px 8px;
    border-radius: 8px;
    font-size: 10px;
    font-weight: bold;
    margin-left: -1%;
}
</style>
</head>
<body>
<div id="col-1">    
{{ Form::open(array('url' => '/Upload','enctype'=>'multipart/form-data','id'=>'frmSub')) }}
{{ Form::hidden('dealerName', $dealerName,array('id'=>'dealerName')) }}
<div class="container" id="wrapper" style="width: 98%;
    float: left;
    background: linear-gradient(-135deg, #c850c0, #4158d0);
    border: 1px solid #212229;
    margin: 1% 1%;
    color: white;margin-top: 5%;">
  <h3 style="text-align: center;font-weight: bold;color: #f8f9fa;background-color: #9990; margin-bottom:3%">Design 2</h3>
  <div class="row"> <div class='col-sm-5'>Background 1:</div><div class='col-sm-6'><input type="color" id="color" style="border-radius: 5%;width: 32%;"></div></div> <br/>

  <div class="row"> <div class='col-sm-5'>Background Color 2 : </div><div class='col-sm-6'><input type="color" id="color1" class="column2" style="border-radius: 5%;width: 32%;"> </div></div><br/>
  <div class="row "><div class='col-sm-5'>Font Color : </div> <div class='col-sm-6'><input type="color" id="font_color" class="column2" style="border-radius: 5%;width: 32%;"></div></div><br/>
  <input type="hidden" name="bgcolor" id="bg_color">
  <input type="hidden" name="bgcolor1" id="bg_color1">
    <input type="hidden" name="fontcolor" id="ft_color">
    <input type="hidden" value="{{ csrf_token() }}" name="_token">
    <input type="hidden" name="template_name" value="2">
        <div class="row ">  <div class='col-sm-5'> Background Image :</div> <div class='col-sm-5'><input type="file" class="column2 btn1" name="background" id="myFile"  onchange="myFunction(this)"></div></div><br/>
                    @if ($errors->has('background'))
          <span class="help-block">
              <strong>{{ $errors->first('background') }}</strong>
          </span>
        @endif          
        <div class="row"> <div class='col-sm-5'>Logo:</div><div class='col-sm-6'><input type="file" name="logo" id="myFile1" class="btn1" name="logo" onchange="readURL(this)"></div></div>
        @if ($errors->has('logo'))
        <span class="help-block">
        <strong>{{ $errors->first('logo') }}</strong>
        </span>
        @endif
        <div class="row" style="padding-top: 35px;margin-left: 40%;margin-bottom: 5%;"><input type="submit" class="btn green" value="Upload" name="submit" style="width: 54%;"></div>
{{ Form::close() }}
</div>
</div>
<div id="col-2">
  <div class="limiter">
    <div class="container-login100"  id="grid">
      <div class="login100-more" style="background-image: url('../../../assets/imgs/background1.png');"></div>

      <div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
        <form class="login100-form validate-form">

           <span style="font-size: 60px; color: #333333;
  display: -webkit-box;
  display: -webkit-flex;
  display: -moz-box;
  display: -ms-flexbox;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 120px;
  margin: 0 auto;">
           <img  class="img-circular" alt="centered image" id="blah"  src="../../../assets/imgs/logo3.png" style="width: 41vh;height: 38vh;margin-bottom: 10vh;" / > 
          </span>

          <span class="login100-form-title p-b-59">
            LOGIN
          </span>

          <div class="wrap-input100 validate-input" data-validate="Name is required">
            <span class="label-input100">User Name</span>
            <input class="input100" type="text" name="name" >
            <span class="focus-input100"></span>
          </div>


          <div class="wrap-input100 validate-input" data-validate = "Password is required">
            <span class="label-input100">Password</span>
            <input class="input100" type="password" name="pass" >
            <span class="focus-input100"></span>
          </div>

          <div class="flex-m w-full p-b-33">
            <div class="contact100-form-checkbox">
              <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
              <label class="label-checkbox100" for="ckb1">
                <span class="txt1">
                  Remember me
                </span>
              </label>
            </div>

            
          </div>

          <div class="container-login100-form-btn">
            <div class="wrap-login100-form-btn">
              <div class="login100-form-bgbtn"></div>
              <button class="login100-form-btn">
                Sign In
              </button>
            </div>
            <a href="#" class="dis-block txt3 hov1" style="margin-top: 30%;font-size: 76%;text-align: center;">
              Forgot Password?
              <i class="fa fa-long-arrow-right m-l-5"></i>
            </a>
          </div>


        </form>
      </div>
    </div>
  </div>
</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
      $('#frmSub').on('submit', function(e) {
        var bgIMG=$('#myFile').val();
        var bgLogo=$('#myFile1').val();
        if(bgIMG==''){
            alert("Select the background Image");
            return false;
        }else if(bgLogo==''){
           alert("Select the Logo Image");
           e.preventDefault();
           return false;
        }else{
          var extensionLogo = bgLogo.split('.').pop().toUpperCase();
          var extension = bgIMG.split('.').pop().toUpperCase();
          if (extension!="BMP" && extension!="PNG" && extension!="JPEG" && extension!="JPG"){
             e.preventDefault();
             alert("You can upload Background Image that are .JPEG, .JPG, or .PNG.");
             return false;
          }
          if (extensionLogo!="BMP" && extensionLogo!="PNG" && extensionLogo!="JPEG" && extensionLogo!="JPG"){
             e.preventDefault();
             alert("You can upload Logo that are .JPEG, .JPG, or .PNG.");
             return false;
          }
        }
        
  });
    var grid = document.getElementById("grid");
    color_input = document.getElementById("color");
     color_input1 = document.getElementById("color1");
    font_color = document.getElementById("font_color");
    

    color_input.addEventListener("change", function() {
        var newdiv = document.createElement("div");
        grid.appendChild(newdiv);
        newdiv.style.backgroundColor = color_input.value;
        $(".wrap-login100").css('background',"linear-gradient(-135deg,"+color_input.value+","+font_color.value+")" );
        $("#bg_color").val(color_input.value);
    });
    color_input1.addEventListener("change", function() {
        var newdiv = document.createElement("div");
        grid.appendChild(newdiv);
        newdiv.style.backgroundColor = color_input1.value;
        $(".wrap-login100").css('background',"linear-gradient(-135deg,"+color_input.value+","+color_input1.value+")" );
        $("#bg_color1").val( color_input1.value);
    });
  font_color = document.getElementById("font_color");
  font_color.addEventListener("change", function() {
    var newdiv = document.createElement("div");
    grid.appendChild(newdiv);
    newdiv.style.backgroundColor = font_color.value;
    $(".label-input100").css('color', font_color.value);
    $(".login100-form-title").css('color', font_color.value);
    $(".txt1").css('color', font_color.value);
    $(".login100-form-btn").css('color', font_color.value);
     $(".dis-block").css('color', font_color.value);
    $("#ft_color").val( font_color.value);
  });

    function readURL(input) {
        console.log("test");
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(316)
                        .height(289);
                };

                reader.readAsDataURL(input.files[0]);
            }
      }
function myFunction(input) {
   var file = document.getElementById("myFile").files[0];
   var reader = new FileReader();
   reader.onloadend = function(){ 
      $(".login100-more").css('background', "url(" + reader.result + ")");       
   }
   if(file){
      reader.readAsDataURL(file);
    }else{
      console.log('Failed');
    }
}

  </script> 
<!--===============================================================================================-->
  <script src="../../../vendor/jquery13/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script src="../../../vendor/animsition13/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script src="../../../vendor/bootstrap13/js/popper.js"></script>
  <script src="../../../vendor/bootstrap13/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script src="../../../vendor/select213/select2.min.js"></script>
<!--===============================================================================================-->
  <script src="../../../vendor/daterangepicker13/moment.min.js"></script>
  <script src="../../../vendor/daterangepicker13/daterangepicker.js"></script>
<!--===============================================================================================-->
  <script src="../../../vendor/countdowntime13/countdowntime.js"></script>
<!--===============================================================================================-->
  <script src="../../../js/main13.js"></script>

</body>
</html>