<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<!DOCTYPE html><html lang='en' class=''>
<head><script src='//production-assets.codepen.io/assets/editor/live/console_runner-079c09a0e3b9ff743e39ee2d5637b9216b3545af0de366d4b9aad9dc87e26bfd.js'></script><script src='//production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js'></script><script src='//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js'></script><meta charset='UTF-8'><meta name="robots" content="noindex"><link rel="shortcut icon" type="image/x-icon" href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" /><link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" /><link rel="canonical" href="https://codepen.io/marcruecker/pen/YWwWem?limit=all&page=26&q=editor" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.3/ace.js"></script>
<script src="https://use.fontawesome.com/1bc4308c1f.js"></script>

<style class="cp-pen-styles">*{margin:0;padding:0;box-sizing:border-box;}

body,html{
    height:100%;
}

#wrap{
    height:100%;
    position:relative;
    padding:0px 50px;
    background:#ccc;
}

#coder{ 
    display:block;
    position:relative;
    width:100%;
    height:30%;
    border:3px solid red;
}
#preview{
    display:block;
    position:relative;
    width:100%;
    height:50%;
    border:3px solid blue;
    background:#fff;
}
#log_BG{
    display:block;
    position:relative;
    width:100%;
    height:10%;
    border:3px solid red;
    background:#fff;
}
#preview iframe{
        width:100%;
        height:100%;
        border:none;
}
#status{
    padding:25px 0;
    height:50px;
}</style></head><body>

{{ Form::open(array('url' => '/sourceCode','enctype'=>'multipart/form-data','id'=>'frmSub')) }}
{{ Form::hidden('dealerName', $dealerName,array('id'=>'dealerName')) }}

<div id="wrap" style="overflow: auto;">
 <input type="hidden" name="template_name" value="4">
<div id="status">
    <div class="row">
        <div class="col-sm-9" > <p><i class="fa fa-spinner fa-spin fa-fw"></i> generating preview</p></div>
        <div class="col-sm-3" ><button type="submit" style="margin-left: 40%;margin-bottom: 1vh;background-color: aqua;width: 50%;" class="btn">APPLY</button></div>
    </div>
   
   
</div>
 <input type="hidden" value="{{ csrf_token() }}" name="_token">
  <input type="hidden" name="template_name" value="4">
<p style="color: red;"> </p>
<div id="coder">
    
</div>

<div id="preview">

</div>
<div id="log_BG">
     <div class="row">
  <div class="col-sm-2" style="margin-left: 5%;">Background Image:- <input type="file" class="column2 btn1" name="background" id="myFile"  onchange="readURL1(this)"></div>
  <div class="col-sm-3"> <img id="blah" style="width: 10vh;height: 10vh;margin-top: 1vh;" src="#" alt="your image" /></div>
  <div class="col-sm-3" > Logo :- <input type="file" id="myFile1" class="column2 btn1" name="logo"   onchange="readURL(this)"></div>
  <div class="col-sm-3" ><img id="blah1" src="#" alt="your image" style="width: 10vh;height: 10vh;margin-top: 1vh;" /></div>
</div>
</div>
<div id="preview1" style="display: none;">
   <textarea id="source" name="sourceCode">
       
   </textarea>
</div>
</div>

{{ Form::close() }}

    
<script src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script>
<script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
var editor = ace.edit("coder");
// editor.setTheme("ace/theme/monokai");
editor.getSession().setMode("ace/mode/html");
editor.getSession().setUseWorker(false);

function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah1')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
}
function readURL1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
}
$('#frmSub').on('submit', function(e) {
        var bgIMG=$('#myFile').val();
        var bgLogo=$('#myFile1').val();

        var extensionLogo = bgLogo.split('.').pop().toUpperCase();
        var extension = bgIMG.split('.').pop().toUpperCase();
        if(bgIMG==''){
            alert("Select the Background Image");
            e.preventDefault();
            return false;
        }else if (extension!="BMP" && extension!="PNG" && extension!="JPEG" && extension!="JPG"){
             e.preventDefault();
             alert("You can upload Background Image that are .JPEG, .JPG, or .PNG.");
             return false;
        } 
        if(bgLogo==''){
           alert("Select the Logo Image");
           e.preventDefault();
           return false;
        }else{
            if (extensionLogo!="BMP" && extensionLogo!="PNG" && extensionLogo!="JPEG" && extensionLogo!="JPG"){
             e.preventDefault();
             alert("You can upload Logo that are .JPEG, .JPG, or .PNG.");
             return false;
          }
        }
  });


//editor.setTheme("ace/theme/monokai");

/*$('#frmSub').on('submit', function(e) {
     var code = editor.getValue();
     var iframe = document.createElement('iframe');
     var preview1 = document.getElementById('preview1');
     var content = '<!doctype html> ' + code;
     document.getElementById("source").value =content;
     preview1.appendChild(iframe);
     iframe.contentWindow.document.open('text/htmlreplace');
     iframe.contentWindow.document.write(content);
     iframe.contentWindow.document.close();

    var lines = editor.session.doc.getAllLines()
    var fooLineNumbers = false;
    var fooLineNumbers1 = false;
    for (var i = 0, l = lines.length; i < l; i++) {
        if (lines[i].indexOf("<body>") != -1){
           fooLineNumbers=true;
           break;
        }else{
            fooLineNumbers=false;
        }
    }
    for (var i = 0, l = lines.length; i < l; i++) {
        if (lines[i].indexOf("</body>") != -1){
           fooLineNumbers1=true;
           break;
        }else{
            fooLineNumbers1=false;
        }
    }
    var fooLineNumber = false;
    var fooLineNumber1 = false;
    for (var i = 0, l = lines.length; i < l; i++) {
        if (lines[i].indexOf("form") != -1){
           fooLineNumbers=true;
           break;
        }else{
            fooLineNumber=false;
        }
    }
    for (var i = 0, l = lines.length; i < l; i++) {
        if (lines[i].indexOf("/form") != -1){
           fooLineNumbers1=true;
           break;
        }else{
            fooLineNumber1=false;
        }
    }



    if(fooLineNumbers!=true || fooLineNumbers1!=true){
        alert('Please update body tag !!!');
        e.preventDefault();
        return false;
    }else if(fooLineNumber!=true){
        alert('Please update form tag !!!');
        e.preventDefault();
        return false;
    }else{
        var frm = iframe.contentWindow.document.getElementsByTagName("form");
        var n1=iframe.contentWindow.document.frm[0].getElementsByTagName("input").length;
        var elem = iframe.contentWindow.document.frm[0].getElementsByTagName("input");
        var btn = iframe.contentWindow.document.frm[0].getElementsByTagName("button");

        if(n1!=3){  
            alert('Please update only 3 input field');
            e.preventDefault();
            return false;
        }else if(btn.length<1){
            alert('Please update button field for submit and forget password');
            e.preventDefault();
            return false;
        }else if(btn.length>2){
            alert('Please update only 2 button field for submit and forget password');
            e.preventDefault();
            return false;
        }else{
            if(elem[0].type!="text"){
                alert("Please update input type as text in "+elem[0].outerHTML);
                e.preventDefault();
                return false;
            }else if(elem[1].type!="password"){
                alert("Please update input type as password in "+elem[1].outerHTML);
                e.preventDefault();
                return false;
            }else if(elem[2].type!="checkbox"){
                alert("Please update input type as checkbox in "+elem[2].outerHTML);
                e.preventDefault();
                return false;
            }else{
                return true;
            }else if(btn[0].type!="submit"){
                 alert("Please update button type as submit in "+btn[0].outerHTML);
                 return false;
            }
       }

    }
});*/

function myFunction(){  
    
    $("#status p").delay(500).fadeOut(200);
    
    var code = editor.getValue();
    
    var iframe = document.createElement('iframe');
    
    var preview = document.getElementById('preview');
    var content = '<!doctype html> ' + code;
    document.getElementById("source").value =content;
    preview.appendChild(iframe);
    
    iframe.contentWindow.document.open('text/htmlreplace');
    iframe.contentWindow.document.write(content);
    iframe.contentWindow.document.close();
   
  /* var str = '';
   var elem = iframe.contentWindow.document.getElementsByTagName("input");
   for (var i = 0; i < elem.length; i++) {
    if(i==0){
        if(elem[i].type!="text"){
            alert("must be text");
        }
    }
     str += "<b>Type:</b>" + elem[i].type + "&nbsp&nbsp";
     str += "<b>Name:</b>" + elem[i].name + "&nbsp;&nbsp;";
     str += "<b>Value:</b><i>" + elem[i].value + "</i>&nbsp;&nbsp;";
     str += "<BR>";
   }*/

}

myFunction();

var timeout;

$('#search-form .search-terms').on('keydown', function(e){
    // get keycode of current keypress event
    var code = (e.keyCode || e.which);

    // do nothing if it's an arrow key
    if(code == 37 || code == 38 || code == 39 || code == 40) {
        return;
    }

    // do normal behaviour for any other key
    $('#search-items #autocom').fadeIn();
});

$('iframe').load(function() {
    var src = $('iframe').contents().find("html").html();
    alert(src);
});

$("#coder").on('keyup', function() {
    
    $("#status p").fadeIn(200);
    
    console.log("yea");

    if(timeout) {
        clearTimeout(timeout);
        timeout = null;
    }

    $("#preview").empty();
    timeout = setTimeout(myFunction, 500)

}); 
//# sourceURL=pen.js
</script>
</body></html>