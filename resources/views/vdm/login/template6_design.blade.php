<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login V3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style type="text/css">
        #col-1 {
            position: relative;
            width: 35%;
            float: left;
            height: 100%;
            z-index: 1010101010;
            margin-left: 5%;
            margin-top: 2%;
        }

        #col-2 {
            position: relative;
            width: 60%;
            float: left;
            height: 100%;
            z-index: 1010101010;
            margin-bottom: 5%;
            overflow: auto;
        }

        .btn1 {
            border: 2px solid gray;
            color: gray;
            background-color: white;
            padding: 7px 8px;
            border-radius: 8px;
            font-size: 10px;
            font-weight: bold;
            margin-left: -1%;
        }

    </style>
    <style>
        :root {
            --theme:#f26f21;
            --font: #f26f21;
            --white: #ffffff;
            --size: 20px;
            --height:70px;
        }

        .container-login100 {
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        h1,
        input::-webkit-input-placeholder,
        button {
            font-family: "roboto", sans-serif;
            -webkit-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }

        h1 {
            height: var(--height);
            width: 100%;
            font-size: 18px;
            background: var(--theme);
            color: var(--white);
            line-height: 150%;
            border-radius: 5px 5px 0 0;
            box-shadow: 0 2px 5px 1px rgba(0, 0, 0, 0.2);
        }

        img {
            height: var(--height);
            width: auto;
        }

        .input-content {
            box-sizing: border-box;
            width: 250px;
            box-shadow: 2px 2px 5px 1px rgba(0, 0, 0, 0.2);
            background-color: var(--white);
            padding-bottom: 20px;
            border-radius: 10px;
            margin-top: 10%;
        }

        .input-content-wrap {
            margin-top: var(--size);
        }

        .input::before {
            content: "";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 300px;
            background: var(--font);
        }

        .input-content {
            position: relative;
            background: var(--white);
            z-index: 10;
        }

        .input-content .inputbox {
            overflow: hidden;
            position: relative;
            padding: 0 var(--size);
            padding-top: 15px;
        }

        .input-content .inputbox-title {
            position: absolute;
            top: 15px;
            left: 0;
            width: 200px;
            height: 30px;
            color: #666;
            font-weight: bold;
            line-height: 30px;
        }

        .input-content .inputbox-content {
            position: relative;
            width: 100%;
        }

        .input-content .inputbox-content input {
            width: 100%;
            height: 30px;
            box-sizing: border-box;
            line-height: 30px;
            font-size: 14px;
            border: 0;
            background: none;
            border-bottom: 1px solid var(--font);
            color: var(--font);
            outline: none;
            border-radius: 0;
            -webkit-appearance: none;
        }

        .input-content .inputbox-content input:focus~label,
        .input-content .inputbox-content input:valid~label {
            color: var(--font);
            transform: translateY(-20px);
            font-size: 0.825em;
            cursor: default;
        }

        .input-content .inputbox-content input:focus~.underline {
            width: 100%;
        }

        .input-content .inputbox-content label {
            position: absolute;
            top: 0;
            left: 0;
            height: 30px;
            line-height: 30px;
            color: #ccc;
            cursor: text;
            transition: all 200ms ease-out;
            z-index: 10;
        }

        .input-content .inputbox-content .underline {
            content: "";
            display: block;
            position: absolute;
            bottom: -1px;
            left: 0;
            width: 0;
            height: 2px;
            background: var(--font);
            transition: all 200ms ease-out;
        }

        /* btn */
        .input-content .btns {
            display: flex;
        }

        .input-content .btns .btn {
            display: inline-block;
            margin-right: 2px;
            background: none;
            border: 1px solid #c0c0c0;
            border-radius: 2px;
            color: #666;
            font-size: 12px;
            outline: none;
            transition: all 100ms ease-out;
        }

        .input-content .btns .btn:hover,
        .input-content .btns .btn:focus {
            transform: translateY(-3px);
        }

        .input-content .btns .btn-confirm {
            border: 1px solid var(--theme);
            background: var(--white);
            color: var(--theme);
            margin: auto;
            border-radius: 10px;
        }

        .input-content .btns .btn-confirm:hover {
            background: var(--theme);
            color: var(--white);
        }

        .custom-control-input+label {
            color: #c0c0c0 !important;

        }

        .custom-control-input:checked+label {
            color: var(--font) !important;
            font-weight: 500;
        }

        .remember-me {
            margin: 0;
            margin: 0 var(--size);
        }

        select {
            border: 1px solid var(--theme);
            padding: 2px;
            border-radius: 10px;
        }

        select:-moz-focusring {
            color: transparent;
            text-shadow: 0 0 0 #000;
        }
        a,label{
            font-size: 14px;
        }
        a:hover {
            color: var(--font);
        }
    </style>
</head>

<body>
    <div id="stylefont">
        <style>
        </style>
    </div>
    <div id="styletheme">
        <style>
        </style>
    </div>
    <div id="col-1">
        {{ Form::open(['url' => '/Upload', 'enctype' => 'multipart/form-data', 'id' => 'frmSub']) }}
        {{ Form::hidden('dealerName', $dealerName, ['id' => 'dealerName']) }}
        <div class="container" style="float: left;
    background: -webkit-linear-gradient(top, #7579ff, #b224ef);
    border: 1px solid #212229;
    border-radius: 1%;
    margin: 1% -2%;
    color: white;" id="wrapper">

            <h3 style="text-align: center;font-weight: bold;color: #f8f9fa;background-color: #9990;margin-bottom:3%">
                Design 1</h3>
            <div>

                <div class="row">
                    <div class='col-sm-5'>Background Color 1 : </div>
                    <div class='col-sm-6'><input type="color" id="color" class="column2"
                            style="border-radius: 5%;width: 32%;"> </div>
                </div><br />
                <div class="row">
                    <div class='col-sm-5'>Background Color 2 : </div>
                    <div class='col-sm-6'><input type="color" id="color1" class="column2"
                            style="border-radius: 5%;width: 32%;"> </div>
                </div>
                <br />
                <div class="row ">
                    <div class='col-sm-5'>Font Color : </div>
                    <div class='col-sm-6'><input type="color" id="font_color" class="column2"
                            style="border-radius: 5%;width: 32%;"></div>
                </div><br />
                <div class="row ">
                    <div class='col-sm-5'>Web Theme Color : </div>
                    <div class='col-sm-6'><input type="color" id="theme_color" class="column2"
                            style="border-radius: 5%;width: 32%;"></div>
                </div><br />

                <input type="hidden" name="bgcolor" id="bg_color">
                <input type="hidden" name="bgcolor1" id="bg_color1">
                <input type="hidden" name="fontcolor" id="ft_color">
                <input type="hidden" name="themecolor" id="themecolor">
                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                <!--    <input type="hidden" name="web_address" value="live.vamosys.com1"> -->
                <input type="hidden" name="template_name" value="6">
                <div class="row ">
                    <div class='col-sm-5'> Background Image :</div>
                    <div class='col-sm-5'><input type="file" class="column2 btn1" name="background" id="myFile"
                            onchange="myFunction(this)"></div>
                </div><br />
                @if ($errors->has('background'))
                    <span class="help-block">
                        <strong>{{ $errors->first('background') }}</strong>
                    </span>
                @endif
                <div class="row ">
                    <div class='col-sm-5'>Logo : </div>
                    <div class='col-sm-5'><input type="file" id="myFile1" class="column2 btn1" name="logo"
                            onchange="readURL(this)"></div>
                </div>
                @if ($errors->has('logo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('logo') }}</strong>
                    </span>
                @endif
            </div>
            <div class="row" style="padding-top: 35px;margin-left: 40%;"> <input type="submit" class="btn green"
                    value="Upload" onclick="rrmdir()" name="submit"></div>
        </div>
        {{ Form::close() }}
    </div>
    <div id="col-2">
        <div class="container-login100" style="background-image: url('../../../assets/imgs/background3.png');">
                <form class="input-content wrap-login100">
        
                    <h1 class="d-flex justify-content-around">
                        <img src="../../../assets/imgs/logo3.png" id="blah"
                            alt="logo">
                    </h1>
                    <div class="input-content-wrap">
                        <dl class="inputbox">
                            <dd class="inputbox-content">
                                <input id="input0" type="text" required />
                                <label for="input0"><i class="fa fa-user"></i> &nbsp;User Name</label>
                                <span class="underline"></span>
                            </dd>
                        </dl>
                        <dl class="inputbox">
                            <dd class="inputbox-content">
                                <input id="input1" type="password" required />
                                <label for="input1"><i class="fa fa-lock"></i> &nbsp;Password</label>
                                <span class="underline"></span>
                            </dd>
                        </dl>
                        <div class="custom-control custom-checkbox my-1 mr-sm-2 remember-me">
                            <input type="checkbox" class="custom-control-input" id="customControlInline">
                            <label class="custom-control-label" for="customControlInline">Remember Me</label>
                        </div>
                        <div class="btns mt-3 ">
                            <button class="btn btn-confirm">LOGIN</button>
                        </div>
        
                        <div class="d-flex justify-content-around align-items-center mt-3">
                            <select name="lang" class="">
                                <option value="en">English</option>
                                <option value="hi">Hindi</option>
                                <option value="es">Spanish</option>
                            </select>
                            <a href="#"> Forgot Password? </a>
                        </div>
                    </div>
                </form>
        </div>
    </div>

    <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
    <script src="../../../vendor/jquery4/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="../../../vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="../../../vendor/bootstrap3/js/popper.js"></script>
    <script src="../../../vendor/bootstrap3/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="../../../vendor/select24/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="../../../vendor/daterangepicker/moment.min.js"></script>
    <script src="../../../vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="../../../vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="../../../js/main1.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script type="text/javascript">
        $('#frmSub').on('submit', function(e) {
            var bgIMG = $('#myFile').val();
            var bgLogo = $('#myFile1').val();
            if (bgIMG == '') {
                alert("Select the background Image");
                return false;
            } else if (bgLogo == '') {
                alert("Select the Logo Image");
                e.preventDefault();
                return false;
            } else {
                var extensionLogo = bgLogo.split('.').pop().toUpperCase();
                var extension = bgIMG.split('.').pop().toUpperCase();
                if (extension != "BMP" && extension != "PNG" && extension != "JPEG" && extension != "JPG") {
                    e.preventDefault();
                    alert("You can upload Background Image that are .JPEG, .JPG, or .PNG.");
                    return false;
                }
                if (extensionLogo != "BMP" && extensionLogo != "PNG" && extensionLogo != "JPEG" && extensionLogo !=
                    "JPG") {
                    e.preventDefault();
                    alert("You can upload Logo that are .JPEG, .JPG, or .PNG.");
                    return false;
                }
            }

        });
        var grid = document.getElementById("grid");
        color_input = document.getElementById("color");
        color_input1 = document.getElementById("color1");
        color_input.addEventListener("change", function() {
            
            $(".wrap-login100").css('background', "linear-gradient(-160deg," + color_input.value + "," +
                color_input1.value + ")");
            $("#bg_color").val(color_input.value);
        });

        color_input1.addEventListener("change", function() {
            
            $(".wrap-login100").css('background', "linear-gradient(-160deg," + color_input.value + "," +
                color_input1.value + ")");
            $("#bg_color1").val(color_input1.value);
        });

        font_color = document.getElementById("font_color");
        font_color.addEventListener("change", function() {
            $("#ft_color").val( font_color.value);
            change_placeholder_color('font',font_color.value);
        });
        $('#theme_color').on('change', function() {
            $("#themecolor").val($(this).val());
            change_placeholder_color('theme',$(this).val());
        });

        function change_placeholder_color(type,color_choice) {
            $("#style"+type+">style").remove();
            $("#style"+type).append("<style>" + ':root{ --'+type+':' + color_choice +
                " !important;}</style>")
        }

        function resizeIframe(obj) {
            obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
        }

        function myFunction(input) {
            var file = document.getElementById("myFile").files[0];
            var reader = new FileReader();
            reader.onloadend = function() {
                $(".container-login100").css('background', "url(" + reader.result + ")");
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                console.log('Failed');
            }
        }

        function readURL(input) {
            console.log("test");
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

</body>

</html>
