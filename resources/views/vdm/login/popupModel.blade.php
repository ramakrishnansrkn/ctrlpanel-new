<!DOCTYPE html>
<html lang="en">
<head>
    <title>GPS</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/bootstrap_model/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0-model/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/iconic_model/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../fonts/Linearicons-Free-v1.0.0-model/icon-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animate_model/animate.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers_model/hamburgers.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/animsition_model/css/animsition.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/select2_model/select2.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker_model/daterangepicker.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../css/util_model.css">
    <link rel="stylesheet" type="text/css" href="../css/main_model.css">
<!--===============================================================================================-->
</head>
<body>
    <div class="container-contact100">
        <div class="wrap-contact100">
            <div class="contact100-form-title" style="background-image: url(images/bg-02.jpg);">
                <span>Password Reset</span> 
            </div>
             <div class="contact100-form-title1" style="background-image: url(images/bg-02.jpg);  font-size: initial;   ">
              <span>We will send an Password Reset Link through e-mail</span>
            </div>
       
            <form class="contact100-form validate-form" action="../password/resetting">
                <div class="wrap-input100 validate-input">
                    <input id="name" class="input100" type="text" name="uname" placeholder="User Name">
                    <span class="focus-input100"></span>
                    <label class="label-input100" for="name">
                        <span class="lnr lnr-user m-b-2"></span>
                    </label>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="container-contact100-form-btn">
                            <button id="cancel" class="contact100-form-btn" style="background: red;">
                              Cancel 
                            </button>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="container-contact100-form-btn">
                            <button class="contact100-form-btn" style="background: darkblue;">
                              Send Now 
                            </button>
                        </div>
                    </div>
                </div>
                
                
            </form>
        </div>
    </div>
<!--===============================================================================================-->
    <script src="../vendor/jquery_model/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="../vendor/animsition_model/js/animsition.min.js"></script>
<!--===============================================================================================-->
    <script src="../vendor/bootstrap_model/js/popper.js"></script>
    <script src="../vendor/bootstrap_model/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script src="../vendor/select2_model/select2.min.js"></script>
<!--===============================================================================================-->
    <script src="../vendor/daterangepicker_model/moment.min.js"></script>
    <script src="../vendor/daterangepicker_model/daterangepicker.js"></script>
<!--===============================================================================================-->
    <script src="../vendor/countdowntime_model/countdowntime.js"></script>
<!--===============================================================================================-->
    <script src="../js/main_model.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-23581568-13');
    </script>
</body>
<script type="text/javascript">
$('#cancel').click(function(){
 
     window.location.href = '{{url("login")}}';   


  });
</script>
</html>
