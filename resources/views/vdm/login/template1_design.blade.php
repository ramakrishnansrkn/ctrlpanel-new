<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login V3</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
  <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../vendor/bootstrap3/css/bootstrap.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../fonts1/font-awesome-4.7.1/css/font-awesome.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../fonts1/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../vendor/animate3/animate.css">
<!--===============================================================================================-->  
   <link rel="stylesheet" type="text/css" href="../../../vendor/css-hamburgers4/hamburgers.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../vendor/select24/select2.min.css">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="../../../vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="../../../css/util1.css">
  <link rel="stylesheet" type="text/css" href="../../../css/main1.css">
<!--===============================================================================================-->
<style type="text/css">
  #col-1 {
    position: relative;
    width: 35%;
    float: left;
    height: 100%;
    z-index: 1010101010;
    margin-left: 5%;
    margin-top: 2%;
}

#col-2 {
  position: relative;
  width: 60%;
  float: left;
  height: 100%;
  z-index: 1010101010;
  margin-bottom: 5%;
  overflow: auto;
}
.btn1 {
    border: 2px solid gray;
    color: gray;
    background-color: white;
    padding: 7px 8px;
    border-radius: 8px;
    font-size: 10px;
    font-weight: bold;
    margin-left: -1%;
}
</style>
</head>
<body>
  <div id="col-1"> 
      {{ Form::open(array('url' => '/Upload','enctype'=>'multipart/form-data','id'=> 'frmSub')) }}
       {{ Form::hidden('dealerName', $dealerName,array('id'=>'dealerName')) }}
     <div  class="container" style="float: left;
    background: -webkit-linear-gradient(top, #7579ff, #b224ef);
    border: 1px solid #212229;
    border-radius: 1%;
    margin: 1% -2%;
    color: white;" id="wrapper">

        <h3 style="text-align: center;font-weight: bold;color: #f8f9fa;background-color: #9990;margin-bottom:3%">Design 1</h3>
        <div >
            
              <div class="row"> <div class='col-sm-5'>Background Color 1 : </div><div class='col-sm-6'><input type="color" id="color" class="column2" style="border-radius: 5%;width: 32%;"> </div></div><br/>
              <div class="row"> <div class='col-sm-5'>Background Color 2 : </div><div class='col-sm-6'><input type="color" id="color1" class="column2" style="border-radius: 5%;width: 32%;"> </div></div>
              <br/>
              <div class="row ">   <div class='col-sm-5'>Font Color : </div> <div class='col-sm-6'><input type="color" id="font_color" class="column2" style="border-radius: 5%;width: 32%;"></div></div><br/>
                <input type="hidden" name="bgcolor" id="bg_color">
                <input type="hidden" name="bgcolor1" id="bg_color1">
                <input type="hidden" name="fontcolor" id="ft_color">
                <input type="hidden" value="{{ csrf_token() }}" name="_token">
             <!--    <input type="hidden" name="web_address" value="live.vamosys.com1"> -->
                <input type="hidden" name="template_name" value="1">
               <div class="row ">  <div class='col-sm-5'> Background Image :</div> <div class='col-sm-5'><input type="file" class="column2 btn1" name="background" id="myFile"  onchange="myFunction(this)"></div></div><br/>
                    @if ($errors->has('background'))
                    <span class="help-block">
                        <strong>{{ $errors->first('background') }}</strong>
                    </span>
                    @endif
                    <div class="row "> <div class='col-sm-5'>Logo : </div><div class='col-sm-5'><input type="file" id="myFile1" class="column2 btn1" name="logo" onchange="readURL(this)"></div></div>
                    @if ($errors->has('logo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('logo') }}</strong>
                    </span>
                    @endif
                  </div>
                  <div class="row" style="padding-top: 35px;margin-left: 40%;">  <input type="submit" class="btn green"  value="Upload" onclick="rrmdir()" name="submit"></div>
    </div>
     {{ Form::close() }}
  </div>
  <div id="col-2">
  <div class="limiter">
    <div class="container-login100" style="background-image: url('../../../assets/imgs/background3.png');" >
      <div class="wrap-login100" id="grid">
        <form class="login100-form validate-form">
          <span class="login100-form-logo">
            <img id="blah" style="width: 100%;height: 100%;" src="../../../assets/imgs/logo3.png"  / >
          </span>

          <span class="login100-form-title p-b-34 p-t-27">
            LOGIN
          </span>

          <div class="wrap-input100 validate-input" data-validate = "Enter username">
            <input class="input100" type="text" name="username" placeholder="Username">
            <span class="focus-input100" data-placeholder="&#xf207;"></span>
          </div>

          <div class="wrap-input100 validate-input" data-validate="Enter password">
            <input class="input100" type="password" name="pass" placeholder="Password">
            <span class="focus-input100" data-placeholder="&#xf191;"></span>
          </div>

          <div class="contact100-form-checkbox">
            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
            <label class="label-checkbox100" for="ckb1">
              Remember me
            </label>
          </div>

          <div class="container-login100-form-btn">
            <button class="login100-form-btn">
              Login
            </button>
          </div>

          <div class="text-center p-t-34">
            <a class="txt1" href="#">
              Forgot Password?
            </a>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>

  <div id="dropDownSelect1"></div>
  
<!--===============================================================================================-->
   <script src="../../../vendor/jquery4/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
 <script src="../../../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script src="../../../vendor/bootstrap3/js/popper.js"></script>
  <script src="../../../vendor/bootstrap3/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
   <script src="../../../vendor/select24/select2.min.js"></script>
<!--===============================================================================================-->
 <script src="../../../vendor/daterangepicker/moment.min.js"></script>
  <script src="../../../vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
   <script src="../../../vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
  <script src="../../../js/main1.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
  $('#frmSub').on('submit', function(e) {
        var bgIMG=$('#myFile').val();
        var bgLogo=$('#myFile1').val();
        if(bgIMG==''){
            alert("Select the background Image");
            return false;
        }else if(bgLogo==''){
           alert("Select the Logo Image");
           e.preventDefault();
           return false;
        }else{
          var extensionLogo = bgLogo.split('.').pop().toUpperCase();
          var extension = bgIMG.split('.').pop().toUpperCase();
          if (extension!="BMP" && extension!="PNG" && extension!="JPEG" && extension!="JPG"){
             e.preventDefault();
             alert("You can upload Background Image that are .JPEG, .JPG, or .PNG.");
             return false;
          }
          if (extensionLogo!="BMP" && extensionLogo!="PNG" && extensionLogo!="JPEG" && extensionLogo!="JPG"){
             e.preventDefault();
             alert("You can upload Logo that are .JPEG, .JPG, or .PNG.");
             return false;
          }
        }
        
  });
    var grid = document.getElementById("grid");
    color_input = document.getElementById("color");
    color_input1 = document.getElementById("color1");
color_input.addEventListener("change", function() {
  var newdiv = document.createElement("div");
  grid.appendChild(newdiv);
  newdiv.style.backgroundColor = color_input.value;
  $(".wrap-login100").css('background',"linear-gradient(-160deg,"+color_input.value+","+color_input1.value+")" );
  $("#bg_color").val(color_input.value);
});

color_input1.addEventListener("change", function() {
  var newdiv = document.createElement("div");
  grid.appendChild(newdiv);
  newdiv.style.backgroundColor = color_input1.value;
   $(".wrap-login100").css('background',"linear-gradient(-160deg,"+color_input.value+","+color_input1.value+")" );
  $("#bg_color1").val(color_input1.value);
});

font_color = document.getElementById("font_color");
font_color.addEventListener("change", function() {
  var newdiv = document.createElement("div");
  grid.appendChild(newdiv);
  newdiv.style.backgroundColor = font_color.value;
  $(".login100-form-title").css('color', font_color.value);
  $(".label-checkbox100").css('color', font_color.value);
  $(".txt1").css('color', font_color.value);
  $("input::-webkit-input-placeholder").css('color', font_color.value);
  $("#ft_color").val( font_color.value);
});

 function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }

function myFunction(input) {
    var file = document.getElementById("myFile").files[0];
   var reader = new FileReader();
   reader.onloadend = function(){ 
      $(".container-login100").css('background', "url(" + reader.result + ")");       
   }
   if(file){
      reader.readAsDataURL(file);
    }else{
      console.log('Failed');
    }
}

     function readURL(input) {
        console.log("test");
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(150);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

</script>

</body>
</html>








