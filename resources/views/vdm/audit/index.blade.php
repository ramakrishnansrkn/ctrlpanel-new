@include('includes.header_create')

<div id="wrapper">
<div class="content animate-panel">
<div class="row">
    <div class="col-lg-14">
        <div class="hpanel">
         <div class="panel-heading">
          <div class="panel-heading" style="    margin-top: -2%;
    color: inherit;
    font-weight: 600;
    padding: 10px 4px;
    transition: all .3s;
    border: 1px solid transparent;
    margin-bottom: 1%;">
         		@if($model=='AuditDealer')
		        <h4><b>Audit Dealers</b></h4>
		        @elseif($model=='AuditVehicle')
		        <h4><b>Audit Vehicles</b></h4>
		        @elseif($model=='AuditUser')
		        <h4><b>Audit Users</b></h4>
                @elseif($model=='RenewalDetails')
		        <h4><b>Audit Renewal</b></h4>
                @elseif($model=='AuditFrans')
                <h4><b>Audit Admin</b></h4>
                @elseif($model=='AuditOrg')
                <h4><b>Audit Organization</b></h4>
                @elseif($model=='AuditGroup')
                <h4><b>Audit Groups</b></h4>
                 @elseif($model=='loginCustomisation')
                <h4><b>Login Customisation</b></h4>
                @elseif($model=='CustomizeLandingPage')
                <h4><b>Customize Landing Page</b></h4>
		        @else
		        <h4><b>Audit</b></h4>
		        @endif
		    </div>
         
        </div>
        <div class="panel-body" style="margin-top: -2%;overflow-x:auto;">
             @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
          @endif
          <input type="hidden" id="colIndex" name="colIndex" value="<?php echo $indexVal;?>">
        <font color="red"> {{ HTML::ul($errors->all()) }}</font>
     @if($model!='RenewalDetails')
	<table id="exam" class="table table-bordered dataTable sorting_desc">
    @endif
    @if($model=='RenewalDetails')
	<table id="example1" class="table table-bordered dataTable sorting_desc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Processed_Date: activate to sort column ascending" aria-sort="descending">
    @endif
      <thead>
		<tr class="uppercase">
			@foreach($columns as $key => $value)
			@if($value=='country')
			<th style="text-transform: capitalize;">CountryTimeZone</th>
	        @else
			<th id="<?php echo $value; ?>1" style="text-transform: capitalize;">{{$value}}</th>
			@endif
			@endforeach

		</tr>
	</thead>
    <tbody id="myTable">
	@foreach($rows as  $rowId=>$row)
	@if($row['status']==Config::get('constant.created')&&($model=='AuditUser'))
		<tr class="alert alert-success">
			@foreach($columns as $key => $value)
			@if($value=='reports')
					<td>
						<div >
					   @foreach($AllReport[$rowId] as $reportTitle => $Reports)

							@if(count($Reports)>0)
				            <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#<?php echo $reportTitle.$rowId;?>"  style="margin-top: 2%;">{{ $reportTitle }} </button>
				                 
					             <div id="<?php echo $reportTitle.$rowId;?>" class="collapse">
					             	<ul>
					             	@foreach($Reports as $report)
					             	
					                <li style="list-style-type: decimal;">{{ $report }}</li>
					            
					                @endforeach
					              </ul>
					             </div>


				           @endif

		              @endforeach
		          </div>
				   </td>
		      @elseif($value=='addedReports')
					<td>
					   @foreach($AddedReportArr[$rowId] as $reportTitle => $Reports)

							@if(count($Reports)>0)
							<div >
							
				            <input class="btn btn-success" type="button" value="<?php echo $reportTitle;?>" style="margin-top: 5%;"  data-toggle="collapse" data-target="#<?php echo $reportTitle.$rowId;?>add">
				                 
					             <div id="<?php echo $reportTitle.$rowId;?>add" class="collapse">
					             	<ul>
					             	@foreach($Reports as $report)
					             	
					                <li style="list-style-type: decimal;">{{ $report }}</li>
					            
					                @endforeach
					              </ul>
					             </div>
				           </div>


				           @endif

		              @endforeach
				   </td>
			 @elseif($value=='removedReports')
					<td>
					   @foreach($RemovedReportArr[$rowId] as $reportTitle => $Reports)

							@if(count($Reports)>0)
							<div >
							
				            <input class="btn btn-danger" type="button" value="<?php echo $reportTitle;?>" style="margin-top: 5%;"  data-toggle="collapse" data-target="#<?php echo $reportTitle.$rowId;?>rem">
				                 
					             <div id="<?php echo $reportTitle.$rowId;?>rem" class="collapse">
					             	<ul>
					             	@foreach($Reports as $report)
					             	
					                <li style="list-style-type: decimal;">{{ $report }}</li>
					            
					                @endforeach
					              </ul>
					             </div>
				           </div>


				           @endif

		              @endforeach
				   </td>

			@else
			   <td>{{ $row[$value] }}</td>
			@endif
			<!-- <td><span style="word-break: break-word;width: 200px;">{{ $row[$value] }}</span></td> -->
			@endforeach
		</tr>
	@elseif($row['status']==Config::get('constant.created'))
		<tr class="alert alert-success">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.updated'))
		<tr class="alert alert-info">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.deleted'))
		<tr class="alert alert-danger">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['Status']=='OnBoard')
		<tr class="alert alert-success">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['Status']=='Migration')
		<tr class="alert alert-warning">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['Status']=='Renew')
		<tr class="alert alert-info">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.disabled'))
		<tr style="color:#8a8467">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.enabled'))
		<tr style="color:#729493">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.reportsEdited'))
		<tr style="color:#845c84">
			@foreach($columns as $key => $value)
			@if($value=='reports')
					<td>
						<div >
					   @foreach($AllReport[$rowId] as $reportTitle => $Reports)

							@if(count($Reports)>0)
				            <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#<?php echo $reportTitle.$rowId;?>"  style="margin-top: 2%;">{{ $reportTitle }} </button>
				                 
					             <div id="<?php echo $reportTitle.$rowId;?>" class="collapse">
					             	<ul>
					             	@foreach($Reports as $report)
					             	
					                <li style="list-style-type: decimal;">{{ $report }}</li>
					            
					                @endforeach
					              </ul>
					             </div>


				           @endif

		              @endforeach
		          </div>
				   </td>
		      @elseif($value=='addedReports')
					<td>
					   @foreach($AddedReportArr[$rowId] as $reportTitle => $Reports)

							@if(count($Reports)>0)
							<div >
							
				            <input class="btn btn-success" type="button" value="<?php echo $reportTitle;?>" style="margin-top: 5%;"  data-toggle="collapse" data-target="#<?php echo $reportTitle.$rowId;?>add">
				                 
					             <div id="<?php echo $reportTitle.$rowId;?>add" class="collapse">
					             	<ul>
					             	@foreach($Reports as $report)
					             	
					                <li style="list-style-type: decimal;">{{ $report }}</li>
					            
					                @endforeach
					              </ul>
					             </div>
				           </div>


				           @endif

		              @endforeach
				   </td>
			 @elseif($value=='removedReports')
					<td>
					   @foreach($RemovedReportArr[$rowId] as $reportTitle => $Reports)

							@if(count($Reports)>0)
							<div >
							
				            <input class="btn btn-danger" type="button" value="<?php echo $reportTitle;?>" style="margin-top: 5%;"  data-toggle="collapse" data-target="#<?php echo $reportTitle.$rowId;?>rem">
				                 
					             <div id="<?php echo $reportTitle.$rowId;?>rem" class="collapse">
					             	<ul>
					             	@foreach($Reports as $report)
					             	
					                <li style="list-style-type: decimal;">{{ $report }}</li>
					            
					                @endforeach
					              </ul>
					             </div>
				           </div>


				           @endif

		              @endforeach
				   </td>

			@else
			   <td>{{ $row[$value] }}</td>
			@endif
			<!-- <td><span style="word-break: break-word;width: 200px;">{{ $row[$value] }}</span></td> -->
			@endforeach
		</tr>
		@else
		<tr style="color:#6b6666;border-color:#8c8585">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@endif

	@endforeach
	</tbody>
  </table>                
</div>
</div>
</div>
	<style type="text/css">
		tr:nth-child(even) {
  background-color: #f2f2f2
}
	</style>
 
</body>
</html>
 


@include('includes.js_create')

<script type="text/javascript">
 window.onload = function()
 {
   var tabIn = document.getElementById("colIndex").value;  
    var id = 0;
    $('#exam').dataTable( {
        "aaSorting": [[ tabIn, "desc" ]]
    } );
 }

</script>   







