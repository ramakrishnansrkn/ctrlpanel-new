@include('includes.header_create')
<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
			<div class="col-lg-12">
				<div class="hpanel">
				    <div class="panel-heading" align="left">
                  		<h4><b><font> View Tags </font></b></h4>
                	</div>
					<div class="panel-body">
						{{ HTML::ul($errors->all()) }}
						{{ Form::open(array('url' => 'Business/batchSale')) }}
						<div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
							<div class="row">
								<!-- <div class="col-sm-6">
									<div class="col-sm-6">
										<div id="example2_filter" class="dataTables_filter"></div>
									</div>
							</div> 
						<div class="row">-->
							<div class="col-sm-12">

								<!-- <div class="form-group"> -->

									                                 

	                            <table id="example1" class="table table-bordered dataTable">

	                            	<thead>

	                            		<tr style="text-align: center;font-size: 12px">

	                            			<th style="text-align: center;font-size: 12px">Tag ID</th>
	                            			<th style="text-align: center;font-size: 12px">Tag Name</th>
	                            			<th style="text-align: center;font-size: 12px">Mobile Number</th>
											<th style="text-align: center;font-size: 12px">Designation</th>
	                            			<th style="text-align: center;font-size: 12px">Org Name</th>
	                            			<th style="text-align: center;font-size: 12px">Action</th>

	                            		</tr>
	                            	</thead>
	                            	<tbody>
	                            		@if($form=="show")
	                            		@if(isset($values))
	                            		@foreach($values as $key => $value)
	                            		<tr style="text-align: center;font-size: 12px">
	                            			<td style="text-align: center;font-size: 12px">{{ $key }}</td>
	                            			<td style="text-align: center;font-size: 12px">{{ array_get($tagnameList, $key)}}</td>
											<td style="text-align: center;font-size: 12px">{{ array_get($mobileList, $key)}}</td>
											<td style="text-align: center;font-size: 12px">{{ array_get($designationList, $key) }}</td>
											<td>{{ $orgIdUi}}</td>
	                            			<td>
												<a class="btn btn-success" href="{{ URL::to('rfid/'.$key.';'.$orgIdUi.'/edit') }}">Edit</a>
												<a class="btn btn-danger" href="{{ URL::to('rfid/'.$key.';'.$orgIdUi.'/destroy') }}">Delete</a>
                                            </td>
	                            		</tr>
	                            		@endforeach
	                            		@endif
	                            		@else
	                            		<tr style="text-align: center;font-size: 12px">
	                            			<td style="text-align: center;font-size: 12px">{{$tagid}}</td>
	                            			<td style="text-align: center;font-size: 12px">{{$tagname}}</td>
											<td style="text-align: center;font-size: 12px">{{ $mobile}}</td>
											<td style="text-align: center;font-size: 12px">{{ $designation }}</td>
											<td>{{ $orgIdUi}}</td>
	                            			<td>
										<a class="btn btn-success" href="{{ URL::to('rfid/'.$tagid.';'.$orgIdUi.'/edit') }}">Edit</a>
										<a class="btn btn-danger" href="{{ URL::to('rfid/'.$tagid.';'.$orgIdUi.'/destroy') }}">Delete</a>
                                                    </td>
	                            		</tr>
	                            		
	                            		@endif

	                            	</tbody>
	                            </table>

	                       <!--  </div> -->


	                  <!--   </div> -->



	                </div>
	            </div>
	        </div>
	        <br>
	       
	        <font color="blue">
	        	
	        		<br>
	        	</p>     
	    </div>
	</div>
	</div>
	@include('includes.js_create')
	</body>
	</html>