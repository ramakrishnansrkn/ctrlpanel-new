@extends('includes.vdmheader')
@section('mainContent')
<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
			<div class="col-lg-12">
				<div class="hpanel">
					<div class="panel-heading">
						<div style="background-color: #66cc99" class="panel-heading" align="center">
                   		<h4><font size="6px" color="#133926" >Edit Tag</font></h4>
                	</div> 
						</div>
						<hr>
                	{{ HTML::ul($errors->all()) }}
                	{{ Form::open(array('url' => 'rfid/update')) }}
                	<div class="panel-body">
                        
                		<div class="row">
                			<div class="col-md-2"></div>
                			<div class="col-md-3">{{ Form::label('tagid', 'Tag Id') }}</div>
                			<div class="col-md-4">{{ Form::text('tagid', $tagid, array('class' => 'form-control')) }}
							{{ Form::hidden('tagidtemp', $tagid, array('class' => 'form-control')) }}</div>
                		</div>
                    	<br>
                		<div class="row">
                			<div class="col-md-2"></div>
                			<div class="col-md-3">{{ Form::label('tagname', 'Tag Name') }}</div>
                			<div class="col-md-4">{{ Form::text('tagname', $tagname, array('class' => 'form-control')) }}</div>
                		</div>
                		<br>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-3">{{ Form::label('mobile', 'Mobile number') }}</div>
                            <div class="col-md-4">{{ Form::text('mobile', $mobile, array('class' => 'form-control')) }}</div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-3">{{ Form::label('designation', 'Designation') }}</div>
                            <div class="col-md-4">{{ Form::select('designation', array(''=>'Select','Driver' => 'Driver','Helper' => 'Helper','Staff'=>'Staff','Student'=>'Student'), $designation, array('class' => 'form-control')) }}</div>
                        </div>
                        <br>
                		<div class="row"> 
                			<div class="col-md-2"></div>
                			<div class="col-md-3">{{ Form::label('org', 'Org Name') }}</div>
                			<div class="col-md-4">	{{ Form::select('org', $orgList, $orgname,array('class' => 'selectpicker form-control','data-live-search '=> 'true')) }}
                			{{ Form::hidden('orgT', $orgname,array('id' => 'orgid')) }}</div>
                		</div>
                        <br>
                		<div class="row">
                			<div class="col-md-2"></div>
                			<div class="col-md-3"></div>
							<div class="row">
                			<div class="col-md-3">	{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}</div>
							<div class="col-md-2">  {{ Form::submit('cancel',array('class'=>'btn btn-warning'),URL::previous()) }}</div></div>
                			                			
                		</div>
                	</div>
                	{{ Form::close() }}
                	
                </div>
            </div>
        </div>
    </div>
</div>

<div style="top: 0; left: 0;right: 0; padding-top: 150px;" align="center">
	<hr>
	@stop
			
		</body>
		</html>
