@include('includes.header_create')
<div id="wrapper">
<div class="content animate-panel">
	<div class="row">
    	<div class="col-lg-12">
       		<div class="hpanel" style="margin-top: -2%;">
               		<div class="panel-heading">
                   <h4><b><font>Allocate Licence</font></b></h4>
                	</div>
                	<div class="panel-body">
                		<font color="red">{{ HTML::ul($errors->all()) }}</font>                                  
                    	{{ Form::open(array('url' => 'dealers/licenceUpdate','id'=>'sub')) }}

                    	{{Form::hidden('availableBasicLicence',$availableBasicLicence,array('id'=>'avlbc'))}}
                    	{{Form::hidden('availableAdvanceLicence',$availableAdvanceLicence,array('id'=>'avlad'))}}
                    	{{Form::hidden('availablePremiumLicence',$availablePremiumLicence,array('id'=>'avlpr'))}}
                        {{Form::hidden('availablePremPlusLicence',$availablePremPlusLicence,array('id'=>'avlprpl'))}}
                		<div class="row well" style="background: #14cfed6b; padding: 0%;">
                  		<div class="col-lg-3" ><h5 style="padding: 3%;">Basic Available Licence:{{$availableBasicLicence}}</h5></div>
                  		<div class="col-lg-3" ><h5 style="padding: 3%;">Advance Available Licence:{{$availableAdvanceLicence}}</h5></div>
                  		<div class="col-lg-3" ><h5 style="padding: 3%;">Premium Available Licence:{{$availablePremiumLicence}}</h5></div>
                         <div class="col-lg-3" ><h5 style="padding: 3%;">Premium Plus Available Licence:{{$availablePremPlusLicence}}</h5></div>
                 		</div>
                 		<div class="rowNew">
                 			<div class="col-md-2" style="margin-left: -1%;">
		            	    <label for="dealersList">Dealer List :</label></div>
		            	    <div class="col-md-4" style="padding-left: 21px !important; padding-right: 0px !important;">
		            		{{ Form::select('dealersList', $dealersList,null,array('id'=>'dealersList','class' => 'form-control show-menu-arrow selectpicker', 'data-live-search'=> 'true','data-dropup-auto '=>'false' ,'required' => 'required')) }}
		            		</div>
                 		</div>

                 		<div class="row"></div>
                 		<br/>
                 		<div class="row">
                 			<div class="col-md-2">
		            	    <label for="addofBasic">Add Basic Licence :</label></div>
		            	    <div class="col-md-4">
		            		<input class="form-control" placeholder="Quantity" id="addofBasic" min="0" name="addofBasic" type="number">
		            		</div>
                 		</div><br/>
                 		<div class="row">
                 			<div class="col-md-2">
		            	    <label for="addofAdvance">Add Advance Licence :</label></div>
		            	    <div class="col-md-4">
		            		<input class="form-control" placeholder="Quantity" min="0" id="addofAdvance" name="addofAdvance" type="number" >
		            		</div>
                 		</div><br/>
                 		<div class="row">
                 			<div class="col-md-2">
		            	    <label for="addofPremium">Add Premium Licence :</label></div>
		            	    <div class="col-md-4">
		            		<input class="form-control" placeholder="Quantity" min="0" id="addofPremium" name="addofPremium" type="number">
		            		</div>
                 		</div><br/>
                        <div class="row">
                            <div class="col-md-2">
                            <label for="addofPremiumPlus">Add Premium Plus Licence :</label></div>
                            <div class="col-md-4">
                            <input class="form-control" placeholder="Quantity" min="0" id="addofPremiumPlus" name="addofPremiumPlus" type="number">
                            </div>
                            </div>

                 			<input class="btn btn-primary" id="submit" type="submit" value="Submit" style="width: 8%;margin-left: 25%;margin-top: 2%;">
                 			{{ Form::close() }}
					</div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
 $('#sub').on('submit', function(e) {
  var enter={
      'bc':$('#addofBasic').val(),
      'ad':$('#addofAdvance').val(),
      'pr':$('#addofPremium').val(),
      'prpl':$('#addofPremiumPlus').val()
  }
  var avl={
  	  'avlbc':$('#avlbc').val(),
      'avlad':$('#avlad').val(),
      'avlpr':$('#avlpr').val(),
      'avlprpl':$('#avlprpl').val()
  }
 if(parseInt(enter.bc)>parseInt(avl.avlbc)){
 	e.preventDefault()
    alert("Basic Licence type not available"); 
 }
 if(parseInt(enter.ad)>parseInt(avl.avlad)){
 	e.preventDefault()
    alert("Advance Licence type not available"); 
 }
 if(parseInt(enter.pr)>parseInt(avl.avlpr)){
 	e.preventDefault()
    alert("Premium Licence type not available"); 
 }
 if(parseInt(enter.prpl)>parseInt(avl.avlprpl)){
  e.preventDefault()
    alert("Premium Plus Licence type not available"); 
 }

});
</script>
@include('includes.js_create')
</body>
</html>
