@extends('includes.adminheader')
@section('mainContent')
	<style type="text/css">
		.tableFixHead {
  			overflow-y: auto;
		}

.tableFixHead table {
  width: 100%;
  height: 25%
}

.tableFixHead th,
.tableFixHead td {
  padding: 8px 16px;
}

.tableFixHead th {
  position: sticky;
  top: 0;
  background: lightblue;
}
	</style>
<div class="row" style="margin-top: -2%;">
	<div class="col-lg-9">
	<h1>Language Suggestion</h1>
	</div>
	 <div class="col-lg-3">
	<input class="form-control" style="margin-top: 10%;" id="myInput" type="text" placeholder="Search..">
	</div>
</div>
<div style="height: 82vh;" class="tableFixHead">
<table class="table table-bordered">
	<thead>
		<tr class="uppercase">
			@foreach($columns as $key => $value)
			<th style="text-transform: capitalize;">{{$value}}</th>
			@endforeach
            <th>Accept</th>
            <th>Ignore</th>
		</tr>
		@if(count($rows)==0)
		<tr ><td colspan="16" style="text-align: center;color: red"> No Records Found</td></tr>
		@endif
	</thead>

	<tbody id="myTable">
	@foreach($rows as $rowId=>$row)
		<tr>
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
			<td><a href="{{ route('acceptWord', $row['id']) }}" class="btn btn-info">Accept</a></td>
            <td><a href="{{ route('rejectWord', $row['id']) }}" class="btn btn-warning">Ignore</a></td>
		</tr>
	@endforeach
	</tbody>
</table>
    
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  
<script type="text/javascript">
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
    
</script>
</div>    
@stop
