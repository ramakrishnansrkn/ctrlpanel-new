@extends('includes.adminheader')
@section('mainContent')
<h1>Add a Franchise</h1>

<!-- if there are creation errors, they will show here -->
<font color="red">{{ HTML::ul($errors->all()) }}</font>

{{ Form::open(array('url' => 'vdmFranchises')) }}

  <script>
    function caps(element){
    element.value = element.value.toUpperCase();
    }                         
  </script>

	<div class="form-group">
		{{ Form::label('fname', 'Franchise Name') }}
		{{ Form::text('fname', Input::old('fname'), array('class' => 'form-control')) }}
	</div>
	
	<div class="form-group">
		{{ Form::label('fcode', 'Franchise Code') }}
		{{ Form::text('fcode', Input::old('fcode'), array('class' => 'form-control','onkeyup' => 'caps(this)')) }}
	</div>
	
	<div class="form-group">
		{{ Form::label('description', 'Description') }}
		{{ Form::text('description', Input::old('description'), array('class' => 'form-control')) }}
	</div>
	
	<div class="form-group">
		{{ Form::label('fullAddress', 'Full Address') }}
		{{ Form::text('fullAddress', Input::old('fullAddress'), array('class' => 'form-control')) }}
	</div>
	
	<div class="form-group">
		{{ Form::label('landline', 'Landline Number') }}
		{{ Form::text('landline', Input::old('landline'), array('class' => 'form-control')) }}
	</div>
	
	<div class="form-group">
		{{ Form::label('mobileNo1', 'Mobile Number1') }}
		{{ Form::text('mobileNo1', Input::old('mobileNo1'), array('class' => 'form-control')) }}
	</div>

	<div class="form-group">
		{{ Form::label('mobileNo2', 'Mobile Number2') }}
		{{ Form::text('mobileNo2', Input::old('mobileNo2'), array('class' => 'form-control')) }}
	</div>
	
	<div class="form-group">
		{{ Form::label('email1', 'Email 1') }}
		{{ Form::text('email1', Input::old('email1'), array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('email2', 'Email 2') }}
		{{ Form::text('email2', Input::old('email2'), array('class' => 'form-control')) }}
	</div>
	
	<div class="form-group">
		{{ Form::label('userId', 'User Id(login)') }}
		{{ Form::text('userId', Input::old('userId'), array('class' => 'form-control')) }}
	</div>
	
    <div class="form-group">
		{{ Form::label('otherDetails', 'Other Details') }}
		{{ Form::text('otherDetails', Input::old('otherDetails'), array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('prepaid', 'Prepaid Franchise') }}
		{{ Form::select('prepaid', array('' =>'--Select--', 'no' => 'No','yes' => 'Yes'),null,array('class' => 'form-control','id'=>'prepaid','required' => 'required')) }}
	</div>
	<div class="form-group" id="numberofLicence">
		{{ Form::label('numberofLicence', 'Number of Licence') }}
		{{ Form::text('numberofLicence', Input::old('numberofLicence'), array('class' => 'form-control')) }}
	</div>
<div class="form-group" id="typeCount">
  <div class="row">
		<div class="col-md-3" id="Basic">
			{{ Form::label('numberofBasicLicence', 'Number of Basic Licence') }}
			{{ Form::number('numberofBasicLicence', Input::old('numberofBasicLicence'), array('class' => 'form-control', 'placeholder'=>'Quantity', 'min'=>'0')) }}
		</div>
		<div class="col-md-3" id="Advance">
			{{ Form::label('numberofAdvanceLicence', 'Number of Advance Licence') }}
			{{ Form::number('numberofAdvanceLicence', Input::old('numberofAdvanceLicence'), array('class' => 'form-control', 'placeholder'=>'Quantity', 'min'=>'0')) }}
		</div>
		<div class="col-md-3" id="Premium">
			{{ Form::label('numberofPremiumLicence', 'Number of Premium Licence') }}
			{{ Form::number('numberofPremiumLicence', Input::old('numberofPremiumLicence'), array('class' => 'form-control', 'placeholder'=>'Quantity', 'min'=>'0')) }}
		</div>
        <div class="col-md-3" id="PremPlus">
			{{ Form::label('numberofPremPlusLicence', 'Number of Premium Plus Licence') }}
			{{ Form::number('numberofPremPlusLicence', Input::old('numberofPremPlusLicence'), array('class' => 'form-control', 'placeholder'=>'Quantity', 'min'=>'0')) }}
		</div>	
	</div></div>
 
	<div class="form-group">
		{{ Form::label('website', 'Website') }}
		{{ Form::text('website', Input::old('website'), array('class' => 'form-control')) }}
	</div>
 <div class="form-group">
		{{ Form::label('apiKey', 'Map Key / Api Key') }}
		{{ Form::text('apiKey', Input::old('apiKey'), array('id'=>'mapkey','class' => 'form-control','placeholder'=>'Map Key')) }}
	</div>
 <div class="form-group">
		{{ Form::label('website2', 'Website 2') }}
		{{ Form::text('website2', Input::old('website2'), array('class' => 'form-control')) }}
	</div>
 <div class="form-group">
		{{ Form::label('apiKey2', 'Map Key / Api Key 2') }}
		{{ Form::text('apiKey2', Input::old('apiKey2'), array('id'=>'mapkey','class' => 'form-control','placeholder'=>'Map Key')) }}
	</div>
 <div class="form-group">
		{{ Form::label('website3', 'Website 3') }}
		{{ Form::text('website3', Input::old('website3'), array('class' => 'form-control')) }}
	</div>
 <div class="form-group">
		{{ Form::label('apiKey3', 'Map Key / Api Key 3') }}
		{{ Form::text('apiKey3', Input::old('apiKey3'), array('id'=>'mapkey','class' => 'form-control','placeholder'=>'Map Key')) }}
	</div>
 <div class="form-group">
		{{ Form::label('website4', 'Website 4') }}
		{{ Form::text('website4', Input::old('website4'), array('class' => 'form-control')) }}
	</div>
 <div class="form-group">
		{{ Form::label('apiKey4', 'Map Key / Api Key 4') }}
		{{ Form::text('apiKey4', Input::old('apiKey4'), array('id'=>'mapkey','class' => 'form-control','placeholder'=>'Map Key')) }}
	</div>
	<div class="form-group">
		{{ Form::label('subDomain', 'SubDomain Name') }}&nbsp<span class="label label-success">NEW</span>
		{{ Form::text('subDomain', Input::old('subDomain'), array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
   		{{ Form::label('type', 'Track Page') }}</br>
   		<div class="col-md-4">{{ Form::radio('trackPage', 'Normal View',true) }} Normal View</div>
   		<div class="col-md-3">{{ Form::radio('trackPage', 'Customized View') }} Customized View</div>
    </div>
 	<br/>
	<div class="form-group">
		{{ Form::label('smsSender', 'SMS Sender') }}
		{{ Form::text('smsSender', Input::old('smsSender'), array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('smsProvider', 'SMS Provider') }}
		 {{ Form::select('smsProvider',$smsP, Input::old('smsProvider'), array('class' => 'form-control')) }} 
	</div>
	
	<div class="form-group">
		{{ Form::label('providerUserName', 'SMS Provider User Name') }}
		{{ Form::text('providerUserName', Input::old('providerUserName'), array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('providerPassword', 'SMS Provider Password') }}
		{{ Form::text('providerPassword', Input::old('providerPassword'), array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('ipadd', 'DB IP') }}
		 {{ Form::select('ipadd',$dbIp, Input::old('ipadd'), array('class' => 'form-control')) }} 
	</div>
	<div class="form-group">
		{{ Form::label('backUpDays', 'DB BackupDays') }}
		 {{ Form::number('backUpDays',   Input::old('backUpDays'), array('class' => 'form-control','required' => 'required', 'placeholder'=>'numbers', 'min'=>'1')) }} 
	</div>
	<div class="form-group">
		{{ Form::label('dbType', 'DB Type') }}
		 {{ Form::select('dbType',$backType, 'mysql', array('class' => 'form-control')) }} 
	</div>
	<div class="form-group">
		{{ Form::label('timeZone', 'Time Zone') }}
		{{ Form::select('timeZone',$timeZoneC, 'Asia/Kolkata', array('class' => 'selectpicker form-control', 'data-live-search '=> 'true')) }}
	</div>
	<!-- <div class="form-group">
		{{ Form::label('mapKey', 'Map Key') }}
		{{ Form::text('mapKey', Input::old('mapKey'), array('class' => 'form-control')) }}
	</div> -->
	<div class="form-group">
		{{ Form::label('gpsvtsAppKey', 'Mobile App Key') }}
		{{ Form::text('gpsvtsAppKey', Input::old('gpsvtsAppKey'),  array('id'=>'gpsapp','class' => 'form-control','placeholder'=>'Mobile App Key')) }}
	</div>
	<div class="form-group">
		{{ Form::label('addressKey', 'Address Key') }}
		{{ Form::text('addressKey', Input::old('addressKey'), array('id'=>'addkey','class' => 'form-control','placeholder'=>'Address Key')) }}
	</div>
	<div class="form-group">
		{{ Form::label('notificationKey', 'Notification Key') }}
		{{ Form::text('notificationKey', Input::old('notificationKey'),  array('id'=>'notify','class' => 'form-control','placeholder'=>'Notification Key')) }}
	</div>
     <div class="form-group">
		{{ Form::label('zoho', 'Zoho Organisation') }}
		{{ Form::text('zoho', Input::old('zoho'), array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('auth', 'Zoho Authentication') }}
		{{ Form::text('auth', Input::old('auth'), array('class' => 'form-control')) }}
	</div>
	{{ Form::submit('Add the Franchise!', array('class' => 'btn btn-primary')) }}

	{{ Form::close() }}

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script>
$(document).ready(function () {
	$('#prepaid').on('change', function() {
		var licence={
    	  'Type':$('#prepaid').val()
 		 }
 		  if(licence.Type == 'yes'){
 		  		$('#typeCount').show();
  				$('#numberofLicence').hide();
 		  }else{
 		  		$('#typeCount').hide();
  				$('#numberofLicence').show();
 		  }

	});


$('#sub').on('click', function() {
	 var datas={
				'val':$('#notify').val(),
        'val1':$('#gpsapp').val(),
        'val2':$('#mapkey').val(),
        'val3':$('#addkey').val()
				} 
     
	 if ((datas.val).indexOf(' ') !== -1) {
 
       alert('Please Enter Valid Notification Key');
         document.getElementById("notify").focus();
        return false;

       }
    else if ((datas.val1).indexOf(' ') !== -1) {
       alert('Please Enter Valid Mobile App Key');
         document.getElementById("gpsapp").focus();
        return false;

       }
   else if ((datas.val2).indexOf(' ') !== -1) {
       alert('Please Enter Valid Map Key / API Key');
         document.getElementById("mapkey").focus();
        return false;

       }
    else if ((datas.val3).indexOf(' ') !== -1) {
       alert('Please Enter Valid Address Key');
         document.getElementById("addkey").focus();
        return false;

       }
 });
 });

window.onload = function()
{
	var licence={
    	  'Type':$('#prepaid').val()
 		 }
 		  if(licence.Type == 'yes'){
 		  		$('#typeCount').show();
  				$('#numberofLicence').hide();
 		  }else{
 		  		$('#typeCount').hide();
  				$('#numberofLicence').show();
 		  }

}
</script>

@stop
