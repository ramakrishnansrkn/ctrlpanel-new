@extends('includes.adminheader')
@section('mainContent')

<h2><font color="blue">Select the Franchise name</font></h2>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'vdmFranchises/findUsersList')) }}
<br>
<div class="row">
    <div class="col-md-6">
    <div class="form-group">
        
    <div class="row">
         <div class="col-md-6">
        {{ Form::label('dealerId', 'Franchise Id') }}
        {{ Form::select('users',$userId,null,array('class' => 'form-control selectpicker show-menu-arrow', 'data-live-search '=> 'true','required' => 'required')) }}</div>
         <div class="col-md-6">
            <input class="btn btn-primary" type="submit" value="Submit" style="margin-top: 10%;">
        </div>
     </div>

    </div>

</br>
</br>
</div>
    {{ Form::close() }}
   </div>
@stop