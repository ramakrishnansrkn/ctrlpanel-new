@extends('includes.adminheader')
@section('mainContent')
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial;
}
.coupon {
  border: 5px solid #bbb;
  width: 100%;
  border-radius: 15px;
  margin-top: -1%;
  margin-bottom: 5%;
}

.container1 {
  padding: 2px 16px;
  background-color: #f1f1f1;
}

.promo {
  background: #ccc;
  padding: 3px;
}

.expire {
  color: red;
}
</style>
</head>
<body>

<div class="coupon">
  <div class="container1">
    <h2><b>{{$fname}}</b></h2>
    <strong>Franchise Id :<span class="promo">{{$fcode }}</span></strong>
    <div class="row">
    	<div class="col-lg-11"> <p>{{$description}}</p></div>
    	<div class="col-lg-1"> <a href="{{ URL::to('vdmFranchises') }}" style="color: blue;">&#8250; Back</a></div>
    </div>
   
  
  </div>
  <div class="container1" style="background-color:white">

    <strong>Franchise Details :</strong><p>
    {{'User Id :'.$userId}}<br>
   	{{'Website :'.$websites}}<br>
   	{{'TrackPage :'.$trackPage}}
	</p><hr>
  	<strong>Licence Details :</strong><p>
  	@if($prepaid!="yes")
   	{{'Number of Licence :'.$numberofLicence}}<br>
   	{{'Available Licence :'.$availableLincence}}
   	@else
   		<table class="table table-striped table-bordered">
   			<thead>
			<tr>
			<td>Type</td>
            <td>Number of Licence</td>
			<td>Available</td>
		</tr>
		<tbody id="myTable">
		<tr>
			<td>Basic</td>
            <td>{{$numberofBasicLicence}}</td>
			<td>{{$availableBasicLicence}}</td>
		</tr>
		<tr>
			<td>Advance</td>
            <td>{{$numberofAdvanceLicence}}</td>
			<td>{{$availableAdvanceLicence}}</td>
		</tr>
		<tr>
			<td>Premium</td>
            <td>{{$numberofPremiumLicence}}</td>
			<td>{{$availablePremiumLicence}}</td>
		</tr>
		<tr>
			<td>Premium Plus</td>
            <td>{{$numberofPremPlusLicence}}</td>
			<td>{{$availablePremPlusLicence}}</td>
		</tr>
		</tbody>
	</thead>
   		</table>
   		
   	@endif
	</p>
   <hr>
    <strong>DataBase Details :</strong><p>
   	{{'DB IP :'.$dbIp}}<br>
   	{{'DB Type :'.$dbType}}<br>
   	{{'Backup Days :'.$backUpDays}}
	</p>
   <hr>
   <strong>Other Details :</strong><p>
   	{{'MAP API Key :'.$apiKeys}}<br>
   	<!--{{'MAP Key :'.$mapKey}}<br>-->
   	{{'Address Key :'.$addressKey}}<br>
   	{{'Notification Key :'.$notificationKey}}<br>
   	{{'Package Name :'.$gpsvtsAppKey}}<br>
   	{{'ZOHO :'.$zoho}}<br>
   	{{'Auth :'.$auth}}
	</p>
  </div>
  <div class="container1">
    <p>Mobile No : {{$mobileNo1.', '.$mobileNo2}}<br>
    	Email	 : {{$email1.', '.$email2}}<br>
    	Address   :{{$fullAddress}}<br>
    	Landline. :{{$landline}}</p>
  </div>
</div>

</body>
</html> 


@stop