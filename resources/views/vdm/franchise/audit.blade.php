@extends('includes.adminheader')
@section('mainContent')
	<style type="text/css">
		.tableFixHead {
  			overflow-y: auto;
		}

.tableFixHead table {
  width: 100%;
  height: 25%
}

.tableFixHead th,
.tableFixHead td {
  padding: 8px 16px;
}

.tableFixHead th {
  position: sticky;
  top: 0;
  background: lightblue;
}
	</style>
<div class="row" style="margin-top: -2%;">
	<div class="col-lg-9">
	<h1>Audit Trial - Franchises</h1>
	</div>
	 <div class="col-lg-3">
	<input class="form-control" style="margin-top: 10%;" id="myInput" type="text" placeholder="Search..">
	</div>
</div>
<div style="height: 82vh;" class="tableFixHead">
<table class="table table-bordered">
	<thead>
		<tr class="uppercase">
			@foreach($columns as $key => $value)
			<th style="text-transform: capitalize;">{{$value}}</th>
			@endforeach

		</tr>
		@if(count($rows)==0)
		<tr ><td colspan="16" style="text-align: center;color: red"> No Records Found</td></tr>
		@endif
	</thead>

	<tbody id="myTable">
	@foreach($rows as $rowId=>$row)
	@if($row['status']==Config::get('constant.created'))
		<tr class="alert alert-success">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.updated'))
		<tr class="alert alert-info">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.deleted'))
		<tr class="alert alert-danger">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['Status']=='OnBoard')
		<tr class="alert alert-success">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['Status']=='Migration')
		<tr class="alert alert-warning">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['Status']=='Renew')
		<tr class="alert alert-info">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.disabled'))
		<tr style="background-color: #fcf8e3;color:#8a8467">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.enabled'))
		<tr style="background-color: #d8f1e8;color:#729493">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.reportsEdited'))
		<tr style="background-color: #e9e5f5;color:#845c84">
			@foreach($columns as $key => $value)
			@if($value=='reports')
					<td>
						
					   @foreach($AllReport[$rowId] as $reportTitle => $Reports)

							@if(count($Reports)>0)
				            <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#<?php echo $reportTitle.$rowId;?>"  style="margin-top: 2%;">{{ $reportTitle }} </button>
				                 
					             <div id="<?php echo $reportTitle.$rowId;?>" class="collapse">
					             	<ul>
					             	@foreach($Reports as $report)
					             	
					                <li style="list-style-type: decimal;">{{ $report }}</li>
					            
					                @endforeach
					              </ul>
					             </div>


				           @endif

		              @endforeach
		          
				   </td>
		      @elseif($value=='addedReports')
					<td>
					   @foreach($AddedReportArr[$rowId] as $reportTitle => $Reports)

							@if(count($Reports)>0)
							
							
				            <input class="btn btn-success" type="button" value="<?php echo $reportTitle;?>" style="margin-top: 5%;"  data-toggle="collapse" data-target="#<?php echo $reportTitle.$rowId;?>add">
				                 
					             <div id="<?php echo $reportTitle.$rowId;?>add" class="collapse">
					             	<ul>
					             	@foreach($Reports as $report)
					             	
					                <li style="list-style-type: decimal;">{{ $report }}</li>
					            
					                @endforeach
					              </ul>
					             </div>


				           @endif

		              @endforeach
				   </td>
			 @elseif($value=='removedReports')
					<td>
					   @foreach($RemovedReportArr[$rowId] as $reportTitle => $Reports)

							@if(count($Reports)>0)
							
				            <input class="btn btn-danger" type="button" value="<?php echo $reportTitle;?>" style="margin-top: 5%;"  data-toggle="collapse" data-target="#<?php echo $reportTitle.$rowId;?>rem">
				                 
					             <div id="<?php echo $reportTitle.$rowId;?>rem" class="collapse">
					             	<ul>
					             	@foreach($Reports as $report)
					             	
					                <li style="list-style-type: decimal;">{{ $report }}</li>
					            
					                @endforeach
					              </ul>
					             </div>

				           @endif

		              @endforeach
				   </td>

			@else
			   <td>{{ $row[$value] }}</td>
			@endif
			<!-- <td><span style="word-break: break-word;width: 200px;">{{ $row[$value] }}</span></td> -->
			@endforeach
		</tr>
		@else
		<tr style="background-color: #e8e4e4;color:#6b6666;border-color:#8c8585">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@endif

	@endforeach
	</tbody>
</table>
    
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  
<script type="text/javascript">
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
    
</script>
</div>    
@stop
