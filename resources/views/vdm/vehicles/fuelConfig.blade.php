@extends('includes.vdmEditHeader')
@section('mainContent')
<!-- if there are creation errors, they will show here -->
<div id="wrapper">
        <div class="content animate-panel">
                <div class="row">
                <div class="col-lg-12">
                         <div class="hpanel">
                        <div style="background-color: #183044; width: 97%; height: 6%; margin-left: 25px; margin-right: 25px; border-radius: 25px;" align="center">
                                <h4 style="padding-top:5px;"><font size="6px" color="#ffffff" face="Georgia">Fuel Configuration</font></h4>
                        </div>

                         <font color="red">{{ HTML::ul($errors->all()) }}</font>
                                        {{ Form::open(array('url' => 'vdmfuel/fuelUpdate')) }}
                          <br>
                        <div class="panel-body">
                     <div class="row">
                      <div class="col-md-2"></div>
                      <div class="col-md-3">{{ Form::label('vehicleName','Vehicle Name') }}
                  </div>
                      <div class="col-md-4">
						{{ Form::text('vehicleName', $vehicleName, array('class' => 'form-control','disabled' => 'disabled')) }}
					  {{ Form::hidden('vehicleId', $vehicleId, array('class' => 'form-control','disabled' => 'disabled')) }}
             {{ Form::hidden('deviceModel', $deviceModel, array('class' => 'form-control','disabled' => 'disabled','id'=>'dm')) }}
            {{ Form::hidden('vehicleIdTemp', $vehicleId) }}
              </div>
                    </div>
                    <br>
                                <div class="row">
                                        <div class="col-md-2"></div>
                            <div class="col-md-3">{{ Form::label('vehicletype', 'Mode of Vehicle') }}</div>
                            <div class="col-md-4">{{ Form::text('vehicletype',$vehicletype, array('class' => 'form-control','disabled' => 'disabled')) }}</div>
                                </div>
								<br>
								<div class="row">
                                    <div class="col-md-2"></div>
									<div class="col-md-3">{{ Form::label('fuelType', 'Fuel Type') }}</div>
									<div class="col-md-4">{{ Form::select('fuelType', array('nill'=>'Select','serial' => 'Serial','analog' => 'Analog'), $fuelInput, array('class' => 'form-control')) }}</div>
                                </div>
                                <br>
                                <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-3">{{ Form::label('minifilling', 'Minimum Filling') }}</div>
                                        <div class="col-md-4">{{ Form::number('minifilling', $minifilling, array('class' => 'form-control','min'=>$minfill) ) }}
                                                        </div>
                                </div>
                                <br>
                                <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-3">{{ Form::label('maxitheft', 'Maximum Theft') }}</div>
                                        <div class="col-md-4">{{ Form::number('maxitheft', $maxitheft, array('class' => 'form-control','min'=>'5') ) }}
                                                        </div>
                                </div>

                                <br>

                                <br>
                              <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-3">{{ Form::label('tanksize', 'Tank size') }}
                                                <div style="font-size: 9px;margin-left: 2px; margin-top: -5px;"><b>(Note:</b> In meter)</div>
                                       </div>
                                        <div class="col-md-4">{{ Form::number('tanksize', $tanksize, array('class' => 'form-control','min'=>'0','max'=>'5000','readonly' => 'true') ) }}</div>
                              </div>
                    <br>
                       <div class="row">
                      <div class="col-md-2"></div>
                            <div class="col-md-3">{{ Form::label('tankposition', 'Fuel Tank Position') }}</div>
                            <div class="col-md-4">{{ Form::select('tankposition', array(''=>'Select','vertical' => 'Vertical','horizontal' => 'Horizontal'), $tankposition, array('class' => 'form-control','id'=>'tankposition')) }}</div>
                    </div><br/>      
                    <div class="row">
                      <div class="col-md-2"></div>
                            <div class="col-md-3">{{ Form::label('tankshape', 'Fuel Tank Shape') }}</div>
                            <div class="col-md-4">{{ Form::select('tankshape', array(''=>'Select','Rectangle' => 'Rectangle','Cylinder' => 'Cylinder','Abnormal'=>'Abnormal'), $tankshape, array('class' => 'form-control','id'=>'tankshape')) }}</div>
                    </div>
                                <br>
                                <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-3"></div>
                                        <div class="col-md-4">
                                        
                                            <div id="cylinder">  
												                      <div class="col-md-2" id="r1">{{ Form::label('radius', 'Radius') }}
												                        <div style="font-size: 9px; text-align: center;margin-right: 12px;width: 45px;">(In mm)</div></div>
                                                <div class="col-md-4" id="r2">{{ Form::number('radius', $radius, array('class' => 'form-control','min'=>'0.001','step' => '0.001','id'=>'r') )}}</div>

                                                <div class="col-md-2" id="l1">{{ Form::label('Cylength', 'length') }}
                                                  <div style="font-size: 9px; text-align: center; width: 45px;margin-left: -3px;">(In mm)</div></div>
                                                <div class="col-md-4" id="l2">{{ Form::number('Cylength', $Cylength, array('class' => 'form-control','min'=>'0.001','step' => '0.001','id'=>'cl') ) }}</div>
                                            </div>
                                        </div>
                                </div>
								 <br/>
								<div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-3">{{ Form::label('maxval', 'Maximum Fuel Value') }}</div>
                                        <div class="col-md-4">
										{{ Form::number('maxval', $maxval, array('class' => 'form-control','min'=>'1.000','step' => '0.001', 'id'=>'maxFill') ) }}
                                        </div>
                                </div>
                                <br/>
								<div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-3">{{ Form::label('maxvolt', 'Maximum Voltage') }}</div>
                                        <div class="col-md-4">
										{{ Form::number('maxvolt', $maxvolt, array('class' => 'form-control','min'=>'1.000','step' => '0.001',  'id'=>'maxVolt') ) }}
                                        </div>
                                </div>
                                        <hr>
                        <div class="col-md-4" style="top: 20px; position: relative; left: 62%">
                                         <a class="btn btn-warning"  href="{{ URL::to('vdmVehicles/edit/'.$vehicleId) }}" style="margin-left: -41%;width: 30%;">Cancel</a>
                                        {{ Form::submit('Update Configuration', array('id'=>'sub','class' => 'btn btn-primary')) }}
                                        </div>
                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>



/*<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
      $(function () {
      $('.selectpicker').selectpicker();
      });
$('#sub').on('click', function() {
  var datas={
      'val':$('#tankshape').val(),
      'r':$('#r').val(),
      'cl':$('#cl').val(),
      'dm':$('#dm').val()  
  }
  if(datas.dm=='TRAKMATE'){
    $("#moveVehicle").prop('required',false);
    $("#tankshape").prop('required',false);
    $("#maxFill").prop('required',false);
     $("#maxVolt").prop('required',false);
  }else{
    $("#moveVehicle").prop('required',true);
    $("#tankshape").prop('required',true);
    $("#maxFill").prop('required',true);
    $("#maxVolt").prop('required',true);
    if (datas.val == 'Cylinder') {
      if(datas.cl == '')  {
        alert('Please enter valid Cylinder Length');
        document.getElementById("cl").focus();
        return false;
      }else if( datas.r == ''){
        alert('Please enter valid Radius');
        document.getElementById("r").focus();
        return false;
      }
    }
  }
 });

$('#tankshape').on('change', function() {
console.log('=======');
  var datas={
        'val':$('#tankshape').val()
  }
  if (datas.val == 'Rectangle') {
        $("#cylinder").hide();
  }else if(datas.val == 'Cylinder'){
       $("#cylinder").show();
  }else if(datas.val == 'Abnormal'){
      $("#cylinder").hide();
  }else{
      $("#cylinder").hide();
  }
});
window.onload = function(){
        var datas={
            'val':$('#tankshape').val()
        }
        console.log(datas.val);
        if(datas.val=='Rectangle'){
            $("#cylinder").hide();
        }else if(datas.val == 'Cylinder'){
            $("#cylinder").show();
        }else{
            $("#cylinder").hide();
        }

}
</script> */
<div align="center">@stop</div>

