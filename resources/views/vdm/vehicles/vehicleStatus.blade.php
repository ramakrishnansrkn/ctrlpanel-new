@include('includes.header_create')
<style>
table.dataTable tbody th, table.dataTable tbody td {
padding: 17px 1px !important; 
    text-overflow: clip; 
    text-align: center;
    }
 #tabNew table {
width: 100%; 
}
 #tabNew tr:nth-of-type(odd) {
 /* background: white;*/
}

@media  only screen and (max-width: 760px), 
 (min-device-width: 320px) and (max-device-width: 780px) {
   #tabNew table,  #tabNew thead,  #tabNew tbody,  #tabNew th,  #tabNew td,  #tabNew tr {
    display: block;
  }
    #tabNew thead tr {
    position: absolute;
        top: -9999px;
        left: -9999px;
  }
   #tabNew tr {
    border: 1px solid #eee; 
  }
   #tabNew td {

    border-bottom: 1px solid #eee;
    position: relative;
    white-space: normal;
   /* width: 50%;*/
    font-size: 17px;
  }
  #tabNew td:before { 
    position:relative;
    color: #8AC007; 
    padding: 2px;
    right: 10px;
	border: 2px solid #aaa; 
  }
   #tabNew th {
	  text-align: left;
  }
/*.vCol'=='1')
{
	td:nth-of-type(6):before {content: "Action ";}
}*/
   #tabNew td:nth-of-type(1):before {content: "ID ";}
   #tabNew td:nth-of-type(2):before {content: "AssetID ";}
   #tabNew td:nth-of-type(3):before {content: "Vehicles Name ";}
   #tabNew td:nth-of-type(4):before {content: "Org Name ";}
   #tabNew td:nth-of-type(5):before {content: "Device ID ";}
   #tabNew td:nth-of-type(6):before {content: "GPS Sim No ";}
   #tabNew td:nth-of-type(7):before {content: "Last Comm Time ";}
   #tabNew td:nth-of-type(8):before {content: "Status ";}  
   #tabNew td:nth-of-type(9):before {content: "Device Model ";}
   #tabNew td:nth-of-type(10):before {content: "Licence Issued Date ";}
   #tabNew td:nth-of-type(11):before {content: "On Board Date ";}
   @if(Session::get('cur1') =='prePaidAdmin')
   	  #tabNew td:nth-of-type(12):before {content: "Lic Exp Date ";}
   @endif
   #tabNew td:nth-of-type(12):before {content: "Veh Exp Date ";}
  
  @if(Session::get('vCol')=='1')
   #tabNew td:nth-of-type(13):before {content: "Action "}
  @endif
  } 
 </style>
<div id="wrapper">
<div class="content animate-panel">
<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
		   <div class="pull-right">
                <a href="../vehicleStatus/sendExcel" ><img  class="pull-right"  width="20%" height="20%" src="../../app/views/reports/image/xls.png" method="get"/ ></a>
            </div> 
                <div class="panel-heading">
				     @if(Session::has('message')) 
                	 <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                	 @endif
                    <h6><b><font color="red"> {{ HTML::ul($errors->all()) }}</font></b></h6>
                    <h4><b>Vehicles Status</b></h4>                
                </div>
                <div class="panel-body">
                
				<div id="tabNew" style="font-size: 11px; overflow-y: auto;">
                	<table id="example1" class="table table-bordered dataTable">
               		 <thead>
						<tr>
						
							<th style="text-align: center;">AssetID </th>
							<th style="text-align: center;">Vehicle Name</th>
             <th style="text-align: center;">Device Id</th>
							<th style="text-align: center;">Org Name</th>
              <th style="text-align: center;">Reg No</th>
              <th style="text-align: center;">Groups</th>
              <th style="text-align: center;">Position</th>
              <th style="text-align: center;">Ignition Status</th>
              <th style="text-align: center;">Last Comm</th>
              <th style="text-align: center;">Last Loc</th>
                <th style="text-align: center;">G-Map</th>
               
               <th style="text-align: center;">Address</th>
               <th style="text-align: center;">Speed</th>							
						</tr>
					</thead>
					<tbody style="word-break: break-all;">
					@foreach($vehicleList as $key => $value)
						<tr>
                <td style="text-overflow: clip !important;">{{ $value }}</td>
							<td>{{ array_get($shortNameList, $value)}}</td>
             <td>{{ array_get($deviceList, $value)}}</td>	
							<td>{{ array_get($orgIdList, $value)}}</td>
               <td>{{ array_get($regNoList, $value) }}</td>
               <td>{{ array_get($groupList, $value) }}</td>
               <td>
									@if(array_get($statusList, $value) == 'P')
										<div style="color: #8e8e7b">Parking</div>
									@endif
									@if(array_get($statusList, $value) == 'M')
										<div style="color: #00b374">Moving</div>
									@endif
									@if(array_get($statusList, $value) == 'S')
										<div style="color: #ff6500">Idle</div>
									@endif
									@if(array_get($statusList, $value) == 'U')
										<div style="color: #fe068d">No Data</div>
									@endif
									@if(array_get($statusList, $value) == 'N')
										<div style="color: #0a85ff">New Device</div>
									@endif
								</td>
              <td>{{ array_get($ignList, $value)}}</td>
               <td>{{ array_get($commList, $value)}}</td>
							 <td>{{ array_get($locList, $value)}}</td>
						<!--	<td>{{ array_get($latlonList, $value)}}</td> -->
                <td><a href="https://www.google.com/maps?q=loc:<?php echo array_get($latlonList, $value) ?>" target="_blank" style="text-decoration: underline; color:#000099;" />G-Link</td>
              <td>{{ array_get($addressList, $value)}}</td>
              <td>{{ array_get($speedList, $value)}}</td>
							 
						</tr>
						@endforeach
						<script>




  function ConfirmDelete()
  {
  var x = confirm("Confirm to remove?");
  if (x)
    return true;
  else
    return false;
  }

</script>
						
					</tbody>
                </table>
				</div>
         
</div>
</div>
</div>
@include('includes.js_create')
</body>
</html>
