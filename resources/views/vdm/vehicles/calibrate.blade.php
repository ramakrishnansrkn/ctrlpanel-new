@extends('includes.vdmEditHeader')
@section('mainContent')
<style>
.countbtn
{
    width: 50%;
    margin-left: 17%;
}
</style>
<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
    		<div class="col-lg-12">
       			 <div class="hpanel">
               		<div style="background-color: #3ca9a9; border-radius: 11px; font-family: initial; margin-top: 9px; height: 6%;" class="panel-heading" align="center">
                   		<h4 style="margin-top: -5px; !impotant"><font size="6px" color="white" >Calibrate Vehicle</font></h4>
                	</div>
                	<div class="panel-body">
                		{{ HTML::ul($errors->all()) }}
						{{ Form::open(array('url' => 'vdmVehicles/updateCalibration')) }}
                		<hr>
                		<div class="row">
                			<div class="col-md-3"></div>
                			<div class="col-md-3">{{ Form::label('vehicleId', 'Asset ID :')  }}</div>
                			<div class="col-md-3">{{ Form::hidden('vehicleId', $vehicleId, array('class' => 'form-control')) }}{{ Form::text('vehicleId1', $vehicleId, array('class' => 'form-control','disabled' => 'disabled','id'=>'vehicleid')) }}</div>
                		</div>
                		<br>
                		<div class="row">
                			<div class="col-md-3"></div>
                			<div class="col-md-3">{{ Form::label('deviceId', 'Device Id / IMEI No :') }}</div>
                			<div class="col-md-3">{{ Form::text('deviceId', $deviceId, array('class' => 'form-control','disabled' => 'disabled')) }}</div>
                    </div>
                		<hr>
                 
                		<div class="row">
                            <!-- <div class="col-md-2">as</div> -->
                			<div class="col-md-2"></div>
                			<div class="col-md-3" style="color: #ffffff; text-align: center; background: #3ca9a9; border-radius: 25px; margin-left: 86px; width: 13%;">{{ Form::label('litre', "Litre", array('disabled' => 'disabled')) }}</div>
							        <div class="col-md-3" style="color: #ffffff; text-align: center; background: #3ca9a9; border-radius: 25px; margin-left: 163px; width: 13%;">{{ Form::label('volt', "Volt", array('disabled' => 'disabled')) }}</div>
                      <div class="col-md-2" style="margin-left: 3%; width: 18%;" >{{ Form::Number('count','',array('class' => 'form-control countbtn','placeholder'=>'Count', 'min'=>'1', 'id'=>'counting')) }}</div>                 
                      <div class="col-md-2" style="text-align: center;margin-left: -6%; width: 0%"> <a  class="btn btn-sm "  id="addCalibrate" onclick="getadd('<?php echo $addvalue ?>')" value="<?php echo $addvalue ?>" style="padding-left: 13px; margin-left: 52%; background: #314a63; color: #ffffff; height: 4%;">Add More</a></div>
                      </div>
                       
                    @foreach($place as $key => $value)
                		<br id='delbr<?php echo $m++ ?>'>
                		<div class="row" id='delrow<?php echo $d++ ?>'>
                			<div class="col-md-2"></div>
                      
                			<div class="col-md-3">{{ Form::number( 'litre'.$i++,isset(explode(":", $value)[1])?explode(":", $value)[1]:(($k++)+1)*5,array('class' => 'form-control','id'=>'litre'.$i++,'step'=>'5','min'=>'5') ) }}</div>
							        <div class="col-md-3">{{ Form::number('volt'.$j++, isset(explode(":", $value)[0])?explode(":", $value)[0]:0,  array('class' => 'form-control','id'=>'volt'.$j++,'step'=>'0.001','min'=>'0.001')) }}</div>
                     
                      {{ Form::hidden('listvalue', count($place), array('class' => 'form-control','id'=>'listvalue')) }}
							        {{ Form::hidden('jval', $a++,array('id'=>'jval')) }}
					            <div class="col-md-2"><a  class="btn btn-sm btn-info" id='rem<?php echo $b++ ?>' onclick="getvalues('<?php echo $b++ ?>')" >Get Volt</a>
                      <a  class="btn btn-sm " id='remove<?php echo $c++ ?>' onclick="delvalues('<?php echo $c++ ?>')" style="background-color: #b3294b; color: #ffffff;">Remove</a></div>
						        </div>
                		@endforeach
                		<br>
                		<div class="row">
							<div class='col-md-6'></div>
							<div class='col-md-4' align="center" style=" margin-left: -8%; width: 0%;">{{ Form::submit('Calibrate!', array('class' => 'btn btn-primary')) }}</div>
                			<div class='col-md-2'><a  class="btn btn-sm " style="height: 33px; color: #ffffff; background-color: #b3294b;" href="../../../VdmVehicleScan<?php echo $vehicleId ?>">Cancel</a></div>
                		</div>
  
             		{{ Form::close() }}
                   
                	</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<script type="text/javascript">

var input = document.getElementById("counting");

input.addEventListener("keyup", function(event) {
 
    if (event.keyCode === 13) {
        document.getElementById("addCalibrate").click();
    }
});

function delvalues(d)
{

var d1=Number(d)-1;
var d2=((Number(d)+1)/2)-1;
  var data={
    
        litre : $('#litre'+d).val(),
        volt :$('#volt'+d).val(),
        vehi : $('#vehicleid').val(),
        val4 : $('#remove'+d1).val(),
        val5 : $('#addCalibrate').val(),
        val6 : $('#listvalue').val() 
  };
  
  var r=Number(data.val6)-1;
  var test=document.getElementById('delrow'+d2);
   
 document.getElementById('delrow'+d2).style.display="none";
 document.getElementById('delrow'+d2).disabled =true;
 document.getElementById('delbr'+d2).style.display="none";
 document.getElementById('delbr'+d2).disabled =true;
 document.getElementById('litre'+d).style.display="none";
 document.getElementById('litre'+d).disabled =true;
 document.getElementById('volt'+d).style.display="none";
 document.getElementById('volt'+d).disabled =true;
 
}


function getadd(n){

    var n1=((2*Number(n))-1);
    var data={
        val1 :$('#volt'+n1).val(),
        val2 :$('#listvalue').val(),
        val3 : $('#vehicleid').val(), 
        val4 : $('#counting').val()
 
    };
   
    if(data.val4=='')
    {
    alert('Please enter Valid Count value');
    document.getElementById("counting").focus();
     return false;
    }
  
    else {
   
$('#addCalibrate').attr('href', '../../../vdmVehicles/calibrate/'+data.val3+'/'+data.val4+'/'+data.val2);
  }
  }

function getvalues(j){

    $('#volt').attr('id', 'volt'+j);
    var data={
        vehicleId :$('#vehicleid').val(), 
        volt : $('#volt'+j).val()

    };
    console.log('volttt  '+data.volt);
    $.post('{{ route("ajax.calibrateget") }}',data)
            .done(function(data,textStatus, xhr) {
            
            if(data.volt ==''){
                alert('There is no volt Found !');
                return false;
            }
            else {
            document.getElementById("volt"+j).value =data.volt;
        }    
});

}
</script>

<div align="center">@stop</div>