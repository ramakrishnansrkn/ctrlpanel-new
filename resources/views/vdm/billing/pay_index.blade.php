@include('includes.header_index')
<div id="wrapper">
@include('vdm.billing.licenceList_index')
<div class="content animate-panel">
<div class="row">
    <div class="col-lg-14">
        <div class="hpanel">
                <div class="panel-heading">
        </div>

                <div class="panel-body" style="margin-top: -2%; ">
                @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                @endif
                <font color="red"> {{ HTML::ul($errors->all()) }}</font>

                 <div class="row well" style="background: #14cfed6b; padding: 0%;">
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Basic Available Licence:{{$availableBasicLicence}}</h5></div>
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Advance Available Licence:{{$availableAdvanceLicence}}</h5></div>
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Premium Available Licence:{{$availablePremiumLicence}}</h5></div>
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Premium Plus Available Licence:{{$availablePremPlusLicence}}</h5></div>
                 </div>
                  <hr>
                  <table id="example1" class="table table-bordered dataTable">
                   <thead>
            <tr>
              <th style="text-align: center;">ID</th>
              <th style="text-align: center;">Licence ID</th>
              <th style="text-align: center;">Vehicle ID </th>
              <th style="text-align: center;">Type</th>
              <th style="text-align: center;">Device ID</th>
              <th style="text-align: center;">Device Model</th>
              <th style="text-align: center;">Dealer Name</th>
              <th style="text-align: center;">Licence Issued Date</th>
              <th style="text-align: center;">Licence Onboard Date</th>
              <th style="text-align: center;">Licence Expire Date</th>
              <th style="text-align: center;">Status</th>
            </tr>
          </thead>
          <tbody id="myTable">
          @foreach($LicenceList as $key => $value)
            @if(in_array($value,$ExpiredLicenceList))
                <tr style="text-align: center; color: #ff2f00;">
            @else
                <tr style="text-align: center; ">
            @endif
              <td>{{ ++$key }}</td>
              <td>{{$value}}</td>
              <td>{{ array_get($vehicleList,$value) }}</td>
              <td>{{ array_get($LtypeList,$value) }}</td>
              <td>{{ array_get($deviceList, $value)}}</td>  
              <td>{{ array_get($deviceModelList, $value)}}</td>   
              <td>{{array_get($dealerList, $value)}}</td>
              <td>{{array_get($licenceissuedList, $value)}}</td>
              <td>{{ array_get($LicenceOnboardList, $value)}}</td>
              <td>{{ array_get($LicenceExpiryList, $value)}}</td>
              @if(in_array($value,$ExpiredLicenceList))
                <td><h6><b style="color: #ff2f00;">Expired</b></h6></td>
              @else
                <td><h6><b style="color: #00a1ff;">Active</b></h6></td>
              @endif
            </tr>
            @endforeach
            
          </tbody>
                </table>                
</div>
</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script src="http://code.jquery.com/ui/1.9.1/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
     $("#bala1").addClass("active");
   
});
</script>

</body>
</html>
@include('includes.js_index')