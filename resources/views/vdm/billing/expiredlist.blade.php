@include('includes.header_create')
<div id="wrapper">
@include('vdm.billing.licenceList_create')
{!! Form::open(array('url' => 'Billing','id'=>'testForm')) !!}
<div class="content animate-panel">
<div class="row">
    <div class="col-lg-14">
        <div class="hpanel">
                <div class="panel-heading">
          @if(Session::has('message'))
                  <p class="alert {!! Session::get('alert-class', 'alert-success') !!}">{!! Session::get('message') !!}</p>
          @endif
        </div>

                <div class="panel-body" style="margin-top: -2%;">
                <font color="red"> {!! HTML::ul($errors->all()) !!}</font>

                 <div class="row well" style="background: #14cfed6b; padding: 0%;">
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Basic Available Licence:{!!$availableBasicLicence!!}</h5></div>
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Advance Available Licence:{!!$availableAdvanceLicence!!}</h5></div>
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Premium Available Licence:{!!$availablePremiumLicence!!}</h5></div>
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Premium Plus Available Licence:{!!$availablePremPlusLicence!!}</h5></div>
                 </div>
                  <hr>
                  <table id="example1" class="table table-bordered dataTable">
                   <thead>
            <tr>
              <th style="text-align: center;">ID</th>
              <th style="text-align: center;"><input type="checkbox" class="check1" id="checkAll"></th>
              <th style="text-align: center;">Licence ID</th>
              <th style="text-align: center;">Vehicle ID </th>
              <th style="text-align: center;">Type</th>
              <th style="text-align: center;">Device ID</th>
              <th style="text-align: center;">Device Model</th>
              <th style="text-align: center;">Licence Issued Date</th>
              <th style="text-align: center;">Licence Onboard Date</th>
              <th style="text-align: center;">Licence Expire Date</th>
            </tr>
          </thead>
          <tbody id="myTable" style="color: red;">
          @if($licenceExpList!=null)
          @foreach($licenceExpList as $key => $value)
            <tr style="text-align: center;">
              <td>{!! ++$key !!}</td>
              @if(in_array($value,$ExpiredLicenceList))
              <td><input name="renewalLicence[]" type="checkbox" class="check" value="<?php echo htmlspecialchars($value); ?>"></td>
               @else
                <td><input type="checkbox" onclick="return false;"/></td>
              @endif
              <td>{!!$value!!}</td>
              <td>{!! array_get($vehicleList,$value) !!}</td>
              <td>{!! array_get($LtypeList,$value) !!}</td>
              <td>{!! array_get($deviceList, $value)!!}</td>  
              <td>{!! array_get($deviceModelList, $value)!!}</td>   
              <td>{!!array_get($licenceissuedList, $value)!!}</td>
              <td>{!! array_get($LicenceOnboardList, $value)!!}</td>
              <td>{!! array_get($LicenceExpiryList, $value)!!}</td>             
            </tr>
            @endforeach
            @endif                                     
<script>
    
    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure to renewal ?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }

</script>               
          </tbody>
                </table>                
<input class="btn btn-primary" id="sub" type="submit" value="RENEW" style="width: 20%;margin-left: 35%;">
</div>
</div>
</div>

{!! Form::close() !!}
<script src="http://code.jquery.com/ui/1.9.1/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript">
  $("#checkAll").click(function () {
    $(".check").prop('checked', $(this).prop('checked'));  
});

$('#testForm').on('submit', function(e) {

  var chkArray = [];
  
  $(".check:checked").each(function() {
    chkArray.push($(this).val());
  });
  var selected;
  selected = chkArray.join(',') ;
  if(selected.length > 0){
    if (!confirm('Are you sure to renewal ? ' +selected)) e.preventDefault();
  }else{
    e.preventDefault()
    alert("Please at least check one of the Licence"); 
  }


});

$(document).ready(function(){
     $("#bala2").addClass("active");
   
});
</script>

</body>
</html>
@include('includes.js_create')