@extends('includes.vdmEditHeader')
@section('mainContent')
<style>
#tabNew table {
width: 100%; 
}
#tabNew tr:nth-of-type(odd) {
 /* background: white;*/
}
@media  only screen and (max-width: 760px), 
 (min-device-width: 320px) and (max-device-width: 780px) {
  #tabNew table, #tabNew thead, #tabNew tbody, #tabNew th, #tabNew td, #tabNew tr {  
    display: block;
  }
   #tabNew thead tr {
    position: absolute;
        top: -9999px;
        left: -9999px;
  }
  #tabNew tr {
    border: 1px solid #eee; 
  }
  #tabNew td {
    border-bottom: 1px solid #eee;
    position: relative;
    white-space: normal;
   /* width: 50%;*/
    font-size: 17px;
  }
 #tabNew td:before {
     position:relative;
    color: #8AC007; 
    padding: 2px;
    right: 10px;
   border: 2px solid #aaa;  
  }
  #tabNew td:nth-of-type(1):before {content: "Track Name ";}
  #tabNew td:nth-of-type(2):before {content: "Vehicle ID ";}
  #tabNew td:nth-of-type(3):before {content: "Actions ";}
 } 
</style>
<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
			<div class="col-lg-12">
				<div class="hpanel">
					<div class="panel-heading">
						<div style="background-color: #183044; width: 97%; height: 6%; margin-left: 25px; margin-right: 25px; border-radius: 25px;" align="center">
                                <h4 style="padding-top:5px;"><font size="5px" color="#ffffff" face="Georgia">Organization Track List-{{$organizationId}}</font></h4>
            </div>			
					  <div style="padding: 5px;margin-right: 5%;">
					  <a class="btn btn-warning pull-right"  href="{{ URL::to('vdmOrganization/' . $organizationId .'/orgTrack') }}">Go Back</a>
					  </div>	
					</div>
					<font color="red">{{ HTML::ul($errors->all()) }}</font>  
                                         
                                        <br/>
					<div class="panel-body">	
                          <div id="tabNew">
                  <table id="example1" class="table table-bordered dataTable">
                                            <thead>
                                                <tr>
                                                   <th style="text-align: center;">Track Name</th>
                                                    <th style="text-align: center;">Vehicle ID</th>
                                                    <th style="text-align: center;">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($trkNameList as $key => $value)
                                                <tr>
                                                  <td>{{ $value }}</td>
                                                    <td>{{array_get($trkvehicleList,$value) }}</td>
                                                    <!-- we will also add show, edit, and delete buttons -->
                                                    <td> 
                        
<a class="btn btn-warning" href="{{ URL::to('vdmOrganization/' . $organizationId . '/orgTrackEdit/' . $value ) }}">Edit</a>
<a class="btn btn-danger"href="{{ URL::to('vdmOrganization/' . $organizationId . '/orgTrackDelete/' . $value ) }}">Delete</a>
</td>

<script>

  function ConfirmDelete()
  {
  var x = confirm("It will removes all stops generated in this routes?");
  if (x)
    return true;
  else
    return false;
  }

</script>
                                                    
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                    </div>
						<!-- </div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
