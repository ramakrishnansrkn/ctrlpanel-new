@extends('includes.vdmEditHeader')
@section('mainContent')
<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
			<div class="col-lg-12">
				<div class="hpanel">
					<div class="panel-heading">
						<div style="background-color: #183044; width: 97%; height: 6%; margin-left: 25px; margin-right: 25px; border-radius: 25px;" align="center">
                                <h4 style="padding-top:5px;"><font size="5px" color="#ffffff" face="Georgia">Organization Track</font></h4>
                        </div>
						<div style="padding: 5px;margin-right: 5%;">
						<a href="{{ URL::to('vdmOrganization/' . $organizationId . '/orgTrackList') }}"  class="btn btn-warning pull-right">Go Back</a>
						</div>
                       
						
					</div>
					<font color="red">{{ HTML::ul($errors->all()) }}</font>                                  
                                        {{ Form::open(array('url' => 'vdmOrganization/orgTrackEditUpdate')) }}
                                        <br/>
					<div class="panel-body">						
						<div class="row">
                      <div class="col-md-2"></div>
                   	   <div class="col-md-3">{{ Form::label('orgId', 'Organization Name:') }}
                    	  </div>
                      	<div class="col-md-4">
                      		{{ Form::text('organizationId',$organizationId, array('class' => 'form-control','id'=>'orgId','required' => 'required','readonly' => 'true')) }}
                          
                      		
              			</div>

              			
                    </div>	<br/>
                    <div hidden>
                        {{ Form::select('vehicleIdOld[]',$vehicleIdList, $vehicleId,array('id'=>'vehicleList','class' => 'form-control selectpicker show-menu-arrow', 'data-live-search '=> 'true','multiple'=>'multiple')) }}
                    </div>

                    <div class="row">
                      <div class="col-md-2"></div>
                   	   <div class="col-md-3">{{ Form::label('vehicleId', 'Vehicle Name:') }}
                    	  </div>
                      	<div class="col-md-4">
                          {{ Form::select('vehicleId[]',$vehicleIdList, $vehicleId,array('id'=>'vehicleList','class' => 'form-control selectpicker show-menu-arrow', 'data-live-search '=> 'true','required' => 'required','multiple'=>'multiple')) }}
                      		
              			</div>
                    </div>
                    <br/>
                    <div class="row">
                      <div class="col-md-2"></div>
                   	   <div class="col-md-3">{{ Form::label('trackName', 'Track Name:') }}
                    	  </div>
                      	<div class="col-md-4">{{ Form::text('trackName',$trackName, array('class' => 'form-control','placeholder'=>'Track Name','id'=>'trackNameId','required' => 'required','onkeyup' =>'validate(this)')) }}
                          {{ Form::hidden('trackNameOld', $trackName,array('id'=>'vehicleList','class' => 'form-control ','required' => 'required','readonly' => 'true')) }}
              			</div>
                    </div>				
                        <div class="col-md-4" style="top: 20px; position: relative; left: 55%;width: 100px">									
									{{ Form::submit('Submit', array('class' => 'btn btn-primary','id'=>'submit')) }}

									<br/ >
								</div>
								{{ Form::close() }}
							</div>
						<!-- </div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
function validate(element){
var word=element.value
var new1 = word.replace(/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/,"")
element.value=new1
}

$(document).ready(function(){
             $("#trackNameId").on("keydown", function (e) {
    return e.which !== 32;
});
 });
</script>
