@include('includes.header_create')
<!-- Main Wrapper -->
<div id="wrapper">
	<div class="content animate-panel">
		<div class="hpanel">
			<h4><font><b>Add a school / college / Organization</b></font></h4>
			<div class="panel-body">
				{{ HTML::ul($errors->all()) }}{{ Form::open(array('url' => 'vdmOrganization')) }}
				<div class="col-md-12">
					<span id="validation" style="color: red; font-weight: bold"></span>
				</div>
				<div class="row">
					<div class="col-md-3">{{ Form::label('organizationId', 'School/College/Organization Id * :')  }}</div>
					<div class="col-md-6">{{ Form::text('organizationId', $orgName1, array('class' => 'form-control', 'required' => 'required', 'placeholder'=>'School/College/Organization Id', 'id'=>'orgId','onkeyup' => 'caps(this)')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('description', 'Description :') }}</div>
					<div class="col-md-6">{{ Form::text('description', Input::old('[description'), array('class' => 'form-control', 'placeholder'=>'Description')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('email', 'Email * :') }}</div>
					<div class="col-md-6">{{ Form::text('email', Input::old('email'), array('class' => 'form-control', 'required' => 'required','placeholder'=>'Email','id'=>'email')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('mobile', 'Mobile * :') }}</div>
					<div class="col-md-6">{{ Form::text('mobile', Input::old('mobile'), array('class' => 'form-control', 'required' => 'required','placeholder'=>'Mobile Number')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('mtc', 'Morning Trip Cron :') }}</div>
					<div class="col-md-6">{{ Form::text('mtc', Input::old('mtc'), array('class' => 'form-control','placeholder'=>'Morning Trip Cron')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('atc', 'After Trip Cron :') }}</div>
					<div class="col-md-6">{{ Form::text('atc', Input::old('atc'), array('class' => 'form-control','placeholder'=>'After Trip Cron')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('etc', 'Evening Trip Cron :') }}</div>
					<div class="col-md-6">{{ Form::text('etc', Input::old('etc'), array('class' => 'form-control','placeholder'=>'Evening Trip Cron')) }}</div>
				</div>
				<br />
				<div class="rowNew">
					<div class="col-md-3"  style="width:26% !important; padding-left: 0px !important;">{{ Form::label('smsProvider', 'SMS Provider') }}</div>
					<div class="col-md-6" style="padding-left: 0px !important; padding-right: 16px !important;">{{ Form::select('smsProvider',$smsP, Input::old('smsProvider'), array('class' => 'selectpicker show-menu-arrow form-control','data-live-search '=> 'true','placeholder'=>'SMS Provider')) }}</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">{{ Form::label('providerUserName', 'SMS Provider User Name') }}</div>
					<div class="col-md-6">{{ Form::text('providerUserName', Input::old('providerUserName'), array('class' => 'form-control','placeholder'=>'SMS Provider Name')) }}</div>
				</div>
				<br />
				<div class="row">
				    <div class="col-md-3">{{ Form::label('providerPassword', 'SMS Provider Password') }}</div>
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
					<div class="col-md-6"> {{ Form::text('providerPassword',Input::old('providerPassword'), array('class' => 'form-control','placeholder'=>'SMS Provider Password','id'=>'method2')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('isRfid', 'IS RFID') }}</div>
                    <div class="col-md-6">{{ Form::select('isRfid',  array( 'no' => 'No','yes' => 'Yes' ), Input::old('isRfid'), array('class' => 'form-control','id'=>'isrfid')) }} </div>         
				</div>
				<br/>
				<div id="pickup">
				<div class="row">
                    <div class="col-md-3"><label for="pickup" style="margin-top: 14%;">Pick Up :</label></div>
                    <div class="col-md-3">
                       {{ Form::label('PickupStartTime', 'Start Time ') }}
                       {{  Form::input('time', 'PickupStartTime', null, ['class' => 'form-control', 'placeholder' => 'time','id'=>'PickupStartTime'])}}
            		</div>
                    <div class="col-md-3">
                        {{ Form::label('PickupEndTime', 'End Time') }}
                        {{  Form::input('time', 'PickupEndTime', null, ['class' => 'form-control', 'placeholder' => 'time','id'=>'PickupEndTime'])}}
                     </div>
               </div><br/></div>
			   <div id="drop">
               <div class="row">
                    <div class="col-md-3"><label for="drop" style="margin-top: 14%;">Drop :</label></div>
                    <div class="col-md-3">
                       {{ Form::label('DropStartTime', 'Start Time ') }}
                       {{  Form::input('time', 'DropStartTime', null, ['class' => 'form-control', 'placeholder' => 'time','id'=>'DropStartTime'])}}
            		</div>
                    <div class="col-md-3">
                        {{ Form::label('DropEndTime', 'End Time') }}
                        {{  Form::input('time', 'DropEndTime', null, array('class' => 'form-control', 'placeholder' => 'time','id'=>'DropEndTime'))}}
                     </div>
               </div>
               <br/></div>
				<div class="row">
					<div class="col-md-3">{{ Form::label('parkingAlert', 'Parking Alert :') }}</div>
					<div class="col-md-6">{{ Form::select('parkingAlert',  array( 'no' => 'No','yes' => 'Yes' ), Input::old('parkingAlert'), array('class' => 'form-control')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('idleAlert', 'Idle Alert :') }}</div>
					<div class="col-md-6">{{ Form::select('idleAlert',  array( 'no' => 'No','yes' => 'Yes' ), Input::old('idleAlert'), array('class' => 'form-control')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('sosAlert', 'SOS Alert :') }}</div>
					<div class="col-md-6"> {{ Form::select('sosAlert',  array( 'no' => 'No','yes' => 'Yes' ), Input::old('sosAlert'), array('class' => 'form-control')) }} </div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('overspeedalert', 'Over Speed Alert :') }}</div>
					<div class="col-md-6">{{ Form::select('overspeedalert',  array( 'no' => 'No','yes' => 'Yes' ), Input::old('overspeedalert'), array('class' => 'form-control')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('SchoolGeoFence', 'School GeoFence :') }}</div>
					<div class="col-md-6">{{ Form::select('SchoolGeoFence',  array( 'no' => 'No','yes' => 'Yes ' ), Input::old('SchoolGeoFence'), array('class' => 'form-control','id'=>'SchoolGeoFence')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('sendGeoFenceSMS', 'Send GeoFence SMS :') }}</div>
					<div class="col-md-6">{{ Form::select('sendGeoFenceSMS',  array( 'no' => 'No','yes' => 'Yes (Both)','Entry' => 'Entry','Exit' => 'Exit'  ), Input::old('sendGeoFenceSMS'), array('class' => 'form-control','id'=>'sendGeoFenceSMS')) }}</div>
				</div></br>

               <div id="entry">
               <div class="row">
               	   <div class="col-md-3">{{ Form::label('entry', 'Entry :') }}</div>
               </div><br/>

				<div class="row">
						<div class="col-md-3">{{ Form::label('morningEntryStartTime', 'Morning Entry Start Time:') }}</div>
						<div class="col-md-6">{{  Form::input('time', 'morningEntryStartTime', null, ['class' => 'form-control', 'placeholder' => 'time','id'=>'morningEntryStartTime'])}}</div>
			    </div> <br/>

			    <div class="row">
						<div class="col-md-3">{{ Form::label('morningEntryEndTime', 'Morning Entry End Time:') }}</div>
						<div class="col-md-6">{{  Form::input('time', 'morningEntryEndTime', null, ['class' => 'form-control', 'placeholder' => 'time','id'=>'morningEntryEndTime'])}}</div>
			    </div> <br/>
			    <div class="row">
						<div class="col-md-3">{{ Form::label('eveningEntryStartTime', 'Evening Entry Start Time:') }}</div>
						<div class="col-md-6">{{  Form::input('time', 'eveningEntryStartTime', null, ['class' => 'form-control', 'placeholder' => 'time','id'=>'eveningEntryStartTime'])}}</div>
			    </div> <br/>

			    <div class="row">
						<div class="col-md-3">{{ Form::label('eveningEntryEndTime', 'Evening Entry End Time:') }}</div>
						<div class="col-md-6">{{  Form::input('time', 'eveningEntryEndTime', null, ['class' => 'form-control', 'placeholder' => 'time','id'=>'eveningEntryEndTime'])}}</div>
			    </div> <br/>
			    </div>

                <div id="exit">
			    <div class="row">
               	   <div class="col-md-3">{{ Form::label('exit', 'Exit :') }}</div>
               </div><br/>


			    <div class="row">
						<div class="col-md-3">{{ Form::label('morningExitStartTime', 'Morning Exit Start Time:') }}</div>
						<div class="col-md-6">{{  Form::input('time', 'morningExitStartTime', null, ['class' => 'form-control', 'placeholder' => 'time','id'=>'morningExitStartTime'])}}</div>
			    </div> <br/>

			    <div class="row">
						<div class="col-md-3">{{ Form::label('morningExitEndTime', 'Morning Exit End Time:') }}</div>
						<div class="col-md-6">{{  Form::input('time', 'morningExitEndTime', null, ['class' => 'form-control', 'placeholder' => 'time','id'=>'morningExitEndTime'])}}</div>
			    </div> <br/>
			    <div class="row">
						<div class="col-md-3">{{ Form::label('eveningExitStartTime', 'Evening Exit Start Time:') }}</div>
						<div class="col-md-6">{{  Form::input('time', 'eveningExitStartTime', null, ['class' => 'form-control', 'placeholder' => 'time','id'=>'eveningExitStartTime'])}}</div>
			    </div> <br/>

			    <div class="row">
						<div class="col-md-3">{{ Form::label('eveningExitEndTime', 'Evening Exit End Time:') }}</div>
						<div class="col-md-6">{{  Form::input('time', 'eveningExitEndTime', null, ['class' => 'form-control', 'placeholder' => 'time','id'=>'eveningExitEndTime'])}}</div>
			    </div> <br/>
			    </div>

				<div class="row">
					<div class="col-md-3">{{ Form::label('geofence', 'GeoFence') }}</div>
					<div class="col-md-6">{{ Form::select('geofence',  array( 'nill' => 'nill','no' => 'OUTSIDE','yes' => 'INSIDE' ), Input::old('geofence'), array('class' => 'form-control')) }} </div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('safemove', 'Safety Movement Alert') }}</div>
					<div class="col-md-6">{{ Form::select('safemove',  array( 'no' => 'No','yes' => 'Yes' ), Input::old('safemove'), array('class' => 'form-control')) }} </div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('parkDuration', 'Park Duration (mins) :') }}</div>
					<div class="col-md-6">{{ Form::text('parkDuration', Input::old('parkDuration'), array('class' => 'form-control','placeholder'=>'Park Duration (mins)')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('idleDuration', 'Idle Duration (mins) :') }}</div>
					<div class="col-md-6">{{ Form::text('idleDuration', Input::old('idleDuration'), array('class' => 'form-control','placeholder'=>'Idle Duration (mins)')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('noDataDuration', 'No Data Duration :') }}</div>
					<div class="col-md-6">{{ Form::select('noDataDuration',  array( '' => 'Select','30' => '30 Mins','45' => '45 Mins','60' => '1 hr','120' => ' 2 hrs','240' => '4 hrs','360' => '6 hrs','480' => '8 hrs','720' => '12 hrs','960' => '16 hrs','1080' => '18 hrs','1200' => '20 hrs','1440' => '24 hrs'), Input::old('noDataDuration'), array('class' => 'form-control')) }}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('smsSender', 'Sms Sender') }}</div>
					<div class="col-md-6">{{ Form::text('smsSender', Input::old('smsSender'), array('class' => 'form-control','placeholder'=>'Sms Sender')) }}</div>
				</div>
				<br />
				<div class="row">
          			<div class="col-md-3">{{ Form::label('harshBreak','Harsh Breaking Alert') }}</div>
         			<div class="col-md-6">{{ Form::select('harshBreak',  array( 'no' => 'No','yes' => 'Yes' ), Input::old('harshBreak'), array('class' => 'form-control')) }}</div>
        		</div>
        		<br />
        		<div class="row">
   	      			<div class="col-md-3">{{ Form::label('dailySummary','Daily Summary') }}</div>
         			<div class="col-md-6">{{ Form::select('dailySummary',  array( 'no' => 'No','yes' => 'Yes' ), Input::old('dailySummary'), array('class' => 'form-control')) }}</div>
        		</div>
				<br />
				<div class="row">
   	        		<div class="col-md-3">{{ Form::label('dailyDieselSummary','Daily Diesel Summary') }}</div>
           			<div class="col-md-6">{{ Form::select('dailyDieselSummary',  array( 'no' => 'No','yes' => 'Yes' ),Input::old('dailyDieselSummary'), array('class' => 'form-control')) }}</div>
        		</div>
        		<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('fuelLevelBelow','Fuel Level Below') }}</div>
          			<div class="col-md-6">{{ Form::select('fuelLevelBelow',  array( 'no' => 'No','yes' => 'Yes' ), Input::old('fuelLevelBelow'), array('id'=>'fuelLevel','class' => 'form-control')) }}</div>
         		</div>
         		<br />
				<div class="row" id='fvalue'>
            		<div class="col-md-3">{{ Form::label('fuelLevelBelowValue','Fuel Level Below Value') }}</div>
            		<div class="col-md-6">{{ Form::number('fuelLevelBelowValue',Input::old('fuelLevelBelowValue'), array('class' => 'form-control','id'=>'fuelValue','min'=>'0.01','step'=>'0.01')) }}</div>
        		</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('fuelAlarm','Fuel Alarm ') }}</div>
          			<div class="col-md-6">{{ Form::select('fuelAlarm',  array( 'no' => 'No','yes' => 'Yes' ), Input::old('fuelAlarm'), array('class' => 'form-control')) }}</div>
         		</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('live', 'Show Live Site') }}</div>
					<div class="col-md-6">{{ Form::select('live',  array( 'no' => 'No','yes' => 'Yes' ), Input::old('live'), array('class' => 'form-control')) }} </div>
				</div><br/>
                <div class="row">
					<div class="col-md-3">{{ Form::label('escalatesms', 'SMS Escalation') }}</div>
					<div class="col-md-6">{{ Form::select('escalatesms',  array( 'no' => 'No','yes' => 'Yes' ), Input::old('escalatesms'), array('class' => 'form-control')) }} </div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-3">{{ Form::label('address', 'Address :') }}</div>
					<div class="col-md-6"><textarea rows="4" cols="40" class="form-control" placeholder="Address" name="address"></textarea></div>
				</div>
				<br />
				<ul id="itemsort">
					<div class="row">
						<div class="col-md-2">{{ Form::label('startTime', 'Start Time :') }}</div>
						<div class="col-md-4">{{  Form::input('time', 'time1', null, ['class' => 'form-control', 'placeholder' => 'time'])}}</div>
						<div class="col-md-2">{{ Form::label('endTime :', 'End Time :') }}</div>
						<div class="col-md-4">{{  Form::input('time', 'time2', null, ['class' => 'form-control', 'placeholder' => 'time'])}}</div>
					</div> 
				</ul>
				<br />
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-6">{{ Form::submit('Add the School/College/Organization!', array('class' => 'btn btn-primary','id'=>'submit')) }}</div>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){	
    $("#method2").mouseover(function(){
        this.type = "text";
    }).mouseout(function(){
        this.type = "password";
    })

$('#sendGeoFenceSMS').on('change',function() {
 
var input={
    'val':$('#sendGeoFenceSMS').val(), 
    'val1':$('#SchoolGeoFence').val()
 }
 
 if (input.val== 'Entry' && input.val1== 'yes') {
  $('#entry').show();
  $('#exit').hide();
 }else if(input.val== 'Exit' && input.val1== 'yes'){
    $('#exit').show();
    $('#entry').hide();
 }
 else if(input.val=='yes' && input.val1== 'yes')
 {
    $('#entry').show();
    $('#exit').show();
 }
 else
 {
    $('#entry').hide();
    $('#exit').hide();
 }

});

$('#isrfid').on('change',function() {
    var input={
       'val':$('#isrfid').val()
    }
    if (input.val== 'yes') {
        $('#pickup').show();
        $('#drop').show();
    }else{
        $('#pickup').hide();
        $('#drop').hide();
    }

});


$('#SchoolGeoFence').on('change',function() {
 
var input={
    'val':$('#sendGeoFenceSMS').val(), 
    'val1':$('#SchoolGeoFence').val()
 }
 
 if (input.val== 'Entry' && input.val1== 'yes') {
  $('#entry').show();
  $('#exit').hide();
 }else if(input.val== 'Exit' && input.val1== 'yes'){
    $('#exit').show();
    $('#entry').hide();
 }
 else if(input.val=='yes' && input.val1== 'yes')
 {
    $('#entry').show();
    $('#exit').show();
 }
 else
 {
    $('#entry').hide();
    $('#exit').hide();
 }


});


$('#submit').on('click',function(){
	
	//isRfid
	var rfid={
       'val':$('#isrfid').val()
    }
    if (rfid.val== 'yes') {
    	document.getElementById("PickupStartTime").required= 'required';
  		document.getElementById("PickupEndTime").required= 'required';
  		document.getElementById("DropStartTime").required= 'required';
  		document.getElementById("DropEndTime").required= 'required';
    }else{
    	document.getElementById("PickupStartTime").required= '';
  		document.getElementById("PickupEndTime").required= '';
  		document.getElementById("DropStartTime").required= '';
  		document.getElementById("DropEndTime").required= '';
    }
    
  var input={
    'val':$('#sendGeoFenceSMS').val(), 
    'val1':$('#SchoolGeoFence').val()
 }

 if (input.val== 'Entry' && input.val1== 'yes') {
  document.getElementById("morningEntryStartTime").required= 'required';
  document.getElementById("morningEntryEndTime").required= 'required';
  document.getElementById("eveningEntryStartTime").required= 'required';
  document.getElementById("eveningEntryEndTime").required= 'required';
  document.getElementById("morningExitStartTime").required= '';
  document.getElementById("morningExitEndTime").required= '';
  document.getElementById("eveningExitStartTime").required= '';
  document.getElementById("eveningExitEndTime").required= '';
  document.getElementById("morningExitStartTime").value= '';
  document.getElementById("morningExitEndTime").value= '';
  document.getElementById("eveningExitStartTime").value= '';
  document.getElementById("eveningExitEndTime").value= '';
 }else if(input.val== 'Exit' && input.val1== 'yes'){
  document.getElementById("morningExitStartTime").required= 'required';
  document.getElementById("morningExitEndTime").required= 'required';
  document.getElementById("eveningExitStartTime").required= 'required';
  document.getElementById("eveningExitEndTime").required= 'required';
  document.getElementById("morningEntryStartTime").required= '';
  document.getElementById("morningEntryEndTime").required= '';
  document.getElementById("eveningEntryStartTime").required= '';
  document.getElementById("eveningEntryEndTime").required= '';
  document.getElementById("morningEntryStartTime").value= '';
  document.getElementById("morningEntryEndTime").value= '';
  document.getElementById("eveningEntryStartTime").value= '';
  document.getElementById("eveningEntryEndTime").value= '';
 }
 else if(input.val=='yes' && input.val1== 'yes')
 {
  document.getElementById("morningEntryStartTime").required= 'required';
  document.getElementById("morningEntryEndTime").required= 'required';
  document.getElementById("eveningEntryStartTime").required= 'required';
  document.getElementById("eveningEntryEndTime").required= 'required';
  document.getElementById("morningExitStartTime").required= 'required';
  document.getElementById("morningExitEndTime").required= 'required';
  document.getElementById("eveningExitStartTime").required= 'required';
  document.getElementById("eveningExitEndTime").required= 'required';
 }
 else
 {
  document.getElementById("morningEntryStartTime").required= '';
  document.getElementById("morningEntryEndTime").required= '';
  document.getElementById("eveningEntryStartTime").required= '';
  document.getElementById("eveningEntryEndTime").required= '';
  document.getElementById("morningEntryStartTime").value= '';
  document.getElementById("morningEntryEndTime").value= '';
  document.getElementById("eveningEntryStartTime").value= '';
  document.getElementById("eveningEntryEndTime").value= '';

  document.getElementById("morningExitStartTime").required= '';
  document.getElementById("morningExitEndTime").required= '';
  document.getElementById("eveningExitStartTime").required= '';
  document.getElementById("eveningExitEndTime").required= '';
  document.getElementById("morningExitStartTime").value= '';
  document.getElementById("morningExitEndTime").value= '';
  document.getElementById("eveningExitStartTime").value= '';
  document.getElementById("eveningExitEndTime").value= '';
    
 }


});


});
</script>

<script type="text/javascript">
$('#orgId').on('change', function() {
		orgcheck();
		
	});
$('#email').on('change', function() {
		orgcheck();
		
	});
function orgcheck()
{
	$('#validation').text('');
		var postValue = {
			'id': $('#orgId').val()

			};
		// alert($('#groupName').val());
		$.post('{{ route("ajax.ordIdCheck") }}',postValue)
			.done(function(data) {
				if(data!='') {
				$('#validation').text(data);
				alert(data);
				document.getElementById("orgId").focus();
				return false;
			}
        		
      		}).fail(function() {
        		console.log("fail");
      });
}
function caps(element){
   element.value = element.value.toUpperCase();
  }

$('#fuelLevel').on('change',function() {
   
    var input={
    'val':$('#fuelLevel').val()
 }
  if(input.val=='yes')
 {
   document.getElementById("fvalue").style.display ="block";
   document.getElementById("fuelValue").required= 'required';
 }
 else {
    document.getElementById("fvalue").style.display ="none"; 
   document.getElementById("fuelValue").required= ''; 
   document.getElementById("fuelValue").value= '';
}

});

window.onload = function()
    {
        var datas={
                'val':$('#fuelLevel').val()
                }
               
        if(datas.val=='no')
        {

       document.getElementById("fvalue").style.display ="none";
       }
var input={
    'val':$('#sendGeoFenceSMS').val(), 
    'val1':$('#SchoolGeoFence').val()
 }
if (input.val== 'Entry' && input.val1== 'yes') {
  $('#entry').show();
  $('#exit').hide();
 }else if(input.val== 'Exit' && input.val1== 'yes'){
    $('#exit').show();
    $('#entry').hide();
 }
 else if(input.val=='yes' && input.val1== 'yes')
 {
    $('#entry').show();
    $('#exit').show();
 }
 else
 {
    $('#entry').hide();
    $('#exit').hide();
 }
 
  //is rfid
 var rfid={
       'val':$('#isrfid').val()
    }
    if (rfid.val== 'yes') {
        $('#pickup').show();
        $('#drop').show();
    }else{
        $('#pickup').hide();
        $('#drop').hide();
    }


}

</script>
@include('includes.js_create')
</body>
</html>