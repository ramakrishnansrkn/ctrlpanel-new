@include('includes.header_create')
<!-- Main Wrapper -->
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style>
	#enable{
	 min-width: 135px;
	 min-height: 37px;
     border-radius: 4px;
     border: 1px solid #E1E0E3;
     padding-left: 8px;
     font-size: 14px;
    }
    #days{
    	min-width: 135px;
	 min-height: 37px;
     border-radius: 4px;
     border: 1px solid #E1E0E3;
     padding-left: 8px;
     font-size: 14px;
    }
    .btn {
        background-color: DodgerBlue;
        border: none;
        color: white;
        font-size: 16px;
        cursor: pointer;
    }
    .btn:hover {
        background-color: RoyalBlue;
    }
    </style>
</head>
<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
    		<div class="col-lg-12">
        		<div class="hpanel">
               		 <div class="panel-heading">
                  		<h4><b><font> User Create</font></b></h4>
                	</div>
               		 <div class="panel-body">
				
                		{{ HTML::ul($errors->all()) }}
						{{ Form::open(array('url' => 'vdmUsers','id'=>'formid')) }}
						<div class="col-md-12">
							<span id="validation" style="color: red; font-weight: bold"></span>
						</div>
							<div class="row">
							<div class="col-md-4">
							<h5><font color='red', size='1px'>{{ Form::label('#User name is case sensitive and space is not allowed') }}</font></h5>
							<div class="form-group">
							{{ Form::label('userId', 'User Name / User Id *') }}
							{{ Form::text('userId', $userName, array('class' =>'form-control','placeholder'=>'UserName', 'required' => 'required', 'id'=>'userID','onkeyup' => 'caps(this)')) }}
							</div>
							</br>
							<div class="form-group">
							{{ Form::label('mobileNo', 'Mobile Number *') }}
							{{ Form::text('mobileNo', Input::old('mobileNo'), array('class' => 'form-control','placeholder'=>'Mobile Number', 'required' => 'required','id'=>'mob')) }}
							</div>
							</br>
							<div class="form-group">
							{{ Form::label('companyName', 'Company Name') }}
							{{ Form::text('companyName', Input::old('mobileNo'), array('class' => 'form-control','placeholder'=>'Company Name','onkeyup' => 'validate(this)')) }}
							</div>
							</div>
							<div class="col-md-4">
							<div class="form-group">
							{{ Form::label('email', 'Email *') }}
							
							{{ Form::Email('email', Input::old('email'), array('class' => 'form-control','placeholder'=>'Email', 'required' => 'required')) }}
							</div>
							<br/>
							<div class="form-group">
							{{ Form::label('cc_email', 'CC Mails') }}
							<input type="email" id="cc_email" class="form-control" name="cc_email"  multiple placeholder="Email">
							</div>
							</br>
							 <div class="form-group">
							{{ Form::label('password', 'Password *') }}
							
							{{ Form::text('password', Input::old('password'), array('class' =>'form-control','placeholder'=>'Password', 'required' => 'required','id'=>'userpass','onmouseover'=>'mouseoverPass()','onmouseout'=>'mouseoutPass()')) }}
							</div>

							</br>
							 <div class="form-group">
							{{ Form::label('zoho', 'zoho ') }}
							
							{{ Form::text('zoho', Input::old('zoho'), array('class' =>'form-control','placeholder'=>'zoho', 'required' => 'required')) }}
							</div>
							<div class="form-group">
								 <div class="row">
                            	 <div class="col-md-6">{{ Form::label('enable', 'Enable debugs:') }}</div>
                            	 <div class="col-md-3">{{ Form::select('enable', array('Disable' => 'Disable','Enable' => 'Enable'), array('class' => 'form-control','id'=>'enable', 'required'=>'required')) }}</div>
                    			</div>
							</div>
							</br>
							 <div class="form-group">
							 {{Form::checkbox('virtualaccount', 'value',true, ['class' => 'check1'])}}
							{{ Form::label('virtualaccount', 'Virtual Account') }}
							</div>
							<div class="form-group">
							 {{Form::checkbox('assetuser', 'value',false, ['class' => 'check1'])}}
							{{ Form::label('assetuser', 'Asset User') }}
							</div>
							</div>
	                        <div class="col-md-3" style="text-align: right"><br>
							<h6>{{ Form::submit('submit', array('class' => 'btn btn-primary')) }}</h6>
							</div>
							</div>
							<hr>
							 
							<div class="row">
								 <h5><font color="#086fa1">{{ Form::label('vehicleGroups', 'Select the Groups:') }}</font></h5>
                            <div class="col-md-5">
                                <h5> {{ Form::label('Filter', 'Search :') }}</h5>
                                    <input class="searchkey" placeholder="Search.." id="search" name="searchtext" type="text" style="width: 50%;height: 5vh;">
                                    <button class="btn" type="button" id="btnsearch"><i class="fa fa-search"></i></button>
                            </div>
<div class="col-md-4">{{Form::label('Select All :')}} {{Form::checkbox('$vehicleGroups', 'value', false, ['class' => 'check'])}}</div>
							 </div>
							 </br>



							 	<div class="row">
				            		<div class="col-md-12" id="selectedItems" style="border-bottom: 1px solid #a6a6a6;"></div>
				            		<br>
				            		<div class="col-md-12" id="unSelectedItems">
					            		@if(isset($vehicleGroups))
									        @foreach($vehicleGroups as $key => $value)
												<div class="col-md-3 vehiclelist">
													{{ Form::checkbox('vehicleGroups[]', $key, null, ['class' => 'field', 'id' => 'questionCheckBox']) }}
													{{ Form::label($value) }}
												</div>
											@endforeach
										@endif
					            	</div>
					            </div>



							            
								
								{{ Form::close() }}
						</div>
						
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script type="text/javascript">
	list = [];
    var value = <?php echo json_encode($vehicleGroups ); ?>;
	
function usercheck()
{
	$('#validation').text('');
		var postValue = {
			'id': $('#userID').val()

			};
		// alert($('#groupName').val());
		$.post('{{ route("ajax.userIdCheck") }}',postValue)
			.done(function(data) {
      if(data!='')
				{
				alert('User Id already exist. Please use different id ');
				$('#validation').text(data);
				document.getElementById("userID").focus();
				return false;
			}
        		
        	}).fail(function() {
        		console.log("fail");
      });

}	
	$('#userID').on('change', function() {
		
		usercheck();
	});
	$('#mob').on('change', function() {
		
		usercheck();
	});

 function caps(element){
    element.value = element.value.toUpperCase();
}
function mouseoverPass(obj) {
  var obj = document.getElementById('userpass');
  obj.type = "text";
}
function mouseoutPass(obj) {
  var obj = document.getElementById('userpass');
  obj.type = "password";	
} 

function validate(element){
var word=element.value
var new1 = word.replace(/[\'\/~`\!@#\$%\^&\*\(\)\\\+=\{\}\[\]\|;:"\<\>,\?\\\']/,"")
element.value=new1
}

</script>
@include('includes.js_footer')
@include('includes.js_create')
</body>
</html>
