<head>

<style>
table, td, th {
    border: 1px solid black;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th {
    height: 30px;
}

.caption {
  background: #f2f2f2;
  padding: 5px;
  font-weight: bolder;
}

</style></head>
<h4>Hi, {{ $fcode}}!</h4>

<p>The following action is move vehicle.</p>

<p>Franchise Name -  {{$fcode}}</p>
<table>
  <tr>
    
    <td colspan="4" align="center">Device Moved from <b>{{$oldown}}</b> to <b>{{$newown}}</b></td>
    
</tr>
  <tr>
    
    <td colspan="2" class="caption">Vehicle Details</td>
     <td colspan="2" class="caption">Device Details</td>
    
</tr>

<tbody>
  <tr>
    <td colspan="2">{{$Vehicle}}</td>
     <td colspan="2">{{$Device}}</td>
</tr>

</tbody>
</table>

<p> 
Thanks,
<br>
GPS Platform.
 </p>
