<div style="box-shadow:0px 8px 17px rgba(0, 0, 0, 0.2)  !important;">     
<nav class="menus" >
    <ul>
        <li class="cl-effect-5">
            
            <!-- <a><span  data-hover="Tracking">Tracking</span></a><span class="glyphicons glyphicons-road"></span> -->
            <a><span  data-hover="<?php echo Lang::get('content.tracking'); ?>">&nbsp;&nbsp;<div class="glyphicon glyphicon-map-marker"></div>&nbsp;<?php echo Lang::get('content.tracking'); ?></span></a>
            <ul>
                <li class="removeli" id="historyRl"><a id="history" href="" style="left: 7px;" target="_blank"><img src="assets/menu/history.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.history'); ?></div></a></li> 
                <li class="removeli" id="singleTrackRl"><a id="singleTrack" href="" style="left: 7px;"><img src="assets/menu/single_track.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.single_track'); ?></div></a></li>
                <li class="removeli" id="multiTrackRl"><a id="multiTrack" href="" style="left: 7px;"><img src="assets/menu/multi_track.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.multi_track'); ?></div></a></li>
                <!-- <li class="removeli" id="multiTrackRlNew"><a id="multiTrackNew" href=""><img src="assets/menu/multi_track.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px">MultiTrack New</div></a></li> -->
                <li class="removeli removeli_2" id="routesRl"><a href="../public/track?rt=routes&maps=newreplay" style="left: 7px;" target="_blank"><img src="assets/menu/history.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.routes'); ?></div></a></li>
                <li class="removeli removeli_2" id="addsiteRl"><!-- <a id="addsites">Admin</a>
                    <div  style="padding-left: 10px"> -->
                       <a href="../public/password_check" id="addsites" style="left: 7px;"><img src="assets/menu/Addsite_Geofence.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.addsite_geofence'); ?></div></a>
               <!-- </div> -->
                </li>
              <!-- <li class="removeli" id="singleTrackRls"><a id="landNew" href="" target="_blank">New Landing Page</a></li> -->
              <!--  <li class="removeli"><a id="dashNew" href="../public/dashNew" target="_blank">New DashBoard</a></li> -->
                <!-- <li id="removeli"><a href="../public/groupEdit">Edit Group</a></li> -->
            </ul>
        </li>
        <li class="cl-effect-5" style="margin-left: -22px;">
            <!-- <a><span data-hover="Reports">Reports</span></a> -->
            <a><span  data-hover="<?php echo Lang::get('content.reports'); ?>">&nbsp;&nbsp;<div class="glyphicon glyphicon-list-alt"></div>&nbsp;<?php echo Lang::get('content.reports'); ?></span></a>
            <ul>
                <li class="removeli" id="curStatRl"><a href="../public/reports?ind=1" style="left: 7px;"><img src="assets/menu/Dashboard.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.dashboard'); ?></div></a></li>
                <li class="removeli" id="vehConsRl"><a id="assetLabel" href="../public/reports?ind=2" style="left: 7px;"><img src="assets/menu/Vehicles_Consolidated.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.vehicles_consolidated'); ?> </div></a></li>
                <li class="removeli" id="conSiteRl"><a href="../public/reports?ind=3" style="left: 7px;"><img src="assets/menu/Consolidated.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.cons_location'); ?></div></a></li>
           <!-- <li class="removeli"><a href="../public/reports?ind=4"> <img src="assets/menu/Site.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px">Site Location</div></a></li> -->
                <li class="removeli" id="conOvrRl"><a href="../public/reports?ind=4" style="left: 7px;"><img src="assets/menu/Consolidated_OverSpeed.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.cons_overspeed'); ?></div></a></li>
                <li class="removeli" id="stopReportRl"><a id="stopReport" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Stoppage.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.cons_stoppage'); ?></div></a></li>
                <li class="removeli" id="conSiteLocRl"><a id="ConSiteLocReport" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Site_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.cons_site_report'); ?></div></a></li>
                <li class="removeli" id="tollReportRl"><a id="tollReport" href="" style="left: 7px;"><img src="assets/menu/Tollgate_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.tollgate_report'); ?></div></a></li>
                <li class="removeli" id="nonMovingReportRl"><a id="nonMovingReport" href="" style="left: 7px;"><img src="assets/menu/Non_Moving_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.non_moving_report'); ?></div></a></li>
                <li class="removeli" id="travelSumReportRl"><a id="travelSumReport" href="" style="left: 7px;"><img src="assets/menu/Travel_Summary_list.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.travell_summary'); ?></div></a></li>
                <li class="removeli" id="fuelConReportRl"><a id="fuelConReport" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Fuel_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.cons_fuel_report'); ?></div></a></li>
                <li class="removeli" id="conPriEngReportRl"><a id="conPriEngReport" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Primary_Engine_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.cons_pri_report'); ?></div></a></li>
                <li class="removeli" id="empAtnReportRl"><a id="empAtnReport" href="" style="left: 7px;"><img src="assets/menu/Employee_Attendance_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.employee_attendance'); ?></div></a></li>
                <li class="removeli" id="SchoolSmsReportRl"><a id="SchoolSmsReport" href="" style="left: 7px;"><img src="assets/menu/School_SMS_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.school_sms_report'); ?></div></a></li>
                <li class="removeli" id="siteAlertReportRl"><a id="siteAlertReport" href="" style="left: 7px;"><img src="assets/menu/Site_Stpoppage_Alert_Rrport.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.site_alert'); ?> </div></a></li>
                <li class="removeli" id="conRfidReportRl"><a id="conRfidReport" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Rfid_Tag_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.cons_rfid_report'); ?></div></a></li>
                <li class="removeli" id="conAlarmReportRl"><a id="conAlarmReport" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Alarm_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.cons_alarm_report'); ?></div></a></li>

                <!-- <li> <a href="#"> Advanced &nbsp; </a>
                     <div style="padding-left: 10px">
                        <table style="width: 300px; height: 100px;">
                            <tr>
                                <td><a id="movement" href="" class="lowcase"> Movement </a></td>
                                <td><a id="overspeed" href="" class="lowcase"> OverSpeed </a></td>
                                <td><a id="parked" href="" class="lowcase"> Parked </a></td>
                            </tr>
                            <tr>
                                <td><a id="idle" href="" class="lowcase"> Idle </a></td>
                                <td><a id="event" href="" class="lowcase"> Event </a></td>
                                <td><a id="site" href="" class="lowcase"> Site </a></td>
                            </tr>
                            <tr>
                                <td><a id="load" href="" class="lowcase"> Load </a></td>
                                <td><a id="fuel" href="" class="lowcase"> Fuel </a></td>
                                <td><a id="ignition" href="" class="lowcase"> Ignition </a></td>
                            </tr>
                            <tr>
                                <td><a id="trip" href="" class="lowcase"> Trip Time </a></td>
                                <td><a id="tripkms" href="" class="lowcase"> Trip Summary</a></td>
                                <td><a id="temp" href="" class="lowcase"> Temperature</a></td>
                            </tr>
                            <tr>
                                <td><a id="alarm" href="" class="lowcase">Alarm</a></td>
                            </tr>
                        </table>

                    </div>
                </li> -->
            </ul>
        </li>
            <li class="cl-effect-5" style="margin-left: -38px;">
                <!-- <a><span data-hover="analytics">analytics</span></a> -->
                <a><span  data-hover="<?php echo Lang::get('content.analytics'); ?>">&nbsp;&nbsp;<div class="glyphicon glyphicon-screenshot"></div>&nbsp;<?php echo Lang::get('content.analytics'); ?></span></a>
                <ul>
                    <li class="removeli" id="movementRl"><a id="movement" href="" style="left: 7px;"><img src="assets/menu/Movement.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.movement'); ?></div></a></li>
                    <li class="removeli" id="overspeedRl"><a id="overspeed" href="" style="left: 7px;"> <img src="assets/menu/OverSpeed.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.overspeed'); ?></div> </a></li>
                    <li class="removeli" id="parkedRl"><a id="parked" href="" style="left: 7px;"> <img src="assets/menu/Parked.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.parked'); ?></div> </a></li>
                    <li class="removeli" id="idleRl"><a id="idle" href="" style="left: 7px;"> <img src="assets/menu/Idle.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.idle'); ?> </div></a></li>
                    <li class="removeli" id="ignitionRl"><a id="ignition" href="" style="left: 7px;"><img src="assets/menu/Ignition.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.ign'); ?> </div></a></li>
                    <!-- <li class="removeli" id="fuelRl"><a id="fuel" href=""><img src="assets/menu/Fuel.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.fuel'); ?> </div></a></li> -->
               <!-- <li class="removeli" id="eventRl"><a id="event" href=""><img src="assets/menu/Event.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> Event </div></a></li>
                    <li class="removeli" id="siteRl"><a id="site" href=""><img src="assets/menu/Site.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> Site </div></a></li>
                    <li class="removeli" id="multiSiteRl"><a id="multiSite" href=""><img src="assets/menu/Site.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> Site (Mulitple Site)</div></a></li> -->
                    <li class="removeli" id="tripSiteRl"><a id="tripSite" href="" style="left: 7px;"><img src="assets/menu/Site.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.site_trip'); ?></div></a></li>
                    <li class="removeli" id="tripRl"><a id="trip" href="" style="left: 7px;"><img src="assets/menu/Trip_Time.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.trip_time'); ?></div> </a></li>
                    <li class="removeli" id="tripkmsRl"><a id="tripkms" href="" style="left: 7px;"><img src="assets/menu/Trip_Summary.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.trip_summary'); ?></div></a></li>
                    <li class="removeli" id="alarmRl"><a id="alarm" href="" style="left: 7px;"><img src="assets/menu/Alarm.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.alarm'); ?></div></a></li>
                    <li class="removeli" id="stoppageRl"><a id="stoppage" href="" style="left: 7px;"><img src="assets/menu/Stoppage.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.stoppage'); ?></div></a></li>
                    <li class="removeli" id="noDataRl"><a id="noData" href="" style="left: 7px;"><img src="assets/menu/No_Data.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.n_data'); ?></div></a></li>
                    <li class="removeli" id="routeDeviationRl"><a id="routeDev" href="" style="left: 7px;"><img src="assets/menu/Route_Deviation.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.route_deviation'); ?></div></a></li>
                </ul>
            </li>
        <li class="cl-effect-5" style="margin-left: -28px;">
            <!-- <a><span data-hover="Statistics">Statistics</span></a> -->
            <a><span  data-hover="<?php echo Lang::get('content.statistics'); ?>">&nbsp;&nbsp;<div class="glyphicon glyphicon-stats"></div>&nbsp;<?php echo Lang::get('content.statistics'); ?></span></a>
            <ul>
                <li class="removeli" id="dailyRl"><a href="../public/statistics?ind=1" style="left: 7px;"><img src="assets/menu/Vehiclewise_Performance.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.vehiclewise_performance'); ?></div></a></li>
                <li class="removeli" id="poiRl"><a href="../public/statistics?ind=2" style="left: 7px;"><img src="assets/menu/POI.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.poi'); ?></div></a></li>
                <li class="removeli" id="consolRl"><a href="../public/statistics?ind=3" style="left: 7px;"><img src="assets/menu/Consolidated.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.consolidated'); ?></div></a></li>
                <li class="removeli" id="exfuelRl"><a href="../public/statistics?ind=4" style="left: 7px;"><img src="assets/menu/Excutive_Fuel.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.exe_fuel'); ?></div></a></li>
                <li class="removeli" id="monDistRl"><a href="../public/statistics?ind=5" style="left: 7px;"><img src="assets/menu/Monthly_Dist.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.monthly_dist'); ?></div></a></li> 
                <li class="removeli" id="monDistFuelRl"><a href="../public/statistics?ind=6" style="left: 7px;"><img src="assets/menu/Monthly_Dist_and_Fuel.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.monthly_dist_fuel'); ?></div></a></li> 
            </ul>
        </li>
        <li class="cl-effect-5" id="sensMenu">
        <a><span  data-hover="<?php echo Lang::get('content.sensor'); ?>">&nbsp;&nbsp;<div class="glyphicon glyphicon-list"></div>&nbsp;<?php echo Lang::get('content.sensor'); ?></span></a>
            <!-- <a><span data-hover="sensor">sensor</span></a> -->
            <ul class="scrollbox" style=" max-height: 600px;overflow-y: auto;overflow-x: hidden;">
                    <li class="removeli" id="acRl"><a id="ac" href="" style="left: 7px;"> <img src="assets/menu/AC .png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.ac'); ?></div> </a></li>
                    <li class="removeli" id="idleWasteRl"><a id="idleWaste" href="" style="left: 7px;"><img src="assets/menu/Idle_Wastage .png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.idle_wastage'); ?></div> </a></li>
                    <li class="removeli" id="prmEngineOnRl"><a id="prmEngineOn" href="" style="left: 7px;"><img src="assets/menu/Primary_Engine_On.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.pri_eng_on'); ?> </div></a></li>
                    <li class="removeli" id="doorSensorRl"><a id="doorSensor" href="" style="left: 7px;"><img src="assets/menu/secondary_Engine_On.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px">
                    <?php echo Lang::get('content.door_sensor'); ?></div> </a></li>
                    <li class="removeli" id="engineOnRl"><a id="engineOn" href="" style="left: 7px;"><img src="assets/menu/secondary_Engine_On.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.sec_eng_on'); ?></div> </a></li>
                <!--<li class="removeli" id="loadRl"><a id="load" href=""> Load </a></li>-->
                    <li class="removeli" id="tempRl"><a id="temp" href="" style="left: 7px;"> <img src="assets/menu/Temperature.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.temperature'); ?></div></a></li>
                    <li class="removeli" id="tempdevRl"><a id="tempdev" href="" style="left: 7px;"> <img src="assets/menu/Temperature_Deviation.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.temp_deviation'); ?></div></a></li>
                <!--<li class="removeli" id="distTimeRl"><a href="../public/fuel">Distance &amp; Time</a></li>-->
                   <!--  <li class="removeli" id="fuelNewRl"><a id="fuelNew" href=""><img src="assets/menu/Fuel.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_speed'); ?></div></a></li> -->
                <!--<li class="removeli" id="fuelFillRl"><a href="../public/fuel?tab=fuelfill">Fuel Fill</a></li>-->
                    <!-- <li class="removeli" id="fuelFillRl"><a id="fuelFill" href=""><img src="assets/menu/No_Data" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_fill'); ?></div></a></li> -->
                    <li class="removeli" id="fuelMachineRl"><a id="fuelMachine" href="" style="left: 7px;"><img src="assets/menu/Fuel Analytics.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_analytics'); ?></div></a></li>
                     <li class="removeli" id="fuelFillV2Rl"><a id="fuelFillV2" href="../public/fuelFillV2?tn=fuelfillV2" style="left: 7px;"><img src="assets/menu/Fuel Fill_BETA.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuelfill_BETA'); ?></div></a></li>
                    <!-- <li class="removeli" id="fuelTheftRl"><a  id="fuelTheft" href="" ><img src="assets/menu/No_Data" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_theft'); ?></div></a></li> -->
                    <li class="removeli" id="fuelProtocolRl"><a  id="fuelProtocol" href="" style="left: 7px;"><img src="assets/menu/Protocol_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.protocol'); ?> <?php echo Lang::get('content.report'); ?></div></a></li>
                    <!-- <li class="removeli" id="fuelMileageRl"><a  id="fuelMileage" href="" ><img src="assets/menu/No_Data" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_mileage'); ?></div></a></li>
                    <li class="removeli" id="fuelMileageNewRl"><a  id="fuelMileageNew" href="" ><img src="assets/menu/Fuel.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_mileage_new'); ?></div></a></li> -->
                    <li class="removeli" id="fuelRawRl"><a  id="fuelRaw" href="" style="left: 7px;"><img src="assets/menu/Fuel Raw Data.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_raw_data'); ?></div></a></li>
                    <li class="removeli" id="fuelConVehiRl"><a  id="fuelConVehicle" href="" style="left: 7px;"> <img src="assets/menu/Vehicle_Wise_Fuel_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.vehicle_wise_report'); ?></div></a></li>
                    <li class="removeli" id="fuelConTimeRl"><a  id="fuelConTime" href="" style="left: 7px;"><img src="assets/menu/Vehicle_Wise_Intraday_Fuel_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.vehicle_wise_intraday'); ?> </div></a></li>
                    <li class="removeli" id="fuelPerformanceRl"><a  id="fuelPerfor" href="" style="left: 7px;"><img src="assets/menu/Fuel_Performance_Reports.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_performance'); ?></div></a></li>
                    <li class="removeli" id="rfidRl"><a id="rfid" href="" style="left: 7px;"><img src="assets/menu/Rfid.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.rfid'); ?></div></a></li>
                    <li class="removeli" id="rfidNewRl"><a id="rfidNew" href="" style="left: 7px;"><img src="assets/menu/Rfid_Tag.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.rfid_tag'); ?></div></a></li>
                    <li class="removeli" id="cameraRl"><a id="camera" href="" style="left: 7px;"><img src="assets/menu/Camera.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.camera'); ?></div></a></li> 
            </ul>
        </li>
        <!-- <li class="cl-effect-5">
           
            <a><span  data-hover="Fuel"><div class="glyphicon glyphicon-tint"></div>&nbsp;Fuel</span></a>
            <ul>
                <li><a href="../public/fuel">Distance &amp; Time</a></li>
                <li><a href="../public/fuel?tab=fuelfill">Fuel Fill</a></li>
            </ul>
        </li> -->
        <li class="cl-effect-5" style="margin-left: -41px;">
            <a><span  data-hover="<?php echo Lang::get('content.performance'); ?>">&nbsp;&nbsp;<div class="glyphicon glyphicon-stats"></div>&nbsp;<?php echo Lang::get('content.performance'); ?></span></a>
            <!-- <a><span  data-hover="Performance">Performance</span></a> -->
            <ul>
                <li class="removeli" id="dailyPerfRl"><a href="../public/performance?tab=daily" style="left: 7px;"><img src="assets/menu/Daily_Performance.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.daily_per'); ?></div></a></li>
                <li class="removeli" id="monPerfRl"><a href="../public/performance" style="left: 7px;"><img src="assets/menu/Monthly_Performance.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.monthly_per'); ?></div></a></li>
            </ul>
        </li>
        
        <li id="userId">
            <span class="glyphicon glyphicon-user"><span id="valueUser" style="font-size:12px; font-weight: bold; font-family: Helvetica"></span></span>  
            <ul>           
                <li id="payDetRl"><a href="../public/payDetails"><?php echo Lang::get('content.payment_details'); ?></a></li>
                <li class="removeli removeli_2" id="editGrpRl"><a href="../public/groupEdit"><?php echo Lang::get('content.edit_group'); ?></a></li>
                <li class="removeli removeli_2" id="editNotRl"><a href="../public/passWd?userlevel=notify"><?php echo Lang::get('content.edit_notification'); ?></a></li>
                <li class="removeli removeli_2" id="editUsNotRl"><a href="../public/userNotify"><?php echo Lang::get('content.edit_user_notification'); ?></a></li>
                <li class="removeli removeli_2" id="resetRl"><a href="../public/passWd?userlevel=reset"><?php echo Lang::get('content.reset_password'); ?></a></li>
                <li class="removeli removeli_2" id="editBusRl"><a href="../public/passWd?userlevel=busStop"><?php echo Lang::get('content.edit_busstop'); ?></a></li>
                <li class="removeli removeli_2" id="getApiRl"><a href="../public/passWd?userlevel=apiKeys"><?php echo Lang::get('content.get_apikey'); ?></a></li>
                <li class="removeli removeli_2" id="homepage"><a href="../public/showUserPre"><?php echo Lang::get('content.customize_landing_page'); ?></a></li>
                <li class="removeli removeli_2" id="homepage"><a href="langSuggestion"><?php echo Lang::get('content.language_suggestion'); ?></a></li>
                 <!-- <li class="removeli removeli_2" id="English"><a href="English">english</a></li>
                <li class="removeli removeli_2" id="Hindi"><a href="Hindi">hindi</a></li> -->
                <li class="removeli removeli_2"  id="homepage" data-toggle="collapse" data-target="#demo" ng-app="mapApp" ng-controller="mainCtrl"><a href="#demo">
                    <span id="changedlanguage"></span><i class="fa fa-angle-down"></i></a></li>
                <div id="demo" class="collapse">
                   <li class="removeli removeli_2" id="English"><a href="English">English</a></li>
                   <li class="removeli removeli_2" id="Hindi"><a href="Hindi">Hindi</a></li>
                </div>
            </ul>
        </li>
        
        <li>
            <a   style="padding: 10px 15px;color: #fff; text-align: center;"><span class="glyphicon glyphicon-log-out" onclick="logout()" style="cursor: pointer;"></span></a>
        </li>
    </ul>
</nav>
</div>

<script type="text/javascript">
  if(document.getElementById("search_box")){
    document.getElementById("search_box").value="";
  }
  


    function logout()
{
    window.location.href = 'logout';
}
        var swUser="<?php echo Session::get('swUser'); ?>";
        //alert(swUser);
        if(swUser){
        localStorage.setItem('userIdName', JSON.stringify('username'+","+swUser));
        }

// $('#pwdModal').appendTo("body") 
    
    // var menuValue = JSON.parse(localStorage.getItem('userIdName'));
    // var sp = [];
    // try{

    //     sp = menuValue.split(",");
        
    // }catch (err){
    //     $.ajax({

    //         async: false,
    //         method: 'GET', 
    //         url: "aUthName",
    //         // data: {"orgId":$scope.orgIds},
    //         success: function (response) {

    //             console.log(response)
    //             sp[1] = response[1];
    //         }
    //     })
    //     // sp[1] = 'DEMO';
    //     // console.log(document.location)
    // }


        var globalIP = document.location.host;
        var contextMenu = '/'+document.location.pathname.split('/')[1];
        var labelVal;
        var menuValue = JSON.parse(localStorage.getItem('userIdName'));

        window.localStorage.setItem("userMasterName",menuValue);
        sp1 = menuValue.split(",");
        $('#valueUser').text("  "+sp1[1]);
        if(localStorage.getItem('lang')=='en')
        $('#changedlanguage').text('English');
        else if(localStorage.getItem('lang')=='hi')
        $('#changedlanguage').text('Hindi');
        else 
        $('#changedlanguage').text('English');
        if(sp1[1]!='MSS'&&sp1[1]!='SALEM-MINES')document.getElementById('fuelFillV2Rl').innerHTML = '';
        //if(sp1[1]!='DEMO')document.getElementById('multiTrackRlNew').innerHTML = '';
        console.log(sp1[1]);

        var obj = JSON.parse(localStorage.getItem('user'));
      //var menuObj = JSON.parse(window.localStorage.getItem('MenuData'));
      //console.log(menuObj);
        var sp;
        var vid;
        var gname; 
        if(obj!=null){
            sp     =  obj.split(',');
            vid    =  sp[0];
            gname  =  sp[1];
        }
        else{
            vid    =  "<?php echo Session::get('vehicle'); ?>";
            //alert(vid)
            gname  =  "<?php echo Session::get('group'); ?>";
        }
       if(localStorage.getItem('timeTochange')!='yes'){
                  var dateObj           =   new Date();
                  var fromNowTS        =   new Date(dateObj.setDate(dateObj.getDate()-1));
                  var totime        =   formatAMPM(fromNowTS.getTime());
                  localStorage.setItem('toTime', totime);
                  //alert(localStorage.getItem('toTime'));
             }
           function formatAMPM(date) {
                  var date = new Date(date);
                  var hours = date.getHours();
                  var minutes = date.getMinutes();
                  var ampm = hours >= 12 ? 'PM' : 'AM';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  minutes = minutes < 10 ? '0'+minutes : minutes;
                  var strTime = hours + ':' + minutes + ' ' + ampm;
                  return strTime;
             }
        
            //$("#movement").click(function() {
              //  window.localStorage.removeItem("reload");
               $("#movement").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=movement' );
           // });
           // $("#noData").click(function() {
                $("#noData").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=noData' );
                $("#routeDev").attr("href", 'routeDeviation?vid='+vid+'&vg='+gname+'&tn=routeDeviation' );
           // });

          //  $("#overspeed").click(function() {
                $("#overspeed").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=overspeed')
          //  });
          //  $("#parked").click(function() {
                $("#parked").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=parked')
          //  });
         //   $("#idle").click(function() {
                $("#idle").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=idle')
          //  });
         //   $("#event").click(function() {
                $("#event").attr("href", 'event?vid='+vid+'&vg='+gname+'&tn=event')
         //   });
         //   $("#site").click(function() {
                $("#site").attr("href", 'siteReport?vid='+vid+'&vg='+gname+'&tn=site')
         //   }); 
          //  $("#multiSite").click(function() {
                $("#multiSite").attr("href", 'multiSite?vid='+vid+'&vg='+gname+'&tn=multiSite')
          //  });
          //  $("#tripSite").click(function() {
                $("#tripSite").attr("href", 'tripSite?vid='+vid+'&vg='+gname+'&tn=tripSite')
          //  });
          //  $("#load").click(function() {
                $("#load").attr("href", 'loadDetails?vid='+vid+'&vg='+gname+'&tn=load')
          //  });
          //  $("#fuel").click(function() {
                $("#fuel").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=fuel')
          //  });
             // $("#fuelFill").click(function() {
                // $("#fuelFill").attr("href", 'fuel?vid='+vid+'&vg='+gname+'&tab=fuelfill')
             // });
          //  $("#fuelTheft").click(function() {
                // $("#fuelTheft").attr("href", 'fuelTheft?vid='+vid+'&vg='+gname+'&tn=fuelTheft')
         //   });
          //  $("#fuelProtocol").click(function() {
                $("#fuelFillV2").attr("href", 'fuelFillV2?vid='+vid+'&vg='+gname+'&tn=fuelFillV2')
                $("#fuelProtocol").attr("href", 'fuelProtocol?vid='+vid+'&vg='+gname+'&tn=fuelProtocol')
         //   });
         //  $("#fuelProtocol").click(function() {
                $("#fuelMachine").attr("href", 'fuelMachine?vid='+vid+'&vg='+gname+'&tn=fuelMachine')
         //   });
         //   $("#fuelMileage").click(function() {
                // $("#fuelMileage").attr("href", 'fuelMileage?vid='+vid+'&vg='+gname+'&tn=fuelMileage')
                // $("#fuelMileage").attr("href", 'fuelMileageNew?vid='+vid+'&vg='+gname+'&tn=fuelMileage')
         //   });
         //   $("#fuelConReport").click(function() {
                $("#fuelConReport").attr("href", 'fuelConReport?vid='+vid+'&vg='+gname+'&tn=fuelConReport')
         //   });
         //   $("#fuelRaw").click(function() {
                $("#fuelRaw").attr("href", 'fuelRaw?vid='+vid+'&vg='+gname+'&tn=fuelRaw')
         //   }); 
         //   $("#fuelConVehicle").click(function() {
                $("#fuelConVehicle").attr("href", 'fuelConsolidVehicle?vid='+vid+'&vg='+gname+'&tn=fuelConVehicle')
         //   });
         //   $("#fuelConVehicle").click(function() {
                $("#fuelConTime").attr("href", 'fuelConsolidTime?vid='+vid+'&vg='+gname+'&tn=fuelConTime')
         //   });

         //   $("#fuelConVehicle").click(function() {
               $("#fuelPerfor").attr("href", 'monthlyfuelperformance?vid='+vid+'&vg='+gname+'&tn=fuelPerfor')
         //   });


         //   $("#conAlarmReport").click(function() {
                $("#conAlarmReport").attr("href", 'conAlarmReport?vid='+vid+'&vg='+gname+'&tn=conAlarmReport')
         //   });
         //   $("#conRfidReport").click(function() {
                $("#conRfidReport").attr("href", 'conRfidReport?vid='+vid+'&vg='+gname+'&tn=conRfidReport')
         //   });
        //    $("#conPriEngReport").click(function() {
                $("#conPriEngReport").attr("href", 'conPriEngReport?vid='+vid+'&vg='+gname+'&tn=conPriEngReport')
        //    });
         //   $("#fuelNew").click(function() {
                // $("#fuelNew").attr("href", 'fuelNew?vid='+vid+'&vg='+gname+'&tn=fuelNew')
         //   });
         //   $("#ignition").click(function() {
                $("#ignition").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=ignition')
         //   });
         //   $('#trip').click(function() {
                $('#trip').attr("href", 'trip?vid='+vid+'&vg='+gname+'&tn=trip')
         //   });
         //   $('#tripkms').click(function() {
                // console.log(' clck ')
                $('#tripkms').attr("href", 'track?vid='+vid+'&vg='+gname+'&tn=tripkms&maps=tripkms')
         //   });
         //   $('#temp').click(function() {
                // console.log(' clck ')
                $('#temp').attr("href", 'temperature?vid='+vid+'&vg='+gname+'&tn=temperature')
          //  });
          //  $('#tempdev').click(function() {
                // console.log(' clck ')
                $('#tempdev').attr("href", 'temperature?vid='+vid+'&vg='+gname+'&tn=temperatureDev')
         //   });
         //   $('#alarm').click(function(){
                $('#alarm').attr("href", 'alarm?vid='+vid+'&vg='+gname+'&tn=alarm')
          //  })

        /*  $('#ac').click(function(){
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=acreport')
            }) */

         //   $('#stoppage').click(function(){
                $('#stoppage').attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=stoppageReport')
         //   })

        //    $('#rfid').click(function(){
                $('#rfid').attr("href", 'rfidTag?vid='+vid+'&vg='+gname+'&tn=rfid')
         //   })

         //   $('#rfidNew').click(function(){
                $('#rfidNew').attr("href", 'rfidTagNew?vid='+vid+'&vg='+gname+'&tn=rfidNew')
         //   })

         //   $('#ac').click(function(){
                $('#ac').attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=ac')
         //   })

         //    $('#idleWaste').click(function(){
                $('#idleWaste').attr("href", 'idleWaste?vid='+vid+'&vg='+gname+'&tn=ac')
         //   })

         //   $('#engineOn').click(function(){
                $('#engineOn').attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=engine')
                $('#doorSensor').attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=drSensor')
         //   })

         //   $('#prmEngineOn').click(function(){
                $('#prmEngineOn').attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=prmEngine')
         //   })
            
         //   $('#camera').click(function(){
                $('#camera').attr("href", 'camera?vid='+vid+'&vg='+gname+'&tn=camera')
          //  })

         //   $('#stopReport').click(function(){
                $('#stopReport').attr("href", 'stopReport?vid='+vid+'&vg='+gname+'&tn=stopReport')
        //    })

         //   $('#ConSiteLocReport').click(function(){
                $('#ConSiteLocReport').attr("href", 'ConSiteLocReport?vid='+vid+'&vg='+gname+'&tn=ConSiteLocReport')
         //   })

          //  $('#tollReport').click(function(){
                $('#tollReport').attr("href", 'tollReport?vid='+vid+'&vg='+gname+'&tn=tollReport')
          //  })

         //   $('#nonMovingReport').click(function(){
                $('#nonMovingReport').attr("href", 'nonMovingReport?tn=nonMovingReport')
         //   })

          //  $('#travelSumReport').click(function(){
                $('#travelSumReport').attr("href", 'travelSummary?vid='+vid+'&vg='+gname+'&tn=travelSumReport')
         //   })

          //  $('#empAtnReport').click(function(){
                $('#empAtnReport').attr("href", 'empAtnReport?vid='+vid+'&vg='+gname+'&tn=empAtnReport')
         //   })

          //  $('#SchoolSmsReport').click(function(){
                $('#SchoolSmsReport').attr("href", 'SchoolSmsReport?vid='+vid+'&vg='+gname+'&tn=SchoolSmsReport')
         //   })
            
         //   $('#siteAlertReport').click(function(){
                $('#siteAlertReport').attr("href", 'siteAlertReport?vid='+vid+'&vg='+gname+'&tn=siteAlertReport')
          //  })

          //  $('#multiTrack').click(function(){
                $('#multiTrack').attr("href", 'track?vehicleId='+vid+'&vg='+gname+'&track=multiTrack&maps=mulitple');
                $('#multiTrack').attr("target", '_blank');

                $('#multiTrackNew').attr("href", 'track?vehicleId='+vid+'&vg='+gname+'&track=multiTrack&maps=mulitpleNew');
                $('#multiTrackNew').attr("target", '_blank');
         //   });
         //   $('#singleTrack').click(function(){
                $('#singleTrack').attr("href", 'track?vehicleId='+vid+'&track=single&maps=single');
                $('#singleTrack').attr("target", '_blank');
         //   });

         //   $('#history').click(function () {
                $('#history').attr("href", "track?vehicleId="+vid+'&gid='+gname+'&maps=replay');
         //   })



    $.get('//'+globalIP+contextMenu+'/public/isAssetUser', function(response) {
                var menuVal  = response.trim();
                    labelVal = menuVal;
                if(menuVal=="true"){
                   document.getElementById('assetLabel').innerHTML = 'Assets Consolidated';
                     //document.getElementById('singleTrackRls').innerHTML   = '';
                       document.getElementById('sensMenu').innerHTML = '';
                }
                //localStorage.setItem('isAssetUser', JSON.stringify(menuVal));
            }).error(function(){
                    console.log('error in isAssetUser...');
    });


     
 $.get('//'+globalIP+contextMenu+'/public/getReportsRestrictForMobile', function(res) {
                    if(document.getElementById("search_box")){
                        document.getElementById("search_box").value="";
                      }
                       var Restrictmenu=JSON.parse(res);
                       var Restrict_FUEL=Restrictmenu.FUEL;
                       var Restrict_RFID=Restrictmenu.RFID;
                       var Restrict_TEMPERATURE=Restrictmenu.TEMPERATURE;
                       if(Restrict_FUEL==false){  
                                      document.getElementById('fuelPerformanceRl').innerHTML = '';
                                  }
                       //alert(Restrict_FUEL);
    $.get('//'+globalIP+contextMenu+'/public/getReportsList', function(response) {
         if(document.getElementById("search_box")){
            document.getElementById("search_box").value="";
          }
        if(response){
              var menuObj=JSON.parse(response);
               //console.log(menuObj);

           if( menuObj != "" && menuObj!=null ){ 

             for(var i=0;i<menuObj.length;i++){

            // console.log( Object.getOwnPropertyNames(menuObj[i]).sort() );
                  var newReportName = Object.getOwnPropertyNames(menuObj[i]).sort();

                if(newReportName == "Analytics_IndivdiualReportsList"){

                  //  console.log( "Analytics_Reports");
                  //  console.log(menuObj[i].Analytics_IndivdiualReportsList.length);
               

                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Analytics_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);

                        
                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'ALARM':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].ALARM==false){ document.getElementById('alarmRl').innerHTML ='';}
                                break;
                                case 'IDLE':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].IDLE==false){   document.getElementById('idleRl').innerHTML = '';}
                                break;
                                case 'MOVEMENT':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].MOVEMENT==false){   document.getElementById('movementRl').innerHTML = ''; document.getElementById('noDataRl').innerHTML = ''; }
                                break;
                                case 'OVERSPEED':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].OVERSPEED==false){   document.getElementById('overspeedRl').innerHTML = '';}
                                break;
                                case 'PARKED':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].PARKED==false){   document.getElementById('parkedRl').innerHTML = '';}
                                break;
                                case 'TRIP_TIME':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].TRIP_TIME==false){   document.getElementById('tripRl').innerHTML = '';}
                                break;
                                case 'TRIP_SUMMARY':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].TRIP_SUMMARY==false){   document.getElementById('tripkmsRl').innerHTML = '';}
                                break;
                            /*  case 'SITE':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].SITE==false){   document.getElementById('siteRl').innerHTML = '';}
                                break; */
                                case 'SITE_TRIP':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].SITE_TRIP==false){   document.getElementById('tripSiteRl').innerHTML = '';}
                                break;
                             /* case 'EVENT':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].EVENT==false){   document.getElementById('eventRl').innerHTML = '';}
                                break; */
                                case 'STOPPAGE':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].STOPPAGE==false){   document.getElementById('stoppageRl').innerHTML = '';}
                                break;
                                case 'ROUTE_DEVIATION':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].ROUTE_DEVIATION==false){   document.getElementById('routeDeviationRl').innerHTML = '';}
                                break;
                            /*  case 'MULTIPLE_SITE':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].MULTIPLE_SITE==false){   document.getElementById('multiSiteRl').innerHTML = '';}
                                break; */
                                case 'FUEL':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].FUEL==false||Restrict_FUEL==false){  
                                    // document.getElementById('fuelRl').innerHTML = '';
                                      document.getElementById('fuelMachineRl').innerHTML = '';
                                      //document.getElementById('fuelFillV2Rl').innerHTML = '';

                                    // if(labelVal=="false") {
                                    //     document.getElementById('fuelNewRl').innerHTML = '';
                                    // }

                                    }
                                break;
                                case 'IGNITION':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].IGNITION==false){  document.getElementById('ignitionRl').innerHTML = '';document.getElementById('prmEngineOnRl').innerHTML ='';}
                                break;
                            }
 
                        }

                } else if(newReportName == "Consolidatedvehicles_IndivdiualReportsList"){

                   // console.log( "Consolidatedvehicles_Reports");
                   // console.log(menuObj[i].Consolidatedvehicles_IndivdiualReportsList.length);
               
                     var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0]).sort();
                    // console.log(newReportNamesss.length);

                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'CONSOLIDATED_SITE_LOCATION':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATED_SITE_LOCATION==false ){ document.getElementById('conSiteRl').innerHTML ='';}
                                break;
                                case 'SCHOOL_SMS_REPORT':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].SCHOOL_SMS_REPORT==false ){ document.getElementById('SchoolSmsReportRl').innerHTML = '';}
                                break;
                                case 'CONSOLIDATED_STOPPAGE':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATED_STOPPAGE==false ){ document.getElementById('stopReportRl').innerHTML = '';}
                                break;
                                case 'SITESTOPPAGE_ALERT':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].SITESTOPPAGE_ALERT==false ){ document.getElementById('siteAlertReportRl').innerHTML = '';}
                                break;
                                case 'CURRENT_STATUS':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CURRENT_STATUS==false ){ document.getElementById('curStatRl').innerHTML = '';}
                                break;
                                case 'TOLLGATE_REPORT':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].TOLLGATE_REPORT==false ){ document.getElementById('tollReportRl').innerHTML = '';}
                                break;
                                case 'NON_MOVING_REPORT':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].NON_MOVING_REPORT==false ){ document.getElementById('nonMovingReportRl').innerHTML = '';}
                                break; 
                                case 'TRAVEL_SUMMARY_REPORT':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].TRAVEL_SUMMARY_REPORT==false ){ document.getElementById('travelSumReportRl').innerHTML = '';}
                                break; 
                                case 'FUEL_CONSOLIDATE_REPORT':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].FUEL_CONSOLIDATE_REPORT==false||Restrict_FUEL==false ){ 
                                    document.getElementById('fuelConReportRl').innerHTML = ''; 
                                if (document.getElementById('fuelConVehiRl') !=null)
                                   document.getElementById('fuelConVehiRl').innerHTML = '';
                               if (document.getElementById('fuelConTimeRl') !=null)
                                   document.getElementById('fuelConTimeRl').innerHTML = '';
                              }
                                break; 
                                case 'PRIMARY_ENGINE_CONSOLIDATE_REPORT':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].PRIMARY_ENGINE_CONSOLIDATE_REPORT==false ){ document.getElementById('conPriEngReportRl').innerHTML = '';}
                                break; 
                                case 'EMPATTN_REPORT':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].EMPATTN_REPORT==false||Restrict_RFID==false){ document.getElementById('empAtnReportRl').innerHTML = '';}
                                break;
                                case 'CONSOLIDATED_OVERSPEED':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATED_OVERSPEED==false ){ document.getElementById('conOvrRl').innerHTML = '';}
                                break;
                                case 'VEHICLES_CONSOLIDATED':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].VEHICLES_CONSOLIDATED==false ){ document.getElementById('vehConsRl').innerHTML = '';}
                                break;
                                case 'CONSOLIDATED_SITE':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATED_SITE==false ){ document.getElementById('conSiteLocRl').innerHTML = '';}
                                break;
                                case 'CONSOLIDATE_ALARM_REPORT':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATE_ALARM_REPORT==false ){ document.getElementById('conAlarmReportRl').innerHTML = '';}
                                break; 
                                case 'CONSOLIDATE_RFID_REPORT':
                                  if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATE_RFID_REPORT==false ||Restrict_RFID==false){ document.getElementById('conRfidReportRl').innerHTML = '';}
                                break;
                            }
                         }

                } else if(newReportName == "Performance_IndivdiualReportsList"){

                  //  console.log( "Performance_Reports");
                   // console.log(menuObj[i].Performance_IndivdiualReportsList.length);
               
                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Performance_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);

                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'DAILY_PERFORMANCE':
                                  if( menuObj[i].Performance_IndivdiualReportsList[0].DAILY_PERFORMANCE==false){ document.getElementById('dailyPerfRl').innerHTML ='';}
                                break;
                                case 'MONTHLY_PERFORMANCE':
                                  if( menuObj[i].Performance_IndivdiualReportsList[0].MONTHLY_PERFORMANCE==false){   document.getElementById('monPerfRl').innerHTML = '';}
                                break;
                            }
                         }

                } else if(newReportName == "Sensor_IndivdiualReportsList") {

                  if(labelVal!="true"){

                // console.log( "Sensor_Reports");
                   // console.log(menuObj[i].Sensor_IndivdiualReportsList.length);
               
                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Sensor_IndivdiualReportsList[0]).sort();
                      // console.log(newReportNamesss.length);

                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'AC':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].AC==false){ 
                                    document.getElementById('acRl').innerHTML ='';
                                    document.getElementById('engineOnRl').innerHTML ='';
                                    document.getElementById('doorSensorRl').innerHTML ='';
                                  }
                                case 'DOOR_SENSOR':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].DOOR_SENSOR==false){ 
                                    document.getElementById('doorSensorRl').innerHTML ='';
                                  }
                                break;
                             /* case 'DISTANCE_TIME':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].DISTANCE_TIME==false){ document.getElementById('distTimeRl').innerHTML ='';}
                                break; */
                                case 'TEMPERATURE_DEVIATION':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].TEMPERATURE_DEVIATION==false||Restrict_TEMPERATURE==false){  document.getElementById('tempdevRl').innerHTML = '';}
                                break;
                                case 'TEMPERATURE':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].TEMPERATURE==false||Restrict_TEMPERATURE==false){  document.getElementById('tempRl').innerHTML = '';}
                                break;
                             /* case 'LOAD':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].LOAD==false){  document.getElementById('loadRl').innerHTML = '';}
                                break; 
                                case 'FUEL_NEW':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_NEW==false){  document.getElementById('fuelNewRl').innerHTML = '';}
                                break; */
                                // case 'FUEL_FILL':
                                //   if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_FILL==false){  document.getElementById('fuelFillRl').innerHTML = '';}
                                // break;
                                // case 'FUEL_MACHINE':
                                //   if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_MACHINE==false){  document.getElementById('fuelMachineRl').innerHTML = '';}
                                // break;
                                case 'FUEL_RAW':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_RAW==false||Restrict_FUEL==false){  document.getElementById('fuelRawRl').innerHTML = '';}
                                break;
                                // case 'FUEL_THEFT':
                                //   if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_THEFT==false){  document.getElementById('fuelTheftRl').innerHTML = '';}
                                // break;
                                case 'FUEL_PROTOCOL':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_PROTOCOL==false){  document.getElementById('fuelProtocolRl').innerHTML = '';
                                  localStorage.setItem('hideFuelProtocol', "true"); 
                              }
                                break;
                              //   case 'FUEL_MILEAGE':
                              //     if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_MILEAGE==false){  document.getElementById('fuelMileageRl').innerHTML = '';
                              //     document.getElementById('fuelMileageNewRl').innerHTML = '';
                              // }
                                  
                                break;
                                case 'RFID':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].RFID==false||Restrict_RFID==false){  
                                    document.getElementById('rfidRl').innerHTML = ''; 
                                    document.getElementById('rfidNewRl').innerHTML = '';}
                                break;
                                case 'CAMERA':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].CAMERA==false){  document.getElementById('cameraRl').innerHTML = '';}
                                break;
                            }
                         } 
                    }
                        
                } else if(newReportName == "Statistics_IndivdiualReportsList"){

                   // console.log( "Statistics_Reports");
                   // console.log(menuObj[i].Statistics_IndivdiualReportsList.length);
               
                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Statistics_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);

                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'EXECUTIVE_FUEL':
                                  if( menuObj[i].Statistics_IndivdiualReportsList[0].EXECUTIVE_FUEL==false||Restrict_FUEL==false){ document.getElementById('exfuelRl').innerHTML ='';}
                                break;
                                case 'MONTHLY_DIST_AND_FUEL':
                                  if( menuObj[i].Statistics_IndivdiualReportsList[0].MONTHLY_DIST_AND_FUEL==false||Restrict_FUEL==false){  document.getElementById('monDistFuelRl').innerHTML = '';}
                                break;
                                case 'DAILY':
                                  if( menuObj[i].Statistics_IndivdiualReportsList[0].DAILY==false){  document.getElementById('dailyRl').innerHTML = '';}
                                break;
                                case 'MONTHLY_DIST':
                                  if( menuObj[i].Statistics_IndivdiualReportsList[0].MONTHLY_DIST==false){  document.getElementById('monDistRl').innerHTML = '';}
                                break;
                                case 'POI':
                                  if( menuObj[i].Statistics_IndivdiualReportsList[0].POI==false){  document.getElementById('poiRl').innerHTML = '';}
                                break;
                                case 'CONSOLIDATED':
                                  if( menuObj[i].Statistics_IndivdiualReportsList[0].CONSOLIDATED==false){  document.getElementById('consolRl').innerHTML = '';}
                                break;
                            }
 
                        }

                } else if(newReportName == "Tracking_IndivdiualReportsList"){

                   // console.log( "Tracking_Reports");
                   // console.log(menuObj[i].Tracking_IndivdiualReportsList.length);
               
                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Tracking_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);

                        
                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'SINGLE_TRACK':
                                  if( menuObj[i].Tracking_IndivdiualReportsList[0].SINGLE_TRACK==false){ 
                                    document.getElementById('singleTrackRl').innerHTML ='';
                                    //document.getElementById('singleTrackRls').innerHTML ='';
                                  }
                                break;
                                case 'ROUTES':
                                  if( menuObj[i].Tracking_IndivdiualReportsList[0].ROUTES==false){ document.getElementById('routesRl').innerHTML = '';}
                                break;
                                case 'ADDSITES_GEOFENCE':
                                  if( menuObj[i].Tracking_IndivdiualReportsList[0].ADDSITES_GEOFENCE==false){ document.getElementById('addsiteRl').innerHTML = '';}
                                break;
                                case 'MULTITRACK':
                                  if( menuObj[i].Tracking_IndivdiualReportsList[0].MULTITRACK==false){ document.getElementById('multiTrackRl').innerHTML = '';}
                                break;
                                case 'HISTORY':
                                  if( menuObj[i].Tracking_IndivdiualReportsList[0].HISTORY==false){ document.getElementById('historyRl').innerHTML = '';}
                                break;
                            }
                         }

                } else if(newReportName == "Useradmin_IndivdiualReportsList"){

                   // console.log( "Useradmin_Reports");
                   // console.log(menuObj[i].Useradmin_IndivdiualReportsList.length);
               
                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Useradmin_IndivdiualReportsList[0]).sort();
                    //   console.log(newReportNamesss.length);

                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'EDIT_NOTIFICATION':
                                  if( menuObj[i].Useradmin_IndivdiualReportsList[0].EDIT_NOTIFICATION==false){ document.getElementById('editNotRl').innerHTML ='';document.getElementById('editUsNotRl').innerHTML =''}
                                break;
                                case 'PAYMENT_REPORTS':
                                  if( menuObj[i].Useradmin_IndivdiualReportsList[0].PAYMENT_REPORTS==false){  document.getElementById('payDetRl').innerHTML = '';}
                                break;
                                case 'GET_APIKEY':
                                  if( menuObj[i].Useradmin_IndivdiualReportsList[0].GET_APIKEY==false){  document.getElementById('getApiRl').innerHTML = '';}
                                break;
                                case 'EDIT_BUSSTOP':
                                  if( menuObj[i].Useradmin_IndivdiualReportsList[0].EDIT_BUSSTOP==false){  document.getElementById('editBusRl').innerHTML = '';}
                                break;
                                case 'RESET_PASSWORD':
                                  if( menuObj[i].Useradmin_IndivdiualReportsList[0].RESET_PASSWORD==false){  document.getElementById('resetRl').innerHTML = '';}
                                break;
                                case 'EDIT_GROUP':
                                  if( menuObj[i].Useradmin_IndivdiualReportsList[0].EDIT_GROUP==false){  document.getElementById('editGrpRl').innerHTML = '';}
                                break;
                            }
                        }

                } else if(newReportName == "TRACK_PAGE"){

                     if( menuObj[i].TRACK_PAGE[0].Normal_View==true ) {

                        localStorage.setItem('trackNovateView', false);

                         
                     } else {

                        localStorage.setItem('trackNovateView', true); 

                     }
           
                   
                }
             }
             
        } else{
            console.log('Empty getReportsList API ....');
        }
   }

    }).error(function(){

        console.log('error getReportsList');
     });

    }).error(function(){

            console.log('error getReportsRestrictForMobile');
     });
   



           
// // 
           // added for global datetime for all reports - start
            $("#dateFrom,#ovrFrom,#tripDatefrom").on("change paste keyup", function() {
              //alert('HELLO');
                 localStorage.setItem('fromDate', $(this).val());
              });
             $("#timeFrom,#ovrTimeFrom,#tripTimeFrom").on("change paste keyup", function() {
                 localStorage.setItem('fromTime', $(this).val());
              });
             $("#dateTo,#ovrTo,#tripDateTo").on("change paste keyup", function() {
                 localStorage.setItem('toDate', $(this).val());
              });
             $("#timeTo,#ovrTimeTo,#tripTimeTo").on("change paste keyup", function() {
                 localStorage.setItem('timeTochange', 'yes');
                 localStorage.setItem('toTime', $(this).val());
              });
            
        //global datetime end

           $("#movement").click(function() {
                window.localStorage.removeItem("reload");
               $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=movement' );
            });

            $("#noData").click(function() {
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=noData' );
            });
            $("#routeDev").click(function() {
                $(this).attr("href", 'routeDeviation?vid='+vid+'&vg='+gname+'&tn=routeDeviation' );
            });
            $("#overspeed").click(function() {
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=overspeed')
            });
            $("#parked").click(function() {
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=parked')
            });
            $("#idle").click(function() {
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=idle')
            });
            $("#event").click(function() {
                $(this).attr("href", 'event?vid='+vid+'&vg='+gname+'&tn=event')
            });
            $("#site").click(function() {
                $(this).attr("href", 'siteReport?vid='+vid+'&vg='+gname+'&tn=site')
            }); 
            $("#multiSite").click(function() {
                $(this).attr("href", 'multiSite?vid='+vid+'&vg='+gname+'&tn=multiSite')
            });
            $("#tripSite").click(function() {
                $(this).attr("href", 'tripSite?vid='+vid+'&vg='+gname+'&tn=tripSite')
            });
            $("#load").click(function() {
                $(this).attr("href", 'loadDetails?vid='+vid+'&vg='+gname+'&tn=load')
            });
            // $("#fuel").click(function() {
            //     $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=fuel')
            // });
            // $("#fuelFill").click(function() {
            //     //alert(vid);
            //     $(this).attr("href", 'fuel?vid='+vid+'&vg='+gname+'&tab=fuelfill')
            // });
            // $("#fuelTheft").click(function() {
            //     $(this).attr("href", 'fuelTheft?vid='+vid+'&vg='+gname+'&tn=fuelTheft')
            // });
            $("#fuelFillV2").click(function() {
                $(this).attr("href", 'fuelFillV2?vid='+vid+'&vg='+gname+'&tn=fuelFillV2')
            });
            $("#fuelProtocol").click(function() {
                $(this).attr("href", 'fuelProtocol?vid='+vid+'&vg='+gname+'&tn=fuelProtocol')
            });
            // $("#fuelMileage").click(function() {
            //     $(this).attr("href", 'fuelMileage?vid='+vid+'&vg='+gname+'&tn=fuelMileage')
            // });
            // $("#fuelMileageNew").click(function() {
            //     $(this).attr("href", 'fuelMileageNew?vid='+vid+'&vg='+gname+'&tn=fuelMileage')
            // });
            $("#fuelConReport").click(function() {
                $(this).attr("href", 'fuelConReport?vid='+vid+'&vg='+gname+'&tn=fuelConReport')
            });
            $("#fuelRaw").click(function() {
                $(this).attr("href", 'fuelRaw?vid='+vid+'&vg='+gname+'&tn=fuelRaw')
            });            
            $("#fuelMachine").click(function() {
                $(this).attr("href", 'fuelMachine?vid='+vid+'&vg='+gname+'&tn=fuelMachine')
            }); 
            $("#fuelConVehicle").click(function() {
                $(this).attr("href", 'fuelConsolidVehicle?vid='+vid+'&vg='+gname+'&tn=fuelConVehicle')
            });
            $("#fuelConTime").click(function() {
                $(this).attr("href", 'fuelConsolidTime?vid='+vid+'&vg='+gname+'&tn=fuelConTime')
            });
             $("#fuelPerfor").click(function() {
                $(this).attr("href", 'monthlyfuelperformance?vid='+vid+'&vg='+gname+'&tn=fuelPerfor')
            });
            $("#conAlarmReport").click(function() {
                $(this).attr("href", 'conAlarmReport?vid='+vid+'&vg='+gname+'&tn=conAlarmReport')
            });
            $("#conRfidReport").click(function() {
                $(this).attr("href", 'conRfidReport?vid='+vid+'&vg='+gname+'&tn=conRfidReport')
            });
            $("#conPriEngReport").click(function() {
                $(this).attr("href", 'conPriEngReport?vid='+vid+'&vg='+gname+'&tn=conPriEngReport')
            });
            // $("#fuelNew").click(function() {
            //     $(this).attr("href", 'fuelNew?vid='+vid+'&vg='+gname+'&tn=fuelNew')
            // });
            $("#ignition").click(function() {
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=ignition')
            });
            $('#trip').click(function() {
                $(this).attr("href", 'trip?vid='+vid+'&vg='+gname+'&tn=trip')
            });
            $('#tripkms').click(function() {
                // console.log(' clck ')
                $(this).attr("href", 'track?vid='+vid+'&vg='+gname+'&tn=tripkms&maps=tripkms')
            });
            $('#temp').click(function() {
                // console.log(' clck ')
                $(this).attr("href", 'temperature?vid='+vid+'&vg='+gname+'&tn=temperature')
            });
            $('#tempdev').click(function() {
                // console.log(' clck ')
                $(this).attr("href", 'temperature?vid='+vid+'&vg='+gname+'&tn=temperatureDev')
            });
            $('#alarm').click(function(){
                $(this).attr("href", 'alarm?vid='+vid+'&vg='+gname+'&tn=alarm')
            })

        /*  $('#ac').click(function(){
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=acreport')
            }) */

            $('#stoppage').click(function(){
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=stoppageReport')
            })

            $('#rfid').click(function(){
                $(this).attr("href", 'rfidTag?vid='+vid+'&vg='+gname+'&tn=rfid')
            })

            $('#rfidNew').click(function(){
                $(this).attr("href", 'rfidTagNew?vid='+vid+'&vg='+gname+'&tn=rfidNew')
            })

            $('#ac').click(function(){
                $(this).attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=ac')
            })

             $('#idleWaste').click(function(){
                $(this).attr("href", 'idleWaste?vid='+vid+'&vg='+gname+'&tn=ac')
            })

            $('#engineOn').click(function(){
                $(this).attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=engine')
            })
            $('#doorSensor').click(function(){
                $(this).attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=drSensor')
            })

            $('#prmEngineOn').click(function(){
                $(this).attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=prmEngine')
            })
            
            $('#camera').click(function(){
                $(this).attr("href", 'camera?vid='+vid+'&vg='+gname+'&tn=camera')
            })

            $('#stopReport').click(function(){
                $(this).attr("href", 'stopReport?vid='+vid+'&vg='+gname+'&tn=stopReport')
            })

            $('#ConSiteLocReport').click(function(){
                $(this).attr("href", 'ConSiteLocReport?vid='+vid+'&vg='+gname+'&tn=ConSiteLocReport')
            })

            $('#tollReport').click(function(){
                $(this).attr("href", 'tollReport?vid='+vid+'&vg='+gname+'&tn=tollReport')
            })

            $('#nonMovingReport').click(function(){
                $(this).attr("href", 'nonMovingReport?tn=nonMovingReport')
            })

            $('#travelSumReport').click(function(){
                $(this).attr("href", 'travelSummary?vid='+vid+'&vg='+gname+'&tn=travelSumReport')
            })

            $('#empAtnReport').click(function(){
                $(this).attr("href", 'empAtnReport?vid='+vid+'&vg='+gname+'&tn=empAtnReport')
            })

            $('#SchoolSmsReport').click(function(){
                $(this).attr("href", 'SchoolSmsReport?vid='+vid+'&vg='+gname+'&tn=SchoolSmsReport')
            })
            
            $('#siteAlertReport').click(function(){
                $(this).attr("href", 'siteAlertReport?vid='+vid+'&vg='+gname+'&tn=siteAlertReport')
            })

            $('#multiTrack').click(function(){
                $(this).attr("href", 'track?vehicleId='+vid+'&vg='+gname+'&track=multiTrack&maps=mulitple');
                $(this).attr("target", '_blank');
            });

            $('#multiTrackNew').click(function(){
                $(this).attr("href", 'track?vehicleId='+vid+'&vg='+gname+'&track=multiTrack&maps=mulitpleNew');
                $(this).attr("target", '_blank');
            });
            
            $('#singleTrack').click(function(){
                $(this).attr("href", 'track?vehicleId='+vid+'&track=single&maps=single');
                $(this).attr("target", '_blank');
            });

            $('#history').click(function () {
                $(this).attr("href", "track?vehicleId="+vid+'&gid='+gname+'&maps=replay');
            })

        /*  $('#vehiView').click(function () {
                $(this).attr("href", "track?maps=viewVehicles&userId="+sp1[1]+'&group='+gname);
            }) */

        /*    $('#landNew').click(function () {
                $(this).attr("href", "track?maps=landNew");
            }) */

          /*   $('#dashNew').click(function () {
                $(this).attr("href", "dashNew");
            }) */

            //auto logout 

            /*   
             function  logout() {      
              window.location ="./public/login";
             }

            setTimeout(function(){ logout() }, 3600000);  //logout after one hour

        
        /*  setInterval(function(){ 
                $.get( "http://localhost/vamo/public/getVehicleHistory?vehicleId=TN66M0451&fromDate=2017-09-04&fromTime=16:17:20&fromDateUTC=1504522040000&userId=MSS", function( data ) {
                      $( ".result" ).html( data );
                      console.log(JSON.parse(data));
                });
             }, 3000); */


             //on click menu function to refresh START
/*                $('.funMe').click(function(){

                    $("#wrapper").click(function() {
                        window.location='history?vid='+vid+'&vg='+gname+'&tn=movement';
                    });        
                })

           // on click menu function to refresh END


           // code for onload div selection START
  
                function callSelectedDiv(){
                    {        
                         $('#wrapper').click(function()
                         {
                            $("#wrapper").click(function()
                            {
                                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=movement' );
                                window.localStorage.setItem("reload",1);
                            });
                                
                            window.location="#"+vid;
                           });

                           $('#movement').trigger('click'); // trigger auto click
                           window.localStorage.setItem("reload",1);
                          }
                        }

            // code for onload div selection END


            // calling SelectedDiv Function START                

             $(document).ready(function(){
                
                callSelectedDiv();
             });

*/          //calling SelectedDiv Function END


            $('#addsites').click(function(){
                if(localStorage.getItem('auth') == 'sitesVal')
                    $(this).attr("href", 'track?maps=sites')
                else
                    $(this).attr("href", 'password_check') 
            });
           

        //    var isVir = JSON.parse(localStorage.getItem('isVirtualUser'));
        //    if(isVir == null || isVir == undefined){
              

            $.get('//'+globalIP+contextMenu+'/public/isVirtualUser', function(response) {
                
                var menuVal =response.trim();
                localStorage.setItem('isVirtualUser', JSON.stringify(menuVal));

                if(menuVal == 'true') {
                    var myList = document.getElementsByClassName('removeli_2');
                    for (var i = 0; myList.length > i ; i++) {
                       myList[i].innerHTML = '';
                    }
                }
                
            }).error(function(){
                    console.log('error in isVirtualUser');
            });


        

          
    // }
    
/*      function getMaxOfArray(numArray) {
           return Math.max.apply(null, numArray);
        }

        function trimDueDays(value){
           var splitValue=value.split(/[ ]+/);
           return  splitValue[2];
        }

        function zohoDayValue(value){
      
           if(value>=7){
            
             var myList = document.getElementsByClassName('removeli');
                for (var i = 0; myList.length > i ; i++) {
                    myList[i].innerHTML = '';
                }
               console.log('7 (or) more than 7 days...');
           }
           else{

            console.log('less than 7 days...');
           }
        }

        function zohoDataCall(data){

            var zohoDayss=[];
   
            angular.forEach(data.hist,function(val, key){
                zohoDayss.push(trimDueDays(val.dueDays));
            })
         //  console.log(getMaxOfArray(zohoDayss));

        zohoDayValue(getMaxOfArray(zohoDayss));
        }
   

        $.get('//'+globalIP+contextMenu+'/public/getZohoInvoice?', function(response) {
             
              var zohoRaw=[];
                  zohoRaw=JSON.parse(response); 

                  zohoDataCall(zohoRaw);
               
        }).error(function(){
                    console.log('Error in Zoho Url(Nav Menu Page)');
     }); */
  $("#English").click(function() {
                localStorage.setItem('lang','en');
                $('#changedlanguage').text('English');
            });

  $("#Hindi").click(function() {

                localStorage.setItem('lang','hi');
                $('#changedlanguage').text('Hindi');
            });

</script>
<style type="text/css">
.menus li ul li{
  width:250px !important;
}
/*.scrollbox:hover {
  max-height: 600px;
  overflow-y: auto;
  overflow-x: hidden;
}*/
</style>