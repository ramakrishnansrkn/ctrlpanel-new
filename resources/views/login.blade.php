<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <style>
body {font-family: Arial, Helvetica, sans-serif;}

/* Full-width input fields */

/* Set a style for all buttons */


/* Extra styles for the cancel button */
.cancelbtn {
    width: 100%;
    padding: 10px 18px;
    background-color: #128282;
    color: #ffffff;
    margin-top: 6%;
    border-radius: 10px;
}

/* Center the image and position the close button */
.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
    position: relative;
}

img.avatar {
        border-radius: 8px;
    max-width: 75%;
    height: auto;
}

.container {
    padding: 21px;
    padding-top: 33px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 60px;
}

/* Modal Content/Box */
.modal-content {
  /*background-image: url(../public/uploads/pucture6.jpg);*/
    background-color: #fefefe;
    margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    border-radius: 12px;
    width: 330px;
    max-height: 72%;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
    position: absolute;
    right: 19px;
    top: -20px;
    color: #ab9999;
    font-size: 30px;
    /*font-weight: bold;*/
}

.close:hover,
.close:focus {
    color: red;
    cursor: pointer;
}
.textbox:hover {
 /* background-color: red;
    cursor: pointer; */
    box-shadow: none !important;
    outline: none !important;
    border: 1px solid #1da7da !important;
}


/* Add Zoom Animation */
.animate {
    -webkit-animation: animatezoom 0.6s;
    animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
    from {-webkit-transform: scale(0)} 
    to {-webkit-transform: scale(1)}
}
    
@keyframes animatezoom {
    from {transform: scale(0)} 
    to {transform: scale(1)}
}


</style>
  <title>GPS</title>
  
  <link rel="stylesheet" type="text/css" href="assets/css/login.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

</head>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<body class="cont">
<!-- <body class="cont"   onload="callMe()"> -->

  {{ Form::open(array('url' => "login$mobileNo")) }}
    <div class="demo">
      <div class="login" align="center"  id="loginStyle">
      <div class="login__check"> </div>
        <img id="imagesrc" src="{{$webLogo}}" style="border-radius: 8px;max-width: 75%;height: auto;"/>

        <br>
        <br>
        <p class="login__signup" style="font-size: 16px"><a><?php echo Lang::get('content.gps_tracking_system'); ?></a></p>
        <h5>
          <?php if(Session::has('flash_notice')): ?>
            <div class="flashMessage" id="flash_notice"><?php echo Session::get('flash_notice') ?></div>
          <?php endif; ?>
          
            <span id="error" style="color:#ff6666;font-weight:bold;font-size:15px;">{{ HTML::ul($errors->all()) }}</span> 
        </h5>

        <div class="login__form"  id="loginFormStyle">
          
          <div class="login__row">
            <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
              <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
            </svg>
            {{ Form::text('userName', Input::old('userName'), array('placeholder' => Lang::get('content.username'),'class'=>'login__input name', 'id'=>'userIds')) }}
          </div>
          <div class="login__row">
            <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
              <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
            </svg>
            {{ Form::password('password', array('placeholder' =>$mobileNo == ""?Lang::get('content.password'):"OTP",'class'=>'login__input pass')) }}
          </div>

        {{ Form::submit(Lang::get('content.login'), array('class'=>'login__submit', 'id'=>'clickme')) }}
        {{ Form::close() }}
       <form action="{{ url('/changeLang') }}" method="get" id="langform">
              <select name="lang" id="lang" style="margin-top: 0px;">
					<option value="en" <?php echo (Lang::locale() == 'en' )? 'selected' : ' '  ?> >@lang('content.english')</option>
					<option value="ach" <?php echo (Lang::locale() == 'ach' )? 'selected' : ' '  ?> >français</option>
					<option value="pt" <?php echo (Lang::locale() == 'pt') ? 'selected' : ' '  ?> >português'</option>
					<option value="ar" <?php echo (Lang::locale() == 'ar') ? 'selected' : ' '  ?> >عربى</option>
					<option value="hi" <?php echo (Lang::locale() == 'hi') ? 'selected' : ' '  ?> >हिन्दी</option>
             </select>
       </form>
       <a class="login__signup" onclick="document.getElementById('id01').style.display='block'" style="cursor: pointer; width:auto; color: #ffffff;"><?php echo Lang::get('content.Forgot_password'); ?></a>
&nbsp;&nbsp;&nbsp;<a href="/gps/public/apiAcess" style="font-size: 1.2rem;color: #7cc2ca;"><?php echo Lang::get('content.api_access'); ?></a> 
        <span style="padding: 10px" id="icons">
          <div id="cf">
            <img class="bottom" src="/gps/public/assets/imgs/andG.png" style="width: 25px; height: 25px"/>
            <img class="top" src="/gps/public/assets/imgs/andGy.png" style="width: 25px; height: 25px"/>
          </div>

          <div id="cff">
            <img class="bottom" src="/gps/public/assets/imgs/appG.png" style="width: 25px; height: 25px"/>
            <img class="top" src="/gps/public/assets/imgs/appGy.png" style="width: 25px; height: 25px"/>
          </div>
         
      </span>
      <div>
    @if($url == 'yes')
      <a href="/gps/public/faq" style="font-size: 10px; color: #fff" target="blank"><?php echo Lang::get('content.FAQ'); ?></a>
   @endif
   
      <label style="font-size: 10px; color: #fff">
       <input name="remember" type="checkbox" /> <?php echo Lang::get('content.remember_me'); ?>
      </label>
    </div>
        
        </div>
    </div>

  </div>
     
<div id="id01" class="modal">
  
  <form class="modal-content animate" action="password/resetting">
    <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close">&times;</span>
      <img  id="avatarsrc" src="{{$webLogo}}" class="avatar" />
    </div>

    <div class="container">
     <!-- <label for="uname" style="font-size: 20px; font-family: Times, Times New Roman, serif;"><b>Username</b></label> -->
      <input  type="text" id="usern" style="width: 100%; padding: 12px 20px; margin: -5px 0; display: inline-block; border-bottom:1px solid #ccc; box-sizing: border-box; border-radius: 10px;" class="textbox" placeholder="Enter Username" name="uname" required >

     <!-- <label for="psw" style="font-size: 19px;"><b>Password</b></label>
      <input type="password" style="width: 100%; padding: 12px 20px; margin: 8px 0; display: inline-block; border:1px solid #ccc; box-sizing: border-box;" placeholder="Enter Password" name="psw" required> -->
        
      <button class="cancelbtn"><a>Send Password Reset Link</a></button>
      
    </div>
   </form>
</div>


</body>

<script>
                
          localStorage.clear();
          var logo = document.location.host;
           var substring = "www.";
        
         if(logo.indexOf(substring) !== -1){
           var logocsk =logo.replace(substring,'');
           logo=logocsk;
         }
         console.log(logo);
         var select = document.getElementById('lang');
          select.onchange = function(){
            $('#langform').trigger('submit');
              //this.form.submit();
          };
          function ValidateIPaddress(ipaddress) {  
           var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;  
            if(ipaddress.match(ipformat)) {
              return (true)  
            }  
          // alert("You have entered an invalid IP address!")  
          return (false)  
          }  

          if(ValidateIPaddress(logo)) {
             var parser    =   document.createElement('a');
             parser.href   =   document.location.ancestorOrigins[0];
             logo          =   parser.host;
          }

          // (function test(){
          //    console.log("arun")
          // }());
          
          var path       =  document.location.pathname;
          //alert(document.location.hostname);
          var splitpath  =  path.split("/");
          //console.log(' path '+"----"+splitpath[1]);

          //var imgName = '/'+splitpath[1]+'/public/uploads/'+logo+'.png'; 
          //var imgName= '/'+splitpath[1]+'/public/uploads/gpsvts.com.png';
		  var imgName = 'http://206.189.94.37/cpanel/public/uploads/'+logo+'.png';
          var wwwSplit = logo.split(".")
              if(wwwSplit[0]=="www"){
                wwwSplit.shift();
                imgName = '/'+splitpath[1]+'/public/uploads/'+wwwSplit[0]+'.'+wwwSplit[1]+'.png';
              }
           if(document.location.hostname=="cpanel.gpsvts.net"){
                  $('#avatarsrc').attr('src', "");
                  $('#imagesrc').attr('src', "");
                  $("#icons").hide();
                  var loginStyle = document.getElementById('loginStyle');
                  loginStyle.style.height = 90 + "%";
                  loginStyle.style.top = 10 + "%";
                  var loginFormStyle = document.getElementById('loginFormStyle');
                  loginFormStyle.style.top = 30 + "%";
          }
          // $('#imagesrc').attr('src', imgName);
          
        </script>
    
<script type="text/javascript">
   // window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;   //compatibility for firefox and chrome
   //    var pc = new RTCPeerConnection({iceServers:[]}), noop = function(){};      
   //    pc.createDataChannel("");    //create a bogus data channel
   //    pc.createOffer(pc.setLocalDescription.bind(pc), noop);    // create offer and set local description
   //    pc.onicecandidate = function(ice){  //listen for candidate events
   //        if(!ice || !ice.candidate || !ice.candidate.candidate)  return;
   //         var myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];
   //        localStorage.setItem('myIP',myIP);
   //       //alert(myIP);

        
   //        //document.write('IP: ', myIP);   
   //        pc.onicecandidate = noop;
   //    };﻿
   
 var globalIP = document.location.host;
 var contextMenu = '/'+document.location.pathname.split('/')[1];

  // function callMe() {

  //   var isChrome = !!window.chrome && !!window.chrome.webstore;

  //       if(isChrome == false && (navigator.userAgent.indexOf('Chrome') == -1)) {
                
  //         for(i=0;i <= 15; i++) {
  //           alert("Please Open Site on Google Chrome");
  //         }
  //       }
  // }

  $('#clickme').click(function() {
      
      var userId  = $('#userIds').val().toUpperCase();
      var userIP=localStorage.getItem('myIP');
      var postVal = {'id':userId,'userIP':userIP};

    //$.get('http://128.199.159.130:9000/isAssetUser?userId=MSS', function(response) {
      $.get('//'+globalIP+contextMenu+'/public/isAssetUser', function(response) {
          //alert(response);
          localStorage.setItem('isAssetUser', response);
      }).error(function(){
          console.log('error in isAssetUser');
      });
      
     
      //alert(localStorage.getItem('myIP'));
       

       $.post('{{ route("ajax.fcKeyAcess") }}',postVal)
         .done(function(data) {

          localStorage.setItem('fCode',data);
        //alert(data);
        
        }).fail(function() {
            console.log("fcode fail..");
      });

      localStorage.setItem('userIdName', JSON.stringify('username'+","+userId));
      var language=$('#lang').val();
      localStorage.setItem('lang',language);
      var usersID = JSON.stringify(userId);

      if(usersID == '\"BSMOTORS\"' || usersID == '\"TVS\"') {

        window.localStorage.setItem('refreshTime',120000);

      } else {

        window.localStorage.setItem('refreshTime',60000);
      }

  });

  $('#userIds').on('change', function() {
    
    var postValue = {
      'id': $(this).val().toUpperCase()

      };
    // alert($('#groupName').val());
    $.post('{{ route("ajax.apiKeyAcess") }}',postValue)
      .done(function(data) {
        
        // $('#validation').text(data);
            localStorage.setItem('apiKey', JSON.stringify(data));
            
          }).fail(function() {
            console.log("fail");
      });

    $.post('{{ route("ajax.dealerAcess") }}',postValue)
      .done(function(data) {
        
        //alert(data);
          localStorage.setItem('dealerName', data);
            
      }).fail(function() {
          console.log("fail");
    });    

    
  });

// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


</script>
