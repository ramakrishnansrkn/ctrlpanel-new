<?php
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

 /*
 * |-------------------------------------------------------------------------- | Application Routes |-------------------------------------------------------------------------- | | Here is where you can register all of the routes for an application. | It's a breeze. Simply tell Laravel the URIs it should respond to | and give it the Closure to execute when that URI is requested. |
 */
// if(Session::get('phplang')){
// App::setLocale(Session::get('phplang'));
// }
// else{
// App::setLocale('en');
// Session::set('phplang','en');
// }
// log::info(App::getLocale());
// View::addExtension('html', 'php');
Route::get('/track', function() {

    if (!Auth::check()) {

        if($_GET['maps'] == 'track'){
            return View('maps.trackSingleVeh');
        }
        else if($_GET['maps'] == 'viewVehicles'){
            return View('maps.viewVehicles');
        }
        else {
            return Redirect::to('login');
        }
    }
    else
    {
        try {
                if($_GET['maps'] == 'replay'){
                    log::info(' replay');
                    return View('maps.replay');
                    // log::info(' single -->'.$actual_link);
                }
                else if($_GET['maps'] == 'newreplay'){
                    log::info(' replay');
                    return View('maps.newreplay');
                    // log::info(' single -->'.$actual_link);
                }

                else if ($_GET['maps'] == 'sites'){
                    log::info(' site ');
                    return View('reports.siteDetails');
                }
                else if ($_GET['maps'] == 'mulitple'){
                    log::info(' mulitple ');
                    return View('maps.multiTracking');
                }
                else if ($_GET['maps'] == 'mulitplenew'){
                    log::info(' mulitplehomenew ');
                    return View('maps.multiTrackingNew');
                }
                else if($_GET['maps'] == 'replaynew'){
                    log::info(' replaynew Bala');
                    return View('maps.replaynew');
                    // log::info(' single -->'.$actual_link);
                }
                else if ($_GET['maps'] == 'mulitpleNew'){
                    log::info(' mulitpleNew ');
                    return View('maps.multiTrackingNew');
                }
                else if ($_GET['maps'] == 'single'){
                    log::info(' single ');
                    return View('maps.track');
                }
                else if ($_GET['maps'] == 'singlenew'){
                    log::info(' single ');
                    return View('maps.tracknew');
                }
                else if($_GET['maps'] == 'tripkms'){
                    log::info(' tripkms ');
                    return View('reports.tripReportKms');
                }
                else if($_GET['maps'] == 'track'){
                    log::info(' public ');
                    return View('maps.trackSingleVeh');
                }
                else if($_GET['maps'] == 'viewVehicles'){
                    log::info(' public ');
                    return View('maps.viewVehicles');
                }
            /*  else if($_GET['maps'] == 'landNew'){
                    log::info(' public ');
                    return View('maps.landNew');
                } */
                else {
                   log::info('landNew');
                   return View('maps.landNew');
                }
            } catch (Exception $e) {
                log::info(' exception ');
                return View('maps.landNew');
            }
    }

});

Route::get('/apiAcess', function() {
    Log::info(' api acess ');
    return View('maps.api');

});

Route::get('/faq', function() {
    Log::info(' faq ');
    return View('maps.fqa');

});

Route::get('/live', function() {
    return Redirect::to('login');
});

// Route::get('/replay', function(){
//     return View('maps.double');
// });


Route::get('/newUI', function(){
    return View('motorUI.index');
});


Route::get('/ElectionCommisionTrustedClient', function() {
    Log::info( '-------login-----' );
    $user=Input::get('userId');
    Log::info(' users name ' . $user);
    $redis = Redis::connection ();
//ElectionCommisionUser
    if($redis->sismember('ElectionCommisionUser', $user)=='1') {
        $user1=User::where('username', '=', $user)->firstOrFail();
        Log::info(' users name ' . $user1);
        Auth::login($user1);
        return View('maps.landNew');
    }
    else {
        return Redirect::to('login');

    }
});

Route::get('/passWd', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('maps.reset');
});


Route::get('/history', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('maps.history');
});

View::addExtension('html', 'php');
// Route::get('/track', function() {

//     return View('maps.trackSingleVeh');
// });

Route::get('/trackSingleVeh', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $single = strstr($actual_link , 'single');
    $multi = strstr($actual_link , 'multiTrack');

    if ($single)
    {

        return View('maps.track');
        log::info(' single -->'.$actual_link);
    }
    else if ($multi)
    {
        return View('maps.multiTracking');
        log::info(' multi -->'.$actual_link);
    }
});

Route::get('/multiple_vehicle', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('maps.multiTracking');
});

Route::get('/alarm', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    return View('reports.alarmReport');
});

Route::get('/allVehicles', function() {
    $user = User::find(1);
    Auth::login($user);
    return View('maps.landNew');
});
// View::addExtension('html', 'php');
// Route::get('/liveTrack', function() {

//     return View('maps.track');//
// });

Route::get('/settings', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('maps.settings');
});


/*Route::get('/fms', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    return View('maps.fms');
});*/

// Route::get('/fms', function(){

//     if(!Auth::check()) {
//         return Redirect::to('login');
//     }

//     //else {

//         $user = Auth::user()->username;
//         Log::info(' User Name : ' . $user);

//         if($user=="ALY") {
//             return View('reports.fmsReport2');
//         } else {
//             return View('maps.fms');
//         }
//    // }

// });
Route::get('/fleetManagement', function(){

   if(!Auth::check()) {
       return Redirect::to('login');
   }

   //else {

       $user = Auth::user()->username;
       Log::info(' User Name : ' . $user);

       if($user=="ALY") {
           return View('reports.fmsReport2');
       } else {
           return View('reports.fleetManagement');
       }
  // }

});

Route::get('/rfidTag', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.rfidReport');
});

Route::get('/rfidTagNew', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.rfidReportNew');
});

Route::get('/camera', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.cameraReportNew');
});

Route::get('/ac', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.acReportNew');
});

Route::get('/stopReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.stopReport');
});

Route::get('/conAlarmReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.conAlarmReport');
});

Route::get('/conRfidReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.conRfidReport');
});

Route::get('/tollReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.tollReport');
});

Route::get('/travelSummary', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.travelSummary');
});

Route::get('/nonMovingReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.nonMovingReport');
});

Route::get('/idleWaste', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.idleReport');
});

Route::get('/empAtnReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.empAtnReport');
});

Route::get('/ConSiteLocReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.ConSiteLocReport');
});

Route::get('/SchoolSmsReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.SchoolSmsReport');
});

Route::get('/siteAlertReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.siteAlertReport');
});

View::addExtension('html', 'php');
Route::get('/menu', function() {
    // if (!Auth::check()) {
    //     return Redirect::to('login');
    // }
    return View('maps.menu.menu');
});
Route::get('/userPage', function() {
    return View('userPage');;
});

View::addExtension('html', 'php');
Route::get('/reports', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.current');
});

View::addExtension('html', 'php');
Route::get('/dashNew', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.dashNew');
});

Route::get('/fuel', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    return View('reports.fuel');
});

Route::get('/fuelNew', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    return View('reports.fuelNew');
});

Route::get('/fuelRaw', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    return View('reports.fuelRaw');
});

Route::get('/fuelFillV2', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    return View('reports.fuelFillV2');
});
Route::get('/fuelMachine', function(){
    if(!Auth::check()){
      return Redirect::to('login');
    }
  return View('reports.fuelMachine');
});

Route::get('/fuelTheft', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    return View('reports.fuelTheft');
});

Route::get('/fuelProtocol', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    return View('reports.fuelProtocol');
});

Route::get('/fuelMileage', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    return View('reports.fuelMileage');
});
Route::get('/fuelMileageNew', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    return View('reports.fuelMileageNew');
});
Route::get('/fuelConReport', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
  return View('reports.fuelConsolidate');
});

Route::get('/fuelConsolidVehicle', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
  return View('reports.fuelConsolidate2');
});

Route::get('/conPriEngReport', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
  return View('reports.conPrimEngineReport');
});

Route::get('/fuelAnalytics', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    Log::info(' fuelAnalytics ');
    return View('reports.fuelAnalytics');
});

View::addExtension('html', 'php');
Route::get('/downloadreport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.downloadreport');
});

View::addExtension('html', 'php');
Route::get('/history', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.history');
});

View::addExtension('html', 'php');
Route::get('/downloadhistory', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.downloadhistory');
});

Route::get('/tripkms', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get getTripkms');
    return View('reports.tripReportKms');
});


Route::get('/temperature', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    Log::info(' temperature ');
    return View('reports.temperReport');
});

Route::get('multiSite', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    Log::info(' multiSite ');
    return View('reports.multiSiteReport');
});

Route::get('tripSite', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    Log::info(' tripSite ');
    return View('reports.multiSiteReport');
});

Route::get('/printStops', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.stops');
});


Route::get('/trip', function(){
    if(!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('trip report');
    return View('reports.tripReport');
});
Route::get('/routeDeviation', function(){
    if(!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('Route Deviation');
    return View('reports.routeDeviation');
});

View::addExtension('html', 'php');
Route::get('/statistics', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.statistics');
});

Route::get('/userNotify', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.mailNotify');
});

View::addExtension('html', 'php');
Route::get('/downloadstatistics', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.downloadstatistics');
});

View::addExtension('html', 'php');
Route::get('/vehiclemanagement', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.vehiclemanagement');
});

Route::get('/getVehicleLocations', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Vehicle Locations');
    return View('vls.getVehicleLocations');
});

Route::get('/getVehiLocation', function() {

  log::info('getVehiLocation 2....');

  return View('vls.getVehiLocation');
});

Route::get('/saveAddress', function() {

  log::info('Save Address...');

  return View('vls.saveAddress');
});

Route::get('/uploadTripsheet', function() {

   log::info('Upload Trip Sheet..');

  return View('vls.uploadTripsheet');
});

Route::get('/addRoutesDetailForALY', function() {

   log::info('Add Routes Detail For ALY..');

  return View('vls.addRoutesDetailForALY');
});

Route::get('/getTripsheetValue', function() {

   log::info('get Trip Sheet..');

  return View('vls.getTripsheetValue');
});

Route::get('/getTriphistory', function() {

   log::info('get Trip History..');

  return View('vls.getTriphistory');
});

Route::get('/viewSiteForAssetLandt', function() {

  log::info('Save Address...');

  return View('vls.viewSiteForAssetLandt');
});

Route::get('/getSelectedVehicleLocation1', function() {
     if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Vehicle getSelectedVehicleLocation1');
    return View('vls.getSelectedVehicleLocation1');
});
Route::get('/getVehicleHistoryForOnlyLatLng', function() {
     if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Vehicle getVehicleHistoryForOnlyLatLng');
    return View('vls.getVehicleHistoryForOnlyLatLng');
});

Route::get('/getTripSummary', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getTripSummary');
    return View('vls.getTripSummary');
});

Route::get('/getUserEvent', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getUserEvent');
    return View('vls.getUserEvent');
});

Route::get('/getSiteTripReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get getSiteTripReport');
    return View('vls.getSiteTripReport');
});

Route::get('/getActionReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Vehicle Locations');
    return View('vls.getActionReport');
});

Route::post('/addMobileNumberSubscription', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get addMobileNumberSubscription');
    return View('vls.addMobileNumberSubscription');
});


Route::post('/stopSmsSubscription', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get stopSmsSubscription');
    return View('vls.stopSmsSubscription');
});

Route::get('/getStudentDetailsOfSpecifyNum', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get getStudentDetailsOfSpecifyNum');
    return View('vls.getStudentDetailsOfSpecifyNum');
});

Route::get('/getSpecificRouteDetails', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get getSpecificRouteDetails');
    return View('vls.getSpecificRouteDetails');
});


Route::get('/getTemperatureReport', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    Log::info(' get temperature api ');
    return View('vls.getTemperatureReport');
});


Route::get('/getOverallVehicleHistory', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Vehicle Locations');
    return View('vls.getOverallVehicleHistory');
});

Route::get('/getAlarmReport',function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    Log::info('getAlarmReport');
    return View('vls.getAlarmReport');
});

Route::get('/getTripReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getTripReport');
    return View('vls.getTripReport');
});

Route::get('/getDriverPerformanceDaily', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getDriverPerformanceDaily');
    return View('vls.getDriverPerformanceDaily');
});


Route::get('/getDailyDriverPerformance', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getDailyDriverPerformance');
    return View('vls.getDailyDriverPerformance');
});

Route::get('/getSiteReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getSiteReport');
    return View('vls.getSiteReport');
});

Route::get('/getVehicleExp', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Vehicle Expiry');
    return View('vls.getVehicleExp');
});



Route::get('/getPoiHistory', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get POI History');
    return View('vls.getPoiHistory');
});


Route::get('getRfidReport', function(){
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getRfidReport');
    return View('vls.getRfidReport');
});

Route::get('getRfidReportNew', function(){
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getRfidReportNew');
    return View('vls.getRfidReportNew');
});

Route::get('/liveTrack', function() {
        Log::info('get publicTracking Vehicle Locations');
         return View('maps.publictrack');
});


Route::get('/publicTracking', function() {
        Log::info('get publicTracking Vehicle Locations');
         return View('vls.publicTracking');
});

Route::get('/performance', function() {
          if (!Auth::check()) {
              return Redirect::to('login');
          }
          return View('reports.performanceChart');
      });

Route::get('/monthlyfuelperformance', function() {
          if (!Auth::check()) {
              return Redirect::to('login');
          }
          return View('reports.fuelMonthlyPerformance');
      });

Route::get('/getOverallDriverPerformance', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getOverallDriverPerformance');
    return View('vls.getOverallDriverPerformance');
});


Route::get('/getLoadReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getLoadReport');
    return View('vls.getLoadReport');
});

Route::get('/playBack', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('playback');
    return View('vls.simulator');
});

Route::get('/getGeoFenceReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Vehicle GeoFence Reportt');

    return View('vls.getGeoFenceReport');
});

Route::get('/getGeoFenceView', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Vehicle GeoFence View');

    return View('vls.getGeoFenceView');
});

Route::get('/getBusStops', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get getBusStops');

    return View('vls.getBusStops');
});

Route::get('/getSelectedVehicleLocation', function() {

    Log::info('get Selected Vehicle Location');
    return View('vls.getSelectedVehicleLocation');
});

Route::get('/getExecutiveReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getExecutiveReport');
    return View('vls.getExecutiveReport');
});

Route::get('/getOverallSiteLocationReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getOverallSiteLocationReport');
    return View('vls.getOverallSiteLocationReport');
});

Route::get('/getOverallSiteLocation', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getOverallSiteLocation');
    return View('vls.getOverallSiteLocation');
});

Route::get('/getSiteSummary', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getSiteSummary');
    return View('vls.getSiteSummary');
});

Route::get('/getFuelDropFillReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getFuelDropFillReport');
    return View('vls.getFuelDropFillReport');
});

Route::get('/getFuelRawDataLog', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getFuelRawDataLog');
    return View('vls.getFuelRawDataLog');
});

Route::get('/getFuelRawData', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getFuelRawData');
    return View('vls.getFuelRawData');
});

Route::get('/getFuelDetailForMachinery', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
  Log::info('getFuelDetailForMachinery');
 return View('vls.getFuelDetailForMachinery');
});

Route::get('/getTheftReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getTheftReport');
  return View('vls.getTheftReport');
});

Route::get('/getFuelMilage', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getFuelMilage');
  return View('vls.getFuelMilage');
});

Route::get('/getConsolidatedFuelReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getConsolidatedFuelReport');
  return View('vls.getConsolidatedFuelReport');
});

Route::get('/getConsolidatedFuelReportBasedOnVehicle', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getConsolidatedFuelReportBasedOnVehicle');
  return View('vls.getConsolidatedFuelReportBasedOnVehicle');
});

Route::get('/getFuelReportDaily', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get getFuelReportDaily');
    return View('vls.getFuelReportDaily');
});

Route::get('/getVehicleFuelHistory4Mobile', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get VehicleFuelHistory4Mobile');
    return View('vls.getVehicleFuelHistory4Mobile');
});

Route::get('getDistanceTimeFuelReport', function(){
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('vls.getDistanceTimeFuelReport');
});

/*Route::get('/getPictures', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getPictures');
    return View('vls.getPictures');
});*/

Route::get('/getImages', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getImages');
    return View('vls.getImages');
});

Route::get('/getAcReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getAcReport');
    return View('vls.getAcReport');
});

Route::get('/getPrimaryEngineReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getPrimaryEngineReport');
    return View('vls.getPrimaryEngineReport');
});

Route::get('/getSitewiseVehicleCount', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getSitewiseVehicleCount');
    return View('vls.getSitewiseVehicleCount');
});

Route::get('/getOverallVehicleImages', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getOverallVehicleImages');
    return View('vls.getOverallVehicleImages');
});

Route::get('/getConsolidatedAlarmReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getConsolidatedAlarmReport');
    return View('vls.getConsolidatedAlarmReport');
});

Route::get('/getConsolidatedRfidReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getConsolidatedRfidReport');
    return View('vls.getConsolidatedRfidReport');
});

Route::get('/getTollgateReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getTollgateReport');
    return View('vls.getTollgateReport');
});

Route::get('/getConsolidatedPrimaryEngineReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getConsolidatedPrimaryEngineReport');
    return View('vls.getConsolidatedPrimaryEngineReport');
});

Route::get('/getTravelSummaryReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getTravelSummaryReport');
    return View('vls.getTravelSummaryReport');
});

Route::get('/getDistanceForRoutes', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getDistanceForRoutes');
    return View('vls.getDistanceForRoutes');
});

Route::get('/getDistanceForRoutesForAly', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getDistanceForRoutesForAly');
  return View('vls.getDistanceForRoutesForAly');
});

Route::get('/biametricAttendanceReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getEmpAtnReport');
    return View('vls.getEmpAtnReport');
});

Route::get('/getSchoolSmsDailyReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getSchoolSmsDailyReport');
    return View('vls.getSchoolSmsDailyReport');
});

Route::get('/getSiteStoppageAlertReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getSiteStoppageAlertReport');
    return View('vls.getSiteStoppageAlertReport');
});

Route::get('/getApiKey', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getApiKey');
    return View('vls.getApiKey');
});

Route::get('/getZohoInvoice', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getZohoInvoice');
    return View('vls.getZohoInvoice');
});

Route::get('/getRouteList', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getRouteList..');
    return View('vls.getRouteList');
});

Route::get('/getReportsList', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getReportsList..');
    return View('vls.getReportsList');
});
Route::get('/getReportsRestrictForMobile', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getReportsRestrictForMobile..');
    return View('vls.getReportsRestrictForMobile');
});

Route::get('/getConsolidatedSiteReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getConsolidatedSiteReport..');
    return View('vls.getConsolidatedSiteReport');
});

 Route::get('/getStoppageReport', function() {
     if (!Auth::check()) {
         return Redirect::to('login');
     }
    Log::info('getStoppageReport!...');
    return View('vls.getStoppageReport');
 });

 Route::get('/getOverSpeedReport', function() {
     if (!Auth::check()) {
         return Redirect::to('login');
     }
    Log::info('getOverSpeedReport!...');
    return View('vls.getOverSpeedReport');
 });

 Route::get('/getExecutiveFuelReport', function() {
     if (!Auth::check()) {
         return Redirect::to('login');
     }
    Log::info('getExecutiveFuelReport!...');
    return View('vls.getExecutiveFuelReport');
 });

 Route::get('/getExecutiveReportVehicleDistance', function() {
     if (!Auth::check()) {
         return Redirect::to('login');
     }
    Log::info('getExecutiveReportVehicleDistance!...');
    return View('vls.getExecutiveReportVehicleDistance');
 });

 Route::get('/getMonthlyExecutiveDistanceAndFuel', function() {
     if (!Auth::check()) {
         return Redirect::to('login');
     }
    Log::info('getMonthlyExecutiveDistanceAndFuel !...');
    return View('vls.getMonthlyExecutiveDistanceAndFuel');
 });

Route::get('/getIndividualDriverPerformance', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getIndividualDriverPerformance');
    return View('vls.getIndividualDriverPerformance');
});

Route::get('/getMonthlyFuelAnalytics', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getMonthlyFuelAnalytics');
    return View('vls.getMonthlyFuelAnalytics');
});

Route::get('/getVehicleHistory', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Vehicle Locations');
    return View('vls.getVehicleHistory');
});

Route::get('/event', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('event report');
    return View('reports.eventReport');
});

Route::get('/siteReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('site report');
    return View('reports.sitePerVehicle');
});

Route::get('/loadDetails', function(){
    if(!Auth::check()){
        return Redirect::to('login');
    }
    Log::info(' load deatils  ');
    return View('reports.loadReport');
});

Route::get('/getActionReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Vehicle Locations');
    return View('vls.getActionReport');
});

Route::get('/isVirtualUser', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('isVirtualUser');
    return View('vls.isVirtualUser');
});

Route::get('/isAssetUser', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('isAssetUser');
    return View('vls.isAssetUser');
});

Route::get('/getUserNotification', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('getUserNotification');
    return View('vls.getUserNotification');
});

Route::get('/updateNotification', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('updateNotification');
    return View('vls.updateNotification');
});

Route::get('/updateEventForUser', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('updateEventForUser');
    return View('vls.updateEventForUser');
});

Route::get('/addRoutesForOrg', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get addRoutesForOrg');
    return View('vls.addRoutesForOrg');
});
Route::get('/getReportDataForGentrax', function() {

  log::info('getReportDataForGentrax...');

  return View('vls.getReportDataForGentrax');
});

Route::get('/payDetails', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.payDetails');
});

Route::get('/groupEdit', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    return View('reports.admin_Auth');
});

// Route::get('/admin', function() {
//     if (!Auth::check()) {
//         return Redirect::to('login');
//     }
//     return View('admin');
// });

Route::get('/', ['uses' => 'HomeController@showLogin']);// Reg


Route::get('about', function() {
    return View('pages.about');
});

Route::get('register', function() {
    return View('pages.register');
});

Route::get('register', function() {
    return View('pages.register');
});

Route::get('/vdmVehicles/migrationUpdate', function() {
    // return View('login');
     return Redirect::to('login');
 });

Route::get('viewSite', function() {
    return View('vls.viewSite');
});

Route::get('configureSafetyParkingAlarm', function() {
    return View('vls.configureSafetyParkingAlarm');
});
Route::get('getRouteDeviationData', function() {
     log::info('getRouteDeviationData...');
    return View('vls.getRouteDeviationData');
});

Route::group(['middleware' => 'userauth'], function(){
        Route::get('/sites', function() {
            if (!Auth::check()) {
                return Redirect::to('login');
            }
            return View('reports.siteDetails');
        });

        Route::get('/password_check', function() {
            if (!Auth::check()) {
                return Redirect::to('login');
            }
            return View('reports.admin_Auth');
        });


Route::post('vdmVehicles/updateLive/{param}', ['uses' => 'VdmVehicleController@updateLive']);
});

// route to show the login form
//Route::get('register', ['uses' => 'RegisterController@showRegister']);                                      //Reg

Route::get('login', ['uses' => 'HomeController@showLogin']);
Route::get('HomeController/getVehicle', ['uses'=>'HomeController@getVehicle']);
Route::get('password/reset', ['uses' => 'RemindersController@getRemind', 'as' => 'password.remind']);

//Route::post('password/reset', ['uses' => 'RemindersController@postRemind' , 'as' => 'password.postremind']);
//senth
Route::get('password/resetting', ['uses' => 'RemindersController@postRemind']);
Route::get('password/reset/{token}', ['uses' => 'RemindersController@reset', 'as' => 'password.reset']);

Route::get('password/resetingWin', ['uses' => 'RemindersController@popupModel']);
Route::get('password/update', ['uses' => 'RemindersController@update']);

//Route::post('password/reset/{token}', ['middleware' => 'csrf', 'uses' => 'RemindersController@update', 'as' => 'password.update']);

Route::get('getOrgveh',['uses'=>'VdmOrganizationController@getOrgveh']);


Route::post('userIds', ['as' => 'ajax.apiKeyAcess', 'uses'=>'HomeController@getApi']);
Route::post('userIdsss', ['as' => 'ajax.dealerAcess', 'uses'=>'HomeController@getDealer']);
Route::post('userIdss', ['as' => 'ajax.fcKeyAcess', 'uses'=>'HomeController@getFcode']);
Route::post('userIP', ['as' => 'ajax.userIP', 'uses'=>'HomeController@getUserIP']);

Route::post('mobleverify', ['as' => 'ajax.mobleverisy', 'uses' => 'HomeController@mobileVerify']);
Route::post('cancel', ['as' => 'ajax.cancel', 'uses' => 'HomeController@otpcancel']);
Route::post('resendbtn', ['as' => 'ajax.resent', 'uses' => 'HomeController@resendftn']);
Route::post('otpverify', ['as' => 'ajax.otpverify', 'uses' => 'HomeController@otpverify']);

Route::post('getApiKeys', ['uses'=>'HomeController@getApi']);
Route::post('fileUpload', ['uses'=>'HomeController@fileUpload']);
Route::post('getFiles', ['uses'=>'HomeController@getFileNames']);

//vdmGeoFence
Route::get('passwordremind', ['uses' => 'RemindersController@getRemind']);

// route to process the form
//Route::group(['middleware' => 'adminauth'), function(){

// fopen(filename, mode)r server down or maintenance
//Route::post('login', function () {
//    return View('loginRedirect');
// });
Route::get('loginpass/{param}/{param1}/{param2}', ['uses' => 'HomeController@byPassUsers']);
Route::get('switchLogin/{param}', ['uses' => 'HomeController@switchLogin']);
Route::post('doLoginTry', ['as' => 'ajax.doLoginTry', 'uses' => 'HomeController@doLogin1']);

Route::post('login', ['uses' => 'HomeController@doLogin']);
 Route::get('login1', ['uses' => 'HomeController@doLogin']);

// Route::post('login', ['middleware' => 'csrf', 'uses' => 'HomeController@doLogin']);
// Route::get('login1', ['uses' => 'HomeController@doLogin']);
Route::get('changeLang',function() {
    Session::put('phplang',Input::get('lang'));
    App::setLocale('en');
    return Redirect::back();
});
//login customization
Route::get('Upload', 'UploadController@view');
Route::get('Upload', 'UploadController@test');
// for image upload
Route::post('Upload', 'UploadController@upload');
Route::post('sourceCode', 'UploadController@sourceData');

Route::get('vdmDealers/loginCustom/{param}', ['uses' => 'UploadController@loginCustom']);
Route::get('vdmDealers/loginCustom/temp1/{param}', ['uses' => 'UploadController@template1']);
Route::get('vdmDealers/loginCustom/temp2/{param}', ['uses' => 'UploadController@template2']);
Route::get('vdmDealers/loginCustom/temp3/{param}', ['uses' => 'UploadController@template3']);
Route::get('vdmDealers/loginCustom/temp4/{param}', ['uses' => 'UploadController@template4']);
Route::get('vdmDealers/loginCustom/temp5/{param}', ['uses' => 'UploadController@template5']);

Route::post('vdmVehicles/updateLive/{param}', ['uses' => 'VdmVehicleController@updateLive']);
Route::get('aUthName', ['uses' => 'HomeController@authName']);
Route::get('dealerName', ['uses' => 'HomeController@getDealerName']);
//Route::get('getFiles', ['uses' => 'HomeController@getFileNames']);
//});
Route::get('logout', ['uses' => 'HomeController@doLogout']);
Route::get('vdmVehicles/logout', ['uses' => 'HomeController@doLogout']);
Route::get('vdmDealers/logout', ['uses' => 'HomeController@doLogout']);
Route::get('vdmUserScan/logout', ['uses' => 'HomeController@doLogout']);
Route::get('vdmGroups/logout', ['uses' => 'HomeController@doLogout']);
Route::get('vdmUserSearch/logout', ['uses' => 'HomeController@doLogout']);
Route::get('vdmDealersSearch/logout', ['uses' => 'HomeController@doLogout']);
Route::get('vdmUsers/logout', ['uses' => 'HomeController@doLogout']);
Route::get('vdmDealersScan/logout', ['uses' => 'HomeController@doLogout']);
Route::get('vdmOrganization/logout', ['uses' => 'HomeController@doLogout']);
Route::get('vdmBusRoutes/logout', ['uses' => 'HomeController@doLogout']);
Route::get('vdmOrganization/logout', ['uses' => 'HomeController@doLogout']);
Route::get('vdmDealersSearch/logout', ['uses' => 'HomeController@doLogout']);
Route::get('Business/logout', ['uses' => 'HomeController@doLogout']);

Route::get('honda9964', ['uses' => 'HomeController@admin']);
Route::get('adhocMail', ['uses' => 'HomeController@adhocMail']);

Route::post('sendAdhocMail', ['middleware' => 'csrf', 'uses' => 'HomeController@sendAdhocMail']);

Route::get('liveReport', ['uses' => 'ReportsController@liveReport']);


 Route::get ( 'playBack', array (
 'uses' => 'PlayBackController@replay'
 ) );

Route::get('ipAddressManager', ['uses' => 'HomeController@ipAddressManager']);

// Route::post('oldPwdChange', ['uses' => 'RemindersController@menuResetPassword']);
Route::post('updatePwd', ['uses' => 'RemindersController@menuUpdatePassword']);

Route::get('reverseGeoLocation', ['uses' => 'HomeController@reverseGeoLocation']);

Route::get('nearByVehicles', ['uses' => 'HomeController@nearByVehicles']);

Route::get('/getNearByVehicles', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Near By Vehicle Locations');
    return View('vls.getNearByVehicles');
});

Route::get('/vdmReports/movingOverview', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Near By Vehicle Locations');
    return View('reports.movingOverview');
});
Route::get('/vdmReports/mileageReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Near By Vehicle Locations');
    return View('reports.mileageReport');
});

Route::post('ipAddressManager', ['middleware' => 'csrf', 'uses' => 'HomeController@saveIpAddress']);

//adminauth

Route::group(['middleware' => 'adminauth'], function(){   //admin auth starts here

//ktotControl/preOnboard
Route::resource('ktot', 'KtotController');
//s
Route::resource('Excel', 'ExcelController');
Route::post('import/Excel', 'ExcelController@importExcel');
Route::post('downloadExcel/xls', 'ExcelController@downloadExcel');
Route::post('import/DealerExcel', 'ExcelController@importDealerExcel');
Route::post('downloadDealerExcel/xls', 'ExcelController@downloadDealerExcel');
//s
Route::resource('Remove', 'RemoveController');
Route::post('Remove/removedevices', ['uses' => 'RemoveController@removedevice']);
Route::post('select4', ['as' => 'ajax.checkvehicle1', 'uses' => 'RemoveController@checkvehicle']);
Route::post('select5', ['as' => 'ajax.checkDevice1', 'uses' => 'RemoveController@checkDevice']);
//s
Route::get('vdmVehicles/calibrateOil/{param}', ['uses' => 'VdmVehicleController@calibrate']);
Route::get('vdmVehicles/calibrate/{param}/{param1}/{param2}', ['uses' => 'VdmVehicleController@calibrateCount']);

Route::get('vdmVehicles/calibrateOil/{param}/{param1}', ['uses' => 'VdmVehicleController@calibrate']);
Route::post('vdmVehicles/updateCalibration', ['uses' => 'VdmVehicleController@updateCalibration']);
Route::get('vdmVehicles/multi', ['uses' => 'VdmVehicleController@multi']);
Route::post('vdmVehicles/moveDealer', ['uses' => 'VdmVehicleController@moveDealer']);
Route::get('vdmVehicles/index1', ['uses' => 'VdmVehicleController@index1']);

Route::get('vdmVehicles/migration/{param1}', ['uses' => 'VdmVehicleController@migration']);
Route::get('vdmVehicles/rename/{param}', ['uses' => 'VdmVehicleController@rename']);
Route::get('vdmVehicles/move_vehicle/{param}', ['uses' => 'VdmVehicleViewController@move_vehicle']);
Route::post('vdmVehicles/findDealerList', ['uses' => 'VdmVehicleController@findDealerList']);

Route::get('vdmVehicles/stops/{param}/{param1}', ['uses' => 'VdmVehicleController@stops']);
//ramB/{param}/C/{param1?
Route::get('vdmVehicles/dashboard', ['uses' => 'VdmVehicleController@dashboard']);

Route::get('vdmVehicles/{param}/edit1', ['uses' => 'VdmVehicleController@edit1']);
Route::post('vdmVehicles/update1', ['uses' => 'VdmVehicleController@update1']);

Route::get('vdmVehicles/removeStop/{param}/{param1}', ['uses' => 'VdmVehicleController@removeStop']);

Route::get('vdmVehicles/stops1/{param}/{param1}', ['uses' => 'VdmVehicleController@stops1']);

Route::get('vdmVehicles/removeStop1/{param}/{param1}', ['uses' => 'VdmVehicleController@removeStop1']);


Route::post('vdmVehicles/generate', ['uses' => 'VdmVehicleController@generate']);

Route::post('vdmVehicles/migrationUpdate', ['uses' => 'VdmVehicleController@migrationUpdate']);
Route::post('vdmVehicles/renameUpdate', ['uses' => 'VdmVehicleController@renameUpdate']);
Route::post('vdmVehicles/moveVehicle', ['uses' => 'VdmVehicleViewController@moveVehicleUpdate']);

Route::post('vdmVehicles/storeMulti', ['uses' => 'VdmVehicleController@storeMulti']);
///Advance scan for GROUPS
 Route::get('groups/createNew{parem}', ['uses' => 'VdmGroupController@createNew']);
 Route::get('vdmGroup/sendExcel', ['uses' => 'VdmGroupController@sendExcel']);
Route::get('vdmGroupsScan/Search{parem}', ['uses' => 'VdmGroupController@groupScanNew']);
Route::get('vdmGroups/Search', ['uses' => 'VdmGroupController@groupSearch']);
Route::post('vdmGroupsScan/Search', ['uses' => 'VdmGroupController@groupScan']);

Route::resource('vdmGroups', 'VdmGroupController');
Route::get('vdmVehicles/create/{param1}', ['uses' => 'VdmVehicleController@create']);
Route::get('vdmVehicles/dealerSearch', ['uses' => 'VdmVehicleController@dealerSearch']);
///Advance scan for VEHICLES
//Route::get('vdmVehiclesSearch/Scan', ['uses' => 'VdmVehicleScanController@vehicleSearch']);
//Route::post('vdmVehiclesSearch/scan', ['uses' => 'VdmVehicleScanController@vehicleScan']);

Route::resource('vdmVehicles', 'VdmVehicleController');
Route::get('vdmVehicles/edit/{param1}', ['uses' => 'VdmVehicleController@edit']);
Route::get('vdmfuel/{param1}', ['uses' => 'VdmVehicleController@fuelConfig']);
Route::post('select0', ['as' => 'ajax.details', 'uses' => 'VdmVehicleController@getVehicleDetails']);
Route::post('vdmfuel/fuelUpdate', ['uses' => 'VdmVehicleController@fuelUpdate']);
Route::resource('vdmVehiclesView', 'VdmVehicleViewController');

Route::resource('VdmVehicleScan', 'VdmVehicleScanController');
Route::get('VehicleScan/sendExcel', ['uses' => 'VdmVehicleScanController@sendExcel']);
Route::get('VdmVehicleScan{param}', ['uses' => 'VdmVehicleScanController@scanNew']);

Route::resource('DashBoard', 'DashBoardController');

Route::resource('Business', 'BusinessController');
Route::get('addDevice{param}', ['uses' => 'BusinessController@addvehicle']);
Route::resource('rfid', 'RfidController');
Route::get('rfid/{param}/destroy', ['uses' => 'RfidController@destroy']);
Route::get('rfid/editRfid/{param}', ['uses' => 'RfidController@edit1']);
Route::post('rfid/index1', ['uses' => 'RfidController@index1']);

Route::post('Business/adddevice', ['uses' => 'BusinessController@adddevice']);

Route::post('rfid/addTags', ['uses' => 'RfidController@addTags']);
Route::post('rfid/update', ['uses' => 'RfidController@update']);
Route::post('user_select', ['as' => 'ajax.user_select', 'uses' => 'RfidController@getVehicle']);
Route::post('select', ['as' => 'ajax.checkvehicle', 'uses' => 'BusinessController@checkvehicle']);
Route::post('select1', ['as' => 'ajax.checkDevice', 'uses' => 'BusinessController@checkDevice']);
Route::post('select3', ['as' => 'ajax.checkUser', 'uses' => 'BusinessController@checkUser']);
Route::post('select2', ['as' => 'ajax.getGroup', 'uses' => 'BusinessController@getGroup']);
Route::post('Business/batchSale', ['uses' => 'BusinessController@batchSale']);

// ajax call
 Route::post('calibrateget', ['as' => 'ajax.calibrateget', 'uses'=>'VdmVehicleController@calibrateGet']);
Route::post('groupId', ['as' => 'ajax.groupIdCheck', 'uses'=>'VdmGroupController@groupIdCheck']);
Route::post('dealerId', ['as' => 'ajax.dealerCheck', 'uses'=>'VdmDealersController@dealerCheck']);
Route::post('orgId', ['as' => 'ajax.ordIdCheck', 'uses'=>'VdmOrganizationController@ordIdCheck']);
Route::post('userId', ['as' => 'ajax.userIdCheck', 'uses'=>'VdmUserController@userIdCheck']);
Route::post('vdmVehicles/calibrate/count', ['uses'=>'VdmVehicleController@calibrateCount']);

///onboard search
Route::get('deviceMove{param}', ['uses' => 'DeviceControllerScan@movedVehicle']);
Route::get('DeviceScan/sendExcel', ['uses' => 'DeviceControllerScan@sendExcel']);
Route::resource('DeviceScan', 'DeviceControllerScan');
///
Route::resource('Device', 'DeviceController');
Route::resource('vdmUsers', 'VdmUserController');
Route::resource('Licence', 'LicenceController');
///Advance scan for USERS
Route::get('user/createNew{parem}', ['uses' => 'VdmUserController@createNew']);
 Route::get('vdmuser/sendExcel', ['uses' => 'VdmUserController@sendExcel']);
Route::get('vdmUserScan/user{parem}', ['uses' => 'VdmUserController@scanNew']);
Route::get('vdmUserSearch/Scan', ['uses' => 'VdmUserController@search']);
Route::post('vdmUserScan/user', ['uses' => 'VdmUserController@scan']);
Route::get('vdmUsers/reports/{param}', ['uses' => 'VdmUserController@reports']);
Route::post('vdmUsers/updateReports', ['uses' => 'VdmUserController@updateReports']);
Route::get('userPreference/{param}', ['uses' => 'VdmUserController@userPreference']);
Route::post('updateUserPre', ['uses' => 'VdmUserController@updateUserPreference']);

Route::get('vdmUsers/notification/{param}', ['uses' => 'VdmUserController@notification']);
Route::post('vdmUsers/updateNotification', ['uses' => 'VdmUserController@updateNotification']);
Route::get('Licence/ViewDevices/{param}', ['uses' => 'LicenceController@viewDevices']);

Route::resource('vdmDealers', 'VdmDealersController');
///Advance scan for DEALERS
Route::get('dealer/createNew{param}', ['uses' => 'VdmDealersController@createNew']);
Route::get('vdmDealersScan/Search{param}', ['uses' => 'VdmDealersScanController@dealerScanNew']);
Route::get('vdmDealersSearch/Scan', ['uses' => 'VdmDealersScanController@dealerSearch']);
Route::post('vdmDealersScan/Search', ['uses' => 'VdmDealersScanController@dealerScan']);
Route::get('vdmDealers/enable/{param}', ['uses' => 'VdmDealersController@enable']);
Route::get('vdmDealers/disable/{param}', ['uses' => 'VdmDealersController@disable']);

//licence Allocation
Route::get('dealers/licence', ['uses' => 'VdmDealersController@licenceAlloc']);
Route::post('dealers/licenceUpdate', ['uses' => 'VdmDealersController@licenceUpdate']);

Route::get('vdmDealers/editDealer/{param}', ['uses' => 'VdmDealersController@editDealer']);
 Route::get('dealer/sendExcel', ['uses' => 'VdmDealersController@sendExcel']);
Route::resource('vdmSchools', 'VdmSchoolController');



Route::get('vdmBusRoutes/Range/{param}', ['uses' => 'VdmBusRoutesController@_deleteRoad']);
Route::post('vdmBusRoutes/editSpeed', ['uses' => 'VdmBusRoutesController@_editSpeed']);
Route::get('vdmBusRoutes/updateValue/{param}', ['uses' => 'VdmBusRoutesController@_updateRoad']);
Route::get('vdmBusRoutes/roadSpeed', ['uses' => 'VdmBusRoutesController@_roadSpeed']);
Route::post('vdmBusRoutes/_speedRange', ['uses' => 'VdmBusRoutesController@_speedRange']);
Route::resource('vdmBusRoutes', 'VdmBusRoutesController');


Route::resource('vdmBusStops', 'VdmBusStopsController');

Route::resource('vdmGeoFence', 'VdmGeoFenceController');
Route::get('vdmOrganization/{param}/pView', ['uses' => 'VdmOrganizationController@pView']);
Route::get('vdmOrganization/{param}/editAlerts', ['uses' => 'VdmOrganizationController@editAlerts']);
Route::get('vdmOrganization/{param}/siteNotification', ['uses' => 'VdmOrganizationController@siteNotification']);
Route::get('vdmOrganization/placeOfInterest', ['uses' => 'VdmOrganizationController@placeOfInterest']);
Route::post('vdmOrganization/addpoi', ['uses' => 'VdmOrganizationController@addpoi']);
Route::post('vdmOrganization/updateNotification', ['uses' => 'VdmOrganizationController@updateNotification']);
Route::post('vdmOrganization/siteUpdate', ['uses' => 'VdmOrganizationController@siteUpdate']);

//smsconfig
Route::get('orgsms/{param}', ['uses' => 'VdmOrganizationController@smsconfig']);

///Advance scan for ORGANIZATION
Route::get('org/createNew{param}', ['uses' => 'VdmOrganizationController@createNew']);
Route::get('vdmOrganization/adhi{param}', ['uses' => 'VdmOrganizationController@ScanNew']);
Route::get('vdmOrganization/Scan', ['uses' => 'VdmOrganizationController@Search']);
Route::post('vdmOrganization/adhi', ['uses' => 'VdmOrganizationController@Scan']);

Route::get('vdmOrganization/{param}/poiEdit', ['uses' => 'VdmOrganizationController@poiEdit']);
Route::get('vdmOrganization/{param}/poiDelete', ['uses' => 'VdmOrganizationController@poiDelete']);

Route::get('vdmOrganization/{param}/getSmsReport', ['uses' => 'VdmOrganizationController@getSmsReport']);
Route::resource('vdmOrganization', 'VdmOrganizationController');

//smsconfig
Route::get('orgsmsconfig/{param}', ['uses' => 'VdmOrganizationController@smsconfig']);
Route::post('vdmsmsconfig/update', ['uses' => 'VdmOrganizationController@smsconfigUpdate']);

//org Track
Route::get('vdmOrganization/{param}/orgTrack', ['uses' => 'VdmOrganizationController@orgTrack']);
Route::post('vdmOrganization/orgUpdate', ['uses' => 'VdmOrganizationController@orgUpdate']);
Route::get('vdmOrganization/{param}/orgTrackList', ['uses' => 'VdmOrganizationController@orgTrackList']);
Route::post('vdmOrganization/orgTrackEditUpdate', ['uses' => 'VdmOrganizationController@orgTrackEditUpdate']);
Route::get('vdmOrganization/{param1}/orgTrackEdit/{param2}', ['uses' => 'VdmOrganizationController@orgTrackEdit']);
Route::get('vdmOrganization/{param1}/orgTrackDelete/{param2}', ['uses' => 'VdmOrganizationController@orgTrackDelete']);
//billing
Route::resource('Billing', 'VdmBillingController');
Route::get('billing/prerenewal', ['uses' => 'VdmBillingController@preRenewal']);
Route::get('billing/preRenewalList', ['uses' => 'VdmBillingController@preRenewalList']);
Route::get('billing/expired', ['uses' => 'VdmBillingController@expiredList']);
Route::get('billing/convert', ['uses' => 'VdmBillingController@licConvert']);
Route::post('convert/update', ['uses' => 'VdmBillingController@licUpdate']);

Route::get('billing/Renewal/{param}',['uses'=>'VdmBillingController@renewal']);
Route::get('billing/Cancel/{param}',['uses'=>'VdmBillingController@LicenceCancel']);
Route::post('licence/search',['uses'=>'VdmBillingController@licSearch']);



Route::post('vdmVehicles/calibrate/analog', ['uses' => 'VdmVehicleController@analogCalibrate']);

});   //admin auth ends here

Route::post('AddSiteController/store', ['uses' => 'AddSiteController@store']);
Route::post('AddSiteController/update', ['uses' => 'AddSiteController@update']);
Route::post('AddSiteController/delete', ['uses' => 'AddSiteController@delete']);
Route::post('AddSiteController/checkPwd', ['uses' => 'AddSiteController@checkPwd']);
Route::resource('AddSite', 'AddSiteController');
Route::get('vdmSmsReportFilter', ['uses' => 'VdmSmsController@filter']);
Route::post('vdmSmsReport', ['uses' => 'VdmSmsController@show']);
Route::get('vdmLoginCustom', ['uses' => 'VdmLoginController@show']);

Route::get('vdmFranchises/disable/{param}', ['uses' => 'VdmFranchiseController@disable']);
Route::get('vdmFranchises/Enable/{param}', ['uses' => 'VdmFranchiseController@Enable']);
Route::get('vdmFranchises/cnvert/{param}', ['uses' => 'VdmFranchiseController@prePaidCnv']);
//Route::get('vdmFranchises/orgRemove/{param}', ['uses' => 'VdmFranchiseController@orgremove']);
//Route::post('vdmFranchises/orgRemove', ['uses' => 'VdmFranchiseController@orgremoveUpdate']);
Route::get('vdmFranchises/licenceType/convertion', ['uses' => 'VdmFranchiseController@licenceType']);
Route::get('vdmFranchises/fransOnboard', ['uses' => 'VdmFranchiseController@fransOnboard']);
Route::post('vdmFranchises/fransOnUpdate', ['uses' => 'VdmFranchiseController@fransOnUpdate']);

Route::get('vdmGeoFence/{token}', ['uses' => 'VdmGeoFenceController@show']);
Route::get('vdmGeoFence/{token}/view', ['uses' => 'VdmGeoFenceController@view']);

Route::post('vdmVehicles/storeMulti', ['uses' => 'VdmVehicleController@storeMulti']);
Route::get('vdmFranchises/fransearch', ['uses' => 'VdmFranchiseController@fransearch']);
Route::get('vdmFranchises/users', ['uses' => 'VdmFranchiseController@users']);
Route::get('vdmFranchises/buyAddress', ['uses' => 'VdmFranchiseController@buyAddress']);
Route::get('vdmFranchises/expiry', ['uses' => 'VdmFranchiseController@dateModification']);
Route::resource('vdmFranchises', 'VdmFranchiseController');
Route::post('vdmFranchises/updateAddCount', ['uses' => 'VdmFranchiseController@updateAddCount']);
Route::post('vdmFranchises/findFransList', ['uses' => 'VdmFranchiseController@findFransList']);
Route::post('vdmFranchises/findUsersList', ['uses' => 'VdmFranchiseController@findUsersList']);
Route::get('vdmFranchises/reports/{param}', ['uses' => 'VdmFranchiseController@reports']);
Route::post('vdmFranchises/updateReports', ['uses' => 'VdmFranchiseController@updateReports']);
Route::get('vdmFranchises/{param}/Sensoer', ['uses' => 'VdmFranchiseController@loadRemove']);
Route::get('vdmFranchises/EnableVehicleExpiry/{param}/{param1}', ['uses' => 'VdmFranchiseController@enableVehicleExpiry']);
Route::get('vehicle/getcount', ['uses' => 'VdmFranchiseController@liveVehicleCount']);

Route::get('notificationFrontend', ['uses' => 'VdmUserController@notificationFrontend']);
Route::post('notificationFrontendUpdate', ['uses' => 'VdmUserController@notificationFrontendUpdate']);
 // for scheduled reports
Route::post('ScheduledController/reportScheduling', ['uses' => 'ScheduledController@reportScheduling']);
Route::post('ScheduledController/reportScheduling2', ['uses' => 'ScheduledController@reportScheduling2']);
Route::get('ScheduledController/getRepName', ['uses' => 'ScheduledController@getRepName']);
Route::get('ScheduledController/getValue', ['uses' => 'ScheduledController@getValue']);
Route::get('ScheduledController/getValue2', ['uses' => 'ScheduledController@getValue2']);
Route::get('ScheduledController/reportDelete', ['uses'=>'ScheduledController@reportDelete']);
Route::get('ScheduledController/reportDelete2', ['uses'=>'ScheduledController@reportDelete2']);

// invoke from javascript
Route::get('storeOrgValues/val', ['uses' => 'VdmOrganizationController@storedOrg']);
Route::get('storeOrgValues/editRoutes', ['uses' => 'VdmOrganizationController@_editRoutes']);
Route::get('storeOrgValues/deleteRoutes', ['uses' => 'VdmOrganizationController@_deleteRoutes']);
Route::get('storeOrgValues/mapHistory',['uses'=>'VdmOrganizationController@_mapHistory']);
Route::get('VdmOrg/getStopName',['uses'=>'VdmOrganizationController@getStopName']);

//arun Route::post('ScheduledController/reportScheduling', ['uses' => 'ScheduledController@reportScheduling']);
Route::post('VdmGroup/removingGroup', ['uses'=>'VdmGroupController@removeGroup']);
Route::post('VdmGroup/showGroup', ['uses'=>'VdmGroupController@_showGroup']);
Route::post('VdmGroup/saveGroup', ['uses'=>'VdmGroupController@_saveGroup']);
Route::post('VdmGroup/groupId', ['uses'=>'VdmGroupController@groupIdCheck']);


Route::get('/setPOIName', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('setPOIName');
    return View('vls.setPOIName');
});

Route::get('file/download', function() {
    $file = 'path_to_my_file.pdf';
    return Response::download($file);
});

Route::get('/store', function() {
    return View('vls.geoCode');
});

Route::get('/download', function() {
    // PDF file is stored under project/public/download/info.pdf
    $file = public_path() . "/reports/sample.txt.gz";
    $headers = ['Content-Type: application/zip'];
    return Response::download($file, 'sample.txt.gz', $headers);
});

Route::get('/SmsReport',['uses'=>'SmsReportController@testSmsReport']);
Route::post('/SmsReport',['uses'=>'SmsReportController@testSmsReport']);
Route::get('/Test',['uses'=>'TestController@postAuth']);
Route::get('/Example',['uses'=>'ExampleController@testExample']);
Route::get('/Hello',['uses'=>'HelloController@testHello']);
Route::post('/meenatest',['uses'=>'HelloController@meenatest']);

Route::post('/updateLogDays', ['uses' => 'VdmVehicleController@updateLogDays']);

Route::get('/getNodataReport', function() {
    if (!Auth::check()) {
        return Redirect::to('login');
    }
    Log::info('get Vehicle Nodata Reportt');

    return View('vls.getNodataReport');
});
// for audit
//Route::get('audit/{model}', ['uses' => 'VdmFranchiseController@auditShow']);
Route::get('audit/{model}', ['uses' => 'AuditController@auditShow']);



Route::get('/getFmsDetails', function() {
   if (!Auth::check()) {
       return Redirect::to('login');
   }
   Log::info('getFmsDetails');
   return View('vls.getFmsDetails');
});
Route::get('preference', function() {
   return View('reports.preference');
});
Route::get('/showUserPre', ['uses' => 'UserPrePageController@userPreference']);
Route::post('/updateUserPre', ['uses' => 'UserPrePageController@updateUserPreference']);
Route::get('/getFileNames', ['uses'=>'UserPrePageController@getFileNames']);
//Route::post('/updateUserPre', ['uses' => 'UserPrePageController@updateUserPreference']);
Route::get('/fuelConsolidTime', function(){
   if(!Auth::check()){
       return Redirect::to('login');
   }
 return View('reports.fuelConsolidateTime');
});
Route::get('/getConsolidatedFuelReportBasedOnTime', function() {
   if (!Auth::check()) {
       return Redirect::to('login');
   }
   Log::info('getConsolidatedFuelReportBasedOnTime');
 return View('vls.getConsolidatedFuelReportBasedOnTime');
});

Route::get('/Hindi', function() {
    App::setLocale('hi');
    Session::put('phplang',"hi");
     return Redirect::back();
});
Route::get('/English', function() {
     App::setLocale('en');
     Session::put('phplang',"en");
      return Redirect::back();
});
Route::get('/pdfView', function() {
    return View('pdfviewer.pdfView');
});
// Route::any('form-submit', function(){
//     if(Input::file('file')==null){
//          Session::flash ( 'message', 'Please Select File ... ' );
//        return Redirect::back();
//     }
//   else{
//     $path=Input::file('file')->move(__DIR__.'/views/ReleaseNotes/',Input::file('file')->getClientOriginalName());
//     //return "Uploaded Successfully";
//      Session::flash ( 'message', 'Uploaded Successfully... ' );
//       return Redirect::back();
//    }
// });
Route::any('form-submit', ['uses' => 'UploadController@UploadPDFfile']);
