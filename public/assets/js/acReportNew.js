app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global','$translate',function($scope, $http, vamoservice, $filter, GLOBAL,$translate){
   var language=localStorage.getItem('lang');
    $scope.multiLang=language;
  $translate.use(language);
  var translate = $filter('translate');
  var licenceExpiry="";
  var strExpired='expired';
  if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
  $scope.errMsg="";
   //alert("hello");
   $scope.dealerName     =  localStorage.getItem('dealerName');
   $scope.filterDuration=180000;
   $scope.dealerFilter   =  false;  
   if($scope.dealerName == 'FUELVIEW') {
     $scope.dealerFilter = true;  
   }
   //alert($scope.dealerFilter);
   $scope.todaymillisec=new Date().getTime();
   var tab = getParameterByName('tn');
   $scope.drSenReport=false;
   $scope.trvShow   =  localStorage.getItem('trackNovateView');
   
    if( tab == "ac" ){

      $scope.acReportShow   =  true; 
      $scope.prmEngineShow  =  false; 

      $scope.reportNam      =  translate("ac_rep");
      $scope.reportTdNam    =  translate("AC");
      $scope.dowloadId      =  "acReport";

    } 
    else if( tab == "drSensor" ){

      $scope.acReportShow   =  true; 
      $scope.prmEngineShow  =  false; 

      $scope.reportNam      =  'Door Sensor';
      $scope.reportTdNam    =  'Door';
      $scope.dowloadId      =  "Door_Sensor_Report";
      $scope.drSenReport=true;

    } else if( tab == "engine" ){

      $scope.acReportShow   =  true;
      $scope.prmEngineShow  =  false;
       
      $scope.reportNam      =  translate("sec_eng_rep");
      $scope.reportTdNam    =  translate("Engine");
      $scope.dowloadId      =  "secondary_EngineOnReport";
      
    } else if( tab == "prmEngine" ){

      $scope.acReportShow   =  false;
      $scope.prmEngineShow  =  true;

        $scope.reportNam      =  translate("pri_eng_rep");
        $scope.reportTdNam    =  translate("Engine");
        $scope.dowloadId      =  "primary_EngineOnReport";

    }

    $scope.durVarTot  = "-";
    $scope.durVarTot2 = "-";

  //global declaration
  $scope.uiDate   =  {};
  $scope.uiValue  =  {};
    $scope.sort     =  sortByDate('alarmTime');

  function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  //global declartion
  $scope.locations = [];
  $scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gIndex =0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
    $scope.trimColon = function(textVal) {

      if(textVal){
       var spltVal = textVal.split(":");
       return spltVal[0];
      }
    }

  function sessionValue(vid, gname,licenceExpiry){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    localStorage.setItem('licenceExpiry', licenceExpiry);
    $("#testLoad").load("../public/menu");
  }
  
  function getTodayDate(date) {
      var date = new Date(date);
      return date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2);
    };

    function convert_to_24h(time_str) {
    //console.log(time_str);
    var str   = time_str.split(' ');
    var stradd  = str[0].concat(":00");
    var strAMPM = stradd.concat(' '+str[1]);
    var time = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
      var hours = Number(time[1]);
      var minutes = Number(time[2]);
      var seconds = Number(time[2]);
      var meridian = time[4].toLowerCase();
  
      if (meridian == 'p' && hours < 12) {
        hours = hours + 12;
      }
      else if (meridian == 'a' && hours == 12) {
        hours = hours - 12;
      }     
      var marktimestr =''+hours+':'+minutes+':'+seconds;      
      return marktimestr;
    };

    // millesec to day, hours, min, sec
    $scope.msToTime = function(ms) 
    {
        days = Math.floor(ms / (24 * 60 * 60 * 1000));
      daysms = ms % (24 * 60 * 60 * 1000);
    hours = Math.floor((ms) / (60 * 60 * 1000));
    hoursms = ms % (60 * 60 * 1000);
    minutes = Math.floor((hoursms) / (60 * 1000));
    minutesms = ms % (60 * 1000);
    seconds = Math.floor((minutesms) / 1000);
    // if(days==0)
    //  return hours +" h "+minutes+" m "+seconds+" s ";
    // else
    return hours +":"+minutes+":"+seconds;
  }
    
  var delayed4 = (function () {
      var queue = [];

      function processQueue() {
        if (queue.length > 0) {
          setTimeout(function () {
            queue.shift().cb();
            processQueue();
          }, queue[0].delay);
        }
      }

      return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
          processQueue();
        }
      };
  }());

    function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
      vamoservice.getDataCall(tempurlEvent).then(function(data) {
      $scope.addressEvent[index4] = data.results[0].formatted_address;
      //console.log(' address '+$scope.addressEvent[index4])
      // var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
    })
  };

  $scope.recursiveEvent   =   function(locationEvent, indexEvent)
  {
    var index4 = 0;
    angular.forEach(locationEvent, function(value ,primaryKey){
      //console.log(' primaryKey '+primaryKey)
      index4 = primaryKey;
      if(locationEvent[index4].address == undefined)
      {
        var latEvent     =  locationEvent[index4].latitude;
        var lonEvent     =  locationEvent[index4].longitude;
        var tempurlEvent =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";
        delayed4(2000, function (index4) {
              return function () {
                google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
              };
            }(index4));
      }
    })
  }

  $scope.msToTime2   = function(ms) {
      days = Math.floor(ms / (24*60*60*1000));
      daysms=ms % (24*60*60*1000);
      hours = Math.floor((ms)/(60*60*1000));
      hoursms=ms % (60*60*1000);
      minutes = Math.floor((hoursms)/(60*1000));
      minutesms=ms % (60*1000);
      sec = Math.floor((minutesms)/(1000));

      return hours+"h : "+minutes+"m : "+sec+"s";

    /*  if(days>0) {
        return days+"d : "+hours+"h : "+minutes+"m : "+sec+"s";
      } else {
        return hours+"h : "+minutes+"m : "+sec+"s";
      }*/
      //return hours+":"+minutes+":"+sec;
    }


  function formatAMPM(date) {
        var date = new Date(date);
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
  }

    //get the value from the ui
  function getUiValue(){
      $scope.uiDate.fromdate   =  $('#dateFrom').val();
      $scope.uiDate.fromtime   =  $('#timeFrom').val();
      $scope.uiDate.todate     =  $('#dateTo').val();
      $scope.uiDate.totime     =  $('#timeTo').val();
      if(localStorage.getItem('timeTochange')!='yes'){
      updateToTime();
      $scope.uiDate.totime    =   localStorage.getItem('toTime');
      }
  
  }

  function getTodayDates() {
      var date = new Date();
      return date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2);
    };


  function _pairFilter2(data) { 

      console.log('pair filter 2..');

      var ign_On     =  0;
      var durVar     =  0;
      var durVarTot  =  0;
      var lastOnId;

      var ret_Arr        =  [];
      var toDateTimeVal  =  utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));  
      var remdata=false;
      
        for(var i=0; i<data.length; i++) {

            if(data[i].alarmType == "A/C ON") {
           if(ign_On==0){

                 console.log(i+' '+'ON');

                 durVar = durVar + data[i].alarmTime;
                 if(!remdata){
                 ret_Arr.push(data[i]);}

                 ign_On=1;
               }

        } else if(data[i].alarmType == "A/C OFF") {
           
          if(ign_On==1) {

            ret_Arr.push(data[i]);

          remdata=false;
          if($scope.dealerFilter)
          {
          if(ret_Arr.length>=2){
                if((ret_Arr[ret_Arr.length-1].alarmTime-ret_Arr[ret_Arr.length-2].alarmTime)<=$scope.filterDuration){
                //alert('pop');
                ret_Arr.pop();
                remdata=true;
                //remdata=true;
                 }
               }  
             }

            durVarTot  =  durVarTot + ( data[i].alarmTime - durVar );

            lastOnId = i+1;

                   console.log(i+' '+'A/C OFF');

                   durVar = 0;

                  ign_On = 0;  
                }
        }

        }


        var ignVar    = 0;
        var onInit    = 0;
        var secTol = 0;

         for(var i=0; i<ret_Arr.length; i++) {

            if(ret_Arr[i].alarmType == "A/C ON" ) {

               if(onInit==0) {
                  ignVar=ignVar+ret_Arr[i].alarmTime;
                  onInit=1;
               }  

            } else if(ret_Arr[i].alarmType == "A/C OFF") {

                if(onInit==1) {
                  secTol = secTol+(ret_Arr[i].alarmTime-ignVar);
                  ignVar    = 0;
                  onInit    = 0;
                }
            }
       }    
            //alert(secTol);
            durVarTot=secTol; 
            if(onInit==1) {
               ret_Arr.push({alarmType:"A/C ONN",alarmTime:toDateTimeVal});
               durVarTot=secTol+(toDateTimeVal-ret_Arr[ret_Arr.length-2].alarmTime);
            }
        $scope.durVarTot2  =  $scope.msToTime2(durVarTot);
           if(ign_On==1) {
               ret_Arr.push({alarmType:"A/C ONN",alarmTime:toDateTimeVal});
            }

        console.log(data);
         console.log(ret_Arr);    
      
      return ret_Arr; 
    }


    function _pairFilter(data) {

      console.log('pair filter..');
      var ign_On    = 0;
      var lastOnId;
      var remdata=false;
      var durVar     =  0;
      var durVarTot  =  0;
      var remdata=false;
      var ret_Arr       = [];
      var toDateTimeVal = utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));  

      
        for(var i=0; i<data.length; i++) {

            if(data[i].ignitionStatus == "ON") {

           if(ign_On==0) {

                 //console.log(i+' '+'ON');

                  ret_Arr.push(data[i]);
                 if(remdata==true&&ret_Arr.length>=2) {
                  
                 if((ret_Arr[ret_Arr.length-1].date-ret_Arr[ret_Arr.length-2].date)<=$scope.filterDuration){
                               ret_Arr.pop();
                               ret_Arr.pop();
                                }
                                else if(ret_Arr.length>=3){
                                  ret_Arr.pop();
                                  ret_Arr.pop();
                                  ret_Arr.pop();
                                  ret_Arr.push(data[i]);
                                }
               }

                  if(ret_Arr.length>=2&&remdata==false){
                               if((ret_Arr[ret_Arr.length-1].date-ret_Arr[ret_Arr.length-2].date)<=$scope.filterDuration){
                               //alert('pop');
                               ret_Arr.pop();
                               ret_Arr.pop();
                              
                               //remdata=true;
                                }
                        }  

                
                 ign_On = 1;
               }

        } else if(data[i].ignitionStatus == "OFF") {

          if(ign_On==1) {

            ret_Arr.push(data[i]);
            remdata=false;
            if(ret_Arr.length>=2){
                               if((ret_Arr[ret_Arr.length-1].date-ret_Arr[ret_Arr.length-2].date)<=$scope.filterDuration){
                               if(data.length==(i+1)){
                                ret_Arr.pop();
                               }
                               remdata=true;
                                }
                        }  

                   //console.log(i+' '+'OFF');
                  ign_On=0;  
                }
        }

        }

      if(ret_Arr.length){ 
        if(ret_Arr[ret_Arr.length-1].ignitionStatus=="OFF"){

         if(ret_Arr.length>=2){
                               if((ret_Arr[ret_Arr.length-1].date-ret_Arr[ret_Arr.length-2].date)<=$scope.filterDuration){
                               
                                ret_Arr.pop();
                                ret_Arr.pop();
                               
                                }
                        }  

        }
      }

         var ignVar    = 0;
         var onInit    = 0;
         var priTol = 0;
 
          for(var i=0; i<ret_Arr.length; i++) {
 
             if(ret_Arr[i].ignitionStatus=="ON" ) {
 
                if(onInit==0) {
                   ignVar=ignVar+ret_Arr[i].date;
                   onInit=1;
                }  
 

             } else if(ret_Arr[i].ignitionStatus=="OFF") {
 

                 if(onInit==1) {
                   priTol = priTol+(ret_Arr[i].date-ignVar);
                   ignVar    = 0;
                   onInit    = 0;
                 }
             }
        }
             durVarTot=priTol;
            if(onInit==1) {
                ret_Arr.push({ignitionStatus:"ONN",date:toDateTimeVal});
                durVarTot=priTol+(toDateTimeVal-ret_Arr[ret_Arr.length-2].date);
             }
             console.log(durVarTot);
             $scope.durVarTot  =  $scope.msToTime(durVarTot);
  
      
      return ret_Arr; 
    }

  
    function filterData(data){

        //console.log(data);
            var retArr = [];
            var toDateval = utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));  

                    angular.forEach(data.alarmList, function(val, key){

                        if(data.alarmList.length-1 == key) {

                          retArr.push({address:val.address, alarmTime:val.alarmTime, alarmType:val.alarmType, lat:val.lat, lng:val.lng});
                               retArr.push({ alarmTime:toDateval, alarmType:"ONN"});

                        } else {

                            retArr.push({address:val.address, alarmTime:val.alarmTime, alarmType:val.alarmType, lat:val.lat, lng:val.lng});
                         }
                    });

                 var retArrVal = [{alarmList:retArr,error:data.error,fcode:data.fcode,fromTime:data.fromTime,orgId:data.orgId,toTime:data.toTime,vehicleId:data.vehicleId,vehicleName:data.vehicleName}];

        //console.log(retArrVal);

      return retArrVal[0];  
    }

 
$scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
 $scope.todayDisabled = true;
$scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate       = getTodayDate(now.setDate(now.getDate() - 1));
switch(duration){
  
  case 'yesterday':
$scope.yesterdayDisabled = true;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
 $scope.todayDisabled = false;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 1));
  $scope.uiDate.totime    =   '11:59 PM';
  webCall();
  break;
  case 'lastweek':
$scope.yesterdayDisabled = false;
 $scope.weekDisabled = true;
 $scope.monthDisabled = false;
 $scope.todayDisabled = false;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 8));
  $scope.uiDate.totime    =   '11:59 PM';
  webCall();
  break;
  case 'month':
$scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = true;
 $scope.todayDisabled = false;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 31));
  $scope.uiDate.totime    =   '11:59 PM';
  webCall();
  break;
  case 'today':
$scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
 $scope.todayDisabled = true;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
  $scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
  $scope.uiDate.totime    =   formatAMPM(d);
  webCall();
  break;

}
}
  
  //var initAcVal = 0;

    function webCall(){

      var acUrl='';
      $scope.interval='';

      if( tab == 'ac' || tab == 'engine'|| tab == 'drSensor' ) {

         if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {
            acUrl = GLOBAL.DOMAIN_NAME+'/getAcReport?vehicleId='+$scope.vehIds+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
         }

        } else if( tab == 'prmEngine' ) {
          
           //acUrl = 'http://209.97.163.4:9000/getPrimaryEngineReport?vehicleId=SENTINI_AP16TA5414&userId=SENTINI&fromDateTime=1535991774000&toDateTime=1536078189000';
             acUrl =  GLOBAL.DOMAIN_NAME+'/getPrimaryEngineReport?vehicleId='+$scope.vehIds+'&fromDateTime='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateTime='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
       
       /* if(initAcVal==0){
              initAcVal++;
              $scope.fromTimes  =  "00:00:00";
              $scope.fromDates  =  getTodayDates();       
              acUrl   =  GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+$scope.vehIds+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromDates,$scope.fromTimes);
          }else{
              acUrl   =  GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+$scope.vehIds+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUTC='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
          } */
            
        }

      console.log(acUrl);

        $scope.acData       =  [];
        var ignitionValue   =  [];
        $scope.acData2      =  [];
        if((licenceExpiry.indexOf(strExpired)== -1)||licenceExpiry==""||licenceExpiry=="-"){
        $http.get(acUrl).success(function(data){
        
            if( tab == 'prmEngine' ) {

              $scope.acData2   =  data;

              console.log(data);

              if(data.vehicleLocations!=null){
                 ignitionValue    =  ($filter('filter')(data.vehicleLocations, {'ignitionStatus': "!null"}));
                 $scope.acData    =  _pairFilter(ignitionValue);
              }

                console.log($scope.acData);
           } else {

            console.log('accc');

             console.log(data);

               $scope.acData2   =  data;

               if(data.alarmList!=null){

                /*if( data.alarmList.length%2 == 0 ) {
                   $scope.acData = data;
                } else {
                   $scope.acData = filterData(data);
                }*/

                $scope.acData    =  _pairFilter2(data.alarmList);

            }
           }
           //console.log($scope.acData);
          stopLoading();
    }); 
   }
   else{
            stopLoading();
            $scope.errMsg=licenceExpiry;
            $scope.showErrMsg = true;
      } 
 }


  // initial method
  $scope.$watch("url", function (val) {
    vamoservice.getDataCall($scope.url).then(function(data ) {
           
    //startLoading();
      $scope.selectVehiData = [];
      $scope.vehicle_group=[];
      $scope.vehicle_list = data;

      if(data.length) {
        $scope.vehiname = getParameterByName('vid');
        $scope.uiGroup  = $scope.trimColon(getParameterByName('vg'));
        $scope.gName    = getParameterByName('vg');
        angular.forEach(data, function(val, key) {
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
          if($scope.gName == val.group) {
            $scope.gIndex = val.rowId;
 
            angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys) {

              $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

              if($scope.vehiname == value.vehicleId) {
                  $scope.shortNam = value.shortName;
                    $scope.vehIds   = value.vehicleId;
                    licenceExpiry=value.licenceExpiry;
                }
            })
            
          }
          });

      //console.log($scope.selectVehiData);
        sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
      }
      
        $scope.uiDate.fromdate    = localStorage.getItem('fromDate');
        $scope.uiDate.fromtime    = localStorage.getItem('fromTime');
        $scope.uiDate.todate    = localStorage.getItem('toDate');
        $scope.uiDate.totime    =  localStorage.getItem('toTime');
        if(localStorage.getItem('timeTochange')!='yes'){
                updateToTime();
                $scope.uiDate.totime    =   localStorage.getItem('toTime');
                }
    
        startLoading();
        webCall();
      //stopLoading();
    }); 
  });

    
   
    $scope.groupSelection   = function(groupName, groupId) {
    startLoading();
    licenceExpiry="";
    $scope.errMsg="";
    $scope.durVarTot  = "-";
        $scope.durVarTot2 = "-";
    $scope.gName  =   groupName;
    $scope.uiGroup  =   $scope.trimColon(groupName);
    $scope.gIndex = groupId;
    var url     =   GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

    vamoservice.getDataCall(url).then(function(response){
    
      $scope.vehicle_list = response;
      $scope.shortNam     = response[$scope.gIndex].vehicleLocations[0].shortName;
      $scope.vehiname     = response[$scope.gIndex].vehicleLocations[0].vehicleId;
      licenceExpiry   = response[$scope.gIndex].vehicleLocations[0].licenceExpiry;
      sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
            $scope.selectVehiData=[];
            //console.log(response);
              angular.forEach(response, function(val, key){
          if($scope.gName == val.group){
          //  $scope.gIndex = val.rowId;
                        angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys){

                            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

              if($scope.vehiname == value.vehicleId){
                  $scope.shortNam = value.shortName;
                  $scope.vehIds   = value.vehicleId;
                }
            })
            }
        })

      getUiValue();
      if((licenceExpiry!="-")&&(licenceExpiry.indexOf(strExpired)!= -1)){
           stopLoading();
           $scope.errMsg=licenceExpiry;
        }else{
      webCall();
    }
      //stopLoading();
    });

  }


  $scope.genericFunction  = function (vehid, index,shortname,position, address,groupName,licenceExp){
    
         licenceExpiry=licenceExp;
         $scope.errMsg="";
         $scope.durVarTot  = "-";
         $scope.durVarTot2 = "-";
         $scope.vehiname = vehid;
    
          startLoading();
          sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
          angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
            if(vehid == val.vehicleId){
              $scope.shortNam = val.shortName;
                $scope.vehIds   = val.vehicleId;
            }
          });
          if((licenceExpiry!="-")&&(licenceExpiry.indexOf(strExpired)!= -1)){
            stopLoading();
          $scope.acData       =  [];
          $scope.errMsg=licenceExpiry;
          }else{
                getUiValue();
                webCall();
          }
  }

    $scope.submitFunction   = function(){
 $scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
  $scope.todayDisabled = false;
  if((licenceExpiry.indexOf(strExpired)== -1)||licenceExpiry==""||licenceExpiry=="-"){
      startLoading();
      getUiValue();
      webCall();
    }else{
            $scope.errMsg=licenceExpiry;
      } 
  //webServiceCall();
    //stopLoading();
  }

  $scope.exportData = function (data) {
    // console.log(data);
    var blob = new Blob([document.getElementById(data).innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, data+".xls");
    };

    $scope.exportDataCSV = function (data) {
    // console.log(data);
    CSV.begin('#'+data).download(data+'.csv').go();
    };

  $('#minus').click(function(){
    $('#menu').toggle(1000);
  })

}]);
