app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', function($scope, $http, vamoservice, $filter, GLOBAL){
	
  //global declaration
	$scope.uiDate 		=	{};
	$scope.uiValue	 	=  	{};
  //$scope.sort         =   sortByDate('startTime');
    $scope.totalDur     =   0;
    $scope.filterDuration=180000;
    var tab = getParameterByName('tn');

    var assLabel         =   localStorage.getItem('isAssetUser');
  //console.log(assLabel);
   
	if( assLabel == "true" ) {
	    $scope.vehiLabel  =  "Asset";
	    $scope.vehiImage  =  true;
	} else if( assLabel == "false" ) {
	    $scope.vehiLabel  =  "Vehicle";
	    $scope.vehiImage  =  false;
	} else {
	    $scope.vehiLabel  =  "Vehicle";
	    $scope.vehiImage  =  false;
	}
       
	function getParameterByName(name) {
    	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	//global declartion
	$scope.locations = [];
	$scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
	$scope.gIndex =0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
	$scope.trimColon = function(textVal){
		return textVal.split(":")[0].trim();
	}

	function sessionValue(vid, gname){
		localStorage.setItem('user', JSON.stringify(vid+','+gname));
		$("#testLoad").load("../public/menu");
	}
	
	function getTodayDate(date) {
     	var date = new Date(date);
    	return date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2);
    };

    function convert_to_24h(time_str) {
		//console.log(time_str);
 		var str		=	time_str.split(' ');
 		var stradd	=	str[0].concat(":00");
 		var strAMPM	=	stradd.concat(' '+str[1]);
 		var time = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
	    var hours = Number(time[1]);
	    var minutes = Number(time[2]);
	    var seconds = Number(time[2]);
	    var meridian = time[4].toLowerCase();
	
	    if (meridian == 'p' && hours < 12) {
	      hours = hours + 12;
	    }
	    else if (meridian == 'a' && hours == 12) {
	      hours = hours - 12;
	    }	    
	    var marktimestr	=''+hours+':'+minutes+':'+seconds;	    
	    return marktimestr;
    };

    $scope.msToTime2 = function(ms) 
    {
    days = Math.floor(ms / (24 * 60 * 60 * 1000));
    daysms = ms % (24 * 60 * 60 * 1000);
    hours = Math.floor((daysms) / (60 * 60 * 1000));
    hoursms = ms % (60 * 60 * 1000);
    minutes = Math.floor((hoursms) / (60 * 1000));
    minutesms = ms % (60 * 1000);
    seconds = Math.floor((minutesms) / 1000);
    
   if(days>1){
     return days+":"+hours+":"+minutes+":"+seconds;
   } 
   else if(days==1){
     return days+":"+hours+":"+minutes+":"+seconds;
   }
   else if(days==0){
      return hours +":"+minutes+":"+seconds;
   }
  }

    // millesec to day, hours, min, sec
    $scope.msToTime = function(ms) 
    {
        days = Math.floor(ms / (24 * 60 * 60 * 1000));
	  	daysms = ms % (24 * 60 * 60 * 1000);
		hours = Math.floor((ms) / (60 * 60 * 1000));
		hoursms = ms % (60 * 60 * 1000);
		minutes = Math.floor((hoursms) / (60 * 1000));
		minutesms = ms % (60 * 1000);
		seconds = Math.floor((minutesms) / 1000);
		// if(days==0)
		// 	return hours +" h "+minutes+" m "+seconds+" s ";
		// else
	  return hours +":"+minutes+":"+seconds;
	}
   	
	var delayed4 = (function () {
  		var queue = [];

	  	function processQueue() {
		    if (queue.length > 0) {
		      setTimeout(function () {
		        queue.shift().cb();
		        processQueue();
		      }, queue[0].delay);
		    }
	  	}

	  	return function delayed(delay, cb) {
	    	queue.push({ delay: delay, cb: cb });

	    	if (queue.length === 1) {
	      	processQueue();
	    	}
	  	};
	}());

   	function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
   		vamoservice.getDataCall(tempurlEvent).then(function(data) {
			$scope.addressEvent[index4] = data.results[0].formatted_address;
			//console.log(' address '+$scope.addressEvent[index4])
			// var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
		})
	};

	$scope.recursiveEvent 	= 	function(locationEvent, indexEvent)
	{
		var index4 = 0;
		angular.forEach(locationEvent, function(value ,primaryKey){
			//console.log(' primaryKey '+primaryKey)
			index4 = primaryKey;
			if(locationEvent[index4].address == undefined)
			{
				var latEvent		 =	locationEvent[index4].latitude;
			 	var lonEvent		 =	locationEvent[index4].longitude;
				var tempurlEvent =	"https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";
				delayed4(2000, function (index4) {
				      return function () {
				        google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
				      };
				    }(index4));
			}
		})
	}


	function formatAMPM(date) {
    	  var date = new Date(date);
		  var hours = date.getHours();
		  var minutes = date.getMinutes();
		  var ampm = hours >= 12 ? 'PM' : 'AM';
		  hours = hours % 12;
		  hours = hours ? hours : 12; // the hour '0' should be '12'
		  minutes = minutes < 10 ? '0'+minutes : minutes;
		  var strTime = hours + ':' + minutes + ' ' + ampm;
		  return strTime;
	}


 
  /*  function _pairFilter(data) {

    	console.log('pair filter..');

    	var ign_On    = 0;
    	var lastOnId;

    	var durVar     =  0;
    	var durVarTot  =  0;

    	var ret_Arr       = [];
    	var toDateTimeVal = utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));  

    	
        for(var i=0; i<data.length; i++) {

            if(data[i].ignitionStatus == "ON") {

		       if(ign_On==0) {

                 //console.log(i+' '+'ON');

                 ret_Arr.push(data[i]);

                 durVar = durVar + data[i].date;
                 ign_On = 1;
               }

		    } else if(data[i].ignitionStatus == "OFF") {

		    	if(ign_On==1) {

		    		ret_Arr.push(data[i]);

		    		durVarTot  =  durVarTot + ( data[i].date - durVar );

		    		lastOnId=i+1;

		    		durVar = 0;

                   //console.log(i+' '+'OFF');
                  ign_On=0;  
                }
		    }

        }

        console.log(durVarTot);

          if(ign_On == 1){
             durVarTot  = toDateTimeVal-durVar;
                
          }


        $scope.durVarTot  =  $scope.msToTime(durVarTot);

//       console.log(ign_On);
      //    console.log(data.length-1);
            

            if(ign_On==1) {
               ret_Arr.push({ignitionStatus:"ONN",date:toDateTimeVal});
            }

     //    console.log(data);
        //   console.log(ret_Arr);    
      
      return ret_Arr; 
    }



*/









    function _pairFilter(data) {

    	//console.log('pair filter..');

    	console.log( data );
        var remdata=false;
        var lastOnId;
    	var ign_On          =   0;
    	var durVar          =   0;
    	var durVarTot       =   0;
    	var ret_Arr         =   [];
    	var toDateTimeVal   =   utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));  

    	
        for(var i=0; i<data.length; i++) {
            remdata=false;
        	console.log( data[i].vehicleId );
        	  console.log( data[i].vehicleName );
        	    //console.log(data[i].getEngineData.length);

        	    ret_Arr.push( { vehicleId: data[i].vehicleId, vehicleName: data[i].vehicleName });

        	    ret_Arr[i].getEngineData  =  [];

        	if(data[i].getEngineData!=null) {

           	    for(var j=0; j<data[i].getEngineData.length; j++) {


                    if( data[i].getEngineData[j].ignitionStatus == "ON" ) {

		                if(ign_On==0) {

        		            console.log( data[i].getEngineData[j].ignitionStatus );

                           ret_Arr[i].getEngineData.push({   address: data[i].getEngineData[j].address , dateTime: data[i].getEngineData[j].dateTime, ignitionStatus: data[i].getEngineData[j].ignitionStatus });
        		  if(remdata==true&&ret_Arr[i].getEngineData.length>=2) {
                  
			                 if((ret_Arr[i].getEngineData[ret_Arr[i].getEngineData.length-1].dateTime-ret_Arr[i].getEngineData[ret_Arr[i].getEngineData.length-2].dateTime)<=$scope.filterDuration){
			                               ret_Arr[i].getEngineData.pop();
			                               ret_Arr[i].getEngineData.pop();
			                                }
			                                else if(ret_Arr[i].getEngineData.length>=3){
			                                  ret_Arr[i].getEngineData.pop();
			                                  ret_Arr[i].getEngineData.pop();
			                                  ret_Arr[i].getEngineData.pop();
			                                  ret_Arr[i].getEngineData.push({   address: data[i].getEngineData[j].address , dateTime: data[i].getEngineData[j].dateTime, ignitionStatus: data[i].getEngineData[j].ignitionStatus });
			                                }
			               }


                            if(remdata==false&&ret_Arr[i].getEngineData.length>=2) {
        		            //ret_Arr[i].getEngineData.push({   address: data[i].getEngineData[j].address , dateTime: data[i].getEngineData[j].dateTime, ignitionStatus: data[i].getEngineData[j].ignitionStatus });
		                            if(ret_Arr[i].getEngineData.length>=2){
		                               if((ret_Arr[i].getEngineData[ret_Arr[i].getEngineData.length-1].dateTime-ret_Arr[i].getEngineData[ret_Arr[i].getEngineData.length-2].dateTime)<=$scope.filterDuration){
		                               //alert('pop');
		                               ret_Arr[i].getEngineData.pop();
		                               ret_Arr[i].getEngineData.pop();
		                               
		                                }
		                            }  
                            }
                            durVar   =  durVar + data[i].getEngineData[j].dateTime;
                            ign_On   =  1; 

                        }

                    } else if( data[i].getEngineData[j].ignitionStatus == "OFF" ) {

		                if(ign_On==1) {

		                	  console.log( data[i].getEngineData[j].ignitionStatus );

                              ret_Arr[i].getEngineData.push({   address: data[i].getEngineData[j].address , dateTime: data[i].getEngineData[j].dateTime, ignitionStatus: data[i].getEngineData[j].ignitionStatus });

					    	  durVarTot  =  durVarTot + ( data[i].getEngineData[j].dateTime - durVar );
					    	  lastOnId   =  i+1;
					    	  durVar     =  0;

					    	  remdata=false;
				              if(ret_Arr[i].getEngineData.length>=2){
				                               if((ret_Arr[i].getEngineData[ret_Arr[i].getEngineData.length-1].dateTime-ret_Arr[i].getEngineData[ret_Arr[i].getEngineData.length-2].dateTime)<=$scope.filterDuration){
					                                  if(ret_Arr[i].getEngineData.length==(i+1)){
						                                ret_Arr[i].getEngineData.pop();
						                               }
				                               remdata=true;
				                                }
				                        }  

			                //console.log(i+' '+'OFF');
			                  ign_On     =  0; 
		                }
		            }

                }
                if(ret_Arr[i].getEngineData[ret_Arr[i].getEngineData.length-1].ignitionStatus=="OFF"){

                if(ret_Arr[i].getEngineData.length>=2){
                                if((ret_Arr[i].getEngineData[ret_Arr[i].getEngineData.length-1].dateTime-ret_Arr[i].getEngineData[ret_Arr[i].getEngineData.length-2].dateTime)<=$scope.filterDuration){
                                ret_Arr[i].getEngineData.pop();
                                ret_Arr[i].getEngineData.pop();
                               
                                }
                        }  
                    }
            }

            //console.log(durVarTot);

		        if(ign_On == 1) {
		            durVarTot  = toDateTimeVal-durVar;
		        }

		        $scope.durVarTot  =  $scope.msToTime(durVarTot);

		        //console.log(ign_On);
		           // console.log(data.length-1);
		            
		        if(ign_On==1) {
		            ret_Arr[i].getEngineData.push({ignitionStatus:"ONN", dateTime:toDateTimeVal});
		        }

		        ign_On        =   0;
            	durVar        =   0;
    	        durVarTot     =   0;
        }
     
         //console.log(data);
            console.log(ret_Arr);   

     return ret_Arr; 
    }


$scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
$scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate       = getTodayDate(now.setDate(now.getDate() - 1));
switch(duration){
  
  case 'yesterday':
 $scope.yesterdayDisabled = true;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 1));
  $scope.uiDate.totime		=	localStorage.getItem('toTime');
  webCall();
  break;
  case 'today':
 $scope.yesterdayDisabled = false;
 $scope.weekDisabled = true;
 $scope.monthDisabled = false;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
  $scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
  $scope.uiDate.totime       = formatAMPM(d);
  webCall();
  break;
  case 'month':
 $scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = true;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 31));
  webCall();
  break;

}
}


    function webCall() {

    	var urlAllow    =  true; 
        var fromTms     =  utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime));
        var toTms       =  utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
        var totalTms    =  toTms-fromTms;
                       
        var splitTimes  =  $scope.msToTime2(totalTms).split(':');
        var daysDiff    =  0;

          if(splitTimes.length==4) {
          	daysDiff = splitTimes[0];
		  }
                        
      //alert(daysDiff);

        if(daysDiff < 4){

	        if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {        
	           var conPriEngUrl   =  GLOBAL.DOMAIN_NAME+'/getConsolidatedPrimaryEngineReport?groupId='+$scope.gName+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
	         //var conPriEngUrl   =  'http://128.199.159.130:9000/getConsolidatedPrimaryEngineReport?userId=SANGHI&groupId=SANGHI:SMP&fromTimeUtc=1532346665000&toTimeUtc=1532519495000';
	        }

	        console.log( conPriEngUrl );

	        $scope.conPriEngData  = [];

	        $http.get( conPriEngUrl ).success(function(data) {

	         $scope.conPriEngData = _pairFilter(data);

	            //$scope.conPriEngData  =  data;

	             // console.log( $scope.conPriEngData );
	          
	          stopLoading();
			}); 

      } else {

          alert("Please select less than 4 days.");

          $scope.conPriEngData  = [];
          stopLoading();
      }

    }



	//get the value from the ui
	function getUiValue(){
		$scope.uiDate.fromdate 		=	$('#dateFrom').val();
	  	$scope.uiDate.fromtime		=	$('#timeFrom').val();
	  	$scope.uiDate.todate		=	$('#dateTo').val();
	  	$scope.uiDate.totime 		=	$('#timeTo').val();
 	
	}


// service call for the event report

/*	function webServiceCall(){
		$scope.siteData = [];
		if((checkXssProtection($scope.uiDate.fromdate) == true) && (checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true)) {
			
			var url 	= GLOBAL.DOMAIN_NAME+"/getActionReport?vehicleId="+$scope.vehiname+"&fromDate="+$scope.uiDate.fromdate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+$scope.uiDate.todate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&interval="+$scope.interval+"&stoppage="+$scope.uiValue.stop+"&stopMints="+$scope.uiValue.stopmins+"&idle="+$scope.uiValue.idle+"&idleMints="+$scope.uiValue.idlemins+"&notReachable="+$scope.uiValue.notreach+"&notReachableMints="+$scope.uiValue.notreachmins+"&overspeed="+$scope.uiValue.speed+"&speed="+$scope.uiValue.speedkms+"&location="+$scope.uiValue.locat+"&site="+$scope.uiValue.site+'&fromDateUTC='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUTC='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
			vamoservice.getDataCall(url).then(function(responseVal){
				$scope.recursiveEvent(responseVal, 0);
				$scope.eventData = responseVal;
				var entry=0,exit=0; 
				angular.forEach(responseVal, function(val, key){
					if(val.state == 'SiteExit')
						exit++ 
					else if (val.state == 'SiteEntry')
						entry++
				})
				$scope.siteEntry 	=	entry;
				$scope.siteExit 	=	exit;
				stopLoading();
			});
		}
		stopLoading();
	}*/

	// initial method

	$scope.$watch("url", function (val) {
		vamoservice.getDataCall($scope.url).then(function(data) {
           
          //startLoading();
            $scope.selectVehiData = [];
			$scope.vehicle_group=[];
			$scope.vehicle_list = data;

			if(data.length){
				$scope.vehiname	= getParameterByName('vid');
				$scope.uiGroup 	= $scope.trimColon(getParameterByName('vg'));
				$scope.gName 	= getParameterByName('vg');
				angular.forEach(data, function(val, key){
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
					if($scope.gName == val.group){
						$scope.gIndex = val.rowId;
 
						angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

                            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

							if($scope.vehiname == value.vehicleId)
							$scope.shortNam	= value.shortName;
						})
						
					}
			    })

		  //console.log($scope.selectVehiData);
		    sessionValue($scope.vehiname, $scope.gName)
			}
			var dateObj 			 = 	new Date();
			$scope.fromNowTS		 =	new Date(dateObj.setDate(dateObj.getDate()-1));
			$scope.uiDate.fromdate 	 =	getTodayDate($scope.fromNowTS);
		  	$scope.uiDate.fromtime	 =	'12:00 AM';
		  	$scope.uiDate.todate	 =	getTodayDate($scope.fromNowTS);
		  //$scope.uiDate.totime 	 =	formatAMPM($scope.fromNowTS.getTime());
            $scope.uiDate.totime 	 =  '11:59 PM';
		  //webServiceCall();
		    startLoading();
		    webCall();
		  //stopLoading();
		});	
	});

    
   
  	$scope.groupSelection 	= function(groupName, groupId) {
		startLoading();
		$scope.gName 	 = 	groupName;
		$scope.uiGroup 	 = 	$scope.trimColon(groupName);
		$scope.gIndex	 =	groupId;
		var url  		 = 	GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

		vamoservice.getDataCall(url).then(function(response){
		
			$scope.vehicle_list = response;
			$scope.shortNam		= response[$scope.gIndex].vehicleLocations[0].shortName;
			$scope.vehiname		= response[$scope.gIndex].vehicleLocations[0].vehicleId;
			sessionValue($scope.vehiname, $scope.gName);
            $scope.selectVehiData=[];
            //console.log(response);
            	angular.forEach(response, function(val, key){
					if($scope.gName == val.group){
					//	$scope.gIndex = val.rowId;
                        angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys){

                            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

							if($scope.vehiname == value.vehicleId)
							$scope.shortNam	= value.shortName;
						})
				    }
				})

			getUiValue();
			webCall();
		  //webServiceCall();
	      //stopLoading();
		});

	}


/*	$scope.genericFunction 	= function (vehid, index){
		startLoading();
		$scope.vehiname		= vehid;
		sessionValue($scope.vehiname, $scope.gName)
		angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
			if(vehid == val.vehicleId)
				$scope.shortNam	= val.shortName;
		})
		getUiValue();
	//	webServiceCall();

	}*/

  	$scope.submitFunction 	=	function(){
 $scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
	  startLoading();
	  getUiValue();
	  webCall();
	//webServiceCall();
    //stopLoading();
	}

	$scope.exportData = function (data) {
		$(".deleteRows").remove();
		$(".removeDuration").remove();
		// console.log(data);
		var blob = new Blob([document.getElementById(data).innerHTML], {
           	type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, data+".xls");
    };

    $scope.exportDataCSV = function (data) {
		// console.log(data);
		CSV.begin('#'+data).download(data+'.csv').go();
    };

	$('#minus').click(function(){
		$('#menu').toggle(1000);
	})

}]);
