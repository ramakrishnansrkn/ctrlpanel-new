app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', function($scope, $http, vamoservice, $filter, GLOBAL){
//global declaration
  $scope.uiDate       =  {};
  $scope.uiValue      =  {};
  $scope.sort         =  sortByDate('startTime', false);
  $scope.interval     =  "";
  $scope.fuelDataMsg  =  "";
  var licenceExpiry="";
  var strExpired='expired';
  if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
  $scope.errMsg="";
  var tab = getParameterByName('tn');
  var fuelDataVals;
       
  function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  //global declartion
  $scope.locations = [];
  $scope.url       = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gIndex    = 0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
    $scope.trimColon = function(textVal) {

      if(textVal){
       var spltVal = textVal.split(":");
       return spltVal[0];
      }
    }

  function sessionValue(vid, gname,licenceExpiry){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    localStorage.setItem('licenceExpiry', licenceExpiry);
    $("#testLoad").load("../public/menu");
  }
  
  function getTodayDate(date) {
      var date = new Date(date);
      return date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2);
  };

  $scope.roundOffDecimal = function(val) {
      return val.toFixed(2);
  }

  function convert_to_24h(time_str) {
    //console.log(time_str);
      var str       =  time_str.split(' ');
      var stradd    =  str[0].concat(":00");
      var strAMPM   =  stradd.concat(' '+str[1]);
      var time      =  strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
      var hours     =  Number(time[1]);
      var minutes   =  Number(time[2]);
      var seconds   =  Number(time[2]);
      var meridian  =  time[4].toLowerCase();
  
      if (meridian == 'p' && hours < 12) {
        hours = hours + 12;
      }
      else if (meridian == 'a' && hours == 12) {
        hours = hours - 12;
      }     
      var marktimestr = ''+hours+':'+minutes+':'+seconds;      
     return marktimestr;
    };

    // millesec to day, hours, min, sec
    $scope.msToTime = function(ms) {
      days = Math.floor(ms / (24 * 60 * 60 * 1000));
      daysms = ms % (24 * 60 * 60 * 1000);
      hours = Math.floor((ms) / (60 * 60 * 1000));
      hoursms = ms % (60 * 60 * 1000);
      minutes = Math.floor((hoursms) / (60 * 1000));
      minutesms = ms % (60 * 1000);
      seconds = Math.floor((minutesms) / 1000);
    // if(days==0)
    //  return hours +" h "+minutes+" m "+seconds+" s ";
    // else
     return hours +":"+minutes+":"+seconds;
    }

    $scope.msToTime2 = function(ms) {

      days       =  Math.floor(ms / (24 * 60 * 60 * 1000));
      daysms     =  ms % (24 * 60 * 60 * 1000);
      hours      =  Math.floor((daysms) / (60 * 60 * 1000));
      hoursms    =  ms % (60 * 60 * 1000);
      minutes    =  Math.floor((hoursms) / (60 * 1000));
      minutesms  =  ms % (60 * 1000);
      seconds    =  Math.floor((minutesms) / 1000);
      
    if(days>1) {
      return days+":"+hours+":"+minutes+":"+seconds;
    } else if(days==1) {
      return days+":"+hours+":"+minutes+":"+seconds;
    } else if(days==0) {
      return hours +":"+minutes+":"+seconds;
    }
    }
    
  var delayed4 = (function () {
      var queue = [];

      function processQueue() {
        if (queue.length > 0) {
          setTimeout(function () {
            queue.shift().cb();
            processQueue();
          }, queue[0].delay);
        }
      }

      return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
          processQueue();
        }
      };
  }());

    function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
      vamoservice.getDataCall(tempurlEvent).then(function(data) {
      $scope.addressEvent[index4] = data.results[0].formatted_address;
      //console.log(' address '+$scope.addressEvent[index4])
      //var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
    })
  };

  $scope.recursiveEvent   =   function(locationEvent, indexEvent){
    var index4 = 0;
    angular.forEach(locationEvent, function(value ,primaryKey){
    //console.log(' primaryKey '+primaryKey)
      index4 = primaryKey;
      if(locationEvent[index4].address == undefined)
      {
        var latEvent     =  locationEvent[index4].latitude;
        var lonEvent     =  locationEvent[index4].longitude;
        var tempurlEvent =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";
        delayed4(2000, function (index4) {
              return function () {
                google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
              };
            }(index4));
      }
    })
  }


  function formatAMPM(date) {
      var date = new Date(date);
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
  }

    //get the value from the ui
  function getUiValue(){
      $scope.uiDate.fromdate   =  $('#dateFrom').val();
      $scope.uiDate.fromtime   =  $('#timeFrom').val();
      $scope.uiDate.todate     =  $('#dateTo').val();
      $scope.uiDate.totime     =  $('#timeTo').val();
      if(localStorage.getItem('timeTochange')!='yes'){
             updateToTime();
             $scope.uiDate.totime    =   localStorage.getItem('toTime');
        }
      $scope.uiDate.fromtimes  =  convert_to_24h($scope.uiDate.fromtime);
      $scope.uiDate.totimes    =  convert_to_24h($scope.uiDate.totime);
  }

$scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
 $scope.todayDisabled = true;
  $scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate       = getTodayDate(now.setDate(now.getDate() - 1));
switch(duration){
  
  case 'yesterday':
  $scope.yesterdayDisabled = true;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
 $scope.todayDisabled = false;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 1));
  $scope.uiDate.totime    =   '11:59 PM';
  webCall();
  break;
  case 'lastweek':
  $scope.yesterdayDisabled = false;
 $scope.weekDisabled = true;
 $scope.monthDisabled = false;
 $scope.todayDisabled = false;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 8));
  $scope.uiDate.totime    =   '11:59 PM';
  webCall();
  break;
  case 'month':
  $scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = true;
 $scope.todayDisabled = false;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 31));
  $scope.uiDate.totime    =   '11:59 PM';
  webCall();
  break;
  case 'today':
  $scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
 $scope.todayDisabled = true;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
  $scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
  $scope.uiDate.totime    =   formatAMPM(d);
  webCall();
  break;

}
}

  function webCall() {

     /*   var urlAllow    =  true; 
        var fromTms     =  utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime));
        var toTms       =  utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
        var totalTms    =  toTms-fromTms;                       
        var splitTimes  =  $scope.msToTime2(totalTms).split(':');
        var daysDiff    =  0;

          if( splitTimes.length == 4 ) {
            daysDiff = splitTimes[0];
          }
                        
      //alert(daysDiff);

        if( daysDiff == 0 ) { */

       if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {                 
          
         //var fuelRawUrl      =  GLOBAL.DOMAIN_NAME+'/getFuelRawData?vehicleId='+$scope.vehIds+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));        
         //var fuelRawUrl      =  'http://128.199.159.130:9000/getFuelRawData?userId=naplogi&vehicleId=NAPLOGI-AP16TE3458&fromTimeUtc=1533037324617&toTimeUtc=1533195298000';     
         //var fuelMachineUrl  =  'http://209.97.163.4:9000/getFuelDetailForMachinery?userId=SENTINI&vehicleId=SENTINI_AP16TY5191&fromDateTime=1536049434000&toDateTime=1536567851000';           
           var fuelMachineUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelDetailForMachinery?vehicleId='+$scope.vehIds+'&fromDateTime='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateTime='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
            
            console.log( fuelMachineUrl );
        if((licenceExpiry.indexOf(strExpired)== -1)||licenceExpiry==""||licenceExpiry=="-"){
            $http.get( fuelMachineUrl ).success(function(data) { 

              console.log( data );    

                $scope.fuelMachineData = data;
                                 
                  console.log( $scope.fuelMachineData );
           
                 
              stopLoading();  

            });
        }else{
            stopLoading();
            $scope.errMsg=licenceExpiry;
            $scope.showErrMsg = true;
        } 

         }  else {

           stopLoading();
       }

    /*  } else {

          $scope.fuelRawData = [];
          document.getElementById("container").innerHTML = '';

          alert('Plaese select less than 1 day.');
          stopLoading(); 
  
      }*/
  }


  function fuelGraph(data) {

    var Ltrs         =  [];
    var fuelDate     =  [];
  //var distCovered  =  [];
  //var spdVals      =  [];
    var tankSize = $scope.tankSize;

    console.log(data);

    console.log( tankSize );

    try {

      if(data.length) {
        for(var i = 0; i < data.length; i++) {
            if(data[i].fuelLitr !='0' || data[i].fuelLitr !='0.0') {
                Ltrs.push( { y:parseFloat(data[i].fuelLitr), odo:data[i].odoMeterReading, ignition:data[i].ignitionStatus } );
                var dat = $filter('date')(data[i].dt, "dd/MM/yyyy HH:mm:ss");
                fuelDate.push(dat);
              //distCovered.push(data[i].distanceCovered);
              //spdVals.push(data[i].speed);
            }
        }
      }
    } catch(err) {
        console.log(err.message);
    }

  console.log( Ltrs );

  $(function () {
   
        $('#container').highcharts({
            chart: {
                zoomType: 'x',
              //alignTicks: true
            },
            title: {
                text: 'Fuel'
            },
            credits: {
              enabled: false
            },
            xAxis: {
               categories: fuelDate,
                 title: {
                    text: 'Date & Time'
                 }

            },
            
            yAxis: {
                title: {
                    text: 'Fuel (Ltrs)'
                },   
              //tickInterval: 10,         
                max: parseFloat($scope.tankSize),
                min: 0,
                endOnTick: false              
            },
            tooltip: {
              formatter: function() {
                  var s, a = 0;
                //console.log(this.points);
                    $.each(this.points, function() {
                      //console.log(this.point);
                        s = '<b>'+this.x+'</b>'+'</b>'+'<br>'+' '+'</br>'+
                            '--------------------'+'<br>'+' '+'</br>'+
                            'Fuel     : ' +'  '+'<b>' + this.point.y + '</b>'+' Ltrs'+'<br>'+' '+'</br>'+
                          /*'Speed    : ' +'  '+'<b>' + this.point.speed + '</b>'+' Kmph'+'<br>'+' '+'</br>'+*/
                            'Ignition : ' +'  '+'<b>' + this.point.ignition + '</b>'+'<br>'+' '+'</br>'+
                            'Odo      : ' +'  '+'<b>' + this.point.odo + '</b>';                             
                    });
               return s;
              },
             shared: true
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null,
                    turboThreshold: 0
                }
            },

            series: [{
                type: 'area',
                name: 'Fuel Level',
                data: Ltrs
            }]
        });

    });

}


  // initial method
  $scope.$watch("url", function(val) {
    vamoservice.getDataCall($scope.url).then(function(data) {

    //startLoading();
    //$scope.selectVehiData = [];
    //alert('fuel machine..');
      $scope.vehicle_group  =  [];
      $scope.vehicle_list   =  data;

      if(data.length) {
      //alert('fuel machine 2..');
        $scope.vehiname  =  getParameterByName('vid');
        $scope.uiGroup   =  $scope.trimColon(getParameterByName('vg'));
        $scope.gName     =  getParameterByName('vg');

        console.log( $scope.gName );

        angular.forEach(data, function(val, key){
        //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
          if($scope.gName == val.group){
           
            //$scope.gIndex = 0;
            $scope.gIndex = val.rowId;
            //alert( $scope.gName );
            angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){
          //$scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});
                if($scope.vehiname == value.vehicleId){
                   $scope.shortNam = value.shortName;
                   $scope.vehIds   = value.vehicleId;
                   licenceExpiry=value.licenceExpiry;
                }
            });
          }
        });

      //console.log($scope.selectVehiData);
        sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
      }
      $scope.uiDate.fromdate    = localStorage.getItem('fromDate');
      $scope.uiDate.fromtime    = localStorage.getItem('fromTime');
      $scope.uiDate.todate    = localStorage.getItem('toDate');
      $scope.uiDate.totime    =  localStorage.getItem('toTime');
      if(localStorage.getItem('timeTochange')!='yes'){
                updateToTime();
                $scope.uiDate.totime    =   localStorage.getItem('toTime');
                }
      //alert($scope.uiDate.totime);
      $scope.uiDate.fromtimes  =   convert_to_24h($scope.uiDate.fromtime);
      $scope.uiDate.totimes    =   convert_to_24h($scope.uiDate.totime);
    //$scope.uiDate.totime     =   '11:59 PM';
    
      startLoading();
      webCall();
    //stopLoading();
    }); 
  });

    
  $scope.groupSelection = function(groupName, groupId) {
    startLoading();
    licenceExpiry="";
    $scope.errMsg="";
    $scope.gName    =  groupName;

    console.log( $scope.gName );

    $scope.uiGroup  =  $scope.trimColon(groupName);
    $scope.gIndex   =  groupId;
    var url         =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

    vamoservice.getDataCall(url).then(function(response) {
    
      $scope.vehicle_list = response;
      $scope.shortNam     = response[$scope.gIndex].vehicleLocations[0].shortName;
      $scope.vehiname     = response[$scope.gIndex].vehicleLocations[0].vehicleId;
      licenceExpiry   = response[$scope.gIndex].vehicleLocations[0].licenceExpiry;
      sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
      $scope.selectVehiData = [];
    //console.log(response);
        angular.forEach(response, function(val, key) {
          if($scope.gName == val.group) {
          //$scope.gIndex = val.rowId;
             angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys) {

                $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

                if($scope.vehiname == value.vehicleId) {
                  $scope.shortNam = value.shortName;
                  $scope.vehIds   = value.vehicleId;
                }
            });
            }
        });

      
      if((licenceExpiry!="-")&&(licenceExpiry.indexOf(strExpired)!= -1)){
           stopLoading();
           $scope.errMsg=licenceExpiry;
        }else{
          getUiValue();
          webCall();
        }
      //stopLoading();
    });

  }

  $scope.genericFunction  = function (vehid, index,shortname,position, address,groupName,licenceExp){
    startLoading();
    licenceExpiry=licenceExp;
    $scope.errMsg="";
    //licenceExpiry="gfd hgdhghf hbfg";
    $scope.vehiname = vehid;
    sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
    angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
      if(vehid == val.vehicleId){                                         
        $scope.shortNam = val.shortName;
        $scope.vehIds   = val.vehicleId;
      }
    });
    if((licenceExpiry!="-")&&(licenceExpiry.indexOf(strExpired)!= -1)){
            stopLoading();
          $scope.fuelMachineData       =  [];
          $scope.errMsg=licenceExpiry;
    }else{
              getUiValue();
              webCall();
          }
  }

  $scope.submitFunction   = function(){
 $scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
$scope.todayDisabled = false;
    startLoading();
  //$scope.interval="";
  if((licenceExpiry.indexOf(strExpired)== -1)||licenceExpiry==""||licenceExpiry=="-"){
    getUiValue();
    webCall();
  }else{     
            stopLoading();
            $scope.errMsg=licenceExpiry;
      } 
  //webServiceCall();
    //stopLoading();
  }

  $scope.exportData = function (data) {
    //console.log(data);
    var blob = new Blob([document.getElementById(data).innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, data+".xls");
  };

  $scope.exportDataCSV = function (data) {
    //console.log(data);
      CSV.begin('#'+data).download(data+'.csv').go();
  };

  $('#minus').click(function(){
    $('#menu').toggle(1000);
  });


}]);
