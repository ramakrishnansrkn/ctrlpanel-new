app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global','PDFViewerService', function($scope, $http, vamoservice, $filter, GLOBAL, pdf){
	
  //global declaration
	$scope.uiDate 				=	{};
	$scope.uiValue	 			= 	{};
  //$scope.sort                 = sortByDate('startTime');

    $scope.trvShow       =  localStorage.getItem('trackNovateView');

    var tab = getParameterByName('tn');
       
	function getParameterByName(name) {
    	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	//global declartion
	$scope.locations = [];
	$scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
	$scope.gIndex =0;

    $scope.trimColon = function(textVal){
		return textVal.split(":")[0].trim();
	}
	
    
	$scope.$watch("url", function (val) {
		vamoservice.getDataCall($scope.url).then(function(data) {
           
          //startLoading();
            $scope.selectVehiData = [];
			$scope.vehicle_group=[];
			$scope.vehicle_list = data;

			if(data.length){
				$scope.vehiname	= getParameterByName('vid');
				$scope.uiGroup 	= $scope.trimColon(getParameterByName('vg'));
				$scope.gName 	= getParameterByName('vg');
				angular.forEach(data, function(val, key){
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
					if($scope.gName == val.group){
						$scope.gIndex = val.rowId;
 
						angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

                            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

							if($scope.vehiname == value.vehicleId)
							$scope.shortNam	= value.shortName;
						})
						
					}
			    })

		  //console.log($scope.selectVehiData);
		    //sessionValue($scope.vehiname, $scope.gName)
			}
			var dateObj 			= 	new Date();
			$scope.fromNowTS		=	new Date(dateObj.setDate(dateObj.getDate()-1));
			$scope.uiDate.fromdate 		=	getTodayDate($scope.fromNowTS);
		  	$scope.uiDate.fromtime		=	'12:00 AM';
		  	$scope.uiDate.todate		=	getTodayDate($scope.fromNowTS);
		  //$scope.uiDate.totime 		=	formatAMPM($scope.fromNowTS.getTime());
            $scope.uiDate.totime 		=   '11:59 PM';
		  //webServiceCall();
		   
		   
		  stopLoading();
		});	
	});

	  console.log('TestController: new instance');
        $scope.FileNames=[];
        //$scope.pdfURL = "test.pdf";
	    $http({
	         method: 'get', 
	         url: 'getFileNames', 
	      }).
	        then(function(response) {
	             //alert('hello');
	             $scope.status = response.status;
	             $scope.FileNames   = response.data;
	             //$scope.pdfURL = $scope.FileNames[0];
	             var d = new Date();
	             var monthNo = d.getMonth();
                 var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	             $scope.pdfURL=$filter('filter')($scope.FileNames, months[monthNo])[0];

	             if($scope.pdfURL==undefined){
	             	$scope.pdfURL=$filter('filter')($scope.FileNames, months[monthNo-1])[0];
	             	if($scope.pdfURL==undefined){
	             		$scope.pdfURL = $scope.FileNames[0];
	             	}
	             }

	            //alert($scope.pdfURL);
	            console.log(response.data);
	        
	          }, function(response) {

	             $scope.data   = response.data || 'Request failed';
	             $scope.status = response.status;
	        });
		
        $scope.quantity=10;
        $scope.less=false;
		$scope.instance = pdf.Instance("viewer");
        $scope.currentPage=localStorage.getItem('currentPage');
        $scope.totalPages=localStorage.getItem('totalPages');
		$scope.nextPage = function() {
			$scope.instance.nextPage();
			$scope.currentPage=localStorage.getItem('currentPage');
		};
		$scope.zoom = function(zoomtype) {
			$scope.instance.zoom(zoomtype);
		};

		$scope.prevPage = function() {
			$scope.instance.prevPage();
			$scope.currentPage=localStorage.getItem('currentPage');
		};

		$scope.gotoPage = function(page) {
			$scope.instance.gotoPage(page);
			$scope.currentPage=localStorage.getItem('currentPage');
		};

		$scope.pageLoaded = function(curPage, totalPages) {
			$scope.currentPage = curPage;
			$scope.totalPages = totalPages;
		};

		$scope.loadProgress = function(loaded, total, state) {
			console.log('loaded =', loaded, 'total =', total, 'state =', state);
			$scope.totalPages=localStorage.getItem('totalPages');
			$scope.currentPage=localStorage.getItem('currentPage');
		};  
		$scope.changeReleaseNotes = function(file) {
			//alert(file);
            $scope.pdfURL = file;
		};
		$scope.changeQuantity = function(option) {
			//alert(option);
				if(option=='more'){
	                 $scope.less=true;
	                 $scope.quantity=$scope.FileNames.length
				}
				else{
	                 $scope.less=false;
	                 $scope.quantity= 10;
				}
		};
		
$("#testLoad").load("../public/menu");
}]);
