app.controller('mainCtrl',['$scope','$http','vamoservice','$filter','_global', function($scope,$http,vamoservice, $filter,GLOBAL) {

 // global declaration

  $scope.fileName        =  '';  
//$scope.fileExtn        =  false;
  $scope.fileNameShow    =  false;
  $scope.fileUploadsBox  =  true;  
  $scope.tripSearchBox   =  true;   
  $scope.tripBaseShow    =  false;   
  $scope.vehiBaseShow    =  false; 
  $scope.backBtnShow     =  false;
  $scope.organId         =  '';

  $scope.sort    = sortByDate('Zone');
    
  $('#notifyMsg').hide();

  $scope.getFileName = function() {

        $.ajax({
            url: 'getFiles', 
            dataType: 'text',  
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(response){
            //console.log(response);
              var arrName = response;
              var sptVal  = arrName.split('"');

              //console.log(arrName);
                var retArr =[];

                for(var i=0;i<sptVal.length;i++) {

                    if( sptVal[i].includes(".xls") || sptVal[i].includes(".xlsx") ) {
                      //console.log(sptVal[i]);   
                         retArr.push(sptVal[i]);
                    }
                }

                $scope.fileData = retArr;
              //console.log('File uploaded successfully...');

            },
            error: function (textStatus, errorThrown) {
                console.log('File upload failed...');
                  timeOutVar = setTimeout(setsTimeOuts, 3000);
            }

        });
  }

  $scope.getFileName();
  $scope.groupId = 0;

  $scope.orgUrl  = GLOBAL.DOMAIN_NAME+'/viewSite';   
  $scope.vehUrl  = GLOBAL.DOMAIN_NAME+'/getVehicleLocations';


  stopLoading();

  function sessionValue(vid, gname){
    $("#testLoad").load("../public/menu");
  }
  sessionValue();
  

  $scope.$watch("vehUrl", function (val) {
    
        $http({
            method : "GET",
            url : $scope.vehUrl
        }).then(function mySuccess(response) {

            console.log(response.data);

          //$scope.vehiName   = response.data[0].vehicleId;
            $scope.grpName    =  response.data[$scope.groupId].group;
            $scope.data       =  response.data;
            $scope.grpList    =  [];
            $scope.vehiList   =  [];

            angular.forEach(response.data, function(value, key){

               //var spltVal = value.group.split(":");
                 $scope.grpList.push({gName:value.group});

                  if($scope.grpName == value.group) { 

                     $scope.groupId=value.rowId;

                     angular.forEach(value.vehicleLocations, function(val, k){

                        $scope.vehiList.push({'vehiId' : val.vehicleId, 'vName' : val.shortName});

                            if(val.vehicleId == $scope.vehiName){
                                $scope.shortVehiId  = val.shortName;
                                $scope.vehiId  = val.vehicleId;
                            }
                    });
                  } 

            });     

             console.log( $scope.grpList );
                console.log( $scope.vehiList );

                createData();
                  callDistanceApi();

        }, function errorCallback(response) {
              
          console.log(response.status);

    });

  });

   /* Get Org Ids */
   
    $scope.$watch("orgUrl", function (val) {

      $http.get($scope.orgUrl).success(function(response) {
        console.log(response);
        $scope.orgList = [];
        var orgId = response.orgIds;
        $scope.organIds = response.orgIds;
        $scope.organId  = $scope.organIds[0];
        alert($scope.organId);

          for( var i=0; i<orgId.length; i++ ) {
               console.log(orgId[i]);
                 $scope.orgList.push( { 'orgName' : orgId[i] } );
          }
      });

    }); 


       
  
  $scope.getMap = function(routesMap){

    myMap();

    $scope.windowRouteName=routesMap;
       
   // if(!routesMap =='' && $scope.orgIds.length>0 && !$scope.orgIds==''){
        $.ajax({
          async:false,
          method:'GET',
          url:"storeOrgValues/mapHistory",
          data:{"maproute":routesMap,"organId":$scope.organIds},
          success:function(response){
            $scope.mapValues =response;
            getMapArray($scope.mapValues);                       }
        });
   // }        
  }

var latLanpath=[];
var markerList=[];
var pathCoords=[];


function clearMap(path){
    
    for (var i=0; i<latLanpath.length; i++){
      latLanpath[i].setMap(null);
    }

    for (var i = 0; i < markerList.length; i++) {
      markerList[i].setMap(null);
    }
}

function getMapArray(values){

    var latSplit ;
    var latLangs=values;
    clearMap(pathCoords);
    pathCoords=[];

    for(i=0;i<latLangs.length;i++){

        latSplit = latLangs[i].split(",");
        pathCoords.push({"lat": latSplit[0],"lng": latSplit[1]});
    }

  dvMap.setCenter(new google.maps.LatLng(pathCoords[0].lat,pathCoords[0].lng)); 
  autoRefresh(dvMap);
}

function myMap(){

  var mapCanvas=document.getElementById("dvMap");
  var mapOptions={
    center: new google.maps.LatLng(0,0),
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  dvMap = new google.maps.Map(mapCanvas,mapOptions);
  new google.maps.event.trigger(dvMap, "resize");
}
//myMap();

/*$('#myModal2').on('shown.bs.modal', function () {
  google.maps.event.trigger(dvMap, "resize");
});*/

function moveMarker(marker, latlng) {
  
  marker.setPosition(latlng);
}

function autoRefresh(map) {
  var i, route, marker;
  
  route = new google.maps.Polyline({
    path: [],
    geodesic : true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2,
    editable: false,
    map:map
  });
                
  latLanpath.push(route);
    marker = new google.maps.Marker({map:map,icon:""});
    markerList.push(marker);
  
    for(i=0; i<pathCoords.length; i++) {
    
      setTimeout(function (coords){
        var latlng = new google.maps.LatLng(coords.lat, coords.lng);
        route.getPath().push(latlng);
                moveMarker( marker, latlng);
                map.panTo(latlng);
            
      }, 0* i, pathCoords[i]);
    }
}




  
  $(document).ready(function() {

      $('input[type="file"]').change(function(e) {
           
            var fileVal  = e.target.files[0].name;
          //alert('The file "' + fileName +  '" has been selected.');
            var splitVal = fileVal.split(".");

            if( splitVal[1]=="xls" || splitVal[1]=="xlsx"){
                 $scope.fileName = fileVal;
                 //$scope.fileExtn = true;
                   console.log($scope.fileName);
            } else{
                  $scope.fileName='';
                  $('#file').val('');
                  alert("Please choose '.xls (or) .xlsx' file to upload.");
                  //$scope.fileExtn=false;
            }
      });
  });
    

  $scope.orgFunction = function(val) {
    console.log(val);
      $scope.organId = val.orgName;
  }

  $scope.gMapLink1 = function(val) {

    var splitValue    = val.split(',');

    return splitValue[0];
  }

  $scope.gMapLink2 = function(val) {

    var splitValue    = val.split(',');

    return splitValue[1];
  }

  $scope.backUpload = function(){

      $scope.fileNameShow    =  false;
      $scope.fileUploadsBox  =  true; 
  }


   var timeOutVar;   
    function setsTimeOuts() {

        $('#notifyF').hide(1200);
             $('#notifyS').hide(1200);
                    $('#notifyMsg').hide(1200);


        if(timeOutVar!=null){
          //console.log('timeOutVar'+timeOutVar);
            clearTimeout(timeOutVar);
        }
    }

    $scope.tripSheetView = function() {

      $scope.fileUploadsBox  =  false; 
      $scope.fileNameShow    =  true;
    } 

function callApiFunc() {

       console.log($scope.organId);
    // console.log($scope.fileName);
    // var tripUrl = "http://128.199.159.130:9000/uploadTripsheet?userId=MSS&orgId=RAJ&tripName=sampleTripsheet.csv";  
    // var tripUrl = GLOBAL.DOMAIN_NAME+'/uploadTripsheet?orgId='+$scope.organId+'&tripName='+$scope.fileName;  
    // http://128.199.159.130:9000/addRoutesDetailForALY?userId=ALY&fileName=aly.xlsx&orgId=ALY
    // encodeURIComponent($scope.fileName)

    var tripUrl = GLOBAL.DOMAIN_NAME+'/addRoutesDetailForALY?orgId='+$scope.organId+'&fileName='+$scope.fileName; 


    $http({
          method: 'GET',
          url: tripUrl
    }).then(function successCallback(response) {

            if(response.status==200) {
                console.log("Send File Name Successfully!..");
            }
    }, function errorCallback(response) {
              console.log(response.status);
    });
}

    

   $scope.submitFunction = function() {

    console.log("submit.....");
      console.log($scope.fileName);

     if($scope.fileName!=='' && $scope.organId!='') {        

          var file_data = $('#file').prop('files')[0];   
            var form_data = new FormData();         

        form_data.append('file', file_data);
          //alert(form_data);  
            console.log(form_data);

        $.ajax({
            url: 'fileUpload', // point to server-side PHP script 
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(response){

                console.log('File uploaded successfully...');

                    $('#notifyMsg').show();
                    $('#notifyF').hide();
                    $('#notifyS').show(500);

                    timeOutVar = setTimeout(setsTimeOuts, 3000);
                    $scope.getFileName();

                    callApiFunc();

            },
            error: function (textStatus, errorThrown) {

                    console.log('File upload failed...');

                    $('#notifyMsg').show();
                    $('#notifyS').hide();
                    $('#notifyF').show(500);

                    timeOutVar = setTimeout(setsTimeOuts, 3000);
                }

        });

      } else {

          if($scope.fileName=='' && $scope.organId==''){

              alert("Please.. Choose xls (or) xlsx file and Organization name.");

          } else if($scope.fileName=='' && $scope.organId!='') {

              alert("Please.. Choose xls (or) xlsx file.");

          } else if($scope.fileName!='' && $scope.organId=='') {

              alert("Please.. Choose Organization Name.");

          }
       }

    }



    
   $scope.groupSelection = function(val) {

    console.log(val);

    var grpVal     =  val;
    $scope.grpName =  val;

    var grpVehUrl  =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+grpVal;
    console.log(grpVehUrl);

        $http({
            method : "GET",
            url : grpVehUrl
        }).then(function mySuccess(response) {

            console.log(response.data);

            //$scope.vehiName = response.data[0].vehicleId;
            //$scope.grpName  = response.data[$scope.groupId].group;

            $scope.data     = response.data;

            $scope.grpList  = [];
            $scope.vehiList = [];

            angular.forEach(response.data, function(value, key){

               //var spltVal = value.group.split(":");

                $scope.grpList.push({gName:value.group});

                  if($scope.grpName == value.group) { 

                    $scope.groupId = value.rowId;

                    angular.forEach(value.vehicleLocations, function(val, k){

                        $scope.vehiList.push({'vehiId' : val.vehicleId, 'vName' : val.shortName});

                            if(val.vehicleId == $scope.vehiName){
                                $scope.shortVehiId  = val.shortName;
                                $scope.vehiId  = val.vehicleId;
                            }
                    });
                  } 

            });  

            callDistanceApi();

             console.log( $scope.grpList );
                console.log( $scope.vehiList );

        }, function errorCallback(response) {
            console.log(response.status);
    });
  }  

  function createData(){

   /* var retArr=[];
   
       retArr.push({Zone:'Chennai', RO:1, AO:2, Transporter:'ALY',Month:'April', Dealer:'SMP', ShipmentQty:2, Transit:'No', Delivery:'Yes'});  
       retArr.push({Zone:'Pune', RO:1, AO:2, Transporter:'ALY',Month:'May', Dealer:'SMP', ShipmentQty:5, Transit:'No', Delivery:'No'});  
       retArr.push({Zone:'Hydrebad', RO:1, AO:2, Transporter:'ALY',Month:'April', Dealer:'SMP', ShipmentQty:3, Transit:'Yes', Delivery:'No'});  
       retArr.push({Zone:'New Delhi', RO:2, AO:3, Transporter:'ALY',Month:'February', Dealer:'SMP', ShipmentQty:5, Transit:'No', Delivery:'Yes'});  
       retArr.push({Zone:'Bangalore', RO:1, AO:2, Transporter:'ALY',Month:'September', Dealer:'SMP', ShipmentQty:4, Transit:'No', Delivery:'No'});  */
      //var reportUrl  =  'http://128.199.159.130:9000/getDistanceForRoutesForAly?userId=ALY&groupName=ALY:SMP&orgId=ALY';

      var reportUrl  = GLOBAL.DOMAIN_NAME+'/getDistanceForRoutesForAly?groupName='+$scope.grpName+'&orgId='+$scope.organId;

      console.log(reportUrl);

       $http({
          method: 'GET',
          url: reportUrl
        }).then(function successCallback(response) {         

            $scope.distRouteData = response.data.getData;

             console.log($scope.distRouteData); 

            stopLoading();

        }, function errorCallback(response) {
              console.log(response.status);
        });
 


  }

  function fmsData(data){
    var retArr = [];

      angular.forEach(data, function(value, key) {

          angular.forEach(value, function(val, k) {

               console.log(val);
                
                retArr.push(val);
          });
      });     

        console.log(retArr);
   return retArr;
  }

  
  $scope.reportFunc = function(zone) {
    //alert(zone);
    var str = zone;

    //console.log( $scope.distRouteData.CHENNAI ); 
       //$scope.fmsData = $scope.distRouteData[zone];
     $scope.fmsData = fmsData( $scope.distRouteData[zone] );
  }

 $scope.tabChange = function(val){

  if(val=="report"){

    $scope.sort = sortByDate('vehicleName');

  } else if(val=="dash"){

    $scope.sort = sortByDate('Zone');
  }

 }

 function callDistanceApi() {
    
    //var fmsUrl   =  GLOBAL.DOMAIN_NAME+'/getDistanceForRoutes?groupName='+$scope.grpName;
    //var fmsUrl   =  'http://128.199.159.130:9000/getDistanceForRoutes?userId=ALY&groupName=ALY:SMP';
    //var fmsUrl   = 'http://128.199.159.130:9000/getDistanceForRoutesForAly?userId=ALY&groupName=ALY:SMP&orgId=ALY';
    var fmsUrl   = GLOBAL.DOMAIN_NAME+'/getDistanceForRoutesForAly?groupName='+$scope.grpName+'&orgId='+$scope.organId;

    console.log(fmsUrl);

        $http({
          method: 'GET',
          url: fmsUrl
        }).then(function successCallback(response) {

            console.log(response.data);

            $scope.fmsData = response.data;



            stopLoading();

        }, function errorCallback(response) {
              console.log(response.status);
        });
  }
 


}]);
