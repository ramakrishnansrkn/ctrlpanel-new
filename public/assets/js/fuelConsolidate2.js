app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', function($scope, $http, vamoservice, $filter, GLOBAL){
  
  //global declaration
    $scope.uiDate         =  {};
    $scope.uiValue        =  {};
  //$scope.sort           =  sortByDate('startTime');
  //$scope.totStartFuel   =  0;
    $scope.totEndFuel     =  0;
    $scope.totFuelCon     =  0;
    $scope.totFuelFill    =  0;
    $scope.totFuelTheft   =  0;
    $scope.totStartKms    =  0;
    $scope.totEndKms      =  0;
    $scope.totDist        =  0;
    $scope.totKmpl        =  0;                                                                     
    $scope.totLtrPerHrs   =  0;
    var licenceExpiry="";
    var strExpired='expired';
    if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
    $scope.errMsg="";
    var tab               =   getParameterByName('tn');
    var assLabel          =   localStorage.getItem('isAssetUser');
  //console.log(assLabel);
    $scope.sort           =   sortByDate('date',true);
   
  if( assLabel == "true" ) {
      $scope.vehiLabel  =  "Asset";
      $scope.vehiImage  =  true;
  } else if( assLabel == "false" ) {
      $scope.vehiLabel  =  "Vehicle";
      $scope.vehiImage  =  false;
  } else {
      $scope.vehiLabel  =  "Vehicle";
      $scope.vehiImage  =  false;
  }
       
  function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  //global declartion
  $scope.locations  =  [];
  $scope.url        =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gIndex     =  0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
  $scope.trimColon = function(textVal){
    return textVal.split(":")[0].trim();
  }

  function sessionValue(vid, gname,licenceExpiry){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    localStorage.setItem('licenceExpiry', licenceExpiry);
    $("#testLoad").load("../public/menu");
  }

   $scope.ltphValue = function(ms) {

        hours       =  Math.floor((ms) / (60 * 60 * 1000));
        hoursms     =  ms % (60 * 60 * 1000);
        minutes     =  Math.floor((hoursms) / (60 * 1000));

        var retVal  =  hours+"."+minutes;

        //alert(retVal);

     return parseFloat(retVal);   
   }
  
  function getTodayDate(date) {
      var date = new Date(date);
    return date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2);
  };

  $scope.roundOffDecimal = function(val) {
    return val.toFixed(2);
  }

  $scope.parseInts = function(val) {
    return parseInt(val);   
  }

  $scope.totalKmpl = function(dist,fuel) {
    var totKmpl = dist/fuel;
    totKmpl     = totKmpl.toFixed(2);
    if(totKmpl=="NaN"||totKmpl==Infinity)totKmpl=0.0;
   return totKmpl;   
  }

  $scope.totalLtph = function(fuel,engHrs) { 
    var totLtph = fuel/$scope.ltphValue(engHrs);

      //console.log( fuel );
       //console.log( $scope.ltphValue(engHrs) );
     
    totLtph  = totLtph.toFixed(2);
    if(totLtph=="NaN"||totLtph==Infinity)totLtph=0.0;

   return totLtph;   
  }

  function convert_to_24h(time_str) {
    //console.log(time_str);
      var str       =  time_str.split(' ');
      var stradd    =  str[0].concat(":00");
      var strAMPM   =  stradd.concat(' '+str[1]);
      var time      =  strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
      var hours     =  Number(time[1]);
      var minutes   =  Number(time[2]);
      var seconds   =  Number(time[2]);
      var meridian  =  time[4].toLowerCase();
  
      if (meridian == 'p' && hours < 12) {
        hours = hours + 12;
      }
      else if (meridian == 'a' && hours == 12) {
        hours = hours - 12;
      }     
      var marktimestr =''+hours+':'+minutes+':'+seconds;      
    return marktimestr;
    };

    $scope.msToTime2 = function(ms) {

        days       =  Math.floor(ms / (24*60*60*1000));
        daysms     =  ms % (24*60*60*1000);
        hours      =  Math.floor((ms)/(60*60*1000));
        hoursms    =  ms % (60*60*1000);
        minutes    =  Math.floor((hoursms)/(60*1000));
        minutesms  =  ms % (60*1000);
        sec        =  Math.floor((minutesms)/(1000));

      //if(days>=1) {
      //  return (days-1)+"d : "+23+" : "+minutes;
       // } else {
        return hours+" : "+minutes+" : "+sec;
      //}
    }

    // millesec to day, hours, min, sec
    $scope.msToTime = function(ms) {

        days       =  Math.floor(ms / (24 * 60 * 60 * 1000));
        daysms     =  ms % (24 * 60 * 60 * 1000);
        hours      =  Math.floor((ms) / (60 * 60 * 1000));
        hoursms    =  ms % (60 * 60 * 1000);
        minutes    =  Math.floor((hoursms) / (60 * 1000));
        minutesms  =  ms % (60 * 1000);
        seconds    =  Math.floor((minutesms) / 1000);
      //if(days==0)
      //return hours +" h "+minutes+" m "+seconds+" s ";
      //else
     return hours +":"+minutes+":"+seconds;
    }
    
  var delayed4 = (function () {
      var queue = [];

      function processQueue() {
        if (queue.length > 0) {
          setTimeout(function () {
            queue.shift().cb();
            processQueue();
          }, queue[0].delay);
        }
      }

      return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
          processQueue();
        }
      };
  }());

    function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
      vamoservice.getDataCall(tempurlEvent).then(function(data) {
      $scope.addressEvent[index4] = data.results[0].formatted_address;
      //console.log(' address '+$scope.addressEvent[index4])
      //var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
    })
  };

  $scope.recursiveEvent   =   function(locationEvent, indexEvent) {
    var index4 = 0;
    angular.forEach(locationEvent, function(value ,primaryKey){
      //console.log(' primaryKey '+primaryKey)
      index4 = primaryKey;
      if(locationEvent[index4].address == undefined)
      {
        var latEvent     =   locationEvent[index4].latitude;
        var lonEvent     =   locationEvent[index4].longitude;
        var tempurlEvent   =   "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";

        delayed4(2000, function (index4) {
              return function () {
                google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
              };
          }(index4));
      }
    })
  }


  function formatAMPM(date) {
        var date = new Date(date);
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
  }

  $scope.orgName = '';

  function totalValues(data) {

            var startFuel        =  0.0;
            var endFuel          =  0.0;
            var fuelConsumption  =  0.0;
            var fuelFilling      =  0.0;
            var fuelTheft        =  0.0;
            var startKms         =  0.0;
            var endKms           =  0.0;
            var dist             =  0.0;
            var kmpl             =  0.0;    
            var ltrsPerHrs       =  0.0;

            var engineIdleHrs    =  0;
            var ignitionHrs      =  0;
            var engineHrs        =  0;
            var secIgnitionHrs   =  0;


            angular.forEach(data, function(val, key) {
              if(val!=null){
              //console.log(val);
                engineIdleHrs    =  engineIdleHrs+parseInt(val.engineIdleHrs);
                ignitionHrs      =  ignitionHrs+parseInt(val.ignitionHrs);
                engineHrs        =  engineHrs+parseInt(val.engineRunningHrs);
                if(val.secondaryEngineDuration==null)val.secondaryEngineDuration=0;
                secIgnitionHrs   =  secIgnitionHrs+parseInt(val.secondaryEngineDuration);

              //console.log(engineIdleHrs);
             
                startFuel        =  startFuel+parseFloat(val.startFuel);
                endFuel          =  endFuel+parseFloat(val.endFuel);
                fuelConsumption  =  fuelConsumption+parseFloat(val.fuelConsumption);
                fuelFilling      =  fuelFilling+parseFloat(val.fuelFilling);
                fuelTheft        =  fuelTheft+parseFloat(val.fuelTheft);
                startKms         =  startKms+parseFloat(val.startKms);
                endKms           =  endKms+parseFloat(val.endKms);
                dist             =  dist+parseFloat(val.dist);
                kmpl             =  kmpl+parseFloat(val.kmpl);     
                ltrsPerHrs       =  ltrsPerHrs+parseFloat(val.ltrsPerHrs);
              }
                                                                 
            });  

                $scope.startFuel        =  startFuel;
                $scope.endFuel          =  endFuel;
                $scope.fuelConsumption  =  fuelConsumption;
                $scope.fuelFilling      =  fuelFilling;
                $scope.fuelTheft        =  fuelTheft;
                $scope.startKms         =  startKms;
                $scope.endKms           =  endKms;
                $scope.dist             =  dist;
                $scope.kmpl             =  kmpl.toFixed(2);     
                $scope.ltrsPerHrs       =  ltrsPerHrs.toFixed(2);

                $scope.engineIdleHrs    =  engineIdleHrs;
                $scope.ignitionHrs      =  ignitionHrs;
                $scope.engineHrs        =  engineHrs;
                $scope.secIgnitionHrs   =  secIgnitionHrs;

                console.log( 'Engine Hours....  :  ' + $scope.engineHrs );
              //console.log(  $scope.startFuel  );
  }

$scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
 $scope.todayDisabled = false;
$scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  

switch(duration){
  
  case 'yesterday':
  $scope.yesterdayDisabled = true;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
 $scope.todayDisabled = false;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 1));
  webCall();
  break;
  case 'lastweek':
  $scope.yesterdayDisabled = false;
 $scope.weekDisabled = true;
 $scope.monthDisabled = false;
 $scope.todayDisabled = false;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 8));
  webCall();
  break;
  case 'month':
  $scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = true;
 $scope.todayDisabled = false;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 31));
  webCall();
  break;
  case 'today':
  $scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
 $scope.todayDisabled = true;
  var d = new Date();
  $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
  $scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
  webCall();
  break;

}
}

    function webCall() {
      //alert('hello');
        //if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {
           //var tollUrl = GLOBAL.DOMAIN_NAME+'/getTollgateReport?group='+$scope.gName+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
             //var fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedFuelReport?groupName='+$scope.gName+'&date='+$scope.uiDate.fromdate;
        //}
        //var fuelConUrl  =  'http://188.166.244.126:9000/getConsolidatedFuelReport?userId=naplogi&groupName=NAPLOGI:SMP&date=2018-07-05';
        //var fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedFuelReport?groupName='+$scope.gName+'&date='+$scope.uiDate.fromdate;
        //var fuelConUrl  =  'http://188.166.244.126:9000/getConsolidatedFuelReportBasedOnVehicle?userId=naplogi&vehicleId=NAPLOGI-AP16T53666&fromDate=2018-08-25&toDate=2018-08-29';
        var fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedFuelReportBasedOnVehicle?vehicleId='+$scope.vehIds+'&fromDate='+$scope.uiDate.fromdate+'&toDate='+$scope.uiDate.todate;

            console.log( fuelConUrl );
            $scope.fuelConData = [];
      if((licenceExpiry.indexOf(strExpired)== -1)||licenceExpiry==""||licenceExpiry=="-"){
        $http.get(fuelConUrl).success(function(data) {
              $scope.fuelConData  =  data;
                console.log($scope.fuelConData);

              totalValues(data);
              if($scope.fuelConData.length!=0){
                $scope.orgName  =  $scope.fuelConData[0].orgId;
              }
              

            stopLoading();
        }); 
      }else{
            stopLoading();
            $scope.errMsg=licenceExpiry;
        } 
    }

  //get the value from the ui
  function getUiValue() {
      $scope.uiDate.fromdate   =  $('#dateFrom').val();
    //$scope.uiDate.fromtime   =  $('#timeFrom').val();
      $scope.uiDate.todate     =  $('#dateTo').val();
    //$scope.uiDate.totime     =  $('#timeTo').val();
  }


// service call for the event report

/*function webServiceCall(){
    $scope.siteData = [];
    if((checkXssProtection($scope.uiDate.fromdate) == true) && (checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true)) {
      
      var url   = GLOBAL.DOMAIN_NAME+"/getActionReport?vehicleId="+$scope.vehiname+"&fromDate="+$scope.uiDate.fromdate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+$scope.uiDate.todate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&interval="+$scope.interval+"&stoppage="+$scope.uiValue.stop+"&stopMints="+$scope.uiValue.stopmins+"&idle="+$scope.uiValue.idle+"&idleMints="+$scope.uiValue.idlemins+"&notReachable="+$scope.uiValue.notreach+"&notReachableMints="+$scope.uiValue.notreachmins+"&overspeed="+$scope.uiValue.speed+"&speed="+$scope.uiValue.speedkms+"&location="+$scope.uiValue.locat+"&site="+$scope.uiValue.site+'&fromDateUTC='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUTC='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
      vamoservice.getDataCall(url).then(function(responseVal){
        $scope.recursiveEvent(responseVal, 0);
        $scope.eventData = responseVal;
        var entry=0,exit=0; 
        angular.forEach(responseVal, function(val, key){
          if(val.state == 'SiteExit')
            exit++ 
          else if (val.state == 'SiteEntry')
            entry++
        })
        $scope.siteEntry  = entry;
        $scope.siteExit   = exit;
        stopLoading();
      });
    }
    stopLoading();
  }*/

  // initial method
$scope.total = function(val1, val2){
  return parseFloat(val1)+parseFloat(val2);
}
  $scope.$watch("url", function (val) {
    vamoservice.getDataCall($scope.url).then(function(data) {          
          //startLoading();
            $scope.selectVehiData  =  [];
      $scope.vehicle_group   =  [];
      $scope.vehicle_list    =  data;

      if(data.length) {

        $scope.vehiname  =  getParameterByName('vid');
        $scope.uiGroup   =  $scope.trimColon(getParameterByName('vg'));
        $scope.gName   =  getParameterByName('vg');
        //console.log(data);
        //$scope.gName   =  data[0].group;
          //console.log(data[0].group);

            if($scope.vehiname == 'undefined' || $scope.vehiname == '') {
               $scope.vehiname  =  data[$scope.gIndex].vehicleLocations[0].vehicleId; 
                licenceExpiry=data[$scope.gIndex].vehicleLocations[0].licenceExpiry;
             //alert($scope.vehiname );
            } 

        if($scope.gName == 'undefined' || $scope.gName == ''){
           $scope.gName  =  data[$scope.gIndex].group; 
        } 
          
        angular.forEach(data, function(val, key) {
              //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
          if($scope.gName == val.group) {
            $scope.gIndex = val.rowId;
     
            angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

                        $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

              if($scope.vehiname == value.vehicleId){
                  $scope.shortNam  =  value.shortName;
                  $scope.vehIds   = value.vehicleId;
                  licenceExpiry=value.licenceExpiry;
              }
              });
            }
          });
        //console.log($scope.selectVehiData);
        sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
        }

            var dateObj1       =   new Date();
            var  dateNow        = new Date(dateObj1.setDate(dateObj1.getDate()));
            var fromdatenow         = getTodayDate(dateNow);
            //alert(fromdatenow);
              if(fromdatenow==localStorage.getItem('fromDate'))
              {
                  var dateObj       =   new Date();
                  $scope.fromNowTS    = new Date(dateObj.setDate(dateObj.getDate()-1));
                  $scope.uiDate.fromdate    = getTodayDate($scope.fromNowTS);
                  $scope.uiDate.todate    = getTodayDate($scope.fromNowTS);
              }else{

                  $scope.uiDate.fromdate    = localStorage.getItem('fromDate');
                  $scope.uiDate.todate      = localStorage.getItem('toDate');
                   }
      //webServiceCall();
        startLoading();
        webCall();
      //stopLoading();
    }); 
  });

    
   
    $scope.groupSelection = function(groupName, groupId) {
    startLoading();
    licenceExpiry="";
    $scope.errMsg="";
    $scope.gName  =  groupName;
    $scope.uiGroup  =  $scope.trimColon(groupName);
    $scope.gIndex =  groupId;
    var url     =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

    vamoservice.getDataCall(url).then(function(response) {
    
      $scope.vehicle_list     =  response;
      $scope.shortNam       =  response[$scope.gIndex].vehicleLocations[0].shortName;
      $scope.vehiname       =  response[$scope.gIndex].vehicleLocations[0].vehicleId;
      licenceExpiry   = response[$scope.gIndex].vehicleLocations[0].licenceExpiry;
      sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
            $scope.selectVehiData   =  [];
            //console.log(response);
              angular.forEach(response, function(val, key) {
          if($scope.gName == val.group){
          //  $scope.gIndex = val.rowId;
                        angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys) {

                            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

              if($scope.vehiname == value.vehicleId){
                $scope.shortNam  =  value.shortName;
                $scope.vehIds   = value.vehicleId;
                }
            });
            }
        });
      if((licenceExpiry!="-")&&(licenceExpiry.indexOf(strExpired)!= -1)){
           stopLoading();
           $scope.errMsg=licenceExpiry;
        }else{
          getUiValue();
          webCall();
        }
      
      //webServiceCall();
        //stopLoading();
    });

  }


    $scope.genericFunction  = function (vehid, index,shortname,position, address,groupName,licenceExp){
     startLoading();
     licenceExpiry=licenceExp;
     $scope.errMsg="";
     $scope.vehiname    = vehid;
     sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
     angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
      if(vehid == val.vehicleId) {
      $scope.shortNam = val.shortName;
      $scope.vehIds   = val.vehicleId;
      }
     });
     if((licenceExpiry!="-")&&(licenceExpiry.indexOf(strExpired)!= -1)){
          stopLoading();
          $scope.datashow=true;
          $scope.errMsg=licenceExpiry;
    }else{
              getUiValue();
              webCall();
          }
   //webServiceCall();
  }

    $scope.submitFunction   = function(){
 $scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
  $scope.todayDisabled = false;
    startLoading();
    if((licenceExpiry.indexOf(strExpired)== -1)||licenceExpiry==""||licenceExpiry=="-"){
    getUiValue();
    webCall();
  }else{     
            stopLoading();
            $scope.errMsg=licenceExpiry;
      } 
  //webServiceCall();
    //stopLoading();
  }

  $scope.exportData = function (data) {
    //console.log(data);
    var blob = new Blob([document.getElementById(data).innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, data+".xls");
    };

    $scope.exportDataCSV = function (data) {
    //console.log(data);
    CSV.begin('#'+data).download(data+'.csv').go();
    };

  $('#minus').click(function(){
    $('#menu').toggle(1000);
  })

}]);
